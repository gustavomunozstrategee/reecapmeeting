
<?php
require_once __DIR__ . '/vendor/autoload.php';  


define('APPLICATION_NAME', 'Clave de API Recapmeeting');
define('CREDENTIALS_PATH', '~/.credentials/calendar-php-quickstart.json');
if(isset($meetingAdd)){
  define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret_add_event.json');
} else {
  if(isset($meetingsFollowing)){
      if($meetingsFollowing == true){ 
        define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret_add_event_meeting_following.json');  
      } else if($meetingsFollowing == false){ 
        define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret_list_event.json');
      } 
  } else {
    define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret_edit_meeting.json');   
  }  
}
 
define('SCOPES', implode(' ', array(Google_Service_Calendar::CALENDAR)));
  
    function getClient($url = null){
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH); 
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setDeveloperKey("AIzaSyDWcymvKKbKOw4Gv1imW3CtIVdQ8kJCEjs");
       // Load previously authorized credentials from a file.
        $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);  
        if (file_exists($credentialsPath)) {         
            $accessToken = json_decode(file_get_contents($credentialsPath), true);         
        } else {
          // Request authorization from the user.
          $authUrl  = $client->createAuthUrl(); 
          //header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));          
          if (isset($_GET['code'])) {
              
              $authCode = $_GET['code'];
              // Exchange authorization code for an access token.
              $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
              //header('Location: ' . filter_var($this->redirectUri,FILTER_SANITIZE_URL));
              if(!file_exists(dirname($credentialsPath))) {
                  mkdir(dirname($credentialsPath), 0700, true);
              }
              file_put_contents($credentialsPath, json_encode($accessToken));
              if(isset($url)){
                header("Location: $url"); 
              }
          }else{
              header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));    
          } 
      }
     if(!empty($accessToken)){
      $client->setAccessToken($accessToken);
      if ($client->isAccessTokenExpired()) {
          // save refresh token to some variable
          $refreshTokenSaved = $client->getRefreshToken();
          // update access token
          $client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
          // pass access token to some variable
          $accessTokenUpdated = $client->getAccessToken();
          // append refresh token
          $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;
          //Set the new acces token
          $accessToken = $refreshTokenSaved;
          $client->setAccessToken($accessToken);
          // save to file
          file_put_contents($credentialsPath,json_encode($accessTokenUpdated));
          } 
      } else {
        header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));  
      } 
      return $client;
  } 

function expandHomeDirectory($path) {
  $homeDirectory = getenv('HOME');
  if (empty($homeDirectory)) {
    $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
  }
  return str_replace('~', realpath($homeDirectory), $path);
}

function listEvents(){
  $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
  $url         = configure::read("REDIRECT_LIST_EVENT");  
  $client      = getClient($url);  
  $clientNew   = (array) $client;
  $accessToken = false;
  foreach ($clientNew as $key => $value) { 
    if(!empty(@$value["access_token"])){
      $accessToken = true;
    } 
  }
  if (!empty($_GET['callback']) || $accessToken == true) { 
    $service    = new Google_Service_Calendar($client);
    $dateMin    = date('Y-m-01');
    $dateMax    = date('Y-m-t');
    $timeMin    = $dateMin.'T00:00:00Z';
    $timeMax    = $dateMax.'T00:00:00Z'; 
    $optParams  = array("timeMin" => $timeMin, "timeMax" => $timeMax);
    $events     = $service->events->listEvents('primary', $optParams); 
    $infoEvents = array(); 
    while(true) {
      foreach ($events->getItems() as $event) {
        if($event->start->date){ 
          $createDate    = new DateTime($event->start->date);
          $initialDate   = $createDate->format('Y-m-d'); 
        } else if($event->start->dateTime){        
          $createDateEnd = new DateTime($event->start->dateTime);
          $initialDate   = $createDateEnd->format('Y-m-d H:i:00'); 
        }
        if($event->end->date){
          $createDateEnd = new DateTime($event->end->date);
          $finalDate     = $createDateEnd->format('Y-m-d'); 
        } else if($event->end->dateTime){          
          $createDateEnd = new DateTime($event->end->dateTime);
          $finalDate     = $createDateEnd->format('Y-m-d H:i:00'); 
        } 
        $infoEvents[] = array( 
          "title"       => $event->getSummary(),
          "start"       => $initialDate,  
          "startDate"   => $initialDate,  
          "finalDate"   => $finalDate,  
          "end"         => $finalDate, 
          "location"    => $event->location, 
          "summary"     => $event->summary, 
          "description" => $event->description, 
          "allDay"      => false  
          ); 
      } 
      $pageToken = $events->getNextPageToken();
      if ($pageToken) {
        $optParams = array('pageToken' => $pageToken);
        $events = $service->events->listEvents('primary', $optParams);
      } else {
        break;
      }
    }
  } 
  cleanLog(); 
  unlink($credentialsPath); 
  return $infoEvents;
}

function saveAllMeetingsFollowing($primary, $infoTotal){ 
  $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH); 
  $url            = configure::read("REDIRECT_ADD_EVENT_FOLLOWING");
  $client          = getClient($url); 
  $clientNew       = (array) $client;
  $accessToken     = false;
  foreach ($clientNew as $key => $value) { 
    if(!empty(@$value["access_token"])){
      $accessToken = true;
    } 
  }  
  if (!empty($_GET['callback']) || $accessToken == true) { 
    $service       = new Google_Service_Calendar($client);
    $availableUser = "";
    if($infoTotal["meetingFormData"]["Meeting"]["private"] == 1){
      $availableUser = "transparent";
    } else if ($infoTotal["meetingFormData"]["Meeting"]["private"] == 2) {
      $availableUser  = "opaque";
    } 
    AddEventInCalendar($service, $availableUser, $infoTotal, $credentialsPath);  
  }

}

function AddEventInCalendar($service, $availableUser, $infoTotal, $credentialsPath){
  $segundos    = ":00-05:00";
  $t           = "T";
  $subject     = $infoTotal["meetingFormData"]["Meeting"]["subject"];
  $location    = isset($infoTotal["meetingFormData"]["Meeting"]["location"]) ? $infoTotal["meetingFormData"]["Meeting"]["location"] : __("No suministrada");
  $description = $infoTotal["meetingFormData"]["Meeting"]["description"];
  $emails      = array();
  foreach ($infoTotal["Inviteds"] as $user) {
    $emails[] = array("email" =>$user["email"]); 
  }
  $calendarIds = array();
  foreach ($infoTotal["meetingsFollowingData"] as $meeting) { 
    $dates = buildDateToEvent($meeting["start_date"], $meeting["end_date"]);
    $event = new Google_Service_Calendar_Event(array(
      'summary'      => $subject,
      'transparency' => $availableUser,
      'location'     => $location,
      'description'  => $description,
      'start' => array(
        'dateTime' => $dates["fechaInicioFinal"],
        'timeZone' => 'America/Los_Angeles',
        ),
      'end' => array(
        'dateTime' => $dates["fechaFinFinal"],
        'timeZone' => 'America/Los_Angeles',
        ),
      'reminders' => array(
        'useDefault' => FALSE,
        'overrides' => array(
          array('method' => 'email', 'minutes' => 0),
          array('method' => 'popup', 'minutes' => 10),
          ),
        )
      )); 
    $event["attendees"] = $emails;
    $calendarId       = 'primary';
    $optionaArguments = array("sendNotifications"=>true);
    $event         = $service->events->insert($calendarId, $event, $optionaArguments);+
    $calendarIds[] = $event["id"]; 
  }
  cleanLog(); 
  $_SESSION["CalendarIds"] = $calendarIds; 
  unlink($credentialsPath); 
}

function buildDateToEvent($startDate, $endDate){
    $createDateInicio  = new DateTime($startDate);
    $createDateFin     = new DateTime($endDate);
    $createHoraInicio  = new DateTime($startDate);
    $createHoraFin     = new DateTime($endDate);
    $fechaInicio       = $createDateInicio->format('Y-m-d');
    $fechaFin          = $createDateFin->format('Y-m-d');
    $horaInicio        = $createHoraInicio->format('H:i');
    $horaFin           = $createHoraFin->format('H:i');
    $segundos          = ":00-05:00";
    $t = "T";
    $fechaInicioFinal = $fechaInicio.$t.$horaInicio.$segundos;
    $fechaFinFinal    = $fechaFin.$t.$horaFin.$segundos;
    return array("fechaInicioFinal" => $fechaInicioFinal, "fechaFinFinal" => $fechaFinFinal);
}

function editMeetingGmail($primary, $data = array(), $client){
    $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
    $url     =  configure::read("REDIRECT_EDIT_EVENT");
    $emails  = array();
    foreach ($data["users"] as $user) {
      $emails[] = array("email" =>$user["email"]); 
    }  
    $service   = new Google_Service_Calendar($client);
    $event     = $service->events->get('primary', $data["idEvent"]); 
    $eventEdit = new Google_Service_Calendar_Event(array(    
    'summary'      => $data["subject"],
    'transparency' => $data["transparency"],
    'location'     => $data["location"],
    'description'  => $data["description"],
    'start' => array(
      'dateTime' => $data["startDate"],
      'timeZone' => 'America/Los_Angeles',
    ),
    'end' => array(
      'dateTime' => $data["endDate"],
      'timeZone' => 'America/Los_Angeles',
    ),
    'reminders' => array(
      'useDefault' => FALSE,
      'overrides' => array(
        array('method' => 'email', 'minutes' => 0),
        array('method' => 'popup', 'minutes' => 10),
      ),
    )
  ));
  $eventEdit["attendees"] = $emails; 
  $optionaArguments = array("sendNotifications"=>true, "supportsAttachments" => true); 
  $updatedEvent = $service->events->update('primary', $event->getId(), $eventEdit, $optionaArguments);
  unlink($credentialsPath);
  cleanLog();  
}

function cleanLog(){
   $log = __DIR__ . '/app/tmp/logs/error.log'; 
    if (file_exists($log)) {   
        unlink($log); 
    }
}

 