
<?php
require_once __DIR__ . '/vendor/autoload.php'; 
 
define('APPLICATION_NAME', 'Google Calendar API PHP Quickstart');
define('CREDENTIALS_PATH', '~/.credentials/calendar-php-quickstart.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret_add_event.json');
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-php-quickstart.json
define('SCOPES', implode(' ', array(
  Google_Service_Calendar::CALENDAR)
));
 
  
    function getClient($url = null){
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH); 
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setDeveloperKey("AIzaSyDWcymvKKbKOw4Gv1imW3CtIVdQ8kJCEjs");
       // Load previously authorized credentials from a file.
        $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
         if (file_exists($credentialsPath)) {
           $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
          // Request authorization from the user.
          $authUrl  = $client->createAuthUrl(); 
          //header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));          
          if (isset($_GET['code'])) {
              $authCode = $_GET['code'];
              // Exchange authorization code for an access token.
              $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
              //header('Location: ' . filter_var($this->redirectUri,FILTER_SANITIZE_URL));
              if(!file_exists(dirname($credentialsPath))) {
                  mkdir(dirname($credentialsPath), 0700, true);
              }
              file_put_contents($credentialsPath, json_encode($accessToken));
              header("Location: $url"); 
          }else{
              header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));    
          } 
      }
      if(!empty($accessToken)){
        $client->setAccessToken($accessToken);
        if ($client->isAccessTokenExpired()) {
          // save refresh token to some variable
          $refreshTokenSaved = $client->getRefreshToken();
          // update access token
          $client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
          // pass access token to some variable
          $accessTokenUpdated = $client->getAccessToken();
          // append refresh token
          $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;
          //Set the new acces token
          $accessToken = $refreshTokenSaved;
          $client->setAccessToken($accessToken);
          // save to file
          file_put_contents($credentialsPath,json_encode($accessTokenUpdated));
        } 
      } else {
        header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));  
      } 
      return $client;
    } 
 
    function expandHomeDirectory($path) {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
          $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }
  
function createMeetingGmail($primary, $users = array(), $data = array(), $client){
    $subject     = $data["Meeting"]["subject"];
    $location    = isset($data["Meeting"]["location"]) ? $data["Meeting"]["location"] : __("No suministrada");
    $description = $data["Meeting"]["description"];
    $startDate   = $data["Meeting"]["start_date"];
    $endDate     = $data["Meeting"]["end_date"];
    $emails      = array();

    foreach ($users as $user) {
     $emails[] = array("email" =>$user["email"]); 
    }
    $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH); 
    $url             = configure::read("REDIRECT_ADD_EVENT"); 
    $service = new Google_Service_Calendar($client);
    $availableUser = "";
    if($data["Meeting"]["private"] == 1){
        $availableUser = "transparent";
    } else if ($data["Meeting"]["private"] == 2) {
        $availableUser  = "opaque";
    }

    $createDateInicio  = new DateTime($startDate);
    $createDateFin     = new DateTime($endDate);
    $createHoraInicio  = new DateTime($startDate);
    $createHoraFin     = new DateTime($endDate);
    $fechaInicio       = $createDateInicio->format('Y-m-d');
    $fechaFin          = $createDateFin->format('Y-m-d');
    $horaInicio        = $createHoraInicio->format('H:i');
    $horaFin           = $createHoraFin->format('H:i');
    $segundos          = ":00-05:00";
    $t = "T";


    $fechaInicioFinal = $fechaInicio.$t.$horaInicio.$segundos;
    $fechaFinFinal    = $fechaFin.$t.$horaFin.$segundos;
    $event = new Google_Service_Calendar_Event(array(
    
    'summary'      => $subject,
    'transparency' => $availableUser,
    'location'     => $location,
    'description'  => $description,
    'start' => array(
      'dateTime' => $fechaInicioFinal,
      'timeZone' => 'America/Los_Angeles',
    ),
    'end' => array(
      'dateTime' => $fechaFinFinal,
      'timeZone' => 'America/Los_Angeles',
    ),
    'reminders' => array(
      'useDefault' => FALSE,
      'overrides' => array(
        array('method' => 'email', 'minutes' => 0),
        array('method' => 'popup', 'minutes' => 10),
      ),
    )
  ));
  $event["attendees"] = $emails;
  $calendarId = 'primary'; 
  $optionaArguments = array("sendNotifications"=>true);
  $event = $service->events->insert($calendarId, $event, $optionaArguments); 
  $_SESSION['IdCalendar'] = $event["id"];
  unlink($credentialsPath);
  cleanLog(); 
}


function cleanLog(){
   $log = __DIR__ . '/app/tmp/logs/error.log'; 
    if (file_exists($log)) {   
        unlink($log); 
    }
}
