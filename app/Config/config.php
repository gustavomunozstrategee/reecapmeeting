<?php

$project = explode('/', $_SERVER['REQUEST_URI'])[1]; 
 
$config = array(
	'nn'				=> 'nn',
	'ADMIN_ROLE_ID'     => 1, 
	'FUNCIONARIO' 		=> 4,
	'TYPE_USER'         => array('1'=>__('Colaborador'), '2'=>__('Asistente externo')),
	'TYPE_USER_ENG'     => array('1'=>__('Employee'),    '2'=>__('External assistant')),
	'DISABLED'    		=> 0,
	'DISABLED_MANUAL'   => 2,
	'ENABLED'     		=> 1,
	"APPROVAL" 			=> 3,
	"NEW_APPROVAL" 		=> 3,
	"CONTAC_EDITAR"		=> 4,
	'COD_ACTA_INIT'		=> 0,
	'ID_ACTA_INIT'		=> 0,
	'VALOR_MINIMO_PAYU' => 3,
	'Application' => array(
		'name' 	  => 'RecapMeeting',
		'version' => 'v2.10 cake',
		'status'  => 1,
		'maintenance' => 0,
    	'smf' => 1,
		'taskee' => 1,
		'smarchat' => 1,
		'team_id' =>"5",
	),
	'Meta' => array(
		'title' 	  => '',
		'description' => '',
		'keywords' 	  => '',
	),
	
	'Google' => array(
		'analytics'  => '',
	),

	'Firebase' => array(
		'Url'			 => 'https://contact-now-saas.firebaseio.com/',
		'sesion'		 => 'test/sesionNotifications/',
		'contact'		 => 'test/contactNotifications/',
		'notificaciones' => 'test/userNotificaciones/'
	),
	
	'Email' => array(
		'from_email'   => array('info@strategeesuite.com' => 'RecapMeeting'),
		'contact_mail' => array('RecapMeeting' => 'info@strategeesuite.com')
	),
	'WHITE_BRAND'         => array('1'=>__('Sí'),  '2'=>__('No')),
	'WHITE_BRAND_ENG'     => array('1'=>__('Yes'), '2'=>__('No')),
	'TEMPLATE'            => array('1'=>__('Sí'),  '0'=>__('No')),
	'TEMPLATE_ENG'        => array('1'=>__('Yes'), '0'=>__('No')),
	"HAVE_WHITE_BRAND"    => 1,
	"COMMITMENT_DONE"     => 1,
	"COMMITMENT_NOT_DONE" => 0,
	"COMMITMENT_EXPIRED"  => 2,
	"CUPON_USADO"         => 1,
	"CUPON_NO_USADO"      => 0,	  
	'TRANSACTION_APROBADA'  => 'APPROVED', 
	'TRANSACTION_RECHAZADA' => 'ANTIFRAUD_REJECTED', 
	'TRANSACTION_PENDING'   => 'Pendiente',
	'TRANSACTION_CANCEL'    => 'Cancelada', 
	"PLAN_INICIAL" 		   => 1,
	"CAMBIAR_PLAN" 		   => 0,
	'TYPE_MEETING'         => array('Reunión'=>__('Reunión'), 'Reunión de seguimiento'=>__('Reunión de seguimiento')),
	'TYPE_MEETING_ENG'     => array('Reunión'=>__('Meeting'), 'Reunión de seguimiento'=>__('Follow-up meeting')),
	'MODALITY_MEETING'     => array('1'=>__('Reunión presencial'), '2'=>__('Reunión telefónica'), '3'=>__('Reunión virtual')),
	'MODALITY_MEETING_ENG' => array('1'=>__('Face-to-face meeting'), '2'=>__('Telephone meeting'), '3'=>__('Virtual meeting')),
	"MEETING_PRESENTIAL"   => 1,
	"MEETING_TELEPHONE"    => 2,
	"MEETING_VIRTUAL"      => 3,
	"AUTH_OUTLOOK_URL"    					=> FULL_BASE_URL.'/meetings/saveEventsOutlook',
	"AUTH_OUTLOOK_URL_MEETING_FOLLOWING"    => FULL_BASE_URL.'/meetings/saveEventsOutlookFollowingMeetings',
	"AUTH_OUTLOOK_URL_EVENTS"  				=> FULL_BASE_URL.'/meetings/getEventsOutlook',
	"AUTH_OUTLOOK_LOGIN"  				    => FULL_BASE_URL.'/users/OutlookLoginResult',
	"AUTH_OUTLOOK_URL_EDIT_EVENTS"  		=> FULL_BASE_URL.'/meetings/editEventOutlook',
	"REDIRECT_LIST_EVENT" 					=> FULL_BASE_URL.'/meetings/listMeetingGmail',
	"REDIRECT_ADD_EVENT"					=> FULL_BASE_URL.'/meetings/addEventInGmail',
	"REDIRECT_ADD_EVENT_FOLLOWING" 			=> FULL_BASE_URL.'/meetings/addEventInGmailMeetingFollowing',
	"REDIRECT_EDIT_EVENT" 					=> FULL_BASE_URL.'/meetings/finish_process_edit_meeting',
	'SEARCH_FIELDS' => array(
		'Client'    => array('name','description','team_id'),
		'Project'   => array('name','description','team_id'),
		'Contac'    => 'description', 
		'UserTeam'  => array('team_id'), 
		'Commitment'=> 'description',
		'Meeting'   => 'subject'
	),
	'MODELS' => array(
		"Contac" 		 => __("Actas"),
		"ApprovalContac" => __("Aprobaciones de actas"), 
		"Commitment" 	 => __("Compromisos"),  
	),
	'MODELS_ENG' => array(
		"Contac" 		 => __("Minutes"),
		"ApprovalContac" => __("Minutes approvals"), 
		"Commitment" 	 => __("Commitments"),  
	),
	'SlatesRoles' => array(
		//"0" 		 => __("CREADOR"), TOMADO AUTOMATICAMENTE POR EL SISTEMA
		"2" 	 => __("Colaborador"),  
		"1" => __("Editor"), 
		
	),
	"NEW_ASSISTANT"    => 2,
	"DELETE_ASSISTANT" => 3,
	//'URL_PAYU'  => 'https://checkout.payulatam.com/ppp-web-gateway-payu', //Producción
	'URL_PAYU'	   	   => 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/', //Pruebas,
	"COLLABORATOR" 	   => 1,
	"EXTERNAL"     	   => 2, 
	"ADMIN_PLAN"   	   => "yeisonmejia@strategee.us", 
	"LEGAL_COOKIES"    => "https://strategeesuite.com/legal#cookies",
	"LEGAL_DISCLAIMER" => "https://strategeesuite.com/legal#disclaimer",
	"LEGAL_EULA"   	   => "https://strategeesuite.com/legal#EULA",
	"LEGAL_PRIVACITY"  => "https://strategeesuite.com/legal#Privacity",
	"LEGAL_RETURNS"    => "https://strategeesuite.com/legal#Returns",
	"LEGAL_T_Y_C"      => "https://strategeesuite.com/legal#Terms", 
	"DATA_SUITE"       => "https://strategeesuite.com/information",
	"LANG"             => array("esp" => __("Español"), "eng" => __("Inglés")),
	"LANG_ENG"         => array("esp" => __("Spanish"), "eng" => __("English")),
	"CONF_PLAN_TIME"   => 12,
	"FACEBOOK_URL_SUITE"  => "https://www.facebook.com/StrategeeSuite/",
	"TWITER_URL_SUITE" 	  => "https://twitter.com/StrategeeSuite",
	"INSTAGRAM_URL_SUITE" => "https://www.instagram.com/strategeeSuite/",
	"YOUTUBE_URL_SUITE"   => "https://www.youtube.com/channel/UCCLiyHlFn6jTFSTl_B8aAhQ",
	'URL_YOUTUBE_ESP'     => "Y0eYJQZ92ws",
	'URL_YOUTUBE_ENG'     => "puNLz3WmUxU",
	'CLIENT_ID_OUT2'     => "2dcd0bc9-60a3-4135-8e42-2f243b315ca1",
	'CLIENT_SECRET_OUT2'     => "spIFZ+*|gdrsqPQDA47068}",
	'CLIENT_URL_OUT2'     => FULL_BASE_URL."/users/OutlookLoginResult",
"PrioridadTareas" => array(
        "2"=>  "Estándar", 
        "1" => "Alta", 
    ),
    "TipoCompromiso" => array(
        "3"=>  "Flat", 
        "1" => "Cotidiano",
        "2"=>  "SMF", 

    ),
    "ColorPriority" => array(
        "0" => "label label-danger",
        "1" => "label label-danger",
        "2"	=>"label label-warning", 
        "3" => "label label-success",
    ),
    "EstadosCompromiso" => array(
        "0" => "Inactivo",
        "1" => "Pendiente",
        "2" => "Completado ",
        "3" => "Rechazado",
    ),
    "permission_taskee" => array(
        "1" => "General (Puede Crear y Editar Compromisos a nivel General)",
        "2" => "Área (Puede Crear y Ver Compromisos de Area)",
        "3" => "Personal (Puede Crear y Ver sus propios Compromisos)"
    ),
    "QA" => array(
        "0" => "No (Solo si no es necesario hacer uso de la columna QA)",
        "1" => "SI (Solo si dedea hacer uso de la columna de QA)",
    ),



    //APIS APP MOBILE
    'PAGINATION_LIMIT' => 50,
    'APIS_CONTROLLERS' => array('apis'),
	'USER_PAGINATION_LIMIT' => 200,
	'USER_DEFAULT_IMG' => 'default.jpg',
	'IMAGE_ASSET_TYPES' => array('image/jpeg', 'image/png', 'image/jpg', 'image/gif'),
	'FIREBASE_APP' => array(
		'URL'        => 'https://recapmeeting-82c64.firebaseio.com',
		'DATABASE'   => 'app_mobile',
		'USERS_NODE' => 'users',
	),

	//CONFIGURACIÓN DEL RECAPTCHA
	'RE_CAPTCHA_KEY_WEB_SITE' => '6LcOnTcaAAAAAI_7zQpMgVMuq4zwiVYBVdcZs74K', //visit this url to create a key: https://www.google.com/recaptcha/admin/create
	'RE_CAPTCHA_SECRET_KEY'   => '6LcOnTcaAAAAABpubIqMheUkTGeXcWig83ooy2kB',
	'URL_VERIFY_CAPTCHA' 	  => 'https://www.google.com/recaptcha/api/siteverify',
	'URL_SCRIPT_CAPTCHA'      => 'https://www.google.com/recaptcha/api.js',

	'URL_EENOVART'    => 'https://eenovart.strategee.us/',
	'URL_FLEX_CLIENT' => 'http://localhost/flex_point_clients/',
	'CODE_PROJECT'    => 'REC',
	'ADMIN_USERS' => array(
		'1' => 'nataliamunera@strategee.us'
	),
);


?>
