<?php

  class OutlookService {
    private static $outlookApiUrl = "https://outlook.office.com/api/v2.0";

    public static function makeApiCall($access_token, $user_email, $method, $url, $payload = NULL) {
      // Generate the list of headers to always send.
      $headers = array(
        "User-Agent: outlook/1.0",         // Sending a User-Agent header is a best practice.
        "Authorization: Bearer ".$access_token, // Always need our auth token!
        "Accept: application/json",             // Always accept JSON response.
        "client-request-id: ".self::makeGuid(), // Stamp each new request with a new GUID.
        "return-client-request-id: true",       // Tell the server to include our request-id GUID in the response.
        "X-AnchorMailbox: ".$user_email         // Provider user's email to optimize routing of API call
      );
      
      $curl = curl_init($url); 
      switch(strtoupper($method)) {
        case "GET": 
          $headers[] = 'Prefer: outlook.timezone="America/Bogota"'; 
          break;
        case "POST": 
          // Add a Content-Type header (IMPORTANT!)
          $headers[] = "Content-Type: application/json";
          curl_setopt($curl, CURLOPT_POST, true);
          curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
          break;
        case "PATCH": 
          // Add a Content-Type header (IMPORTANT!)
          $headers[] = "Content-Type: application/json";
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
          curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
          break;
        case "DELETE": 
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
          break;
          default: 
      }
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      $response = curl_exec($curl);       
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);       
      if ($httpCode >= 400) { 
        return array('errorNumber' => $httpCode, 'error' => 'Request returned HTTP error '.$httpCode);
      }
      $curl_errno = curl_errno($curl);
      $curl_err   = curl_error($curl);      
      if ($curl_errno) { 
        $msg = $curl_errno.": ".$curl_err; 
        curl_close($curl);
        return array('errorNumber' => $curl_errno, 'error' => $msg);
      } else {
        curl_close($curl); 
        return json_decode($response, true);
      }
    }
    
    // This function generates a random GUID.
    public static function makeGuid(){
        if (function_exists('com_create_guid')) { 
          return strtolower(trim(com_create_guid(), '{}'));
        } else { 
          $charid = strtolower(md5(uniqid(rand(), true)));
          $hyphen = chr(45);
          $uuid = substr($charid, 0, 8).$hyphen
                  .substr($charid, 8, 4).$hyphen
                  .substr($charid, 12, 4).$hyphen
                  .substr($charid, 16, 4).$hyphen
                  .substr($charid, 20, 12);                
          return $uuid;
        }
    }

    public static function getUser($access_token) {
      $getUserParameters = array (
        // Only return the user's display name and email address
        "\$select" => "DisplayName,EmailAddress"
      );
      $getUserUrl = self::$outlookApiUrl."/Me?".http_build_query($getUserParameters);
      return self::makeApiCall($access_token, "", "GET", $getUserUrl);
    }

    public static function getMessages($access_token, $user_email) {
      $getMessagesParameters = array (
        // Only return Subject, ReceivedDateTime, and From fields
        "\$select" => "Subject,ReceivedDateTime,From",
        // Sort by ReceivedDateTime, newest first
        "\$orderby" => "ReceivedDateTime DESC",
        // Return at most 10 results
        "\$top" => "10"
      );      
      $getMessagesUrl = self::$outlookApiUrl."/Me/MailFolders/Inbox/Messages?".http_build_query($getMessagesParameters);
      return self::makeApiCall($access_token, $user_email, "GET", $getMessagesUrl);
    }

    public static function getContacts($access_token, $user_email) {
      $getContactsParameters = array (
        // Only return GivenName, Surname, and EmailAddresses fields
        "\$select" => "GivenName,Surname,EmailAddresses",
        // Sort by GivenName, A-Z
        "\$orderby" => "GivenName",
        // Return at most 10 results
        "\$top" => "100"
      );
      $getContactsUrl = self::$outlookApiUrl."/Me/Contacts?".http_build_query($getContactsParameters);
      return self::makeApiCall($access_token, $user_email, "GET", $getContactsUrl);
    }

    public static function getCalendars($access_token, $user_email) {
      $fechaInicio = date('Y-m-01')."T"."01:00:00";
      $fechaFin    = date('Y-m-t')."T"."23:59:00";
      $getEventsParameters = array ( 
        "startDateTime" => $fechaInicio, 
        "endDateTime"   => $fechaFin, 
        "top"           => "10"
      ); 
      $getContactsUrl = self::$outlookApiUrl."/Me/calendars/Calendar/events?".http_build_query($getEventsParameters); 
      return self::makeApiCall($access_token, $user_email, "GET", $getContactsUrl);
    }

    public static function postCalendars($access_token, $user_email, $datos = array()){
      $urlAddCalendars = self::$outlookApiUrl."/me/events";
      $datos["datos"]['Attendees'] = array_values($datos["usersAttends"]);
      return self::makeApiCall($access_token, $user_email, "POST", $urlAddCalendars,json_encode($datos["datos"]));
    }

    public static function updateCalendars($access_token, $user_email, $datos = array()){
      $urlAddCalendars = self::$outlookApiUrl."/me/events/".$datos["idEvent"];
      $datos["datos"]['Attendees'] = array_values($datos["usersAttends"]); 
      return self::makeApiCall($access_token, $user_email, "PATCH", $urlAddCalendars, json_encode($datos["datos"])); 
    }
  }
