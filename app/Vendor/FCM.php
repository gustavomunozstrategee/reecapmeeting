<?php 
	class FCM{

		public function sendNotification($data, $apiKey) {
			$options = $this->__getOptions($data, $apiKey);
			return $this->__sendRequest($options);
		}

		private function __getOptions($data, $apiKey) {
			$typeNotification = (isset($data['type_notification'])) ? $data['type_notification'] : 'APP_NOTIFICATION';
			$msg = array(
				'title'   => $data['title'],
				'body'    => $data['message'],
				'vibrate' => true,
				'sound'   => 'default',
				'group'   => 'Mr.H',
			);
			$options = array(
				'fields' => array(
					'registration_ids' => $data['tokens'],
					'notification' => $msg,
					'data' => array(
						'show_in_foreground' => "true",
						'type'  => $typeNotification,
						'badge' => 1,
					),
				),
				'headers' => array(
					'Authorization: key='.$apiKey,
					'Content-Type: application/json'
				),
			);
			return $options;
		}
		 
		private function __sendRequest($options) {
			try {
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt($ch,CURLOPT_POST, true );
				curl_setopt($ch,CURLOPT_HTTPHEADER, $options['headers']);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($options['fields']));
				$result = curl_exec($ch );
				curl_close( $ch );
				return $result;
			} catch (Exception $e) {}
		}
	}

 ?>