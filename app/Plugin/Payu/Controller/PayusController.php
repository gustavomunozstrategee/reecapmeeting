<?php 
class PayusController extends PayuAppController{

    public function index() { 
    	if($this->Session->read("val") == null || $this->Session->read("referenceCode")== null){
        	$this->Session->setFlash(__('El carrito de compras se encuentra sin ítems'), 'flash_fail');
        	$this->redirect(array("controller"=>"plans","action"=>"cart","plugin"=>false));
 	 	}
    }

}