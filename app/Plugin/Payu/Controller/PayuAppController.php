<?php 
App::uses('AppController', 'Controller');
App::import('Vendor', 'Payu/lib/PayU');


class PayuAppController extends AppController{
    
	public $_apiKey 				= null;
	public $_apiLogin 				= null;
	public $_merchantId 			= null;
	public $_url 					= null;
	public $_isTest 				= null;
	public $_currency 				= null;
	public $_url_confirmation		= null;
	public $_url_response			= null;
	
	public function beforeRender(){

		$this->initData();
		$this->set("apiKey",$this->_apiKey);
		$this->set("apiLogin",$this->_apiLogin);
		$this->set("merchantId",$this->_merchantId);
		$this->set("url",$this->_url);
		$this->set("accountId",$this->_accountId);
		$this->set("isTest",$this->_isTest);
		$this->set("urlResponse",$this->_url_response);
		$this->set("UrlConfirmation",$this->_url_confirmation);
		$this->set("currency",$this->_currency);

	}

	public function initData(){		

		$data = Configure::read('PayU');
		
		if($data["test_enable"]){
			$this->_apiKey 			 = $data["api_key_test"];
			$this->_apiLogin 		 = $data["api_login_test"];
			$this->_merchantId 		 = $data["merchantId_test"];
			$this->_url				 = $data["url_test"];
			$this->_accountId 		 = $data["accountId_test"];
			$this->_isTest 			 = 1;
		}else{
			$this->_apiKey 			 = $data["api_key_pdn"];
			$this->_apiLogin 		 = $data["api_login_pdn"];
			$this->_merchantId 		 = $data["merchantId_pdn"];
			$this->_url				 = $data["url_pdn"];
			$this->_accountId 		 = $data["accountId_pdn"];
			$this->_isTest 			 = 0;
		}

		$this->_url_confirmation 	 = $data["url_confirmation"];
		$this->_url_response 		 = $data["url_response"];
		$this->_currency 			 = $data["currency"];

	}

	


}