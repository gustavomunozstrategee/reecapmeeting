<?php
  if($this->request->query['typePlan'] == configure::read('PLAN_INICIAL')){
    $description    = __('Adquirir plan');
    $urlProcessData =  Router::url('/', true).'transactions/get_transaction';
  } else if($this->request->query['typePlan'] == configure::read('CAMBIAR_PLAN')) {
    $description    = __('Cambiar plan');
    $urlProcessData =  Router::url('/', true).'transactions/get_transaction';
  } 
?>
 <form method="post" action="<?php echo $url ?>" id="pay-yPayment">
  <input name="merchantId"    type="hidden"  value="<?php echo $merchantId ?>">
  <input name="accountId"     type="hidden"  value="<?php echo $accountId ?>">
  <input name="description"   type="hidden"  value="<?php echo $description?>">
  <input name="referenceCode" type="hidden"  value="<?php echo $this->Session->read("referenceCode") ?>">
  <input name="extra1"        type="hidden"  value="<?php echo $this->Session->read("Config.language") ?>">
  <input name="extra2"        type="hidden"  value="<?php echo $this->request->query['typePlan'] ?>">
  <?php if($this->Session->read("Config.language") == "esp"): ?>
    <input name="lng"           type="hidden"  value="es">
  <?php else :?>
    <input name="lng"           type="hidden"  value="en">
  <?php endif; ?>

  <input name="amount"        type="hidden"  value="<?php echo $this->Session->read("val") ?>">
  <input name="tax"           type="hidden"  value="0">
  <input name="taxReturnBase" type="hidden"  value="0">
  <input name="currency"      type="hidden"  value="<?php echo $currency ?>" >
  <input name="signature"     type="hidden"  value="<?php echo md5($apiKey."~".$merchantId."~".$this->Session->read("referenceCode")."~".$this->Session->read("val")."~".$currency) ?>"  >
  <input name="payerFullName" type="hidden"  value="<?php echo AuthComponent::user("name") ?>">
  <input name="test"          type="hidden"  value="1" >
  <input name="buyerEmail"    type="hidden"  value="<?php echo AuthComponent::user("email") ?>">

  <input name="confirmationUrl" type="hidden"  value="https://zonapagos.strategee.us/apis/addTransaction?clave=REE">
  <input name="extra3"          type="hidden"  value="<?php echo $urlProcessData?>" >
  <input name="responseUrl"     type="hidden"  value="https://reecapmeeteng.strategee.us/transactions/get_transaction"> 
  <input name="Submit"          type="submit"  value="Enviar" style="display:none">
</form>

<div class="overlay">
    <div>
        <p class="texto-carga">
            <?php echo __("Cargando..."); ?>
        </p>
    </div>
</div>

<style>

p.texto-carga{
    width: 35%;
    display: block;
    margin: 0 auto !important;
    background: #fff;
    padding: 20px;
    font-size: 30px;
}    

div.overlay {
    display:        table;
    position:       fixed;
    top:            0;
    left:           0;
    width:          100%;
    height:         100%;    
    z-index: 99999999;
}
div.overlay > div {
    display: table-cell;
    width: 100%;
    height: 100%;
    background: rgba(255, 255, 255, 0.9);
    text-align: center;
    vertical-align: middle;
}
</style>

<script type="text/javascript">
    document.getElementById("pay-yPayment").submit(); 
</script>
