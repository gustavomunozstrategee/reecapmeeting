<?php 

use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;

App::uses('HttpSocket', 'Network/Http');

class FirebaseBehavior extends ModelBehavior {

	private $_collection = null;
	private $_model 	 = null;
	private $_ColumnId 	 = "id";
	private $_push		 = null;

	function __construct() {
	    
	    App::import('Vendor', 'Fcm/vendor/autoload');   

	}

	public function setup(Model $model, $config = array()) {		


	    if(isset($config["collection"])){
	    	$this->_collection = $config["collection"];
	    	$this->_model = $model->name;

	    	if(isset($config["ColumnId"]))
	    	  $this->ColumnId = $config["ColumnId"];

	    	if(isset($config["notify"]))
	    		$this->_push = 1;

	    }else{
	    	return;
	    }

	}

	public function beforeSave(Model $model, $options = array()) {

	}

	public function beforeDelete(Model $model, $cascade = true){

		if(!is_null($this->_collection)){
			$HttpSocket = new HttpSocket();
			$firebaseId = '/'.$model->data[$this->_model]["firebaseid"];
			$results = $HttpSocket->delete(Configure::read('Firebase.Url').$this->_collection.'/'.$model->data[$this->_model][$this->ColumnId].$firebaseId.'.json');
		}


	}

	public function afterSave(Model $model, $created, $options = array()) {

		if(!is_null($this->_collection)){
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false)); 

			$firebaseId = "";
			$method = "put";

			if($created){
				$model->data[$this->_model]["state"] = 0;				
				$method = "post";
			}else{
				$firebaseId = '/'.$model->data[$this->_model]["firebaseid"];
			}
			$model->data[$this->_model]["time"] = time();
			$results = $HttpSocket->$method(Configure::read('Firebase.Url').$this->_collection.'/'.$model->data[$this->_model][$this->ColumnId].$firebaseId.'.json', json_encode($model->data[$this->_model]));

			if($created){
				$idFire = json_decode($results->body);
				$model->data[$this->_model]["firebaseid"] = $idFire->name;
				$model->save($model->data);

				if(!is_null($this->_push)){
					$Movile = ClassRegistry::init('Movile');

					$userTokens = $Movile->find("all",array("conditions"=>array(

						"Movile.user_id" => $model->data[$this->_model][$this->ColumnId]

						)));

					$this->sendPushNotification($userTokens,$info);

				}

			}

		}

	}

	private function sendPushNotification($userTokens = array(), $info = null){

		$apiKey = 'AIzaSyBEj_tubDPGmorL4M-tu5bgsqvbGMmhkPA';
		$client = new Client();
		$client->setApiKey($apiKey);
		$client->injectHttpClient(new \GuzzleHttp\Client());

		$note = new Notification(__("Notification"), $info["type"]);
		$note->setIcon('notification_icon_resource_name')
			    ->setColor('#00365F')
			    ->setBadge(1);

	    if(!empty($userTokens)){

	    	foreach ($userTokens as $key => $value) {
	    		
	    		$message = new Message();
				$message->addRecipient(new Device($userTokens["Movile"]['token']));
				$message->setNotification($note)
				    ->setData(array('someId' => 111, "type" =>$info["type"],"sender"=> $info["sender_name"]));

				$response = $client->send($message);

	    	}

	    }




	}

}