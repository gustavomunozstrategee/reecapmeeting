<?php
/**
 * MeetingFollowing Fixture
 */
class MeetingFollowingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'meeting_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'meeting_room' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'start_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'meeting_id' => 1,
			'meeting_room' => 'Lorem ipsum dolor sit amet',
			'start_date' => '2017-11-14 09:20:27',
			'created' => '2017-11-14 09:20:27',
			'modified' => '2017-11-14 09:20:27'
		),
	);

}
