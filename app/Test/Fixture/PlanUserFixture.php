<?php
/**
 * PlanUserFixture
 *
 */
class PlanUserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'id del usuario que compro el plan'),
		'plan_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'id del plan adquirido'),
		'validity' => array('type' => 'date', 'null' => false, 'default' => null, 'comment' => 'fecha de vigencia'),
		'buy_date' => array('type' => 'date', 'null' => false, 'default' => null, 'comment' => 'fecha de compra'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'plan_id' => 1,
			'validity' => '2017-10-18',
			'buy_date' => '2017-10-18',
			'created' => '2017-10-18 08:41:13',
			'modified' => '2017-10-18 08:41:13'
		),
	);

}
