<?php
/**
 * Coupon Fixture
 */
class CouponFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'code' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'description' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'discount_average' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'start_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'end_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'email' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'addressee_name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'comment' => 'nombre del destinatario', 'charset' => 'utf8'),
		'phone' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 11, 'collate' => 'utf8_bin', 'comment' => 'teléfono', 'charset' => 'utf8'),
		'usage_coupon' => array('type' => 'string', 'null' => false, 'default' => '0', 'collate' => 'utf8_bin', 'comment' => '0 - no se ha usado. 1 - usado', 'charset' => 'utf8'),
		'date_use' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'plan_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'state' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_bin', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'code' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'discount_average' => 1,
			'start_date' => '2017-10-30',
			'end_date' => '2017-10-30',
			'email' => 'Lorem ipsum dolor sit amet',
			'addressee_name' => 'Lorem ipsum dolor sit amet',
			'phone' => 'Lorem ips',
			'usage_coupon' => 'Lorem ipsum dolor sit amet',
			'date_use' => '2017-10-30 16:14:12',
			'plan_id' => 1,
			'state' => 1,
			'created' => '2017-10-30 16:14:12',
			'modified' => '2017-10-30 16:14:12'
		),
	);

}
