<?php
/**
 * DiaryFixture
 *
 */
class DiaryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'meeting_type' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'meeting_room' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'start_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'end_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'private' => array('type' => 'integer', 'null' => false, 'default' => null),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'state' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'meeting_type' => 'Lorem ipsum dolor sit amet',
			'meeting_room' => 'Lorem ipsum dolor sit amet',
			'start_date' => '2017-10-23 15:21:07',
			'end_date' => '2017-10-23 15:21:07',
			'private' => 1,
			'password' => 'Lorem ipsum dolor sit amet',
			'state' => 1,
			'created' => '2017-10-23 15:21:07',
			'modified' => '2017-10-23 15:21:07'
		),
	);

}
