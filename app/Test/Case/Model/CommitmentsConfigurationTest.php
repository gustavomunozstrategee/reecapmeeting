<?php
App::uses('CommitmentsConfiguration', 'Model');

/**
 * CommitmentsConfiguration Test Case
 */
class CommitmentsConfigurationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.commitments_configuration',
		'app.user',
		'app.role',
		'app.menu',
		'app.permission',
		'app.plan',
		'app.plan_user',
		'app.transaction',
		'app.payment',
		'app.plans_permission'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CommitmentsConfiguration = ClassRegistry::init('CommitmentsConfiguration');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CommitmentsConfiguration);

		parent::tearDown();
	}

}
