<?php
App::uses('ApprovalContac', 'Model');

/**
 * ApprovalContac Test Case
 */
class ApprovalContacTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.approval_contac',
		'app.contac',
		'app.user',
		'app.role',
		'app.menu',
		'app.permission',
		'app.plan',
		'app.plan_user',
		'app.transaction',
		'app.payment',
		'app.plans_permission',
		'app.client',
		'app.employee',
		'app.commitment',
		'app.project',
		'app.contacs_file',
		'app.contacs_document',
		'app.assistant',
		'app.meeting',
		'app.tag',
		'app.topic'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ApprovalContac = ClassRegistry::init('ApprovalContac');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ApprovalContac);

		parent::tearDown();
	}

}
