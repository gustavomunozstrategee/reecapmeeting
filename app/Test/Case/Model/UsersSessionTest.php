<?php
App::uses('UsersSession', 'Model');

/**
 * UsersSession Test Case
 */
class UsersSessionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.users_session'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UsersSession = ClassRegistry::init('UsersSession');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UsersSession);

		parent::tearDown();
	}

}
