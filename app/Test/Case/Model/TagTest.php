<?php
App::uses('Tag', 'Model');

/**
 * Tag Test Case
 */
class TagTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tag',
		'app.meeting',
		'app.assistant',
		'app.contac',
		'app.user',
		'app.role',
		'app.menu',
		'app.permission',
		'app.plan',
		'app.plan_user',
		'app.transaction',
		'app.payment',
		'app.plans_permission',
		'app.client',
		'app.employee',
		'app.commitment',
		'app.project',
		'app.contacs_file',
		'app.contacs_document',
		'app.topic'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tag = ClassRegistry::init('Tag');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tag);

		parent::tearDown();
	}

}
