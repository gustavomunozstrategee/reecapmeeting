<?php
App::uses('ContacsDocument', 'Model');

/**
 * ContacsDocument Test Case
 */
class ContacsDocumentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contacs_document',
		'app.contac',
		'app.user',
		'app.project',
		'app.client',
		'app.employee',
		'app.contacs_file'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ContacsDocument = ClassRegistry::init('ContacsDocument');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ContacsDocument);

		parent::tearDown();
	}

/**
 * testBuildConditions method
 *
 * @return void
 */
	public function testBuildConditions() {
		$this->markTestIncomplete('testBuildConditions not implemented.');
	}

}
