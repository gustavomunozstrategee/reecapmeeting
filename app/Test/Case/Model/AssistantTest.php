<?php
App::uses('Assistant', 'Model');

/**
 * Assistant Test Case
 */
class AssistantTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.assistant',
		'app.contac',
		'app.user',
		'app.role',
		'app.menu',
		'app.permission',
		'app.plan',
		'app.plan_user',
		'app.transaction',
		'app.coupon',
		'app.payment',
		'app.plans_permission',
		'app.client',
		'app.employee',
		'app.project',
		'app.contacs_file',
		'app.contacs_document',
		'app.commitment',
		'app.diary'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Assistant = ClassRegistry::init('Assistant');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Assistant);

		parent::tearDown();
	}

/**
 * testBuildConditions method
 *
 * @return void
 */
	public function testBuildConditions() {
		$this->markTestIncomplete('testBuildConditions not implemented.');
	}

}
