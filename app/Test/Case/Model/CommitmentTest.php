<?php
App::uses('Commitment', 'Model');

/**
 * Commitment Test Case
 */
class CommitmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.commitment',
		'app.contac',
		'app.user',
		'app.role',
		'app.menu',
		'app.permission',
		'app.plan',
		'app.plan_user',
		'app.transaction',
		'app.coupon',
		'app.payment',
		'app.plans_permission',
		'app.client',
		'app.employee',
		'app.project',
		'app.contacs_file',
		'app.contacs_document'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Commitment = ClassRegistry::init('Commitment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Commitment);

		parent::tearDown();
	}

/**
 * testBuildConditions method
 *
 * @return void
 */
	public function testBuildConditions() {
		$this->markTestIncomplete('testBuildConditions not implemented.');
	}

}
