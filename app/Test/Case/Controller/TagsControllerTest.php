<?php
App::uses('TagsController', 'Controller');

/**
 * TagsController Test Case
 */
class TagsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tag',
		'app.meeting',
		'app.assistant',
		'app.contac',
		'app.user',
		'app.role',
		'app.menu',
		'app.permission',
		'app.plan',
		'app.plan_user',
		'app.transaction',
		'app.payment',
		'app.plans_permission',
		'app.client',
		'app.employee',
		'app.commitment',
		'app.project',
		'app.contacs_file',
		'app.contacs_document',
		'app.topic'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
