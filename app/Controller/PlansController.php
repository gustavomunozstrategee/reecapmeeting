<?php

App::uses('AppController', 'Controller');
 
class PlansController extends AppController {
 
	public $components = array('Paginator');

	public function beforeFilter() {
	  	$this->Auth->allow('getTransaction','disabled_plan_expired','notification_plan_before_expired');
	 	parent::beforeFilter();
	 	
    }

	public function index() {
		$this->validateRolAdministrador(); 
		$this->Plan->unbindModel(array('hasOne' => array('PlanUser')));
		$this->Plan->unbindModel(array('hasMany' => array('Transaction','Payment'))); 
		$conditions[] = $this->Plan->buildConditions($this->request->query);
		$this->Plan->recursive = 1;
		$this->paginate = array(
	        'conditions' => $conditions,
	        'limit' => 10,
	        'order' => 'Plan.modified DESC'
		);
		$plans = $this->Paginator->paginate(); 
		$this->set(compact('plans'));
	}

	public function view($id = null) {
		$this->validateRolAdministrador();
		$id = EncryptDecrypt::decrypt($id); 
		$this->Plan->recursive = 1;
		$this->Plan->unbindModel(array('hasMany' => array('Transaction')));
		$conditions   = array('Plan.id' => $id);
		$plan         = $this->Plan->find('first', compact('conditions'));
		$fields       = array("PlanUser.user_id");
		$conditions   = array("PlanUser.plan_id" => $id);
		$usersPlanIds = $this->Plan->PlanUser->find("list", compact("conditions","fields"));
		$recursive    = -1;
		$limit        = 10; 
		$conditions   = array("User.id" => $usersPlanIds); 
		$this->Paginator->settings = compact('conditions','limit','recursive');
        $business     = $this->Paginator->paginate($this->Plan->PlanUser->User); 
		$this->set(compact('plan','business'));
	}

	public function add() {
		$this->validateRolAdministrador(); 
		if ($this->request->is('post')) { 
			$this->request->data['Plan']['trial'] = Configure::read('DISABLED');
			$guardar = $this->getValidates($this->request->data,'Plan');
			if (!is_array($guardar)) {
				$this->request->data['Plan']['state'] = Configure::read('ENABLED');
				$this->Plan->create();
				if ($this->Plan->save($this->request->data)) { 	 
					$this->__putPlanRecommended($this->request->data, $this->Plan->id);
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->fail(__('Error al guardar, por favor inténtelo nuevamente.'));
				}
			} else {
				$this->Flash->fail(implode('<br>',$guardar));
			}
		} 
	}

	public function edit($id = null) {
		$this->validateRolAdministrador();
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Plan->exists($id)) {
			$this->showMessageExceptions();
		} 
      	$this->Plan->id = $id;
		if ($this->request->is('post') || $this->request->is('put')) {
			$guardar = $this->getValidates($this->request->data,'Plan');
			if (!is_array($guardar)) {  
				$this->__putPlanRecommended($this->request->data, $id);			
				if ($this->Plan->save($this->request->data)) {
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->fail(__('Error al guardar, por favor inténtelo nuevamente.'));
				}
			} else {
				$this->Flash->fail(implode('<br>',$guardar));
			}
		} else { 
			$this->request->data = $this->__getInfoPlan($id);  
		}
	}  
 
	private function __putPlanRecommended($data, $id){ 
	 	if(isset($data["Plan"]["id"])) {
	 		if (isset($data["Plan"]["recommended"])) { 
        		$this->Plan->updateAll(array('Plan.recommended' => Configure::read('DISABLED')));
				$this->Plan->updateAll(array('Plan.recommended' => Configure::read('ENABLED')), array('Plan.id' => $data["Plan"]["id"])); 
				$this->updateCachePermissions();
			} else {
				$this->Plan->updateAll(array('Plan.recommended' => Configure::read('DISABLED')), array('Plan.id' => $data["Plan"]["id"]));
				$this->updateCachePermissions();
			}
	 	} else {
	 		if (isset($data["Plan"]["recommended"])) { 
        		$this->Plan->updateAll(array('Plan.recommended' => Configure::read('DISABLED')));
        		$this->Plan->updateAll(
        			array('Plan.recommended' => Configure::read('ENABLED')),
        			array('Plan.id' => $id)
				);
				$this->updateCachePermissions();  
			}
	 	} 
	} 

	private function __getInfoPlan($id){
		$conditions  = array('Plan.' . $this->Plan->primaryKey => $id);
		$plan        = $this->Plan->find('first', compact('conditions')); 
		return $plan;
	}

	public function planning_management(){ 
		$this->Plan->unbindModel(array('hasMany' => array('Transaction','Payment'))); 
		$planTrial      = $this->__checkHavePlanTrial();
		$conditions     = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("ENABLED"), "PlanUser.expired" => Configure::read("DISABLED"));
		$planActualInfo = $this->Plan->find("first", compact("conditions"));
		if (empty($planActualInfo)) {
			$this->Plan->unbindModel(array('hasMany' => array('Transaction','Payment')));
			$conditions  = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("DISABLED"), "PlanUser.expired" => Configure::read("ENABLED"));
			$order		 = array('PlanUser.id DESC');
			$planVencidoInfo = $this->Plan->find("first", compact("conditions","order")); 
		} else { 
			$planVencidoInfo = null;
		}
		$this->Plan->unbindModel(array('hasMany' => array('Transaction','Payment'))); 
		$conditions     = array("Plan.state" => Configure::read("ENABLED")); 
		$fields         = array("DISTINCT Plan.id","Plan.*");
		$plans          = $this->Plan->find("all", compact("conditions","fields")); 
		$this->set(compact('plans',"planActualInfo",'planVencidoInfo','planTrial'));
	}  

	//COMPRAR PLAN
	public function createReferenceCodeToBuyPlan(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false; 
	    $planId     = isset($this->request->data["planId"]) ? EncryptDecrypt::decrypt($this->request->data["planId"]) : null; 
	    $conditions = array("Plan.id" => $planId);
	    $valuePlan  = $this->Plan->field("price", $conditions);  
     	$valuePlan  = $valuePlan * 12; 
	    $response   = $this->Plan->validateValueGeneralPayu($valuePlan); 
	    if($response["state"] == true){
			$referenceCode = "ref".uniqid(); 
			$this->Session->write("referenceCode", $referenceCode);
			$this->Session->write("val", $valuePlan); 
		    $this->__createTransactionInProgress($valuePlan, $referenceCode, $planId);
	    }
	    $this->outJSON($response);  
	} 

	//CREACION DE LA TRANSACCION EN PROGRESO
	private function __createTransactionInProgress($valuePlan, $referenceCode, $planId){ 
		$userId  = AuthComponent::user('id');
		$out = array( 
			"value" 		=> $valuePlan,
			"referenceCode" => $referenceCode, 
			"state"         => configure::read('TRANSACTION_PENDING'), 
			"email"         => AuthComponent::user('email'),
			"user_id"       => AuthComponent::user('id'),
			"name"          => AuthComponent::user('firstname') . ' ' . AuthComponent::user('lastname'),
			"notify"        => configure::read("ENABLED"),
			"plan_id"       => $planId
		); 
		$this->Plan->Transaction->save($out);
		Cache::delete("transactions_{$userId}",'TRANSACTION_PENDINGS');
	}

	//AQUI RETORNA PAYU LUEGO DE PAGAR
	public function getTransaction(){
		set_time_limit(300000);
		$this->autoRender = false;
		$conditions       = array('Transaction.referenceCode' => $this->request->query["reference_sale"]); 
		$recursive        = -1;
		$transaction      = $this->Plan->Transaction->find('first', compact('conditions','recursive')); 
		if (!empty($transaction) && $transaction["Transaction"]["value"] == $this->request->query["value"]) { 
			if($this->request->query['extra2'] == configure::read('PLAN_INICIAL')) {
				$this->__getJsonFromTransaction($this->request->query, $transaction);
			} else if ($this->request->query['extra2'] == configure::read('CAMBIAR_PLAN')) { 
				$data = $this->Plan->getJsonTransactionToChangePlan($this->request->query, $transaction);
				$this->__reactivateUsersTeams($data["Transaction"]["user_id"]); 
				$this->__sendNotificationTransaction($data, $this->request->query['extra1']); 
			} else { 
				$this->Flash->fail(__('Tu transacción fue denegada.'));
				return $this->redirect(array('controller' => 'transactions','action' => 'index')); 
		 	} 
		} else { 
			$this->Flash->fail(__('Tu transacción fue denegada.'));
			return $this->redirect(array('controller' => 'transactions','action' => 'index'));		 	 
		} 
	} 	

	private function __getJsonFromTransaction($requestQuery = array(), $transactionInfo = array()){  
		$data['Transaction'] = array( 
			"id"					=> $transactionInfo["Transaction"]["id"],
			"value"                 => $transactionInfo["Transaction"]["value"], 
			"referenceCode"         => $requestQuery['reference_sale'], 
			"payment_method_type"   => $requestQuery['payment_method_type'],
			"payment_method_name"   => $requestQuery['payment_method_name'],
			"payment_request_state" => $requestQuery['payment_request_state'], 
			"payment_method" 		=> $requestQuery['payment_request_state'], 
			"state"                 => $requestQuery['response_message_pol'],
			"user_id"               => $transactionInfo["Transaction"]["user_id"],
			"email"                 => $transactionInfo["Transaction"]["email"],
			"plan_id"               => $transactionInfo["Transaction"]["plan_id"], 
			"created"               => date('Y-m-d H:i:s'), 
			"notify"                => configure::read("DISABLED"),
			"transactionId"         => $requestQuery['transaction_id'] 
		);
		$userId = $data['Transaction']["user_id"]; 
		if($data['Transaction']["state"] == configure::read("TRANSACTION_APROBADA")){
			$this->__saveTransaction($data, $requestQuery["extra1"]);			
		} else if ($data['Transaction']["state"] == configure::read("TRANSACTION_RECHAZADA")) {
			$this->Plan->Transaction->save($data);
			Cache::delete("transactions_{$userId}",'TRANSACTION_PENDINGS');
			$this->__sendNotificationTransaction($data, $requestQuery["extra1"]);
		}
	}

	private function __saveTransaction($data, $lang){
		$userId = $data['Transaction']['user_id']; 
		$this->Plan->Transaction->save($data);
		$transactionId = $this->Transaction->id; 	 
		$this->__activateMemberTeam($userId);
		$this->__savePayment($data, $transactionId);
		Cache::delete("transactions_{$userId}",'TRANSACTION_PENDINGS');
		$this->activateStep($userId);
		Cache::write("planAdquired_{$userId}", 1, 'PLAN_ADQUIRED');
		$this->__sendNotificationTransaction($data, $lang); 
	} 

	private function __savePayment($data, $transactionId){ 
		$saveData = array();
		$saveData = array(
			"value"          => $data['Transaction']['value'],  
			"user_id"        => $data['Transaction']['user_id'],
			"plan_id"        => $data['Transaction']['plan_id'],
			"transaction_id" => $transactionId
		);
		$this->Plan->Payment->save($saveData); 
		$this->Plan->PlanUser->savePlanUser($data['Transaction']['plan_id'], $data['Transaction']['user_id']);
	}  

	private function __sendNotificationTransaction($data = array(), $lang){
		if($lang == "esp"){
			$template = $lang.'/transaction_notification';
			$subject  = __('Notificación de transacción ' . configure::read("Application.name"));
		} else {
			$template = $lang.'/transaction_notification';
			$subject  = __('Transaction notification ' . configure::read("Application.name"));
		}
		$opts = array(
			'to'       => $data["Transaction"]["email"],
			'subject'  => $subject,
			'vars'     => compact('data'),
			'template' => $template
		);
		$this->sendMail($opts);
		$this->updateCachePermissions();
	} 	 

	public function changePlan(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout      = 'ajax';
		$this->autoRender  = false;
	 	$planId            = isset($this->request->data["planId"]) ? EncryptDecrypt::decrypt($this->request->data["planId"]) : null;
	    $conditions        = array("Plan.id" => $planId);
	    $valuePlanToChange = $this->Plan->field("price", $conditions);
	    $valuePlanToChange = $valuePlanToChange * 12; 
	    $planActual        = $this->Plan->getPlanActual();
	    $responseValidate  = $this->Plan->validatePriceToPlanActualWithNewPlan($planActual, $valuePlanToChange); 
	    if($responseValidate["state"] == true){
	    	$priceNew = $this->Plan->getPriceDependingDayTheUse($planActual, $responseValidate); 
	    	$response = $this->Plan->validateValueGeneralPayu($priceNew); 
	    	if ($response["state"] == true) {
    		 	$referenceCode = "ref".uniqid(); 
				$this->Session->write("referenceCode", $referenceCode);
				$this->Session->write("val", $priceNew); 
				$planInfo = array("plan_id_old" => $planActual["Plan"]["id"], "plan_id_new" => $planId, "plan_value_new" => $priceNew);
				$response = $this->__checkStatePlan($planActual, $planId);
				$this->Plan->Transaction->createTransactionInProgressToPlanToChange($planInfo, $referenceCode); 
	    	}  
    		$this->outJSON($response); 
	    }  
	} 

	private function __checkStatePlan($planActual = array(), $planIdNew){
		$this->Plan->unbindModel(array('hasMany' => array('Transaction','Payment')));
		$conditions  = array("Plan.id" => $planIdNew);
		$planNew     = $this->Plan->find("first", compact("conditions"));
		if($planActual["PlanUser"]["expired"] == Configure::read("ENABLED")){
			if($planNew["Plan"]["users"] < $planActual["PlanUser"]["number_users"]){
				$response = array("state" => true, "price_less" => true,  "renovated" => true);
			} else {
				$response = array("state" => true, "price_less" => false, "renovated" => true); 
			}
		} else {
			$response = array("state" => true);
		}
		return $response;
	}

	// private function __buildLogChangePlan($data){
	// 	$this->loadModel("Log");
	// 	$infoUserPlan = $this->__getInfoUserBuyPlan($data['Transaction']['user_id'], $data); 
	// 	$description  = __("El usuario ") .$infoUserPlan["firstname"]. ' ' .$infoUserPlan["lastname"]. __(" ha cambiado su plan por un nuevo plan llamado ") . $infoUserPlan["planName"] . __(" por el valor de $ ") . $data['Transaction']['value'];
	// 	$logId 		  = $this->buildInfoLog($data['Transaction']['plan_id'], $description, NULL, NULL, NULL, NULL);
	// 	$this->Log->updateAll(array('Log.bussines_user' => $infoUserPlan["businessId"], 'Log.user_id' => $data['Transaction']['user_id']),array('Log.id' => $logId)); 
	// }

	// private function __getInfoUserBuyPlan($userId, $data = array()){
	// 	$conditions  = array("User.id" => $userId);
	// 	$businessId  = $this->Transaction->User->field("bussines_user", $conditions);
	// 	$firstname   = $this->Transaction->User->field("firstname", $conditions);
	// 	$lastname    = $this->Transaction->User->field("lastname", $conditions);
	// 	$conditions  = array("Plan.id" => $data['Transaction']['plan_id']);
	// 	$planName    = $this->Plan->field("name", $conditions);
	// 	return array("planName" => $planName, "businessId" => $businessId, "firstname" => $firstname, "lastname" => $lastname);
	// }

	//CRON PARA DESACTIVAR LOS PLANES QUE SE HAYAN VENCIDO
	public function disabled_plan_expired(){
		set_time_limit(300000);
		$this->autoRender = false;
		$recursive  = -1;
		$conditions = array('PlanUser.state' => Configure::read('ENABLED'), 'PlanUser.validity <' => date('Y-m-d'));
		$plans		= $this->Plan->PlanUser->find("all", compact('conditions','recursive'));
		if(!empty($plans)){
			$planIds = Set::classicExtract($plans,'{n}.PlanUser.id');
			$userIds = Set::classicExtract($plans,'{n}.PlanUser.user_id'); 
			$this->Plan->PlanUser->updateAll(
            	array('PlanUser.state' => Configure::read('DISABLED'), 'PlanUser.expired' => Configure::read('ENABLED')),
            	array('PlanUser.id' => $planIds)
    		);
			$this->__desactiveTeam($userIds); 
		}  
	} 

	public function buy_plan_trial($id = null) {
		$this->autoRender = false;
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Plan->exists($id)) {
			$this->showMessageExceptions();
		}
		$conditions    = array("PlanUser.user_id" => AuthComponent::user("id"), 'Plan.id' => $id);
		$havePlanTrial = $this->Plan->PlanUser->find("first", compact("conditions")); 
		if(!empty($havePlanTrial)) {
			$this->Flash->fail(__('Ya no puedes adquirir este plan.'));
			return $this->redirect(array('controller' => 'plans','action' => 'planning_management')); 
		} else {
			$this->__activateMemberTeam(AuthComponent::user("id"));
			$this->Plan->PlanUser->savePlanTrial($id, AuthComponent::user("id"));
			$this->__activateMemberDisabled();
			$this->updateCachePermissions();
			$this->activateStep(AuthComponent::user("id"));
			$this->updateCacheStep();
			$this->Flash->success(__('Tu cuenta ha sido activada exitosamente, para comenzar debes crear una empresa.'));
			$this->checkTeamCreate(); 			
		} 

	} 

	//CRON PARA NOTIFICAR 5 DÍAS ANTES DE QUE SE VENZA EL PLAN
	public function notification_plan_before_expired(){
		set_time_limit(300000);
		$this->autoRender = false;
		$recursive  = 0;  
		$conditions = array('PlanUser.state' => Configure::read('ENABLED'), '(PlanUser.validity - INTERVAL 5 DAY) = CURDATE()');
		$fields     = array('User.email','User.firstname','User.lastname','User.lang','PlanUser.validity');
		$users		= $this->Plan->PlanUser->find("all", compact('conditions','recursive','fields'));
		if(!empty($users)){
			foreach ($users as $user) {
				if($user["User"]["lang"] == NULL || $user["User"]["lang"] == "" || empty($user["User"]["lang"])){
					$user["User"]["lang"] = "esp";
				} 
				$template = $user["User"]["lang"].'/notificacion_before_expired_plan';
				if($user["User"]["lang"] == "esp"){
					$subject = __('Notificación de plan próximo a vencer').' - '.Configure::read('Application.name');
				} else {
					$subject = __('Plan is expiring soon').' - '.Configure::read('Application.name');
				} 
				$opts = array(
					'to'       => $user["User"]["email"],
					'subject'  => $subject,
					'vars'     => array('date' => $user["PlanUser"]["validity"], 'name' =>  $user["User"]["firstname"] . ' ' . $user["User"]["lastname"]),
					'template' => $template,
				);
				$this->sendMail($opts); 
			}
		} 
	}

	private function __activateMemberTeam($userId){
		$conditions = array('PlanUser.user_id' => $userId);
		$countPlan  = $this->Plan->PlanUser->find("count", compact("conditions"));
		if($countPlan == 0){
			$this->loadModel("Team");
			$this->loadModel("UserTeam");
			$recursive  = -1;
			$conditions = array("Team.user_id" => $userId);
			$teams      = $this->Team->find("all", compact("conditions","recursive"));
			if(!empty($teams)){
				foreach ($teams as $team) {
				 	$data[] = array(
				 		"user_id" 	  => $userId,
				 		"own_user_id" => $userId,
				 		"type_user"   => Configure::read("ENABLED"),
				 		"position_id" => 1, //Id del permiso "Control total"
				 		"client_id"   => NULL,
				 		"team_id"     => $team["Team"]["id"]
				 	);
				} 
				$this->UserTeam->saveAll($data, array('validate'=>false)); 
			}
		} 
	} 
	
	private function __desactiveTeam($userIds){
		$this->loadModel("UserTeam");
		$this->UserTeam->updateAll(
        	array('UserTeam.state' 		 => Configure::read('DISABLED'), 'UserTeam.type_user' => Configure::read('COLLABORATOR')),
        	array('UserTeam.own_user_id' => $userIds)
		);
		$this->updateCachePermissions();
	} 

	private function __reactivateUsersTeams($userId){ 
		$this->loadModel("UserTeam");
		$type  = Cache::read("renovations_{$userId}", "RENOVATED_ADQUIRED"); 
		$users = Cache::read("teams_{$userId}", "USERS_TEAMS_RENOVATED");
		if($type == "renovated_users_choose") {
			$this->UserTeam->updateAll(
        		array('UserTeam.state' 		 => Configure::read('ENABLED')),
        		array('UserTeam.own_user_id' => $userId, 'UserTeam.user_id' => $users["User"])
			);
		} else if ($type == "renovate_all") {
			$this->UserTeam->updateAll(
        		array('UserTeam.state' 		 => Configure::read('ENABLED')),
        		array('UserTeam.own_user_id' => $userId)
			);
		} 
	 	Cache::delete("teams_{$userId}",'USERS_TEAMS_RENOVATED');
	 	Cache::delete("renovations_{$userId}",'RENOVATED_ADQUIRED');
	} 

	public function put_process_plan(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$userId           = AuthComponent::user("id");
		$this->layout 	  = "ajax";
		$this->autoRender = false; 
		Cache::write("renovations_{$userId}", $this->request->data["type"], "RENOVATED_ADQUIRED"); 
	}

	private function __checkHavePlanTrial(){
		$conditions  = array("Plan.trial" => Configure::read("ENABLED"));
		$planTrialId = $this->Plan->field("id", $conditions); 
		$conditions  = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.plan_id" => $planTrialId);
		$planTrial   = $this->Plan->PlanUser->find("count", compact("conditions"));
		return $planTrial; 
	}

	private function __activateMemberDisabled(){
		$users = $this->Session->read("UsersFree");
		$this->loadModel("UserTeam");
		if(!empty($users)){
			$this->UserTeam->updateAll(
	    		array('UserTeam.state' 		 => Configure::read('ENABLED')),
	    		array('UserTeam.own_user_id' => AuthComponent::user("id"), 'UserTeam.user_id' => $users)
			); 
			$this->Session->write("UsersFree", array());  
		} 
	}
}