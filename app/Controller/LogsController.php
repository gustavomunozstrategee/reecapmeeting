<?php
App::uses('AppController', 'Controller');

class LogsController extends AppController {

public $components = array('Paginator'); 

        public function index() { 
                $this->validateTeamSession();
                $teams    = $this->loadTeams();
                // $teamIds  = $this->getTeamIds();  
                $teamIds  = EncryptDecrypt::decrypt($this->Session->read("TEAM"));  
		$this->check_team_permission_action(array("index"), array("logs"));
                $conditions   = array("Log.team_id" => $teamIds); 
		$conditions[] = $this->Log->buildConditions($this->request->query);
		$order        = array('Log.created DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
                $logs 	      = $this->Paginator->paginate();
                $this->__loadUserTeams();  
		$this->set(compact('logs','teams')); 
	}

        private function __loadUserTeams(){
                $userId        = AuthComponent::user("id");
                $permissionApp = Cache::read("permissions_{$userId}", 'PERMISSIONS');
                $this->loadModel("UserTeam");
                $fields       = array("User.id","User.firstname","User.lastname","User.email");
                $conditions   = array("UserTeam.team_id" => EncryptDecrypt::decrypt($this->Session->read("TEAM")));
                $userTeams    = $this->UserTeam->find("all", compact("conditions","fields"));
                $users = array();
                foreach ($userTeams as $user) {
	               $users[$user["User"]["id"]] = $user["User"]["firstname"] . ' ' . $user["User"]["lastname"]. ' - ' .$user["User"]["email"];
                } 
                $this->set(compact('users'));  
        }
 
}
