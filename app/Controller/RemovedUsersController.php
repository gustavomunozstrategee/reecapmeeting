<?php
App::uses('AppController', 'Controller');
 
class RemovedUsersController extends AppController {
 
	public $components = array('Paginator');
 
	public function index() {
		$order        = array("Team.name DESC");
		$conditions   = array('RemovedUser.own_user_id' => AuthComponent::user('id')); 
		$conditions[] = $this->RemovedUser->buildConditions($this->request->query); 
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
        $removedUsers 	= $this->Paginator->paginate();   
		$this->set(compact('removedUsers')); 
	}
  
}
