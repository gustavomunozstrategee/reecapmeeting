<?php
App::uses('AppController', 'Controller');
 
class ProjectsController extends AppController {

	public $components = array('Paginator');

	public function index() { 
		$this->validateTeamSession();
		$this->check_team_permission_action(array("index"), array("projects"));
		$teams        = $this->loadTeams();  
		$this->Project->bindContacNotDone();
		// $teamIds 	  = $this->getTeamIds(); 
		$conditions   = array("Client.team_id" => EncryptDecrypt::decrypt($this->Session->read("TEAM")));
		$conditions[] = $this->Project->buildConditions($this->request->query);
		$order        = array('Project.id DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
        $projects = $this->Paginator->paginate();  
		$this->set(compact('projects','teams'));
	}

	public function view($id = null) {
		$id = EncryptDecrypt::decrypt($id); 
		$this->Project->bindContacNumber(); 
		$this->Project->bindContacNumberNotDone(); 
		$conditions = array('Project.id' => $id);
		$project    = $this->Project->find('first', compact('conditions')); 
		$this->check_team_permission_action(array("view"), array("projects"), $project["Team"]["id"]);  
		$this->loadModel("Contac");
		$this->loadModel("ProjectsPrivacity");
		$project_id  			   = $this->ProjectsPrivacity->getPrivacityProjectContacs($id); 
		$conditions                = array('Contac.project_id' => $project_id); 
		$limit                     = 10;
		$order                     = array('Project.modified DESC');
		$this->Paginator->settings = compact('conditions','limit');
        $contacs                   = $this->Paginator->paginate($this->Contac); 
        $users                     = $this->__getUsersPrivacity($id);
		$this->set(compact('project','contacs','users'));
	}

	private function __getUsersPrivacity($id){ 
		$conditions 	= array("ProjectsPrivacity.project_id" => $id);
		$users      	= $this->ProjectsPrivacity->find("all", compact("conditions"));
		return $users; 
	}

	public function add($step = null) { 
		$this->validateTeamSession();
		$stepApp = $this->getCacheStep();
		$this->__checkClientTeamCreate($stepApp);
		$this->check_team_permission_action(array("add"), array("projects"));  
		if ($this->request->is('post')) { 
			$this->request->data['Project']['name']    		 = $this->lowercaseText($this->request->data['Project']['name']);
			$this->request->data['Project']['user_id'] 	     = AuthComponent::user('id');
			$this->request->data['Project']['team_id'] 		 = $this->__getTeamIdClient($this->request->data['Project']['client_id']);
			$guardar = $this->getValidates($this->request->data,'Project');
			if (!is_array($guardar)) {
				$this->check_team_permission_action(array("add"), array("projects"), $this->request->data['Project']['team_id']);
				$this->Project->create();
				if ($this->Project->save($this->request->data)) {
					$this->__savePrivacityUsers($this->request->data, $this->Project->id);
					$description = __("La empresa ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha creado el proyecto ") . $this->request->data["Project"]["name"];
					$this->Flash->success(__('El proyecto se ha guardado correctamente.'));
					if($stepApp == 1){
						$this->redirect(array('action' => 'index'));
					} else {
						$this->redirect(array('action' => 'index'));
					}
				} else {
					$this->Flash->fail(__('Error al guardar, por favor inténtelo nuevamente.'));
				}
			} else {
				$this->Flash->fail(implode('<br>',$guardar));
			}
		}  

		$this->__getClientsTeam($this->request->data);
		$totalProject = 0;
		if($stepApp == configure::read("ENABLED")){
			$totalProject = $this->Project->getTotalProject(); 
		}
		$this->set(compact('step','stepApp','totalProject')); 	
	}

	private function __checkClientTeamCreate($stepApp = null){ 
		if($stepApp == 1){
			$clients = $this->Project->Client->getTotalClient();
			$teams   = $this->Project->Team->getTotalTeam(AuthComponent::user("id"));
			if($teams == 0){
				$this->Flash->fail(__('Para crear un proyecto primero debes crear una empresa y un cliente.'));
				$this->redirect(array('action' => 'add','controller' => 'teams'));
			} else if($clients == 0){
				$this->Flash->fail(__('Para crear un proyecto primero debes crear una empresa y un cliente.'));
				$this->redirect(array('action' => 'add','controller' => 'clients'));
			} 
		}
	}

	private function __savePrivacityUsers($data = array(), $projectId){
		$this->loadModel("ProjectsPrivacity");
		$saveData = array();
		if(!empty($data["Project"]["users"])){
			foreach ($data["Project"]["users"] as $userId) {
				$saveData[] = array(
					"project_id" => $projectId,
					"user_id"    => $userId 
				); 	
			}
		}
		$saveData[] = array(
			"project_id" => $projectId,
			"user_id"    => AuthComponent::user("id")
		);  
		if(!empty($saveData)){
			$this->ProjectsPrivacity->saveAll($saveData);
		} 
	}


	public function edit($id = null) {
		$id  		= EncryptDecrypt::decrypt($id); 
		$beforeEdit = $this->__getInfoToBuildLog($id);		
      	$this->Project->id = $id;
		if ($this->request->is('post') || $this->request->is('put')) {			
			$this->__setDefaultValues($beforeEdit["data"]);
			$guardar = $this->getValidates($this->request->data,'Project');
			if (!is_array($guardar)) {
				if ($this->Project->save($this->request->data)) {
					$this->__checkUsersPrivacity($this->request->data);
					$this->Flash->success(__('El proyecto se ha editado correctamente.'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->fail(__('Error al guardar, por favor inténtelo nuevamente.'));
				}
			} else {
				$this->Flash->fail(implode('<br>',$guardar));
			}
		} else {
			$this->request->data = $beforeEdit["data"]; 
		}
		$this->__getClientsTeam($this->request->data);
		$this->set(compact('beforeEdit'));
	}

	private function __getInfoToBuildLog($id){
		$project     = $this->Project->findById($id); 
		$conditions  = array("Client.id" => $project["Client"]["id"]);
		$client      = $this->Project->Client->find("first", compact("conditions")); 
		$this->check_team_permission_action(array("edit"), array("projects"), $client["Client"]["team_id"]); 
		$beforeEdit  = __("Nombre: " ) .  $project["Project"]["name"] . __(" Descripción: ") . $project["Project"]["description"] . __( " Cliente: " ) . $client["Client"]["name"];
		$this->set(compact("project"));
		return array("data" => $project, "beforeEdit" => $beforeEdit);
	}

	private function __projectLogAfterEdit($id, $data = array(), $beforeEdit){
		$conditions  = array("Client.id" => $data["Project"]["client_id"]);
		$nameClient  = $this->Project->Client->field("name", $conditions);
		$afterEdit   = __("Nombre: " ) .  $data["Project"]["name"] . __(" Descripción: ") . $data["Project"]["description"] . __( " Cliente: " ) . $nameClient; 
		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha editado el  proyecto ") . $data["Project"]["name"];
		//$this->buildInfoLog($id, $description, $beforeEdit["beforeEdit"], $afterEdit, NULL, NULL); 
	}

	private function __setDefaultValues($data){ 
		$this->request->data["Project"]["user_id"]  = $data["Project"]["user_id"]; 
		$this->request->data["Project"]["number"]   = $data["Project"]["number"]; 
		$this->request->data['Project']['name']     = $this->lowercaseText($this->request->data['Project']['name']); 
		$this->request->data['Project']['team_id']  = $this->__getTeamIdClient($this->request->data['Project']['client_id']);
	}

	public function projects_clients($clientId = null){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $clientId = EncryptDecrypt::decrypt($clientId);
        $this->layout  = 'ajax';
		$this->Project->bindContacNotDone();
		$teamIds 	  = $this->getTeamIds();  
		$conditions                = array('Project.client_id' => $clientId, 'Client.team_id' => $teamIds); 
		$limit                     = 10;
		$order                     = array('Project.modified DESC');
		$this->Paginator->settings = compact('conditions','limit','order');
        $projects                  = $this->Paginator->paginate();  
		$this->set(compact('projects','clientId'));
	}

	//CARGAR PROYECTOS DESDE EL ACTA
	public function find_projects(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax';
		$client_id    = $this->request->data['cliente'];
		$proyectos    = $this->Project->findProjectsClientSelect($client_id);  
		$this->set(compact('proyectos'));
	}

	//FORMULARIO DE CREAR PROYECTO EN EL MODAL DESDE EL ACTA
	public function add_project(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax';
		$client_id    = $this->request->data['cliente'];
		$teamId       = $this->__getTeamIdClient($client_id);		
		$users        = $this->Project->Team->UserTeam->getUsersCommitments($teamId, $client_id);
		unset($users[AuthComponent::user("id")]);
		$clients 	  = $this->Project->Client->detaillClientSelect($client_id);
		$this->set(compact('clients','users'));
	}

    //FUNCIÓN PARA CREAR EL PROYECTO DESDE EL ACTA
	public function add_project_save(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
		$this->autoRender = false; 
		$data 		      = array();
		$data["errors"]   = "";
		$data["success"]  = 0;
		$data["id"] 	  = 0;
		$data["errors"]   = $this->__validateData($data["errors"]);		
		if ($data["errors"] == "") {
			$conditions = array("Client.id" => $this->request->data['Project']['client_id']);
			$recursive  = -1;
			$client     = $this->Project->Client->find("first", compact("conditions")); 
			$this->request->data['Project']['team_id'] = $client["Client"]["team_id"];
			$this->request->data['Project']['user_id'] = AuthComponent::user("id"); 
			$this->Project->create();
			if($this->Project->save($this->request->data)){
				$this->loadModel("ProjectsPrivacity");
				$this->ProjectsPrivacity->saveUsers($this->request->data["Project"]["users"], $this->Project->id);
				$description = __("La empresa ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha creado el proyecto desde un acta ") . $this->request->data["Project"]["name"];
				// $this->buildInfoLog($this->Project->id, $description, NULL, NULL, NULL, NULL, $client["Client"]["team_id"]);
				$data["success"] = __('Los datos se han guardado correctamente.');
				$data["id"] = $this->Project->id;
				$model = __('proyecto');
				$this->notificacionesEmpresa($model, $this->request->data['Project']['name']);				
			}
		}
		$this->outJSON($data); 
	}

	private function __validateData($errores){ 
		if (empty(trim($this->request->data['Project']['name']))) {
			$errores.= __("El nombre del proyecto es requerido.")."\n";
		} else {
			$this->request->data['Project']['name'] = $this->lowercaseText($this->request->data['Project']['name']);
			$existente = $this->Project->validExistenceProject($this->request->data['Project']['name'],$this->request->data['Project']['client_id']);
			if ($existente > 0) {
				$errores.= __("El nombre del proyecto ya se encuentra registrado, por favor verifica.")."\n";
			}
		}
		if (!isset($this->request->data['Project']['client_id'])) {
			$errores.= __("El cliente es requerido.")."\n";
		}
		if (empty(trim($this->request->data['Project']['description']))) {
			$errores.= __("La descripción del proyecto es requerido.")."\n";
		} 
		if ($this->request->data['Project']['number'] < 1) {
			$errores.= __("El número en que comenzará el acta no puede ser negativo o igual a cero.")."\n";
		} 
		if (empty($this->request->data['Project']['img']['name'])) {
			$errores.= __("La imagen del proyecto es requerida.")."\n";
		}
		return $errores;
	}

	public function find_last_projects(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout    = 'ajax';
		$this->recursive = -1;
        $project_id = $this->request->data['project_id'];
		$projecto = $this->Project->detaillProject($project_id);
		$this->set(compact('projecto'));
	}

	private function __getClientsTeam($data = array()){
		$this->Project->Client->unbindModel(array('hasMany' => array('Project')));
		//$teamIds 	 = $this->getTeamIds();
		$teamIds 	 = EncryptDecrypt::decrypt($this->Session->read("TEAM"));
		
 		$conditions  = array('Team.id' => $teamIds, 'Client.state' => Configure::read('ENABLED'));
	    $clientsTeam = $this->Project->Client->find('all',compact('conditions'));
	    $clients 	 = array();
	    if(!empty($clientsTeam)){
	    	foreach ($clientsTeam as $client) {
	    		$clients[$client["Client"]["id"]] = $client["Client"]["name"] . ' - ' . $client["Team"]["name"];
	    	}
	    }
	    if($this->request->action == "edit"){
	    	$data["Project"]["users"] = $this->__loadUsersInPrivacity($data["Project"]["id"]);
	    } 

	    $users = $this->Project->Team->UserTeam->getUsersCommitments(!empty($data["Project"]["team_id"]) ? $data["Project"]["team_id"]: NULL, !empty($data["Project"]["client_id"]) ? $data["Project"]["client_id"] : NULL);

	    if(!empty($data["Project"]["user_id"])){
		    if($data["Project"]["user_id"] == AuthComponent::user("id")){
		    	unset($users[$data["Project"]["user_id"]]);
		    } else {
		    	unset($users[$data["Project"]["user_id"]]);
		    	unset($users[AuthComponent::user("id")]);
		    } 
	    }
	    $this->set(compact('clients','users','data')); 
	}

	private function __getTeamIdClient($clientId){
		$conditions = array("Client.id" => $clientId);
		$teamId 	= $this->Project->Client->field("team_id", $conditions);
		return $teamId;
	}

	private function __loadUsersInPrivacity($projectId){
		$this->loadModel("ProjectsPrivacity");
		$fields     = array("ProjectsPrivacity.user_id");
		$conditions = array("ProjectsPrivacity.project_id" => $projectId);
		$users      = $this->ProjectsPrivacity->find("list", compact("conditions","fields"));
		return $users;
	}

	private function __checkUsersPrivacity($data = array()){
		try {
			if($data["Project"]["user_id"] == AuthComponent::user("id")){
				$data["Project"]["users"][] = AuthComponent::user("id");
			} else {
				$data["Project"]["users"][] = $data["Project"]["user_id"];
				$data["Project"]["users"][] = AuthComponent::user("id");
			}
			$this->loadModel("ProjectsPrivacity");
	    	$recursive    = -1;
			$conditions   = array("ProjectsPrivacity.project_id" => $data["Project"]["id"]); 
			$users     	  = $this->ProjectsPrivacity->find("all", compact("conditions","recursive")); 
			$usersIds     = array_combine($data["Project"]["users"], $data["Project"]["users"]);
			$usersExist   = Set::combine($users, '{n}.ProjectsPrivacity.user_id', '{n}.ProjectsPrivacity.user_id');
			$usersNoExistInList = array_diff($usersIds, $usersExist);  
			if(!empty($usersNoExistInList)){
				$this->ProjectsPrivacity->saveUsers($usersNoExistInList, $data["Project"]["id"], "edit");
			} 
			$this->ProjectsPrivacity->checkUserRemoved($users, $data); 
		} catch (Exception $e) {} 
    } 

    public function verify_img(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax';
		$this->autoRender = false; 
		$response 		  = $this->Project->validateMimeTypeImg($this->request->data["Project"]);    	
	 	$this->outJSON($response); 
	}

	public function show_image_project(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout    = 'ajax';
		$this->recursive = -1;
        $project_id 	 = isset($this->request->data['id']) ? $this->request->data['id'] : NULL; 
        $conditions      = array("Project.id" => $project_id);
        $img          	 = $this->Project->field("img", $conditions); 
		$this->set(compact('img'));
	}
}
