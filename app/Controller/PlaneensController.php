<?php
App::uses('AppController', 'Controller');
CakePlugin::load('PhpExcel');

class PlaneensController extends AppController {

        public $components = array('Paginator','PhpExcel.PhpExcel');

        public function removeUser(){
            $this->layout = false;
            $this->autoRender = false;
             $this->loadModel('PlaneensUser');
             $this->PlaneensUser->id = $this->request->data["id"]; 
                if ($this->PlaneensUser->delete()) {
                     return 1;
                } else {
                return 0;
                }
        }

        public function editPermission(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('PlaneensUser');
            $this->request->data["PlaneensUser"]["id"] = $this->request->data["id"];
            $this->request->data["PlaneensUser"]["role_id"] = $this->request->data["role_id"];
            if($this->PlaneensUser->save($this->request->data)){
                return 1;
            }else{
                return 0;
            }

        }

        public function addUsers() { 
            $this->loadModel('PlaneensUser');
            if(!empty($this->request->data["PlaneensUser"]["participantes"])){
            foreach ($this->request->data["PlaneensUser"]["participantes"] as $key => $value) {
                $newSave = array(
                    "role_id" => $this->request->data["PlaneensUser"]["permisos"],
                    "user_id" => $value,
                    "planeen_id" => $this->request->data["PlaneensUser"]["id"],
                );
                $conditions = array(
                        "PlaneensUser.user_id" => $value,
                        "PlaneensUser.planeen_id" => $this->request->data["PlaneensUser"]["id"],
                );
                $result = $this->PlaneensUser->find("first",compact("conditions"));
                if(!empty($result)){
                    $id = $result["PlaneensUser"]["id"];
                    $this->PlaneensUser->delete($id);
                    $this->PlaneensUser->saveAll($newSave);
                     
                }else{
                    $this->PlaneensUser->saveAll($newSave);
                }
            }
            }

            $this->Flash->success(__('Usuario añadido / editado correctamente.'));
            return $this->redirect(array('action' => 'edit_planeen/'.EncryptDecrypt::encrypt($this->request->data["PlaneensUser"]["id"])));
         }


        private function listar_permiso($userId,$PlaneenId){
            $this->loadModel('PlaneensUser');
            $conditions = array(
                'PlaneensUser.user_id' => $userId,
                'PlaneensUser.planeen_id' => $PlaneenId,
            );
            $result = $this->PlaneensUser->find("first",compact("conditions"));
            return $result["PlaneensUser"]["role_id"];
        }


        public function edit_planeen($id) { 
            $id = EncryptDecrypt::decrypt($id);
            $this->loadModel('UserTeam');
            $this->loadModel('PlaneensTask');
            $this->loadModel('PlaneensUser');
            if (!$this->Planeen->exists($id)) {
                throw new NotFoundException(__('Invalid slates task'));
            }

            if ($this->request->is(array('post', 'put'))) {

                $tagsArray = $this->request->data["Planeen"]["tags"];
                    $tags = "";
                    foreach ($tagsArray as $key => $value) {
                        if($key > 0){
                            $tags .= "{}";
                        }
                        $tags .= $value;
                    }
                    $this->request->data["Planeen"]["tags"] = $tags;


                
                $this->request->data["Planeen"]["id"] = $id;
                if ($this->Planeen->save($this->request->data)) {
                    $this->Flash->success(__('Planeen editado correctamente.'));
                     
                    return $this->redirect(array("controller"=>"planeens",'action' => 'edit_planeen/'.EncryptDecrypt::encrypt($id)));
                } else {
                    $this->Flash->error(__('The slates task could not be saved. Please, try again.'));
                }
            } else {
                $options = array('conditions' => array('Planeen.' . $this->Planeen->primaryKey => $id));
                $this->request->data = $this->Planeen->find('first', $options);
                $tagsEncontrados = explode("{}", $this->request->data["Planeen"]["tags"]);
                $this->request->data["Planeen"]["tags"] = $tagsEncontrados;
            }
                $conditions = array(
                        "PlaneensUser.planeen_id" => $id
                );
                $lista = $this->PlaneensUser->find("all",compact("conditions","recursive"));
                $this->set(compact("lista"));
                $collaborators = $this->UserTeam->getCollaborators_NOTME(Configure::read("Application.team_id"),AuthComponent::user("id"));
                $this->set(compact('collaborators','id',"tagsEncontrados"));
            
        } 


        public function add() { 
            $this->loadModel('UserTeam');
            if ($this->request->is('post') || $this->request->is('put')){
                    $this->request->data["Planeen"]["user_id"] = AuthComponent::user("id");
                    $listName = $this->request->data["Planeen"]["name"];
                    $tagsArray = $this->request->data["Planeen"]["tags"];
                    $tags = "";
                    foreach ($tagsArray as $key => $value) {
                        if($key > 0){
                            $tags .= "{}";
                        }
                        $tags .= $value;
                    }
                    $this->request->data["Planeen"]["tags"] = $tags;
                    if($this->Planeen->save($this->request->data)){
                        $this->loadModel('PlaneensUser');
                        $info = array(
                                "user_id" => AuthComponent::user("id"),
                                "planeen_id" => $this->Planeen->id,
                                "role_id" => "0"
                               );
                               $this->PlaneensUser->saveAll($info);
                        foreach ($this->request->data["Planeen"]["participantes"] as $key => $value) {
                               $info = array(
                                "user_id" => $value,
                                "planeen_id" => $this->Planeen->id,
                                "role_id" => $this->request->data["Planeen"]["permisos"]
                               );
                                $this->PlaneensUser->saveAll($info);
                                
                                $this->loadModel('User');
                                $conditions = array(
                                    'User.id' => $value,
                                );
                                $emailNotification = $this->Planeen->User->find("first",compact("conditions"));
                                $this->planeen_notifications($emailNotification["User"]["email"],'Te han añadido a al Planeen "'.$listName.'"');
                        }
                        $this->Flash->success(__('Planeen creado correctamente'));
                        return $this->redirect(array('action' => 'view/'.EncryptDecrypt::encrypt($this->Planeen->id)));
                    }
                }
                $collaborators = $this->UserTeam->getCollaborators_NOTME(Configure::read("Application.team_id"),AuthComponent::user("id"));
                $this->set(compact('collaborators'));
        } 
        private function planeen_notifications($email,$subject){
            $commitments_info = array(
               "texto" => $subject
            );
            $template = 'slate_notifications';
            $subject  = sprintf(__($subject." - Planeen"));
            $options = array(
                'to'       => $email,
                'template' => $template,
                'subject'  => Configure::read('Application.name'). ' - '.$subject,
                'vars'     =>  array('info'=>$commitments_info),
            );
            $this->sendMail($options);  
            return true;
        }

        public function inactive() { 
            $this->Planeen->PlaneensUser->recursive = 3;
            $conditions = array(
                    "PlaneensUser.user_id" => AuthComponent::user("id"),
                    "Planeen.pin" => 0,
            );
            $misTareasTotales = 0;
            $misTareasTerminadas = 0;
            $totalesPlaneen = 0;
            $totalesTerminadas = 0;
            $planeens = $this->Planeen->PlaneensUser->find("all",compact("conditions","recursive"));
            foreach ($planeens as $key => $value) {
                $misTareasTerminadas = 0;
                $misTareasTotales = 0;
                $totalesPlaneen = 0;
                $totalesTerminadas = 0;
                foreach ($value["Planeen"]["PlaneensTask"] as $key2 => $value2) {
                   if($value2["user_id"] == AuthComponent::user("id")){
                    if($value2["state"] == 3){
                        $misTareasTerminadas++;
                    }

                     $misTareasTotales++;
                   }
                   if($value2["state"] == 3){
                        $totalesTerminadas++;
                    }
                    $totalesPlaneen++;
                }
                $planeens[$key]["Actividades"]["Totales"] =  $misTareasTotales;
                $planeens[$key]["Actividades"]["Terminadas"] = $misTareasTerminadas;
                $planeens[$key]["Actividades"]["TotalesPlaneen"] = $totalesPlaneen;
                $planeens[$key]["Actividades"]["totalesTerminadas"] = $totalesTerminadas;
            }
            $this->set(compact("planeens"));
        }
        

        public function index() { 
            $this->Planeen->PlaneensUser->recursive = 3;
            $conditions = array(
                    "PlaneensUser.user_id" => AuthComponent::user("id"),
                    "Planeen.pin" => 1,
            );
            $misTareasTotales = 0;
            $misTareasTerminadas = 0;
            $totalesPlaneen = 0;
            $totalesTerminadas = 0;
            $planeens = $this->Planeen->PlaneensUser->find("all",compact("conditions","recursive"));
            foreach ($planeens as $key => $value) {
                $misTareasTerminadas = 0;
                $misTareasTotales = 0;
                $totalesPlaneen = 0;
                $totalesTerminadas = 0;
                foreach ($value["Planeen"]["PlaneensTask"] as $key2 => $value2) {
                   if($value2["user_id"] == AuthComponent::user("id")){
                    if($value2["state"] == 3){
                        $misTareasTerminadas++;
                    }
                     $misTareasTotales++;
                   }
                   if($value2["state"] == 3){
                        $totalesTerminadas++;
                    }
                    $totalesPlaneen++;
                }
                $planeens[$key]["Actividades"]["Totales"] =  $misTareasTotales;
                $planeens[$key]["Actividades"]["Terminadas"] = $misTareasTerminadas;
                $planeens[$key]["Actividades"]["TotalesPlaneen"] = $totalesPlaneen;
                $planeens[$key]["Actividades"]["totalesTerminadas"] = $totalesTerminadas;
            }
            $this->set(compact("planeens"));
        }

        public function edit_modal($id,$idPlaneen){
            $this->loadModel('PlaneensTask');
            //$id = EncryptDecrypt::decrypt($id);
            $conditions = array(
                    "Planeen.id" => $idPlaneen
            );
            $planeen = $this->Planeen->find("all",compact("conditions","recursive"));
            if ($this->request->is(array('post', 'put'))) {
                 
            } else {
                $options = array('conditions' => array('PlaneensTask.' . $this->PlaneensTask->primaryKey => $id));
                $this->request->data = $this->PlaneensTask->find('first', $options);
            }
            $idList = $id;
            $this->loadModel('Project');
            $this->Project->recursive = 2;
            $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
            $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
            $this->set(compact("projects","Slate","idList"));
             //LISTADO DE USUARIOS
            $this->loadModel('PlaneensUser');
            $this->PlaneensUser->recursive = 0;
            $conditions = array(
                    "PlaneensUser.planeen_id" => $idPlaneen
            );
            $users = $this->PlaneensUser->find("all",compact("conditions","recursive"));
            $listUsers = array();
            foreach ($users as $key => $value) {
                $listUsers  += array(
                            $value["User"]["id"] => $value["User"]["firstname"]." ".$value["User"]["lastname"]
                        );
            }
            $users = $listUsers;
            $this->set(compact("users"));
            $tagsPre = $planeen[0]["Planeen"]["tags"];
            $tagsPre = explode("{}",  $tagsPre);
            $tags = array();
            foreach ($tagsPre as $key => $value) {
                $tags[$value] = $value;
            }
            $this->set(compact("planeen","id","tags","idPlaneen"));
            $this->set(compact('users', 'productos'));
        }

        public function edit($id,$idPlaneen){
            $this->loadModel('PlaneensTask');
            $id = EncryptDecrypt::decrypt($id);
            $conditions = array(
                    "Planeen.id" => $idPlaneen
            );
            $planeen = $this->Planeen->find("all",compact("conditions","recursive"));
            if ($this->request->is(array('post', 'put'))) {
                 
            } else {
                $options = array('conditions' => array('PlaneensTask.' . $this->PlaneensTask->primaryKey => $id));
                $this->request->data = $this->PlaneensTask->find('first', $options);
            }
            $idList = $id;
            $this->loadModel('Project');
            $this->Project->recursive = 2;
            $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
            $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
            $this->set(compact("projects","Slate","idList"));
             //LISTADO DE USUARIOS
            $this->loadModel('PlaneensUser');
            $this->PlaneensUser->recursive = 0;
            $conditions = array(
                    "PlaneensUser.planeen_id" => $idPlaneen
            );
            $users = $this->PlaneensUser->find("all",compact("conditions","recursive"));
            $listUsers = array();
            foreach ($users as $key => $value) {
                $listUsers  += array(
                            $value["User"]["id"] => $value["User"]["firstname"]." ".$value["User"]["lastname"]
                        );
            }
            $users = $listUsers;
            $this->set(compact("users"));
            $tagsPre = $planeen[0]["Planeen"]["tags"];
            $tagsPre = explode("{}",  $tagsPre);
            $tags = array();
            foreach ($tagsPre as $key => $value) {
                $tags[$value] = $value;
            }
            $this->set(compact("planeen","id","tags","idPlaneen"));
            $this->set(compact('users', 'productos'));
        }


        public function view($id = null) { 
            if(is_null($id)) {
                $this->Flash->fail(__('El planeen no existe.')); 
                $this->redirect(array('controller' => 'planeens', 'action' => 'index'));
            }
            $id = EncryptDecrypt::decrypt($id);
            //LISTAR PERMISO DE USUARIO
            $user_role = $this->listar_permiso(AuthComponent::user("id"),$id);
            $this->Planeen->PlaneensTask->recursive = 1;
            if ($this->request->is('post')) {
                $filtro1 = array();
                $filtro2 = array();
                $filtro3 = "";
                if($this->request->data['Report']['filtro'] != ''){
                    $text = $this->request->data['Report']['filtro'];
                    $filtro1 = array('PlaneensTask.description LIKE' => "%$text%");
                }
                if($this->request->data['Report']['participantes'] != ''){
                    $usersFilter = $this->request->data['Report']['participantes'];
                    $filtro2 = array('PlaneensTask.user_id IN' => $usersFilter);
                }
                if(isset($this->request->data['Report']['tags'])) {
                    $tagsFilter = $this->request->data['Report']['tags'];
                    if(!empty($tagsFilter)) {
                        foreach ($tagsFilter as $key => $value) {
                            $filtro3[] = array('PlaneensTask.tags LIKE' => "%$value");
                        }
                    }
                    $this->set(compact('tagsFilter'));
                }
                if($this->request->data['Report']['inicio'] != '' && $this->request->data['Report']['fin'] != ''){
                    $filtro4 = "";
                    $filtro4 = array('PlaneensTask.deadline_date BETWEEN ? and ?' => array(
                        $this->request->data['Report']['inicio'],
                        $this->request->data['Report']['fin'])
                    );
                }
                $conditions[] = array(
                    'AND' => array(
                        $filtro1,
                        $filtro2,
                        $filtro4,
                        'PlaneensTask.planeen_id' => $id,
                    ),
                    // 'OR' => $filtro3,
                );
                !empty($filtro3) ? $conditions['0']['OR'] = $filtro3 : '';
                
            }else{
                $conditions = array('PlaneensTask.planeen_id' => $id);
            }
            $tasks = $this->Planeen->PlaneensTask->find('all',compact('conditions','recursive'));
            $this->Planeen->recursive = 0;
            $conditions = array('Planeen.id' => $id);
            $planeen = $this->Planeen->find('all',compact('conditions','recursive'));
            $this->set(compact('tasks','planeen','id','user_role'));
            //LISTADO DE USUARIOS
            $this->loadModel('PlaneensUser');
            $this->PlaneensUser->recursive = 0;
            $conditions = array('PlaneensUser.planeen_id' => $id);
            $users = $this->PlaneensUser->find('all',compact('conditions','recursive'));
            $listUsers = array();
            foreach ($users as $key => $value) {
                $listUsers += array(
                    $value['User']['id'] => $value['User']['firstname'].' '.$value['User']['lastname']
                );
            }
            $users = $listUsers;
            $tags  = explode("{}", $planeen[0]['Planeen']['tags']);
            $this->set(compact('users','id','tags'));
        }

        public function updateTaskProccess($idPlaneen) { 
            $this->layout = false;
            $this->autoRender = false;
            //$id = EncryptDecrypt::decrypt($id);
            $this->loadModel('PlaneensTask');
            if ($this->request->is('post')) {
                $user_role = $this->listar_permiso(AuthComponent::user("id"),$idPlaneen);
                $conditions = array(
                    "PlaneensTask.id" => $this->request->data["id"]
                );
                $PlaneensTask = $this->PlaneensTask->find("first",compact("conditions"));

                
                if($user_role == 0 || $user_role == 1){
                }else{
                    if($PlaneensTask["PlaneensTask"]["user_id"] == AuthComponent::user("id")){
                    } else {
                        return 3;
                    }
                }
                $this->request->data["PlaneensTask"]["id"] = $this->request->data["id"];
                if($this->request->data["state"] == "pendiente"){

                    $this->request->data["PlaneensTask"]["state"] = 1;
                }elseif ($this->request->data["state"] == "proceso") {
                     if($PlaneensTask["PlaneensTask"]["date_proceso_init"] == ""){
                        $this->request->data["PlaneensTask"]["date_proceso_init"] = date("Y-m-d");
                     }
                    $this->request->data["PlaneensTask"]["state"] = 2;  
                }elseif ($this->request->data["state"] == "qa") {
                    if($PlaneensTask["PlaneensTask"]["date_qa_init"] == ""){
                        $this->request->data["PlaneensTask"]["date_qa_init"] = date("Y-m-d");
                     }
                    $this->request->data["PlaneensTask"]["state"] = 3;  
                }else{
                    if($PlaneensTask["PlaneensTask"]["date_finish_init"] == ""){
                        $this->request->data["PlaneensTask"]["date_finish_init"] = date("Y-m-d");
                     }
                    $this->request->data["PlaneensTask"]["state"] = 4;
                }

                 


                if($this->PlaneensTask->save($this->request->data["PlaneensTask"])){
                    return 1;
                }else{
                    return 0;
                }
            }
        }

        public function admin($id = null){
            if(is_null($id)) {
                $this->Flash->fail(__('El planeen no existe.')); 
                $this->redirect(array('controller' => 'planeens', 'action' => 'index'));
            }
            $id = EncryptDecrypt::decrypt($id);
            $user_role = $this->listar_permiso(AuthComponent::user("id"),$id);
            $conditions = array(
                    "Planeen.id" => $id
            );
            $planeen = $this->Planeen->find("all",compact("conditions","recursive"));
            //LISTADO DE USUARIOS
            //ADDD
            $idList = $id;
            $this->loadModel('Project');
            $this->Project->recursive = 2;
            $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
            $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
            $this->set(compact("projects","Slate","idList"));
             //LISTADO DE USUARIOS
            $this->loadModel('PlaneensUser');
            $this->PlaneensUser->recursive = 0;
            $conditions = array(
                    "PlaneensUser.planeen_id" => $id
            );
            $users = $this->PlaneensUser->find("all",compact("conditions","recursive"));
            $listUsers = array();
            foreach ($users as $key => $value) {
                $listUsers  += array(
                            $value["User"]["id"] => $value["User"]["firstname"]." ".$value["User"]["lastname"]
                        );
            }
            $users = $listUsers;
            $this->set(compact("users"));
            $tagsPre = $planeen[0]["Planeen"]["tags"];
            $tagsPre = explode("{}",  $tagsPre);
            $tags = array();
            foreach ($tagsPre as $key => $value) {
                $tags[$value] = $value;
            }
            $this->set(compact("planeen","id","tags","user_role"));
        }

         public function ListarActividades(){
            $id =  $this->request->data['id'];
            $tasks = $this->Planeen->PlaneensTask->find("all",compact("conditions","recursive"));
            $this->set(compact("tasks"));
            //LISTADO DE ACTIVIDADES
            $this->Planeen->PlaneensTask->recursive = 1;
            if($this->Session->read('actividad_filtro') != "" ){
                $conditions = array(
                    "SlatesTask.slate_id" => $id,
                    "SlatesTask.user_id" => $this->Session->read('tasks_filtro')
                );
            }else{
                $conditions = array(
                        "Planeen.id" => $id,
                );
            }
            $order = array('PlaneensTask.deadline_date ASC');
            $actividades = $this->Planeen->PlaneensTask->find("all",compact("conditions","recursive","order"));
            $this->set(compact("actividades"));
        }


        public function add_actividad($id){
            $id = EncryptDecrypt::decrypt($id);
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('PlaneensTask');
            //RECEPCION DEL FORMULARIO
            if ($this->request->is('post')) {
                $this->request->data["PlaneensTask"]["planeen_id"] = $id;
                $conditions = array(
                    "Planeen.id" => $id
                );
                $Planeen = $this->Planeen->find("first",compact("conditions"));
                if($this->PlaneensTask->save($this->request->data)){
                    $conditions = array(
                        'User.id' => $this->request->data["PlaneensTask"]["user_id"],
                    );
                    $emailNotification = $this->Planeen->User->find("first",compact("conditions"));
                    $infoNotification = array(
                                "email" => $emailNotification["User"]["email"],
                                "date" => $this->request->data["PlaneensTask"]["deadline_date"],
                                "compromiso" => $this->request->data["PlaneensTask"]["description"],
                                "lista" => $Planeen["Planeen"]["name"]
                            );
                    $this->actividad_notifications($infoNotification,"Nueva Actividad");
                    return "1";
                }else{
                    return "0";
                }
            }else{
                return "0";
            }
        }

        public function edit_actividad(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('PlaneensTask');
            //RECEPCION DEL FORMULARIO
            if ($this->request->is(array('post', 'put'))) {
                if($this->PlaneensTask->save($this->request->data)){
                    $conditions = array(
                        "Planeen.id" => $this->request->data["PlaneensTask"]["idPlaneen"]
                    );
                    $Planeen = $this->Planeen->find("first",compact("conditions"));
                    $conditions = array(
                        'User.id' => $this->request->data["PlaneensTask"]["user_id"],
                    );
                    $emailNotification = $this->Planeen->User->find("first",compact("conditions"));
                    $infoNotification = array(
                                "email" => $emailNotification["User"]["email"],
                                "date" => $this->request->data["PlaneensTask"]["deadline_date"],
                                "compromiso" => $this->request->data["PlaneensTask"]["description"],
                                "lista" => $Planeen["Planeen"]["name"]
                            );
                    $this->actividad_notifications($infoNotification,"Actividad Actualizada");
                    return "1";
                }else{
                    return "0";
                }
            }else{
                return "0";
            }
        }

        public function edit_actividad_modal(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('PlaneensTask');
            //RECEPCION DEL FORMULARIO
            if ($this->request->is(array('post', 'put'))) {
                if($this->PlaneensTask->save($this->request->data)){
                    $conditions = array(
                        "Planeen.id" => $this->request->data["PlaneensTask"]["idPlaneen"]
                    );

                    $Planeen = $this->Planeen->find("first",compact("conditions"));
                    $conditions = array(
                        'User.id' => $this->request->data["PlaneensTask"]["user_id"],
                    );

                    $emailNotification = $this->Planeen->User->find("first",compact("conditions"));
                    $infoNotification = array(
                                "email" => $emailNotification["User"]["email"],
                                "date" => $this->request->data["PlaneensTask"]["deadline_date"],
                                "compromiso" => $this->request->data["PlaneensTask"]["description"],
                                "lista" => $Planeen["Planeen"]["name"]
                            );
                    $this->actividad_notifications($infoNotification,"Actividad Actualizada");
                    //ACTUALIZACION AJAX ACTIVIDAD
                    $this->PlaneensTask->recursive = 1;
                    $conditions = array(
                        'PlaneensTask.id' => $this->request->data["PlaneensTask"]["id"],
                    );
                    $Task = $this->PlaneensTask->find("first",compact("conditions"));
                   
                    
                    $this->set(compact("Task"));
                    $this->render("refresh_task");
                   // return "1";
                }else{
                    return "0";
                }
            }else{
                return "0";
            }
        }

        public function eliminar_actividad(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('PlaneensTask');
            $this->PlaneensTask->id = EncryptDecrypt::decrypt($this->request->data["id"]); 
            if ($this->PlaneensTask->delete()) {
                return 1;
            } else {
                return 0;
            }

        }


        private function actividad_notifications($commitments_info,$subject){
        $template = 'actividad_notification';
        $emails = array("johandiaz@strategee.us");
                $subject  = sprintf(__($subject." - Planeen"));
                $options = array(
                    'to'       => $commitments_info["email"],
                    'template' => $template,
                    'subject'  => Configure::read('Application.name'). ' - '.$subject,
                    'vars'     =>  array('info'=>$commitments_info),
                );
                $this->sendMail($options);  
                return true;
        }

         



    public function export_excel($id){
        $id = EncryptDecrypt::decrypt($id);
        $user_role = $this->listar_permiso(AuthComponent::user("id"),$id);
        if($user_role == 0 || $user_role == 1){
            $conditions = array(
                'PlaneensTask.planeen_id' =>  $id,
            );
        }else{
            $conditions = array(
                'PlaneensTask.planeen_id' =>  $id,
                'PlaneensTask.user_id' => AuthComponent::user("id")
            );
        }
        $this->layout = false;
        $this->autoRender = false;
        $this->loadModel('PlaneensTask');
            Configure::write('debug', 0);
            $this->autoRender = false;
            $this->PhpExcel->createWorksheet();
            $this->PhpExcel->setDefaultFont('Calibri', 16);
            $header = array(
                array('label' => __('Responsable'), 'width' => '30'),
                array('label' => __('Proyecto'), 'width' => '25'),
                array('label' => __('Actividad'), 'width' => '100'),
                array('label' => __('Módulo / Bloque de trabajo'), 'width' => '25'),
                array('label' => __('Fecha de Cumplimiento'), 'width' => '20'),
                array('label' => __('Fecha de Terminación'), 'width' => '20'),
                array('label' => __('Estado'), 'width' => '20'),
                array('label' => __('~Fecha en Proceso'), 'width' => '20'),
                array('label' => __('~Fecha en QA'), 'width' => '20'),
                array('label' => __('~Fecha en Terminado'), 'width' => '20'),
            );
            $this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
            $this->PlaneensTask->recursive = 1;
            $actividades = $this->PlaneensTask->find("all",compact("conditions","recursive"));
            foreach ($actividades as $key => $value){
                $estado = "";
                $fechaTerminado = "-";
                if($value["PlaneensTask"]["state"] == 1){
                    $estado = "Pendiente";
                }elseif($value["PlaneensTask"]["state"] == 2){
                    $estado = "En Proceso";
                }elseif($value["PlaneensTask"]["state"] == 3){
                    $estado = "En QA";
                }else{
                    $estado = "Terminada";
                    $fechaTerminado = $value["PlaneensTask"]["modified"];
                }
                $this->PhpExcel->addTableRow(
                    array(
                        (h( $value["User"]["firstname"]) . " ". h($value["User"]["lastname"])  ),
                        (h( $value["Project"]["name"])),
                        h($value["PlaneensTask"]["description"]),
                        h($value["PlaneensTask"]["tags"]),
                        h($value["PlaneensTask"]["deadline_date"]),
                        h($fechaTerminado),
                        h($estado),
                        h($value["PlaneensTask"]["date_proceso_init"]),
                        h($value["PlaneensTask"]["date_qa_init"]),
                        h($value["PlaneensTask"]["date_finish_init"]),
                    )
                );
            }
            $this->PhpExcel->output('reporte.xlsx');        
    }

    public function reporte(){
        if ($this->request->is('post')) {
           $this->loadModel('PlaneensTask');
            Configure::write('debug', 0);
            $this->autoRender = false;
            $this->PhpExcel->createWorksheet();
            $this->PhpExcel->setDefaultFont('Calibri', 16);
            $header = array(
                array('label' => __('Planeen'), 'width' => '30'),
                array('label' => __('Responsable'), 'width' => '30'),
                array('label' => __('Proyecto'), 'width' => '25'),
                array('label' => __('Actividad'), 'width' => '100'),
                array('label' => __('Módulo / Bloque de trabajo'), 'width' => '25'),
                array('label' => __('Fecha de Cumplimiento'), 'width' => '20'),
                array('label' => __('Fecha de Terminación'), 'width' => '20'),
                array('label' => __('Estado'), 'width' => '20'),
                array('label' => __('~Fecha en Proceso'), 'width' => '20'),
                array('label' => __('~Fecha en QA'), 'width' => '20'),
                array('label' => __('~Fecha en Terminado'), 'width' => '20'),

            );
            $this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
            $this->PlaneensTask->recursive = 1;
            $filter1 = array();
            if($this->request->data["Report"]["participantes"] != ""){
                $filter1 = array('PlaneensTask.user_id IN' =>  $this->request->data["Report"]["participantes"]);
            }else{
                if(!in_array(AuthComponent::user('email'), Configure::read('ADMIN_USERS'))){
                    $filter1 = array('PlaneensTask.user_id' =>  AuthComponent::user("id"));
                } 
            }
            $filter2 = array();
            if($this->request->data["Report"]["planeens"] != ""){
                $filter2 = array('PlaneensTask.planeen_id IN' =>  $this->request->data["Report"]["planeens"]);
            }
            $conditions = array(
                     $filter1,
                     $filter2,
                    'PlaneensTask.modified >=' =>  $this->request->data["Report"]["inicio"],
                    'PlaneensTask.modified <=' => $this->request->data["Report"]["fin"],
            ); 
            $actividades = $this->PlaneensTask->find("all",compact("conditions","recursive"));
            foreach ($actividades as $key => $value){
                $estado = "";
                $fechaTerminado = "-";
                if($value["PlaneensTask"]["state"] == 1){
                    $estado = "Pendiente";
                }elseif($value["PlaneensTask"]["state"] == 2){
                    $estado = "En Proceso";
                }elseif($value["PlaneensTask"]["state"] == 3){
                    $estado = "En QA";
                }else{
                    $estado = "Terminada";
                    $fechaTerminado = $value["PlaneensTask"]["modified"];
                }
                $this->PhpExcel->addTableRow(
                    array(
                        (h( $value["Planeen"]["name"])),
                        (h( $value["User"]["firstname"]) . " ". h($value["User"]["lastname"])  ),
                        (h( $value["Project"]["name"])),
                        h($value["PlaneensTask"]["description"]),
                        h($value["PlaneensTask"]["tags"]),
                        h($value["PlaneensTask"]["deadline_date"]),
                        h($fechaTerminado),
                        h($estado),
                        h($value["PlaneensTask"]["date_proceso_init"]),
                        h($value["PlaneensTask"]["date_qa_init"]),
                        h($value["PlaneensTask"]["date_finish_init"]),
                    )
                );
            }
            $this->PhpExcel->output('reporte.xlsx');        
        }
        $this->loadModel('UserTeam');
        $this->loadModel('Planeens');
        $collaborators = $this->UserTeam->getCollaborators(Configure::read("Application.team_id"),AuthComponent::user("id"));
        $fields        = array("Planeen.id","Planeen.name");
        if(!in_array(AuthComponent::user('email'), Configure::read('ADMIN_USERS'))){
            $conditions = array(
                'Planeen.user_id' => AuthComponent::user('id'),
            );
            $planeens = $this->Planeen->find('list',compact('conditions','fields'));
        } else {
            $conditions = array(
                'PlaneensUser.user_id' => AuthComponent::user('id'),
            );
            $planeens = $this->Planeen->PlaneensUser->find("all",compact("conditions","fields"));
            $planeens = Hash::combine($planeens,  '{n}.Planeen.id','{n}.Planeen.name'); 
        }
        $this->set(compact('planeens'));
        $this->set(compact('collaborators'));
    }

    public function activar_planeen(){
        $this->layout = false;
        $this->autoRender = false;
        $this->loadModel('PlaneensTask');
        $this->request->data["Planeen"]["id"] =  EncryptDecrypt::decrypt($this->request->data["id"]);
        $this->request->data["Planeen"]["pin"] = "1";
        if($this->Planeen->save($this->request->data)){
            return 1;
        }else{
            return 0;
        }
    }

    public function getActualizacionPendiente(){
        $this->autoRender = false;
        echo json_encode(true);
        die();
    }

    public function inactivar_planeen(){
        $this->layout = false;
        $this->autoRender = false;
        $this->loadModel('PlaneensTask');
        $this->request->data["Planeen"]["id"] =  EncryptDecrypt::decrypt($this->request->data["id"]);
        $this->request->data["Planeen"]["pin"] = "0";
        if($this->Planeen->save($this->request->data)){
            return 1;
        }else{
            return 0;
        }
    }

   
         



       
      



}
