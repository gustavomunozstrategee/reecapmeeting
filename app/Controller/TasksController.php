<?php
App::uses('AppController', 'Controller');
CakePlugin::load('PhpExcel');

class TasksController extends AppController {
	public $components = array('Paginator','PhpExcel.PhpExcel');
    public $helpers   = array('Paginator','Utilities');

	public function beforeRender() {
		if(Configure::read("Application.taskee") != "1"){
			$this->Flash->fail(__('Modulo Inactivo'));
			$this->redirect(array('action' => 'index_page','controller' => 'pages'));
			return true;
		}
		return true;
	}
	public function beforeFilter() {
		$this->Auth->allow('notificationCompromisoActualDayFinish',"notificationCompromisoDiaSiguiente","notificationCompromisoVencido");
		parent::beforeFilter();  
 	}
 	public function notificationCompromisoActualDayFinish(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('Commitment');
		$conditions = array(
				'Commitment.delivery_date  ='=>  date("Y-m-d"),
                "Commitment.state" => "1",
		);
		$commiments = $this->Commitment->find("all",compact("conditions","recursive"));
		foreach ($commiments as $key => $value) {
			$email = explode('@', $value['User']['email']); 
			if(isset($email['1'])) {
				if($email['1'] == 'strategee.us') {
				 	$infoNotification = array(
						"email"      => $value["User"]["email"],
						"date"       => $value["Commitment"]["delivery_date"],
						"compromiso" =>  $value["Commitment"]["description"],
						"categoria"  => Configure::read("TipoCompromiso.". $value["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.". $value["Commitment"]["priority"])
					);
				 	$this->commitments_notifications($infoNotification,"Compromiso que debes cumplir hoy");
				}
			}
		}
	}
	public function notificationCompromisoDiaSiguiente(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('Commitment');
		$conditions = array(
				'Commitment.delivery_date  ='=>  date("Y-m-d",strtotime(date("Y-m-d")."+ 1 days")),
                "Commitment.state" => "1",
		);
		$commiments = $this->Commitment->find("all",compact("conditions","recursive"));
		foreach ($commiments as $key => $value) {
			$email = explode('@', $value['User']['email']); 
			if(isset($email['1'])) {
				if($email['1'] == 'strategee.us') {
				 	$infoNotification = array(
						"email"      => $value["User"]["email"],
						"date"       => $value["Commitment"]["delivery_date"],
						"compromiso" =>  $value["Commitment"]["description"],
						"categoria"  => Configure::read("TipoCompromiso.". $value["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.". $value["Commitment"]["priority"])
					);
				 	$this->commitments_notifications($infoNotification,"Compromiso que debes cumplir mañana");
				}
			}
		}
	}

	public function notificationCompromisoVencido(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('Commitment');
		$group      = array('Commitment.id');
		$conditions = array(
			'Commitment.delivery_date  <' => date("Y-m-d"),
            "Commitment.state" 		      => "1",
            'UserTeam.user_state' 		  => Configure::read('DISABLED')
 		);
		$this->Commitment->unbindModel(array('belongsTo' => array('Contac','Client','Project')));
		$this->Commitment->bindUserTeam();
		$commiments = $this->Commitment->find("all",compact("conditions","recursive",'group')); 
		if(!empty($commiments)) {
			foreach ($commiments as $key => $value) {
				$email = explode('@', $value['User']['email']); 
				if(isset($email['1'])) {
					if($email['1'] == 'strategee.us') {
						$infoNotification = array(
							'email' 	 => $value['User']['email'],
							'date' 	     => $value['Commitment']['delivery_date'],
							'compromiso' => $value['Commitment']['description'],
							'categoria'  => Configure::read("TipoCompromiso.". $value["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.". $value["Commitment"]["priority"])
						);
					 	$this->commitments_notifications($infoNotification,"Compromiso vencido");
					}
				}
			}
		}
	}

	public function edit_commitment($id = ""){
		$this->list_permission();
		$this->loadModel('Commitment');
		//RECEPCION DEL FORMULARIO
		if ($this->request->is('post') || $this->request->is('put')){
			$this->request->data["Commitment"]["id"] = $id; 
			if($this->Commitment->save($this->request->data)){
				$this->Commitment->save($this->request->data);


				$this->loadModel('User');
				$conditions = array(
					'User.id' => $this->request->data["Commitment"]["user_id"],
				);
				$emailNotification = $this->User->find("first",compact("conditions"));
				$infoNotification = array(
					"email"      => $emailNotification["User"]["email"],
					"date"       => $this->request->data["Commitment"]["delivery_date"],
					"compromiso" => $this->request->data["Commitment"]["description"],
					"categoria"  => Configure::read("TipoCompromiso.".$this->request->data["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.".$this->request->data["Commitment"]["priority"])
				);
				if($this->commitments_notifications($infoNotification,"Compromiso Editado")){
					$this->Flash->success(__('Compromiso editado correctamente'));
					return $this->redirect(array('action' => 'report_admin_pending'));
				}
			}
			 
		}else{
			$options = array('conditions' => array('Commitment.' . $this->Commitment->primaryKey => $id));
            $this->request->data = $this->Commitment->find('first', $options);
		}
		$this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
 		$this->set(compact("projects"));
 		//CONSULTO EL ROLE Y PERMISO DE LA PERSONA EN EL EQUIPO
 		//echo $team_id;
 		$this->loadModel('UserTeam');
 		$conditions = array(
			'UserTeam.team_id' => Configure::read("Application.team_id"),
			"UserTeam.user_id" => AuthComponent::User("id")
		);
		$userRolePermission = $this->UserTeam->find("first",compact("conditions"));
		$userRolePermission = $userRolePermission["UserTeam"];
 		$this->loadModel('Position');
 		$conditions = array(
			'Position.id' => $userRolePermission["position_id"],
			 
		);
		$roleInfo = $this->Position->find("first",compact("conditions"));
		$userRolePermission["Position"] = $roleInfo["Position"];
		//SE VALIDAN LOS PERMISOS DE ROLE
		$users = $this->list_user_add_commitment($roleInfo["Position"]["permission_taskee"],Configure::read("Application.team_id"),$userRolePermission["department_id"]);
 		$this->set(compact("users"));
	}



	public function index() {
		return $this->redirect(array('action' => 'mycommitments'));
		$this->loadModel('UserTeam');
		$this->UserTeam->recursive = 0;
		$conditions = array(
			'UserTeam.user_id' => AuthComponent::user("id"),
			"UserTeam.team_id" => Configure::read("Application.team_id")
		);
		$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
		$this->set(compact("userPosition"));
		//echo "<pre>";
		//print_r($userPosition);
		//exit();
	}

	public function admin() {
		$this->loadModel('CommitmentsLog');
		$limit = 20;
		$this->CommitmentsLog->recursive = 0;
		$conditions = array(
			'CommitmentsLog.state' => "0"
		);
		$commitments = $this->CommitmentsLog->find("all",compact("conditions"));
		$this->loadModel('Commitment');
		$arrayCompromisos = array();
		foreach ($commitments as $key => $value) {
			$conditions = array(
				'Commitment.id' => $value["CommitmentsLog"]["commitment_id"]
			);
			$infoCommitment = $this->Commitment->find("first",compact("conditions"));
			$arrayCompromisos[$key] = $infoCommitment;
			$arrayCompromisos[$key]["Log"] = $value;
		}
		$commitments = $arrayCompromisos;
		$this->set(compact("commitments"));
		$this->loadModel('UserTeam');
		$this->UserTeam->recursive = 0;
		$conditions = array(
			'UserTeam.user_id'    => AuthComponent::user("id"),
			"UserTeam.team_id"    => Configure::read("Application.team_id"),
			'UserTeam.user_state' => Configure::read('DISABLED')
		);
		$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
		$this->set(compact("userPosition"));
		 
	}

	public function mycommitments(){
		$this->loadModel('Commitment');
		$this->loadModel('UserTeam');
		$order = array('Commitment.delivery_date ASC');
		$limit = 20;  
		if(isset($_GET["order"])){
			if($_GET["order"] == "first"){
				$order = array('Commitment.delivery_date ASC');
			}elseif($_GET["order"] =="last"){
				$order = array('Commitment.delivery_date DESC');
			}elseif($_GET["order"] =="priority"){
				$order = array('Commitment.priority ASC');
			}
		}
		$conditions = array(
			'Commitment.user_id' => AuthComponent::user("id"),
			"Commitment.state" => "1"
		);
		 
		$this->Commitment->recursive = 0;
		$this->Paginator->settings = compact('conditions','limit','order',"recursive");
		$commitments = $this->Paginator->paginate($this->Commitment);  
		

 
		
		//SE BUSCA EL ROLE EN EL TEAM STRATEGEE
		$this->loadModel('UserTeam');
		$this->UserTeam->recursive = 0;
		$conditions = array(
			'UserTeam.user_id' => AuthComponent::user("id"),
			"UserTeam.team_id" => Configure::read("Application.team_id")
		);
		$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
		$this->set(compact("commitments","userPosition"));
	}

	public function mycommitmentsadmin(){
		$this->loadModel('Commitment');
		$this->loadModel('UserTeam');
		$order = array('Commitment.delivery_date ASC');
		$limit = 50;  
		if(isset($_GET["order"])){
			if($_GET["order"] == "first"){
				$order = array('Commitment.delivery_date ASC');
			}elseif($_GET["order"] =="last"){
				$order = array('Commitment.delivery_date DESC');
			}elseif($_GET["order"] =="priority"){
				$order = array('Commitment.priority ASC');
			}
		}
		$conditions = array(
			'Commitment.user_id' => AuthComponent::user("id"),
			"Commitment.state" => "1"
		);
		 
		$this->Commitment->recursive = 0;
		$this->Paginator->settings = compact('conditions','limit','order',"recursive");
		$commitments = $this->Paginator->paginate($this->Commitment); 
		$collaborators = $this->UserTeam->getCollaborators_NOTME(Configure::read('Application.team_id'), AuthComponent::user("id"));
		$this->set(compact('commitments','collaborators'));
	}

	public function add_commitment(){
		$this->list_permission();
		$this->loadModel('Commitment');
		//RECEPCION DEL FORMULARIO
		if ($this->request->is('post')) {
			//ACTUALIZACION DE CLIENTE
			var_dump($this->request->data["Commitment"]["project_id"]);
			$this->loadModel('Project');
			$projectId = $this->request->data["Commitment"]["project_id"];			 
			$idCliente = $this->Project->query("SELECT client_id FROM projects WHERE id = $projectId");
			 
			$this->request->data["Commitment"]["client_id"]  	  = $idCliente[0]["projects"]["client_id"];
			$this->request->data["Commitment"]["user_create_id"]  = AuthComponent::User("id");
			if($this->Commitment->save($this->request->data)){
				
				$this->loadModel('User');
				$conditions 	   = array('User.id' => $this->request->data["Commitment"]["user_id"]);
				$emailNotification = $this->User->find("first",compact("conditions"));
           		 
				$infoNotification = array(
					"email" 	 => $emailNotification["User"]["email"],
					"date" 		 => $this->request->data["Commitment"]["delivery_date"],
					"compromiso" => $this->request->data["Commitment"]["description"],
					"categoria"  => Configure::read("TipoCompromiso.".$this->request->data["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.".$this->request->data["Commitment"]["priority"])
				);
				if($this->commitments_notifications($infoNotification,"Nuevo Compromiso")){
					$this->Flash->success(__('Compromiso creado correctamente'));
					return $this->redirect(array('action' => 'mycommitments'));
				}
			}			 
		} 
		$this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
 		$this->set(compact("projects"));
 		//CONSULTO EL ROLE Y PERMISO DE LA PERSONA EN EL EQUIPO
 		//echo $team_id;
 		$this->loadModel('UserTeam');
 		$conditions = array(
			'UserTeam.team_id' => Configure::read("Application.team_id"),
			"UserTeam.user_id" => AuthComponent::User("id")
		);
		$userRolePermission = $this->UserTeam->find("first",compact("conditions"));
		$userRolePermission = $userRolePermission["UserTeam"];
 		$this->loadModel('Position');
 		$conditions = array(
			'Position.id' => $userRolePermission["position_id"],
			 
		);
		$roleInfo = $this->Position->find("first",compact("conditions"));
		$userRolePermission["Position"] = $roleInfo["Position"];
		//SE VALIDAN LOS PERMISOS DE ROLE
		$users = $this->list_user_add_commitment($roleInfo["Position"]["permission_taskee"],Configure::read("Application.team_id"),$userRolePermission["department_id"]);
 		$this->set(compact("users"));
	}

	private function commitments_notifications($commitments_info,$subject){
		$template = 'commiment_notification';
		$emails = array("johandiaz@strategee.us");
				$subject  = sprintf(__($subject." - Taskee"));
				$options = array(
					'to'       => $commitments_info["email"],
					'template' => $template,
					'subject'  => Configure::read('Application.name'). ' - '.$subject,
					'vars'     =>  array('info'=>$commitments_info),
				);
				$this->sendMail($options);  
				return true;
	}


	private function list_user_add_commitment($role_permission, $team_id,$department_id){
		//1. CUANDO EL ROL ES ADMINISTRADOR GENERAL 
		if($role_permission == 1){
			$users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ', U.lastname) as name
			FROM users as U 
			JOIN user_teams as UT ON UT.user_id = U.id 
			JOIN departments D ON UT.department_id = D.id  
			JOIN teams T ON UT.team_id = T.id
			WHERE UT.user_state = 0 AND UT.team_id = $team_id AND type_user = '1' AND U.firstname != ''  ");
			$users = Hash::combine($users,  '{n}.U.id','{n}.0.name') ;
			return $users;
			//CUANDO ES ADMIN DE AREA
		}elseif($role_permission == 2){
			$users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ',U.lastname) as name
			FROM users as U 
			JOIN user_teams as UT ON UT.user_id = U.id 
			JOIN departments D ON UT.department_id = D.id  
			JOIN teams T ON UT.team_id = T.id
			WHERE UT.user_state = 0 AND UT.team_id = $team_id
			AND U.firstname != ''
			AND D.id = $department_id
			");
			$users = Hash::combine($users,  '{n}.U.id','{n}.0.name') ;
			return $users;
			//CUANDO ES PERMISO NORMAL
		}else{
			$id = AuthComponent::User("id");
			$users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ',U.lastname) as name
			FROM users as U 
			JOIN user_teams as UT ON UT.user_id = U.id 
			JOIN departments D ON UT.department_id = D.id  
			JOIN teams T ON UT.team_id = T.id
			WHERE UT.user_state = 0 AND UT.team_id = $team_id
			AND U.firstname != ''
			AND D.id = $department_id
			AND U.id = $id
			");
			$users = Hash::combine($users,  '{n}.U.id','{n}.0.name') ;
			return $users;
		}
 		//2. CUANDO EL ROL ES DIRECTOR DE AREA, O CON PERMISOS DE DIRECCION DE AREA.
 		//3. CUANDO ES UN USUARIO SIN PERMISOS DE DIRECCION DE AREA.
		return array();
	}

	public function finished_commiment(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('Commitment');
		if ($this->request->is('post')) {
			$this->request->data["Commitment"]["id"] = $this->request->data["id"];
			$this->request->data["Commitment"]["finished_date"] = date("Y-m-d");
			$this->request->data["Commitment"]["state"] = 2;
			 
			//SE BUSCA EL ROLDE DEL USUARIO EN TASKEE
			$this->loadModel('UserTeam');
			$this->UserTeam->recursive = 0;
			$conditions = array(
				'UserTeam.user_id' => AuthComponent::user("id"),
				"UserTeam.team_id" => Configure::read("Application.team_id")
			);
			$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));

			//SE BUSCA LA INFO DEL COMPROMISO
			$this->loadModel('Commitment');
			$conditions = array(
				'Commitment.id' => $this->request->data["Commitment"]["id"],
			);
			$commitment = $this->Commitment->find("first",compact("conditions","recursive"));
			$smf_generate = $this->calculateBonificationSMF($userPosition,$commitment["Commitment"]["type_categorie"], $commitment["Commitment"]["delivery_date"], $commitment["Commitment"]["priority"]);
			$this->request->data["Commitment"]["smf_generate"] = $smf_generate;
		 

			if($this->Commitment->save($this->request->data)){
				return 1;
			}else{
				return 0;
			}

			 
		}
		return 0;

	}

	public function commiment_cambio_fecha(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('CommitmentsLog');
		$this->loadModel('Commitment');
		
		if ($this->request->is('post')) {
			$recursive  = -1;
			$conditions = array('Commitment.id' => $this->request->data["id"]);
			$commitment = $this->Commitment->find('first', compact('conditions','recursive'));
			$new_date   = date("Y-m-d", strtotime($this->request->data["fecha"]));
 			if($new_date == $commitment['Commitment']['delivery_date']) {
				return 2;
			}
        	if($new_date < date('Y-m-d')) {
				return 3;
			} 
			$this->request->data["CommitmentsLog"]["commitment_id"] = $this->request->data["id"];
			$this->request->data["CommitmentsLog"]["type"] 			= "date";
			$this->request->data["CommitmentsLog"]["description"]   = $this->request->data["motivo"];
			$this->request->data["CommitmentsLog"]["date"] 			= $this->request->data["fecha"];
			if($this->CommitmentsLog->save($this->request->data)) {
				$this->request->data["Commitment"]["id"] = $this->request->data["id"];
				return 1;
			} else {
				return 0;
			}
		}
		return 0;
	}

	public function commiment_cambio_descripcion(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('CommitmentsLog');
		if ($this->request->is('post')) {
			$this->request->data["CommitmentsLog"]["commitment_id"] = $this->request->data["id"];
			$this->request->data["CommitmentsLog"]["type"] = "description";
			$this->request->data["CommitmentsLog"]["description"] = $this->request->data["descripcion"];
			 
			if($this->CommitmentsLog->save($this->request->data)){
			 
				return 1;
			}else{
				return 0;
			}

		}
		return 0;

	}


	public function commiment_rechazo(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('CommitmentsLog');
		if ($this->request->is('post')) {
			$this->request->data["CommitmentsLog"]["commitment_id"] = $this->request->data["id"];
			$this->request->data["CommitmentsLog"]["type"] = "rechazo";
			$this->request->data["CommitmentsLog"]["description"] = $this->request->data["descripcion"];
			 
			if($this->CommitmentsLog->save($this->request->data)){
			 
				return 1;
			}else{
				return 0;
			}

		}
		return 0;

	}

	public function rechazarSolicitud(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('CommitmentsLog');
		if ($this->request->is('post')) {
			$conditions = array(
							"CommitmentsLog.id" => $this->request->data["id"]
			);
			$commimentLog = $this->CommitmentsLog->find("first",array("conditions"));
			$this->request->data["CommitmentsLog"]["id"] = $this->request->data["id"];
			$this->request->data["CommitmentsLog"]["state"] = 2;
			if($this->CommitmentsLog->save($this->request->data)){
				$this->loadModel('Commitment');
				$conditions = array(
					'Commitment.id' => $commimentLog["CommitmentsLog"]["commitment_id"],
				);
				$commiment = $this->Commitment->find("first",array("conditions"));
				$this->loadModel('User');
				$conditions = array(
					'User.id' =>  $commiment["Commitment"]["user_id"],
				);
				$emailNotification = $this->User->find("first",compact("conditions"));
				$infoNotification = array(
							"email" => $emailNotification["User"]["email"],
							"date" => $commiment["Commitment"]["delivery_date"],
							"compromiso" =>  $commiment["Commitment"]["description"],
							"categoria" => Configure::read("TipoCompromiso.". $commiment["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.". $commiment["Commitment"]["priority"])
						);
				$this->commitments_notifications($infoNotification,"Solicitud Rechazada");
				return 1;
			}else{
				return 0;
			}
		}

	}

	public function aceptarSolicitud(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('CommitmentsLog');
		$this->loadModel('Commitment');
		if ($this->request->is('post')) {
			$this->request->data["CommitmentsLog"]["id"] = $this->request->data["id"];
			$this->request->data["CommitmentsLog"]["state"] =  1;
			$conditions = array(
				'CommitmentsLog.id' => $this->request->data["id"],
			);
			$commitmentLog = $this->CommitmentsLog->find("first",compact("conditions","recursive"));

			$conditions = array(
							"CommitmentsLog.id" => $this->request->data["id"]
			);
			$commimentLog = $this->CommitmentsLog->find("first",array("conditions"));
			if($commitmentLog["CommitmentsLog"]["type"] == "date"){
				$this->request->data["Commitment"]["id"] = $commitmentLog["CommitmentsLog"]["commitment_id"];
				$this->request->data["Commitment"]["delivery_date"] = $commitmentLog["CommitmentsLog"]["date"];
				if($this->Commitment->save($this->request->data)){
					if($this->CommitmentsLog->save($this->request->data)){
						$this->loadModel('Commitment');
						$conditions = array(
							'Commitment.id' => $commimentLog["CommitmentsLog"]["commitment_id"],
						);
						$commiment = $this->Commitment->find("first",array("conditions"));
						$this->loadModel('User');
						$conditions = array(
							'User.id' =>  $commiment["Commitment"]["user_id"],
						);
						$emailNotification = $this->User->find("first",compact("conditions"));
						$infoNotification = array(
									"email" => $emailNotification["User"]["email"],
									"date" => $commiment["Commitment"]["delivery_date"],
									"compromiso" =>  $commiment["Commitment"]["description"],
									"categoria" => Configure::read("TipoCompromiso.". $commiment["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.". $commiment["Commitment"]["priority"])
								);
						$this->commitments_notifications($infoNotification,"Solicitud Aceptada");
						return 1;
					}else{
						return 0;
					}
				}else{
					return 0;
				}
			}elseif(($commitmentLog["CommitmentsLog"]["type"] == "description")){
				$this->request->data["Commitment"]["id"] = $commitmentLog["CommitmentsLog"]["commitment_id"];
				$this->request->data["Commitment"]["description"] = $commitmentLog["CommitmentsLog"]["description"];
				if($this->Commitment->save($this->request->data)){
					if($this->CommitmentsLog->save($this->request->data)){
						$conditions = array(
							'Commitment.id' => $commimentLog["CommitmentsLog"]["commitment_id"],
						);
						$commiment = $this->Commitment->find("first",array("conditions"));
						$this->loadModel('User');
						$conditions = array(
							'User.id' =>  $commiment["Commitment"]["user_id"],
						);
						$emailNotification = $this->User->find("first",compact("conditions"));
						$infoNotification = array(
									"email" => $emailNotification["User"]["email"],
									"date" => $commiment["Commitment"]["delivery_date"],
									"compromiso" =>  $commiment["Commitment"]["description"],
									"categoria" => Configure::read("TipoCompromiso.". $commiment["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.". $commiment["Commitment"]["priority"])
								);
						$this->commitments_notifications($infoNotification,"Solicitud Aceptada");
						return 1;
					}else{
						return 0;
					}
				}else{
					return 0;
				}
				return 0;
			}else{
				$this->request->data["Commitment"]["id"] = $commitmentLog["CommitmentsLog"]["commitment_id"];
				$this->request->data["Commitment"]["state"] = 3;
				if($this->Commitment->save($this->request->data)){
					if($this->CommitmentsLog->save($this->request->data)){
						$conditions = array(
							'Commitment.id' => $commimentLog["CommitmentsLog"]["commitment_id"],
						);
						$commiment = $this->Commitment->find("first",array("conditions"));
						$this->loadModel('User');
						$conditions = array(
							'User.id' =>  $commiment["Commitment"]["user_id"],
						);
						$emailNotification = $this->User->find("first",compact("conditions"));
						$infoNotification = array(
									"email" => $emailNotification["User"]["email"],
									"date" => $commiment["Commitment"]["delivery_date"],
									"compromiso" =>  $commiment["Commitment"]["description"],
									"categoria" => Configure::read("TipoCompromiso.". $commiment["Commitment"]["type_categorie"]) ." - ". Configure::read("PrioridadTareas.". $commiment["Commitment"]["priority"])
								);
						$this->commitments_notifications($infoNotification,"Solicitud Aceptada");
						return 1;
					}else{
						return 0;
					}
				}else{
					return 0;
				}
				return 0;
			}
			return 0;
		}
		return 0;
	}

	public function reports(){

		$this->loadModel('Commitment');
		$this->loadModel('UserTeam');
		$order = array('Commitment.delivery_date DESC');
		$limit = 20;

		if (!empty($this->request->query['fechas'])) {
		// if ($this->request->is('post')) {
			$rangoFechas = $this->request->query["fechas"];
			$rangoFechas = explode(" - ", $rangoFechas);
			$conditions = array(
				'Commitment.user_id' => AuthComponent::user("id"),
				"Commitment.state" => "2",
				'finished_date  >='=> $rangoFechas[0],
                'finished_date  <=' => $rangoFechas[1]
			);
			$this->set(compact("rangoFechas")); 
		} else {
			$month = date("m");
			$year  = date("Y");	
 	
			$conditions = array(
				'Commitment.user_id' => AuthComponent::user("id"),
				"Commitment.state" => "2",
				'MONTH(finished_date ) ='=> $month,
                'YEAR(finished_date ) =' => $year
			);
			$rangoFechas = date("y-m-d")." - ".date("y-m-d");
			$rangoFechas = explode(" - ", $rangoFechas);
			$this->set(compact("rangoFechas"));
		} 
		$this->Commitment->recursive = 0;
		$this->Paginator->settings = compact('conditions','limit','order',"recursive");
		$commitments = $this->Paginator->paginate($this->Commitment);
		//SE BUSCA EL ROLE EN EL TEAM STRATEGEE
		$this->loadModel('UserTeam');
		$this->UserTeam->recursive = 0;
		$conditions = array(
			'UserTeam.user_id' => AuthComponent::user("id"),
			"UserTeam.team_id" => Configure::read("Application.team_id")
		);
		$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
		$this->set(compact("commitments","userPosition")); 
	}


	private function calculateBonificationSMF($userPosition,$categorie,$dateFinish,$categorie_priority){
		$dias = "";
		$hoy = date("Y/m/d");
		$dias = (strtotime($hoy)-strtotime($dateFinish))/86400;$dias = floor($dias);
		$fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
		$fecha_entrada = strtotime($dateFinish);
		if($fecha_actual > $fecha_entrada){
			$dias = abs($dias) * -1;
			 
		}else{
			 $dias = abs($dias);
		}
		if($categorie == 3){
			return 0;
		}elseif($categorie == 2){
			if($categorie_priority == 1){
				if($dias <= -1){
					return ((abs($dias) * 3000)) * -1;
				}else{
					return ($dias * 3000) + $userPosition["Position"]["smf_alta"];
				}
			}else{
				if($dias <= -1){
					return ((abs($dias) * 2000)) * -1;
				}else{
					return ($dias * 3000) + $userPosition["Position"]["smf_estandar"];
				}
			}
			return 0;
		}elseif($categorie == 1){
			if($categorie_priority == 1){
				if($dias <= -1){
					return ((abs($dias) * 1200)) * -1;
				}else{
					return ($dias * 1200) + $userPosition["Position"]["cotidiana_alta"];
				}
			}else{
				if($dias <= -1){
					return ((abs($dias) * 800)) * -1;
				}else{
					return ($dias * 800) + $userPosition["Position"]["cotidiana_estandar"];
				}
			}
		}
		return 0;
	}

	public function generate_report(){
		// if ($this->request->is('post')) {

		
			Configure::write('debug', 0);
			$this->autoRender = false;
			$this->PhpExcel->createWorksheet();
			$this->PhpExcel->setDefaultFont('Calibri', 16);
			$header = array(
				array('label' => __('Compromiso'), 'width' => '100'),
				array('label' => __('Tipo de compromiso'), 'width' => '20'),
				array('label' => __('Prioridad'), 'width' => '20'),
				array('label' => __('Fecha compromiso'), 'width' => '20'),
				array('label' => __('Fecha terminacion'), 'width' => '20'),
				array('label' => __('Valor Ganado / Perdido'), 'width' => '20'),
			);
			$this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
			
			$limit = 100000;
			$this->loadModel('Commitment');
			$this->Commitment->recursive = -1;
			$conditions = array(
				'Commitment.user_id' => AuthComponent::user("id"),
				'finished_date  >='=>   $this->request->query["fechaInicio"],
                'finished_date  <=' =>  $this->request->query["fechaFin"],
                "Commitment.state" => "2",
			); 
			$commiments = $this->Commitment->find("all",compact("conditions","recursive"));
			foreach ($commiments as $key => $value){
				 
				$this->PhpExcel->addTableRow(
					array(
						(h( $value["Commitment"]["description"])),
						h(Configure::read("TipoCompromiso.".$value["Commitment"]["type_categorie"])),
						h(Configure::read("PrioridadTareas.".$value["Commitment"]["priority"])),
						h($value["Commitment"]["delivery_date"]),
						h($value["Commitment"]["finished_date"]),
						h($value["Commitment"]["smf_generate"]),
					)
				);
			}
			$this->PhpExcel->output('reporte.xlsx');		
		// } 
	}


	public function report_admin(){
		//////////////////////////////USUARIOS PERMITIDOS////////////////////////
		$this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
 		$this->set(compact("projects"));
 		//CONSULTO EL ROLE Y PERMISO DE LA PERSONA EN EL EQUIPO
 		//echo $team_id;
 		$this->loadModel('UserTeam');
 		$conditions = array(
			'UserTeam.team_id' => Configure::read("Application.team_id"),
			"UserTeam.user_id" => AuthComponent::User("id")
		);
		$userRolePermission = $this->UserTeam->find("first",compact("conditions"));
		$userRolePermission = $userRolePermission["UserTeam"];
 		$this->loadModel('Position');
 		$conditions = array(
			'Position.id' => $userRolePermission["position_id"],
			 
		);
		$roleInfo = $this->Position->find("first",compact("conditions"));
		$userRolePermission["Position"] = $roleInfo["Position"];
		//SE VALIDAN LOS PERMISOS DE ROLE
		$users = $this->list_user_add_commitment($roleInfo["Position"]["permission_taskee"],Configure::read("Application.team_id"),$userRolePermission["department_id"]);
		$ids_query = array();
		foreach ($users as $key => $value) {
			$ids_query[] = $key;
		}
 		$this->set(compact("users"));
 		////////////////////////////////////////////////////////////
		$this->loadModel('Commitment');
		$order = array('Commitment.delivery_date DESC');
		$limit = 20;
		if (!empty($this->request->query['user_id']) || !empty($this->request->query['fechas']) ) { 
		// if ($this->request->is('post')) {
			$rangoFechas  = $this->request->query["fechas"];
			$rangoFechas  = explode(" - ", $rangoFechas);
			if(!empty($this->request->query["user_id"])){
				$ids = array();
				foreach ($this->request->query["user_id"] as $key => $value) {
					$ids[] = $value;
				}
				$conditions = array(
					'Commitment.user_id' =>  $ids,
					"Commitment.state"   => "2",
					'Commitment.finished_date BETWEEN ? and ?' => array($rangoFechas[0], $rangoFechas[1])
				);
			}else{
				$conditions = array(
					'Commitment.user_id' => $ids_query,
					"Commitment.state"   => "2",
					'Commitment.finished_date BETWEEN ? and ?' => array($rangoFechas[0], $rangoFechas[1])
				);
			} 
			$this->set(compact("rangoFechas"));
		} else {
			$month = date("m");
			$year  = date("Y");
			$conditions = array(
				"Commitment.state"       => array("1","2"),
				'MONTH(finished_date) =' => $month,
                'YEAR(finished_date) ='  => $year,
                'Commitment.user_id'     => $ids_query
			);
			$rangoFechas = date("y-m-d")." - ".date("y-m-d");
			$rangoFechas = explode(" - ", $rangoFechas);
			$this->set(compact("rangoFechas"));
		} 
		$this->Commitment->recursive = 0;
		$this->Paginator->settings   = compact('conditions','limit','order',"recursive");
		$commitments = $this->Paginator->paginate($this->Commitment); 
		//SE BUSCA EL ROLE EN EL TEAM STRATEGEE
		$this->loadModel('UserTeam');
		$this->UserTeam->recursive = 0;
		$conditions = array(
			'UserTeam.user_id' => AuthComponent::user("id"),
			"UserTeam.team_id" => Configure::read("Application.team_id")
		);
		$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
		$this->set(compact("commitments","userPosition")); 
	}
	 

	public function report_admin_pending(){
		//////////////////////////////USUARIOS PERMITIDOS////////////////////////
		$this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
 		$this->set(compact("projects"));
 		//CONSULTO EL ROLE Y PERMISO DE LA PERSONA EN EL EQUIPO
 		//echo $team_id;
 		$this->loadModel('UserTeam');
 		$conditions = array(
			'UserTeam.team_id' => Configure::read("Application.team_id"),
			"UserTeam.user_id" => AuthComponent::User("id")
		);
		$userRolePermission = $this->UserTeam->find("first",compact("conditions"));
		$userRolePermission = $userRolePermission["UserTeam"];
 		$this->loadModel('Position');
 		$conditions = array('Position.id' => $userRolePermission["position_id"]);
		$roleInfo   = $this->Position->find("first",compact("conditions"));
		$userRolePermission["Position"] = $roleInfo["Position"];
		//SE VALIDAN LOS PERMISOS DE ROLE
		$users     = $this->list_user_add_commitment($roleInfo["Position"]["permission_taskee"],Configure::read("Application.team_id"),$userRolePermission["department_id"]);
		$ids_query = array();
		foreach ($users as $key => $value) {
			$ids_query[] = $key;
		}
		 
 		$this->set(compact("users"));
 		////////////////////////////////////////////////////////////
		$this->loadModel('Commitment');
		$order = array('Commitment.delivery_date DESC');
		$limit = 1000;
		if (!empty($this->request->query['user_id']) || !empty($this->request->query['fechas']) ) { 
		// if ($this->request->is('post')) {
			$rangoFechas  = $this->request->query["fechas"];
			$rangoFechas  = explode(" - ", $rangoFechas);

			if(!empty($this->request->query["user_id"])) { 
				$ids = array();
				foreach ($this->request->query["user_id"] as $key => $value) {
					$ids[] = $value;
				}
				$conditions = array(
					'Commitment.user_id' => $ids,
					"Commitment.state"   => "1",
					'Commitment.delivery_date BETWEEN ? and ?' => array($rangoFechas[0], $rangoFechas[1])
				);
			} else { 
				$conditions = array(
					'Commitment.user_id' => $ids_query,
					"Commitment.state"   => "1",
					'Commitment.delivery_date BETWEEN ? and ?' => array($rangoFechas[0], $rangoFechas[1])
				);
			}
			$this->set(compact("rangoFechas"));
		} else { 
			$month = date("m");
			$year  = date("Y");
			$conditions = array(
				"Commitment.state"        => array("1"),
				'MONTH(delivery_date) ='  => $month,
                'YEAR(delivery_date)  ='  => $year,
                'Commitment.user_id'      => $ids_query
			); 
			$rangoFechas = date("y-m-d")." - ".date("y-m-d");
			$rangoFechas = explode(" - ", $rangoFechas);
			$this->set(compact("rangoFechas"));
		}  
		$this->Commitment->recursive = 0;
		$this->Paginator->settings = compact('conditions','limit','order',"recursive");
		$commitments = $this->Paginator->paginate($this->Commitment);
		//SE BUSCA EL ROLE EN EL TEAM STRATEGEE
		$this->loadModel('UserTeam');
		$this->UserTeam->recursive = 0;
		$conditions = array(
			'UserTeam.user_id' => AuthComponent::user("id"),
			"UserTeam.team_id" => Configure::read("Application.team_id")
		);
		$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
		$this->set(compact("commitments","userPosition")); 
	}


	public function report_admin_defeated(){
		//////////////////////////////USUARIOS PERMITIDOS////////////////////////
		$this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
 		$this->set(compact("projects"));
 		//CONSULTO EL ROLE Y PERMISO DE LA PERSONA EN EL EQUIPO
 		//echo $team_id;
 		$this->loadModel('UserTeam');
 		$conditions = array(
			'UserTeam.team_id' => Configure::read("Application.team_id"),
			"UserTeam.user_id" => AuthComponent::User("id")
		);
		$userRolePermission = $this->UserTeam->find("first",compact("conditions"));
		$userRolePermission = $userRolePermission["UserTeam"];
 		$this->loadModel('Position');
 		$conditions = array('Position.id' => $userRolePermission["position_id"]);
		$roleInfo   = $this->Position->find("first",compact("conditions"));
		$userRolePermission["Position"] = $roleInfo["Position"];
		//SE VALIDAN LOS PERMISOS DE ROLE
		$users     = $this->list_user_add_commitment($roleInfo["Position"]["permission_taskee"],Configure::read("Application.team_id"),$userRolePermission["department_id"]);
		$ids_query = array();
		foreach ($users as $key => $value) {
			$ids_query[] = $key;
		}
 		$this->set(compact("users"));
 		////////////////////////////////////////////////////////////
		$this->loadModel('Commitment');
		$order = array('Commitment.delivery_date DESC');
		$limit = 20; 
		if (!empty($this->request->query['user_id'])) { 
			$hoy = date("Y-m-d");
			if(!empty($this->request->query["user_id"])){
				$ids = array();
				foreach ($this->request->query["user_id"] as $key => $value) {
					$ids[] = $value;
				}
				$conditions = array(
					'Commitment.user_id' =>  $ids,
					"Commitment.state"   => "1",
					'delivery_date  <'   => $hoy,
				);
			}else{
				$conditions = array(
					'Commitment.user_id' =>  $ids_query,
					"Commitment.state"   => "1",
					'delivery_date  <'   => $hoy,
				);

			}
			$this->set(compact("rangoFechas"));
		}else{
			$month = date("m");
			$year  = date("Y");
			$hoy   = date("Y-m-d");
			$conditions = array(
				'Commitment.user_id' => $ids_query,
				"Commitment.state"   => array("1"),
				'delivery_date < '   => $hoy,
			);
			$rangoFechas = date("y-m-d")." - ".date("y-m-d");
			$rangoFechas = explode(" - ", $rangoFechas);
			$this->set(compact("rangoFechas"));
		} 
		$this->Commitment->recursive = 0;
		$this->Paginator->settings = compact('conditions','limit','order',"recursive");
		$commitments = $this->Paginator->paginate($this->Commitment);
		//SE BUSCA EL ROLE EN EL TEAM STRATEGEE
		$this->loadModel('UserTeam');
		$this->UserTeam->recursive = 0;
		$conditions = array(
			'UserTeam.user_id' => AuthComponent::user("id"),
			"UserTeam.team_id" => Configure::read("Application.team_id")
		);
		$userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
		$this->set(compact("commitments","userPosition")); 
	}

	public function generate_report_admin_penging(){
		$this->layout = false;
		$this->autoRender = false;
		// if ($this->request->is('post')) { 
			Configure::write('debug', 0);
			$this->autoRender = false;
			$this->PhpExcel->createWorksheet();
			$this->PhpExcel->setDefaultFont('Calibri', 16);
			$header = array(
				array('label' => __('Responsable'), 'width' => '20'),
				array('label' => __('Compromiso'), 'width' => '100'),
				array('label' => __('Tipo de compromiso'), 'width' => '20'),
				array('label' => __('Prioridad'), 'width' => '20'),
				array('label' => __('Fecha compromiso'), 'width' => '20'),
				 
			);
			$this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
			if(!empty($this->request->query["user_id"])){
				$ids = array();
				foreach ($this->request->query["user_id"] as $key => $value) {
					$ids[] = $value;
					 
				}
				$limit = 100000;
				$this->loadModel('Commitment');
				$this->Commitment->recursive = 1;
				$conditions = array(
					'Commitment.user_id' =>  $ids,
					'delivery_date  >='=>  $this->request->query["fechaInicio"],
	                'delivery_date  <=' =>  $this->request->query["fechaFin"],
	                "Commitment.state" => "1",
				);
			}else{
				$limit = 100000;
				$this->loadModel('Commitment');
				$this->Commitment->recursive = 1;
				$conditions = array(
					'Commitment.user_id' =>  $ids_query,
					'delivery_date  >='=>  $this->request->query["fechaInicio"],
	                'delivery_date  <=' =>  $this->request->query["fechaFin"],
	                "Commitment.state" => "1",
				);
			}
			$commiments = $this->Commitment->find("all",compact("conditions","recursive"));
			foreach ($commiments as $key => $value){
				$this->PhpExcel->addTableRow(
					array(
						(h( $value["User"]["firstname"]) . " ". h($value["User"]["lastname"])  ),
						(h( $value["Commitment"]["description"])),
						h(Configure::read("TipoCompromiso.".$value["Commitment"]["type_categorie"])),
						h(Configure::read("PrioridadTareas.".$value["Commitment"]["priority"])),
						h($value["Commitment"]["delivery_date"]),
					)
				);
			}
			$this->PhpExcel->output('reporte.xlsx');		
		// } 
	}

	public function generate_report_admin_defeated(){
		$this->layout     = false;
		$this->autoRender = false; 
	
		// if ($this->request->is('post')) {
			Configure::write('debug', 0);
			$this->autoRender = false;
			$this->PhpExcel->createWorksheet();
			$this->PhpExcel->setDefaultFont('Calibri', 16);
			$header = array(
				array('label' => __('Responsable'), 'width' => '20'),
				array('label' => __('Compromiso'), 'width' => '100'),
				array('label' => __('Tipo de compromiso'), 'width' => '20'),
				array('label' => __('Prioridad'), 'width' => '20'),
				array('label' => __('Fecha compromiso'), 'width' => '20'),
				 
			);
			$hoy = date("Y-m-d");
			$this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
			if(!empty($this->request->query["user_id"])){
				$ids = array();
				foreach ($this->request->query["user_id"] as $key => $value) {
					$ids[] = $value;
					 
				}
				$limit = 100000;
				$this->loadModel('Commitment');
				$this->Commitment->recursive = 1;
				$conditions = array(
					'Commitment.user_id' =>  $ids,
					'delivery_date  <'=>  $hoy,
	                "Commitment.state" => "1",
				);
			}else{
				$limit = 100000;
				$this->loadModel('Commitment');
				$this->Commitment->recursive = 1;
				$conditions = array(
					'Commitment.user_id' =>  AuthComponent::user("id"),
					'delivery_date <'=>  $hoy,
	                "Commitment.state" => "1",
				);
			}
			$commiments = $this->Commitment->find("all",compact("conditions","recursive"));
			foreach ($commiments as $key => $value){
				$this->PhpExcel->addTableRow(
					array(
						(h( $value["User"]["firstname"]) . " ". h($value["User"]["lastname"])  ),
						(h( $value["Commitment"]["description"])),
						h(Configure::read("TipoCompromiso.".$value["Commitment"]["type_categorie"])),
						h(Configure::read("PrioridadTareas.".$value["Commitment"]["priority"])),
						h($value["Commitment"]["delivery_date"]),
					)
				);
			}
			$this->PhpExcel->output('reporte.xlsx');		
		// } 
	}

	public function generate_report_admin(){
		$this->layout = false;
		$this->autoRender = false;
		// if ($this->request->is('post')) {

		
			Configure::write('debug', 0);
			$this->autoRender = false;
			$this->PhpExcel->createWorksheet();
			$this->PhpExcel->setDefaultFont('Calibri', 16);
			$header = array(
				array('label' => __('Responsable'), 'width' => '20'),
				array('label' => __('Compromiso'), 'width' => '100'),
				array('label' => __('Tipo de compromiso'), 'width' => '20'),
				array('label' => __('Prioridad'), 'width' => '20'),
				array('label' => __('Fecha compromiso'), 'width' => '20'),
				array('label' => __('Fecha terminación'), 'width' => '20'),
				array('label' => __('Valor Ganado / Perdido'), 'width' => '20'),
			);
			$this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
			if(!empty($this->request->query["user_id"])){
				$ids = array();
				foreach ($this->request->query["user_id"] as $key => $value) {
					$ids[] = $value;
					 
				}
				$limit = 100000;
				$this->loadModel('Commitment');
				$this->Commitment->recursive = 1;
				$conditions = array(
					'Commitment.user_id' =>  $ids,
					'finished_date  >='=>  $this->request->query["fechaInicio"],
	                'finished_date  <=' =>  $this->request->query["fechaFin"],
	                "Commitment.state" => "2",
				);
			}else{
				$limit = 100000;
				$this->loadModel('Commitment');
				$this->Commitment->recursive = 1;
				$conditions = array(
					'Commitment.user_id' =>  AuthComponent::user("id"),
					'finished_date  >='=>  $this->request->data["fechaInicio"],
	                'finished_date  <=' =>  $this->request->data["fechaFin"],
	                "Commitment.state" => "2",
				);
			}
			$commiments = $this->Commitment->find("all",compact("conditions","recursive"));
			foreach ($commiments as $key => $value){
				$this->PhpExcel->addTableRow(
					array(
						(h( $value["User"]["firstname"]) . " ". h($value["User"]["lastname"])  ),
						(h( $value["Commitment"]["description"])),
						h(Configure::read("TipoCompromiso.".$value["Commitment"]["type_categorie"])),
						h(Configure::read("PrioridadTareas.".$value["Commitment"]["priority"])),
						h($value["Commitment"]["delivery_date"]),
						h($value["Commitment"]["finished_date"]),
						h($value["Commitment"]["smf_generate"]),
					)
				);
			}
			$this->PhpExcel->output('reporte.xlsx');		
		// } 
	}


	public function configurarNotificacion(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('Commitment');

		$this->request->data["Commitment"]["id"] = $this->request->data["id"];
		$this->request->data["Commitment"]["notification_date"] = $this->request->data["fecha"];
		if($this->Commitment->save($this->request->data)){
			return 1;
		}else{
			return 0;
		}
		

		return 0;

	}

	private function list_permission(){
        $this->loadModel('UserTeam');
        $this->UserTeam->recursive = 0;
        $conditions = array(
            'UserTeam.user_id' => AuthComponent::user("id"),
            "UserTeam.team_id" => Configure::read("Application.team_id")
        );
        $userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
        $this->set(compact("userPosition")); 
   	}

   	private function task_notifications($commitments_info,$subject){
		$template = 'task_notification';
		$emails   = array("johandiaz@strategee.us");
		$subject  = sprintf(__($subject." - Taskee"));
		$options  = array(
			'to'       => $commitments_info["email"],
			'template' => $template,
			'subject'  => Configure::read('Application.name'). ' - '.$subject,
			'vars'     =>  array('info'=>$commitments_info),
		);
		$this->sendMail($options);  
		return true;
	}

	public function reasignar_compromisos(){
		set_time_limit(100000);
		$this->loadModel('Commitment');
		$this->loadModel('User');
		$this->layout      = 'ajax';
		$this->autoRender  = false;
		$recursive   	   = -1;
		$conditions  	   = array('Commitment.id' => $this->request->data['ids']);
		$compromisos 	   = $this->Commitment->find('all',compact('conditions','recursive'));

		$conditions 	   = array('User.id' => $this->request->data['user_id']);
		$emailNotification = $this->User->find('first',compact('conditions','recursive'));

		foreach ($compromisos as $compromiso) {           		 
	       	$compromiso['Commitment']['user_id'] = $this->request->data['user_id'];
            $this->Commitment->save($compromiso);
			$infoNotification = array(
				'email' 	 => $emailNotification['User']['email'],
				'date' 		 => $compromiso['Commitment']['delivery_date'],
				'compromiso' => $compromiso['Commitment']['description'],
				'categoria'  => Configure::read('TipoCompromiso.'.$compromiso['Commitment']['type_categorie']) ." - ". Configure::read("PrioridadTareas.".$compromiso["Commitment"]["priority"])
			);		 
		    $this->commitments_notifications($infoNotification, "Nuevo Compromiso");
		}
		$this->outJSON(true);
		exit(); 
	} 

	public function eliminar_compromisos(){
		set_time_limit(100000);
		$this->loadModel('Commitment');
		$this->layout     = 'ajax';
		$this->autoRender = false;
		if(!empty($this->request->data['ids'])) {
			$conditions = array('Commitment.id' => $this->request->data['ids']);
			$this->Commitment->deleteAll($conditions, false);
		}
		$this->outJSON(true);
		exit(); 
	}
} 