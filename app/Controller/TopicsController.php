<?php
App::uses('AppController', 'Controller');
 
class TopicsController extends AppController {

	public $components = array('Paginator','RequestHandler');
	public $helpers    = array('Js');
 		 
 	public function add_topic_meeting_edit(){
 		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		if (!$this->Topic->Meeting->exists(EncryptDecrypt::decrypt($this->request->data["meetingId"]))) {
			$this->showMessageExceptions();
		} 
		$this->layout 	  = 'ajax';
		$this->autoRender = false;
		$response = $this->getValidates($this->request->data, "Topic");
		if(!is_array($response)){
			$this->__saveTopicMeetingEdit($this->request->data);
			$response = array("message" => __("Se ha añadido correctamente el tema específico."), "state" => true);
			$this->outJSON($response);
		} else {
			$this->outJSON($response);
		}
 	}

 	private function __saveTopicMeetingEdit($data = array()){
 		$saveData = array(
 			"meeting_id"  => EncryptDecrypt::decrypt($data["meetingId"]),
 			"time"        => $data["time"],
 			"description" => $data["description"]
		);
		$this->Topic->save($saveData);
		$this->__logAddTopicEditMeeting($this->Topic->id, EncryptDecrypt::decrypt($data["meetingId"]));
 	}

 	public function get_info_topic(){
 		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		if (!$this->Topic->exists(EncryptDecrypt::decrypt($this->request->data["topicId"]))) {
			$this->showMessageExceptions();
		} 
		$this->layout 	  = 'ajax';
		$this->autoRender = false;
		$topicId 		  = isset($this->request->data["topicId"]) ? EncryptDecrypt::decrypt($this->request->data["topicId"]) : null;
		$recursive 		  = -1;
		$conditions       = array("Topic.id" => $topicId);
		$topic            = $this->Topic->find("first", compact("conditions","recursive"));
		$time             = $this->__conversorSegundosHoras($topic["Topic"]["time"]);
	 	$topic["Topic"]["hour_convert"]   = $time["hours"];
	 	$topic["Topic"]["minute_convert"] = $time["minutes"]; 
		$this->outJSON($topic); 
 	}

 	private function __conversorSegundosHoras($time) {
	    $horas      = floor($time / 3600);
	    $minutos    = floor(($time - ($horas * 3600)) / 60);
	    $segundos   = $time - ($horas * 3600) - ($minutos * 60);
	    $hours 		= "";
	    $minutes    = "";
	    if ($horas > 0 ) {
	        $hours .= $horas;
	    } else {
	    	$hours = 0;
	    }
	    if ($minutos > 0 ) {
	        $minutes .= $minutos;
	    } else {
	    	$minutes = 0;
	    }
	    return array("hours" => $hours, "minutes" => $minutes);
	}

	public function edit_topic_meeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->request->data["topic_id"]   = EncryptDecrypt::decrypt($this->request->data["topic_id"]);
		$this->request->data["meeting_id"] = EncryptDecrypt::decrypt($this->request->data["meeting_id"]);
		if (!$this->Topic->exists($this->request->data["topic_id"])) {
			$this->showMessageExceptions();
		}
	 	$this->layout 	  = 'ajax';
		$this->autoRender = false;
		$beforeEdit = $this->__logBeforeEditTopicEditMeeting($this->request->data["topic_id"]);
		$response   = $this->getValidates($this->request->data["Topic"], "Topic");
		if(!is_array($response)){
			$this->__editInfoTopicMeeting($this->request->data, $beforeEdit);
			$response = array("message" => __("Se ha editado correctamente el tema específico."), "state" => true);
			$this->outJSON($response);
		} else {
			$this->outJSON($response);
		} 
	}

	private function __editInfoTopicMeeting($data = array(), $beforeEdit){
		$saveData = array(
			"id" 		  => $data["topic_id"],
			"meeting_id"  => $data["meeting_id"],
			"description" => $data["Topic"]["description"],
			"time"  	  => $data["Topic"]["time"],
		);
		$this->Topic->save($saveData);
		$this->__logAfterEditTopicEditMeeting($data["topic_id"], $beforeEdit);
	}

	public function delete_topic_meeting_edit(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		if (!$this->Topic->exists(EncryptDecrypt::decrypt($this->request->data["topicId"]))) {
			$this->showMessageExceptions();
		} 
		$this->layout 	  = 'ajax';
		$this->autoRender = false;
		$this->__logDeleteTopicEditMeeting($this->request->data);
		$this->Topic->delete(EncryptDecrypt::decrypt($this->request->data["topicId"]));
	}

	private function __logDeleteTopicEditMeeting($data = array()){
		$conditions  = array("Topic.id" => EncryptDecrypt::decrypt($data["topicId"]), "Topic.meeting_id" => EncryptDecrypt::decrypt($data["meetingId"]));
		$topic       = $this->Topic->find("first", compact("conditions")); 
		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha eliminado el tema específico  ") . $topic["Topic"]["description"] . __(" de la reunión ") . $topic["Meeting"]["subject"];
		//$this->buildInfoLog($topic["Topic"]["id"], $description, NULL, NULL, NULL, NULL); 
	}

	private function __logAddTopicEditMeeting($topicId, $meetingId){
		$conditions  = array("Topic.id" => $topicId, "Topic.meeting_id" => $meetingId);
		$topic       = $this->Topic->find("first", compact("conditions")); 
		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha añadido el tema específico  ") . $topic["Topic"]["description"] . __(" en la reunión ") . $topic["Meeting"]["subject"];
		//$this->buildInfoLog($topic["Topic"]["id"], $description, NULL, NULL, NULL, NULL); 
	}

	private function __logBeforeEditTopicEditMeeting($topicId){
		$conditions  = array("Topic.id" => $topicId);
		$topic       = $this->Topic->find("first", compact("conditions"));
		$time        = $this->conversorSegundosHorasTopics($topic["Topic"]["time"]);
		$beforeEdit  = __(" Descripción: ") .  $topic["Topic"]["description"] .  __(" tiempo: ") .  $time; 
		return $beforeEdit; 
	}

	private function __logAfterEditTopicEditMeeting($topicId, $beforeEdit){
		$conditions  = array("Topic.id" => $topicId);
		$topic       = $this->Topic->find("first", compact("conditions"));
		$time        = $this->conversorSegundosHorasTopics($topic["Topic"]["time"]);
		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha editado el tema específico  ") . $topic["Topic"]["description"] . __(" en la reunión ") . $topic["Meeting"]["subject"];
		$afterEdit   = __(" Descripción: ") .  $topic["Topic"]["description"] .  __(" Tiempo: ") .  $time;
		//$this->buildInfoLog($topic["Topic"]["id"], $description, $beforeEdit, $afterEdit, NULL, NULL); 
	}
 	 
}
