<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'GoogleApi2', array('file' => 'GoogleApi2/settings.php'));
App::import('Vendor', 'OutLookApi1', array('file' => 'OutlookApiLogin/src/Outlook/Authorizer/Authenticator.php'));
 
class UsersController extends AppController {

	public $components = array('Paginator');

	public function beforeFilter() {
		
        $this->Auth->allow('add','logout','activar_cuenta','remember_password','remember_password_step_2','change_password','resetVariablesFirebase','request_active_account','change_language','register_user_from_suite','GoogleLoginResult','OutlookLoginResult');
	 	parent::beforeFilter(); 
    }
    public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {	
		$url = 'https://accounts.google.com/o/oauth2/token';	
		$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if($http_code != 200) 
			throw new Exception('Error : Failed to receieve access token');
		return $data;
	}

	private function validateUserSocial($info = array()){
		$this->request->data["User"] = $info;
		if (isset($info["email"])) {
			$user = $this->User->findByEmail($info["email"]);
			if (!empty($user)) {				
				if ($user["User"]["state"] == 1) {
					$this->Auth->login($user["User"]);
					$lang = empty(AuthComponent::user("lang")) ? "eng" : AuthComponent::user("lang");
	        		$this->Session->write('Config.language', $lang);
					return $this->redirect(array('action' => 'index_page', 'controller' => 'pages'));  
				}else{
					$this->loadModel("Plan");
					$conditions    = array("PlanUser.user_id" => $user["User"]["id"], 'Plan.id' => 1);
					$havePlanTrial = $this->Plan->PlanUser->find("first", compact("conditions"));
					if (empty($havePlanTrial)) {
						$this->activar_cuenta(base64_encode($user["User"]["email"]));
					}
				}
				
			}else{
				$this->request->data["User"]["hash_change_password"] = $this->User->generateHashChangePassword();
				$this->request->data["User"]["state"] 				 = Configure::read("DISABLED");
				unset($this->User->validate['img']);
				unset($this->User->validate['password']);
				$validations = $this->getValidates($this->request->data,'User');
				
				$this->User->set($this->request->data);
				if ($this->User->save($this->request->data)) {
					$this->activar_cuenta(base64_encode($this->request->data["User"]["email"]));
				}else{
					
					$this->Flash->fail(implode('<br>', $validations));
					$this->redirect(array("controller"=>"users","action"=>"login"));
				}
			}
		}

	}

	public function GetUserProfileInfo($access_token) {	
		unset($this->User->validate['img']);
		unset($this->User->validate['password']);
		$url = 'https://www.googleapis.com/plus/v1/people/me';			
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		if($http_code != 200) 
			throw new Exception('Error : Failed to get user information');
			
		return $data;
	}

    public function GoogleLoginResult(){
    	$this->layout = false;
    	$this->autoRender = false;
    	if(isset($_GET['code'])) {
			try {
				// Get the access token 
				$data = $this->GetAccessToken(CLIENT_ID, CLIENT_REDIRECT_URL,CLIENT_SECRET, $_GET['code']);
				// Get user information
				$user_info = $this->GetUserProfileInfo($data['access_token']);
				//echo '<pre>';print_r($user_info); echo '</pre>';
				 $user = array(
				 		"firstname"=> $user_info["name"]["givenName"],
				 		"lastname" => $user_info["name"]["familyName"],
				 		"email" => $user_info["emails"][0]["value"],
				 		"accepting_policies"=> 1,
				 		"accepting_policies"=> 1,
				 		"accepting_data_collection"=> 1,
				 );
				 $this->validateUserSocial($user);
			}
			catch(Exception $e) {
				$this->redirect("/users/login");
			}
		}
    }




    public function curl_file_get_contents($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

    public function OutlookLoginResult(){
    	$this->layout 	  = false;
    	$this->autoRender = false;
    	if(!isset($_GET["code"])) {
			$this->redirect("/users/login");
		}
		$auth_code = $_GET["code"];
		$urls 	   = 'https://login.live.com/oauth20_authorize.srf?client_id='.Configure::read("CLIENT_ID_OUT2").'&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri='.Configure::read("CLIENT_URL_OUT2")."&grant_type=authorization_code&code=".$auth_code;
		if($auth_code){
			$fields=array(
				'code'=>  urlencode($auth_code),
				'client_id'=>  urlencode(Configure::read("CLIENT_ID_OUT2")),
				'client_secret'=>  urlencode(Configure::read("CLIENT_SECRET_OUT2")),
				'redirect_uri'=>  urlencode(Configure::read("CLIENT_URL_OUT2")),
				'grant_type'=>  urlencode('authorization_code')
			);
			$post = '';
			foreach($fields as $key=>$value) { $post .= $key.'='.$value.'&'; }
			$post = rtrim($post,'&');
			$curl = curl_init();
			curl_setopt($curl,CURLOPT_URL,'https://login.live.com/oauth20_token.srf');
			curl_setopt($curl,CURLOPT_POST,5);
			curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
			$result = curl_exec($curl);
			curl_close($curl);
			$response =  json_decode($result);

			
		 	if(isset($response->access_token)) {
		 		// die();
				try {
					$accesstoken = $response->access_token;
					//$accesstoken = $_SESSION['accesstoken'] ;//= $_GET['access_token'];
					$get_profile_url='https://apis.live.net/v5.0/me?access_token='.$accesstoken;
					$xmlprofile_res= file_get_contents($get_profile_url);
					$profile_res = json_decode($xmlprofile_res, true);
					// Get the access token 
					$user = array(
						 		"firstname"=> $profile_res["first_name"],
						 		"lastname" => $profile_res["last_name"],
						 		"email" => $profile_res["emails"]["account"],
						 		"accepting_policies"=> 1,
						 		"accepting_policies"=> 1,
						 		"accepting_data_collection"=> 1,
			 					);
					$this->validateUserSocial($user);
				}
				catch(Exception $e) {
					$this->redirect("/users/login");
				}
			}else{

				$this->redirect($urls);
				$this->redirect("/users/login");
			}

		}

    }

    public function validate_session(){
    	$this->autoRender = false;
    	$this->outJSON(1);
    }

    public function register_user_from_suite(){
    	$this->allowOriginRequest();
		$this->RequestHandler->ext = 'json';
		$this->autoRender = false;
		if($this->request->is('post') && !empty($this->request->data)) { 
			try {
				$conditions = array('User.email'=>trim($this->request->data["email"]));
            	$id = $this->User->field('id', $conditions);
            	if(!empty($id)) {
            		return $this->outResponse(array('result' => false, 'code' => 1, 'user_id' => $id));
            	}
				$this->User->create();
				$saveData = array(
					"firstname" => $this->request->data["name"],
					"lastname"  => $this->request->data["last_name"],
					"email"     => $this->request->data["email"],
					"password"  => base64_decode($this->request->data["password"]),
					"accepting_policies"  		 => Configure::read("ENABLED"),
					"accepting_data_collection"  => Configure::read("ENABLED"),
					"state"  					 => Configure::read("DISABLED"),
					"step_initial"  		     => Configure::read("ENABLED")
				);
			 	if ($this->User->save($saveData, array('validate' => false))) { 
			 		$this->__createPlan($this->User->id, $this->request->data);
		 		 	$this->__sendNotificationToActivateAccount($this->User->id, $this->request->data["email"], $this->request->data["name"]); 
			 		return $this->outResponse(array('result' => true, 'code' => '', 'user_id' => $this->User->id));
			 	} else {
			 		return $this->outResponse(array('result' => false, 'code' => 2, 'user_id' => false));
			 	}
			} catch (Exception $e) {}
		} 
		return $this->outResponse(array('result' => false, 'code' => 3, 'user_id' => false));
	} 

	private function __createPlan($user_id, $data = array()){
		$recursive  = -1;
		$namePlan   = trim(strtolower($data["plan_name"]));
		$conditions = array("LOWER(Plan.name)" =>$namePlan);
		$plan       = $this->PlanUser->Plan->find("first", compact("conditions","recursive"));
		$this->PlanUser->savePlanDefaultSuite($user_id, $plan, $data["end_date"]); 
	}

    public function change_language($lang = null){
        if(in_array($lang, array('esp','eng'))) {
            $this->autoRender = false;
            $this->Session->write('Config.language', $lang);
            return $this->redirect($this->request->referer()); 
        } else {
            $this->Flash->fail(__('El idioma seleccionado no existe.')); 
            return $this->redirect($this->request->referer()); 
        }
    } 

	public function index() { 
		$conditions   = $this->User->buildConditions($this->request->query);  
		$conditions[] = array('User.id !=' => AuthComponent::user('id'));
		$this->paginate = array(
		        'conditions' => $conditions,
		        'limit' => 10
		    );
		$users = $this->Paginator->paginate(); 
		$this->set(compact('users'));
	}


	public function view($id = null) {
		$id 	 = EncryptDecrypt::decrypt($id);
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$user    = $this->User->find('first', $options);
		$this->set(compact('user'));
	}

	public function add(){
		return $this->redirect(array('controller' => 'users', 'action' => 'login'));
		die();
		if (AuthComponent::user('id')) {
			return $this->redirect(array('controller' => 'pages', 'action' => 'index_page'));
		}
		if ($this->request->is('post')) {
        	if(!isset($_POST["g-recaptcha-response"])){
            	$this->Flash->fail(__('Se debe validar el reCAPTCHA.'));
        	} else {
            	$responseCaptcha = $this->validateCaptcha($_POST["g-recaptcha-response"]);
				if (!$responseCaptcha->success) {
					$this->Flash->fail(__('Se debe validar el reCAPTCHA.'));
				} else {
					unset($this->User->validate['img']);  
					$validations = $this->getValidates($this->request->data,'User');
				 
					$nameUser    = $this->request->data['User']['firstname'].' '.$this->request->data['User']['lastname'];
	 				$this->User->create();
	 				if ($this->User->save($this->request->data)) { 
	 					$this->Session->write("RegisterInitial", "first");
	 					$this->__sendNotificationToActivateAccount($this->User->id, $this->request->data['User']['email'], $nameUser); 
						$this->Flash->success(__('Registro satisfactorio, revisa tu correo electrónico para proceder a activar tu cuenta.'));
						return $this->redirect(array('controller' => 'users','action' => 'login'));
	 				} else {
	 					$this->Flash->fail(implode('<br>', $validations));
	 				}				
				}
            }			
		}
		$this->layout = "website_users";
	}

	private function __sendNotificationToActivateAccount($userId, $correo, $nameUser){  
		$datos['User']['state'] = Configure::read("DISABLED");
		$datos['User']['id']    = $userId;
		$this->User->save($datos);
		$options = array(
			'to'       => $correo,
			'template' => 'activar_cuenta',
			'subject'  => __('Activar cuenta').' - '.Configure::read('Application.name'),
			'vars'     =>  array('nameUser'=>$nameUser,'correo' => $correo),
		);
		$this->sendMail($options);
		$this->loadModel("Coupon");
		$coupon = $this->Coupon->validateUserHaveOneCoupon($correo);
		if(!empty($coupon)){
			$options = array(
				'to'       => $correo,
				'template' => 'notification_coupon_available',
				'subject'  => __('Recordatorio de cupón disponible').' - '.Configure::read('Application.name'),
				'vars'     =>  array('nameUser'=> $nameUser,'correo'=> $correo, "couponInfo" => $coupon),
			); 
			$this->sendMail($options);
		}
	} 

	public function add_admin_user(){
		$this->validateRolAdministrador(); 
		if ($this->request->is('post')) {
			$this->User->set($this->data);
			if ($this->User->validates(array('fieldList' => array('firstname','lastname','telephone','email')))) {
				$has = $this->User->generateHashChangePassword();
				$this->request->data['User']['hash_change_password'] = $has;
				$this->__putRole();
				if ($this->User->save($this->request->data, array('validate'=>false))) {
					// $description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha creado al admnistrador ") . $this->request->data["User"]["firstname"] . ' ' . $this->request->data["User"]["lastname"];
					// $this->buildInfoLog($this->User->id, $description, NULL, NULL, NULL, NULL); 
	        		$this->__sendNotificationAdmin($this->request->data, $has);
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->fail(__('No se pudo ejecutar la acción, inténtalo nuevamente.'));
				}
			}
		}
	}

	private function __validateUsersAvailableInPlan(){	 
		$this->loadModel("PlanUser");
		$conditions  = array("PlanUser.user_id" => AuthComponent::user('bussines_user'));
		$plan_number = $this->PlanUser->find("count", compact('conditions'));
		$conditions  = array("PlanUser.user_id" => AuthComponent::user('bussines_user'), "PlanUser.state" => Configure::read("ENABLED"));
		$activePlan  = $this->PlanUser->find("first", compact('conditions'));
		if ($plan_number == 0) {
			return false;
		} else if (empty($activePlan)){
			return array("availables_users" => -1);
		} else {
			$conditions      = array("User.bussines_user" => AuthComponent::user('bussines_user'));
			$usersInBusiness = $this->User->find("count", compact("conditions"));
			$conditions = array("PlanUser.user_id" => AuthComponent::user('bussines_user'), "PlanUser.state" => Configure::read("ENABLED"));
			$numberUser = $this->PlanUser->field("number_users", $conditions);
			return array("availables_users" => $numberUser - $usersInBusiness); 
		} 
	}

	private function __putRole(){
		$this->request->data['User']['state']   = Configure::read('ENABLED');
		$this->request->data['User']['role_id'] = Configure::read('ADMIN_ROLE_ID'); 
	}
 
	private function __sendNotificationAdmin($data = array(), $has){
	    $correo   = $data['User']['email'];
		$nameUser = $data['User']['firstname'].' '.$data['User']['lastname'];
		$options  = array(
			'to'       => $correo,
			'template' => 'registro_admin',
			'subject'  => __('Activación de cuenta').' - '.Configure::read('Application.name'),
			'vars'     =>  array('nameUser'=>$nameUser,'has'=>$has),
		);
		$this->sendMail($options);
	}

	public function update_password(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		if (AuthComponent::user("password") != "") {			
	        $conditions       = array('User.id' => AuthComponent::user('id'));
	        $fields           = array('User.password');
	        $dbPassword       = $this->User->find('first',compact('fields','conditions'));
	        $passwordActual   = AuthComponent::password($this->request->data['contraActual']);
	        $contraNueva      = isset($this->request->data['contraNueva']) ? $this->request->data['contraNueva'] : null;
	        $contraconfirmar  = isset($this->request->data['contraconfirmar']) ? $this->request->data['contraconfirmar'] : null;
	 		$passwordInfo     = array("passwordActual" => $passwordActual, "contraNueva" => $contraNueva, "contraconfirmar" => $contraconfirmar);
	 		$response         = $this->__validateNewPassword($dbPassword, $passwordInfo);  	
		}else{

			$contraNueva      = isset($this->request->data['contraNueva']) ? $this->request->data['contraNueva'] : null;
	        $contraconfirmar  = isset($this->request->data['contraconfirmar']) ? $this->request->data['contraconfirmar'] : null;

	        if ($contraNueva != $contraconfirmar) {
	        	$response = array("message" => __('Las nuevas contraseñas no coinciden.'), "state" => false);
	        }else{
	        	$this->request->data['User']['password'] = $contraNueva;
    			$this->request->data['User']['id'] 		 = AuthComponent::user('id');
    			if ($this->User->save($this->request->data)) { 
    				$response = array("message" => __('El cambio de contraseña ha sido un éxito.'), "state" => true);					 
				} 
	        }

		}
 		$this->outJSON($response); 
	}

	private function __validateNewPassword($dbPassword, $passwordInfo = array()){  
		$response = array();
		if ($dbPassword['User']['password'] == $passwordInfo["passwordActual"]) {
    		if ($passwordInfo["contraNueva"] == $passwordInfo["contraconfirmar"]) {
    			if(strlen($passwordInfo["contraNueva"]) < 8 || strlen($passwordInfo["contraconfirmar"]) < 8 ){
    				$response = array("message" => __('La contraseña nueva y la confirmación de la contraseña debe contener mínimo 8 dígitos.'), "state" => false);					 
    			} else {
        			$this->request->data['User']['password'] = $passwordInfo["contraNueva"];
        			$this->request->data['User']['id'] = AuthComponent::user('id');
        			if ($this->User->save($this->request->data)) { 
        				$response = array("message" => __('El cambio de contraseña ha sido un éxito.'), "state" => true);					 
					}    				
    			}
        	} else { 
        		$response = array("message" => __('Las nuevas contraseñas no coinciden.'), "state" => false); 
        	}
        } else { 
        	$response = array("message" => __('La contraseña actual no coincide con la que tienes guardada en la base de datos.'), "state" => false);         
        }
        return $response;
	}

	public function edit_user($id){
		$id = EncryptDecrypt::decrypt($id);
		$this->validateRolEmployee();
		$this->validateRolAdministrador(); 
		$user = $this->User->findById($id);
    	if (!$user) {
        	$this->showMessageExceptions();
    	} 
		$beforeEdit  = __("Nombre: " ) .  $user["User"]["firstname"] . __(" Apellido: ") . $user["User"]["lastname"] . __(" Teléfono: ") . $user["User"]["telephone"];
    	if ($this->request->is('post') || $this->request->is('put')) { 		 
			$this->User->id = $id; 
			$this->User->set($this->data);
			if ($this->User->validates(array('fieldList' => array('firstname','lastname','telephone')))) {
				$response = $this->User->checkUserIsAdmin($id);
				if ($response == true) { 
					$this->__editInfoUser($id, $this->data, $beforeEdit);				
				} else {
					$this->showMessageExceptions();
				}				
			} 
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->request->data = $this->User->find('first', $options);
	}

	private function __editInfoUser($id, $data = array(), $beforeEdit){  	 
		if ($this->User->save($data)) {  
			$afterEdit   = __("Nombre: " ) .  $data["User"]["firstname"] . __(" Apellido: ") . $data["User"]["lastname"] . __(" Teléfono: ") . $data["User"]["telephone"]; 
			$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha editado al  usuario ") . $data["User"]["firstname"] . ' ' . $data["User"]["lastname"];
			//$this->buildInfoLog($id, $description, $beforeEdit, $afterEdit, NULL, NULL); 
			$this->Flash->success(__('Se ha editado el usuario correctamente.'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Flash->fail(__('No se pudo ejecutar la acción, inténtalo nuevamente.'));
		} 
	} 

	public function edit() { 
		// if(AuthComponent::user("role_id") != Configure::read("ADMIN_ROLE_ID")){
  //           $this->layout = "default_v2";
  //       }
		if ($this->request->is(array('post', 'put'))) { 
			if(isset($this->request->data['User']['email'])){
				unset($this->request->data['User']['email']);
			} 
			$this->User->id = AuthComponent::user('id');
			$this->request->data["User"]["id"] = AuthComponent::user('id');
			if (empty($this->request->data["User"]["telephone"])) {
				unset($this->request->data["User"]["telephone"]);
			}
			if ($this->User->save($this->request->data)) { 
				$this->User->updateSession($this->request->data, $this->Session);
				$this->Session->write('Config.language', $this->request->data["User"]["lang"]);
				$this->Flash->success(__('Se han guardado los datos correctamente.'));
			} else {
	    		$this->Flash->fail(__('No se pudo ejecutar la acción, inténtalo nuevamente.'));
			} 
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => AuthComponent::user('id')));
			$this->request->data = $this->User->find('first', $options);
		}
	}
 

	public function login() {


		$this->layout = "website";
		if (AuthComponent::user('id')) {
			return $this->redirect(array('controller' => 'pages', 'action' => 'index_page'));
		}
	    if ($this->request->is('post')) {
        	$this->__validateActiveState();
	        if ($this->Auth->login()) { 
	        	$this->__isActive(); 
	        	$firstLogin = $this->Session->read("RegisterInitial");
	        	$lang = empty(AuthComponent::user("lang")) ? "eng" : AuthComponent::user("lang");

	        	$this->Session->write('Config.language', $lang);
	        	if($firstLogin == "first"){
	          		return $this->redirect(array('action' => 'index_page', 'controller' => 'pages'));                 
	        		return $this->redirect(array('action' => 'planning_management', 'controller' => 'plans')); 
	        	} else {
	          		return $this->redirect(array('action' => 'index_page', 'controller' => 'pages'));                 
	        	}
	        } else {
        		$this->Flash->fail(__('Correo electrónico y/o contraseña son incorrectos.'));
	        }
	    }
	    $this->layout = "website_users";
	}

	private function __isActive() {
		if(AuthComponent::user('state') == Configure::read("DISABLED")){
			$this->logout_enabled(AuthComponent::user());
		} else if(AuthComponent::user('state') == Configure::read("DISABLED_MANUAL")) {
			$this->Flash->fail(__('Tu cuenta está inactiva.'));
	   		return $this->redirect($this->Auth->logout());
		}
	} 

	public function activar_cuenta($email, $assigned = null){ 
        $this->autoRender = false;
        $this->validateSession();
		$email        = base64_decode($email);
		$conditions   = array("User.email" => $email);
		$user         = $this->User->find("first", compact("conditions"));
    	$hashPassword = $user["User"]["hash_change_password"];
		$password     = $user["User"]["password"]; 
		if ($user["User"]["state"] == Configure::read('DISABLED')) {
			$this->User->updateAll(
	            array('User.state' => Configure::read('ENABLED')),
	            array('User.email' => $email)
	        );
	        if(is_null($assigned)){
				$this->Flash->success(__('Tu cuenta ha sido activada.')); 
				if ($this->Auth->login($user["User"])) {
					$lang = empty(AuthComponent::user("lang")) ? "eng" : AuthComponent::user("lang");
	        		$this->Session->write('Config.language', $lang);
					$this->redirect(array('controller'=>'contacs','action' => 'index')); 
					// $this->redirect(array('controller'=>'plans','action' => 'buy_plan_trial',EncryptDecrypt::encrypt(1))); 
				}
			} else {
				if(!empty($hashPassword) && $password == "") { 
					$this->Flash->info(__('Para completar el registro ingresa la siguiente información para entrar a tu cuenta.'));
					return $this->redirect(array('action'=>'remember_password_step_2', $hashPassword));
				}
			}
		} else {			
	        if(!is_null($assigned)){ 
				if(!empty($hashPassword) && $password == "") { 
					$this->Flash->info(__('Para completar el registro ingresa la siguiente información para entrar a tu cuenta.'));
					return $this->redirect(array('action'=>'remember_password_step_2', $hashPassword));
				}
	        } 
			$this->Flash->success(__('Tu cuenta ya está activada.'));
			$this->redirect(array('controller'=>'users','action' => 'login'));
		}
		$this->layout = "website_users";
	}

	public function logout_enabled($user = array()) {  
		$hashPassword = !empty($user["hash_change_password"]) ? $user["hash_change_password"]: NULL;
		$password     = !empty($user["password"]) ? $user["password"]: NULL; 
		if(!empty($hashPassword) && $password == "") {
			$this->Auth->logout();
			$this->Flash->fail(__('Tu cuenta está inactiva. Para continuar, por favor ingrese una contraseña.'));
			return $this->redirect(array('action'=>'remember_password_step_2', $hashPassword));
		} else { 
			$this->Flash->fail(__('Tu cuenta está inactiva.')  . ' <a href="'.Router::url(array('controller'=>'users','action'=>'request_active_account', EncryptDecrypt::encrypt($user["id"]))). '"> ' . __("Solicitar activación de cuenta") .' </a> ');
	   		return $this->redirect($this->Auth->logout());
		}
	}

	public function logout($flash = null) {
		if (AuthComponent::user("id")) {			
			$user_id = AuthComponent::user("id");
			Cache::delete("permissions_{$user_id}", 'PERMISSIONS'); 
		 	Cache::delete("stepUser_{$user_id}", 'STEPUSER'); 
		}
		if(isset($_COOKIE["time"])){
    		setcookie('time', "" , time() + (86400 * 30), '/'); // 86400 = 1 day 
    	}  
    	$this->__deleteSessions(); 
    	if (!is_null($flash)) {
    		$this->Flash->fail(__('Sesión cerrada inesperadamente.'));
    	}
	    return $this->redirect($this->Auth->logout());
	}

	private function __deleteSessions(){  
		$this->Session->delete("Auth"); 
		$this->Session->delete("codFirebase"); 
		$this->Session->delete("contactId"); 
		$this->Session->delete("passwordContac"); 
		$this->Session->delete("passwordEnter"); 
		$this->Session->delete("stateContac"); 
		$this->Session->delete("identity_session"); 
    	$this->Session->delete("RegisterInitial");
    	$this->Session->delete("TEAM");
	}

	public function remember_password(){
		if (AuthComponent::user("id")) {
			return $this->redirect(array('controller' => 'pages', 'action' => 'index_page'));
		}
		if ($this->request->is('post')) {
			$user = $this->User->findByEmail($this->request->data['User']['email']);
			if(empty($this->request->data['User']['email'])){
				$this->Flash->fail(__('El correo electrónico es requerido.')); 
			} else {
				if (empty($user)) {
					$this->Flash->fail(__('El correo electrónico que ingresaste no se encuentra en nuestra base de datos.')); 
				} else {
					$hash = $this->User->generateHashChangePassword();
					$data = array('User' => array('id' => $user['User']['id'],'hash_change_password' => $hash));
					$this->User->save($data);
					$options = array(
						'to'       => $user['User']['email'],
						'template' => 'remember_password',
						'subject'  => __('Restablecer contraseña').' - '.Configure::read('Application.name'),
						'vars'     =>  array('hash' => $hash),
					);
					$this->sendMail($options);
					$this->Flash->success(__('Ingresa al correo electrónico y sigue las instrucciones del correo electrónico que te enviamos.'));
					$this->redirect(array('action' => 'login'));
				} 
			}
		}
		$this->layout = "website_users";
	}

	public function remember_password_step_2($hash = null, $restorePassword = null) {
		$this->validateSession();
        $user = $this->User->findByHashChangePassword($hash);
        if (empty($user)) {
            $this->Flash->fail(__('La URL ha caducado, por favor inténtalo nuevamente.'));
            $this->redirect(array('controller'=>'users','action' => 'remember_password'));
        }
        $this->set(compact('hash','restorePassword'));
         $this->layout = "website_users";	    
        $this->render('/Users/change_password');
       
	}

	public function change_password($hash = null, $restorePassword = null){
		$this->validateSession();
		
		if ($this->request->is('post')) {
			if(!isset($restorePassword)){
				$guardar = $this->getValidates($this->request->data,'User');

				if ($this->User->validates(array('fieldList' => array('password','confirm_password','firstname','lastname','accepting_policies','accepting_data_collection')))) { 
					$user = $this->User->findByHashChangePassword($this->request->data['User']['hash']);

					if ($this->request->data['User']['password'] === $this->request->data['User']['confirm_password']) {
	                   	$this->__updateFieldsUser($user);
	                   	$this->request->data['User']['state'] = configure::read("ENABLED");
	                    if ($this->User->save($this->request->data)) {
	                    	$user = $this->User->findById($this->request->data['User']['id']);
	                    	$this->Flash->success(__('La contraseña ha sido guardada.'));
	                    	if ($this->Auth->login($user["User"])) {
	                    		return $this->redirect(array('controller'=>'contacs','action' => 'index'));
	                    		// return $this->redirect(array('controller'=>'plans','action' => 'buy_plan_trial',EncryptDecrypt::encrypt(1)));
	                    		// return $this->redirect(array('controller'=>'users','action' => 'login'));
	                    	}
	                    }
	                } else {
		                $this->Flash->fail(__('Las contraseñas no coinciden, por favor verifica.'));
		 			}
				} else {
					$errors  = $this->User->validationErrors; 
					$message = implode('<br>', array_map('implode', $errors)); 
		   		 	$this->Flash->fail($message);
				}					 
			} else {
				$guardar = $this->getValidates($this->request->data,'User');
				if ($this->User->validates(array('fieldList' => array('password','confirm_password')))) { 
					$user = $this->User->findByHashChangePassword($this->request->data['User']['hash']);
					if ($this->request->data['User']['password'] === $this->request->data['User']['confirm_password']) {
	                   	$this->__updateFieldsUser($user);
	                    if ($this->User->save($this->request->data)) {
	                    	$this->Flash->success(__('La contraseña ha sido guardada.'));
	                    	return $this->redirect(array('controller'=>'users','action' => 'login'));
	                    }
	                } else {
		                $this->Flash->fail(__('Las contraseñas no coinciden, por favor verifica.'));
		 			}
				} else {
					$errors  = $this->User->validationErrors; 
					$message = implode('<br>', array_map('implode', $errors)); 
		   		 	$this->Flash->fail($message);
				}				
			}
		}
		$this->set(compact('hash','restorePassword'));
		$this->layout = "website_users";
	}

	private function __updateFieldsUser($user = array()){
		if($user["User"]["state"] == configure::read("DISABLED")) {
			$this->request->data["User"]["state"] = configure::read("ENABLED");
		}
	    $this->request->data['User']['id'] = $user['User']['id'];
        $this->request->data['User']['hash_change_password'] = ''; 
	}

	private function __validateTypeFileToUser($data) { 
		$response = array("state" => true);
		return $response; 	
	}

	public function create_data_form_coupon(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$userId 	  	  = isset($this->request->data["userId"])   ? EncryptDecrypt::decrypt($this->request->data["userId"]) : null; 
		$recursive        = -1;
		$conditions       = array("User.id" => $userId);
		$user             = $this->User->find("first", compact("conditions","recursive"));
		$formDataCoupon = array(
			"name"      => $user["User"]["firstname"] . ' ' . $user["User"]["lastname"],
			"email"     => $user["User"]["email"],
			"telephone" => $user["User"]["telephone"] 
		);
		$this->Session->write("CouponForm", $formDataCoupon);
		$response = true;
		$this->outJSON($response); 
	} 

	private function __validateActiveState() {
		$conditions = array("User.email" => $this->request->data["User"]["email"]);
		$recursive  = -1;
		$user 	    = $this->User->find("first", compact("conditions","recursive"));
		if(!empty($user)){
			$this->loadModel('UserTeam');
			$conditions = array(
				'UserTeam.user_id' => $user['User']['id'],
				'UserTeam.team_id' => Configure::read('Application.team_id')
			);
			$userTeam = $this->UserTeam->find('first',compact('conditions','recursive'));
			if(!empty($userTeam)) {
				if($userTeam['UserTeam']['user_state'] == Configure::read('ENABLED')){
					$this->Flash->fail(__('Tu cuenta está inactiva.'));
				 	return $this->redirect($this->Auth->logout());
				}
			} 
			if($user["User"]["state"] == Configure::read('DISABLED')) {
				$this->Flash->fail(__('Tu cuenta está inactiva.')  . ' <a href="'.Router::url(array('controller'=>'users','action'=>'request_active_account', EncryptDecrypt::encrypt($user["User"]["id"]))). '"> ' . __("Solicitar activación de cuenta") .' </a> ');
				return $this->redirect($this->Auth->logout());
			}
			$hashPassword = $user["User"]["hash_change_password"];
			$password     = $user["User"]["password"]; 
			if(!empty($hashPassword) && $password == "" && $user["User"]["firstname"] == "") {
				$this->Auth->logout();
				$this->Flash->fail('Para continuar, por favor complete la siguiente información.');
				return $this->redirect(array('action'=>'remember_password_step_2', $hashPassword));
			}
		}
	}

	public function accept_privacity(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		$cookieAccept 	  = "cookieAccept".AuthComponent::user("id");
	 	if(!isset($_COOKIE[$cookieAccept])) { 
            setcookie($cookieAccept, true, time() + (86400 * 30), '/', NULL, true, true); 
        } 
	}

	public function request_active_account($id){
		$this->autoRender = false;
		$id         = EncryptDecrypt::decrypt($id);
		$conditions = array("User.id" => $id);
		$user = $this->User->find("first", compact("conditions"));
		$options = array(
			'to'       => $user["User"]["email"],
			'template' => 'activar_cuenta',
			'subject'  => __('Activar cuenta').' - '.Configure::read('Application.name'),
			'vars'     =>  array('nameUser' =>$user["User"]["firstname"] . ' ' . $user["User"]["lastname"], 'correo' => $user["User"]["email"], 'request' => true),
		);
		$this->sendMail($options);
		$this->Flash->success(__('Se ha enviado a tu correo electrónico la solicitud de activar cuenta.'));
	    return $this->redirect(array('action'=>'login','controller' => 'users'));
	}

	public function verify_img(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax';
		$this->autoRender = false; 
		$response 		  = $this->User->validateMimeTypeImg($this->request->data["User"]);
	 	$this->outJSON($response); 
	}

	public function login_ennovart(){}

	public function login_to_flex_clients(){}
}
