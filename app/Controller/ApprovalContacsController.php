<?php
App::uses('AppController', 'Controller');
 
class ApprovalContacsController extends AppController {

	public $components = array('Paginator');

	public function beforeFilter() { 
		$this->Auth->allow('reminder_approval_contac_today_firebase');
		parent::beforeFilter(); 
 	}

 	public function reminder_approval_contac_today_firebase(){
 		set_time_limit(300000);
 		$this->ApprovalContac->unbindModel(array('belongsTo' => array('Client','Creator')));
 		$this->autoRender = false;
 		$group 	    = array("ApprovalContac.user_id");
	 	$fields     = array("ApprovalContac.user_id","COUNT(ApprovalContac.id) as totalApprovals","User.lang"); 
 		$conditions = array( 
			"ApprovalContac.state" 	    => Configure::read("DISABLED"),
			"ApprovalContac.user_id !=" => NULL,
			"ApprovalContac.limit_date" => date('Y-m-d'),
			"OR" => array(
				array("Contac.in_approval" => Configure::read("ENABLED")),  
				array("Contac.in_approval" => Configure::read("NEW_APPROVAL"))
			)
		); 
		$approvalContacs  = $this->ApprovalContac->find("all", compact("conditions","group","fields"));
		if(!empty($approvalContacs)){
			foreach ($approvalContacs as $approvalContac) {
				if($approvalContac["User"]["lang"] == NULL || $approvalContac["User"]["lang"] == "" || empty($approvalContac["User"]["lang"])){
					$approvalContac["User"]["lang"] = "esp";
				} 				 
				if($approvalContac["User"]["lang"] == "esp"){
					$texto = sprintf(__("Tienes %s acta(s) pendientes por aprobar el día de hoy."), $approvalContac[0]['totalApprovals']);
				} else {
					$texto = sprintf(__("You have %s minutes pending approval today."), $approvalContac[0]['totalApprovals']); 
				}
				$url    = "contacs/approval_contacs/";
				$msgEng = sprintf(__("You have %s minutes pending approval today."), $approvalContac[0]['totalApprovals']); 
		 		$this->notificacionesUsuarios($approvalContac['ApprovalContac']['user_id'], $texto, $msgEng, $url);
			}
		} 
 	}

 	public function users_to_approved(){
 		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
 		$this->layout 	  = "ajax";
 		$this->autoRender = false;
 		$recursive    = -1;
 		$contacId 	  = isset($this->request->data["contacId"]) ? EncryptDecrypt::decrypt($this->request->data["contacId"]): NULL;
 		$conditions   = array("ApprovalContac.contac_id" => $contacId);
 		$approvals    = $this->ApprovalContac->find("all", compact("conditions","recursive"));
 		$users  	  = array();
 		foreach ($approvals as $approval) {
 			$users[] = $approval["ApprovalContac"]["user_id"];
 		}
 		$this->outJSON($users);  
 	}

 
}
