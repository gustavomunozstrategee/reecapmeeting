<?php 

App::uses('Controller', 'Controller');
 
class AppController extends Controller {

    public $components = array('Flash','Session','Auth','Cookie','RequestHandler');
    public $helpers    = array('Utilities','Js','Time');

    public function beforeFilter() { 
        // Para revisar responsive desde el navegador en dispositivos móviles descomentar este código
        // $this->loadModel('User');
        // $recursive  = -1;
        // $conditions = array('User.email' => 'yeisonmejia@strategee.us');
        // $conditions = array('User.email' => 'nataliamunera@strategee.us');
        // $conditions = array('User.email' => 'it@strategee.us');
        // $user       = $this->User->find('first', compact('conditions')); 
        // $user['User']['password'] = 'Mejia1996'; 
        // $user['User']['password'] = '12345678'; 
        // $this->Auth->login($user['User']);
        // FIN - responsive desde el navegador en dispositivos móviles
        if(in_array($this->request->controller, Configure::read('APIS_CONTROLLERS'))) {
            $this->Auth->allow($this->request->action);
            $this->RequestHandler->ext = 'json';

        } else {
            $this->__setHeadersSecurity();  
            App::uses('EncryptDecrypt', 'Vendor'); 
            $this->Auth->authenticate   = array('Form');
            $this->Auth->authenticate   = array('Form' => array('fields' => array('username' => 'email', 'password' => 'password')));
            $this->Auth->loginRedirect  = array('controller' => 'pages', 'action' => 'index_page');
            $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
            $user_id                    = Configure::read('DISABLED');    
            $sessionId                  = $this->Session->id();
            $this->__deleteSessionsStore();
            // $this->showTransactionsPending();
            if(AuthComponent::user('id')) {
                $user_id = AuthComponent::user('id');
                // $this->__notifyUserAdquiredPlan();    
            }  
            $this->__savePermissions(); 
            $permissions = Cache::read("permissions_{$user_id}", 'PERMISSIONS');

            $tasks       = $this->taskPending();  
            $this->__validatePlanExpired(); 
            $this->__validateRequestUrl();
            $this->__setTeamId();
            $this->set(compact('sessionId','user_id','permissions','tasks')); 
        }
    }

    private function __setTeamId(){
        if (isset($this->request->query["team_selection"])) {
            $this->Session->write("TEAM",$this->request->query["team_selection"]);
            $this->loadmodel("Team");
            $this->Session->write("TEAM_NAME",$this->Team->field("name",array("Team.id" => EncryptDecrypt::decrypt($this->request->query["team_selection"]))));
        }
    }

    private function __setHeadersSecurity(){ 
        $this->header('X-Frame-Options: SAMEORIGIN'); 
        $this->header('X-Xss-Protection: 1; mode=block'); 
        $this->header('X-Content-Type-Options: nosniff');
        $this->header('Strict-Transport-Security: max-age=31536000; includeSubDomains'); 
        $this->header("Content-Security-Policy: frame-src *;");
        $this->header("Content-Encoding: compress"); 
        $this->header("Expect-CT: enforce, max-age=30, enforce");
    }

    private function __validateRequestUrl(){
        if($this->request->action == "register_user_from_suite"){
            $this->RequestHandler->ext = 'json';
        } 
        if($this->request->action == "home") {
            if(AuthComponent::user("id")) {
                $this->redirect(array('action' => 'index_page','controller' => 'pages')); 
            }
        }         
    }

    public function beforeRender() { 

        $referer    = $this->request->referer();
        $stepApp    = $this->getCacheStep(); 
        if($stepApp == configure::read("ENABLED")){
            $tasks  = $this->taskPending();
            if(empty($tasks)) {                
                $this->deleteCacheStep();
                if($referer != FULL_BASE_URL.$this->request->base."/contacs/add") {
                    $this->redirect(array("controller" => "contacs", "action" => "add"));
                }
            }
        } 
        if(AuthComponent::user('id') && ($referer == FULL_BASE_URL.$this->request->base."/users/login")) {
            $this->loadModel('UsersSession');
            $row = $this->__sesionValidate(AuthComponent::user('id'), $this->Session->id());
            if ($row['UsersSession']['row'] >= 1) {
                $this->UsersSession->saveFirebase($row['UsersSession']['id']);
                $this->__deleteSesiones($row['UsersSession']['id']);
                $this->UsersSession->updateUserIdOnSession(AuthComponent::user('id'), $this->Session->id());
                $this->UsersSession->updateFirebase($row['UsersSession']['id']);
                $this->Flash->success(__('Hemos cerrado las sesiones que tenías abiertas en otros ordenadores.'));
            }else{
                $this->UsersSession->updateUserIdOnSession(AuthComponent::user('id'), $this->Session->id());    
            }
        }
         
    } 

    private function __sesionValidate($user_id, $sesion_id){
        $conditions = array('UsersSession.user_id' => $user_id, 'UsersSession.id !=' => $sesion_id);
        $datos      = $this->UsersSession->find('first', compact('conditions'));
        $existe     = $this->UsersSession->find('count', compact('conditions'));
        $datos['UsersSession']['row'] = $existe;
        return $datos;
    }

    private function __deleteSesiones($id){
        $this->UsersSession->id = $id;
        $this->UsersSession->delete();
        return true;
    }

    public function deleteImagen($directorio,$imagen){
        unlink($directorio.$imagen);
    }

    public function validateSession(){
        if (AuthComponent::user('id')) {
            $this->Flash->fail(__('Finalizamos la sesión que estaba abierta, vuelve a ingresar al correo electrónico y sigue nuevamente las instrucciones.'));
            return $this->redirect($this->Auth->logout());
        }
    }

    public function notificacionesEmpresa($model, $name){
        $this->loadModel('UserTeam');
        $conditions = array("UserTeam.user_id" => AuthComponent::user('id'));
        $ownerTeam  = $this->UserTeam->find("first", compact("conditions"));         
        if ($ownerTeam["OwnUser"]["id"] != AuthComponent::user('id')) {
            $nodoPrincipal        = 'NOT'.uniqid(); 
            $datos['mensaje']     = __('El funcionario con el correo electrónico').' '.AuthComponent::user('email').' '.__('ha creado un').' '.$model.' '.__('con el nombre').' '.$name.".";
            $datos['mensaje_eng'] = __('The employee with the Email').' '.AuthComponent::user('email').' '.__('you’ve created').' '.$model.' '.__('with the name').' '.$name.".";
            $datos['nodo_id']     = $nodoPrincipal;
            $datos['state']       = Configure::read('ENABLED');
            $this->UserTeam->User->saveFirebaseNotificaciones($ownerTeam["OwnUser"]["id"], $nodoPrincipal, $datos);
        } 
    }
 

    public function validatePlanAvaliable(){
        $this->loadModel("PlanUser");
        $conditions = array("PlanUser.user_id" => AuthComponent::user('bussines_user'));
        $plan = $this->PlanUser->find("count", compact('conditions'));
        if ($plan > 0) {
            $conditions = array("PlanUser.user_id" => AuthComponent::user('bussines_user'), "PlanUser.state" => Configure::read("ENABLED"));
            $data = $this->PlanUser->find("count", compact('conditions'));
        } else {
            $data = -1;
        }
        return $data;
    }

    public function notificacionesUsuarios($user_id, $texto, $msgEng, $url = null){
        $this->loadModel('User'); 
        $nodoPrincipal        = 'NOT'.uniqid();
        $datos['mensaje']     = $texto;
        $datos['mensaje_eng'] = $msgEng;
        $datos['nodo_id']     = $nodoPrincipal;
        $datos['type']        = $url;
        $datos['state']       = Configure::read('ENABLED'); 
        $this->User->saveFirebaseNotificaciones($user_id, $nodoPrincipal, $datos);
    }
 
    public function validateRolAdministrador(){
        if (AuthComponent::user('role_id') != Configure::read('ADMIN_ROLE_ID')){
            $this->Flash->fail(__('No tienes permiso de ingresar a esa vista.'));
            $this->redirect(array('action' => 'index_page','controller' => 'pages'));
        }
    } 

    public function dataEnabled($model){
        $this->loadModel($model);
        $recursive  = -1;
        $conditions = array($model.'.state' => Configure::read('ENABLED'));
        $datos = $this->$model->find('all',compact('conditions','recursive')); 
        return $datos;
    }
  
    public function resetVariablesFirebase(){
        if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax';
        $this->autoRender = false;
        $this->resetVariablesContac();
    }

    public function resetVariablesContac($view = null){ 
        $this->Session->write('codFirebase',Configure::read('COD_ACTA_INIT'));
        if (is_null($view)) {
            $this->Session->write('contactId',Configure::read('ID_ACTA_INIT'));            
        }
        $this->Session->write('passwordContac',Configure::read('DISABLED'));
        $this->Session->write('passwordEnter',Configure::read('DISABLED'));
        $this->Session->write('stateContac',Configure::read('DISABLED'));
        $this->Session->write('identity_session', 'id'.uniqid());
    }

    public function outJSON($value = null) {
        echo json_encode($value);
    }

    public function sendMail($options = array()) {
        try{
            $email = new CakeEmail();
            $email->template($options['template'], 'default')
                ->config('smtp')
                ->emailFormat('html')
                ->subject($options['subject'])
                ->to($options['to'])
                ->from(Configure::read('Email.from_email'))
                ->viewVars($options['vars'])
                ->send();
        } catch (Exception $e) {
        	$this->log(__CLASS__.':'.__FUNCTION__.':'.__LINE__." e",'debug');
            $this->log($e->getMessage(),'debug');
        }
        return true;
    }

    public function sendMailWithCcAndBcc($options = array()) {
        try{
            $email = new CakeEmail();
            $email->template($options['template'], 'default')
                ->config('smtp')
                ->emailFormat('html')
                ->subject($options['subject'])
                ->from(Configure::read('Email.from_email'))
                ->viewVars($options['vars']);
                
            if(!empty($options['to'])) {
                $email->to($options['to']); 
            }
            if(!empty($options['cc'])) {
                $email->cc($options['cc']); 
            }
            if(!empty($options['bcc'])) {
                $email->bcc($options['bcc']);  
            }
            $email->send();
        } catch (Exception $e) {
        	$this->log($e->getMessage(),'debug');
        }
        return true;
    }

    public function getValidates($arrayDatos, $model){
        $this->loadModel($model);
        $this->$model->set($arrayDatos); 
        if ($this->$model->validates()) {
            if($model == "Contac"){
                return $this->__validateCommitments($arrayDatos);
            }
            return true;
        } else {
            $validationErrors = $this->$model->validationErrors;
            $this->$model->validationErrors = array();
            foreach ($validationErrors as $key => $value) {
                $errors[] = $value[0];
            }
            return $errors;
        }
    }

    public function __validateCommitments($data = array()){
        unset($data["Commitments"]["NUEVO"]);
        if(empty($data["Commitments"])){ 
            return true;
        } else {
            $totalDate        = 0; 
            $totalDescription = 0;
            foreach ($data["Commitments"] as $commitment) {
                if(empty($commitment["fecha"])) {
                    $totalDate++;
                }
                if (!strlen(trim($commitment['description']))) {
                    $totalDescription++;
                } 
            } 
            if($totalDate > 0) {
                $response = array(__("Aún se debe definir la fecha límite del compromiso."));
            } else if($totalDescription > 0) {
                $response = array(__("Aún se debe definir la descripción del compromiso."));
            } else {
                $status = $this->Contac->checkWordCharactersCommitments($data["Commitments"]); 
                if($status == false){
                    $response = array(__("Cada palabra de la descripción de los compromisos debe tener máximo 80 caracteres."));
                } else {  
                    return true; 
                }
            }
            return $response; 
        }
    }
    
    public function changeState(){ 
        if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        } 
        $this->autoRender = false;    
        $this->request->data['id'] = EncryptDecrypt::decrypt($this->request->data['id']); 
        $model  = $this->modelClass;
        $this->$model->recursive = -1;
        $item   = $this->$model->findById($this->request->data['id']); 
        if($model == "User"){
            $saveData = array("{$model}"=>array(
                'id'    => $this->request->data['id'],
                'state' => $item[$model]["state"] == Configure::read('ENABLED') ? Configure::read('DISABLED_MANUAL') : Configure::read('ENABLED'),
            )); 
        } else {            
            $saveData = array("{$model}"=>array(
                'id'    => $this->request->data['id'],
                'state' => $item[$model]["state"] == Configure::read('ENABLED') ? Configure::read('DISABLED') : Configure::read('ENABLED'),
            )); 
        }
        if($this->$model->save($saveData, array('validate'=>false))) {
            $this->Flash->success(__('El cambio de estado se ha realizado correctamente.'));
            // $this->__saveDataLogDisabledEnabled($saveData, $this->request->data['id'], $model);
        } else {
            $this->Flash->fail(__('El cambio de estado no fue realizado.'));
        } 
    }
 

    private function __saveDataLogDisabledEnabled($saveData, $id, $model){ 
        if($model == "User"){
            $name = $this->$model->field("firstname") . ' ' . $this->$model->field("lastname");  
        } else {
            $name = $this->$model->field("name");             
        }
        if($saveData[$model]["state"] == Configure::read('ENABLED')){
            $description = __("El usuario ") .AuthComponent::user("firstname"). ' ' . AuthComponent::user("lastname") . __(" ha habilitado el registro ") . $name;
        } else {
            $description = __("El usuario ") .AuthComponent::user("firstname"). ' ' . AuthComponent::user("lastname") . __(" ha deshabilitado el registro ") . $name;
        }
        $this->buildInfoLog($id, $description, NULL, NULL, NULL, NULL); 
    }

    public function showMessageExceptions(){
        $this->Flash->fail(__('Página no encontrada.'));
        return $this->redirect(array('controller' => 'contacs','action' => 'index'));
    }

    public function listExtensions(){
        $extensions = array( 
            '1'=> array(".jpg", ".png",".jpeg",'.PNG','.JPG','.JPEG')  //RADIO
        ); 
        return $extensions;
    }  

    public function showTransactionsPending(){
        $userId                 = AuthComponent::user('id');
        $lastTransactionPending = Cache::read("transactions_{$userId}", 'TRANSACTION_PENDINGS'); 
        if (!$lastTransactionPending) {
            $this->loadmodel('Transaction');
            $limit                  = 1;
            $order                  = array('Transaction.id DESC');
            $recursive              = -1;
            $conditions             = array(
                'Transaction.user_id' => AuthComponent::user('id'), 
                'Transaction.state'   => configure::read('TRANSACTION_PENDING'),
                'Transaction.notify'  => configure::read('ENABLED')
            );
            $lastTransactionPending = $this->Transaction->find('first', compact('conditions','recursive','order','limit'));        
            if(!empty($lastTransactionPending)){
                Cache::write("transactions_{$userId}", $lastTransactionPending, 'TRANSACTION_PENDINGS');
            } 
        }
        if(!empty($lastTransactionPending) && $this->params->action != 'logout' && $this->request->action != "disableNotificationTransactionPending"){ 
            $this->Session->write("Message.flash", array());
            $this->Flash->info(__('Hay una transacción pendiente por completarse.') .  '<br>' .' <a href="#" data-dismiss="alert" id="closeNotificationTransaction" data-id="'.$lastTransactionPending["Transaction"]["id"].'">' . __('No mostrar más').'</a>'); 
        } 
    }

    public function lowercaseText($texto){
        //$texto = mb_strtolower($texto);
        return $texto;
    }

    public function buildInfoLog($registryId, $description, $descriptionEng, $beforeEdit = null, $afterEdit = null, $defaultAction = null, $contacId = null, $teamId = null){
        $this->loadmodel("Log"); 
        $data["Log"] = array(
            "controller"      => $this->request->controller,
            "action"          => isset($defaultAction) ? $defaultAction : $this->request->action,
            "model"           => $this->modelClass,
            "registry_id"     => $registryId,
            "contac_id"       => isset($contacId)   ? $contacId : NULL,
            "description"     => $description,
            "description_eng" => $descriptionEng,
            "before_edit"     => isset($beforeEdit) ? $beforeEdit : NULL,
            "after_edit"      => isset($afterEdit)  ? $afterEdit : NULL,
            "user_id"         => AuthComponent::user("id"),
            "team_id"         => $teamId,
            "date"            => date("Y-m-d"),
            "created"         => date("Y-m-d H:i:s")
        );
        $this->Log->create();
        $this->Log->save($data);
        return $this->Log->id; 
    }    

    private function __deleteSessionsStore(){   
        if(empty($this->Session->read("Config.language"))){
            $this->Session->write("Config.language","esp");
        }
        if($this->request->params["action"] != "add" && !$this->request->is('ajax')){ 
            $this->Session->delete("ActivateUser"); 
        }
        if($this->request->controller != "coupons" || $this->request->action != "add"){
            $this->Session->delete("CouponForm"); 
        }
        if($this->request->controller != "pages" &&  $this->request->action != "filter"){
            $this->Session->delete("SearchAdvance");
        } 
        if($this->request->controller != "contacs" && !$this->request->is('ajax')){
            $this->deleteCookieInitial(); 
        }
        $this->__deleteCookiesNotExist();
    }

    public function deleteCookieInitial(){
        if(isset($_COOKIE["time"])){
            setcookie('time', "" , time() + (86400 * 30), '/'); // 86400 = 1 day 
        } 
    }

    private function __deleteCookiesNotExist(){ 
        try{
            $cookieAccept = "cookieAccept".AuthComponent::user("id");
            if(!isset($_COOKIE[$cookieAccept])) { 
                setcookie($cookieAccept, "", time() + (86400 * 30), '/', NULL, true, true);  
            }
        } catch (Exception $e) {} 
    }
    
    //FUNCION PARA LOS TEMAS ESPECIFICOS
    public function conversorSegundosHorasTopics($time) {
        $horas      = floor($time / 3600);
        $minutos    = floor(($time - ($horas * 3600)) / 60);
        $segundos   = $time - ($horas * 3600) - ($minutos * 60);
        $hora_texto = "";
        if ($horas > 0 ) {
            $hora_texto .= $horas   . "h ";
        }
        if ($minutos > 0 ) {
            $hora_texto .= $minutos . "m ";
        }
        if ($segundos > 0 ) {
            $hora_texto .= $segundos . "s";
        }
        return $hora_texto;
    }

    private function __notifyUserAdquiredPlan(){
        $userId      = AuthComponent::user("id");
        $plan        = Cache::read("planAdquired_{$userId}", 'PLAN_ADQUIRED');   
        if(!empty($plan)){    
            if($plan == 1){
                $this->Session->write("Message.flash", array());
                $this->Flash->success(__('Has adquirido el plan correctamente.')); 
                Cache::delete("planAdquired_{$userId}",'PLAN_ADQUIRED');
                $this->checkTeamCreate();
            } else if($plan == 2) {
                $this->Session->write("Message.flash", array());
                $this->Flash->success(__('Has cambiado de plan correctamente.')); 
                Cache::delete("planAdquired_{$userId}",'PLAN_ADQUIRED');
            } 
        }  
    }

    public function changeRecommended(){
        if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax';
        $this->autoRender = false;
        $this->loadModel('Plan');
        $this->__changePlans();
        $id = EncryptDecrypt::decrypt($this->request->data['id']);
        $this->Plan->updateAll(
            array('Plan.recommended' => Configure::read('ENABLED')),
            array('Plan.id' => $id)
        );
        $this->Flash->success(__('Se ha realizado el cambio del plan a recomendado.'));
        return true;
    }

    private function __changePlans(){
        $this->loadModel('Plan');
        $this->Plan->updateAll(array('Plan.recommended' => Configure::read('DISABLED')));
    }

    //CACHE DE LOS PERMISOS
    private function __savePermissions(){ 
        $user_id = AuthComponent::user("id");
        $this->__storeCacheStep($user_id);
        if (($permissionsApp = Cache::read("permissions_{$user_id}", 'PERMISSIONS')) === false) { 
            $this->loadmodel("PositionsRestriction");
            $planActual  = $this->__getPlanActual();
            $planExpired = $this->__getPlanExpired();
            $userTeams   = $this->__getAllUserTeams();
            $positionsRestrictions = $this->PositionsRestriction->find("all");
            if(!empty($positionsRestrictions) && AuthComponent::user()){
                $restrictions   = array();
                foreach ($positionsRestrictions as $positionsRestriction) {
                    $positionId               = $positionsRestriction['Position']['id'];
                    $restrictions["PlanUser"] = $planActual;
                    $restrictions["PlanUserExpired"] = $planExpired;
                    $restrictions["UserTeam"] = $userTeams["0"];
                    $restrictions["TeamIds"]  = $userTeams["1"];
                    $restrictions["Positions"][$positionId]['Position'] = $positionsRestriction['Position'];
                    $restrictions["Positions"][$positionId]['Restriction'][] = $positionsRestriction['Restriction'];
                }
                Cache::write("permissions_{$user_id}", $restrictions, 'PERMISSIONS'); 
            } 
        }  
    }

    //ALMACENAR EN CACHE EL STEP DEL USUARIO
    private function __storeCacheStep($user_id){
        if (($stepApp = Cache::read("stepUser_{$user_id}", 'STEPUSER')) === false) {
            $this->loadmodel("User");
            $conditions   = array("User.id" => $user_id);
            $step_initial = $this->User->field("step_initial", $conditions);
            if(!empty($step_initial)){
                if($step_initial == configure::read("ENABLED")){
                    Cache::write("stepUser_{$user_id}", $step_initial, 'STEPUSER');
                }
            }
        }
    }

    private function __getPlanActual(){
        $this->loadmodel("PlanUser");
        $conditions = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("ENABLED"), "PlanUser.expired" => Configure::read("DISABLED"));
        $plan       = $this->PlanUser->find("first", compact("conditions"));
        return $plan;
    }

    private function __getPlanExpired(){
        $this->loadmodel("PlanUser");
        $conditions = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("DISABLED"), "PlanUser.expired" => Configure::read("ENABLED"));
        $plan       = $this->PlanUser->find("first", compact("conditions"));
        return $plan;
    }

    private function __getAllUserTeams(){
        $this->loadmodel("UserTeam");
        $conditions = array("UserTeam.user_id" => AuthComponent::user("id"), "UserTeam.state" => configure::read("ENABLED"), "UserTeam.user_state" => configure::read("DISABLED"));
        $userTeams  = $this->UserTeam->find("all", compact("conditions"));
        $teamIds    = Set::extract($userTeams, '{n}.UserTeam.team_id');
        return array("0" => $userTeams, "1" => $teamIds);
    }

    public function updateCachePermissions(){
        $this->loadmodel("User");
        $fields    = array("User.id");
        $usersList = $this->User->find("list", compact("fields")); 
        foreach ($usersList as $user_id) { 
            Cache::delete("permissions_{$user_id}", 'PERMISSIONS');  
        } 
    }

    public function updateCacheStep(){
        $user_id = AuthComponent::user("id");
        Cache::delete("stepUser_{$user_id}", 'STEPUSER');
    }

    public function getTeamIds(){  
        $permissionApp = $this->getCachePermissions();
        return $permissionApp["TeamIds"]; 
    }

    public function validateTeamSession(){
        if (is_null($this->Session->read("TEAM"))) {
            $this->Flash->fail(__("Por favor seleccione una empresa."));
            $this->redirect("/teams");
        }else{
            $this->set("teamIdSelected",EncryptDecrypt::decrypt($this->Session->read("TEAM")));
        }
    }

    //CARGAR LOS EQUIPOS DONDE TENGA PERMISOS
    public function loadTeamPermission(){ 
        $permissionApp = $this->getCachePermissions();
        $teams         = array(); 
        if(!empty($permissionApp["UserTeam"])){
            foreach ($permissionApp["UserTeam"] as $userTeam) {
                if($userTeam["UserTeam"]["user_id"] == AuthComponent::user("id")){
                    if(isset($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]])){
                        foreach ($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]]["Restriction"] as $restriction) {
                            if($this->request->action == "add" && $this->request->controller == "contacs") {
                                if (in_array($restriction["action"], array("index")) && in_array($restriction["controller"], array("contacs"))) { 
                                    $teams[$userTeam["Team"]["id"]] = $userTeam["Team"]["name"]; 
                                }
                            } else if ($this->request->action == "edit" && $this->request->controller == "meetings") {
                                if (in_array($restriction["action"], array("add")) && in_array($restriction["controller"], array("meetings"))){
                                    $teams[$userTeam["Team"]["id"]] = $userTeam["Team"]["name"];
                                }  
                            } else {
                                if (in_array($this->request->action, $restriction) && in_array($this->request->controller, $restriction)) { 
                                    $teams[$userTeam["Team"]["id"]] = $userTeam["Team"]["name"]; 
                                }  
                            }
                        } 
                    } 
                } 
            }   
        }  
        return $teams; 
    }


    // DE ACUERDO A LA ACCION, AL CONTROLADOR Y EL EQUIPO DE TRABJO SE VALIDA LOS PERMISOS
    public function check_team_permission_action($actions = array(), $controller = array(), $teamId = null, $return = false){ 
        $permissionApp = $this->getCachePermissions();

        $actionsAllow  = array(); 
        if(!is_null($teamId)){
            if(!empty($permissionApp["UserTeam"])){
                foreach ($permissionApp["UserTeam"] as $userTeam) {
                    if($userTeam["UserTeam"]["team_id"] == $teamId && $userTeam["UserTeam"]["user_id"] == AuthComponent::user("id")){
                        if(isset($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]])){
                            foreach ($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]]["Restriction"] as $restriction) {
                                if (in_array($restriction["action"], $actions) && in_array($restriction["controller"], $controller)) { 
                                    $actionsAllow[$restriction["action"]] = true;  
                                } 
                            } 
                        } 
                    } 
                }   
            }  
        } else {
            $actionsAllow = $this->__check_actions_permission($permissionApp, $actions, $controller);
        }
        if ($return) {
            return $actionsAllow;
        }

        $this->__checkPermissionInAction($actionsAllow, $actions, $teamId); 
    }

    //SE VALIDA QUE SI TIENE UN PLAN ACTIVO O NO Y SINO LO TIENE SE VALIDA SI ESTA ASIGNADO A UN EQUIPO DE TRABAJO
    private function __check_actions_permission($permissionApp = array(), $actions = array(), $controller = array()){
        if(!empty($permissionApp["PlanUser"])){
            if(!empty($permissionApp["UserTeam"])){
                return $response = $this->__checkActionAllowed($actions, $controller, $permissionApp);
            } 
        } else {  
            if(!empty($permissionApp["UserTeam"])){
                return $response = $this->__checkActionAllowed($actions, $controller, $permissionApp);
            }
        }  
    }

    private function __checkActionAllowed($actions = array(), $controller = array(), $permissionApp){ 
        $actionsAllow = array();
        if(!empty($permissionApp["UserTeam"])){
            foreach ($permissionApp["UserTeam"] as $userTeam) { 
                if(isset($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]])){
                    foreach ($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]]["Restriction"] as $restriction) {
                        if (in_array($restriction["action"], $actions) && in_array($restriction["controller"], $controller)) { 
                            $actionsAllow[$restriction["action"]] = true; 
                        } 
                    }  
                } 
            } 
        } 
        return $actionsAllow;
    } 

    private function __checkPermissionInAction($actionsAllow = array(), $actions = array(), $teamId = null){ 
        $permissionApp = $this->getCachePermissions();
        $team          = array();
        if(!isset($actionsAllow[$actions["0"]])){
            if(!is_null($teamId)){
                foreach ($permissionApp["UserTeam"] as $userTeam) {
                   if($userTeam["Team"]["id"] == $teamId){
                        $team = $userTeam["Team"]["name"];
                   }
                }
                if(empty($team)){
                    $this->Flash->fail(__('No tienes permisos para realizar esta acción.'));
                    $this->redirect(array('action' => 'index_page','controller' => 'pages')); 
                } else {
//                    $this->Flash->fail(__('No tienes permisos para realizar esta acción en la empresa: ') . $team);
                    $this->redirect(array('action' => 'index_page','controller' => 'pages')); 
                } 
            } else {
                $this->Flash->fail(__('No tienes permisos para realizar esta acción.'));
                $this->redirect(array('action' => 'index_page','controller' => 'pages')); 
            } 
        } 
    }

    public function loadTeams(){
        $this->loadmodel("Team"); 
        $permissionApp = $this->getCachePermissions();
        $conditions    = array("Team.id" => $permissionApp["TeamIds"]);
        $teams         = $this->Team->find("list", compact("conditions"));
        return $teams;
    }

    private function __validatePlanExpired(){ 
        $permissionApp = $this->getCachePermissions();  
        if(AuthComponent::user()){
            if(!empty($permissionApp["PlanUserExpired"]) && $this->request->controller != "payus" && !$this->request->is("ajax") && $this->request->action != "planning_management" && $this->request->action != "logout"){
                //$this->Flash->fail(__('Tu plan esta vencido, renuévalo o adquiere otro para que sigas disfrutando de todas las funcionalidades.'));
               // $this->redirect(array('action' => 'planning_management','controller' => 'plans')); 
            }
            if(AuthComponent::user("role_id") == NULL && AuthComponent::user("id")){
                if(empty($permissionApp["PlanUser"]) && empty($permissionApp["PlanUserExpired"])){ 
                   // $this->Flash->info(__('Con tu nueva cuenta puedes crear empresas. ¡Anímate! a comprar un plan para que puedas habilitar funcionalidades como; crear clientes, proyectos, reuniones, actas y más.')); 
                } 
           } 
        }
    } 

    public function getCachePermissions(){
        $userId        = AuthComponent::user("id");
        $permissionApp = Cache::read("permissions_{$userId}", 'PERMISSIONS');
        return $permissionApp;
    }

    public function getCacheStep(){
        $userId  = AuthComponent::user("id");
        $stepApp = Cache::read("stepUser_{$userId}", 'STEPUSER');
        return $stepApp;
    }

    public function set_lang_notifications($lang = null){
        if(empty($lang) || is_null($lang)){
            $lang = "esp";
        }
        $this->Session->write('Config.language', $lang);
        Configure::write('Config.language', $lang);
    }

    public function allowOriginRequest() {
        $this->response->header('Access-Control-Allow-Origin','*');
        $this->response->header('Access-Control-Allow-Methods','*');
        $this->response->header('Access-Control-Allow-Headers','X-Requested-With');
        $this->response->header('Access-Control-Allow-Headers','Content-Type, x-xsrf-token');
        $this->response->header('Access-Control-Max-Age','172800');
    }

    public function outResponse($values) {
        $resultJ = json_encode($values);
        $this->response->type('json');
        $this->response->body($resultJ);
        return $this->response;
    }

    public function activateStep($user_id){
        $this->loadModel("User");
        $this->User->updateAll(
            array('User.step_initial' => Configure::read('ENABLED')),
            array('User.id'           => $user_id)
        );
    } 

    public function checkTeamCreate(){
        $this->loadModel("Team");
        $teamCount = $this->Team->getTotalTeam(Authcomponent::user("id"));
        if($teamCount >= 1) {
            return $this->redirect(array('controller' => 'positions','action' => 'add')); 
        } else {
            return $this->redirect(array('controller' => 'teams','action' => 'add'));
        }
    }

    public function taskPending(){
        $cacheStep   = $this->getCacheStep();
        $taskPending = array();
        if($cacheStep == Configure::read('ENABLED')){
           $taskPending = $this->__checkTaskPending();
        } 
        return $taskPending;
    }

    private function __checkTaskPending(){
        $this->loadModel("Team");
        $this->loadModel("Position");
        $this->loadModel("Client");
        $this->loadModel("Project");
        $this->loadModel("UserTeam");
        $models = array(
            "user_teams" => $this->UserTeam->getTotalUser(),
            "projects"   => $this->Project->getTotalProject(),
            "clients"    => $this->Client->getTotalClient(),
            "positions"  => $this->Position->getTotalPosition(),
            "teams"      => $this->Team->getTotalTeam(Authcomponent::user("id"))
        ); 
        return $this->__buildTaskPending($models);
    }

    private function __buildTaskPending($models){
        $tasks     = array();
        $action    = "";
        $countTask = 0;       
        foreach ($models as $modelName => $totalRecord) {
            if($totalRecord == 0){
                $action  =  FULL_BASE_URL.'/'.$modelName.'/add';
                $countTask += 1;
            }
        }
        if($countTask == 0){
            $tasks = array();
        } else {
            $tasks = array("url" => $action, "message" => sprintf(__("Tienes %s tarea(s) por completar."), $countTask), "models" => $models);
        }
        return $tasks;
    }

    public function deleteCacheStep(){
        $this->loadModel("User");
        $this->User->updateAll(
            array('User.step_initial' => Configure::read('DISABLED')),
            array('User.id'           => AuthComponent::user("id"))
        );
        $this->updateCacheStep();
    }











    //app mobile

    public function out($result, $values, $reponseKey=null) {
        $reponseKey = ($reponseKey != null) ? $reponseKey : $this->modelClass;
        $result[$reponseKey] = $values;
        $this->set([
          "Response"   => $result,
          '_serialize' => "Response"
        ]);
    }

    public function getResult($values = null) {
        if(empty($values)) {
            return array('result'=> false);
        } 
        return array('result'=> true);
    }

    public function outFalseResponse($msg = null) {
        $result = array('result' => false);
        if(!empty($msg)) {
            $result = array_merge($result, $msg);
        }
        $this->set([
          "Response"   => $result,
          '_serialize' => "Response"
        ]);
    }

    /*
    * Poner vacios los campos, cuando los valores sean nulos, para evitar problema de compatibilidad de datos
    * en los lenguajes de programacion mobile que validan los tipos de datos
    * @params Array $resultSet los datos que provienen de la consulta que retorna el modelo
    * @uses $this->__clearHiddenFields
    * @return Array $resultSet
    */
    public function clearFields($resultSet = array()) {
        foreach ($resultSet as $modelName => &$modelValues) {
            $this->__clearHiddenFields($modelValues);
            if (is_array($modelValues)) {
                foreach ($modelValues as $field => $values) {
                    if(is_array($values)) {
                        foreach ($values as $valueKey => $value) {
                            if($value === null) {
                                $resultSet[$modelName][$field][$valueKey] = "";
                            }
                        }
                    }elseif($values === null) {
                        $resultSet[$modelName][$field]= "";
                    }   
                }
            }
        }
        $this->__clearHiddenFields($resultSet);
        return $resultSet;
    }

    private function __clearHiddenFields(&$resultSet = array()) {
        if(isset($resultSet['User']['password'])) {
            unset($resultSet['User']['password']);
        }
        if(isset($resultSet['User']['hash_change_password'])) {
            unset($resultSet['User']['hash_change_password']);
        }
        if(isset($resultSet['User']['hash_confirm_account'])) {
            unset($resultSet['User']['hash_confirm_account']);
        }
        if(isset($resultSet['password'])) {
            unset($resultSet['password']);
        }
    }

    /*
    * Obtener un mensaje a la vez del listado de mensajes que retorna
    * un modelo, cuando no puede guardar, porque falla una validacion
    * @params Array $listErrors listado de errores que retorna el modelo
    * @return String $error
    */
    public function getValidationErrors($listErrors = array()) {
        $error = null;
        if(!empty($listErrors)) {
          foreach ($listErrors as $field => $errors) {
            foreach ($errors as $errorMessage) {
              $error = $this->__getErrorCodeMessage($errorMessage);
              break;
            }
          }
        }
        return $error;
    }

    private function __getErrorCodeMessage($code = null) {
        $errors = array(
            '0001' => array('code'=>'0001', 'message'=>__('Tu cuenta se encuentra inactiva')),
            '0002' => array('code'=>'0002', 'message'=>__('Tu cuenta fue desactivada')),
            '0003' => array('code'=>'0003', 'message'=>__('Correo electrónico o contraseña incorrectos')),
            '0004' => array('code'=>'0004', 'message'=>__('La contraseña actual no coincide, por favor inténtalo nuevamente')),
            '0005' => array('code'=>'0005', 'message'=>__('No se ha encontrado un usuario relacionado al correo electrónico ingresado')),
            '0006' => array('code'=>'0006', 'message'=>__('Error al guardar, por favor inténtelo más tarde')),
        );
        if(!empty($errors[$code])) {
            return $errors[$code]; 
        }
        return array('message'=>$code);
    }

    public function outValue($value = null) {
        echo $value;
    }
	
	public function validateCaptcha($recaptcha){
        $url  = Configure::read('URL_VERIFY_CAPTCHA'); 
        $data = array(
            'secret'   => Configure::read('RE_CAPTCHA_SECRET_KEY'),
            'response' => $recaptcha
        );
        $options = array(
            'http' => array(
                'header'  =>  array(
                    'Content-Type: application/x-www-form-urlencoded', 
                ),
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context         = stream_context_create($options);
        $verify          = file_get_contents($url, false, $context);
        $captcha_success = json_decode($verify); 
        return $captcha_success;
    }
	
	public function get_users_disable_team($role_permission, $team_id, $department_id){
        $this->loadModel('Project');
        //1. CUANDO EL ROL ES ADMINISTRADOR GENERAL 
        if($role_permission == 1){
            $users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ', U.lastname) as name
            FROM users as U 
            JOIN user_teams as UT ON UT.user_id = U.id 
            JOIN departments D ON UT.department_id = D.id  
            JOIN teams T ON UT.team_id = T.id
            WHERE UT.user_state = 1 AND UT.team_id = $team_id AND type_user = '1' AND U.firstname != ''  ");
            $users = Hash::combine($users,  '{n}.U.id','{n}.U.id') ;
            return $users;
            //CUANDO ES ADMIN DE AREA
        }elseif($role_permission == 2){
            $users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ',U.lastname) as name
            FROM users as U 
            JOIN user_teams as UT ON UT.user_id = U.id 
            JOIN departments D ON UT.department_id = D.id  
            JOIN teams T ON UT.team_id = T.id
            WHERE UT.user_state = 1 AND UT.team_id = $team_id
            AND U.firstname != ''
            AND D.id = $department_id
            ");
            $users = Hash::combine($users,  '{n}.U.id','{n}.U.id') ;
            return $users;
            //CUANDO ES PERMISO NORMAL
        }else{
            $id = AuthComponent::User("id");
            $users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ',U.lastname) as name
            FROM users as U 
            JOIN user_teams as UT ON UT.user_id = U.id 
            JOIN departments D ON UT.department_id = D.id  
            JOIN teams T ON UT.team_id = T.id
            WHERE UT.user_state = 1 AND UT.team_id = $team_id
            AND U.firstname != ''
            AND D.id = $department_id
            AND U.id = $id
            ");
            $users = Hash::combine($users,  '{n}.U.id','{n}.U.id') ;
            return $users;
        }
        //2. CUANDO EL ROL ES DIRECTOR DE AREA, O CON PERMISOS DE DIRECCION DE AREA.
        //3. CUANDO ES UN USUARIO SIN PERMISOS DE DIRECCION DE AREA.
        return array();
    } 

    public function get_role_permission_in_team(){
        //CONSULTO EL ROLE Y PERMISO DE LA PERSONA EN EL EQUIPO
        $this->loadModel('UserTeam');
        $this->loadModel('Position');
        $conditions = array(
            'UserTeam.team_id' => Configure::read('Application.team_id'),
            'UserTeam.user_id' => AuthComponent::User('id')
        );
        $userRolePermission = $this->UserTeam->find('first',compact('conditions'));
        $userRolePermission = $userRolePermission['UserTeam'];
        $conditions         = array('Position.id' => $userRolePermission['position_id']);
        $roleInfo           = $this->Position->find('first',compact('conditions'));
        $userRolePermission['Position'] = $roleInfo['Position'];
        return array(
            'userRolePermission' => $userRolePermission,
            'roleInfo'           => $roleInfo,
        );
    }

    public function re_asignar_tareas_compromisos($own_user_id, $new_user_id){
        $this->__updateOwnUserLists($own_user_id, $new_user_id);
        $this->__updateOwnUserCommitments($own_user_id, $new_user_id);       
    }

    private function __updateOwnUserLists($own_user_id, $new_user_id){
        $this->loadModel('Slate');
        $recursive  = -1;

        //LISTAS DE TAREAS
        $conditions = array('Slate.user_id' => $own_user_id);
        $slates     = $this->Slate->find('all', compact('conditions','recursive'));
        if(!empty($slates)) {
            $conditions = array('Slate.user_id' => $own_user_id);
            $fields     = array('Slate.user_id' => $new_user_id);
            $this->Slate->updateAll($fields, $conditions); 
        }

        //PERMISOS DENTRO DE UNA LISTA DE TAREAS
        $conditions = array('SlatesUser.user_id'  => $own_user_id);
        $slate_user = $this->Slate->SlatesUser->find('all', compact('conditions','recursive'));

        if(!empty($slate_user)) {
            $conditions = array('SlatesUser.user_id' => $own_user_id);
            $fields     = array('SlatesUser.user_id' => $new_user_id);
            $this->Slate->SlatesUser->updateAll($fields, $conditions);
        }

        //TAREAS
        $conditions = array(
            'SlatesTask.user_id' => $own_user_id,
            'SlatesTask.state'   => '1'
        );

        $slate_tasks = $this->Slate->SlatesTask->find('all', compact('conditions','recursive'));
        if(!empty($slate_tasks)) {
            $conditions = array(
                'SlatesTask.user_id' => $own_user_id,
                'SlatesTask.state'   => '1'
            );
            $fields = array('SlatesTask.user_id' => $new_user_id);
            $this->Slate->SlatesTask->updateAll($fields, $conditions);
        } 
    }

    private function __updateOwnUserCommitments($own_user_id, $new_user_id){
        $recursive = -1;
        $hoy       = date('Y-m-d');
        $this->loadmodel('Commitment');
        $conditions = array(
            'Commitment.user_id' => $own_user_id,
            'Commitment.state'   => '1',
        );
        $compromisosPendientes = $this->Commitment->find('all',compact('conditions','recursive'));

        //Compromisos vencidos
        $conditions = array(
            'Commitment.user_id' => $own_user_id,
            'Commitment.state'   => '1',
            'Commitment.delivery_date  <' => $hoy,
        );
        $compromisosVencidos   = $this->Commitment->find('all',compact('conditions','recursive'));
 
        if(!empty($compromisosPendientes)) {
            foreach ($compromisosPendientes as $compromiso) {
                $compromiso['Commitment']['user_id'] = $new_user_id;
                $this->Commitment->save($compromiso);
            }
        }

        if(!empty($compromisosVencidos)) {
            foreach ($compromisosVencidos as $compromiso) {
                $compromiso['Commitment']['user_id'] = $new_user_id;
                $this->Commitment->save($compromiso);
            }
        }        
    }
	
}
