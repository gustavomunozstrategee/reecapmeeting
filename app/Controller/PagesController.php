<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'OutLookApi1', array('file' => 'OutLookApi/outlook.php'));
App::import('Vendor', 'OutLookApi2', array('file' => 'OutLookApi/oauth.php'));

class PagesController extends AppController {

	public function request_plan(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
        $this->autoRender = false;
        $this->loadModel("ContactsUs");
        $errors = $this->getValidates($this->request->data["control"], 'ContactsUs'); 
        if(!is_array($errors)){
        	$this->__sendMailPlanRequest($this->request->data["control"]);
        	$response = array("state" => true);
        } else {
        	$response = array("state" => false, "message" => $errors);
        }
    	$this->outJSON($response);
	}
 
	public function contacto(){
		$this->layout = "default_home";
		$this->loadModel("ContactsUs");
		if($this->request->is("post")){
			$errors = $this->getValidates($this->request->data["control"], 'ContactsUs'); 
			$responseCaptcha = $this->validateCaptcha($_POST["g-recaptcha-response"]);
		 	if (!$responseCaptcha->success) {	
		 		if(!is_array($errors)) {
		 			$errors = array();
		 		}	 
		 		$errors[] =  __('Se debe validar el reCAPTCHA.');
			} 
			$this->ContactsUs->set($this->request->data["control"]);
			if(!is_array($errors)) { 
				$this->__sendMailContactsUs($this->request->data["control"]);
				$this->Flash->success(__("Se ha enviado el mensaje correctamente."));
				$this->redirect(array('action' => 'home'));	    
			} else {
				$this->Flash->fail(implode('<br>', $errors));  
			} 
		}
	}

	private function __sendMailContactsUs($data = array()){
		$this->loadModel("User");
		$conditions = array("User.role_id" => Configure::read("ADMIN_ROLE_ID"), "User.state" => Configure::read("ENABLED"));
		$fields     = array("User.id","User.email");
		$admins     = $this->User->find("list", compact("conditions","fields"));
		$options = array( 
			'to'       => $admins,
			'template' => 'message_contacts',
			'subject'  => __('Nuevo mensaje de contáctanos').' - '.Configure::read('Application.name'),
			'vars'     =>  array("data" => $data),
		);
		$this->sendMail($options); 	 
	}

	private function __sendMailPlanRequest($data = array()){
		$options = array( 
			'to'       => Configure::read("ADMIN_PLAN"),
			'template' => 'request_plan',
			'subject'  => __('Solicitud de plan').' - '.Configure::read('Application.name'),
			'vars'     =>  array("data" => $data),
		);
		$this->sendMail($options); 	 
	}

	public $components = array('Paginator');

	public function beforeFilter() { 
	  	$this->Auth->allow('home','resetVariablesFirebase','contacto','request_plan');
		parent::beforeFilter(); 
    }

	public function index_page() { 
    return $this->redirect(array('controller' => 'contacs', 'action' => 'index'));
		$permissionApp = $this->getCachePermissions(); 
		// if($this->request->is("post")){
		// 	if(!empty($this->request->data["Search"]["search_general"])){
		// 		$this->Session->write("SearchAdvance", $this->request->data["Search"]["search_general"]);
		// 		$this->redirect(array('action' => 'filter')); 
		// 	} 
		// } 
		$this->resetVariablesContac();		
		//$plans           = $this->dataEnabled('Plan');  
	    $proyectos       = $this->totalRecords('Project');
	    $clientes        = $this->totalRecords('Client');
		$actas           = $this->totalRecords('Contac');
		$tags            = $this->totalRecords('Tag');
		//$users           = $this->__topTenBusinessMoreUsers();
		$stepUser        = $this->__actionsCompleteds();
		//$activityRecents = $this->__recentActivityBusiness(); 
		//$meetings        = $this->__listAllMeetings();
		//$totalMeetings   = $this->Meeting->countMeetingCreated();  
		$this->__commitments_week_actual();
		$this->set(compact('plans','actas','clientes','proyectos','users','tags','stepUser','activityRecents','meetings','totalMeetings','permissionApp'));
	}

	private function __commitments_week_actual(){
		$this->loadModel("Commitment");
		$monday       = date('Y-m-d', strtotime('monday this week'));
		$sunday       = date('Y-m-d', strtotime('sunday this week'));
		$conditions = array(
			"Commitment.user_id" => AuthComponent::user("id"), 
			"Commitment.state" => configure::read("COMMITMENT_NOT_DONE"),
			"Commitment.delivery_date >=" => $monday,
			"Commitment.delivery_date <=" => $sunday,
			"Contac.state"       	      => configure::read("DISABLED")
		);
 		$order = array('Commitment.id DESC');
 		$limit = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments = $this->Paginator->paginate("Commitment"); 
		$this->set(compact('commitments')); 
	}

	public function home(){
	 	$this->loadModel("Plan");
	 	$recursive  = -1;
	 	$conditions = array("Plan.state" => Configure::read('ENABLED'));
	 	$plans      = $this->Plan->find("all", compact("recursive","conditions"));
		$this->resetVariablesContac();		
		$this->layout = "website";
		$this->set(compact('plans'));
	}

	Public function totalRecords($model) {
		$this->loadModel($model);
		if ($model == 'Contac') {
			$conditions = array($model.'.state' => Configure::read('DISABLED'));
		} else if ($model == 'Tag') {
			$fields    =  array('COUNT(DISTINCT Tag.name) as count'); 
		} else {
			$conditions = array($model.'.state' => Configure::read('ENABLED'));
		}
		$registros = $this->$model->find('count',compact('conditions','fields'));
		return $registros;
	}

	public function notificacionLeida(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
		$this->loadModel('User');
        $this->layout     = 'ajax'; 
        $this->autoRender = false;
		$firebase_id 	  = $this->request->data['firebase_id'];
		$campo 			  = 'state';
		$dato 			  = 2;
		$this->User->updateFirebaseNotificaciones(AuthComponent::user('id'), $firebase_id, $campo, $dato);
		return true;
	}

	public function read_all_notifications(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
		$this->loadModel('User');
        $this->layout     = 'ajax'; 
        $this->autoRender = false;
        $notificacions 	  = isset($this->request->data) ? $this->request->data: NULL;
        if(!empty($notificacions)){
        	try {
		        foreach ($notificacions["notifications"] as $notificacion) { 
		        	if($notificacion["state"] == 1){
		        		$campo = 'state';
						$dato  = 3;
						$this->User->updateFirebaseNotificaciones(AuthComponent::user('id'), $notificacion["nodo_id"], $campo, $dato); 
		        	}
		        }        		
        	} catch (Exception $e) {}
        }
	}

	private function __topTenBusinessMoreUsers(){
		$this->loadModel('User'); 
		$this->loadModel('UserTeam');  
		$fields     = array("OwnUser.id","OwnUser.company_name","COUNT('UserTeam.user_id') as cantidad_usuarios");
	 	$group      = array("OwnUser.id");
	 	$order      = array("cantidad_usuarios DESC");
	 	$limit      = 10;
		$users 		= $this->UserTeam->find("all", compact("fields","group","order","limit")); 
		return $users; 
	}

	private function __actionsCompleteds(){
		return false;
		$conditions    = array("User.id" => AuthComponent::user("id")); 
		$avatarCreated = $this->User->find("first", compact("conditions"));
		$conditions    = array("Client.user_id" => AuthComponent::user("id"));	 
		$clientCreated = $this->Client->find("count", compact("conditions"));
		$conditions    = array("Client.user_id" => AuthComponent::user("id"));		 
		$fields        = array("Client.id");
		$clientIds     = $this->Client->find("list", compact("conditions","fields"));
		$conditions    = array("Team.user_id" => AuthComponent::user("id"));
		$teamsCreated  = $this->Client->Team->find("count", compact("conditions"));
		$conditions      = array("Project.client_id" => $clientIds);
		$projectCreated  = $this->Client->Project->find("count", compact("conditions"));
		$conditions      = array("Commitment.user_id" => AuthComponent::user("id"), "Commitment.state" => Configure::read("COMMITMENT_DONE"));
		$commitmentCompleted = $this->Contac->Commitment->find("count", compact("conditions"));
		$conditions    = array("Contac.user_id" => AuthComponent::user("id"), "Contac.state" => Configure::read("DISABLED"));
 		$contacCreated = $this->Contac->Commitment->find("count", compact("conditions")); 
		return array(
			"avatarCreated" => $avatarCreated,
			"projects"      => $projectCreated,
			"clients"       => $clientCreated,
			"employees"     => $teamsCreated,
			"commitments"   => $commitmentCompleted,
			"contacs"       => $contacCreated
		); 
	}

	private function __recentActivityBusiness(){
		$this->loadModel("Log");
		$teamIds     = $this->getTeamIds();
		$order 		 = array("Log.created DESC");
		$conditions  = array("Log.team_id" => $teamIds, "Log.user_id" => AuthComponent::user("id"));
		$limit       = 10;
		$activityRecents = $this->Log->find("all", compact("order","limit","conditions")); 
		return $activityRecents;
	}

	private function __recentActivityUser(){
		$this->loadModel("Log");
		$order 		     = array("Log.created DESC");
		$limit 	         = 10; 
		$activityRecents = $this->Log->find("all", compact("order","limit"));
		return $activityRecents;
	}

	private function __listAllMeetings(){
		$this->loadModel("Meeting");
		$this->loadModel("Assistant"); 
		$conditions   = array("Assistant.user_id" => AuthComponent::user("id"), "NOT" => array("Assistant.contac_id !=" => NULL));
		$fields       = array("Assistant.meeting_id");
		$meetingsIds  = $this->Assistant->find("list", compact("conditions","fields")); 
		$limit        = 10;
        $order        = array('Meeting.modified'=>'DESC');
        $conditions   = array('Meeting.id' => $meetingsIds);  
        $this->Paginator->settings = compact('conditions','limit','order');
        $meetings     = $this->Paginator->paginate($this->Meeting); 
        return $meetings; 
	}

	public function filter() {
		$teams = $this->loadTeams();
		$this->validateTeamSession();
		$this->check_team_permission_action(array("filter"), array("pages"));
		$listaParametros = $this->__filterParametros();
		$results         = array();
		if ($this->request->is('post')) {
			if ($this->request->data['Search']['frase'] != '' && $this->request->data['Search']['parametros'] != '' && $this->request->data['Search']['team_id'] != '') {
				$dataSearched = $this->request->data['Search']['frase']; 
				$results 	  = $this->__searchData($this->request->data['Search']['parametros'], $this->request->data['Search']['team_id'], $this->request->data['Search']['frase'], $this->request->data['Search']['excluir']);
			} else {
				$results = array();
				$this->Flash->fail(__('Debes ingresar alguna palabra para poder filtrar, seleccionar una empresa y escoger en los módulos que deseas ejecutar la consulta.'));
			}
			$totalResults = array_sum(array_map("count", $results)); 
		}
		$this->set(compact('listaParametros', 'results','dataSearched','totalResults','teams'));
	}

	private function __filterParametros(){
		return array('Project' => __('Proyectos'), 'Client' => __('Clientes'), 'UserTeam' => __('Usuarios'), 'Contac' => __('Actas'),'Commitment' => __('Compromisos'),'Meeting' => __('Reuniones'),'Todo' => __('Todo'));
	}

	private function __searchData($resultados = array(), $teamId, $busqueda = null, $excluir = null){
		$searchField = Configure::read('SEARCH_FIELDS');
		$results     = array();
		foreach ($resultados as $model) {
			if ($model != 'Todo') {
				$modelField = $searchField[$model]; 
				$busqueda   = $this->lowercaseText($busqueda);
				$conditions = $this->__conditionsBusquedaAvanzada($excluir, $modelField, $busqueda, $model, $teamId);
				$this->loadModel("{$model}");
				$recursive  = 1;
				$results["{$model}"] = $this->$model->find('all', compact('conditions','recursive'));
			}
		}  
		return $results;
	}

	public function __conditionsBusquedaAvanzada($excluir = null, $modelField = array(), $busqueda = null, $model = null, $teamId){
		$conditions = array(); 
		$this->loadModel("User"); 
		if ($excluir != '') {
			$palabrasExcluidas = array_filter(explode(',', $excluir));
		} 
		$busqueda =  $this->User->normalizeChars($busqueda);
		$busqueda =  trim(strtolower($busqueda));  
		if(is_array($modelField)) { 
			foreach ($modelField as $field) {
				if ($model != 'UserTeam') {
					$conditions['OR'][] = array("LOWER(CONVERT({$model}.{$field} USING latin1)) LIKE"=> "%{$busqueda}%", "{$model}.team_id" => $teamId); 
					if ($excluir != '') {
						foreach ($palabrasExcluidas as $value) {
							$conditions[] = array("LOWER(CONVERT({$model}.{$field} USING latin1)) NOT LIKE"=> '%'.($value).'%', "{$model}.team_id" => $teamId);
						}
					}
				} else {
					$conditions = array('OR' => array('LOWER(CONVERT(User.email USING latin1))  LIKE'  => "%{$busqueda}%", "LOWER(CONVERT({$this->User->virtualFields["name"]} USING latin1)) LIKE"  => "%{$busqueda}%"));
					$conditions['Team.id'] = $teamId; 
					if ($excluir != '') {
						foreach ($palabrasExcluidas as $value) { 
							$value = trim(strtolower($value));
							$conditions[] = array("LOWER(CONVERT({$this->User->virtualFields["name"]} USING latin1)) NOT LIKE" => '%'.$value.'%', "{$model}.team_id" => $teamId); 
						}
					}
				}
			}
		} else { 			 
			$conditions[] = array("LOWER(CONVERT({$model}.{$modelField} USING latin1)) LIKE"=> "%{$busqueda}%", "{$model}.team_id" => $teamId);
			if ($excluir != '') {
				foreach ($palabrasExcluidas as $value) {
					$value = trim(strtolower($value));
					$conditions[] = array("LOWER(CONVERT({$model}.{$modelField} USING latin1)) NOT LIKE"=> '%'.$value.'%', "{$model}.team_id" => $teamId);
				}
			}
		} 
		return $conditions;
	}
}