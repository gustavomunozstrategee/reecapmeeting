<?php
App::uses('AppController', 'Controller');
CakePlugin::load('PhpExcel');


class SlatesController extends AppController {
public $components = array('Paginator','PhpExcel.PhpExcel'); 






        public function index() { 
            $this->list_permission();
            $this->list_slates_active();
        }

        public function slatesadmin() { 
            $this->list_slates_active();
        }

        private function list_slates_active(){
            $this->Slate->SlatesUser->recursive = 3;
            $conditions    = array('SlatesUser.user_id' => AuthComponent::user('id'));
            $listas        = $this->Slate->SlatesUser->find('all',compact('conditions','recursive'));
            $this->loadModel('UserTeam');
            $collaborators = $this->UserTeam->getCollaborators_NOTME(Configure::read('Application.team_id'), AuthComponent::user("id"));
            $this->set(compact('listas','collaborators'));
        }

         public function addUsers() { 
            $this->loadModel('SlatesUser');
            foreach ($this->request->data["SlatesUser"]["participantes"] as $key => $value) {
                $newSave = array(
                    "role_id" => $this->request->data["Slate"]["permisos"],
                    "user_id" => $value,
                    "slate_id" => $this->request->data["SlatesUser"]["id"],
                );
                $conditions = array(
                        "SlatesUser.user_id" => $value,
                        "SlatesUser.slate_id" => $this->request->data["SlatesUser"]["id"],
                );
                 
                $result = $this->SlatesUser->find("first",compact("conditions"));
                if(!empty($result)){
                    $id = $result["SlatesUser"]["id"];
                    $this->SlatesUser->delete($id);
                    $this->SlatesUser->saveAll($newSave);
                     
                }else{
                    $this->SlatesUser->saveAll($newSave);
                }
            }

            $this->Flash->success(__('Usuario añadido / editado correctamente.'));
            return $this->redirect(array('action' => 'edit/'.EncryptDecrypt::encrypt($this->request->data["SlatesUser"]["id"])));

           
                
                
               
         }

        public function add() { 
                if ($this->request->is('post') || $this->request->is('put')){
                        $this->request->data["Slate"]["user_id"] = AuthComponent::user("id");
                        $listName = $this->request->data["Slate"]["name"];
                        if($this->Slate->save($this->request->data)){
                                $this->loadModel('SlatesUser');
                                $info = array(
                                        "user_id" => AuthComponent::user("id"),
                                        "slate_id" => $this->Slate->id,
                                        "role_id" => "0"
                                       );
                                       $this->SlatesUser->saveAll($info);
                               foreach ($this->request->data["Slate"]["participantes"] as $key => $value) {
                                       $info = array(
                                        "user_id" => $value,
                                        "slate_id" => $this->Slate->id,
                                        "role_id" => $this->request->data["Slate"]["permisos"]
                                       );
                                       $this->SlatesUser->saveAll($info);
                                       $this->loadModel('User');
                                        $conditions = array(
                                            'User.id' => $value,
                                        );
                                        $emailNotification = $this->User->find("first",compact("conditions"));
                                        $this->slate_notifications($emailNotification["User"]["email"],'Te han añadido a la lista "'.$listName.'"');
                               }
                                
                                $this->Flash->success(__('Lista creada correctamente'));
                                return $this->redirect(array('action' => 'view/'.EncryptDecrypt::encrypt($this->Slate->id)));
                        }
                }
                $this->list_permission();
                $collaborators = $this->UserTeam->getCollaborators_NOTME(Configure::read("Application.team_id"),AuthComponent::user("id"));
                $this->set(compact('collaborators'));
                
        }

        private function slate_notifications($email,$subject){
            $commitments_info = array(
               "texto" => $subject
            );
            $template = 'slate_notifications';
            $subject  = sprintf(__($subject." - Taskee"));
            $options = array(
                'to'       => $email,
                'template' => $template,
                'subject'  => Configure::read('Application.name'). ' - '.$subject,
                'vars'     =>  array('info'=>$commitments_info),
            );
            $this->sendMail($options);  
            return true;
        }

        public function edit($id) { 
            $id = EncryptDecrypt::decrypt($id);
            if (!$this->Slate->exists($id)) {
                throw new NotFoundException(__('Invalid slates task'));
            }

            if ($this->request->is(array('post', 'put'))) {
                $this->request->data["Slate"]["id"] = $id;
                if ($this->Slate->save($this->request->data)) {
                    $this->Flash->success(__('Tarea editada correctamente'));
                     
                    return $this->redirect(array("controller"=>"slates",'action' => 'view/'.EncryptDecrypt::encrypt($id)));
                } else {
                    $this->Flash->error(__('The slates task could not be saved. Please, try again.'));
                }
            } else {
                $options = array('conditions' => array('Slate.' . $this->Slate->primaryKey => $id));
                $this->request->data = $this->Slate->find('first', $options);
            }

                $this->Slate->SlatesUser->recursive = 3;
                $conditions = array(
                        "SlatesUser.slate_id" => $id
                );
                $lista = $this->Slate->SlatesUser->find("all",compact("conditions","recursive"));
                $this->set(compact("lista"));

            

            $this->list_permission();
                $collaborators = $this->UserTeam->getCollaborators_NOTME(Configure::read("Application.team_id"),AuthComponent::user("id"));
                $this->set(compact('collaborators','id'));
            
                
        }

        public function list_slates_admin(){ 
            $id       = EncryptDecrypt::decrypt($this->request->data['id']); 
            //LISTADO DE TAREAS
            $this->Slate->SlatesTask->recursive = 1;
            if($this->Session->read('tasks_filtro') != "" ){
                $conditions = array(
                    'SlatesTask.slate_id' => $id,
                    'SlatesTask.state'    => '1',
                    'SlatesTask.user_id'  => $this->Session->read('tasks_filtro'),
                );

            } else {
                $conditions = array(
                    'SlatesTask.slate_id' => $id,
                    'SlatesTask.state'    => '1',
                );
            }
            $order       = array('SlatesTask.deadline_date ASC');
            $tareas      = $this->Slate->SlatesTask->find('all',compact('conditions','recursive','order'));
            $SlateRoleId = $this->getRole($id);
            $this->set(compact("tareas","SlateRoleId"));
        }

        public function listSlates(){ 
            $data     = $this->get_role_permission_in_team(); 
            $user_ids = $this->get_users_disable_team($data['roleInfo']['Position']['permission_taskee'], Configure::read('Application.team_id'), $data['userRolePermission']['department_id']);
            $id       = EncryptDecrypt::decrypt($this->request->data['id']);
            // $lista    = $this->Slate->SlatesUser->find("all",compact("conditions","recursive"));
            // $this->set(compact("lista"));
            //LISTADO DE TAREAS
            $this->Slate->SlatesTask->recursive = 1;
            if($this->Session->read('tasks_filtro') != "" ){
                $conditions = array(
                    'SlatesTask.slate_id' => $id,
                    'SlatesTask.state'    => '1',
                    'SlatesTask.user_id'  => $this->Session->read('tasks_filtro'),
                    'NOT' => array('SlatesTask.user_id' => $user_ids)
                );

            } else {
                $conditions = array(
                    'SlatesTask.slate_id' => $id,
                    'SlatesTask.state'    => '1',
                    'NOT' => array('SlatesTask.user_id' => $user_ids)
                );
            }
            $order       = array('SlatesTask.deadline_date ASC');
            $tareas      = $this->Slate->SlatesTask->find('all',compact('conditions','recursive','order'));
            $SlateRoleId = $this->getRole($id);
            $this->set(compact("tareas","SlateRoleId"));
        }

        public function Slatesfinished(){
            $data     = $this->get_role_permission_in_team();
            $user_ids = $this->get_users_disable_team($data['roleInfo']['Position']['permission_taskee'], Configure::read('Application.team_id'), $data['userRolePermission']['department_id']);
            $id       = EncryptDecrypt::decrypt($this->request->data['id']);
            // $lista = $this->Slate->SlatesUser->find("all",compact("conditions","recursive"));
            // $this->set(compact("lista"));
            //LISTADO DE TAREAS
            $this->Slate->SlatesTask->recursive = 1;
            if($this->Session->read('tasks_filtro') != "" ) {
                $conditions = array(
                    'SlatesTask.slate_id' => $id,
                    'SlatesTask.state'    => '2',
                    'SlatesTask.user_id'  => $this->Session->read('tasks_filtro'),
                    'NOT' => array('SlatesTask.user_id' => $user_ids)
                );
            } else {
                $conditions = array(
                    'SlatesTask.slate_id' => $id,
                    'SlatesTask.state'    => '2',
                    'NOT' => array('SlatesTask.user_id' => $user_ids)
                );
            }
            $order  = array('SlatesTask.deadline_date ASC');
            $tareas = $this->Slate->SlatesTask->find('all',compact('conditions','recursive','order'));
            $this->set(compact('tareas'));
        }

        private function exportListado($id){
            $this->layout     = false;
            $this->autoRender = false;
            Configure::write('debug', 0);
            $this->PhpExcel->createWorksheet();
            $this->PhpExcel->setDefaultFont('Calibri', 16);

            $data     = $this->get_role_permission_in_team();
            $user_ids = $this->get_users_disable_team($data['roleInfo']['Position']['permission_taskee'], Configure::read('Application.team_id'), $data['userRolePermission']['department_id']);
          

            $header = array(
                array('label' => __('Responsable'), 'width' => '30'),
                array('label' => __('Tarea'), 'width' => '100'),
                array('label' => __('Fecha de Planeació'), 'width' => '20'),
                array('label' => __('Fecha de Terminación'), 'width' => '20'),
                array('label' => __('Estado'), 'width' => '20'),
            );
            $this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
            $conditions = array(
                'SlatesTask.slate_id' =>  $id,
                'NOT'   => array('SlatesTask.user_id' => $user_ids)
            );
            $this->loadModel('SlatesTask');
            $tareas = $this->SlatesTask->find('all',compact('conditions','recursive'));
             
            foreach ($tareas as $key => $value){
                $estado           = 0;
                $fechaTerminacion = '';
                if($value['SlatesTask']['state'] == 1){
                    $estado = 'Sin Terminar';
                } else {
                    $estado = 'Terminado';
                    $fechaTerminacion = $value['SlatesTask']['modified'];
                }
                $this->PhpExcel->addTableRow(
                    array(
                        (h( $value['User']['firstname']) .' '. h($value['User']['lastname'])  ),
                        (h( $value['SlatesTask']['description'])),
                         h($value['SlatesTask']['deadline_date']),
                         h($fechaTerminacion), 
                         h($estado),
                    )
                );
            }
            if($this->PhpExcel->output('reporte.xlsx')){
                return $this->redirect(array('action' => 'view/'.EncryptDecrypt::encrypt($id)));
            } 
        }

        public function view_slate_admin($id){
            if ($this->request->is(array('post', 'put'))) {
                $this->Session->write('tasks_filtro',$this->request->data["user_id"]);                
            }else{
                 $this->Session->write('tasks_filtro',"");
            }
            $idList = $id; 
            $id = EncryptDecrypt::decrypt($id);
            //LISTADO DE USUARIOS
            $this->Slate->SlatesUser->recursive = 3;
            $conditions = array(
                    "SlatesUser.slate_id" => $id
            );
            $lista = $this->Slate->SlatesUser->find("all",compact("conditions","recursive"));
            $this->set(compact("lista"));
            $this->set(compact("tareas",'idList'));
            $this->list_permission();
                //ADDD
            $this->loadModel('Slate');
             $conditions = array(
                    "Slate.id" => $id
            );
            $Slate = $this->Slate->find("first",compact("conditions"));
            $this->loadModel('Project');
            $this->Project->recursive = 2;
            $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
            $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
            $this->set(compact("projects","Slate"));
     
            $this->list_permission();
            //LISTADO DE USUARIOS
            $this->loadModel('SlatesUser');
            $this->SlatesUser->recursive = 0;


            $conditions = array(
                    "SlatesUser.slate_id" => $id
            );


            $users = $this->SlatesUser->find("all",compact("conditions","recursive"));
            $listUsers = array();
            foreach ($users as $key => $value) {
                $listUsers  += array(
                            $value["User"]["id"] => $value["User"]["firstname"]." ".$value["User"]["lastname"]
                        );
            }
            $users = $listUsers;
            $this->set(compact("users"));
            $slates = $this->Slate->find('list');
            $this->set(compact('slates'));
        }


        public function view($id) {
            if ($this->request->is(array('post', 'put'))) {
                $this->Session->write('tasks_filtro',$this->request->data["user_id"]);                
            }else{
                 $this->Session->write('tasks_filtro',"");
            }

            $idList = $id; 
            $id = EncryptDecrypt::decrypt($id);
            if(isset($_GET["export_data"])){
                $this->exportListado($id);
                //return $this->redirect(array('action' => 'exportListado/'.$id));
                
            }
            //LISTADO DE USUARIOS
            $this->Slate->SlatesUser->recursive = 3;
            $conditions = array(
                    "SlatesUser.slate_id" => $id
            );
            $lista = $this->Slate->SlatesUser->find("all",compact("conditions","recursive"));
            $this->set(compact("lista"));
            $this->set(compact("tareas",'idList'));
            $this->list_permission();
                //ADDD
            $this->loadModel('Slate');
             $conditions = array(
                    "Slate.id" => $id
            );
            $Slate = $this->Slate->find("first",compact("conditions"));
            $this->loadModel('Project');
            $this->Project->recursive = 2;
            $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
            $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
            $this->set(compact("projects","Slate"));
     
            $this->list_permission();
            //LISTADO DE USUARIOS
            $this->loadModel('SlatesUser');
            $this->SlatesUser->recursive = 0;


            $conditions = array(
                    "SlatesUser.slate_id" => $id
            );


            $users = $this->SlatesUser->find("all",compact("conditions","recursive"));
            $listUsers = array();
            foreach ($users as $key => $value) {
                $listUsers  += array(
                            $value["User"]["id"] => $value["User"]["firstname"]." ".$value["User"]["lastname"]
                        );
            }
            $users = $listUsers;
            $this->set(compact("users"));
            $slates = $this->Slate->find('list');
            $this->set(compact('slates'));
        }


        public function delete() { 
                
        }

        public function getRole($slateId){
            $this->loadModel('SlatesUser');
            $this->SlatesUser->recursive = 0;
            $conditions = array(
                    "SlatesUser.slate_id" => $slateId
            );
            $users = $this->SlatesUser->find("all",compact("conditions","recursive"));
            
            foreach ($users as $key => $value) {
                if($value['User']["id"] == AuthComponent::user("id")){
                    return $value["SlatesUser"]["role_id"];
                }
                
            }
            return "null";
        }

        private function list_permission(){
                $this->loadModel('UserTeam');
                $this->UserTeam->recursive = 0;
                $conditions = array(
                        'UserTeam.user_id' => AuthComponent::user("id"),
                        "UserTeam.team_id" => Configure::read("Application.team_id")
                );
                $userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
                $this->set(compact("userPosition"));

        }

        public function editPermission(){
            $this->layout = false;
            $this->autoRender = false;

             

            $this->loadModel('SlatesUser');
            $this->request->data["SlatesUser"]["id"] = $this->request->data["id"];
            $this->request->data["SlatesUser"]["role_id"] = $this->request->data["role_id"];
            if($this->SlatesUser->save($this->request->data)){
                return 1;
            }else{
                return 0;
            }

        }
        public function removeUser(){
            
        
            $this->layout = false;
            $this->autoRender = false;
             $this->loadModel('SlatesUser');
             $this->SlatesUser->id = $this->request->data["id"]; 
                if ($this->SlatesUser->delete()) {
                     return 1;
                } else {
                return 0;
                }
                
        }

        public function deleteSlate(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('SlatesUser');
            $this->loadModel('SlatesTask');
          
             $this->Slate->id = $this->request->data["id"]; 
                if ($this->Slate->delete()) {
                     $conditions = array(
                        'SlatesUser.slate_id' => $this->request->data["id"],
                     );
                     $this->SlatesUser->deleteAll($conditions);
                     $conditions = array(
                        'SlatesTask.slate_id' => $this->request->data["id"],
                     );
                     $this->SlatesTask->deleteAll($conditions);

                    return 1;
                } else {
                    return 0;
                }
                
        }

        public function outList(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('SlatesUser');
            $conditions = array(
                        'SlatesUser.slate_id' => $this->request->data["id"],
                        'SlatesUser.user_id' => AuthComponent::user("id"),
                     );

               if( $this->SlatesUser->deleteAll($conditions)){
                 return 1;
               }else{
                 return 0;
               }
        }

        public function checkTask(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('SlatesTask');

            $this->request->data["SlatesTask"]["id"] = $this->request->data["id"];
            $this->request->data["SlatesTask"]["state"] = "2";
             if ($this->SlatesTask->save($this->request->data)){
                return 1;
             }else{
                return 0;
             }
             

                   
        }

        public function deleteTask(){
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('SlatesTask');

            $this->request->data["SlatesTask"]["id"] =  EncryptDecrypt::decrypt($this->request->data["id"]);
            $this->request->data["SlatesTask"]["state"] = "0";
             if ($this->SlatesTask->save($this->request->data)){
                return 1;
             }else{
                return 0;
             }
        }

        public function reasignar_tareas(){
            set_time_limit(100000);
            $this->layout      = 'ajax';
            $this->autoRender  = false;
            $recursive         = -1;
            $conditions        = array('Slate.id' => $this->request->data['ids']);
            $listas            = $this->Slate->find('all',compact('conditions','recursive'));
            $response          = array('error' => false);
            foreach ($listas as $key => $lista) {
                if($lista['Slate']['user_id'] == $this->request->data['user_id']){
                    $response = array(
                        'message' => sprintf(__('El usuario seleccionado ya se encuestra asignado a la lista %s'), $lista['Slate']['name']),
                        'error'   => true
                    );
                    $this->outJSON($response);
                    exit();
                    break;
                }
            }   
            $conditions        = array('User.id' => $this->request->data['user_id']);
            $emailNotification = $this->Slate->User->find('first', compact('conditions','recursive'));

            foreach ($listas as $key => $lista) { 
                if($lista['Slate']['user_id'] == AuthComponent::user('id')) {
                    $lista['Slate']['user_id'] = $this->request->data['user_id'];                     
                    $this->Slate->save($lista);
                }  
                $conditions = array(
                    'slate_id' => $lista['Slate']['id'],
                    'user_id'  => AuthComponent::user('id'),
                );
                $slate_user = $this->Slate->SlatesUser->find('first', compact('conditions','recursive'));
                $slate_user['SlatesUser']['user_id'] = $this->request->data['user_id'];

                $this->Slate->SlatesUser->save($slate_user);

 
                $conditions = array(
                    'SlatesTask.user_id'  => AuthComponent::user('id'),
                    'SlatesTask.slate_id' => $lista['Slate']['id']
                );
                $fields = array('SlatesTask.user_id' => $this->request->data['user_id']);
                $this->Slate->SlatesTask->updateAll($fields, $conditions); 
                
                $this->slate_notifications($emailNotification['User']['email'],'Te han añadido a la lista "'.$lista['Slate']['name'].'"');
            }
            $this->outJSON($response);
            exit(); 
        }


        public function eliminar_tareas(){
            set_time_limit(100000);
            $this->layout     = 'ajax';
            $this->autoRender = false;
            if(!empty($this->request->data['ids'])) {
                $conditions = array('SlatesTask.id' => $this->request->data['ids']);
                $this->Slate->SlatesTask->deleteAll($conditions, false);
            }
            $this->outJSON(true);
            exit(); 
        }

        
}
