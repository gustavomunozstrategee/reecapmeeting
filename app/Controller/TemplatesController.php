<?php
App::uses('AppController', 'Controller');
 
class TemplatesController extends AppController {
 
	public $components = array('Paginator');
 
	public function deleteTemplateId(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout       = 'ajax'; 
		$this->autoRender   = false; 
		$this->Template->id = $this->request->data['template_id'];
		$conditions         = array("Template.id" => $this->request->data['template_id']);
		$template           = $this->Template->find("first" ,compact("conditions")); 
		$description        = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha eliminado la plantilla de nombre ") . $template["Template"]["name"];
		$full_name          = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		$description        = sprintf(__("El usuario %s ha eliminado una plantilla con el nombre %s."), $full_name, $template["Template"]["name"]);
		$descriptionEng     = sprintf(__("The user %s You have removed a template with the name %s."), $full_name, $template["Template"]["name"]);
		$this->buildInfoLog($this->request->data['template_id'], $description, $descriptionEng, NULL, NULL, NULL, NULL, $template["Template"]["team_id"]);
		if ($this->Template->delete()) {
			return __('Se ha borrado la plantilla correctamente.');
		}
	}

	public function add_template_acta(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
        $this->loadModel("Team");
        $conditions   = array("Team.id" => $this->request->data["teamId"]);
        $team 		  = $this->Team->find("list", compact("conditions"));
		$this->set(compact('team'));
	}

	public function add_template_acta_save(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
		$this->autoRender = false; 
		$data 			  = array();
		$data["errors"]   = "";
		$data["success"]  = 0;
		$data["errors"]   = $this->__validateData($data["errors"]);
		if ($data["errors"] == "") {
			$this->Template->create();
			$this->request->data['Template']['user_id']     = AuthComponent::user('id');
			$this->request->data['Template']['description'] = $this->request->data['description'];
			$this->request->data['Template']['name']        = $this->request->data['name'];
			$this->request->data['Template']['team_id']     = $this->request->data['teamId'];
			$this->Template->save($this->request->data);			
			$full_name       = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
			$description     = sprintf(__("El usuario %s ha creado una plantilla con el nombre %s."), $full_name, $this->request->data['name']);
			$descriptionEng  = sprintf(__("The user %s has created a template with the name %s."), $full_name, $this->request->data['name']);
			$this->buildInfoLog($this->Template->id, $description, $descriptionEng, NULL, NULL, NULL, NULL, $this->request->data['teamId']);
			$data["success"] = __('Los datos se han guardado correctamente.');
			$model = __('plantilla');
			$this->notificacionesEmpresa($model, $this->request->data['Template']['name']);
		}
		$this->outJSON($data); 
	}

	private function __validateData($errores){
		if (empty(trim($this->request->data['name']))) {
			$errores.= __("El nombre es requerido.")."\n";
		}
		if (!isset($this->request->data['teamId'])) {
			$errores.= __("La empresa es requerida.")."\n";
		}
		if (!strlen(trim($this->request->data['description']))) {
			$errores.= __("La descripción es requerida.")."\n";
		} //else {
			// $response = $this->__checkLength($this->request->data['description']);
			// if($response["response"] == false){
			// 	$errores.= __($response["message"])."\n";
			// } 
		//}
		return $errores;
	}

	public function template_list(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax';
        $teamId       = isset($this->request->data["teamId"]) ? $this->request->data["teamId"] : NULL; 
		$templates    = $this->Template->listTemplateEmpresa($teamId);
		$this->set(compact('templates'));
	}

	public function show_plantilla(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$datos        = $this->Template->dataTemplate($this->request->data['plantilla_id']);
		$this->set(compact('datos'));
	}

	private function __checkLength($descriptionTemplate){       
		$response = true;
        $result   = preg_replace('~(?><(\w++)[^>]*+>(?>\s++|&nbsp;|<br\s*+/?>)*</\1>|(?>\s++|&nbsp;|<br\s*+/?>)+)+$~xi', '', $descriptionTemplate);
        if(empty($result)){
        	$response = false;
        	return array("response" => $response, "message" => __("La descripción es requerida."));
        } else {       
        	$description = str_replace(array("<p>","</p>","<ul>","</ul>","<li>","</li>","<ol>","</ol>","&nbsp;"), array('',' ','',' ','',' ','',' ',''), strip_tags($descriptionTemplate, "<p><ul><li><ol>")); 	
        	$words       = explode(" ", $description); 
			foreach ($words as $word) { 
				if (!empty($word)) { 
					if(strlen($word) > 80){
						$response = false;
						break;
					} 
				}
			} 
			return array("response" => $response, "message" => __("Cada palabra de la descripción de la plantilla debe tener máximo 80 caracteres."));
        } 
 	}

 	public function edit(){
 		$this->layout = 'ajax';
 		if($this->request->data['edit'] == 'false') {
 			$conditions = array('Template.id' => $this->request->data['templateId']);
 			$this->request->data =$this->Template->find('first', compact('conditions'));
 		} else {
 			$data 			  = array();
			$data["errors"]   = "";
			$data["success"]  = 0;
			$data["errors"]   = $this->__validateData($data["errors"]);
			if ($data["errors"] == "") {
				$this->request->data['Template']['description'] = $this->request->data['description'];
				$this->request->data['Template']['name']        = $this->request->data['name'];
				$this->request->data['Template']['id']          = $this->request->data['templateId'];
				$this->Template->save($this->request->data);			
				$full_name       = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
				$description     = sprintf(__("El usuario %s ha editado una plantilla con el nombre %s."), $full_name, $this->request->data['name']);
				$descriptionEng  = sprintf(__("The user %s has created a template with the name %s."), $full_name, $this->request->data['name']);
				$this->buildInfoLog($this->request->data['Template']['id'], $description, $descriptionEng, NULL, NULL, NULL, NULL, $this->request->data['teamId']);
				$data["success"] = __('Los datos se han guardado correctamente.');
				$model = __('plantilla');
				$this->notificacionesEmpresa($model, $this->request->data['Template']['name']);
			}
			$this->outJSON($data); 
			die();
 		}
 	}

}
 