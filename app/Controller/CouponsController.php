<?php
App::uses('AppController', 'Controller');
 
class CouponsController extends AppController {
 
	public $components = array('Paginator');

	public function beforeFilter() { 
	 	$this->Auth->allow('disable_coupon_expired');
	 	parent::beforeFilter();       
    }
 
	public function index() { 
		$this->validateRolAdministrador();
		$limit                     = 10;
        $order                     = array('Coupon.modified'=>'DESC'); 
        $conditions                = $this->Coupon->buildConditions($this->request->query);
        $this->Paginator->settings = compact('limit','order','conditions');
        $this->Coupon->recursive   = 0;
        $coupons = $this->Paginator->paginate(); 
		$this->set(compact('coupons'));
	}
 
	public function view($id = null) {
		$this->validateRolAdministrador();
		$id  = EncryptDecrypt::decrypt($id); 
		if (!$this->Coupon->exists($id)) {
			$this->showMessageExceptions();
		}
		$this->Coupon->unbindModel(array('hasMany' => array('Transaction')));
		$options = array('conditions' => array('Coupon.' . $this->Coupon->primaryKey => $id));
		$this->set('coupon', $this->Coupon->find('first', $options));
	}
 
	public function add() { 
		$this->validateRolAdministrador();
		if ($this->request->is('post')) { 
			$data = $this->Coupon->getStartAndEndDates($this->request->data['daterange'], $this->request->data); 
			$couponCodeSession = $this->Session->read('codeCoupon');
			if(isset($couponCodeSession) || !empty($couponCodeSession)){
				$data['Coupon']['code'] = $couponCodeSession;
			} else {
				$data['Coupon']['code'] = $this->__assignedCodeAutomatically();
			} 
			$this->Coupon->create();
			if ($this->Coupon->save($data['Coupon'])) {
				$this->Session->delete('codeCoupon');
				$this->Session->delete("CouponForm");
				$this->__sendNotificationCoupon($data);
				$this->Flash->success(__('El cupón se ha guardado y enviado correctamente al destinatario.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$errors = $this->Coupon->validationErrors;
				if(isset($errors["daterangedates"])){
					$this->Flash->fail(__($errors["daterangedates"]["0"]));
				} else {
					$this->Flash->fail(__('El cupón no se ha podido guardar y enviar al destinatario, por favor vuelve e inténtalo.'));
				}  
			}
		} 
		$this->__buildList();		
	} 

	private function __assignedCodeAutomatically(){
		$sixDigitsRandomNumber = mt_rand(100000, 999999);
		$codeCoupon 		   = __('REEC'.$sixDigitsRandomNumber);
		return $codeCoupon;
	}

	private function __sendNotificationCoupon($data = array()){
		set_time_limit(300000); 
		$userExist = $this->__validateExistUsers($data["Coupon"]["email_user"]); 	 
		$opts = array(
			'to'       => $data["Coupon"]["email_user"],
			'subject'  => __('Has recibido un cupón'),
			'vars'     => compact('data','userExist'),
			'template' => 'coupon_information',
		); 
		$this->sendMail($opts);	 
	}

	private function __validateExistUsers($email){
		$conditions = array("User.email" => $email);
		$user       = $this->Coupon->Plan->PlanUser->User->find("first", compact("conditions"));
		if(!empty($user)){
			$response = true;
		} else {
			$response = false;
		}
		return $response;
	} 

	private function __buildList(){
		$conditions = array("Plan.state" => configure::read("ENABLED"), "NOT" => array("Plan.trial" => configure::read("ENABLED")));
		$plans 		= $this->Coupon->Plan->find('list', compact("conditions"));
		$this->set(compact('plans'));
	}

	public function generateCodeCoupon(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout          = 'ajax';
		$this->autoRender      = false;
		$sessionCodeCoupon     = $this->Session->read('codeCoupon');
		$sixDigitsRandomNumber = mt_rand(100000, 999999);
		$codeCoupon 		   = __('REEC'.$sixDigitsRandomNumber);
		$this->Session->write('codeCoupon', $codeCoupon);
		$this->outJSON($codeCoupon); 
	}

	public function getCoupon(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout          = 'ajax';
		$this->autoRender      = false;
		$codeCoupon   = isset($this->request->data["codeCoupon"]) ? $this->request->data["codeCoupon"] : null;
		$couponExist  = $this->Coupon->getInfoCoupon($codeCoupon);
		if($couponExist["state"] == true){
			$conditions   = array("PlanUser.user_id" => Authcomponent::user("id"));
			$userHavePlan = $this->Coupon->Plan->PlanUser->find("count", compact("conditions"));
			if($userHavePlan == 0){
				$response = $couponExist["message"];
				$this->Coupon->Plan->savePlanGift($couponExist);
				$this->__notifyCouponRedeemed($couponExist);
				$this->Coupon->usedCoupon($couponExist["Detail"]["Coupon"]["id"]);
				$this->Flash->success(__('Se ha redimido el cupón correctamente.')); 
			} else {
				$response = array("state" => false, "message" => __("Este cupón no se puede usar, ya tienes un plan asignado."));
			} 
		} else {
			$response = $couponExist;
		}
		$this->outJSON($response); 
	}

	private function __notifyCouponRedeemed($couponInfo = array()){  
		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha redimido su cupón con el código ") . $couponInfo["Detail"]["Coupon"]["code"] . __(" sobre el plan ") . $couponInfo["Detail"]["Plan"]["name"];
		//$this->buildInfoLog($couponInfo["Detail"]["Coupon"]["id"], $description, NULL, NULL, NULL, NULL); 
		set_time_limit(300000);  
		$opts = array(
			'to'       => $couponInfo["Detail"]["Coupon"]["email_user"],
			'subject'  => __('Has redimido tu cupón'),
			'vars'     => compact('couponInfo'),
			'template' => 'redeemed_coupon',
		); 
		$this->sendMail($opts);	
		$this->updateCachePermissions(); 
	}

	public function disabledCoupon(){ 
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout          = 'ajax';
		$this->autoRender      = false;
		$couponId   = isset($this->request->data["couponId"]) ? EncryptDecrypt::decrypt($this->request->data["couponId"]) : null;
		$reason     = isset($this->request->data["reason"])   ? $this->request->data["reason"] : null;
		$conditions = array("Coupon.id" => $couponId);
		$email      = $this->Coupon->field("email_user", $conditions);
		$this->__notifyCouponDisabled($email, $reason);
		$this->Coupon->disabledStateCoupon($couponId);
		$this->Flash->success(__('El cupón se ha deshabilitado correctamente.')); 
	}

	private function __notifyCouponDisabled($email, $reason){
		set_time_limit(300000);  
		$opts = array(
			'to'       => $email,
			'subject'  => __('Tu cupón ha sido deshabilitado'),
			'vars'     => compact('reason'),
			'template' => 'coupon_disabled',
		); 
		$this->sendMail($opts);	 
	}

	//cron job para deshabilitar los cupones que esten vencidos
	public function disable_coupon_expired(){
		set_time_limit(300000);
		$this->autoRender = false;
		$fechaActual      = date('Y-m-d');
		$recursive        = -1;
		$coupons          = $this->Coupon->find('all', compact('recursive')); 

		foreach ($coupons as $coupon) {
		 	if($fechaActual > $coupon['Coupon']['end_date'] && $coupon['Coupon']['state'] == configure::read('ENABLED')){
		 		$coupon['Coupon']['state'] = configure::read('DISABLED');
		 		$this->Coupon->save($coupon, array("validate" => false)); 
		 	}  
		} 
	}
}
