<?php
App::uses('AppController', 'Controller');

class CommitmentsController extends AppController {

	public $components = array('Paginator');

	public function beforeFilter() {
		$this->Auth->allow('send_reminder_commitments','commitment_pendings','mark_commitment_as_done','verify_commitment_to_done','mark_commitment','notification_firebase_commitment_today','disabled_commitments_expired');
		parent::beforeFilter();  
 	}

	public function index() {
		$configurationSet = $this->__checkConfigurationsCommitments();
		if(empty($configurationSet["hourConfigurations"]) || empty($configurationSet["timeConfigurations"])) {
			$this->Flash->info(__('Para recibir notificaciones de compromisos pendientes debes configurar el tiempo y la hora de envío.'));
		}
		$teams = $this->loadTeams();
		$this->set(compact('teams')); 
	}

	public function index_all_commitments(){
		$this->check_team_permission_action(array("index_all_commitments"), array("commitments")); 
		$teamsIds 	  = $this->getTeamIds(); 
		$conditions   = array('Contac.state' => Configure::read("DISABLED"), "Commitment.team_id" => $teamsIds);
	 	$conditions[] = $this->Commitment->buildConditions($this->request->query); 
		$order        = array('Commitment.id DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments  = $this->Paginator->paginate(); 
		$teams   = $this->loadTeams();
		$clients = $this->Commitment->Project->Client->getClientTeam($teamsIds);
		$this->set(compact('commitments','clients','teams')); 
	}

	public function my_commitments($employeeId = null){
		$employeeId = EncryptDecrypt::decrypt($employeeId); 
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$conditions   = array('Commitment.employee_id' => $employeeId, 'Contac.state' => Configure::read("DISABLED")); 
		$order        = array('Commitment.id DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments  = $this->Paginator->paginate(); 
		$this->set(compact('commitments','employeeId')); 
	}

	public function commitments_employee($userId = null){ 
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
     	$userId 	  = EncryptDecrypt::decrypt($userId);
        $this->layout = 'ajax'; 
		$conditions   = array('Commitment.user_id' => $userId, 'Contac.state' => Configure::read("DISABLED")); 
		$order        = array('Commitment.id DESC');
		$limit        = 10; 
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments  = $this->Paginator->paginate();  
		$this->set(compact('commitments','userId')); 
	}

	public function send_reminder_commitments(){
		set_time_limit(300000);
		$this->loadModel("CommitmentsConfiguration");
		$this->Commitment->unbindModel(array('belongsTo' => array('Project')));
		$this->autoRender   = false; 
		$conditions         = array("Commitment.state <>" => configure::read("COMMITMENT_DONE"), "Contac.state" => configure::read("DISABLED"));
		$commitments      	= $this->Commitment->find("all", compact("conditions")); 
		$recursive          = -1;
		$conditions         = array("CommitmentsConfiguration.hour !=" => NULL);
		$hourConfigurations = $this->CommitmentsConfiguration->find("all", compact("conditions","recursive"));
		$usersCommitments   = $this->Commitment->getUserWithCommitments($commitments);
		$this->__processInfoCommitements($hourConfigurations, $usersCommitments);
	}

	private function __processInfoCommitements($hourConfigurations, $usersCommitments = array()){
		$horaEnvio = date("H:i");  
		if(!empty($hourConfigurations)){
			foreach ($hourConfigurations as $hourConfiguration) { 
				if($hourConfiguration["CommitmentsConfiguration"]["hour"] == $horaEnvio) {
					foreach ($usersCommitments["usersCommitments"] as $userId => $usersCommitment) {
				 		if(date("Y-m-d") == $hourConfiguration["CommitmentsConfiguration"]["date"] && $hourConfiguration["CommitmentsConfiguration"]["user_id"] == $userId) {
							$this->__updateNextDateNotificationCommitment($hourConfiguration);
				 			$this->__nofificationCommitementsNotDone($usersCommitment); 
				 		} 
					}  
				}
			}
		}  
	}

	private function __addTimeToDateActual($time){
		$fecha 	     = date('Y-m-d'); 
		$nuevafecha  = strtotime("+{$time} day", strtotime($fecha)) ;
		$nuevafecha  = date ('Y-m-d', $nuevafecha);
		return $nuevafecha;
	}

	private function __updateNextDateNotificationCommitment($timeConfiguration = array()){
		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( "+{$timeConfiguration["CommitmentsConfiguration"]["time"]} day" , strtotime ( $timeConfiguration["CommitmentsConfiguration"]["date"]) ) ;
		$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
		$this->loadModel("CommitmentsConfiguration");
		$configurationHour["CommitmentsConfiguration"]["id"]      = $timeConfiguration["CommitmentsConfiguration"]["id"]; 
		$configurationHour["CommitmentsConfiguration"]["date"]    = $nuevafecha; 
		$this->CommitmentsConfiguration->save($configurationHour); 
	}

	private function __nofificationCommitementsNotDone($usersCommitment = array()){
		$this->set_lang_notifications($usersCommitment["lang"]);
		if($usersCommitment["lang"] == NULL || $usersCommitment["lang"] == "" || empty($usersCommitment["lang"])){
			$usersCommitment["lang"] = "esp";
		} 
		$template = $usersCommitment["lang"].'/commitments_pendings';
		if($usersCommitment["lang"] == "esp"){
			$subject = __('Compromisos pendientes').' - '.Configure::read('Application.name');
		} else {
			$subject = __('Pending commitments').' - '.Configure::read('Application.name');
		} 
		$options = array(
			'to'       =>  $usersCommitment["email"],
			'template' =>  $template,
			'subject'  =>  $subject,
			'vars'     =>  array('commitments' => $usersCommitment["commitments"]),
		);
		$this->sendMail($options); 
	}  

	public function commitment_pendings($id = null){
		$id = EncryptDecrypt::decrypt($id);
		$this->Session->write("CommitmentId", $id);
		$this->autoRender  = false;
		$id 		       = $this->Session->read("CommitmentId");
		$conditions 	   = array("Commitment.id" => $id, "Commitment.state" => configure::read("COMMITMENT_DONE"));
		$recursive  	   = -1;
		$commitment 	   = $this->Commitment->find("first", compact("conditions","recursive")); 
		if(!empty($commitment)){
			$this->Session->delete("CommitmentId");
			$this->Flash->success(__('Ya marcaste éste compromiso como realizado.'));
			if(AuthComponent::user()){
	 			return $this->redirect(array('action' => 'index', 'controller' => 'commitments')); 
			} else {
	 			return $this->redirect(array('action' => 'login', 'controller' => 'users'));  
			} 
		} else {
	 		return $this->redirect(array('action' => 'mark_commitment_as_done', 'controller' => 'commitments')); 			
		}
	}

	public function mark_commitment_as_done(){}

	public function verify_commitment_to_done(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout      = 'ajax';
		$this->autoRender  = false;
		$id = $this->Session->read("CommitmentId");
		if(!empty($id) || isset($id) || !is_null($id)){
			$response = true;
		} else {
			$response = false;
		}
		$this->outJSON($response);
	}

	public function mark_commitment(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$id = $this->Session->read("CommitmentId");
		$this->layout      = 'ajax';
		$this->autoRender  = false;
		if(AuthComponent::user()){
			$conditions = array("Commitment.id" => $id, "Commitment.user_id" => AuthComponent::user("id"));
		} else {
			$conditions = array("Commitment.id" => $id);
		}
		$recursive  = -1;
		$commitment = $this->Commitment->find("first", compact("conditions","recursive"));
		if(!empty($commitment)){
			$commitment["Commitment"]["state"] = configure::read("COMMITMENT_DONE");
			$this->Commitment->save($commitment);
			//$this->Commitment->saveLogCommitmentDone($commitment);		
			$this->Session->delete("CommitmentId");
			$this->Flash->success(__('El compromiso se ha marcado como realizado correctamente.'));
			$response = array("state" => true);
		} else {
			$this->Flash->fail(__('El compromiso no se ha marcado como realizado.'));
			$response = array("state" => true);
		}
		$this->outJSON($response); 
	}

	public function commitments_not_done(){	
		$teamGet = isset($this->request->query["teamIds"]) ? $this->request->query["teamIds"] : array(); 
 		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        } 
        $this->layout = 'ajax';  
		$conditions = array(
			"Commitment.user_id" => AuthComponent::user("id"), 
			"Commitment.state"   => configure::read("COMMITMENT_NOT_DONE"),
			"Contac.state"       => configure::read("DISABLED"), 
		);  
		if(!empty($this->request->query["teamIds"])){ 
			$conditions["OR"] = array("Contac.team_id" => $this->request->query["teamIds"]); 
		}
		$order = array('Commitment.id DESC');
		$limit = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments  = $this->Paginator->paginate();  
		$countCommitmentDotDone = $this->Commitment->find("count", compact("conditions")); 
		$this->set(compact('commitments','countCommitmentDotDone','teamGet')); 
	}

	public function commitments_week_actual(){
		$teamGet = isset($this->request->query["teamIds"]) ? $this->request->query["teamIds"] : array();
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$monday       = date('Y-m-d', strtotime('monday this week'));
		$sunday       = date('Y-m-d', strtotime('sunday this week'));
		$conditions = array(
			"Commitment.user_id" => AuthComponent::user("id"), 
			"Commitment.state" => configure::read("COMMITMENT_NOT_DONE"),
			"Commitment.delivery_date >=" => $monday,
			"Commitment.delivery_date <=" => $sunday,
			"Contac.state"       	      => configure::read("DISABLED")
		);
		if(!empty($this->request->query["teamIds"])){ 
			$conditions["OR"] = array("Contac.team_id" => $this->request->query["teamIds"]); 
		}
 		$order = array('Commitment.id DESC');
 		$limit = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments = $this->Paginator->paginate(); 
		$countCommitmentWeek = $this->Commitment->find("count", compact("conditions")); 
		$this->set(compact('commitments','countCommitmentWeek','teamGet')); 
	}

	public function commitments_completeds(){
		$teamGet = isset($this->request->query["teamIds"]) ? $this->request->query["teamIds"] : array();
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$conditions   = array(
			"Commitment.user_id" => AuthComponent::user("id"), 
			"Commitment.state"   => configure::read("COMMITMENT_DONE")
		);
		if(!empty($this->request->query["teamIds"])){ 
			$conditions["OR"] = array("Contac.team_id" => $this->request->query["teamIds"]); 
		}
		$order = array('Commitment.id DESC');
		$limit = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments = $this->Paginator->paginate(); 
		$commitmentsCompleted = $this->Commitment->find("count", compact("conditions"));  
		$this->set(compact('commitments','commitmentsCompleted','teamGet')); 
	}

	public function save_commitment_id_selected(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}		 
		$this->layout       = 'ajax';
		$this->autoRender   = false; 
		$commitmentsSession = $this->Session->read("CommitmentsIds");
		$this->request->data["commitmentsIds"] = EncryptDecrypt::decrypt($this->request->data["commitmentsIds"]);
		if(isset($this->request->data["add"])){
			$this->Session->write("CommitmentsIds.{$this->request->data["commitmentsIds"]}", $this->request->data["commitmentsIds"]);
		 	$this->Commitment->updateAll(
	            array('Commitment.selected' => Configure::read("ENABLED")), 
	            array('Commitment.id'       => $this->request->data["commitmentsIds"])
	        );
		} else if(isset($this->request->data["edit"])){
			unset($commitmentsSession[$this->request->data["commitmentsIds"]]);
			$this->Session->write("CommitmentsIds", $commitmentsSession);  
			$this->Commitment->updateAll(
	            array('Commitment.selected' => Configure::read("DISABLED")), 
	            array('Commitment.id'       => $this->request->data["commitmentsIds"])
	        );		 
		} 
	}

	public function read_commitments_select(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}		 
		$this->layout       = 'ajax';
		$this->autoRender   = false; 
		$commitmentsSession = $this->Session->read("CommitmentsIds"); 
		if(!empty($commitmentsSession)){
			$response = true;
		} else {
			$response = false;
		} 
		$this->outJSON($response); 
	}

	public function save_commitments(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}		 
		$this->layout       = 'ajax';
		$this->autoRender   = false; 
		$commitmentsSession = $this->Session->read("CommitmentsIds");
		$this->Commitment->updateAll(
            array(
            	'Commitment.selected' => Configure::read("DISABLED"),
            	'Commitment.state'    => Configure::read("ENABLED")
			), 
        	array('Commitment.id'     => $commitmentsSession)
        );
        $this->Commitment->saveLogsCommitmentsUserInSession($commitmentsSession);
        $this->Session->delete("CommitmentsIds"); 
        $response = array("state" => true, "message" => __("Se ha marcado como realizado correctamente."));
        $this->outJSON($response); 
	}

	public function commitments_createds(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$conditions   = array("Commitment.user_id" => AuthComponent::user("id"), "Contac.state" => configure::read("DISABLED"));
		$order 		  = array('Commitment.id DESC');
		$limit 		  = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments  = $this->Paginator->paginate();
		$this->set(compact('commitments'));  
	}

	public function commitments_expireds(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax';
		$conditions   = array("Commitment.user_id" => AuthComponent::user("id"), "Contac.state" => configure::read("DISABLED"), "Commitment.state" => configure::read("COMMITMENT_EXPIRED"));
		$order 		  = array('Commitment.id DESC');
		$limit 		  = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
		$commitments  = $this->Paginator->paginate();
		$this->set(compact('commitments'));  
	}

	public function number_commitments(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}		 
		$this->layout         = 'ajax';
		$this->autoRender     = false; 
		$conditions   		  = array("Commitment.user_id" => AuthComponent::user("id"), "Contac.state" => configure::read("DISABLED"));
	 	$commitmentsCreateds  = $this->Commitment->find("count", compact("conditions"));
        $conditions   		  = array("Commitment.user_id" => AuthComponent::user("id"), "Contac.state" => configure::read("DISABLED"), "Commitment.state" => configure::read("COMMITMENT_DONE"));
	 	$commitmentsCompleted = $this->Commitment->find("count", compact("conditions"));
        $conditions   		  = array("Commitment.user_id" => AuthComponent::user("id"), "Contac.state" => configure::read("DISABLED"), "Commitment.state" => configure::read("COMMITMENT_NOT_DONE"));
	 	$commitmentsPendings  = $this->Commitment->find("count", compact("conditions"));
        $conditions   		  = array("Commitment.user_id" => AuthComponent::user("id"), "Contac.state" => configure::read("DISABLED"), "Commitment.state" => configure::read("COMMITMENT_EXPIRED"));
	 	$commitmentsExpireds  = $this->Commitment->find("count", compact("conditions"));
	 	$data = array(
	 		"createds"   => $commitmentsCreateds,
	 		"completeds" => $commitmentsCompleted,
	 		"pendings"   => $commitmentsPendings,
	 		"expireds"   => $commitmentsExpireds
 		);
 		$this->outJSON($data);
	}

	//NOTIFICACIONES DE LOS COMPRIMISOS QUE EL USUARIO TENGA EN EL DIA ACTUAL
	public function notification_firebase_commitment_today(){
		$this->autoRender = false; 
		$this->Commitment->unbindModel(array('belongsTo' => array('Project','Client','Team','Contac')));
		$group 		 = array("Commitment.user_id");
	 	$fields      = array("Commitment.user_id","COUNT(Commitment.id) as totalCommmitments","User.lang");	
		$conditions  = array('Commitment.delivery_date' => date('Y-m-d'));
		$commitments = $this->Commitment->find("all", compact("conditions","recursive","group","fields"));
		if(!empty($commitments)){
			foreach ($commitments as $commitment) {
				if($commitment["User"]["lang"] == NULL || $commitment["User"]["lang"] == "" || empty($commitment["User"]["lang"])){
					$commitment["User"]["lang"] = "esp";
				}  
				if($commitment["User"]["lang"] == "esp"){
					$texto = sprintf(__("Tienes %s compromiso(s) para el día de hoy."), $commitment[0]['totalCommmitments']);
				} else {
					$texto = sprintf(__("You have %s commitment(s) for today."), $commitment[0]['totalCommmitments']);
				}  
				$url    = "commitments/index/";
				$msgEng = sprintf(__("You have %s commitment(s) for today."), $commitment[0]['totalCommmitments']);
		 		$this->notificacionesUsuarios($commitment['Commitment']['user_id'], $texto, $msgEng, $url);
			}
		} 
	}

	private function updateCommitmentTaskee($dataInfo = array()){

		$conditions = array(
					"Commitment.contac_id" => $dataInfo["contac"]["Contac"]["id"]
		);
		$fields = array(
			"Commitment.project_id"=>$dataInfo["contac"]["Project"]["id"],
			"Commitment.client_id"=>$dataInfo["contac"]["Client"]["id"],
			"Commitment.state"=>"1"
		);
		$this->Contac->Commitment->updateAll($fields,$conditions);

	}

	//GUARDAR EL COMPROMISO EN EL EDITAR EL ACTA Y DEVOLVER EL ID
	public function save_commitments_edit_contac(){
    
    	 
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}		 
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		$datos['Commitment']['user_id'] 	  = $this->request->data['user'];
		$datos['Commitment']['delivery_date'] = $this->request->data['limitDate'];
		$datos['Commitment']['description']   = $this->request->data['description'];
		$datos['Commitment']['contac_id']     = $this->Session->read("contactId");
		$this->Commitment->create();
		$this->Commitment->save($datos);
		$this->__logCommitmentsContacEdit($this->Commitment->id, $this->Session->read("contactId"));  
		$this->outJSON($this->Commitment->id); 
	}

	//LOG CUANDO SE AÑADE UN NUEVO COMPROMISO EN EL BORRADOR DEL ACTA
	private  function __logCommitmentsContacEdit($commitmentId, $contacId){
 		$this->Commitment->unbindModel(array('belongsTo' => array('Project')));
 		$conditions  = array("Commitment.id" => $commitmentId);
 		$commitment  = $this->Commitment->find("first", compact("conditions"));
 		$full_name   = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		if(!empty($commitment)){
			$employee       = $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"];
			$description    = sprintf(__("El usuario %s ha creado un compromiso para el funcionario %s para el día %s."), $full_name, $employee, $commitment["Commitment"]["delivery_date"]);
			$descriptionEng = sprintf(__("The user %s You’ve created a commitment %s for employee for the day %s."), $full_name, $employee, $commitment["Commitment"]["delivery_date"]);
			$data           = array("date" => $commitment["Commitment"]["delivery_date"], "user" => $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"], "description" => $commitment["Commitment"]["description"]); 
			$this->buildInfoLog($commitment["Commitment"]["id"], $description,  $descriptionEng, json_encode($data), __("Sin modificaciones"), "logCommitmentsContac", $contacId, $commitment["Contac"]["team_id"]); 
		}  
 	} 

 	//CRON PARA DESHABILITAR LOS COMPROMISOS VENCIDOS
 	public function disabled_commitments_expired(){
 		$this->autoRender = false;
 		set_time_limit(300000);
 		$conditions  = array("Commitment.delivery_date <" => date('Y-m-d'), "Commitment.state <>" => configure::read("COMMITMENT_DONE"));
 		$recursive   = -1;
 		$commitments = $this->Commitment->find("all", compact("conditions","recursive"));
 		if(!empty($commitments)){
 			$commitmentsIds = Set::extract($commitments, '{n}.Commitment.id');
 			$this->Commitment->updateAll(
	            array('Commitment.state' => Configure::read("COMMITMENT_EXPIRED")), 
	            array('Commitment.id'    => $commitmentsIds)
	        );
 		} 
 	}

 	private function __checkConfigurationsCommitments(){
 		$this->loadModel("CommitmentsConfiguration");
 		$recursive          = -1;
 		$conditions         = array("CommitmentsConfiguration.hour !=" => NULL, "CommitmentsConfiguration.user_id" => AuthComponent::user("id"));
		$hourConfigurations = $this->CommitmentsConfiguration->find("first", compact("conditions","recursive"));
		$conditions         = array("CommitmentsConfiguration.time !=" => NULL, "CommitmentsConfiguration.user_id" => AuthComponent::user("id"));
		$timeConfigurations = $this->CommitmentsConfiguration->find("first", compact("conditions","recursive"));
		return array("timeConfigurations" => $timeConfigurations, "hourConfigurations" => $hourConfigurations);
 	}

 	public function complete_commitment(){

 		$this->layout     = 'ajax';
		$this->autoRender = false; 
		
		$response = 0;

		$this->Commitment->recursive = -1;

		$commitment = $this->Commitment->findById(EncryptDecrypt::decrypt($this->request->data["dataId"]));

		$commitment["Commitment"]["state"] = Configure::read("COMMITMENT_DONE");

		$this->Commitment->id = EncryptDecrypt::decrypt($this->request->data["dataId"]);

		$this->Commitment->save($commitment);

		$this->Flash->success(__('El compromiso se ha marcado como realizado correctamente.'));

		$this->outJSON($this->Commitment->id); 
 	}
}
