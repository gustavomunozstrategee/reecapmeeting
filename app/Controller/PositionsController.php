<?php
App::uses('AppController', 'Controller');
 
class PositionsController extends AppController {
 
	public $components = array('Paginator');

	private function __validatePlanActiveAction(){
        $cachePermission = $this->getCachePermissions();
        if(empty($cachePermission["PlanUser"]) || !empty($cachePermission["PlanUserExpired"])){
        	// $this->Flash->fail(__('Para crear roles de empresa primero debes comprar un plan.')); 
        	return $this->redirect(array('action' => 'index','controller' => 'teams'));
        } 
	}
 
	public function index() { 
		$this->validateTeamSession(); 
		$this->__validatePlanActiveAction();
		$conditions   = array("NOT" => array("Position.id" => 1), "Position.team_id" => Configure::read("Application.team_id"));
		$conditions[] = $this->Position->buildConditions($this->request->query);
		$order        = array('Position.created DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
        $positions 	  = $this->Paginator->paginate();  
		$this->set(compact('positions'));  
	}
 
	public function view($id = null) {
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Position->exists($id)) {
			$this->showMessageExceptions();
		}
		$options = array('conditions' => array('Position.' . $this->Position->primaryKey => $id));
		$this->set('position', $this->Position->find('first', $options));
	}
 
	public function add($step = null) {
		$this->validateTeamSession();
		$this->__validatePlanActiveAction();
		$stepApp = $this->getCacheStep();
		if ($this->request->is('post')) { 

			$validations = $this->getValidates($this->request->data,'Position');
			$this->request->data["Position"]["user_id"] = authcomponent::user("id");
			$this->Position->create();
			$permissions = $this->Session->read("Permissions_rol");
			if(!empty($permissions)){
				if ($this->Position->save($this->request->data)) {
					$this->__saveAllPermission($this->Position->id, $permissions); 
					$this->Session->write("Permissions_rol", array());
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					if($stepApp == 1){
						return $this->redirect(array('action' => 'add'));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				} else {
					$this->Flash->fail(implode('<br>', $validations));
				}
			} else {
				$this->Flash->fail(__('Se debe seleccionar mínimo un permiso para este rol.')); 
			}
		}
		$this->__buildListAdd($step);
	}
 
	public function edit($id = null) {
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Position->exists($id)) {
			$this->showMessageExceptions();
		}
		if ($this->request->is(array('post', 'put'))) {
			$validations = $this->getValidates($this->request->data,'Position');
			$permissions = $this->__checkPositionHavePermissions($id);
			if(!empty($permissions)){
				if ($this->Position->save($this->request->data)) { 
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->fail(implode('<br>', $validations));
				} 
			} else {
				$this->Flash->fail(__('Se debe seleccionar mínimo un permiso para este rol.')); 
			}
		} else {
			$this->__putRequestData($id); 
		}
		$this->__buildListEdit($id); 
	}
 
	private function __buildListAdd($step = null){

		$not_ids     = array("17","18");

		$stepApp     = $this->getCacheStep();
		$permissions = $this->Session->read("Permissions_rol");

		$fields  	 = array("Restriction.id","Restriction.name");
		$conditions  = array("NOT" => array("Restriction.id" => $not_ids));
		$actions 	 = $this->Position->Restriction->find('all', compact("fields","conditions"));
		if(!empty($permissions)){
			$permissionId = Set::extract($permissions, '{n}.Restriction.id');
			$permissionId[] = 17;
			$permissionId[] = 18;
			$conditions   = array("Restriction.id" => $permissionId);
			$permission_sessions = $this->Position->Restriction->find('all', compact("fields","conditions"));
			$conditions   = array("NOT" => array("Restriction.id" => $permissionId));
			$actions      = $this->Position->Restriction->find('all', compact("fields","conditions"));
		} else {
			$permission_sessions = array();
		}
		$this->loadModel("Team");
		$conditions    = array("Team.user_id" => authcomponent::user("id"));
		$teams         = $this->Team->find('list', compact("conditions")); 
		$totalPosition = 0;
		if($stepApp == configure::read("ENABLED")){
			$totalPosition = $this->Position->getTotalPosition(); 
		} 		 
		// echo "<pre>";
		// print_r($actions);
		// die();
		$this->set(compact('teams', 'actions','permission_sessions','step','totalPosition','stepApp'));
	} 

	public function store_permission_choose(){
		$this->autoRender = false;
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax';
        $permissions_session = $this->Session->read("Permissions_rol");
        $permission_id = isset($this->request->data["permission_id"]) ? $this->request->data["permission_id"] : NULL;
       	if (!$this->Position->Restriction->exists($permission_id)) {
    	 	$this->showMessageExceptions();
        }
     	$permissions["Restriction"] = array(
     		"id" => $permission_id
 		);
 		$this->Session->write("Permissions_rol.{$permission_id}", $permissions); 
	}

	public function delete_permission_choose(){
		$this->autoRender = false;
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax';
        $permissions_session = $this->Session->read("Permissions_rol");
        $permission_id = isset($this->request->data["permission_id"]) ? $this->request->data["permission_id"] : NULL;
     	if (!$this->Position->Restriction->exists($permission_id)) {
    	 	$this->showMessageExceptions();
        }
        unset($permissions_session[$permission_id]);
        $this->Session->write("Permissions_rol", $permissions_session); 
	}

	private function __saveAllPermission($positionId, $permissions = array()){ 
		$data = array();
		foreach ($permissions as $permission) {
			$data[] = array(
				"position_id"    => $positionId,
				"restriction_id" => $permission["Restriction"]["id"]
			);	 
		}
		$this->Position->PositionsRestriction->saveAll($data); 
	}

	private function __buildListEdit($id = null){
		//Consultamos los permisos actuales
		$conditions  = array("PositionsRestriction.position_id" => $id);
		$fields      = array("PositionsRestriction.restriction_id");
		$permisionIdsActual = $this->Position->PositionsRestriction->find('list', compact("fields","conditions"));
		//Consultamos los permisos que ya estan asignados
		$conditions  = array("Restriction.id" => $permisionIdsActual);
		$fields  	 = array("Restriction.id","Restriction.name");
		$assignedPermissions = $this->Position->Restriction->find('all', compact("fields","conditions")); 
		//No incluimos las que ya tiene asignado el rol sobre la consulta que trae todos los permisos en general de la tabla de restrictions
		$permisionIdsActual["17"] = "17"; 
		$permisionIdsActual["18"] = "18"; 
		$conditions  = array("NOT" => array("Restriction.id" => $permisionIdsActual));
		$fields  	 = array("Restriction.id","Restriction.name");
		$permissions = $this->Position->Restriction->find('all', compact("fields","conditions"));
		$this->loadModel("Team"); 
		$conditions  = array("Team.user_id" => authcomponent::user("id"));
		$teams       = $this->Team->find('list', compact("conditions")); 
		$this->set(compact('teams', 'permissions','assignedPermissions','id'));
	}

	public function save_permission_edit(){
		$this->autoRender = false;
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax';
       	$permission_id = isset($this->request->data["permission_id"]) ? $this->request->data["permission_id"] : NULL;
       	$position_id   = isset($this->request->data["position_id"])   ? EncryptDecrypt::decrypt($this->request->data["position_id"]) : NULL;
 		if (!$this->Position->Restriction->exists($permission_id) || !$this->Position->exists($position_id)) {
    	 	$this->showMessageExceptions();
        }
     	$permission["PositionsRestriction"] = array(
            "position_id"    => $position_id,
            "restriction_id" => $permission_id
        );
        $this->Position->PositionsRestriction->create();
        $this->Position->PositionsRestriction->save($permission);
     	$this->updateCachePermissions();
	}

	public function delete_permission_edit(){
		$this->autoRender = false;
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax';
        $this->layout  = 'ajax';
       	$permission_id = isset($this->request->data["permission_id"]) ? $this->request->data["permission_id"] : NULL;
       	$position_id   = isset($this->request->data["position_id"])   ? EncryptDecrypt::decrypt($this->request->data["position_id"]) : NULL;
 		if (!$this->Position->Restriction->exists($permission_id) || !$this->Position->exists($position_id)) {
    	 	$this->showMessageExceptions();
        }
    	$this->Position->PositionsRestriction->deleteall(array('position_id' => $position_id, 'restriction_id' => $permission_id));
		$this->updateCachePermissions();
	}

	private function __checkPositionHavePermissions($id = null){
		$conditions  = array("PositionsRestriction.position_id" => $id);
		$permissions = $this->Position->PositionsRestriction->find("all", compact("conditions"));
		return $permissions; 
	}
  
	private function __putRequestData($id){ 
		$options = array('conditions' => array('Position.' . $this->Position->primaryKey => $id));
		$this->request->data = $this->Position->find('first', $options); 
	} 
}
