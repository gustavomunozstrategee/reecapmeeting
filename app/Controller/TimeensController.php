<?php
App::uses('AppController', 'Controller');
CakePlugin::load('PhpExcel');

class TimeensController extends AppController {

        public $components = array('Paginator','PhpExcel.PhpExcel'); 
        

        public function add() { 
            $this->loadModel('Project');
            $this->Project->recursive = 2;
            $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
            $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
            $this->set(compact("projects"));

        }

        public function addTime() { 
            $this->layout = false;
            $this->autoRender = false;
            if ($this->request->is('post')) {
                if($this->request->data["Timeen"]["project_id"] == ""){
                    return "Debe seleccionar un proyecto";
                }if($this->request->data["Timeen"]["descripcion"] == ""){
                    return "Debe ingresar una descripcion";
                }elseif($this->request->data["Timeen"]["fecha"] == ""){
                    return "Debe indicar la fecha de actividad";
                }elseif($this->request->data["Timeen"]["hora_inicio"] == ""){
                    return "Debe indicar la hora inicio";
                }elseif($this->request->data["Timeen"]["hora_fin"] == ""){
                    return "Debe indicar la hora fin";
                }elseif($this->request->data["Timeen"]["hora_fin"] < $this->request->data["Timeen"]["hora_inicio"]){
                    return "La hora de finalización no puede ser menor a la de inicio";
                }
                $this->request->data["Timeen"]["user_id"] = AuthComponent::user("id");
                if($this->Timeen->save($this->request->data)){
                    return "1";
                }else{
                    return "0";
                }
            }
        }


        public function index() { 
            if ($this->request->is('post')) {
           
                $rangoFechas  = $this->request->data["fechas"];
                $rangoFechas = explode(" - ", $rangoFechas);
           
              
                $this->Session->write('timeen_filter_init',$rangoFechas[0]);
                $this->Session->write('timeen_filter_finish',$rangoFechas[1]);
               
            }else{
                $fecha_actual = date("Y-m-d");
                $this->Session->write('timeen_filter_init',date("Y-m-d",strtotime($fecha_actual."- 30 days")));
                $this->Session->write('timeen_filter_finish',$fecha_actual);
                $rangoFechas[] = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
                $rangoFechas[] = $fecha_actual;
                $this->request->data["fechas"] = date("Y-m-d",strtotime($fecha_actual."- 30 days"))." - ". $fecha_actual;
            
            }

             $this->set(compact("rangoFechas"));

        }

        public function loadContent() {

            if(!empty($this->Session->read('timeen_filter_init'))){
                 
                $fechas = $this->Timeen->find('all',array(
                            'conditions' => array(
                                "Timeen.user_id" =>  AuthComponent::user("id"),
                                "Timeen.fecha >=" => $this->Session->read('timeen_filter_init'), 
                                "Timeen.fecha <=" => $this->Session->read('timeen_filter_finish'), 
                            ),
                            'group' => ['Timeen.fecha'],
                            'order' => ['Timeen.fecha DESC']
                        ));
            }else{

                
                $fechas = $this->Timeen->find('all',array(
                            'conditions' => array(
                                "Timeen.user_id" =>  AuthComponent::user("id"),
                            ),
                            'group' => ['Timeen.fecha'],
                            'order' => ['Timeen.fecha DESC']
                        ));
            }


            
            $timeens = array();
            foreach ($fechas as $key => $value) {
               
               $timeens[$key] = array(
                    "dia" => $value["Timeen"],
                    "informacion" => $this->Timeen->find('all',array(
                        "conditions" => array(
                            "Timeen.user_id" =>  AuthComponent::user("id"),
                            "Timeen.fecha" => $value["Timeen"]["fecha"]),
                            "order"=> ["Timeen.hora_inicio DESC"]
                        )
                    )

               );
              
            }
            $this->set(compact("timeens"));
        }

        public function edit($id) { 
            if (!$this->Timeen->exists($id)) {
                throw new NotFoundException(__('Invalid slates task'));
            }

            if ($this->request->is(array('post', 'put'))) {
                $this->request->data["Timeen"]["id"] = $id;
                $this->request->data["Timeen"]["edited"] = 1;
                $this->layout     = false;
                $this->autoRender = false;

                if($this->request->data["Timeen"]["project_id"] == ""){
                    return "Debe seleccionar un proyecto";
                }if($this->request->data["Timeen"]["descripcion"] == ""){
                    return "Debe ingresar una descripcion";
                }elseif($this->request->data["Timeen"]["fecha"] == ""){
                    return "Debe indicar la fecha de actividad";
                }elseif($this->request->data["Timeen"]["hora_inicio"] == ""){
                    return "Debe indicar la hora inicio";
                }elseif($this->request->data["Timeen"]["hora_fin"] == ""){
                    return "Debe indicar la hora fin";
                }elseif($this->request->data["Timeen"]["hora_fin"] < $this->request->data["Timeen"]["hora_inicio"]){
                    return "La hora de finalización no puede ser menor a la de inicio";
                }
                if ($this->Timeen->save($this->request->data)) {
                    return "1";
                } else {
                   return "0";
                }
            } else {
                $this->loadModel('Project');
                $this->Project->recursive = 2;
                $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
                $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
                $this->set(compact("projects"));
                $options = array('conditions' => array('Timeen.' . $this->Timeen->primaryKey => $id));
                $this->request->data = $this->Timeen->find('first', $options);

                $this->request->data['Timeen']['hora_inicio'] = substr($this->request->data['Timeen']['hora_inicio'],0,-3);
                $this->request->data['Timeen']['hora_fin']    = substr($this->request->data['Timeen']['hora_fin'],0,-3);
            }
                
        }

        public function report(){

             if ($this->request->is('post')) {
                 
                if($this->request->data["user_id"] == ""){
                    $this->Session->write('timeen_filter_users',array(AuthComponent::User("id")));
                }else{
                    $ids = array();
                    foreach ($this->request->data["user_id"] as $key => $value) {
                        $ids[] = $value;
                    }
                     $this->Session->write('timeen_filter_users',$ids);
                }
                if($this->request->data["project_id"] == ""){
                    $this->Session->write('timeen_filter_projects',"all");
                }else{
                    $ids = array();
                    foreach ($this->request->data["project_id"] as $key => $value) {
                        $ids[] = $value;
                    }
                     $this->Session->write('timeen_filter_projects',$ids);
                }
                $rangoFechas  = $this->request->data["fechas"];
                $rangoFechas = explode(" - ", $rangoFechas);
                $this->Session->write('timeen_filter_init',$rangoFechas[0]);
                $this->Session->write('timeen_filter_finish',$rangoFechas[1]);
               
            }else{
                $this->Session->delete('timeen_filter_users');
                $this->Session->delete('timeen_filter_init');
                $this->Session->delete('timeen_filter_finish');

            }

             //////////////////////////////USUARIOS PERMITIDOS////////////////////////
            $this->loadModel('Project');
            $this->Project->recursive = 2;
            $projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
            $projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
            $this->set(compact("projects"));

            $this->loadModel('UserTeam');
            $conditions = array(
                'UserTeam.team_id' => Configure::read("Application.team_id"),
                "UserTeam.user_id" => AuthComponent::User("id")
            );
            $userRolePermission = $this->UserTeam->find("first",compact("conditions")); 
            $userRolePermission = $userRolePermission["UserTeam"];
            $this->loadModel('Position');
            $conditions = array('Position.id' => $userRolePermission["position_id"]);
            $roleInfo   = $this->Position->find("first",compact("conditions")); 
            if(empty($roleInfo)) {
                $this->Flash->fail(__('No tienes permisos para realizar esta acción.'));
                $this->redirect(array('action' => 'index','controller' => 'contacs')); 
            }
            $userRolePermission["Position"] = $roleInfo["Position"];
            //SE VALIDAN LOS PERMISOS DE ROLE
            $users     = $this->list_user_add_commitment($roleInfo["Position"]["permission_taskee"],Configure::read("Application.team_id"),$userRolePermission["department_id"]);
            $ids_query = array();
            foreach ($users as $key => $value) {
                $ids_query[] = $key;
            }
            $this->set(compact('users','rangoFechas'));
        }

        public function reportLoad() {
            if(!empty($this->Session->read('timeen_filter_init'))){
                $conditions = array(
                    "Timeen.user_id" => AuthComponent::user("id")
                );
                $fechas = $this->Timeen->find('all',array(
                            'conditions' => array(
                                "Timeen.user_id" =>  $this->Session->read('timeen_filter_users'),
                                "Timeen.fecha >=" => $this->Session->read('timeen_filter_init'), 
                                "Timeen.fecha <=" => $this->Session->read('timeen_filter_finish'), 
                            ),
                            'group' => ['Timeen.fecha'],
                            'order' => ['Timeen.fecha DESC']
                        ));
                
            }else{
                $conditions = array(
                    "Timeen.user_id" => AuthComponent::user("id")
                );
                $fechas = $this->Timeen->find('all',array(
                            'conditions' => array(
                                "Timeen.user_id" =>  AuthComponent::user("id"),
                                "Timeen.fecha >=" => date("Y-m-d"), 
                                "Timeen.fecha <=" => date("Y-m-d"), 
                            ),
                            'group' => ['Timeen.fecha'],
                            'order' => ['Timeen.fecha DESC']
                        ));
            }
            $this->Timeen->recursive = 2;
            $timeens = array();
            foreach ($fechas as $key => $value) {
               if($this->Session->read('timeen_filter_projects') != "all"){
                $timeens[$key] = array(
                    "dia" => $value["Timeen"],
                    "informacion" => $this->Timeen->find('all',array(
                        "conditions" => array(
                            "Timeen.fecha" => $value["Timeen"]["fecha"],
                            "Timeen.project_id" =>  $this->Session->read('timeen_filter_projects'),
                            "Timeen.user_id" =>  $this->Session->read('timeen_filter_users'),
                        ),

                        "order"=> ["Timeen.hora_inicio DESC"]))

               );
                }else{
                    $timeens[$key] = array(
                    "dia" => $value["Timeen"],
                    "informacion" => $this->Timeen->find('all',array(
                        "conditions" => array(
                            "Timeen.fecha" => $value["Timeen"]["fecha"],
                            "Timeen.user_id" =>  $this->Session->read('timeen_filter_users'),
                        ),

                        "order"=> ["Timeen.hora_inicio DESC"]))

                    );

                }
            }
            $this->set(compact("timeens"));
        }

        public function generate_report(){
        if ($this->request->is('post')) {
            ///OBTENIENDO INFORMACION DE TIMEEN
            
            if(!empty($this->Session->read('timeen_filter_init'))){
            
            
            
                $conditions = array(
                    "Timeen.user_id" => AuthComponent::user("id")
                );
                $fechas = $this->Timeen->find('all',array(
                            'conditions' => array(
                                "Timeen.user_id" =>  $this->Session->read('timeen_filter_users'),
                                "Timeen.fecha >=" => $this->Session->read('timeen_filter_init'), 
                                "Timeen.fecha <=" => $this->Session->read('timeen_filter_finish'), 
                            ),
                            'group' => ['Timeen.fecha'],
                            'order' => ['Timeen.fecha DESC']
                        ));
            }else{
                $conditions = array(
                    "Timeen.user_id" => AuthComponent::user("id")
                );
                $fechas = $this->Timeen->find('all',array(
                            'conditions' => array(
                                "Timeen.user_id" =>  AuthComponent::user("id"),
                                "Timeen.fecha >=" => date("Y-m-d"), 
                                "Timeen.fecha <=" => date("Y-m-d"), 
                            ),
                            'group' => ['Timeen.fecha'],
                            'order' => ['Timeen.fecha DESC']
                        ));
            }
            $this->Timeen->recursive = 2;
            $timeens = array();
            foreach ($fechas as $key => $value) {
               if($this->Session->read('timeen_filter_projects') != "all"){
                $timeens[$key] = array(
                    "dia" => $value["Timeen"],
                    "informacion" => $this->Timeen->find('all',array(
                        "conditions" => array(
                            "Timeen.fecha" => $value["Timeen"]["fecha"],
                            "Timeen.project_id" =>  $this->Session->read('timeen_filter_projects'),
                            "Timeen.user_id" =>  $this->Session->read('timeen_filter_users'),
                        ),

                        "order"=> ["Timeen.hora_inicio DESC"]))
               );
                }else{
                    $timeens[$key] = array(
                    "dia" => $value["Timeen"],
                    "informacion" => $this->Timeen->find('all',array(
                        "conditions" => array(
                            "Timeen.fecha" => $value["Timeen"]["fecha"],
                            "Timeen.user_id" =>  $this->Session->read('timeen_filter_users'),
                        ),

                        "order"=> ["Timeen.hora_inicio DESC"]))

                    );

                }
            }
            //////////////////////
            Configure::write('debug', 0);
            $this->autoRender = false;
            $this->PhpExcel->createWorksheet();
            $this->PhpExcel->setDefaultFont('Calibri', 16);
            $header = array(
                array('label' => __('Fecha'), 'width' => '20'),
                array('label' => __('Tiempo'), 'width' => '20'),
                array('label' => __('Descripción'), 'width' => '50'),
                array('label' => __('Funcionario'), 'width' => '20'),
                array('label' => __('Cliente / Proyecto'), 'width' => '20'),
                array('label' => __('Duración'), 'width' => '20'),
                array('label' => __('Tiempo Tota / Horas'), 'width' => '20'),
            );
            $this->PhpExcel->addTableHeader($header, array('name' => 'Cambria', 'bold' => true));
            $limit = 100000;
             
            foreach ($timeens as $key => $value){
                foreach ($value["informacion"] as $key2 => $value2){
                    $horaInicio = new DateTime($value2["Timeen"]["hora_inicio"]);
                    $horaTermino = new DateTime($value2["Timeen"]["hora_fin"]);
                    $interval = $horaInicio->diff($horaTermino);
                    $this->PhpExcel->addTableRow(
                    array(
                        (h( $value["dia"]["fecha"])),
                        h($horaInicio->format('H:i')." - ".$horaTermino->format('H:i')),
                        h( $value2["Timeen"]["descripcion"]),
                        h($value2["User"]["firstname"]." ".$value2["User"]["lastname"]),
                        h("(".$value2["Project"]["Client"]["name"].") - ".$value2["Project"]["name"]),
                        h($interval->format('%Hh %Im')),
                         h($this->calcular_tiempo_trasnc($value2["Timeen"]["hora_fin"],$value2["Timeen"]["hora_inicio"])),
                    )
                );

                }

            }

 
             
             
            $this->PhpExcel->output('Timeen'.date("Y_m_d").'.xlsx');        
        } 
    }

    private function calcular_tiempo_trasnc($hora1,$hora2){
            $separar[1]=explode(':',$hora1);
            $separar[2]=explode(':',$hora2);

        $total_minutos_trasncurridos[1] = ($separar[1][0]*60)+$separar[1][1];
        $total_minutos_trasncurridos[2] = ($separar[2][0]*60)+$separar[2][1];
        $total_minutos_trasncurridos = $total_minutos_trasncurridos[1]-$total_minutos_trasncurridos[2];
         return $total_minutos_trasncurridos / 60;
         

        
    }


    private function list_user_add_commitment($role_permission, $team_id,$department_id){
        //1. CUANDO EL ROL ES ADMINISTRADOR GENERAL
        if($role_permission == 1){
            $users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ', U.lastname) as name
            FROM users as U 
            JOIN user_teams as UT ON UT.user_id = U.id 
            JOIN departments D ON UT.department_id = D.id  
            JOIN teams T ON UT.team_id = T.id
            WHERE UT.team_id = $team_id AND type_user = '1' AND UT.state = 1 AND UT.user_state = 0 AND U.firstname != ''  ");
            $users = Hash::combine($users,  '{n}.U.id','{n}.0.name') ;
            return $users;
            //CUANDO ES ADMIN DE AREA
        }elseif($role_permission == 2){
            $users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ',U.lastname) as name
            FROM users as U 
            JOIN user_teams as UT ON UT.user_id = U.id 
            JOIN departments D ON UT.department_id = D.id  
            JOIN teams T ON UT.team_id = T.id
            WHERE UT.team_id = $team_id
            AND U.firstname != ''
            AND D.id = $department_id
            AND UT.state = 1 AND UT.user_state = 0
            ");
            $users = Hash::combine($users,  '{n}.U.id','{n}.0.name') ;
            return $users;
            //CUANDO ES PERMISO NORMAL
        }else{
            $id = AuthComponent::User("id");
            $users =  $this->Project->query("SELECT U.id,CONCAT(U.firstname,' ',U.lastname) as name
            FROM users as U 
            JOIN user_teams as UT ON UT.user_id = U.id 
            JOIN departments D ON UT.department_id = D.id  
            JOIN teams T ON UT.team_id = T.id
            WHERE UT.team_id = $team_id
            AND U.firstname != ''
            AND D.id = $department_id
            AND U.id = $id
            AND UT.state = 1 AND UT.user_state = 0
            ");
            $users = Hash::combine($users,  '{n}.U.id','{n}.0.name') ;
            return $users;
        }
        //2. CUANDO EL ROL ES DIRECTOR DE AREA, O CON PERMISOS DE DIRECCION DE AREA.
        //3. CUANDO ES UN USUARIO SIN PERMISOS DE DIRECCION DE AREA.
        return array();
    }


    public function delete($id) { 
            $this->layout = false;
            $this->autoRender = false;
            if (!$this->Timeen->exists($id)) {
                throw new NotFoundException(__('Invalid slates task'));
            }
             $this->Timeen->id = $id;

            $this->Timeen->delete();

        }



}

