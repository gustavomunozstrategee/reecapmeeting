<?php
App::uses('AppController', 'Controller');
 
class UserTeamsController extends AppController {
 
	public $components = array('Paginator');

	private function __validatePlanActiveAction(){
        $cachePermission = $this->getCachePermissions();
        if(empty($cachePermission["PlanUser"]) || !empty($cachePermission["PlanUserExpired"])){
        	// $this->Flash->fail(__('Para crear usuarios de empresa primero debes comprar un plan.')); 
        	return $this->redirect(array('action' => 'index','controller' => 'teams'));
        } 
	}

	public function edit_role($id = null){
		$this->validateTeamSession();
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->UserTeam->exists($id)) {
			$this->showMessageExceptions();
		} 
		$recursive     = 1;
		$conditions    = array("UserTeam.id" => $id);
		$userTeam      = $this->UserTeam->find("first", compact("conditions","recursive"));

		$departments = array();
		$this->loadModel("Department");
		$departments = $this->Department->find("list");


		$role_selected = $userTeam["UserTeam"]["position_id"];
		$department_selected = $userTeam["UserTeam"]["department_id"]; 
		if($userTeam["UserTeam"]["team_id"] == NULL){
			$this->showMessageExceptions();
		}
		if ($this->request->is(array('post', 'put'))) {
			if(empty($this->request->data["UserTeam"]["position_id"])){
				$role_selected = "";
				$this->Flash->fail(__('Se debe seleccionar un rol.'));
			} else {
				$this->request->data["UserTeam"]["id"] = $id;
				/*$this->UserTeam->updateAll(
	            	array("UserTeam.position_id" => $this->request->data["UserTeam"]["position_id"]),
	            	array("UserTeam.department_id" => $this->request->data["UserTeam"]["department_id"]),
	            	array('UserTeam.id' 		 => $id)
	    		);
	    		*/
	    		$this->UserTeam->save($this->request->data["UserTeam"]);
	    		 
	    		 
	    		$user_id = $userTeam["UserTeam"]["user_id"];
    		 	Cache::delete("permissions_{$user_id}", 'PERMISSIONS');  
	    		$this->Flash->success(__('Los datos se han guardado correctamente.'));
	    		return $this->redirect(array('action' => 'index')); 
			} 
		}
		$positions = $this->__listPositions();
		$this->set(compact("positions","role_selected","userTeam",'departments','department_selected'));
	}
 
	public function index() { 
		$this->validateTeamSession();
		// $this->__validatePlanActiveAction();
		$teams        = $this->loadTeams();
		$order        = array("Team.name DESC");
		$conditions   = array( "User.id !=" =>  AuthComponent::user('id')); 
		$conditions["UserTeam.team_id"] = EncryptDecrypt::decrypt($this->Session->read("TEAM"));
		$conditions[] = $this->UserTeam->buildConditions($this->request->query); 
		$group        = array("UserTeam.user_id");

		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order','group');
        $userTeams 	 = $this->Paginator->paginate(); 
		$this->set(compact('userTeams','teams')); 
	}

	public function all_my_teams() { 
		$teams        = $this->loadTeams();
		$order        = array("Team.name DESC");
		$conditions   = array('UserTeam.team_id' => array_keys($teams));  
		$conditions[] = $this->UserTeam->buildConditions($this->request->query); 
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
        $userTeams 	 = $this->Paginator->paginate(); 
		$this->set(compact('userTeams','teams')); 
	}
 
	public function add($step = null) {
		$this->validateTeamSession();
		// $this->__validatePlanActiveAction();
		$stepApp   = $this->getCacheStep();
		if ($this->request->is('post')) {  
			$response = $this->__checkInfo($this->request->data); 
			if($response['state'] == true) {
				if($this->request->data['UserTeam']['type_user'] == Configure::read('COLLABORATOR')){
					$this->request->data['UserTeam']['client_id']   = NULL;
					$this->request->data['UserTeam']['team_id'] 	= array($this->request->data['UserTeam']['team_id']);
				} else { 
					$this->request->data['UserTeam']['position_id'] = NULL;
					$this->request->data['UserTeam']['team_id'] 	= array($this->request->data['UserTeam']['team_id']);
				} 
				$user = $this->__checkUserExist($this->request->data['UserTeam']['email'], $this->request->data['UserTeam']['team_id'], $this->request->data['UserTeam']['type_user'], $this->request->data['UserTeam']['client_id']);
				if($user['state'] == true){
					if(!empty($user['user'])) {
						$this->__saveInfoUserAssigned($this->request->data, $user['user']['User']['id']);
					} else {
						$this->__saveInfoUserAssigned($this->request->data); 
					}
					$this->__deleteUserFromRemoved();
					$this->updateCachePermissions(); 
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					if($stepApp == 1){
						return $this->redirect(array('action' => 'index'));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				} else { 
					$this->Flash->fail(__($user['message']['0']));
					return $this->redirect(array('action' => 'add', $step));
				}  
			} else {
				$this->Flash->fail(implode('<br>', $response['message']));
			}
		}
		$this->__buildList($step);
	}
 
	private function __buildList($step = null){ 
		$recursive      = -1;
		$conditions     = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("ENABLED"), "PlanUser.expired" => Configure::read("DISABLED"));
		$planActualInfo = $this->UserTeam->Team->User->PlanUser->find("first", compact("conditions","recursive"));
		$removeUserId   = $this->Session->read("ActivateUser"); 
		$stepApp   	    = $this->getCacheStep();
		$totalUsers     = 0;
		if($stepApp == configure::read("ENABLED")){
			$totalUsers     = $this->UserTeam->getTotalUser(); 
		}
		$this->set(compact('planActualInfo','removeUserId','step','stepApp','totalUsers'));  
	} 

	public function load_positions(){ 
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout  = 'ajax';
		$teams 	       = array();
		$positions     = array();
		$clients       = array();
		$type  	       = isset($this->request->data["typeUser"]) ? $this->request->data["typeUser"] : NULL;  
		// $teamId = $this->getTeamIds();
		$teamId = EncryptDecrypt::decrypt($this->Session->read("TEAM"));
		if($type == configure::read("COLLABORATOR")){
			$this->loadModel('Department');
			// $teams     = $this->UserTeam->getAllTeams();
			$teams       = array($teamId => $this->Session->read("TEAM_NAME"));
			$positions   = $this->UserTeam->Position->loadAllPositions($teamId);
			$departments = $this->Department->loadDepartments();
		} else {

			$conditions   = array("Client.team_id" => $teamId);
			$fields       = array("Client.id","Client.name");
			$clients      = $this->UserTeam->Client->find("list", compact("conditions","fields"));
		} 
		$this->set(compact('teams','type','positions','clients','departments'));  
	}

	public function load_team_client(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax';
		$clientId  	  = isset($this->request->data["clientId"]) ? $this->request->data["clientId"] : NULL;
		$conditions   = array('Client.id' => $clientId);  
		$fields       = array("Team.id","Team.name");
		$client       = $this->UserTeam->Client->find("first", compact("conditions")); 
		$team         = array();
		if(!empty($client)){
			$team[$client["Team"]["id"]] = $client["Team"]["name"];
		} 
		$this->set(compact('team')); 
	}

	private function __listPositions(){
		// $conditions    = array("Position.user_id" => AuthComponent::user("id"), "NOT" => array("Position.id" => 1));
		$conditions    = array("Position.team_id" => EncryptDecrypt::decrypt($this->Session->read("TEAM")), "NOT" => array("Position.id" => 1));
		$fields        = array("Position.id","Position.name");
		$positionsUser = $this->UserTeam->Position->find("all", compact("fields","conditions"));
		$positions     = array();
		if(!empty($positionsUser)){
			foreach ($positionsUser as $position) {
				if(!empty($position["Restriction"])){ 
					$positions[$position["Position"]["id"]] = $position["Position"]["name"]; 	 
				}
			}
		}  
		return $positions;
	}

	public function search_user(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$data = array("UserTeam" => array('email' => $this->request->data['email'], 'team_id' => $this->request->data['teamId'], 'type_user' => $this->request->data['type_user']));
		if($data['UserTeam']['type_user'] == Configure::read('COLLABORATOR')){
			unset($this->UserTeam->validate['client_id']);
		} else { 
			$this->request->data['teamId'] = $data['UserTeam']['team_id'];
			$data['UserTeam']['team_id']   = $data['UserTeam']['team_id'];
			unset($this->UserTeam->validate['position_id']);
		}  
		$this->UserTeam->set($data); 
		if ($this->UserTeam->validates(array('fieldList' => array('email','team_id','type_user')))) { 
			if(empty($this->request->data['clientId'])) {
				$this->request->data['clientId'] = null;
			}			
			$response = $this->__checkUserExist($this->request->data['email'], $this->request->data['teamId'], $this->request->data['type_user'], $this->request->data['clientId']); 
		} else {
			$errors   = $this->UserTeam->validationErrors;  
            foreach ($errors as $key => $error) {
                $errorsValidations[] = $error[0];
            }
			$response = array('state' => false, 'message' => $errorsValidations);
		}
		$this->outJSON($response);
	}

	private function __checkUserExist($email, $teamId = array(), $type, $clientId = null){
		$conditions = array('User.email' => $email);
		$recursive  = 0;
		$user       = $this->UserTeam->User->find('first', compact('conditions','recursive')); 
		$response   = $this->__checkNumberUserAvailable($teamId, $type); 
		if($response['state'] == true){
			if(!empty($user)){
				if($user['User']['role_id'] == Configure::read('ADMIN_ROLE_ID')){
					$response = array('message' => array(__('Este usuario no se puede añadir porque es un usuario administrador.')), "state" => false);	
				} else {
					$this->UserTeam->unbindModel(array('belongsTo' => array('Client','Position','OwnUser','User')));
					$conditions = array('UserTeam.user_id' => $user['User']['id'], 'UserTeam.team_id' => $teamId);
					if ($type == Configure::read('EXTERNAL') && !empty($clientId)) {
						$conditions['client_id'] = $clientId;
					}
					$userInTeam = $this->UserTeam->find('all', compact('conditions','recursive'));
					if(!empty($userInTeam)){ 
						$teams    = Set::extract($userInTeam, '{n}.Team.name');
					 	$response = array('message' => array(sprintf(__('Este usuario ya esta asignado en la empresa: %s.'), implode(", ", $teams))), 'state' => false);
					} else {
						$response = array('message' => __('Invitar usuario'), 'state' => true, 'user' => $user); 
					} 
				}
			} else {
				$response = array('message' => __('Invitar usuario'), 'state' => true, 'user' => array());
			} 
			return $response;
		} else {
			return $response;
		}
	}

	private function __checkNumberUserAvailable($teamId, $type){
		$conditions  = array('PlanUser.user_id' => AuthComponent::user('id'), 'PlanUser.state' => Configure::read('ENABLED'), 'PlanUser.expired' => Configure::read('DISABLED'));
		$planActual  = $this->UserTeam->User->PlanUser->find('first', compact('conditions'));
	 	$conditions  = array('UserTeam.team_id' => $teamId, 'UserTeam.type_user' => configure::read('COLLABORATOR'));
	 	$group       = array('UserTeam.team_id');
	 	$fields      = array('UserTeam.*','Team.*','COUNT(UserTeam.id) as usuarios'); 
	 	$usersInTeam = $this->UserTeam->find('all', compact('conditions','group','fields'));
	 	$teamFull    = array();
	 	$response    = array();
 		if($type == configure::read('COLLABORATOR')){ 			
 			foreach ($usersInTeam as $user) {
 				if($user['0']['usuarios'] == $planActual['PlanUser']['number_users']){
 					$teamFull[] = $user['Team']['name']; 
 				} 				
 			}
 			if(!empty($teamFull)){
 				$response = array('message' => array(sprintf(__('Ya superaste el límite de usuarios permitidos en tu plan para asignar en la(s) empresa(s): %s. Por favor selecciona otra(s) empresa(s).'), implode(", ", $teamFull))), "state" => false);
 			} else {
 				$response = array('state' => true);
 			} 
 		} else {
 		 	$response = array('state' => true);
 		} 
	 	return $response; 
	}

	private function __checkInfo($data = array()){ 
		if($data['UserTeam']['type_user'] == 1){
			unset($this->UserTeam->validate['client_id']); 
			$data['UserTeam']['team_id'] = array($data['UserTeam']['team_id']);
			if($data['UserTeam']['department_id'] == ''){
				return $response = array('message' =>  array(__('El área es requerida.')), 'state' => false);
			}
		} else {
			$data['UserTeam']['team_id'] = array($data['UserTeam']['team_id']); 
			if(isset($this->request->data['UserTeam']['client_id']) && count($this->request->data['UserTeam']['client_id']) > 0){
				unset($this->UserTeam->validate['client_id']); 
			}
			unset($this->UserTeam->validate['position_id']); 
		} 
		$validations = $this->getValidates($data, 'UserTeam');  
		if(!is_array($validations)){
			return $response = array('state' => true);
		} else { 
			return $response = array('message' =>  $validations, 'state' => false);
		} 
	}

	public function save_user(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$response = $this->__checkInfo($this->request->data); 
		$this->outJSON($response); 
	}

	private function __saveInfoUserAssigned($data = array(), $userId = null){
		if($data["UserTeam"]["type_user"] == 1){
			$data["UserTeam"]["client_id"]   = NULL; 
		} else {
			$data["UserTeam"]["position_id"] = NULL; 
		}
		$email = $data["UserTeam"]["email"];
		unset($data["UserTeam"]["email"]);  
		if(!is_null($userId)){ 
		 	$this->__assignedUser($data, $userId, $email); 
		} else { 
			$this->__assignedUserNew($data, $email); 
		}
	}

	//CUANDO EL USUARIO NO EXISTE
	private function __assignedUserNew($data = array(), $email){
		$hash 	    = $this->UserTeam->User->generateHashChangePassword();
		$saveData   = array('User' => array('email' => $email, 'state' => Configure::read('DISABLED'), 'hash_change_password' => $hash));
		$usersTeams = array();
		$this->UserTeam->User->create();		 
		$this->UserTeam->User->save($saveData, array('validate'=>false)); 
		foreach ($data['UserTeam']['team_id'] as $team_id) {
			if(!is_array($data['UserTeam']['client_id'])) {
				$usersTeams[] = array(
					'type_user'     => $data['UserTeam']['type_user'],
					'team_id' 	    => $team_id,
					'position_id'   => $data['UserTeam']['position_id'],
					'client_id'     => $data['UserTeam']['client_id'],
					'user_id'       => $this->UserTeam->User->id,
					'own_user_id'   => AuthComponent::user('id'),
					'department_id' => isset($data['UserTeam']['department_id']) ? $data['UserTeam']['department_id'] : 0,
				); 
			} else {				
				foreach ($this->request->data['UserTeam']['client_id'] as $cliente_id) {
					$usersTeams[] = array(
						'type_user'     => $data['UserTeam']['type_user'],
						'team_id' 	    => $team_id,
						'position_id'   => $data['UserTeam']['position_id'],
						'client_id'     => $cliente_id,
						'user_id'       => $this->UserTeam->User->id,
						'own_user_id'   => AuthComponent::user('id'),
						'department_id' => isset($data['UserTeam']['department_id']) ? $data['UserTeam']['department_id'] : 0,
					); 
				}
			}
		}  
	 	if($this->UserTeam->saveAll($usersTeams, array('validate'=>false))) {
	 		foreach ($usersTeams as $user) { 
				$this->__mailAssignationTeam($email, $data['UserTeam']['type_user'], 'user_new', $user['team_id'], $user['client_id']);
 			 	$this->__deleteUserRemoved($this->UserTeam->User->id, $user['team_id']);
	 		}
	 	} 
	}

	//CUANDO YA EXISTE EL USUARIO
	private function __assignedUser($data = array(), $userId, $email){
		$usersTeams = array(); 
		foreach ($data['UserTeam']['team_id'] as $team_id) {  
			if(!is_array($data['UserTeam']['client_id'])) {
				$usersTeams[] = array(
					'type_user'     => $data['UserTeam']['type_user'],
					'team_id' 	    => $team_id,
					'position_id'   => $data['UserTeam']['position_id'],
					'client_id'     => $data['UserTeam']['client_id'],
					'user_id'       => $userId,
					'own_user_id'   => AuthComponent::user('id'),
					'department_id' => isset($data['UserTeam']['department_id']) ? $data['UserTeam']['department_id'] : 0,
				);  
			} else {
				foreach ($this->request->data['UserTeam']['client_id'] as $cliente_id) {
					$usersTeams[] = array(
						'type_user'     => $data['UserTeam']['type_user'],
						'team_id' 	    => $team_id,
						'position_id'   => $data['UserTeam']['position_id'],
						'client_id'     => $cliente_id,
						'user_id'       => $userId,
						'own_user_id'   => AuthComponent::user('id'),
						'department_id' => isset($data['UserTeam']['department_id']) ? $data['UserTeam']['department_id'] : 0,
					); 
				}				
			}
		} 
		if($this->UserTeam->saveAll($usersTeams, array('validate'=>false))) {
			foreach ($usersTeams as $user) {
 				$this->__deleteUserRemoved($userId, $user['team_id']);
	 			$this->__mailAssignationTeam($email, $data['UserTeam']['type_user'], 'user_exist', $user['team_id'], $user['client_id']);
	 		}
		}   
	}
  
	private function __mailAssignationTeam($email, $typeUser, $userState, $teamId, $clienteId = null){
		$user        = AuthComponent::user("firstname") . ' ' . AuthComponent::user("lastname"); 
		$conditions  = array('Team.id' => $teamId);
		$teamName    = $this->UserTeam->Team->field('name', $conditions);
		$recursive   = -1;
		$conditions  = array('User.email' => $email);
		$userInvited = $this->UserTeam->User->find('first', compact('conditions','recursive')); 
		if($typeUser == Configure::read("TYPE_USER.{$typeUser}")){
			$type = Configure::read("TYPE_USER.{$typeUser}");
		} else {
			$type = Configure::read("TYPE_USER.{$typeUser}");
		}
		if($userState == 'user_exist'){
			if($user == " "){
				$subject  = sprintf(__("El remitente <strong>%s</strong> te ha añadido como <strong>%s</strong> a su empresa llamada: <strong>%s</strong>"), AuthComponent::user('email'), $type, $teamName);
			} else {
				$subject = sprintf(__("El usuario <strong>%s</strong> te ha añadido como <strong>%s</strong> a su empresa llamada: <strong>%s</strong>"), $user, $type, $teamName);
			}
		} else {
			if($user == " "){
				$subject  = sprintf(__("El remitente <strong>%s</strong> te ha registrado en <strong>%s</strong> y te ha añadido a su empresa llamada: <strong>%s</strong> como <strong>%s</strong>"), AuthComponent::user('email'), configure::read('Application.name'), $teamName, $type);
			} else {
				$subject  = sprintf(__("El usuario <strong>%s</strong> te ha registrado en <strong>%s</strong> y te ha añadido a su empresa llamada: <strong>%s</strong> como <strong>%s</strong>"), $user, configure::read('Application.name'), $teamName, $type);
			}
		} 
		if(!is_null($clienteId)) {
			$conditions  = array('Client.id' => $clienteId);
			$clienteName = $this->UserTeam->Client->field('name', $conditions);
			$subject .= __(' del cliente '). '<strong>'.$clienteName.'</strong><br>';
		} else {
			$subject .= '<br>';
		}
		$options = array(
			'to'       => $email,
			'template' => 'assignation_team',
			'subject'  => __('Asignación de empresa').' - '.Configure::read('Application.name'),
			'vars'     =>  array(
				'subject'     => $subject, 
				'userState'   => $userState, 
				'correo' 	  => $email, 
				'userInvited' => $userInvited['User']['firstname'] . ' ' . $userInvited['User']['lastname'],
			),
		); 
		$this->sendMail($options);
	}

	public function load_users(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout = "ajax";
	 	$this->UserTeam->unbindModel(array('belongsTo' => array('Client','Position','Team'))); 
		$conditions   = array("UserTeam.own_user_id" => AuthComponent::user("id"), "UserTeam.state" => Configure::read("DISABLED"), 'UserTeam.type_user' => Configure::read("COLLABORATOR"));
		$group        = array("UserTeam.user_id");
		$users        = $this->UserTeam->find("all", compact("conditions","group"));
	 	$planId       = isset($this->request->data["planId"]) ? EncryptDecrypt::decrypt($this->request->data["planId"]) : null;
	 	$conditions   = array("Plan.id" => $planId);
	 	$number_users = $this->UserTeam->User->PlanUser->Plan->field("users", $conditions); 
		$this->Session->write("NumberUser", $number_users);
		$this->set(compact('users')); 
	}

	public function load_users_to_plan_free(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout = "ajax";
	 	$this->UserTeam->unbindModel(array('belongsTo' => array('Client','Position','Team'))); 
		$conditions   = array("UserTeam.own_user_id" => AuthComponent::user("id"), "UserTeam.state" => Configure::read("DISABLED"));
		$group        = array("UserTeam.user_id");
		$users        = $this->UserTeam->find("all", compact("conditions","group"));
	 	$planId       = isset($this->request->data["planId"]) ? EncryptDecrypt::decrypt($this->request->data["planId"]) : null;
	 	$conditions   = array("Plan.id" => $planId);
	 	$number_users = $this->UserTeam->User->PlanUser->Plan->field("users", $conditions); 
		$this->Session->write("NumberUserFree", $number_users);
		$this->set(compact('users')); 
	}

	public function store_users_to_renovated(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout 	  = "ajax";
		$this->autoRender = false; 
		$usersPlan  	  = $this->Session->read("NumberUser");
		if(!empty($this->request->data["userIds"])){
			$userChoose = count($this->request->data["userIds"]); 
			$message    = sprintf(__("No puedes seleccionar más de %s usuarios permitidos en el plan que intentas adquirir."), $usersPlan);
			if($userChoose > $usersPlan){
				$response = array("state" => false, "message" => $message);
			} else {
				$this->__createCacheUsersTeams($this->request->data["userIds"]);
				$response = array("state" => true);
			} 
		} else {
			$response = array("state" => false, "message" => __("Debes seleccionar mínimo un usuario para renovar."));
		} 
		$this->outJSON($response);  
	}

	private function __createCacheUsersTeams($userIds = array()){
		$userId = AuthComponent::user("id");
		$users  = array();
		foreach ($userIds as $user) {
			$users["User"][] = EncryptDecrypt::decrypt($user);
		} 
		Cache::write("teams_{$userId}", $users, "USERS_TEAMS_RENOVATED");
		Cache::write("renovations_{$userId}", "renovated_users_choose", "RENOVATED_ADQUIRED"); 
	} 

	public function check_have_users_disabled(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout 	  = "ajax";
		$this->autoRender = false;
		$this->UserTeam->unbindModel(array('belongsTo' => array('Client','Position','Team'))); 
		$conditions   = array("UserTeam.own_user_id" => AuthComponent::user("id"), "UserTeam.state" => configure::read("DISABLED"));
		$group        = array("UserTeam.user_id");
		$users        = $this->UserTeam->find("count", compact("conditions","group"));
		if($users > 0){
			$message  = sprintf(__("Tienes %s usuarios desactivados. ¿Deseas renovarlos?"), $users);
			$response = array("state" => true, "message" => $message);
		} else {
			$response = array("state" => false);
		}
		$this->outJSON($response); 
	} 

	public function save_user_free(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout 	  = "ajax";
		$this->autoRender = false;
		$usersPlan  	  = $this->Session->read("NumberUserFree"); 
		if(!empty($this->request->data["userIds"])){
			$userChoose = count($this->request->data["userIds"]); 
			$message    = sprintf(__("No puedes seleccionar más de %s usuarios permitidos en el plan que intentas adquirir."), $usersPlan);
			if($userChoose > $usersPlan){
				$response = array("state" => false, "message" => $message);
			} else {
				$this->__createUserToRenovateFree($this->request->data["userIds"]);
				$response = array("state" => true);
			} 
		} else {
			$response = array("state" => false, "message" => __("Debes seleccionar mínimo un usuario para renovar."));
		}
		$this->outJSON($response); 
	}

	private function __createUserToRenovateFree($usersIds){
		$users = array();
		foreach ($usersIds as $usersId) {
			$users[] = EncryptDecrypt::decrypt($usersId);
		} 
		$this->Session->write("UsersFree", $users); 
	}

	public function find_collaborators(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout  = "ajax"; 
		$collaborators = $this->UserTeam->getCollaborators($this->request->data["teamId"]);
		$this->set(compact('collaborators'));  
	}

	public function find_assistants(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}  
		$this->layout  = "ajax"; 
		$assistants    = $this->UserTeam->getAssistants($this->request->data["teamId"], $this->request->data["clientId"]);
		$this->set(compact('assistants'));  
	}

	//CARGAR USUARIOS PARA LA LISTA DE COMPROMISOS
	public function load_users_commitments(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout = "ajax";
		$assistants   = $this->UserTeam->getUsersCommitments($this->request->data["teamId"], $this->request->data["clientId"]);
		$this->set(compact('assistants')); 
	}

	//CARGAR LOS USUARIOS PARA LA LISTA DE APROBACIÓN DE ACTAS
	public function load_users_approval(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout = "ajax";
		$allUsers     = $this->UserTeam->getUsersCommitments($this->request->data["teamId"], $this->request->data["clientId"]);
		$this->set(compact('allUsers')); 
	}

	//DESHABILITAR USUARIO EN EL EMPRESA
	public function disabled_license($id = null){
		$this->autoRender = false;
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->UserTeam->exists($id)) {
			$this->showMessageExceptions();
		} 
		$this->UserTeam->updateAll(array('UserTeam.user_state' => configure::read("ENABLED")), array('UserTeam.id' => $id));
		$conditions = array("UserTeam.id" => $id);
		$userTeam   = $this->UserTeam->find("first", compact("conditions")); 
		$this->__mailNotificationDisabledTeam($userTeam, configure::read("ENABLED"));
		$this->updateCachePermissions();
		$this->re_asignar_tareas_compromisos($userTeam['User']['id'], 1);
		$this->Flash->success(__('Se ha deshabilitado el registro correctamente y se han asignado los compromisos y tareas al usuario it@strategee.us'));
		return $this->redirect(array('action' => 'index')); 
	}

	//HABILITAR USUARIO EN EL EMPRESA
	public function enabled_license($id = null){
		$this->autoRender = false;
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->UserTeam->exists($id)) {
			$this->showMessageExceptions();
		} 
		$this->UserTeam->updateAll(array('UserTeam.user_state' => configure::read("DISABLED")), array('UserTeam.id' => $id));
		$conditions = array("UserTeam.id" => $id);
		$userTeam   = $this->UserTeam->find("first", compact("conditions"));
		$this->__mailNotificationDisabledTeam($userTeam, configure::read("DISABLED"));
		$this->updateCachePermissions();
		$this->Flash->success(__('Se ha habilitado el registro correctamente.'));
		return $this->redirect(array('action' => 'index')); 
	}

	private function __mailNotificationDisabledTeam($userTeam = array(), $userState){
		$user        = $userTeam["OwnUser"]["firstname"] . ' ' . $userTeam["OwnUser"]["lastname"];
		$userInvited = $userTeam["User"]["firstname"] . ' ' . $userTeam["User"]["lastname"];
		if($userState == configure::read("ENABLED")){
			$subject = __('Deshabilitación de empresa').' - '.Configure::read('Application.name');
			if($user == " "){
				$message = sprintf(__("El remitente <strong>%s</strong> te ha habilitado nuevamente en su empresa llamada: <strong>%s</strong>."), AuthComponent::user("email"), $userTeam["Team"]["name"]);
			} else {
				$message = sprintf(__("El usuario <strong>%s</strong> te ha deshabilitado de su empresa llamada: <strong>%s</strong>."), $user, $userTeam["Team"]["name"]);
			}
		} else {
			$subject = __('Habilitación de empresa').' - '.Configure::read('Application.name');
			if($user == " "){
				$message = sprintf(__("El remitente <strong>%s</strong> te ha habilitado nuevamente en su empresa llamada: <strong>%s</strong>."), AuthComponent::user("email"), $userTeam["Team"]["name"]);
			} else {
				$message = sprintf(__("El usuario <strong>%s</strong> te ha habilitado nuevamente en su empresa llamada: <strong>%s</strong>."), $user, $userTeam["Team"]["name"]);
			}
		}
		$opts = array(
			'to'       => $userTeam["User"]["email"],
			'subject'  => $subject,
			'vars'     => array('subject' => $message, 'userInvited' => $userInvited),
			'template' => 'notification_team',
		); 
		$this->sendMail($opts);	 
	} 

	//REMOVER LICENCIA A USUARIO
	public function remove_user($id = null){
		$this->autoRender = false;
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->UserTeam->exists($id)) {
			$this->showMessageExceptions();
		} 
		$conditions = array("UserTeam.id" => $id);
		$userTeam   = $this->UserTeam->find("first", compact("conditions"));
		if($userTeam["UserTeam"]["state"] == configure::read("DISABLED")){
			$this->Flash->fail(__('Esta licencia no se puede remover porque esta vencida.'));
			return $this->redirect(array('action' => 'index','controller' => 'removed_users'));
		}
		$this->__mailRemovedLicense($userTeam);
		$this->__saveUserRemoved($userTeam);
		$this->Flash->success(__('Se ha removido el usuario correctamente.'));
		return $this->redirect(array('action' => 'index','controller' => 'removed_users')); 
	}


	private function __mailRemovedLicense($userTeam = array()){
		$user        = $userTeam["OwnUser"]["firstname"] . ' ' . $userTeam["OwnUser"]["lastname"];
		$userInvited = $userTeam["User"]["firstname"] . ' ' . $userTeam["User"]["lastname"];
		$subject     = __('Deshabilitación de empresa').' - '.Configure::read('Application.name');
 		$message     = sprintf(__("El usuario <strong>%s</strong> te ha removido de su empresa llamada: <strong>%s</strong>."), $user, $userTeam["Team"]["name"]);
 		$opts = array(
			'to'       => $userTeam["User"]["email"],
			'subject'  => $subject,
			'vars'     => array('subject' => $message, 'userInvited' => $userInvited),
			'template' => 'notification_team',
		); 
		$this->sendMail($opts);	
	}
 
	private function __saveUserRemoved($userTeam = array()){
		$this->loadModel("RemovedUser");
		$data = array(
			"user_id"     => $userTeam["UserTeam"]["user_id"],
			"own_user_id" => $userTeam["UserTeam"]["own_user_id"], 
			"team_id"     => $userTeam["UserTeam"]["team_id"]
		);
		$this->RemovedUser->create();
		$this->RemovedUser->save($data);
		$this->UserTeam->deleteAll(array('UserTeam.id' =>  $userTeam["UserTeam"]["id"]), false);
		$this->updateCachePermissions(); 
	}

	public function activate_license($id = null){
		$this->loadModel("RemovedUser");
		$this->autoRender = false;
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->RemovedUser->exists($id)) {
			$this->showMessageExceptions();
		} 
		$conditions  = array("RemovedUser.id" => $id); 
		$email       = $this->RemovedUser->find("first", compact("conditions"));
		$sessionData = array("id" => $id, "email" => $email["User"]["email"], 'user_id' => $email["User"]["id"]); 
		$this->Session->write("ActivateUser", $sessionData); 
		return $this->redirect(array('action' => 'add','controller' => 'user_teams')); 
	}

	private function __deleteUserFromRemoved(){
		$this->loadModel("RemovedUser"); 
		if($this->Session->check('ActivateUser')){ 
			$this->RemovedUser->deleteAll(array('RemovedUser.id' =>  $this->Session->read('ActivateUser.id'), 'RemovedUser.user_id' =>  $this->Session->read('ActivateUser.user_id'), 'RemovedUser.own_user_id' => AuthComponent::user("id")), false);
			$this->Session->delete("ActivateUser"); 
		}
	}

	private function __deleteUserRemoved($userId, $teamId){
		$this->loadModel("RemovedUser"); 
		$this->RemovedUser->deleteAll(array('RemovedUser.user_id' =>  $userId, 'RemovedUser.own_user_id' => AuthComponent::user("id"), 'RemovedUser.team_id' => $teamId), false);
		$this->Session->delete("ActivateUser"); 
	}

	public function load_all_users(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}  
		$this->layout  = "ajax"; 
		$clientId      = isset($this->request->data["clientId"]) ? $this->request->data["clientId"] : NULL;
		$conditions    = array("Client.id" => $clientId); 
		$teamId        = $this->UserTeam->Client->field("team_id", $conditions);
		$users 		   = $this->UserTeam->getUsersCommitments($teamId, $clientId); 
		unset($users[AuthComponent::user("id")]); 
		$this->set(compact("users")); 
	}

	public function complete_step(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}  
		$this->layout     = "ajax"; 
		$this->autoRender = false;  
		$this->deleteCacheStep();
  		return true; 
	}

	public function check_data_createds(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}  
		$this->layout     = "ajax"; 
		$this->autoRender = false;  
		$response         = array();
		$client           = $this->UserTeam->Client->getTotalClient();
		// $teams            = $this->UserTeam->Team->getTotalTeam();
		$teams            = 1;
		$positions        = $this->UserTeam->Position->getTotalPosition();
		$type             = isset($this->request->data["typeUser"]) ? $this->request->data["typeUser"] : NULL; 
		if($type == configure::read("COLLABORATOR")){
			if($teams == 0){
				$this->Flash->fail(__('Para crear un usuario como colaborador primero debes crear una empresa y un rol.'));
				$response = array("state" => false, "url" => Router::url(array('controller'=>'teams', 'action'=>'add')));
			} else if($positions == 0){
				$this->Flash->fail(__('Para crear un usuario como colaborador primero debes crear una empresa y un rol.'));
				$response = array("state" => false, "url" => Router::url(array('controller'=>'positions', 'action'=>'add')) );
			}  else {
				$response = array("state" => true);
			}
		} else  {
			if($client == 0){
				$this->Flash->fail(__('Para crear un usuario como asistente externo primero debes crear una empresa y un cliente.'));
				$response = array("state" => false, "url" => Router::url(array('controller'=>'clients', 'action'=>'add')) );
			} else if ($teams == 0){
				$this->Flash->fail(__('Para crear un usuario como asistente externo primero debes crear una empresa y un cliente.'));
				$response = array("state" => false, "url" => Router::url(array('controller'=>'teams', 'action'=>'add')));
			} else {
				$response = array("state" => true);
			}
		}
		$this->outJSON($response);  
	}
  
}
