<?php
App::uses('AppController', 'Controller');
 
class ShareApplicationsController extends AppController {

	public $uses = array();

	public function share() {
		$this->autoRender = false;
		$this->__sendShareEmail();
		$this->Flash->success(__('La aplicación se ha compartido exitosamente'));
		$this->redirect($this->request->referer());
	}

	private function __sendShareEmail() {
		if(!empty($this->request->data['ShareApplication']['email'])){
			$emails = $this->request->data['ShareApplication']['email'];
			if(!is_array($emails)) {
				$emails = explode(",", $emails);
			}
			$emails = array_map('trim', $emails);

			App::Import('Utility', 'Validation');
			$data = $this->request->data;
			foreach($emails as $email) {
				if(Validation::email($email)) {
					$opts = array(
					    'to'       => $email,
					    'subject'  => sprintf(__('%s %s te ha enviado un mensaje'), AuthComponent::user('firstname'), AuthComponent::user('lastname')),
					    'vars'     => compact('data'),
					    'template' => 'share_application',
					);
					$this->sendMail($opts);	
				}
			}
		}
	}
}