<?php
App::uses('AppController', 'Controller');

class ClientsController extends AppController {

	public $components = array('Paginator'); 

	public function index() { 
		$this->validateTeamSession();
		$this->check_team_permission_action(array("index"), array("clients"));
		$teams        = $this->loadTeams();  
		// $teamIds 	  = $this->getTeamIds();

		$this->Client->bindContacNotDone(); 
		$conditions   = array("Client.team_id" => EncryptDecrypt::decrypt($this->Session->read("TEAM")));
		$conditions[] = $this->Client->buildConditions($this->request->query);
		$order        = array('Client.id DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
        $clients = $this->Paginator->paginate();  
		$this->set(compact('clients','teams')); 
	}

	public function view($id = null) { 
		$id  		   = EncryptDecrypt::decrypt($id);
		$client        = $this->Client->find('first', array('conditions' => array('Client.id' => $id)));
		$this->check_team_permission_action(array("view"), array("clients"), $client["Client"]["team_id"]); 
		$conditions    = array('Project.client_id' => $id); 
		$projectsCount = $this->Client->Project->find("count", compact("conditions")); 
		$this->set(compact('client','projectsCount'));
	}

	public function add($step = null) {
		$this->validateTeamSession();  
		$stepApp = $this->getCacheStep();
		$this->__checkTeamCreate($stepApp);	 
		$this->check_team_permission_action(array("add"), array("clients")); 
		if ($this->request->is('post')) { 
			$response = $this->__validateTypeFileToClient($this->request->data);
			if($response["state"] == true){
				$this->request->data['Client']['name'] = $this->lowercaseText($this->request->data['Client']['name']); 
				$this->request->data['Client']['user_id'] = AuthComponent::user('id');
				$this->Client->create();
				$this->check_team_permission_action(array("add"), array("clients"), !empty($this->request->data['Client']['team_id']) ? $this->request->data['Client']['team_id'] : NULL);
				if ($this->Client->save($this->request->data)) { 
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					if($stepApp == 1){
						$this->redirect(array('action' => 'index'));
					} else {
						$this->redirect(array('action' => 'index'));						
					}
				} else {
					$this->Flash->fail(__('Error al guardar, por favor inténtelo nuevamente.'));
				}
			} else {
			 	$this->Client->set($this->request->data);
	   		 	$this->Client->validates();
			 	$this->Flash->fail(__($response["message"])); 
			} 
		}
		$this->__buildList($step);
	}

	private function __checkTeamCreate($stepApp = null){ 
		if($stepApp == 1){
			$teams = $this->Client->Team->getTotalTeam(AuthComponent::user("id"));
			if($teams == 0){
				$this->Flash->fail(__('Para crear un cliente primero debes crear una empresa.'));
				$this->redirect(array('action' => 'add','controller' => 'teams'));
			} 
		}
	}

	public function edit($id = null) {
		$id         = EncryptDecrypt::decrypt($id); 
		$beforeEdit = $this->__getInfoToBuildLog($id);
      	$this->Client->id = $id;
		if ($this->request->is('post') || $this->request->is('put')) { 
			$response = $this->__validateTypeFileToClient($this->request->data);
			$this->request->data['Client']['name'] = $this->lowercaseText($this->request->data['Client']['name']);
			if($response["state"] == true){
				if ($this->Client->save($this->request->data)) { 
					$this->updateCachePermissions();
					//$this->__clientLogAfterEdit($id, $this->request->data, $beforeEdit);
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->fail(__('Error al guardar, por favor inténtelo más tarde.'));
				}
			} else {
				$this->Client->set($this->request->data);
	   		 	$this->Client->validates();
			 	$this->Flash->fail(__($response["message"]));
			}
		} else {
			$this->request->data = $beforeEdit["data"]; 
		}
		$this->__buildList($beforeEdit); 
	}

	private function __getInfoToBuildLog($id){
		$client = $this->Client->findById($id);
		$this->check_team_permission_action(array("edit"), array("clients"), $client["Client"]["team_id"]); 
		$beforeEdit  = __("Nombre: " ) .  $client["Client"]["name"] . __(" Descripción: ") . $client["Client"]["description"] . __(" Logo: ") . $client["Client"]["img"];
		return array("data" => $client, "beforeEdit" => $beforeEdit);
	}

	private function __clientLogAfterEdit($id, $data = array(), $beforeEdit){ 
		$afterEdit   = __("Nombre: " ) .  $this->request->data["Client"]["name"] . __(" Descripción: ") . $this->request->data["Client"]["description"] . __(" Logo: ") . $this->request->data["Client"]["img"]["name"]; 
		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha editado al  cliente ") . $this->request->data["Client"]["name"];
		//$this->buildInfoLog($id, $description, $beforeEdit["beforeEdit"], $afterEdit, NULL, NULL); 
	}

	private function __validateTypeFileToClient($data) { 
		$extensions = $this->listExtensions();
		if(!empty($data["Client"])){
			$extensionImgToUpload = ltrim(strstr($data["Client"]["img"]["name"], '.'));
			if($data["Client"]["img"]["error"] == 4 && $this->request->action == "add"){
				$response = array("state" => false, "message" => __("La imagen del cliente es requerida."));
			} else if($this->request->action == "edit" && $data["Client"]["img"]["error"] == 4) {
				unset($this->Client->validate['img']);
				$response = array("state" => true);
			} else {
				$response = array("state" => true);
			} 
			return $response;
		}		
	}

	//PREPARAR EL FORMULARIO DE CREAR CLIENTE QUE SE MOSTRARÁ EN CUANDO SE ESTE CREANDO UN ACTA O EDITANDO 
	public function add_client(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax';
        $conditions   = array("Team.id" => $this->request->data["teamId"]);
        $team         = $this->Client->Team->find("list", compact("conditions")); 
		$this->set(compact('team'));
	}

	//AÑADIR UN CLIENTE DESDE EL ACTA
	public function add_client_save(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
		$this->autoRender = false; 
		$data 			  = array();
		$data["errors"]   = "";
		$data["success"]  = 0;
		$data["id"]       = 0;
		$data["errors"]   = $this->__validateData($data["errors"]);
		if ($data["errors"] == "") { 
			$this->Client->create();
			$this->request->data['Client']['user_id'] = AuthComponent::user('id');
			$this->Client->save($this->request->data);
			$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha creado al cliente desde un acta") . ' ' . $this->request->data["Client"]["name"];
		    // $this->buildInfoLog($this->Client->id, $description, NULL, NULL, NULL, NULL, $this->request->data['Client']['team_id']); 
			$data["success"] = __('Los datos se han guardado correctamente.');
			$data["id"]      = $this->Client->id;
			$model 			 = __('cliente');
			$this->notificacionesEmpresa($model, $this->request->data["Client"]["name"]); 
		}
		$this->outJSON($data); 
	}

	private function __validateData($errores){ 
		if (empty(trim($this->request->data['Client']['name']))) {
			$errores.= __("El nombre del cliente es requerido")."\n";
		}else{
			$this->request->data['Client']['name'] = $this->lowercaseText($this->request->data['Client']['name']);
			$existente = $this->Client->validExistenceClient($this->request->data['Client']['name'], $this->request->data['Client']['team_id']);
			if ($existente > 0) {
				$errores.= __("El nombre del cliente ya se encuentra registrado con la empresa seleccionada, por favor verifica")."\n";
			}
		}
		if (!isset($this->request->data['Client']['team_id'])) {
			$errores.= __("La empresa es requerida.")."\n";
		}
		if (empty(trim($this->request->data['Client']['description']))) {
			$errores.= __("La descripción del cliente es requerido.")."\n";
		}
		if (empty($this->request->data['Client']['img']['name'])) {
			$errores.= __("La imagen del cliente es requerida.")."\n";
		} else {
			if (empty($this->request->data['Client']['img']['tmp_name'])) {
				$errores.= __("El archivo se encuentra dañado, por favor sube otro archivo.")."\n";
			} else {
				$array = explode('.', $this->request->data['Client']['img']['name']);
				$extension = mb_strtolower($array[count($array) - 1]);
				if (!in_array($extension, array('jpg','png','jpeg'))) {
					$errores.= __("Recuerda que el archivo que se debe adjuntar es una imagen.")."\n";
				}
			}
		}
		return $errores;
	}

	public function find_last_client(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout    = 'ajax';
		$this->recursive = -1;
        $client_id 		 = $this->request->data['client_id'];
		$cliente  		 = $this->Client->detaillClient($client_id);
		$this->set(compact('cliente'));
	}

	private function __buildList($beforeEdit = null){ 
		$teams       = $this->loadTeamPermission(); 
		$stepApp     = $this->getCacheStep();
		$totalClient = 0;
		if($stepApp == configure::read("ENABLED")){
			$totalClient = $this->Client->getTotalClient();
		}
		$userId = AuthComponent::user("id");  
		$this->set(compact('teams','beforeEdit','totalClient','stepApp')); 
	}
	

	public function show_image_client(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout    = 'ajax';
		$this->recursive = -1;
        $client_id 		 = isset($this->request->data['id']) ? $this->request->data['id'] : NULL;
        $conditions      = array("Client.id" => $client_id);
        $img          	 = $this->Client->field("img", $conditions); 
		$this->set(compact('img'));
	}


	public function verify_img(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax';
		$this->autoRender = false; 
		$response 		  = $this->Client->validateMimeTypeImg($this->request->data["Client"]);
	 	$this->outJSON($response); 
	}
}
