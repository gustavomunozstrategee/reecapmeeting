<?php
App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');
App::import('Vendor', 'OutLookApi1', array('file' => 'OutLookApi/outlook.php'));
App::import('Vendor', 'OutLookApi2', array('file' => 'OutLookApi/oauth.php')); 


class MeetingsController extends AppController {
 
	public $components = array('Paginator');

	public function beforeFilter() {	
		$this->Auth->allow('reminder_metings','finish_process_edit_meeting','addEventInGmail');
		parent::beforeFilter(); 	 
 	}
 
	public function index() {
		$teams        = $this->loadTeams();
		$this->Session->delete('InvitedsDelete');
		$this->Session->delete("Dates");
		$this->Session->write('MEETING_EDITAR',Configure::read('DISABLED'));
		$limit        = 10;
        $order        = array('Meeting.modified'=>'DESC');
        $recursive    = 0;
        $conditions   = array('Meeting.user_id' => Authcomponent::user('id'));  
        $conditions[] = $this->Meeting->buildConditions($this->request->query);
        $this->Paginator->settings = compact('conditions','limit','order','recursive');
        $meetings     = $this->Paginator->paginate(); 
        $this->set(compact('meetings','teams')); 
	}

	//EDITAR REUNION SIN AUTENTICARSE POR GMAIL O OUTLOOK
	public function edit($id = null){  
		$id = EncryptDecrypt::decrypt($id);
		$this->__initDataMeetingEdit($id,"edit");
		$this->Meeting->id = $id;
		if ($this->request->is('post') || $this->request->is('put')) { 
			$this->__unsetFields($this->request->data);
			$this->request->data['Meeting']['id'] = $id;
			$response = $this->__validateTopics($this->request->data);
			if ($response["state"] == true) {
				$guardar = $this->getValidates($this->request->data,'Meeting'); 
				$this->__validateAssistantInMeeting($id);
				$data = $this->Meeting->buildaRequestDataMeetingEdit($this->request->data);
				if ($this->Meeting->save($data)) { 
					$this->__updateListAssistants($id, $this->request->data['Meeting']['team_id']);
					//$this->Meeting->logAfterEditMeeting($id, $beforeEditMeeting);				 
					$this->Flash->success(__('La reunión se ha editado.'));
					$this->redirect(array('action' => 'index'));
				} else { 
					$this->Flash->fail(__('Error al editar la reunión, por favor inténtelo nuevamente.'));
				} 
			} else {  
				$this->Flash->fail($response["message"]); 
			} 
		} else {
			$conditions = array('Meeting.' . $this->Meeting->primaryKey => $id);
			$this->request->data = $this->Meeting->find('first', compact('conditions'));
		}
		$this->__buildInfiMeeting("edit", $id);
	}

	private function __unsetFields($data = array()){ 
		if($data["Meeting"]["meeting_room"] != configure::read("ENABLED")){
			unset($this->Meeting->validate['location']);
		}
		if($data["Meeting"]["private"] == configure::read("ENABLED")){
			unset($this->Meeting->validate['password']);
		}
	}
 
	private function __initDataMeetingEdit($id,$type){ 
		$this->__validateOnlyMeetingCreated($id);
		if (!$this->Meeting->exists($id)) {
			$this->showMessageExceptions();
		} 	
		$detailMeeting     = $this->Meeting->searchDetailMeeting($id);
		if ($detailMeeting["0"]['Meeting']['private'] == 2 && $this->Session->read('MEETING_EDITAR') == Configure::read("DISABLED")) {
			$this->redirect(array('action' => 'valid_password',EncryptDecrypt::encrypt($id),$type));
		}
	}
 
	public function valid_password($id,$type){
		$id = EncryptDecrypt::decrypt($id);
		if ($this->request->is('post') || $this->request->is('put')) {
			$contra = $this->request->data['Meeting']['password'];
			$detailMeeting = $this->Meeting->searchDetailMeeting($id);
			if ($detailMeeting["0"]['Meeting']['password'] == $contra) {
				$this->Session->write('MEETING_EDITAR',Configure::read('ENABLED'));
				$this->redirect(array("action"=>$type,EncryptDecrypt::encrypt($id)));
			} else {
				$this->Flash->fail(__('Contraseña incorrecta.'));
				$this->request->data['Meeting']['password'] = '';
			}
		}

	}

	public function view($meetingId = null){ 
		$meetingId = EncryptDecrypt::decrypt($meetingId);
		$this->__initDataMeetingEdit($meetingId,"view");
		$this->Session->delete("Dates");
		$meeting  	  = $this->Meeting->getInfoMeetingToContact($meetingId); 
		$this->set(compact('meeting')); 
	}
 
	private function __validateOnlyMeetingCreated($id){
		$recursive  = -1;
		$conditions = array("Meeting.id" => $id, "Meeting.email_type" => NULL);
		$meeting    = $this->Meeting->find("first", compact("conditions","recursive")); 
		if(empty($meeting)){
			$this->Flash->fail(__('Esta reunión no se puede editar.'));
			return $this->redirect(array('action' => 'index'));	
		} else {
			$conditions = array("Meeting.id" => $id);
			$meeting    = $this->Meeting->find("first", compact("conditions","recursive"));
			if($meeting["Meeting"]["contac_id"] != NULL && $this->request->action == "edit"){
				$this->Flash->fail(__('Esta reunión no se puede editar ya que pertenece a un acta finalizada.'));
				return $this->redirect(array('action' => 'index'));
			} 
		}
	}

	private function __validateAssistantInMeeting($id){  		
		$assistants    = $this->Session->read("StoredAssistant");
		$countDelete   = 0;
		$countAssitant = 0;
		if(!empty($assistants)){
			foreach ($assistants as $userId => $assistant) {
				$countAssitant += count($userId);
				if($assistant["state"] == configure::read("DELETE_ASSISTANT")) {
					$countDelete++;
				}
			}
		}
		if($countDelete == $countAssitant){
			$this->Flash->fail(__('Se debe añadir mínimo un usuario a la reunión.'));
			return $this->redirect(array('action' => 'edit', EncryptDecrypt::encrypt($id))); 
		}  
	}

	private function __buildInfiMeeting($action, $id = null){
		$teams = $this->__getTeamsWithClients(); 
		if(empty($teams)){
			$this->Flash->info(__('Para crear una reunión primero debes crear un cliente y asociarlo a una empresa.'));
		}
		if($action == "edit"){
			$recursive  = -1;
			$conditions = array('Meeting.id' => $id);
			$meeting    = $this->Meeting->find("first", compact("conditions","recursive")); 
			$conditions = array("Assistant.meeting_id" => $id, "Assistant.state !=" => 3);
			$assistants = $this->Meeting->Assistant->find("all", compact("conditions"));  
			$clients    = $this->__getClientMeetingEdit($meeting); 
			$assistantsSession = $this->Session->read("StoredAssistant");
			$this->check_team_permission_action(array("add"), array("meetings"), $meeting["Meeting"]["team_id"]); 
			$this->set(compact('meeting','topics','assistants','assistantsSession','teams','clients')); 
		} else {
			$this->set(compact('teams')); 
		}
	}

	private function __getTeamsWithClients(){
		$this->Meeting->Team->unbindModel(array('hasMany' => array('Contac','Log','UserTeam')));
		$teamIds     = $this->loadTeamPermission();
		$conditions  = array('Team.id' => array_keys($teamIds)); 
		$teamsUser   = $this->Meeting->Team->find('all',compact('conditions','recursive')); 
		$teams       = array();
		foreach ($teamsUser as $team) {
			if(!empty($team["Client"])){
				$teams[$team["Team"]["id"]] = $team["Team"]["name"];
			}
		} 
		return $teams;
	}

	private function __getClientMeetingEdit($meeting = array()){
		$conditions = array("Client.id" => $meeting["Meeting"]["client_id"]);
		$clients    = $this->Meeting->Client->find("list", compact("conditions"));
		return $clients;
	}

	public function meetings_invitations(){
		$teams            = $this->loadTeams();
		$fields 	      = array("Meeting.id");
		$conditions       = array('Meeting.user_id' => Authcomponent::user('id')); 
	 	$myMeetings       = $this->Meeting->find("list", compact("conditions","fields"));
	 	$fields           = array("Assistant.meeting_id");
	 	$conditions       = array("NOT" => array("Assistant.meeting_id" => $myMeetings), "Assistant.user_id" => Authcomponent::user('id'));
	 	$meetingsReceived = $this->Meeting->Assistant->find("list", compact("conditions","fields"));
	 	$limit            = 10;
        $order            = array('Meeting.modified'=>'DESC');
        $conditions       = array('Meeting.id' => $meetingsReceived);
        $conditions[]     = $this->Meeting->buildConditions($this->request->query);  
	 	$this->Paginator->settings = compact('conditions','limit','order');
        $meetings         = $this->Paginator->paginate();
        $this->set(compact('meetings','teams')); 	 
	}

	public function reminder_metings(){ 
		$this->autoRender 	= false;
		$fields 	      	= array("Meeting.id");
		$conditions       	= array('Meeting.date' => date("Y-m-d"));
	 	$meetingId          = $this->Meeting->find("list", compact("conditions","fields")); 
	 	if(!empty($meetingId)){
			$this->Meeting->Assistant->unbindModel(array('belongsTo' => array('Contac'))); 
		 	$conditions = array("Assistant.meeting_id" => $meetingId);
			$this->Meeting->Assistant->bindClient();
		 	$asistentes = $this->Meeting->Assistant->find("all", compact("conditions"));
		 	if(!empty($asistentes)){
			 	foreach ($asistentes as $asistente) {			 		 
					$texto  = sprintf(__("El día de hoy estas citado a una reunión con asunto %s con el cliente %s."), $asistente["Meeting"]["subject"], $asistente["Client"]["name"]); 
		 			$url    = "meetings/view/".EncryptDecrypt::encrypt($asistente["Meeting"]["id"]);
		 			$msgEng = sprintf(__("Today you have been cited to a meeting with subject %s with client %s."), $asistente["Meeting"]["subject"], $asistente["Client"]["name"]);
		 			$this->notificacionesUsuarios($asistente['Assistant']['user_id'], $texto, $msgEng, $url); 		 	 
			 	}  		
		 	} 
	 	}
	}

	//VALIDAR INFORMACION DE LA REUNIÓN
	public function validate_info_meeting_edit(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$response 	   = $this->Meeting->validateMeetingHaveAssistant($this->Session->read("StoredAssistant"));
		$topicResponse = $this->__validateTopics($this->request->data);
		if ($topicResponse["state"] == true) { 
			if ($response["state"] == true) {  
				$this->Meeting->set($this->request->data);
				if ($this->Meeting->validates()) { 
					$response = array("state" =>true);
					$this->__deleteCredentials();
					$this->Session->write("MeetingInfoEdit", $this->request->data);			 
				} else {
					$message  = $this->__getErrors();
					$response = array("message" => $message, "state" => false, "validation" => true);
				}
			}
		} else {
			$response = array("message" => $topicResponse["message"], "state" => false);
		}
		$this->outJSON($response); 
	}

	private function __deleteCredentials(){
		$credentials = "~/.credentials/calendar-php-quickstart.json";
		$url = $this->__expandHomeDirectory($credentials);  
		if (file_exists($url)) { 
	 		unlink($url);
 		} 
	}

	private function __expandHomeDirectory($path) {
 	 	$homeDirectory = getenv('HOME');
  		if (empty($homeDirectory)) {
    		$homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
  		}
  		return str_replace('~', realpath($homeDirectory), $path);
	}

	public function delete_credentials(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$this->__deleteCredentials();
	}
	
	public function add(){
		// $this->__checkTeamClientCreated();
		$this->check_team_permission_action(array("add"), array("meetings"));  
		$this->__buildInfiMeeting("add"); 
	}

	private function __checkTeamClientCreated(){
		$stepApp = $this->getCacheStep();
		if($stepApp == 1){
			$clients = $this->Meeting->Client->getTotalClient();
			$teams   = $this->Meeting->Team->getTotalTeam(AuthComponent::user("id"));
			if($teams == 0){
				$this->Flash->fail(__('Para crear una reunión primero debes crear una empresa.'));
				$this->redirect(array('action' => 'add','controller' => 'teams'));
			} else if($clients == 0){
				$this->Flash->fail(__('Para crear una reunión primero debes crear un cliente.'));
				$this->redirect(array('action' => 'index','controller' => 'teams'));
			} 
		}
	}
 

	//REUNIÓN TIPO REUNIÓN NORMAL
	public function addMeeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
	 	$response 		  = $this->__validateTopics($this->request->data);
	 	if ($response["state"] == true) {
			if (!empty($this->Session->read("Inviteds"))) { 
				$this->request->data["Meeting"]["user_id"] = authcomponent::user("id");
				$this->Meeting->set($this->request->data);  
				if ($this->Meeting->validates()) {
					$message  = $this->__validateDatesAndTopicsDuration($this->request->data); 
					$response = array("state" => true, "message" => $message);
					$this->__deleteCredentials();
					$this->Session->write("Meeting", $this->request->data); 
				} else {
					$message  = $this->__getErrors();
					$response = array("message" => $message, "state" => false, "validation" => true);
				}
			} else { 
				$response = array("message" => __("Se debe añadir mínimo un usuario a la reunión."), "state" => false,"validation" => false);
			} 
		}
		$this->outJSON($response); 
	}

	private function __validateDatesAndTopicsDuration($data = array()){
		$timeMeeting = abs(strtotime($data["Meeting"]["start_date"]) - strtotime($data["Meeting"]["end_date"]));
		$timeTopics  = 0;
		$message     = NULL;
		if(!empty($data["Topic"])){
			foreach ($data["Topic"] as $time) { 
				$timeTopics +=  $time["time"];
			} 
			if($timeTopics > $timeMeeting){
				$message = __("Nota: El tiempo total de los temas específicos es mayor al tiempo de la duración de la reunión.");
				return $message;
			} 
		} else {
			return $message;
		}
	}	

	public function saveMeetingFollowing(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		if (!empty($this->Session->read("Inviteds"))) {
			$this->__unsetDates();
			if (!empty($this->Session->read("Dates"))) {
				$this->request->data["Meeting"]["user_id"] = authcomponent::user("id");
				$this->Meeting->set($this->request->data); 
				if ($this->Meeting->validates()) {
					$response = array("state" =>true);
					$this->__deleteCredentials();
					$this->Session->write("MeetingFollowing", $this->request->data); 
				} else {
					$message  = $this->__getErrors();
					$response = array("message" => $message, "state" => false, "validation" => true);
				}
			} else {
				$response = array("message" => __("Se debe definir mínimo la fecha inicio y la fecha fin de la reunión."), "state" => false,"validation" => false); 
			}
		} else { 
			$response = array("message" => __("Se debe añadir mínimo un usuario a la reunión."), "state" => false,"validation" => false);
		}	 
		$this->outJSON($response); 
	}

	private function __unsetDates(){
		unset($this->Meeting->validate['start_date']); 
		unset($this->Meeting->validate['end_date']);
	}

	private function __validateTopics($data = array()){			 
		$time        = 0;
        $description = 0;
        if(isset($data["Topic"])){
	        foreach ($data["Topic"] as $topic) {
	           	if($topic["time"] != 0 && trim($topic["time"] != "")){
	            	$time++;           	
	      	 	} else {
	      	 		$time = $topic;
	      	 	}
	           	if(trim($topic["description"]) == "" ) {
	       		 	$description = $topic;
	           	} else {
	            	$description++;           	
	           	}
	        }  
	        if (($time == $description && $time != 0 && $description != 0)){ 
	        	$response = array("state" => true); 
	        } else { 
	        	$response = $this->__getMessages($time, $description);
	        }
        } else {
        	$response = array("state" => true);
        }
        return $response; 
	}

	private function __getMessages($time, $description){     
		if($time["time"] == "" && $time["description"] != ""){
			$response = array("message" => __("El tiempo es requerido."), "state" => false);
		} else if ($time["time"] == 0 && $time["description"] != "") {
			$response = array("message" => __("El tiempo es requerido."), "state" => false);
		} else {
			$response = array("message" => __("La descripción es requerida."), "state" => false);
		} 
		return $response; 
	} 

	private function __getErrors(){
		$errors   = $this->Meeting->validationErrors;
		$messages = array();
		foreach ($errors as $error) {
            $messages[] = $error[0];
        }
        return $messages;
	}

	public function listMeetingGmail(){
		$meetingsFollowing = false;  
		$primary = Authcomponent::user("email");
		$base    = ROOT . DS . 'listevents.php';
 	 	require_once $base;  
 	 	$events  = listEvents(); 
 	 	$this->__updateCalendarGmail($events);
 	 	$this->Flash->success(__('El calendario se ha sincronizado correctamente.'));  	 
 	 	return $this->redirect(array('action' => 'index'));	  	 
	}

	private function __updateCalendarGmail($events){
		$this->Meeting->deleteAll(array('Meeting.email_type' => "gmail","Meeting.user_id" => Authcomponent::user("id")), false);
		$recursive        = -1;
	 	$fields           = array("Assistant.meeting_id");
	 	$conditions       = array("Assistant.user_id" => Authcomponent::user('id'));
	 	$meetingsReceived = $this->Meeting->Assistant->find("list", compact("conditions","fields"));
		$conditions       = array('Meeting.id' => $meetingsReceived, "Meeting.start_date >=" => date('Y-m-01'), "Meeting.end_date <=" => date('Y-m-t')); 
		$meetings         = $this->Meeting->find("all", compact("conditions","recursive"));	
		$meetingsDb       = array();
		foreach ($meetings as $key => $meeting) {
			$meetingsDb[] =  $meeting["Meeting"];
		} 
		$this->__getRecordToUpdateCalendar($events, $meetingsDb);
	}

	private function __getRecordToUpdateCalendar($events, $meetingsDb){ 
		$meetingData    = $this->Meeting->buildDataFromMeetingsDatabase($meetingsDb, $events); 
		$newMeetingData = array(); 
		foreach ($meetingData as $meetingId => $meeting) {
		  	$createDateCreated  = new DateTime($meeting["startDate"]);	    
	    	$fechaInicio        = $createDateCreated->format('Y-m-d');
		 	$newMeetingData[] = array( 
		 		"subject"     => $meeting["title"],
		 		"location"    => $meeting["location"],
		 		"description" => !empty($meeting["description"]) ? $meeting["description"] : __("Sin descripción"),
		 		"start_date"  => $meeting["startDate"],
		 		"end_date"    => $meeting["finalDate"],
		 		"email_type"  => "gmail",
		 		"user_id"     => Authcomponent::user("id"),
		 		"created"     => $fechaInicio
		 	);
		}
		if(!empty($newMeetingData)){
			$this->Meeting->saveAll($newMeetingData, array('validate'=>false));
		} 			 
	}

	public function generateIdMeeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		$this->__deleteCredentials(); 
	}

	//METODO DE EDITAR UN EVENTO DE GMAIL Y OBTENCIÓN DEL CLIENTE
	public function create_meeting_gmail(){ 
		$this->__deleteCredentials();
		$this->autoRender  = false; 
		$meetingAdd 	   = 2;
		$base  			   = ROOT . DS . 'listevents.php';
 	 	require_once $base; 
 	 	$urlRedirect = Configure::read("REDIRECT_ADD_EVENT");
 	 	$client 	 = getClient($urlRedirect);   	
 	 	die();
	}

	// AQUI LO MANDARÁ LUEGO DE AUTENTICARSE DE GMAIL PARA EDITAR LA REUNION
	public function addEventInGmail(){ 
	 	if (isset($this->request->query['code']) || $this->request->query['callback'] == true) { 
	 		$this->__createMeetingEventInGmail();
	 	} else {
	 		$this->Flash->success(__('No se ha podido crear la reunión, inténtelo nuevamente.'));
			return $this->redirect(array('action' => 'index'));	 
	 	}  
	}
 
	//GUARDAR LA REUNION TIPO NORMAL AUTENTICANDOSE POR GMAIL EN LOS CALENDARIOS
	private function __createMeetingEventInGmail(){ 
		$this->autoRender = false; 
		set_time_limit(300000);
		$data      = $this->Session->read("Meeting");
		$users     = $this->Session->read("Inviteds");
		$this->Meeting->create();
		$this->Meeting->save($data["Meeting"], array("validate" => false));
		$this->Session->write("MeetinId", $this->Meeting->id);
        	// $this->__logAfterAddMeeting($data); 
		$meetingId = $this->Session->read("MeetinId");
		$primary   = Authcomponent::user("email");
		$base      = ROOT . DS . 'quickstart.php';
 	 	require_once $base;
 	 	$client = getClient(); 
 		$events = createMeetingGmail($primary, $users, $data, $client);
   		$this->Meeting->saveCalendarIdMeeting($meetingId, $this->Session->read("IdCalendar"));
  		$this->Meeting->saveFromCreatedCalendar($meetingId, "gmail");  	 
 	 	$this->Meeting->Tag->saveTags($data, $meetingId);
	    $this->Meeting->Topic->saveTopics($meetingId, $data);
        $this->Meeting->Assistant->saveAllUsersInviteds($data["Meeting"]["team_id"], $meetingId, $this->Session->read("Inviteds"));
        $this->Meeting->notificationFirebaseMeetingCreated($meetingId);
 	 	$this->__logAfterAddOtherInfo($meetingId);
 	 	$this->__sendMailInvitationsReunionNormal($meetingId);
	    $this->Session->delete("Meeting");
	    $this->Session->delete("Inviteds");
	    $this->Session->delete("MeetinId");
	    $this->Session->delete("IdCalendar");
 	 	$this->Flash->success(__('La reunión se ha guardado correctamente.'));
 	 	return $this->redirect(array('action' => 'index'));	 	 
	}

	//CREACIÓN DE LA REUNIÓN DE TIPO SEGUIMIENTO EN LOS CALENDARIOS DE GMAIL
	public function addEventInGmailMeetingFollowing(){
		$this->autoRender      = false;
		$meetingsFollowing     = true;
		$users     			   = $this->Session->read("Inviteds");
		$meetingFormData       = $this->Session->read("MeetingFollowing");
		$meetingsFollowingData = $this->Session->read("Dates");
		$primary   = Authcomponent::user("email"); 
		$base      = ROOT . DS . 'listevents.php';
 	 	require_once $base; 
 	 	$infoTotal = array("Inviteds" => $users, "meetingFormData" => $meetingFormData, "meetingsFollowingData" => $meetingsFollowingData);  
 	 	$addEvents = saveAllMeetingsFollowing($primary, $infoTotal); 
		$this->__saveAllReunions($meetingFormData, $meetingsFollowingData);		
	    $this->Session->delete("Inviteds");
	    $this->Session->delete("MeetingFollowing");
	    $this->Session->delete("Dates");
	    $this->Session->delete("CalendarIds");
 	 	$this->Flash->success(__('La reunión de seguimiento se ha guardado correctamente.'));
 	 	return $this->redirect(array('action' => 'index')); 
	}

	private function __saveAllReunions($meetingFormData, $meetingsFollowingData){
		$meetingIds = $this->Meeting->getMeetingsIds($meetingFormData, $meetingsFollowingData);
		$this->__buildLogAfterSaveMeetingFollowing($meetingIds["meetingsIds"]);
		$this->Meeting->saveCalendarIdMeetingFollowing($meetingIds["meetingsIds"], $this->Session->read("CalendarIds")); 
		$this->Meeting->Tag->saveTagsFollowingMeetings($meetingFormData, $meetingIds["meetingsIds"]);
		$this->Meeting->Topic->saveTopicsMeetingFollowing($meetingIds["topicsMeeting"]); 
		$this->loadModel("MeetingFollowing");
		$this->MeetingFollowing->saveMeetingFollowing($meetingIds["meetingsIds"], $meetingsFollowingData);
		$this->Meeting->Assistant->saveAllUsersInvitedsMeetingFollowing($meetingFormData["Meeting"]["team_id"], $meetingIds["meetingsIds"], $this->Session->read("Inviteds"));   
		$this->Meeting->notificationFirebaseMeetingFollowingCreated($meetingIds["meetingsIds"]);
		$this->__logAfterAddOtherInfo($meetingIds["meetingsIds"]);
		$this->__sendMailInvitationsReunionSeguimiento($meetingIds['topicsMeeting']);
		return $meetingIds["meetingsIds"];
	}

	private function __buildLogAfterSaveMeetingFollowing($meetingsIds = array()){
		$recursive  = -1;
		$conditions = array("Meeting.id" => $meetingsIds);
		$meetings   = $this->Meeting->find("all", compact("conditions","recursive"));
		foreach ($meetings as $meeting) {
			$this->__logAfterAddMeeting($meeting, $meeting["Meeting"]["id"]); 
		} 
	}
    
	public function generatePasswordMeeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout           = 'ajax';
		$this->autoRender       = false; 
		$fourDigitsRandomNumber = mt_rand(1000, 9999);
		$codeCoupon 		    = $fourDigitsRandomNumber; 
		$this->outJSON($codeCoupon); 
	}

	public function listAllMeeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		$fields           = array("Assistant.meeting_id");
		$conditions       = array("Assistant.user_id" => Authcomponent::user('id'));
		$meetingIds       = $this->Meeting->Assistant->find("list", compact("conditions","fields")); 
      	$order            = array('Meeting.modified'=>'DESC');
      	$conditions       = array('Meeting.id' => $meetingIds, "Meeting.start_date >=" => date('Y-m-01 01:00:00'), "Meeting.end_date <=" => date('Y-m-t 23:59:00'));
      	$recursive        = -1;
      	$meetings         = $this->Meeting->find("all", compact("conditions","order","recursive")); 
		$conditions       = array("Meeting.user_id" => Authcomponent::user('id'), "Meeting.start_date >=" => date('Y-m-01 01:00:00'), "Meeting.end_date <=" => date('Y-m-t 23:59:00'));
		$recursive        = -1;
		$myMettings       = $this->Meeting->find("all", compact("conditions","recursive"));
		$allMeetings      = array_unique(array_merge($meetings,$myMettings), SORT_REGULAR); 
      	$meetingsJson 	  = $this->__getJsonResponse($allMeetings);
      	$meetingEncrypte  = array();
      	foreach ($meetingsJson as $meeting) {
      		$meeting["id"] 	   = EncryptDecrypt::encrypt($meeting["id"]);
      		$meetingEncrypte[] = $meeting;
      	}      
 	  	$this->outJSON($meetingEncrypte); 
	}

	private function getButtonActions($meeting){
		$actions = array();
    	$permission     = $this->check_team_permission_action(array("add"), array("meetings"), $meeting['Meeting']['team_id'], true);
		$permissionView = $this->check_team_permission_action(array("view"), array("meetings"), $meeting['Meeting']['team_id'], true);

		if($meeting["Meeting"]["email_type"] == NULL || $meeting["Meeting"]["email_type"] == ""){
			$actions[] = array(
				"href" => Router::url(
						array("controller" => "meetings", "action" => "view", EncryptDecrypt::encrypt($meeting["Meeting"]["id"]))
					),
				"rel"=>"tooltip",
				"data-id" => EncryptDecrypt::encrypt($meeting["Meeting"]["id"]),
				"title"=>__('Ver detalle de la reunión'),
				"class"=>"btn btn-sm",
				"data-toggle"=>"tooltip", 
				"data-placement" =>"top",
				"html" => '<i class="la la-eye la-2x"></i>'
			);
		}
		if(isset($permissionView["view"])){
			if($meeting['Meeting']['password'] != "" || $meeting['Meeting']['password'] != NULL){
				$actions[] = array(
					"href" => Router::url(
							array("controller" => "meetings", "action" => "request_password_meeting", EncryptDecrypt::encrypt($meeting["Meeting"]["id"]))
						),
					"rel"	=>"tooltip",
					"data-id" => EncryptDecrypt::encrypt($meeting["Meeting"]["id"]),
					"title"	=>__('Solicitar la contraseña de la reunión'),
					"class"	=>"btn btn-sm",
					"data-toggle"=>"tooltip", 
					"data-placement" =>"top",
					"html" => '<i class="la la-key  la-2x" aria-hidden="true"></i>'
				);
			}
		}

		if(isset($permission["add"])){
			if($meeting['Meeting']['contac_id'] == NULL){
				$actions[] = array(
					"href" => Router::url(
							array("controller" => "meetings", "action" => "edit", EncryptDecrypt::encrypt($meeting["Meeting"]["id"]))
						),
					"rel"	=>"tooltip",
					"data-id" => EncryptDecrypt::encrypt($meeting["Meeting"]["id"]),
					"title"	=>__('Editar reunión'),
					"class"	=>"btn btn-sm",
					"data-toggle"=>"tooltip", 
					"data-placement" =>"top",
					"html" => '<i class="la la-pencil la-2x"></i>'
				);
			}
		}
		return $actions;
	}

	private function __getJsonResponse($meetings){

		$meetingsJson     = array();
        foreach ($meetings as $meeting) {

        	$permissions = $this->getButtonActions($meeting);

     	 	$meetingsJson[] = array( 
     	 		"id"          => $meeting["Meeting"]["id"],
                "title"       => $meeting["Meeting"]["subject"],
                "location"    => !empty($meeting["Meeting"]["location"]) ? __($meeting["Meeting"]["location"]) : __("No disponible"),
                "start"       => $meeting["Meeting"]["start_date"],   
                "end"         => $meeting["Meeting"]["end_date"], 
                "type"        => __($meeting["Meeting"]["meeting_type"]), 
                "allDay"      => false,  
                "startDate"   => $meeting["Meeting"]["start_date"],  
                "endDate"     => $meeting["Meeting"]["end_date"],  
                "email_type"  => $meeting["Meeting"]["email_type"],  
                "contac_id"   => $meeting["Meeting"]["contac_id"] ,
                "permissions" => $permissions,
            ); 
     	}
     	return $meetingsJson;
	}

	public function addMeetingFollowing(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$dateStart     = isset($this->request->data["dateStart"]) ? $this->request->data["dateStart"]: null;
		$dateEnd       = isset($this->request->data["dateEnd"]) ?  $this->request->data["dateEnd"]: null;
		$modalityId    = isset($this->request->data["modality"]) ?  $this->request->data["modality"]: null;
		$modalityName  = isset($this->request->data["modalityName"]) ?  $this->request->data["modalityName"]: null;
		$response      = $this->__validateDate($dateStart, $dateEnd, $modalityId);
		if($response["state"] == true){
			$response = $this->__validateDateFromMeetings($dateStart, $dateEnd);
			if($response["state"] == true){
				$this->__createSessionDate($dateStart, $dateEnd, $modalityName, $modalityId);				
			} 	 
		}
		$this->outJSON($response);  
	}

	private function __validateDate($dateStart, $dateEnd, $modalityId){
		$today    = date('Y-m-d');	 
		$response = array();
		if($dateStart == ""){
			$response = array("state" => false, "message" => __("La fecha inicio es requerida."));
		} else if ($dateEnd == "") {
			$response = array("state" => false, "message" => __("La fecha fin es requerida."));
		} else if ($dateEnd == $dateStart) {
			$response = array("state" => false, "message" => __("La fecha inicio y fecha fin no pueden ser iguales."));
		} else if ($dateEnd < $dateStart) {
			$response = array("state" => false, "message" => __("La fecha fin no puede ser menor que la fecha inicio."));
		} else if ($modalityId == "") {
			$response = array("state" => false, "message" => __("La modalidad es requerida."));
		} else if ($dateStart < $today) {
			$response = array("state" => false, "message" => __("La fecha inicio no se puede agendar de un día que ya pasó."));
		} else {
			$response = array("state" => true);
		}
		return $response;
	}

	private function __createSessionDate($dateStart, $dateEnd, $modalityName, $modalityId){
		$data   = $this->Session->read('Dates'); 
		$data[] = array( 
			"start_date"   => $dateStart,
			"end_date"	   => $dateEnd,
			"meeting_room" => $modalityId,
			"meeting_name" => $modalityName
		);
		$this->Session->write('Dates', $data); 
	}

	public function delete_following_item(){
		$this->autoRender = false;
		$id = $this->request->data["dateId"];
		$this->Session->delete("Dates.{$id}");
	}

	private function __validateDateFromMeetings($dateStart, $dateEnd){
		$data 	  = $this->Session->read('Dates');
		$response = array();
		if(!empty($data)){
			foreach ($data as $date) {
			 	if($date["start_date"] == $dateStart && $date["end_date"] == $dateEnd){
			 		$response = array("state" => false, "message" => __("Ya hay una reunión en las fechas seleccionadas."));
			 	} else {
		 			$response = array("state" => true);
			 	}
			}
		} else {
			$response = array("state" => true);
		}
		return $response;
	}

	public function addTopicsToFollowing($id = null){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$description      = isset($this->request->data["Topic"]["description"]) ? $this->request->data["Topic"]["description"]: null;
		$time             = isset($this->request->data["Topic"]["time"]) ?  $this->request->data["Topic"]["time"]: null; 
		$response         = $this->__validateTopicsMeetingFollowing($description, $time);
		if($response["state"] == true){
			$this->__updateSessionWithTopics($description, $time, $id); 
		}
		$this->outJSON($response); 
	}

	private function __validateTopicsMeetingFollowing($description, $time){
		$response = array();
		if($description == ""){
			$response = array("state" => false, "message" => __("La descripción es requerida."));
		} else if ($time == 0) {
			$response = array("state" => false, "message" => __("El tiempo es requerido."));
		} else {
			$response = array("state" => true, "message" => __("Se ha agregado el tema específico correctamente."));
		}
		return $response;
	}

	private function __updateSessionWithTopics($description, $time, $id){
		$data  = $this->Session->read('Dates');
		$data[$id]['Topics'][] = array(
			"time"        => $time,
			"description" => $description
		); 
		$this->Session->write('Dates', $data); 
	}

	public function deleteAllSessionMeeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$this->Session->write('Dates', array());
	}

	public function meeting_following(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax'; 
		$data  = $this->Session->read('Dates');
		$this->set(compact('data')); 
	}

	public function show_topics_meeting_following(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax'; 
		$dataId       = isset($this->request->data["dataId"]) ? $this->request->data["dataId"] : null;
		$data         = $this->Session->read('Dates');
		$topics       = isset($data[$dataId]["Topics"]) ? $data[$dataId]["Topics"] : array(); 
		$this->set(compact('topics')); 
	}

	//REDIRECCIÓN CUANDO SE AUTENTIQUE POR OUTLOOK DE LA REUNIÓN DE TIPO NORMAL
	public function saveEventsOutlook(){
		$this->Flash->success(__('La reunión se ha guardado correctamente.')); 
	 	$auth_code   = $this->request->query['code'];
    	$redirectUri = Configure::read("AUTH_OUTLOOK_URL"); 
    	$Oauth       = new oAuthService();  
    	$OutLook     = new OutlookService();  
    	$tokens      = $Oauth::getTokenFromAuthCode($auth_code, $redirectUri); 
	    if (!empty($tokens['access_token']) || isset($tokens['access_token'])) {  
	        // expires_in is in seconds
	        // Get current timestamp (seconds since Unix Epoch) and
	        // add expires_in to get expiration time
	        // Subtract 5 minutes to allow for clock differences
	        $expiration = time() + $tokens['expires_in'] - 300;   
	        // Get the user's email
	        $user      = $OutLook::getUser($tokens['access_token']); 
	        $infoToken = $this->__createSessionToken($tokens, $user, $expiration);
        	$this->Session->write("TokenInfo", $infoToken); 
	        $this->__addInOutlookCalendarMeeting();
	        return $this->redirect(array('action' => 'index','controller' => 'meetings'));
	    } else { 
	        $urlLogin = $Oauth::getLoginUrl($redirectUri); 
	  		header("Location: $urlLogin");
	  		exit();
     	}
	}

	private function __createSessionToken($tokens, $user, $expiration){
		$infoToken = array(
        	"access_token"  => $tokens['access_token'], 
        	"refresh_token" => $tokens['refresh_token'],
        	"token_expires" => $expiration,
        	"user_email"	=> $user['EmailAddress']
    	); 
    	return $infoToken;
	}

	//GUARDADO DE LA REUNIÓN DE TIPO NORMAL Y GUARDADO EN LOS CALENDARIOS DE OUTLOOK
	private function __addInOutlookCalendarMeeting(){
		$this->autoRender = false;		
		$tokenInfo = $this->Session->read("TokenInfo");
		$data      = $this->Session->read("Meeting");
		if($this->Meeting->save($data["Meeting"])) {
			$this->__logAfterAddMeeting($data);
			$this->Session->write("MeetinId", $this->Meeting->id);
			$users = $this->Session->read("Inviteds"); 
			$date  = $this->__buildDateToEvent($data["Meeting"]["start_date"], $data["Meeting"]["end_date"]);
			$datos['Subject'] 			  = $data["Meeting"]["subject"];
	      	$datos['Body']['ContentType'] = "HTML";
	      	$datos['Body']['Content']     = $data["Meeting"]["description"];
	      	$datos['Start']['DateTime']   = $date["fechaInicioFinal"];
	      	$datos['Start']['TimeZone']   = "America/Bogota"; 
	      	$datos['End']['DateTime']     = $date["fechaFinFinal"];
	      	$datos['End']['TimeZone']     = "America/Bogota"; 
			$datos['Location']['DisplayName'] = !empty($data["Meeting"]["location"]) ? $data["Meeting"]["location"]: null;
	      	$usersAttends 	  = $this->__getAttends($users);
	      	$dataFinalMeeting = array("datos" => $datos, "usersAttends" => $usersAttends); 
	      	$this->__saveEventInOutlook($data, $tokenInfo, $dataFinalMeeting);
	      	$this->Meeting->notificationFirebaseMeetingCreated($this->Meeting->id);
		}
	}

	private function __saveEventInOutlook($data = array(), $tokenInfo = array(), $dataFinalMeeting = array()){
		$OutLook     = new OutlookService(); 
      	$createEvent = $OutLook::postCalendars($tokenInfo["access_token"], $tokenInfo["user_email"], $dataFinalMeeting);
      	$this->Meeting->saveCalendarIdMeeting($this->Session->read("MeetinId"), $createEvent["Id"]);
      	$this->Meeting->saveFromCreatedCalendar($this->Session->read("MeetinId"), "outlook"); 
		$this->Meeting->Tag->saveTags($data, $this->Session->read("MeetinId"));
	    $this->Meeting->Topic->saveTopics($this->Session->read("MeetinId"), $data);
        $this->Meeting->Assistant->saveAllUsersInviteds($data["Meeting"]["team_id"], $this->Session->read("MeetinId"), $this->Session->read("Inviteds"));
		$this->__sendMailInvitationsReunionNormal($this->Session->read("MeetinId"));
	    $this->__logAfterAddOtherInfo($this->Session->read("MeetinId")); 
	    $this->Session->delete("Meeting");
	    $this->Session->delete("Inviteds");
	    $this->Session->delete("MeetinId"); 		
	}

	private function __buildDateToEvent($startDate, $endDate){
	    $createDateInicio  = new DateTime($startDate);
	    $createDateFin     = new DateTime($endDate);
	    $createHoraInicio  = new DateTime($startDate);
	    $createHoraFin     = new DateTime($endDate);
	    $fechaInicio       = $createDateInicio->format('Y-m-d');
	    $fechaFin          = $createDateFin->format('Y-m-d');
	    $horaInicio        = $createHoraInicio->format('H:i');
	    $horaFin           = $createHoraFin->format('H:i');
	    $segundos          = ":00";
	    $t = "T";
	    $fechaInicioFinal = $fechaInicio.$t.$horaInicio.$segundos;
	    $fechaFinFinal    = $fechaFin.$t.$horaFin.$segundos;
	    return array("fechaInicioFinal" => $fechaInicioFinal, "fechaFinFinal" => $fechaFinFinal);
	}

	private function __getAttends($users){
		$usersAttends = array();
      	foreach ($users as $user) {
      		$usersAttends[] = array(
      			"EmailAddress" => array(
  					"Address" => $user["email"],
  					"Name"	  => $user["name"]
  				),
  				"Type" => "Required"
  			); 
      	}
      	return $usersAttends; 
	}

	//PROCESO DE GUARDADO DE REUNIÓN DE SEGUIMIENTO CUANDO SE AUTENTIQUE VÍA OUTLOOK API
	public function saveEventsOutlookFollowingMeetings(){ 
		$this->Flash->success(__('La reunión de seguimiento se ha guardado correctamente.')); 
	 	$auth_code   = $this->request->query['code'];
    	$redirectUri = Configure::read("AUTH_OUTLOOK_URL_MEETING_FOLLOWING"); 
    	$Oauth       = new oAuthService();  
    	$OutLook     = new OutlookService();  
    	$tokens      = $Oauth::getTokenFromAuthCode($auth_code, $redirectUri); 
	    if (!empty($tokens['access_token']) || isset($tokens['access_token'])) {  
	        // expires_in is in seconds
	        // Get current timestamp (seconds since Unix Epoch) and
	        // add expires_in to get expiration time
	        // Subtract 5 minutes to allow for clock differences
	        $expiration = time() + $tokens['expires_in'] - 300;   
	        // Get the user's email
	        $user      = $OutLook::getUser($tokens['access_token']); 
	        $infoToken = $this->__createSessionToken($tokens, $user, $expiration);
        	$this->Session->write("TokenInfo", $infoToken); 
	        $this->__addInGmailCalendarFollowingMeeting();
	        return $this->redirect(array('action' => 'index','controller' => 'meetings'));
	    } else { 
	        $urlLogin = $Oauth::getLoginUrl($redirectUri); 
	  		header("Location: $urlLogin");
	  		exit();
     	}
	}

	//PROCESO DE GUARDAR LA REUNIÓN DE TIPO SEGUIMIENTO EN OUTLOOK VARIAS REUNIONES
	private function __addInGmailCalendarFollowingMeeting(){
		$this->autoRender = false; 
		$data       = $this->Session->read("MeetingFollowing");
		$meetings   = $this->Session->read("Dates");
		$this->__buildDataToEventOutlook($meetings, $data);
		$meetingIds = $this->__saveAllReunions($data, $meetings);
		$this->Meeting->updateCreatedFromOutlookMeetingFollowing($meetingIds); 		
	    $this->Session->delete("Inviteds");
	    $this->Session->delete("MeetingFollowing");
	    $this->Session->delete("Dates", array()); 
	}

	//CONSTRUCCIÓN DE LA INFORMACIÓN DE TODAS LAS REUNIONES QUE SE HAYAN AGENDADO
	private function __buildDataToEventOutlook($meetings = array(), $data = array()){
		$OutLook     = new OutlookService(); 
		$users       = $this->Session->read("Inviteds");
		$tokenInfo   = $this->Session->read("TokenInfo");
		$calendarIds = array(); 
		if(!empty($meetings)){
		 	foreach ($meetings as $meeting) { 
			 	$date = $this->__buildDateToEvent($meeting["start_date"], $meeting["end_date"]);
			    $datos['Subject'] 			  = $data["Meeting"]["subject"];
		      	$datos['Body']['ContentType'] = "HTML";
		      	$datos['Body']['Content']     = $data["Meeting"]["description"];
		      	$datos['Start']['DateTime']   = $date["fechaInicioFinal"];
		      	$datos['Start']['TimeZone']   = "America/Bogota";
		      	$datos['End']['DateTime']     = $date["fechaFinFinal"];
		      	$datos['End']['TimeZone']     = "America/Bogota";
				$datos['Location']['DisplayName'] = !empty($data["Meeting"]["location"]) ? $data["Meeting"]["location"]: null;
		      	$usersAttends 	  			  = $this->__getAttends($users); 
		 		$dataFinalMeeting = array("datos" => $datos, "usersAttends" => $usersAttends);       
	      		$createEvent      = $OutLook::postCalendars($tokenInfo["access_token"], $tokenInfo["user_email"], $dataFinalMeeting); 	 		 
				$calendarIds[]    = $createEvent["Id"]; 
			} 
			$this->Session->write("CalendarIds", $calendarIds); 
		}
	}

	//PROCESO DE CONSULTA DE LOS EVENTOS DEL USUARIO EN OUTLOOK
	public function getEventsOutlook(){
		$this->Flash->success(__('El calendario se ha sincronizado correctamente.'));   
	 	$auth_code   = $this->request->query['code'];
    	$redirectUri = Configure::read("AUTH_OUTLOOK_URL_EVENTS"); 	    
    	$Oauth       = new oAuthService();  
    	$OutLook     = new OutlookService();  
    	$tokens      = $Oauth::getTokenFromAuthCode($auth_code, $redirectUri); 
	    if (!empty($tokens['access_token']) || isset($tokens['access_token'])) {  
	        // expires_in is in seconds
	        // Get current timestamp (seconds since Unix Epoch) and
	        // add expires_in to get expiration time
	        // Subtract 5 minutes to allow for clock differences
	        $expiration = time() + $tokens['expires_in'] - 300;   
	        // Get the user's email
	        $user      = $OutLook::getUser($tokens['access_token']);  
	        $infoToken = $this->__createSessionToken($tokens, $user, $expiration);
        	$this->Session->write("TokenInfo", $infoToken); 
	        $this->__getAndSaveEventsOutlook();
	        return $this->redirect(array('action' => 'index','controller' => 'meetings'));
	    } else { 
	        $urlLogin = $Oauth::getLoginUrl($redirectUri); 
	  		header("Location: $urlLogin");
	  		exit();
     	}
	}

	//METODO DE EDITAR UN EVENTO DE GMAIL Y OBTENCIÓN DEL CLIENTE
	public function edit_meeting_gmail(){
		$this->autoRender = false; 
		$base  			  = ROOT . DS . 'listevents.php';
 	 	require_once $base; 
 	 	$urlRedirect = Configure::read("REDIRECT_EDIT_EVENT");
 	 	$client 	 = getClient($urlRedirect);  
 	 	die();
	}

	// AQUI LO MANDARÁ LUEGO DE AUTENTICARSE DE GMAIL PARA EDITAR LA REUNION
	public function finish_process_edit_meeting(){ 
	 	if (isset($this->request->query['code']) || $this->request->query['callback'] == true) {
	 		$this->__editMeetingGmailFinalProcess();
	 	} else {
	 		$this->Flash->success(__('No se ha podido editar la reunión, inténtelo nuevamente.'));
			return $this->redirect(array('action' => 'index'));	 
	 	}  
	}

	private function __editMeetingGmailFinalProcess(){
		$meeting = $this->Session->read("MeetingInfoEdit"); 
 	 	$meeting["Meeting"]["id"] = EncryptDecrypt::decrypt($meeting["Meeting"]["id"]);
 	 	$this->Meeting->id        = EncryptDecrypt::decrypt($meeting["Meeting"]["id"]);
 	 	$data = $this->Meeting->buildaRequestDataMeetingEdit($meeting);
	    $this->Meeting->save($data, array('validate'=>false));
		$primary = Authcomponent::user("email"); 
 		$this->__updateListAssistants($meeting["Meeting"]["id"], $meeting["Meeting"]["team_id"]);
		$infoEditToGmail = $this->Meeting->infoToEditMeeting($meeting); 
 		$base = ROOT . DS . 'listevents.php';
 		require_once $base; 
 	 	$client = getClient(); 
 		$events = editMeetingGmail($primary, $infoEditToGmail, $client);
 		$this->Flash->success(__('Se ha editado la reunión correctamente.'));
		return $this->redirect(array('action' => 'index'));	  
	}

	private function __getAndSaveEventsOutlook(){
		$tokenInfo = $this->Session->read("TokenInfo");
		$OutLook   = new OutlookService();  
		$events    = $OutLook::getCalendars($tokenInfo["access_token"], $tokenInfo["user_email"]); 
		$this->__updateCalendarOutlook($events);
	}

	private function __updateCalendarOutlook($events = array()){ 
  		$this->Meeting->deleteAll(array('Meeting.email_type' => "outlook","Meeting.user_id" => Authcomponent::user("id")), false);
		$recursive        = -1;
	 	$fields           = array("Assistant.meeting_id");
	 	$conditions       = array("Assistant.user_id" => Authcomponent::user('id'));
	 	$meetingsReceived = $this->Meeting->Assistant->find("list", compact("conditions","fields"));
		$conditions       = array("OR" => array('Meeting.id' => $meetingsReceived,"Meeting.start_date >=" => date('Y-m-01'), "Meeting.end_date <=" => date('Y-m-t')), "Meeting.user_id" => AuthComponent::user("id")); 
		$fields           = array('Meeting.subject','Meeting.location','Meeting.description','Meeting.start_date','Meeting.end_date');
		$meetings         = $this->Meeting->find("all", compact("conditions","recursive","fields"));	
		$meetingsDb       = array();
		foreach ($meetings as $key => $meeting) {
			$meetingsDb[] =  $meeting["Meeting"];
		} 
	
		$this->Meeting->getRecordToUpdateCalendarOutlook($events, $meetingsDb); 
	} 

	public function show_detail_day_meeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax';
		$meetingId    = isset($this->request->data["dataId"]) ? EncryptDecrypt::decrypt($this->request->data["dataId"]) : null;
		$meeting      = $this->Meeting->searchDetailMeeting($meetingId); 
	 	$this->set(compact('meeting')); 
	}

	public function createSessionToMeetingContac(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->Session->write("MeetingContac", array());
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		$meetingId        = isset($this->request->data["dataId"]) ? EncryptDecrypt::decrypt($this->request->data["dataId"]): null; 
		$meeting          = $this->Meeting->getInfoMeetingToContact($meetingId); 
		$response         = true;
		$this->Session->write("MeetingContac", $meeting);
		$this->outJSON($response);  
	}

	private function __sendMailInvitationsReunionNormal($meting_id = null){ 
		$conditions = array("Assistant.meeting_id" => $meting_id); 
		$assistants = $this->Meeting->Assistant->find("all", compact("conditions")); 
		$datos = $this->Meeting->detailMeetingRelations($meting_id);
		foreach ($assistants as $assistant) { 
		 	$options = array(
		 		'to'	   => $assistant["User"]["email"],
				'template' => 'detail_meeting',
				'subject'  => __('Citación a reunión').' - '.Configure::read('Application.name'),
				'vars'     =>  array('datos'=>$datos),
			);
		 	$this->sendMail($options);
	 	}
	}

	private function __sendMailInvitationsReunionSeguimiento($meting_ids = array()){ 
		$conditions     = array("Assistant.meeting_id" => $meting_ids["0"]['meeting_id']); 
		$assistants     = $this->Meeting->Assistant->find("all", compact("conditions"));
		$meetingDetail  = $this->Meeting->detailMeetingRelations($meting_ids["0"]['meeting_id']);
		$meetingIds     = Set::extract($meting_ids, '{n}.meeting_id');
		$meetFollowings = $this->Meeting->searchDetailMeeting($meetingIds); 
		$row = count($meting_ids); 
		foreach ($assistants as $assistant) { 
		 	$options = array(
		 		'to'		=> $assistant["User"]["email"],
				'template' => 'detail_meeting_tracing',
				'subject'  => __('Citación a reunión de seguimiento').' - '.Configure::read('Application.name'),
				'vars'     =>  array('meetingsFollowings' => $meetFollowings,'assistants' => $assistants, 'meetingDetail' => $meetingDetail,'meting_ids' => $meting_ids,'row' => $row),
			);
		 	$this->sendMail($options);
	 	} 
	}

	//CREACIÓN DE LA REUNIÓN DE TIPO NORMAL SIN GUARDAR EN LOS CALENDARIOS DE GMAIL O DE OUTLOOK
	public function addMeetingWithoutSync(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
	 	$data      		  = $this->Session->read("Meeting"); 
	 	if($this->Meeting->save($data, array('validate'=>false))) { 
	 		$this->__logAfterAddMeeting($data);
			$this->Session->write("MeetinId", $this->Meeting->id);
			$this->Meeting->Tag->saveTags($data, $this->Session->read("MeetinId"));
		    $this->Meeting->Topic->saveTopics($this->Session->read("MeetinId"), $data);
	        $this->Meeting->Assistant->saveAllUsersInviteds($data["Meeting"]["team_id"], $this->Session->read("MeetinId"), $this->Session->read("Inviteds")); 
			$this->__logAfterAddOtherInfo($this->Session->read("MeetinId"));
			$this->Meeting->notificationFirebaseMeetingCreated($this->Meeting->id);
			$this->__sendMailInvitationsReunionNormal($this->Session->read("MeetinId")); 
			$this->Flash->success(__('Se ha guardado la reunión correctamente.'));   
		    $this->Session->delete("Meeting");
		    $this->Session->delete("Inviteds");
		    $this->Session->delete("MeetinId");
	 	}
	}

	//GUARDADO DE LA REUNIÓN DE TIPO SEGUIMIENTO SIN GUARDAR EN LOS CALENDARIOS DE GMAIL Y OUTLOOK
	public function addMeetingFollingWithoutSync(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout          = 'ajax';
		$this->autoRender      = false;
		$users     			   = $this->Session->read("Inviteds");
		$meetingFormData       = $this->Session->read("MeetingFollowing");
		$meetingsFollowingData = $this->Session->read("Dates");
		$meetingsIds           = $this->__saveAllReunions($meetingFormData, $meetingsFollowingData); 
		$this->Meeting->updateAll(array('Meeting.created_from' => NULL), array('Meeting.id' => $meetingsIds));
		$this->Flash->success(__('Se ha guardado la reunión de seguimiento correctamente.'));  		
	    $this->Session->delete("Inviteds");
	    $this->Session->delete("MeetingFollowing");
	    $this->Session->delete("Dates", array());
	}

	private function __logAfterAddMeeting($data, $id = null){
		$modality = "";		
		if($data["Meeting"]["meeting_room"] == Configure::read("MEETING_PRESENTIAL")){
			$modality = __("Presencial");
		} else if ($data["Meeting"]["meeting_room"] == Configure::read("MEETING_TELEPHONE")) {
			$modality = __("Telefónica");
		} else if ($data["Meeting"]["meeting_room"] == Configure::read("MEETING_VIRTUAL")) {
			$modality = __("Virtual");
		} 
	}

	private function __logAfterAddOtherInfo($meetingIds = array()){
		$conditions = array("Topic.meeting_id" => $meetingIds);
		$recursive  = -1;
		$topics     = $this->Meeting->Topic->find("all", compact("conditions","recursive"));
		$conditions = array("Assistant.meeting_id" => $meetingIds); 
		$assistants = $this->Meeting->Assistant->find("all", compact("conditions"));
		$conditions = array("Tag.meeting_id" => $meetingIds); 
		$tags       = $this->Meeting->Tag->find("all", compact("conditions","recursive"));
		//$this->__logTopics($topics); 
		//$this->__logAssistants($assistants);
		//$this->__logTags($tags); 
	}

	private function __logTopics($topics = array()){
		if(!empty($topics)){
			foreach ($topics as $topic) {
				$duration    = $this->conversorSegundosHorasTopics($topic["Topic"]["time"]);
				$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha creado el tema específico ") . $topic["Topic"]["description"] . __(" con duración de  ") . $duration;
	 			$this->buildInfoLog($topic["Topic"]["id"], $description, NULL, NULL, NULL, NULL); 
			}
		}
	}

	private function __logAssistants($assistants = array()){
		$description = "";
		if(!empty($assistants)){
			foreach ($assistants as $assistant) {
			 	if($assistant["Employee"]["id"] == NULL){
					$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha agendado al funcionario  ") . $assistant["User"]["firstname"] . ' ' . $assistant["User"]["lastname"] . __(" en la reunión ") . $assistant["Meeting"]["subject"];
	 				$this->buildInfoLog($assistant["User"]["id"], $description, NULL, NULL, NULL, NULL); 
			 	} else {
			 		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha agendado al asistente externo  ") . $assistant["Employee"]["name"] . __(" en la reunión ") . $assistant["Meeting"]["subject"];
	 				$this->buildInfoLog($assistant["Employee"]["id"], $description, NULL, NULL, NULL, NULL); 
			 	}
			}
		} 
	}

	private function __logTags($tags = array()){ 
		if(!empty($tags)){
			foreach ($tags as $tag) {
				$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha registrado el tag  ") . $tag["Tag"]["name"];
 				$this->buildInfoLog($tag["Tag"]["id"], $description, NULL, NULL, NULL, NULL); 
 			}
		}
	}

	public function save_session_edit_meeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$this->Session->write("MeetingEditOutlook", $this->request->data);
	} 

	//REDIRECCION CUANDO SE AUTENTIQUE EN OUTLOOK
	public function editEventOutlook(){ 
		$auth_code   = $this->request->query['code'];
    	$redirectUri = Configure::read("AUTH_OUTLOOK_URL_EDIT_EVENTS"); 	    
    	$Oauth       = new oAuthService();  
    	$OutLook     = new OutlookService();  
    	$tokens      = $Oauth::getTokenFromAuthCode($auth_code, $redirectUri); 
	    if (!empty($tokens['access_token']) || isset($tokens['access_token'])) {  
	        // expires_in is in seconds
	        // Get current timestamp (seconds since Unix Epoch) and
	        // add expires_in to get expiration time
	        // Subtract 5 minutes to allow for clock differences
	        $expiration = time() + $tokens['expires_in'] - 300;   
	        // Get the user's email
	        $user      = $OutLook::getUser($tokens['access_token']);  
	        $infoToken = $this->__createSessionToken($tokens, $user, $expiration);
        	$this->Session->write("TokenInfo", $infoToken); 
	        $this->__editMeetingAfterAuthOutlook();
	        $this->Flash->success(__('Se ha editado la reunión correctamente.')); 
	        return $this->redirect(array('action' => 'index','controller' => 'meetings'));
	    } else { 
	        $urlLogin = $Oauth::getLoginUrl($redirectUri); 
	  		header("Location: $urlLogin");
	  		exit();
     	}
	}

	private function __editMeetingAfterAuthOutlook(){
		$meeting 	= $this->Session->read("MeetingEditOutlook");
		$meeting["Meeting"]["id"] = EncryptDecrypt::decrypt($meeting["Meeting"]["id"]);
		$this->Meeting->id = $meeting["Meeting"]["id"];
		$conditions = array("Meeting.id" => $meeting["Meeting"]["id"]);
		$idEvent 	= $this->Meeting->field("ical_uid", $conditions); 
 	 	$data       = $this->Meeting->buildaRequestDataMeetingEdit($meeting); 
	    $this->Meeting->save($data, array('validate'=>false)); 
		$this->__updateListAssistants($meeting["Meeting"]["id"], $meeting["Meeting"]["team_id"]); 
		$users   	= $this->Meeting->getAssistantToEditMeeting($meeting);
		$datos 		= array();
		$date  = $this->__buildDateToEvent($meeting["Meeting"]["start_date"], $meeting["Meeting"]["end_date"]);
	    $datos['Subject'] 			  = $meeting["Meeting"]["subject"];
      	$datos['Body']['ContentType'] = "HTML";
      	$datos['Body']['Content']     = $meeting["Meeting"]["description"];
      	$datos['Start']['DateTime']   = $date["fechaInicioFinal"];
      	$datos['Start']['TimeZone']   = "America/Bogota";
      	$datos['End']['DateTime']     = $date["fechaFinFinal"];
      	$datos['End']['TimeZone']     = "America/Bogota";
		$datos['Location']['DisplayName'] = !empty($meeting["Meeting"]["location"]) ? $meeting["Meeting"]["location"]: null;
		$attends = $this->__getAttends($users); 
		$dataFinalMeeting = array("datos" => $datos, "usersAttends" => $attends, "idEvent" => $idEvent);
		$tokenInfo = $this->Session->read("TokenInfo");
		$this->__updateEventApi($dataFinalMeeting, $tokenInfo);
	}

	private function __updateEventApi($dataFinalMeeting = array(), $tokenInfo = array()){ 
		$OutLook   = new OutlookService(); 
      	$editEvent = $OutLook::updateCalendars($tokenInfo["access_token"], $tokenInfo["user_email"], $dataFinalMeeting);
		$this->Session->delete("MeetingEditOutlook"); 
	}

	//PROCESO PARA MANDAR NOTIFICACION CUANDO SE ELIMINE, EDITE UNA REUNION
	private function __updateListAssistants($meetingId, $teamId){ 
		$assistants = $this->Session->read("StoredAssistant"); 
		if(!empty($assistants)){
			foreach ($assistants as $assistant) {
				if($assistant["state"] == Configure::read("NEW_ASSISTANT")){
					$this->__newAssistantEditMeeting($assistant, $meetingId, $teamId);
				} else if ($assistant["state"] == Configure::read("DELETE_ASSISTANT")) {
					$this->__deleteAssistantEditMeeting($assistant, $meetingId); 
				} 
			}
		} 
		$this->__notificationDeleteAssistants($meetingId); 
		$this->__notificationOldAssistants($meetingId);
		$this->__notificationNewAssistants($meetingId);
		$this->Session->write("StoredAssistant", array()); 
	}

	private function __notificationOldAssistants($meetingId){
		$recursive  = 0;
		$fields     = array("Assistant.meeting_id","Assistant.user_id","User.*");
	 	$conditions = array("Assistant.meeting_id" => $meetingId, "Assistant.state" => 1);	 	
		$assistants = $this->Meeting->Assistant->find("all", compact("conditions","recursive","fields"));
		$emails = array();
		foreach ($assistants as $assistant) { 
			$emails[] = $assistant["User"]["email"]; 
		}
		$this->__notificationEditMeeting($emails, $meetingId); 
	}

	private function __notificationNewAssistants($meetingId){
		$recursive  = 0;
		$fields     = array("Assistant.meeting_id","Assistant.user_id","User.*");
	 	$conditions = array("Assistant.meeting_id" => $meetingId, "Assistant.state" => 2);	 	
		$assistants = $this->Meeting->Assistant->find("all", compact("conditions","recursive","fields"));
		$emails = array();
		foreach ($assistants as $assistant) { 
			$emails[] = $assistant["User"]["email"]; 
		}
		if(!empty($emails)){
			$this->Meeting->Assistant->updateAll(array('Assistant.state' => 1), array('Assistant.meeting_id' => $meetingId, 'Assistant.state' => 2));
			$this->__notificationToAssistantsNewInMeetingEdit($meetingId, $emails);
		} 
	}

	private function __notificationDeleteAssistants($meetingId){
		$recursive  = 0;
		$fields     = array("Assistant.meeting_id","Assistant.user_id","User.*");
	 	$conditions = array("Assistant.meeting_id" => $meetingId, "Assistant.state" => 3);	 	
		$assistants = $this->Meeting->Assistant->find("all", compact("conditions","recursive","fields"));
		$emails = array();
		foreach ($assistants as $assistant) { 
			$emails[] = $assistant["User"]["email"]; 
		}
		if(!empty($emails)){
			$this->Meeting->Assistant->deleteAll(array('Assistant.meeting_id' => $meetingId, "Assistant.state" => 3), false);
			$this->__notificationMeetingCanceled($meetingId, $emails);
		}  
	} 
 
	//NOTIFICACION DE CUANDO SE EDITA UNA REUNION Y SE LE NOTIFICA A LOS QUE YA ESTABAN
	private function __notificationEditMeeting($emails = array(), $meetingId){ 
		$datos = $this->Meeting->detailMeetingRelations($meetingId); 
	 	$options = array(
	 		'to'	   => $emails,
			'template' => 'detail_meeting_edit',
			'subject'  => __('Reunión editada').' - '.Configure::read('Application.name'),
			'vars'     =>  array('datos'=>$datos),
		);
	 	$this->sendMail($options); 
	}

	//NOTIFICACION A LOS NUEVOS USUARIOS QUE FUERON AÑADIDOS A LA REUNION
	private function __notificationToAssistantsNewInMeetingEdit($meetingId, $emails = array()){ 
		$datos = $this->Meeting->detailMeetingRelations($meetingId); 
			$options = array(
		 		'to'	   => $emails,
				'template' => 'detail_meeting',
				'subject'  => __('Citación a reunión').' - '.Configure::read('Application.name'),
				'vars'     =>  array('datos'=>$datos),
			);
	 	$this->sendMail($options); 
	}
	 

	//NOTIFICACIONE A LOS USUARIOS QUE LA REUNION FUE CANCELADA
	private function __notificationMeetingCanceled($meetingId, $emails = array()){	 
		$datos = $this->Meeting->detailMeetingRelations($meetingId);
		$options = array(
		 		'to'	   => $emails,
				'template' => 'canceled_meeting',
				'subject'  => __('Citación cancelada').' - '.Configure::read('Application.name'),
				'vars'     =>  array('datos'=>$datos),
			);
	 	$this->sendMail($options); 
	}

	private function __newAssistantEditMeeting($assistant, $meetingId, $teamId){  
		$saveData = array(
			"Assistant" => array(
				"meeting_id"  => $meetingId,
				"user_id"     => $assistant["user_id"],
				"type_user"   => EncryptDecrypt::decrypt($assistant["type_user"]),
				"team_id"     => $teamId,
				"state" 	  => Configure::read("NEW_ASSISTANT")
			)
		); 
		$this->Meeting->Assistant->create();
		$this->Meeting->Assistant->save($saveData);
	}

	private function __deleteAssistantEditMeeting($assistant, $meetingId){ 
		$this->Meeting->Assistant->updateAll(array('Assistant.state'  => Configure::read("DELETE_ASSISTANT")), array('Assistant.meeting_id' => $meetingId, 'Assistant.user_id' => $assistant["user_id"])); 
	}

	public function search_teams_clients(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax'; 
		$teamId  	  = isset($this->request->data["teamId"]) ? $this->request->data["teamId"] : NULL;
		$conditions   = array("Client.team_id" => $teamId);
		$clients      = $this->Meeting->Client->find("list", compact("conditions"));
		$assistants   = $this->Session->read("Inviteds");
		$assistantsEdit = $this->Session->read("StoredAssistant");
		$this->__cleanUsersTeam($assistants); 
		$this->__cleanUsersTeamEdit($assistantsEdit); 
		$this->set(compact('clients')); 
	}

	private function __cleanUsersTeam($assistants){
		if(!empty($assistants)){
			foreach ($assistants as $userId => $assistant) {
				$typeUser = EncryptDecrypt::decrypt($assistant["type_user"]);
				if($typeUser == configure::read("COLLABORATOR")){
					unset($assistants[$userId]);
				}
			} 
			$this->Session->write("Inviteds", $assistants); 
		}
	}

	private function __cleanUsersTeamEdit($assistants){ 
		if(!empty($assistants)){
			foreach ($assistants as $userId => $assistant) {
				if($assistant["type_user"] == configure::read("COLLABORATOR")){ 
					unset($assistants[$userId]);
				}
			} 
			$this->Session->write("StoredAssistant", $assistants); 
		}
	}

	public function clean_clients_assistants(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax'; 
		$this->autoRender = false;
		$assistants   	  = $this->Session->read("Inviteds"); 
		if(!empty($assistants)){
			foreach ($assistants as $userId => $assistant) {
				$typeUser = EncryptDecrypt::decrypt($assistant["type_user"]);
				if($typeUser == configure::read("EXTERNAL")){
					unset($assistants[$userId]);
				}
			} 
			$this->Session->write("Inviteds", $assistants); 
		}
		return true;
	}

	public function clean_clients_assistants_edit(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax'; 
		$this->autoRender = false;
		$assistants   	  = $this->Session->read("StoredAssistant");
		if(!empty($assistants)){
			foreach ($assistants as $userId => $assistant) { 
				if($assistant["type_user"] == configure::read("EXTERNAL")){
					unset($assistants[$userId]);
				}
			} 
			$this->Session->write("StoredAssistant", $assistants); 
		}
		return true;
	}

    //Recuperar contraseña de la reunión
    public function request_password_meeting($id = null){
    	$id = EncryptDecrypt::decrypt($id);
		if (!$this->Meeting->exists($id)) {
			$this->showMessageExceptions();
		}
		$this->autoRender = false; 
		$conditions = array("Meeting.id" => $id);
		$recursive  = -1;
		$meeting 	= $this->Meeting->find("first", compact("conditions","recursive"));
	 	$user = AuthComponent::user("firstname") . ' ' . AuthComponent::user("lastname"); 
		$opts = array(
			'to'       => AuthComponent::user("email"),
			'subject'  =>__('Contraseña de la reunión').' - '.Configure::read('Application.name'),
			'vars'     => array('user' => $user, 'password' => $meeting["Meeting"]["password"], "id" => $meeting["Meeting"]["id"]),
			'template' => 'request_password_meeting',
		); 
		$this->sendMail($opts);
		$this->Flash->success(__('Se ha enviado una notificación a tu correo electrónico con la información de la contraseña de la reunión.'));
		return $this->redirect(array('action' => 'index','controller' => 'meetings')); 	 
    }

} 
