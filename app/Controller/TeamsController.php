<?php
App::uses('AppController', 'Controller');
 
class TeamsController extends AppController {
 
	public $components = array('Paginator');
 
	public function dashboard() {
		$conditions   = array('Team.user_id' => AuthComponent::user('id')); 
		$conditions[] = $this->Team->buildConditions($this->request->query);
		$order        = array('Team.created DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
        $teams 		  = $this->Paginator->paginate(); 
		$this->set(compact('teams')); 
	}

	public function index(){
		$myTeams 		= 		$this->getUserOwnerTeams();
		$otherTeams 	= 		$this->getUserOtherTeams();
		$this->set(compact("myTeams","otherTeams"));
    	$teamId = 5;
		$this->set("teamId",$teamId);
		$this->set("teamData",$this->Team->field("user_id",array("Team.id" => $teamId)));
	}

	private function getUserOwnerTeams(){
		$recursive	  = -1;
		$conditions   = array("Team.user_id" => AuthComponent::user("id"));
		$order        = array('Team.created DESC');
		return $this->Team->find("all",compact("conditions","order","recursive"));
	}

	private function getUserOtherTeams(){
		$conditions = array("UserTeam.user_id" => AuthComponent::user("id"),"UserTeam.own_user_id <>"=>AuthComponent::user("id"),"UserTeam.type_user"=> Configure::read("COLLABORATOR"));
		$order        = array('Team.created DESC');
		return $this->Team->UserTeam->find("all",compact("conditions","order"));
	}

	public function view($id = null) {
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Team->exists($id)) {
			$this->showMessageExceptions();
		} 
		$recursive 	  =  0;
		$conditions   = array('Team.id' => $id);
		$team    	  = $this->Team->find('first', compact("conditions","recursive"));
		$conditions   = array("UserTeam.team_id" => $id);
		$limit        = 10; 
		$group        = array("UserTeam.user_id"); 
		$this->Paginator->settings = compact('conditions','limit','group');
        $members      = $this->Paginator->paginate($this->Team->UserTeam); 

		$this->set(compact('team','members')); 
	}
 
	public function add($step = null) {
		$stepApp   = $this->getCacheStep();
		$teamCount = 0;
		if($stepApp == configure::read("ENABLED")){
			$teamCount = $this->Team->getTotalTeam(Authcomponent::user("id"));
		}  
		if ($this->request->is('post')) {
			$validations = $this->getValidates($this->request->data,'Team');
			$this->Team->create();
			$this->request->data["Team"]["user_id"] = Authcomponent::user("id");
			if ($this->Team->save($this->request->data)) {
				$this->__createUserTeamWithPlan($this->Team->id);
				$this->Flash->success(__('Los datos se han guardado correctamente.'));
				if($stepApp == configure::read("ENABLED")){
					return $this->redirect(array('action' => 'index'));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Flash->fail(implode('<br>', $validations));
			}
		} 
		$plan   = $this->Team->User->PlanUser->getPlanActualUserContac(); 
		$this->set(compact('plan','step','teamCount','stepApp'));		   
	}
 
	public function edit($id = null) {
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Team->exists($id)) {
			$this->showMessageExceptions();
		}
		if ($this->request->is(array('post', 'put'))) {
			$validations = $this->getValidates($this->request->data,'Team');
			if ($this->Team->save($this->request->data)) { 
				$this->updateCachePermissions();
				$this->Flash->success(__('Los datos se han guardado correctamente.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->fail(implode('<br>', $validations));
			}
		} else {
			$options = array('conditions' => array('Team.' . $this->Team->primaryKey => $id));
			$this->request->data = $this->Team->find('first', $options);
		}  
		$this->__buildList(); 
	} 

	private function __createUserTeamWithPlan($teamId){
		$this->Team->User->PlanUser->Plan->unbindModel(array('hasMany' => array('Transaction','Payment'))); 
		$conditions     = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("ENABLED"), "PlanUser.expired" => Configure::read("DISABLED"));
		$planActualInfo = $this->Team->User->PlanUser->Plan->find("first", compact("conditions"));
		if(!empty($planActualInfo)){
			$data  = array(
		 		"user_id" 	  => AuthComponent::user("id"),
		 		"own_user_id" => AuthComponent::user("id"),
		 		"type_user"   => Configure::read("ENABLED"),
		 		"position_id" => 1, //Id del permiso "Control total"
		 		"client_id"   => NULL,
		 		"team_id"     => $teamId
		 	);  
	 		$this->Team->UserTeam->save($data, array('validate'=>false));  
	 		$this->updateCachePermissions();
		}  
	}

	public function find_clients(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax';
        $teamId        = isset($this->request->data["teamId"]) ? $this->request->data["teamId"] : NULL;   
        $clients       = $this->Team->Client->getClientTeam($teamId);
        $this->set(compact('clients')); 
	} 

	public function check_permission_team_upload_white_label(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax';
        $teamId        = isset($this->request->data["teamId"]) ? $this->request->data["teamId"] : NULL;
        $action        = isset($this->request->data["action"]) ? $this->request->data["action"] : NULL;
        $planActual    = $this->Team->checkPermissionInPlan($teamId); 
        $img           = $this->Team->verifyWhiteLabel($teamId);
		$this->set(compact('planActual','img','action')); 
	}

	public function check_permission_team_create_template(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax';
        $teamId        = isset($this->request->data["teamId"]) ? $this->request->data["teamId"] : NULL;  
		$planActual   = $this->Team->checkPermissionInPlan($teamId);
		$this->set(compact('planActual'));
	}

	private function __buildList($step = null){
		$plan = $this->Team->User->PlanUser->getPlanActualUserContac();
		$this->set(compact('plan','step'));
	}

	public function get_permision() {

		$teamId = EncryptDecrypt::decrypt($this->request->data["teamId"]);
		$teamId = EncryptDecrypt::decrypt($this->request->data["teamId"]);
		$this->set("teamId",$teamId);
		$this->set("teamData",$this->Team->field("user_id",array("Team.id" => $teamId)));
		// echo "<pre>";
		// print_r($this->request->data);
		// die();
	}

}
