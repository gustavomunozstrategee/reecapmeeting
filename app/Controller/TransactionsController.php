<?php
App::uses('AppController', 'Controller');
 
class TransactionsController extends AppController {
 
    public $components = array('Paginator');

    public function beforeFilter() { 
        $this->Auth->allow('get_transaction');
        parent::beforeFilter(); 
    }
 
    public function index() { 
        $this->Transaction->unbindModel(array('hasMany' => array('Payment')));
        $limit                     = 10;
        $order                     = array('Transaction.modified'=>'DESC');
        $conditions                = array('Transaction.user_id' => Authcomponent::user('id'));  
        $conditions[]              = $this->Transaction->buildConditions($this->request->query);
        $this->Paginator->settings = compact('conditions','limit','order');
        $transactions              = $this->Paginator->paginate(); 
        $this->set(compact('transactions')); 
    }
 
    public function view_detail() { 
        if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax'; 
        $this->Transaction->unbindModel(array('hasMany' => array('Payment'))); 
        $transactionId = isset($this->request->data["transactionId"]) ?  EncryptDecrypt::decrypt($this->request->data["transactionId"]) : null; 
        $planId        = isset($this->request->data["planId"]) ?  EncryptDecrypt::decrypt($this->request->data["planId"]) : null; 
        $conditions    = array("Transaction.id" => $transactionId);
        $fields        = array("Transaction.value","Transaction.payment_method_name","Transaction.transactionId","Transaction.state","Transaction.created","Plan.*");
        $transaction   = $this->Transaction->find("first", compact("conditions","fields"));
        $conditions    = array("PlanUser.plan_id" =>  $planId, "PlanUser.user_id" => Authcomponent::user("id"));
        $recursive     = -1;
        $planInfo      = $this->Transaction->Plan->PlanUser->find("first", compact("conditions","recursive")); 
        $this->set(compact('transaction','planInfo'));  
    }
 
    public function disableNotificationTransactionPending(){
        if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout       = 'ajax';
        $this->autoRender   = false;  
        $transactionId      = isset($this->request->data["transactionId"]) ?  $this->request->data["transactionId"] : null; 
        $conditions         = array("Transaction.id" => $transactionId);
        $transactionPending = $this->Transaction->find("first", compact("conditions"));
        if(!empty($transactionPending)) {
            $saveData = array('Transaction' => array(
                'id'     => $transactionPending["Transaction"]["id"],
                'notify' => configure::read("DISABLED")
                )
            ); 
            $userId = $transactionPending["User"]["id"]; 
            $this->Transaction->save($saveData,  array('validate'=>false));
            Cache::delete("transactions_{$userId}",'TRANSACTION_PENDINGS');
        }  
    }

    public function get_transaction(){
        $this->autoRender = false;
        $this->Session->delete("referenceCode");
        $this->Session->delete("val");
        if($this->request->query){
            $recursive   = -1;
            $conditions  = array("Transaction.referenceCode" => $this->request->query["referenceCode"], "Transaction.user_id" => Authcomponent::user("id"));
            $transaction = $this->Transaction->find("first", compact("conditions","recursive"));
            if($transaction["Transaction"]["state"] == configure::read("TRANSACTION_APROBADA")){ 
                $this->Flash->success(__('Tu transacción fue aprobada.'));
                $this->checkTeamCreate();
            } else if ($transaction["Transaction"]["state"] == configure::read("TRANSACTION_RECHAZADA")) { 
                $this->Flash->fail(__('Tu transacción fue rechazada.'));
                return $this->redirect(array('controller' => 'pages','action' => 'index_page'));
            } else if ($transaction["Transaction"]["state"] == configure::read("TRANSACTION_PENDING")) { 
                $this->Flash->info(__('Tu transacción está pendiente.'));
                return $this->redirect(array('controller' => 'pages','action' => 'index_page'));
            }
        }
    }
}
