<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');


class ContacsController extends AppController {

	public $components = array('Paginator','RequestHandler');

	public function beforeFilter() { 
		$this->Auth->allow('average_conctac','reminder_approval_contac','download_contac','delete_files_documents','password','enterPassword','audioPlay');
 		parent::beforeFilter(); 
 	}

	public function re_send(){
		$this->autoRender = false;
		$this->layout     = false;
		if(!empty($this->request->data['Contact']['email'])) {
			$this->request->data['Contact']['email'] = ltrim(rtrim($this->request->data['Contact']['email']));
			$this->request->data['Contact']['email'] = preg_replace("/\s+/", "", $this->request->data['Contact']['email']); 
			$emails = explode(",", $this->request->data['Contact']['email']);
			$this->__resend_Acta($this->request->data['Contact']['ContactId'],$emails);
			$this->Flash->success(__('Acta Reenviada Correctamente.'));
		} else {
			$this->Flash->fail(__('La dirección de correo electrónico es requerida.'));
		}
		$this->redirect($this->request->referer());
	}


	private function __resend_Acta($contacId,$emails){
		$this->__deleteCookieContac($contacId); 
		$info = $this->Contac->getAllInfoToContacPDF($contacId);
				$template = 'esp/contact_finished';
				$subject  = sprintf(__("Acta %s finalizada - %s - %s"), $info["contac"]["Contac"]["number"], $info["contac"]["Client"]["name"], $info["contac"]["Project"]["name"]);
				$options = array(
					'to'       => $emails,
					'template' => $template,
					'subject'  => $subject. ' - ' .Configure::read('Application.name'),
					'vars'     =>  array('info'=>$info),
				);
				$this->sendMail($options);  
	}

	public function index() { 
		$this->Session->delete('passwordEnter');

		$this->Contac->unbindModel(array('hasMany' => array('ContacsFile','ContacsDocument','Commitment','ApprovalContac','CommentContac','QualificationContact','Assistant')));
		$this->Contac->bindCommentContac();
		$this->loadModel("ProjectsPrivacity");
		$this->deleteCookieInitial();
	 	$this->Session->delete("ContacId"); 
		// $this->resetVariablesContac();  
		$teams        = $this->loadTeams();  
		$projectInfo  = $this->ProjectsPrivacity->getPrivacityContacs(); 
		$projects     = $projectInfo["projects"];
		$conditions   = array("OR" => array("Contac.project_id" => $projectInfo["projectIds"], "Contac.user_id" => AuthComponent::user("id")));
		$conditions[] = $this->Contac->buildConditions($this->request->query); 
		$order        = array('Contac.created DESC');
		$limit        = 10;  
		$this->Paginator->settings = compact('conditions','limit','order');
        $contacs = $this->Paginator->paginate();  
		$this->set(compact('contacs','teams','projects'));
	}

	public function edit($id = null, $urlPassword = null) { 
		if(!empty($this->request->data)) {
			$this->__completeEdit();
		}
		$this->Session->delete("approvalAction");
		$id = EncryptDecrypt::decrypt($id);  
		$this->__storeIdContac($id, "edit");
		$this->__validateContacFinish($id);
		$referer    = $this->request->referer();
		$this->Contac->unbindModel(array('hasMany' => array('ContacsFile','ContacsDocument','Commitment','ApprovalContac','CommentContac','QualificationContact','Assistant')));
		$conditions = array('Contac.' . $this->Contac->primaryKey => $id);
		$datos      = $this->Contac->find('first', compact('conditions'));
		$data_firebase = $this->Contac->getFirebase($datos["Contac"]["firebase"]);
		$this->set("data_firebase",$data_firebase);
		$this->__privacityContac($datos);  
		$this->__createCookieContacTimer($id, $datos["Contac"]["duration"]);
		$this->check_team_permission_action(array("index"), array("contacs"), $datos["Contac"]["team_id"]);
        if($referer != FULL_BASE_URL.$this->request->base."/contacs/add") {
        	if ($this->Session->read('passwordEnter') == Configure::read('DISABLED')) {
				$this->resetVariablesContac(); 
        	}
        }
		$codigoContact = $this->Contac->codigoFirebase($id);
        $this->Session->write('codFirebase', $codigoContact);
        $this->Session->write('contactId', $id); 
		$this->Session->write("identity_session", $id);
		if(!empty($urlPassword)) {
			if(EncryptDecrypt::decrypt($urlPassword) != $datos['Contac']['password']) {
    			$this->__validatePasswordContac($datos['Contac']['password'],$this->Session->read('passwordEnter'),true);
			}
		} else {
    		$this->__validatePasswordContac($datos['Contac']['password'],$this->Session->read('passwordEnter'),true);
		}
    	$this->__buildList($datos); 
	}

	private function __completeEdit() {
		$this->request->data = $this->__fieldsDefault($this->request->data, $this->request->data['Contac']['copiesI']); 
   		$validations = $this->getValidates($this->request->data,'Contac');  
		if (!is_array($validations)) { 
			if(!empty($this->request->data['Contac']['approval'])) {   			
				$state = Configure::read('APPROVAL');
			}else {
				$state = Configure::read('DISABLED');
			}
			$this->request->data['Contac']['state']  = $state; 
			$this->request->data['Contac']['id']     = EncryptDecrypt::decrypt($this->request->data['Contac']['id']); 
			$this->request->data['Contac']['copies'] = ltrim(rtrim($this->Contac->Project->findProjectEmailCopies($this->request->data)));
			$this->request->data['Contac']['copies'] = preg_replace("/\s+/", "", $this->request->data['Contac']['copies']); 
			if ($this->Contac->save($this->request->data)) { 
				$this->__saveAllInfoContacAdd($this->Contac->id, $this->request->data, array('savedAction'=> 'completeContact', 'state'=>$state));
				$this->deleteCookieInitial();
				$this->Flash->success(__('Los datos se han guardado correctamente.'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->fail(__('Error al guardar, por favor inténtelo nuevamente.'));
			}
			$this->request->data['Contac']['id'] = EncryptDecrypt::encrypt($this->request->data['Contac']['id']); 
		} else {
			$this->Session->write('identity_session', 'id'.uniqid()); 
			$this->Flash->fail(implode('<br>',$validations)); 
		}
	}

	public function view($id = null) {
		$this->Session->delete("approvalAction"); 
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Contac->exists($id)) {			
			$this->showMessageExceptions();
		}    
		$this->__storeIdContac($id, "view"); 
		// $this->Session->destroy();
		if ($this->Session->read('passwordEnter') == Configure::read('DISABLED') ) {
			$this->resetVariablesContac("view");

    	}
		$this->Contac->recursive = 0;
		$conditions = array('Contac.id' => $id);
		$contac     = $this->Contac->find('first', compact('conditions'));
		$this->__privacityContac($contac);  
		$this->Session->write('contactId',$id);
		
		if ($contac['Contac']['state'] == Configure::read('ENABLED')) {
			$this->Session->write('codFirebase', $contac['Contac']['firebase']);
			$this->Session->write('stateContac', Configure::read('ENABLED')); 
		}
		$this->__validatePasswordContac($contac['Contac']['password'], $this->Session->read('passwordEnter'), false);
		$this->__buildListView($contac); 
    
    	if($contac['Contac']["state"] == 0){
			$audioURL = $this->openAudio($id,$contac['Contac']["description"]);
			$this->set(compact('audioURL'));
			 
		}else{
			$audioURL = "";
			$this->set(compact('audioURL'));
		}
	}

	function audioPlay($id){
    	$this->layout = false;
		//$id =  EncryptDecrypt::decrypt($id);
		$conditions = array('Contac.id' => $id);
		$contac     = $this->Contac->find('first', compact('conditions'));
		if($contac['Contac']["state"] == 0){
			$audioURL = $this->openAudio($id,$contac['Contac']["description"]);
			$this->set(compact('audioURL','contac'));
			 
		}else{
			$audioURL = "";
			$this->set(compact('audioURL','contac'));
		}
    

	}

	private function __privacityContac($contac = array()){
		if($contac["Contac"]["user_id"] != AuthComponent::user("id")){
			$this->loadModel("ProjectsPrivacity");
			$conditions = array("ProjectsPrivacity.project_id" => $contac["Contac"]["project_id"], "ProjectsPrivacity.user_id" => AuthComponent::user("id"));
			$project    = $this->ProjectsPrivacity->find("first", compact("conditions"));
			if(empty($project)){ 
				$this->Flash->fail(__('No tienes permisos para realizar esta acción en la empresa: ') . $contac["Team"]["name"]);
                $this->redirect(array('action' => 'index_page','controller' => 'pages')); 
			}
		}   
	}

	private function __storeIdContac($id, $view){
		$this->Contac->unbindModel(array('hasMany' => array('ContacsFile','ContacsDocument','Commitment','CommentContac','QualificationContact')));
		$conditions  = array('Contac.' . $this->Contac->primaryKey => $id);
		$datos       = $this->Contac->find('first', compact('conditions'));
		if ($datos['Contac']['state'] == Configure::read('DISABLED')) {
			$this->Session->write('codFirebase',  Configure::read('COD_ACTA_INIT'));
			$this->Session->write('stateContac', Configure::read('DISABLED'));
		} 
		if ($datos['Contac']['state'] == Configure::read('NEW_APPROVAL')) { 
			$this->Session->write('codFirebase',  Configure::read('COD_ACTA_INIT'));
			$this->Session->write('stateContac', Configure::read('DISABLED'));
		} 
		$this->Session->write("ContacId", $id);
		$this->Session->write('actionPassword', $view);
		$this->Session->write('InfoContacEdit', $datos); 
	}

	private function __validateContacFinish($id){
		$recursive  = -1;
		$conditions = array('Contac.' . $this->Contac->primaryKey => $id);
		$contac     = $this->Contac->find('first', compact('conditions','recursive'));
		if(!empty($contac)){
			if($contac["Contac"]["state"] == Configure::read("DISABLED")){ 
				$this->Flash->fail(__('No se puede editar el acta, ya finalizó.'));
				$this->redirect(array('action' => 'index'));
			} else if ($contac["Contac"]["state"] == Configure::read("NEW_APPROVAL")){ 
				$this->Flash->fail(__('No se puede editar el acta, está en proceso de calificación.'));
				$this->redirect(array('action' => 'index'));			
			}
		}	 
	}

	private function __validatePasswordContac($password,$validate,$update) {
		if ($validate == Configure::read('DISABLED')) {
			if ($password != '')  {
				$this->Session->write('passwordContac',Configure::read('ENABLED')); 
				$this->redirect(array('action' => 'password'));
			}
		} else {
			$this->Session->write('passwordContac',Configure::read('DISABLED'));
		}
		return true;
	}

	public function password() { }

	public function enterPassword(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        } 
        $this->layout     = 'ajax'; 
        $this->autoRender = false; 
        if ($this->request->is('ajax')) {
        	$password = $this->request->data['password'];
        	$data = array();
			$data["errors"] = "";
			$data["id"]     = 0;
			$data["action"] = "";
			$contacId 	    = $this->Session->read("ContacId");
        	$contac 		= $this->Contac->findPasswordContac($password, $contacId); 
        	if ($contac == false) {
        		$data["errors"] = __("La contraseña ingresada no pertenece al acta.");
        	} else {
        		$data["id"]     = EncryptDecrypt::encrypt($contacId); 
        		if($this->Session->read("approvalAction") != null){
        			$data["id"] = $this->Session->read("approvalAction");
        		}
        		$data["action"] = $this->Session->read('actionPassword');
        		$this->Session->write('passwordEnter',Configure::read('ENABLED'));
        	}  
        	$this->outJSON($data);
		} 
	}

	private function __buildListView($contac){ 
		$users = $this->Contac->Assistant->getAssistantsContacFinished($contac["Contac"]["id"]); 
		$this->set(compact('contac','users'));
	}

	private function __findUserEdit($user_edit){
		$fields = array('User.firstname','User.lastname');
		$conditions = array('User.id' => $user_edit);
		return $this->Contac->User->find('first',compact('conditions','fields'));
	}
 

	public function add() { 
    	 
    	if(AuthComponent::user("name") == "" || AuthComponent::user("name") == " "){
        	$this->Flash->success(__('Debe completar su perfil para poder realizar un Acta.'));
		    $this->redirect(array('controller'=>'users','action' => 'edit'));
        }
		if($this->request->is('get')) {
			$this->__checkIfHaveTeam();
		}
		$this->__deleteSessionMeeting();
		$this->check_team_permission_action(array("index"), array("contacs"));
		if(empty($this->request->data))  {
			$this->resetVariablesContac();
		} 
		if ($this->request->is('post')) {  
			$this->request->data = $this->__fieldsDefault($this->request->data, $this->request->data['Contac']['copiesI']); 
       		$guardar = $this->getValidates($this->request->data,'Contac');  	
			if (!is_array($guardar)) { 
				$this->Contac->create();  
				 
				if ($this->Contac->save($this->request->data)) { 
					$this->__saveAllInfoContacAdd($this->Contac->id, $this->request->data);
	 				$this->deleteCookieInitial();
					$this->Flash->success(__('Los datos se han guardado correctamente.'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->fail(__('Error al guardar, por favor inténtelo nuevamente.'));
				}
			} else {
				$this->Session->write('identity_session', 'id'.uniqid()); 
				$this->Flash->fail(implode('<br>',$guardar)); 
			}
		}  
		$this->__buildList($this->request->data);	
	}

	private function __checkIfHaveTeam(){
		if(!$this->Contac->Team->userHaveTeam(AuthComponent::user("id"))){
			$this->Flash->fail(__('Para crear un acta primero debes crear una empresa.'));
			$this->redirect(array('controller' => 'teams', 'action' => 'add'));
		}
	}

	private function __deleteSessionMeeting(){ 
		$meeting    = $this->Session->read("MeetingContac"); 
		$urlBack    = $this->referer(); 
		if(!empty($meeting)){
			//$this->Session->write("MeetingContacId", $meeting["Meeting"]["id"]);
			if($urlBack != FULL_BASE_URL.$this->request->base."/meetings/view/".EncryptDecrypt::encrypt($meeting["Meeting"]["id"])){
				if($urlBack != FULL_BASE_URL.$this->request->base."/meetings"){
					$this->Session->write("MeetingContac", array());
				}
			}  
		}
	}

	private function __saveAllInfoContacAdd($contacId, $dataContac = array(), $options = array()){
		$this->Session->write("ContacId", $contacId);
		$this->__addIdContacImagen($contacId, $dataContac["Contac"]["team_id"], 'save');
		$this->__addIdContacDocumento($contacId, $dataContac["Contac"]["team_id"], 'save');
		$this->__saveCommitmentAdd($dataContac['Commitments'], $contacId, $dataContac["Contac"]["team_id"]); 
	    $this->Contac->Assistant->saveAsistentesContac($dataContac["Contac"], $contacId);
	    $this->Contac->logAssistantContacAdd($contacId); 
	    $this->Session->delete("ContacId");
	    if(!empty($dataContac["Contac"]["meeting_id"])) {
	    	$this->__updateMeetingWithContac($contacId, $dataContac["Contac"]["meeting_id"]);
	    }
   		$numberContac = $this->Contac->numberContac($dataContac['Contac']['project_id'], $contacId);
   		$saveData 	  = array("Contac" => array("id" => $contacId, "number" => $numberContac)); 
   		if(!empty($opts['savedAction']) && $opts['savedAction'] == "completeContact") {
   			if($opts['state']) {
   				$saveData['Contac']['state'] = $opts['state'];
   			}
   		}
   		$this->Contac->save($saveData,  array('validate' => false));
   		//si el acta necesita ser aprovada
   		if(!empty($this->request->data['Contac']['approval'])) {   			
   			$this->Contac->ApprovalContac->saveAllApprovalUsers($this->request->data["Contac"], $contacId);
   			$this->__getApprovalContac($contacId);
   		}else {
			$this->__sendNotificationContacFinished($contacId); 
   		}
	}

	private function __fieldsDefault($data, $arrayCopies = null){   
		$data['Contac']['user_id'] = AuthComponent::user('id');
		$data['Contac']['state']   = Configure::read('DISABLED'); 
		$data['Contac']['copies']  = $arrayCopies;
		if(!isset($data['Contac']['project_id'])){
			$data['Contac']['project_id'] = NULL;
		}
		if(!isset($data['Contac']['client_id'])){
			$data['Contac']['client_id']  = NULL;
		}
		return $data;
	}
  

	public function view_hystory_approved($contac_id){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax'; 
		$contac_id     = EncryptDecrypt::decrypt($contac_id);
		$conditions    = array('QualificationContact.contac_id' => $contac_id);
		$limit   	   = 10; 
		$this->Paginator->settings = compact('conditions','limit');
        $calificaciones = $this->Paginator->paginate($this->Contac->QualificationContact);
		$this->set(compact('calificaciones','contac_id'));
	}

	public function descartar_borrador(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
        $this->autoRender = false;
		$contac_id        = EncryptDecrypt::decrypt($this->request->data['contac_id']);
		$firebase 		  = EncryptDecrypt::decrypt($this->request->data['firebase']);
    	$this->Contac->deleteFirebase($firebase);
		$this->Contac->saveLogDescartarBorrador($contac_id);
    	$this->__deleteCommitment($contac_id);
		$this->__deleteAsistentes($contac_id);
		$this->__deleteApprovalContac($contac_id);
		$this->__deleteCommentContac($contac_id);
		$this->__deleteFilesContac($contac_id);
		$this->__deleteHistoryCalification($contac_id);
		$this->__deleteCookieContac($contac_id);
		$this->__deleteContac($contac_id);
		$this->Contac->Assistant->Meeting->updateAll(array('Meeting.contac_id'  => NULL), array('Meeting.contac_id' => $contac_id)); 
		$this->Flash->success(__('Se ha descartado el borrador correctamente.'));
		return true;
	}

	private function __validateStateEdit($estado,$usuario_edita){
		$estadoContac = false;
		if ($usuario_edita == Configure::read('DISABLED') || $usuario_edita == AuthComponent::user('id')) {
			$estadoContac = false;
		} else {
			if ($estado == Configure::read('CONTAC_EDITAR')) {
				$estadoContac = true;
			}
		}
		return $estadoContac;
	}

	private function __buildList($data = array()){ 
		$meetingContacSelected = array();
		if($this->request->action == "add") {
			unset($data["Commitments"]["NUEVO"]);
		} else {
			$meetingContacSelected = $this->Contac->getMeetingContac($data["Contac"]["id"]); 
			$conditions        	   = array("ApprovalContac.contac_id" => $data["Contac"]["id"]);
			$dateLimitCalified 	   = $this->Contac->ApprovalContac->field("limit_date", $conditions);
		}
		$planActual    = $this->Contac->User->PlanUser->getPlanActualUserContac();
		$teamIds       = $this->loadTeamPermission();  
		$teams         = $this->Contac->Team->getTeams($teamIds); 	 
        $clients       = $this->Contac->Client->getClientTeam(isset($data["Contac"]["team_id"]) ? $data["Contac"]["team_id"] : NULL);
		$projects      = $this->Contac->Project->findProjectsClientSelect(isset($data["Contac"]["client_id"]) ? $data["Contac"]["client_id"] : NULL);
		$collaborators = $this->Contac->Team->UserTeam->getCollaborators(isset($data["Contac"]["team_id"]) ? $data["Contac"]["team_id"] : NULL);
		$assistants    = $this->Contac->Team->UserTeam->getAssistants(isset($data["Contac"]["team_id"]) ? $data["Contac"]["team_id"] : NULL, isset($data["Contac"]["client_id"]) ? $data["Contac"]["client_id"] : NULL);
		$allUsers      = $this->Contac->Team->UserTeam->getUsersCommitments(isset($data["Contac"]["team_id"]) ? $data["Contac"]["team_id"] : NULL, isset($data["Contac"]["client_id"]) ? $data["Contac"]["client_id"] : NULL);	 
		$this->Session->write("MeetingContacSelected", $meetingContacSelected); 
		$this->set(compact('teams','allUsers','collaborators','assistants','projects','clients','data','planActual','meetingContacSelected','dateLimitCalified'));
	} 

	public function autoguardar(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        try {        	
	        set_time_limit(30000);
	        $this->layout     = 'ajax'; 
	        $this->autoRender = false;	
			$this->__initDataBorrador(); 
			$registro = '0';
			if (isset($this->request->data['Contac']['id'])) {
				$this->request->data['Contac']['id'] = EncryptDecrypt::decrypt($this->request->data['Contac']['id']); 
	        	$this->request->data = $this->__cancelApproval($this->request->data);
	        	
	        	$sessionFirebaseCode = $this->Session->read('codFirebase');
	        	if(!empty($this->request->data['Contac']['firebase_code'])) {
	        		$this->Contac->updateFirebase($this->request->data['Contac']['firebase_code'], $this->request->data['Contac']); 
	        	} else {
	        		$this->Contac->updateFirebase($sessionFirebaseCode,$this->request->data['Contac']); 
	        	}
	        } else { 
	        	$registro   = '1';
	        	$codFibase  = 'COD'.uniqid(); 
	        	$this->request->data['Contac']['firebase'] = $codFibase;
	        	$this->Contac->create(); 
	        }  
	        if(!empty($this->request->data['Contac']['id'])) {
				$conditions = array('Contac.id' => $this->request->data['Contac']['id']);
				$currentState = $this->Contac->field('state', $conditions);
				if($currentState == Configure::read('DISABLED')) {
					return __('No se puede editar el acta, ya finalizó.');
				}
	        }

	        if ($this->Contac->save($this->request->data, array('validate'=>false))) {
	        	if ($registro == '1') { 
		        	$this->Session->write("ContacId", $this->Contac->id);
	        		$this->Contac->ApprovalContac->saveAllApprovalUsers($this->request->data['Contac'], $this->Contac->id); 
	        		$this->__saveInfoContacEdit($this->request->data, $this->Contac->id, 'save');
		        	$this->Contac->saveLogInAddContac($this->Contac->id, $this->request->data);  
		        	$this->__createCookieContacTimer($this->Contac->id, $this->request->data["Contac"]["duration"]);
		        	$this->__updateMeetingWithContac($this->Contac->id, $this->request->data["Contac"]["meeting_id"]); 
		        	$this->request->data['Contac']['id'] = $this->Contac->id;
		        	$this->Contac->saveFirebase($codFibase,$this->request->data['Contac']);
	        		$this->Contac->Commitment->update_commitments_firebase($this->request->data['Contac']['commitments'], $this->Contac->id, $codFibase);		        	 
		        	$this->Flash->success(__('Los datos se han guardado correctamente.'));
		        	if(!empty($this->request->data["Contac"]["password"])) {
						$this->Session->write('passwordEnter',Configure::read('ENABLED'));
		        	}
		        	return EncryptDecrypt::encrypt($this->Contac->id);

		        } else {  
					 
		        	$this->Session->write("ContacId", $this->request->data['Contac']['id']);
	        		$this->Contac->ApprovalContac->updateListUserApproval($this->request->data['Contac']['id'], $this->request->data);
		        	$contacBeforeEdit = $this->Contac->storeInfoContacBeforeEdit($this->request->data['Contac']['id'], $this->Session->read("InfoContacEdit"));  

		        	$this->__saveCommitment($this->request->data['Contac']['commitments'], $this->request->data['Contac']['id']);
		        	$this->__saveAsistentesContac($this->request->data['Contac'], $this->request->data['Contac']['id']); 
		        	$this->Contac->saveAllLogContacAutoSave($this->request->data['Contac']['id'], $contacBeforeEdit, $this->request->data); 
		        	return __('Se ha guardado la información.');
		        }
	        } else {
	        	return __('No se ha guardado la información.');
			}
	        	 
        } catch (Exception $e) { }
	}

	public function list_assistants_view(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout  = 'ajax';  
        $contacId      = isset($this->request->data["contac_id"]) ? $this->request->data["contac_id"] : NULL;
        $users         = array();
        if(!empty($contacId)){
        	$users = $this->Contac->Assistant->getAssistantsContacFinished($contacId);  
        }
		$this->set(compact('users')); 
	} 

	public function find_commitments_contac_finalizado(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }

        $this->layout  = 'ajax'; 
        $commitments   = array();
        $commitments   = $this->Contac->getCommiments($this->Session->read('contactId'));
 		$this->set(compact("commitments"));
	}

	private function __initDataBorrador(){	  
		$this->request->data['Contac']['modified'] = date('Y-m-d H:i:s');
		$this->request->data['Contac']['timestap'] = uniqid();
        $this->request->data['Contac']['state']    = Configure::read('ENABLED');
        if (isset($this->request->data['Contac']['copiesI'])) {
        	$this->request->data['Contac']['copies'] = $this->request->data['Contac']['copiesI'];
        	unset($this->request->data['Contac']['copiesI']);
        } else {
        	$this->request->data['Contac']['copies'] = "";
        }
        unset($this->request->data['Contac']['description']);
        if (isset($this->request->data['description'])) {
        	$this->request->data['Contac']['description'] = $this->request->data['description'];
        }
    	if (isset($this->request->data['Contac']['reunion'])) {
        	unset($this->request->data['Contac']['reunion']);
        }
        if (isset($this->request->data['project_id'])) {
        	$this->request->data['Contac']['project_id'] = $this->request->data['project_id'];
        	unset($this->request->data['project_id']);
        }
        if (!isset($this->request->data['Contac']['project_id'])) {
    		$this->request->data['Contac']['project_id'] = 0;
    	}
	 	if (isset($this->request->data['client_id'])) {
        	$this->request->data['Contac']['client_id'] = $this->request->data['client_id'];
        	unset($this->request->data['client_id']);
        }
        if (!isset($this->request->data['Contac']['client_id'])) {
    		$this->request->data['Contac']['client_id'] = 0;
    	}
        if (isset($this->request->data['externos'])) {
        	$this->request->data['Contac']['externos'] = $this->request->data['externos'];
        	unset($this->request->data['externos']);
        } else {
	      	if (!isset($this->request->data['Contac']['externos'])) {
	        	$this->request->data['Contac']['externos'] = ""; 
	        }
	    }
    	if (isset($this->request->data['Commitments'])) {
        	$this->request->data['Contac']['commitments'] = $this->request->data['Commitments'];
        	unset($this->request->data['Commitments']);
        	/*
	        	if(isset($this->request->data['Contac']['commitments']['NUEVO']) && !empty($this->request->data['Contac']['commitments']['NUEVO']['id'])) {
	        		$id = $this->request->data['Contac']['commitments']['NUEVO']['id'];
	        		$this->request->data['Contac']['commitments'][$id] = $this->request->data['Contac']['commitments']['NUEVO'];
	        		unset($this->request->data['Contac']['commitments']['NUEVO']);
	        	}
        	*/
        	unset($this->request->data['Contac']['commitments']['NUEVO']);
        }      
        if (!isset($this->request->data['Contac']['id'])) {
        	$this->request->data['Contac']['user_id']  = AuthComponent::user('id');
        	$this->request->data['Contac']['end_date'] = $this->request->data['Contac']['end_date'];
        }
	}

	public function campoDescripcionContacSave() {
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
        $this->autoRender = false; 
        if ($this->request->data['description'] == '') { 
    		$this->request->data['Contac']['description'] = NULL;
    	}else {
    		$this->request->data['Contac']['description'] = $this->request->data['description'];
    	}
    	$this->request->data['Contac']['id'] = $this->Session->read('contactId');
    	$this->Contac->save($this->request->data);
        $this->Contac->updateFirebaseContac($this->Session->read('codFirebase'),'description',$this->request->data['description']);
	}
	
	public function borrador() {
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        set_time_limit(30000);
        $this->layout     = 'ajax'; 
        $this->autoRender = false;
        $registro 		  = '0';         
        $this->__initDataBorrador(); 

        if (isset($this->request->data['Contac']['id'])) {
        	$this->request->data['Contac']['id'] = EncryptDecrypt::decrypt($this->request->data['Contac']['id']);
        	$this->request->data = $this->__cancelApproval($this->request->data); 
        	$this->Contac->updateFirebase($this->Session->read('codFirebase'),$this->request->data['Contac']);
        } else {
        	$codFibase  = 'COD'.uniqid();
        	$registro   = '1'; 
        	$this->request->data['Contac']['firebase'] = $codFibase;
        	$this->Contac->create(); 
        }  
        if(!empty($this->request->data['Contac']['id'])) {
			$conditions = array('Contac.id' => $this->request->data['Contac']['id']);
			$currentState = $this->Contac->field('state', $conditions);
			if($currentState == Configure::read('DISABLED')) {
				return $this->Flash->success(__('No se puede editar el acta, ya finalizó.'));
			}
        }



        $this->Contac->save($this->request->data, array('validate'=>false));
        if ($registro == '1') { 
        	$this->Session->write("ContacId", $this->Contac->id); 
        	$this->Contac->ApprovalContac->saveAllApprovalUsers($this->request->data["Contac"], $this->Contac->id);
    		$this->__saveInfoContacEdit($this->request->data, $this->Contac->id, 'save');
        	$this->Contac->saveLogInAddContac($this->Contac->id, $this->request->data);
        	$this->__createCookieContacTimer($this->Contac->id, $this->request->data["Contac"]["duration"]);
        	$this->__updateMeetingWithContac($this->Contac->id, $this->request->data["Contac"]["meeting_id"]);  
        	$this->request->data['Contac']['id'] = $this->Contac->id; 
        	$this->Contac->saveFirebase($codFibase,$this->request->data['Contac']);
    		$this->Contac->Commitment->update_commitments_firebase($this->request->data['Contac']['commitments'], $this->Contac->id, $codFibase); 
        	$this->Flash->success(__('Los datos se han guardado correctamente.'));
        	return $this->Contac->id;
        } else { 
        	$this->Session->write("ContacId", $this->request->data['Contac']['id']); 
        	$this->Contac->ApprovalContac->updateListUserApproval($this->request->data['Contac']['id'], $this->request->data);
    		$contacBeforeEdit = $this->Contac->storeInfoContacBeforeEdit($this->request->data['Contac']['id'], $this->Session->read("InfoContacEdit"));  
        	$this->__saveCommitment($this->request->data['Contac']['commitments'],$this->request->data['Contac']['id']);
        	$this->__saveInfoContacEdit($this->request->data, $this->request->data['Contac']['id']);
        	$this->Contac->saveAllLogContac($this->request->data['Contac']['id'], $contacBeforeEdit, $this->request->data); 
        	$this->Flash->success(__('Se ha guardado la información.')); 
        } 
        return true;
	}

	private function __cancelApproval($data = array()){
		if(!isset($data["Contac"]["approval"])){ 
			$data["Contac"]["approvalusers"] = NULL;
			$this->Contac->ApprovalContac->deleteAll(array('ApprovalContac.contac_id' => $data["Contac"]["id"]),false);
			$data["Contac"]["approval"] = NULL;
			return $data;
		} else {
			return $data;
		}
	}

	private function __saveInfoContacEdit($data = array(), $contacId, $save = null){
    	$this->__addIdContacImagen($contacId, $data['Contac']["team_id"], $save);
		$this->__addIdContacDocumento($contacId, $data['Contac']["team_id"], $save);
    	$this->__saveAsistentesContac($data['Contac'], $contacId); 
	}

	public function generatePassword(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
        $this->autoRender = false;
		$password = $this->Contac->generatePassword();
		return $password;
	}

	private function __deleteAsistentes($identificador){		 
		$this->Contac->Assistant->deleteAll(
			array('Assistant.contac_id'=>$identificador),false
		);
	}

	private function __deleteCommitment($identificador){ 
		$this->Contac->Commitment->deleteAll(
			array('Commitment.contac_id'=>$identificador),false
        );
	}

	private function __deleteContac($identificador){
		$conditions = array("Contac.id" => $identificador);
		$whiteBrand = $this->Contac->field("file_white_brand", $conditions);
		if(!empty($whiteBrand)){ 
			$directorio    = WWW_ROOT."document/WhiteBrand/";
			unlink($directorio.$whiteBrand);
		}
		$this->Contac->deleteAll(
			array('Contac.id'=>$identificador),false
		);
		$this->loadModel("Log");
		$this->Log->deleteAll(array('Log.contac_id'=> $identificador), false);
	}

	private function __deleteApprovalContac($identificador){
		$this->Contac->ApprovalContac->deleteAll(
			array('ApprovalContac.contac_id'=>$identificador),false
		);
	} 

	private function __deleteCommentContac($identificador){
		$this->Contac->CommentContac->deleteAll(
			array('CommentContac.contac_id'=>$identificador),false
		);
	} 

	private function __deleteFilesContac($identificador){		
		$conditions      = array("ContacsFile.contac_id" => $identificador);
		$imagesContacs   = $this->Contac->ContacsFile->find("all", compact("conditions"));
		$conditions      = array("ContacsDocument.contac_id" => $identificador);
		$documentContacs = $this->Contac->ContacsDocument->find("all", compact("conditions"));
		if(!empty($imagesContacs)){
			foreach ($imagesContacs as $imagesContac) {
				$nombreArchivo = $imagesContac['ContacsFile']['img'];
				$directorio    = WWW_ROOT."files/Contac/";
				unlink($directorio.$nombreArchivo); 
			} 
		}
		if(!empty($documentContacs)){
			foreach ($documentContacs as $documentContac) {
				$nombreArchivo = $documentContac['ContacsDocument']['document'];
				$directorio    = WWW_ROOT."document/Contac/";
				unlink($directorio.$nombreArchivo); 
			} 
		} 
		$this->Contac->ContacsFile->deleteAll(array('ContacsFile.contac_id'     => $identificador),false);
		$this->Contac->ContacsDocument->deleteAll(array('ContacsDocument.contac_id' => $identificador),false); 
	}

	private function __deleteHistoryCalification($identificador){
		$this->Contac->QualificationContact->deleteAll(
			array('QualificationContact.contac_id'=>$identificador),false
		);
	}

	private function __saveAsistentesContac($datos, $identificador){
		$this->Contac->update_assistants_contac($datos, $identificador); 
	}

	private function __saveCommitment($commitments, $identificador){ 
		$conditions = array("Contac.id" => $identificador);
		$teamId 	= $this->Contac->field("team_id", $conditions);
		$datos = array();
		$i     = 1;
		if(!empty($commitments)){  
			$commitmentsActual = $this->Contac->Commitment->commitmentsActual($identificador);
			foreach ($commitments as $commitment) { 
				if(!empty($commitment['asistente']) && !empty($commitment['fecha']) && !empty($commitment['description'])){
					$datos[$i]['Commitment']['contac_id']     = $identificador;
					$datos[$i]['Commitment']['team_id']       = $teamId;
					$datos[$i]['Commitment']['description']   = $commitment['description'];
					$datos[$i]['Commitment']['user_id']       = $commitment['asistente'];
					$datos[$i]['Commitment']['delivery_date'] = $commitment['fecha'];
                	$datos[$i]['Commitment']['state'] 		  = Configure::read('ENABLED');
					if(isset($commitment['id'])){
						$datos[$i]['Commitment']['id'] = $commitment['id']; 
					}
					$i++; 
				}
			} 
			if(!empty($datos)) {
				$savedCommitments = $this->Contac->Commitment->saveAll($datos); 
				$this->Contac->Commitment->compareCommitmentsNewLog($identificador, $commitmentsActual); 
			}
		} 
	} 

	private function __saveCommitmentAdd($commitments, $contacId, $teamId){
		$datos = array();
		if(empty($commitments['NUEVO']['fecha']) && empty($commitments['NUEVO']['description'])) {
			unset($commitments["NUEVO"]);
		}
		$i = 1;		 
		foreach ($commitments as $commitment) { 
			if(!empty($commitment['asistente']) && !empty($commitment['fecha']) && !empty($commitment['description'])){
				$datos[$i]['Commitment']['contac_id']     = $contacId;
				$datos[$i]['Commitment']['user_id']       = $commitment['asistente'];
				$datos[$i]['Commitment']['description']   = $commitment['description'];
				$datos[$i]['Commitment']['delivery_date'] = $commitment['fecha'];
				$datos[$i]['Commitment']['team_id']       = $teamId;
            	$datos[$i]['Commitment']['state'] 		  = Configure::read('ENABLED');
				$i++;
			}
		}
		if(!empty($datos)){
			$this->Contac->Commitment->deleteAll(array('Commitment.contac_id' => $contacId), false);
			$this->Contac->Commitment->saveAll($datos);
			$this->Contac->logCommitmentsContac($contacId);
			$this->Contac->Commitment->commitmentsCreatedsFirebaseNotification($contacId);  
		}
	}  

	private function __addIdContacImagen($id, $teamId = null, $save = null){  
		$sessionFileId = $this->Session->read('identity_session'); 
		$this->Contac->ContacsFile->updateAll(
            array('ContacsFile.contac_id' => $id),
            array('ContacsFile.identity_session' => $sessionFileId)
    	);
    	if(!is_null($save)){
	    	$this->loadModel("Log");
		 	$this->Log->updateAll(
	            array('Log.contac_id' => $id, 'Log.team_id' => $teamId),
	            array('Log.action' => "saveImage", 'Log.contac_id' => NULL, 'Log.team_id' => NULL)
	    	);  
    	}
	}

	private function __addIdContacDocumento($id, $teamId = null, $save = null){	 
		$sessionFileId = $this->Session->read('identity_session'); 
		$this->Contac->ContacsDocument->updateAll(
            array('ContacsDocument.contac_id' => $id),
            array('ContacsDocument.identity_session' => $sessionFileId)
        );
        if(!is_null($save)){ 
	        $this->loadModel("Log");
	        $this->Log->updateAll(
	            array('Log.contac_id' => $id, 'Log.team_id' => $teamId),
	            array('Log.action' => "saveDocument", 'Log.contac_id' => NULL, 'Log.team_id' => NULL)
	    	);
	    }	 
	} 

	public function deleteImagenContac(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax'; 
		$this->autoRender = false;
		$this->request->data['valores']['timestap'] = uniqid();
		$conditions     = array("ContacsFile.id" => $this->request->data['id']);
		$imgDelete      = $this->Contac->ContacsFile->find("first", $conditions);
		$nombreArchivo  = $this->request->data['name'];
		$full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		$team           = $this->Contac->getNameTeam($imgDelete["Contac"]["id"]);
		$description    = sprintf(__("El usuario %s ha eliminado una imagen %s de un acta asociada a la empresa %s."), $full_name, $nombreArchivo, $team);
		$descriptionEng = sprintf(__("The user %s has removed an image %s from minute associate with the company %s."), $full_name, $nombreArchivo, $team);
		$this->buildInfoLog($this->request->data['id'], $description, $descriptionEng, __('Sin modificaciones'), __('Sin modificaciones'), __("deleteImagenContac"), $imgDelete["Contac"]["id"], $imgDelete["Contac"]["team_id"]);
		$directorio    = WWW_ROOT."files/Contac/";
		unlink($directorio.$nombreArchivo);
		$this->Contac->updateFirebase($this->Session->read('codFirebase'),$this->request->data['valores']);
		$this->Contac->ContacsFile->delete($this->request->data['id']);
		return true;
	}

	public function deleteDocumetoContac(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax'; 
		$this->autoRender = false;
		$this->request->data['valores']['timestap'] = uniqid();
		$nombreArchivo  = $this->request->data['name'];
		$conditions     = array("ContacsDocument.id" => $this->request->data['id']);
		$documentDelete = $this->Contac->ContacsDocument->find("first", $conditions);
		$full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		$team           = $this->Contac->getNameTeam($documentDelete["Contac"]["id"]);
		$description    = sprintf(__("El usuario %s ha eliminado un documento %s de un acta asociada a la empresa %s."), $full_name, $nombreArchivo, $team);
		$descriptionEng = sprintf(__("The user %s has removed a document %s from minute associate with the company %s."), $full_name, $nombreArchivo, $team);
		$this->buildInfoLog($this->request->data['id'], $description, $descriptionEng, __('Sin modificaciones'), __('Sin modificaciones'), __("deleteDocumetoContac"), $documentDelete["Contac"]["id"], $documentDelete["Contac"]["team_id"]);
		$directorio    = WWW_ROOT."document/Contac/";
		unlink($directorio.$nombreArchivo);
		$this->Contac->updateFirebase($this->Session->read('codFirebase'),$this->request->data['valores']);
		$this->Contac->ContacsDocument->delete($this->request->data['id']);
		return true;
	}

	public function upload_images_delete(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$fields 	  = array('ContacsFile.*','Contac.state');
		$id 		  = $this->Session->read('contactId');
		$conditions   = array('Contac.id' => $id);
		$imagenes     = $this->Contac->ContacsFile->find('all',compact('conditions','fields')); 
		$this->set(compact('imagenes'));
	}

	public function upload_documents_delete(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$fields 	  = array('ContacsDocument.*','Contac.state');
		$id 		  = $this->Session->read('contactId');
		$conditions   = array('Contac.id' => $id);
		$documentos   = $this->Contac->ContacsDocument->find('all',compact('conditions','fields')); 
		$this->set(compact('documentos'));
	}

	public function upload_images(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$fields       = array('ContacsFile.*','Contac.state');
		$id 		  = $this->Session->read('contactId');
		$conditions   = array('Contac.id' => $id);
		$imagenes     = $this->Contac->ContacsFile->find('all',compact('conditions','fields'));
		if (!isset($imagenes[0])) {
			$imagenes = '';
		}
		$this->set(compact('imagenes'));
	}

	public function upload_documents(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
		$fields 	  = array('ContacsDocument.*','Contac.state');
		$id 		  = $this->Session->read('contactId');
		$conditions   = array('Contac.id' => $id);
		$documentos   = $this->Contac->ContacsDocument->find('all',compact('conditions','fields'));
		if (!isset($documentos[0])) {
			$documentos = '';
		}
		$this->set(compact('documentos'));
	}

	public function find_upload_client(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
        $contac_id    = $this->request->data['contac_id'];
        $fields       = array('Contac.client_id','Client.id','Client.name');
		$conditions   = array('Contac.id' => $contac_id);
		$contac       = $this->Contac->find('first', compact('conditions','fields'));
		$this->set(compact('contac'));
	}

	public function find_upload_project(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
        $contac_id    = $this->request->data['contac_id'];
        $fields 	  = array('Contac.project_id','Project.id','Project.name');
		$conditions   = array('Contac.id' => $contac_id);
		$contac 	  = $this->Contac->find('first', compact('conditions','fields'));
		$this->set(compact('contac'));
	} 

	public function find_asistentes(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
        $client_id    = $this->request->data['cliente'];
        $conditions   = array('Employee.client_id' => $client_id,'Employee.state' => Configure::read('ENABLED'));
		$externos     = $this->Contac->Client->Employee->find('list',compact('conditions'));
		$userAll      = array();
		foreach ($externos as $id => $user) {
			$userAll[] = array("value"=>'customer_'.$id, 'name'=>$user);
		}
		$this->set(compact('userAll'));
	}

	public function find_asistentes_commitments(){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout = 'ajax'; 
        $client_id    = $this->request->data['cliente'];
        $conditions   = array('Employee.client_id' => $client_id,'Employee.state' => Configure::read('ENABLED'));
		$externos     = $this->Contac->Client->Employee->find('list',compact('conditions'));
		$userAll      = array();
		$fields       = array('User.id','User.name');
		$conditions = array('User.id' => AuthComponent::user('id'),'User.state' => Configure::read('ENABLED'));
		$users = $this->Contac->User->find('list', compact('conditions','fields'));
		$userAll[] = array("value"=>"", 'name'=>__('Seleccionar...'));
		foreach ($users as $id => $user) {
			$userAll[] = array("value"=>'official_'.$id, 'name'=>$user);
		}
		foreach ($externos as $id => $user) {
			$userAll[] = array("value"=>'customer_'.$id, 'name'=>$user);
		}
		$this->set(compact('userAll'));
	}

	public function saveImage(){
		set_time_limit(300000);
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
        $this->autoRender = false; 
		$this->request->data['ContacsFile']['img']       = $this->request->params['form']['file'];
		$this->request->data['ContacsFile']['identity_session'] = $this->Session->read('identity_session');
		$this->request->data['ContacsFile']['contac_id'] = $this->Session->read("ContacId"); 
		$this->request->data['ContacsFile']['created']   = date("Y-m-d H:i:s");
		$this->Contac->ContacsFile->create();
		$this->Contac->ContacsFile->save($this->request->data);
		$conditions     = array("ContacsFile.id" => $this->Contac->ContacsFile->id);
		$image          = $this->Contac->ContacsFile->find("first", compact("conditions"));
		$path           =  WWW_ROOT.'/files/Contac/'. $image["ContacsFile"]["img"]; 
		$full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		$description    = sprintf(__("El usuario %s ha subido una imagen %s."), $full_name, $this->request->params['form']['file']["name"]);
		$descriptionEng = sprintf(__("The user %s has uploaded an image %s."), $full_name, $this->request->params['form']['file']["name"]);
   		$this->buildInfoLog($image["ContacsFile"]["id"], $description, $descriptionEng, __('Sin modificaciones'), __('Sin modificaciones'), __("saveImage"), $image["Contac"]["id"], $image["Contac"]["team_id"]); 
   		$this->outJSON($this->Contac->ContacsFile->id);
	}

	public function saveDocument(){
		set_time_limit(300000);
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout     = 'ajax'; 
        $this->autoRender = false;
		$this->request->data['ContacsDocument']['document']  = $this->request->params['form']['file'];
		$this->request->data['ContacsDocument']['identity_session'] = $this->Session->read('identity_session');
		$this->request->data['ContacsDocument']['contac_id'] = $this->Session->read('ContacId');
		$this->request->data['ContacsDocument']['created']       = date("Y-m-d H:i:s");  
		$this->Contac->ContacsDocument->create();
		$this->Contac->ContacsDocument->save($this->request->data);
		$conditions  = array("ContacsDocument.id" => $this->Contac->ContacsDocument->id);
		$document    = $this->Contac->ContacsDocument->find("first", compact("conditions"));
		$path        =  WWW_ROOT.'/document/Contac/'. $document["ContacsDocument"]["document"]; 
		$full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		$description    = sprintf(__("El usuario %s ha subido un document %s."), $full_name, $this->request->params['form']['file']["name"]);
		$descriptionEng = sprintf(__("The user %s has uploaded a document %s."), $full_name, $this->request->params['form']['file']["name"]);
   		$this->buildInfoLog($document["ContacsDocument"]["id"], $description, $descriptionEng, __('Sin modificaciones'), __('Sin modificaciones'), __("saveDocument"), $document["Contac"]["id"], $document["Contac"]["team_id"]);
   		$this->outJSON($this->Contac->ContacsDocument->id); 
	}
 
	public function my_contacs($userId = null){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $userId 		   = EncryptDecrypt::decrypt($userId);
        $this->layout      = 'ajax';  
		$conditions        = array("Assistant.user_id" => $userId);
		$fields            = array("Assistant.contac_id");
		$myAssistsContacId = $this->Contac->Assistant->find("list", compact("conditions","fields"));
		$this->Contac->unbindModel(array('hasMany' => array('Assistant','Commitment','ContacsDocument','ContacsFile')));
		$conditions   = array("Contac.id" => $myAssistsContacId, "Contac.state" => Configure::read("DISABLED")); 
		$order        = array('Contac.id DESC');
		$limit        = 10; 
		$this->Paginator->settings = compact('conditions','limit','order');
		$contacs  = $this->Paginator->paginate(); 
		$this->set(compact('contacs','userId')); 
	}

	public function statistical_numbers($userId){
	    $userId     	= EncryptDecrypt::decrypt($userId); 
		$conditions     = array("Client.user_id" => $userId);
		$totalClients   = $this->Contac->Client->find("count", compact("conditions"));
		$conditions     = array("Client.user_id" => $userId);
		$fields         = array("Client.id");
		$clientIds      = $this->Contac->Client->find("list", compact("conditions","fields"));
		$conditions     = array("Project.client_id" => $clientIds);
		$totalProjects  = $this->Contac->Project->find("count", compact("conditions"));
		$conditions     = array("Contac.user_id" => $userId);
		$totalContacs   = $this->Contac->find("count", compact("conditions"));
		$conditions     = array("User.role_id" => $clientIds);
		$this->loadModel("UserTeam");
		$conditions = array("UserTeam.own_user_id" => $userId, "UserTeam.type_user" => 2);
		$totalEmployees = $this->UserTeam->find("count", compact("conditions")); 
		$this->set(compact('totalClients','totalProjects','totalContacs','totalEmployees')); 
	}

	public function contacts_employee($employeeId = null){
		if (!$this->request->is('ajax')) {
            $this->showMessageExceptions();
        }
        $this->layout      = 'ajax';
        $employeeId 	   = EncryptDecrypt::decrypt($employeeId);  
		$conditions        = array("Assistant.employee_id" => $employeeId);
		$fields            = array("Assistant.contac_id");
		$myAssistsContacId = $this->Contac->Assistant->find("list", compact("conditions","fields"));
		$this->Contac->unbindModel(array('hasMany' => array('Assistant','Commitment','ContacsDocument','ContacsFile')));
		$conditions   = array("Contac.id" => $myAssistsContacId, "Contac.state" => Configure::read("DISABLED")); 
		$order        = array('Contac.id DESC');
		$limit        = 10; 
		$this->Paginator->settings = compact('conditions','limit','order');
		$contacs  = $this->Paginator->paginate(); 
		$this->set(compact('contacs','employeeId')); 
	}

	public function projects_contacs($projectId = null){
		$this->Contac->unbindModel(array('hasMany' => array('Assistant','Commitment','ContacsDocument','ContacsFile')));
	 	$projectId 	  = EncryptDecrypt::decrypt($projectId); 
	 	$this->loadModel("ProjectsPrivacity");
		$project_id   = $this->ProjectsPrivacity->getPrivacityProjectContacs($projectId);  
		$conditions   = array("Contac.project_id" => $project_id);
		$order        = array('Contac.id DESC');
		$limit        = 10; 
		$this->Paginator->settings = compact('conditions','limit','order');
		$contacs  = $this->Paginator->paginate(); 
		$this->set(compact('contacs'));
	}

	public function get_info_calendar_meeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax'; 
		$meeting      = $this->Session->read("MeetingContac");
		$this->set(compact('meeting')); 
	}

	public function get_info_meeting_contac(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax'; 
		$meeting      = $this->Session->read("MeetingContacSelected");
		$this->set(compact('meeting')); 
	}
 

	//VISTA PARA APROBAR EL ACTA POR PARTE DE LOS FUNCIONARIOS Y EMPRESA
	public function approval_contacs(){ 
		$this->loadModel("ProjectsPrivacity"); 
		$projectInfo  = $this->ProjectsPrivacity->getPrivacityContacs(); 
		$projects     = $projectInfo["projects"];  
		$teams        = $this->loadTeams();
		$conditions   = array("ApprovalContac.user_id" => AuthComponent::user("id"),"ApprovalContac.state" => Configure::read("DISABLED"),"NOT" => array("ApprovalContac.state" => Configure::read("ENABLED")));
		$conditions[] = array( 
			"OR" => array(
				array("Contac.in_approval" => Configure::read("ENABLED")), 
				array("Contac.in_approval" => Configure::read("NEW_APPROVAL"))
			),
			$this->Contac->ApprovalContac->buildConditions($this->request->query)
		); 
		$limit   = 10; 
		$this->Paginator->settings = compact('conditions','limit');
        $contacs = $this->Paginator->paginate($this->Contac->ApprovalContac); 
        $this->set(compact('contacs','teams','projects'));			
	}
  
	//DETALLE DE DONDE SE MOSTRARÁ EL ACTA PARA CALIFICAR
	public function detail_contac_to_approved($id = null){
	 	$this->Session->delete("approvalAction");
		$id = EncryptDecrypt::decrypt($id); 
		if (!$this->Contac->ApprovalContac->exists($id)) {
			$this->showMessageExceptions();
		}
		$conditions = array("ApprovalContac.id" => $id, "ApprovalContac.state" => Configure::read("DISABLED"), "ApprovalContac.user_id" => AuthComponent::user("id"));
		$contacId   = $this->Contac->ApprovalContac->find("first", compact("conditions")); 
		if(!empty($contacId)){
			$this->Session->write("ContacId", $contacId["ApprovalContac"]["contac_id"]);  
			$this->Contac->unbindModel(array('hasMany' => array('Commitment','Assistant','CommentContac','ApprovalContac')));
			$conditions     = array("Contac.id" => $contacId["ApprovalContac"]["contac_id"]);
			$contac         = $this->Contac->find("first", compact("conditions"));  
			$this->Session->write("actionPassword", "detail_contac_to_approved");
			$this->Session->write("approvalAction", EncryptDecrypt::encrypt($id));
			$this->__validatePasswordContac($contac['Contac']['password'],$this->Session->read('passwordEnter'),true);
			$allUsers 	  = $this->Contac->Assistant->getAssistantsContacFinished($contac["Contac"]["id"]);

		    $externos     = $allUsers["assistants"];
		    $users        = $allUsers["collaborators"]; 
		    $commitments  = $this->Contac->getCommiments($contac["Contac"]["id"]);
 			$this->set(compact('contac','externos','users','commitments','id'));
		} else {
			$this->Flash->fail(__('Ya calificaste esta acta.'));
			$this->redirect(array('action' => 'index'));
		}
	}

	public function validateContactHavePassword(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$recursive 	      = -1;
		$contacId 	      = isset($this->request->data["contacId"]) ?  EncryptDecrypt::decrypt($this->request->data["contacId"]) : null;
		$conditions       = array("Contac.id" => $contacId);
		$contac           = $this->Contac->find("first", compact("conditions","recursive"));
		$response  = array();
		if ($contac["Contac"]["password"] != "") {
			$response = array("state" => true);
		} else {
			$response = array("state" => false);
		}
		$this->outJSON($response);
	} 

	public function rateContac(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$recursive 	      = -1;
		$approvalId	      = isset($this->request->data["approvalId"]) ?  EncryptDecrypt::decrypt($this->request->data["approvalId"]) : null;
		$password 	      = isset($this->request->data["password"])   ?  $this->request->data["password"]   : null;
		$contacId         = isset($this->request->data["contacId"])   ?  EncryptDecrypt::decrypt($this->request->data["contacId"])   : null;
		$conditions       = array("Contac.id" => $contacId);
		$contac           = $this->Contac->find("first", compact("conditions","recursive"));
		if(isset($password)){
			if(trim($password == "")){
				$response = array("state" => false, "message" => __("La contraseña es requerida."));
			} else if ($password != $contac["Contac"]["password"]) {
				$response = array("state" => false, "message" => __("Contraseña incorrecta."));				
			} else {
				$response = $this->__rateContac($approvalId, $contacId, $this->request->data["approved"]);
			}
		} else {
			$response = $this->__rateContac($approvalId, $contacId, $this->request->data["approved"]);
		}
		$this->outJSON($response);		
	}

	public function __saveRecordQualificationContac($options = array()){
		$datos['QualificationContact'] = array(
				'qualification'		=> $options['qualification'],
				'user_id'			=> $options['user_id'], 
				'commentary'		=> $options['commentary'],
				'contac_id'			=> $options['contac_id']
			);
		$this->Contac->QualificationContact->save($datos);
	}

	//APROBAR ACTA
	private function __rateContac($approvalId, $contacId, $approved){
		$recursive  = -1;
		$conditions = array("ApprovalContac.id" => $approvalId);
		$contacToApproved = $this->Contac->ApprovalContac->find("first", compact("conditions","recursive")); 
	 	if($approved == true){
			$contacToApproved["ApprovalContac"]["calification"] = 1;
			$contacToApproved["ApprovalContac"]["state"] = Configure::read("ENABLED");
			$this->Contac->ApprovalContac->saveLogCalification($approvalId);
		}
		$options['commentary'] 		= null;
		$options['contac_id']	 	= $contacId;
		$options['qualification'] 	= 1;
		$options['user_id']     	= AuthComponent::user("id"); 
		$this->__saveRecordQualificationContac($options);
		$this->Contac->ApprovalContac->save($contacToApproved);
		$responseCalification = $this->Contac->ApprovalContac->calculateAverageUsers($contacId); 
		if($responseCalification["approved"] == true){
		    $this->Contac->updateContac($contacId, Configure::read("ENABLED"));
			$this->Contac->ApprovalContac->saveLogLastCalificator($approvalId); 
			$this->Contac->Commitment->commitmentsCreatedsFirebaseNotification($contacId); 
			$this->__sendNotificationContacFinished($contacId, $approvalId);
		} 
		$response = array("state" => true, "message" => __("Acta calificada correctamente."));
		return $response;
	}

	//RECHAZAR ACTA
    public function refuse_contac(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$approvalId	      = isset($this->request->data["approvalId"]) ?  EncryptDecrypt::decrypt($this->request->data["approvalId"]) : null;
		$password 	      = isset($this->request->data["password"])   ?  $this->request->data["password"]   : null;
		$contacId         = isset($this->request->data["contacId"])   ?  EncryptDecrypt::decrypt($this->request->data["contacId"])   : null;
		$comment     	  = isset($this->request->data["comment"])    ?  $this->request->data["comment"]  : null;		
		$response  		  = $this->Contac->validateInfo($password, $comment, $contacId); 
		if($response["state"] == true){ 
			$response = $this->Contac->saveCalificationContac($comment, $approvalId, $contacId);
			$options['commentary'] 		= $comment;
			$options['contac_id']	 	= $contacId;
			$options['qualification'] 	= 0; 
			$options['user_id']     	= AuthComponent::user("id"); 
			$this->__saveRecordQualificationContac($options);
			$this->Contac->ApprovalContac->calculateAverageUsers($contacId);
			$responseFirebaseSave = $this->Contac->saveFirebaseContacRefuse($contacId); 
			$this->Contac->ApprovalContac->saveLogRefuseContac($approvalId);
			$this->__notificationContacUserCreator($contacId, $responseFirebaseSave); 
		}
		$this->outJSON($response);		
    } 

	//SOLICITAR NUAVEMENTE LA APROBACIÓN DEL ACTA
	public function new_approval(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$recursive 	      = -1;
		$contacId 	      = isset($this->request->data["contacId"]) ? EncryptDecrypt::decrypt($this->request->data["contacId"]) : null;
		$password         = isset($this->request->data["password"]) ? $this->request->data["password"] : null; 
		$conditions       = array("Contac.id" => $contacId);
		$contac           = $this->Contac->find("first", compact("conditions","recursive")); 
		if(isset($password)){ 
			if(trim($password == "")){
				$response = array("state" => false, "message" => __("La contraseña es requerida."));
			} else if ($password != $contac["Contac"]["password"]) {
				$response = array("state" => false, "message" => __("Contraseña incorrecta."));				
			} else {
				$response = $this->__saveNewApproval($contacId); 
			}
		} else { 
			$response = $this->__saveNewApproval($contacId); 
		}
		$this->outJSON($response);
	}

	private function __saveNewApproval($contacId){ 
		$response 	     = $this->Contac->ApprovalContac->saveNewApproval($contacId);
		$conditions      = array("ApprovalContac.contac_id" => $contacId, "ApprovalContac.state" => Configure::read("DISABLED")); 
		$contacPending   = $this->Contac->ApprovalContac->find("all", compact("conditions")); 
		$full_name       = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		$team            = $this->Contac->getNameTeam($contacId);
		$description     = sprintf(__("El usuario %s solicitó nuevamente la calificación del acta asociada a la empresa %s."), $full_name, $team);
		$descriptionEng  = sprintf(__("The user %s You’ve requested to check again the minute associate with the company %s."), $full_name, $team);
		$this->buildInfoLog($contacId, $description, $descriptionEng, NULL, NULL, NULL, $contacId, $contacPending["0"]["Contac"]["team_id"]);
		$this->Contac->ApprovalContac->notificationFirebaseContacPendingApproved($contacId);
		$this->__sendNotificationToRateContac($contacId, $contacPending); 
		return $response;
	}

	//VERIFICAR SI EL ACTA REQUIERE APROBACIÓN O NO
	public function save_contac(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$recursive 	      = -1;
		$contacId 	      = isset($this->request->data["contacId"]) ? EncryptDecrypt::decrypt($this->request->data["contacId"]) : null;
		$password         = isset($this->request->data["password"]) ? $this->request->data["password"] : null; 
		$conditions       = array("Contac.id" => $contacId);
		$contac           = $this->Contac->find("first", compact("conditions","recursive"));	
		if(isset($password)){ 
			if(trim($password == "")){
				$response = array("state" => false, "message" => __("La contraseña es requerida."), "required" => true);
			} else if ($password != $contac["Contac"]["password"]) {
				$response = array("state" => false, "message" => __("Contraseña incorrecta."), "required" => true);				
			} else {
				$response = $this->__getApprovalContac($contacId);
			}
		} else { 
			$response = $this->__getApprovalContac($contacId);
		}
		$this->outJSON($response);
	}

	//SOLICITUD DE APROBACION DE ACTA POR PRIMERA VEZ
	private function __getApprovalContac($contacId){
		$conditions    = array("ApprovalContac.contac_id" => $contacId, "ApprovalContac.state" => Configure::read("DISABLED")); 
		$contacPending = $this->Contac->ApprovalContac->find("all", compact("conditions"));
		if(!empty($contacPending)){
			$response = $this->Contac->validateInfoContac($contacId);
			if($response["state"] == true) {
				$full_name       = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
				$team            = $this->Contac->getNameTeam($contacId);
				$description     = sprintf(__("El usuario %s solicitó la calificación del acta asociada a la empresa %s."), $full_name, $team);
				$descriptionEng  = sprintf(__("The user %s You’ve requested to check the minute associate with the company %s."), $full_name, $team);
				$this->buildInfoLog($contacId, $description, $descriptionEng, NULL, NULL, NULL, $contacId, $contacPending["0"]["Contac"]["team_id"]); 
				$this->Contac->updateContac($contacId, Configure::read("APPROVAL"));
				$this->Contac->ApprovalContac->notificationFirebaseContacPendingApproved($contacId);
				$this->__sendNotificationToRateContac($contacId, $contacPending);
				$response = array("state" => true, "message" => __("El acta ha quedado en estado por calificar. Se ha enviado la notificación a los usuarios para que califiquen el acta."));
			}
		} else {
			$response = $this->Contac->validateInfoContac($contacId); 
			if($response["state"] == true) { 		
				$this->Contac->updateContac($contacId, Configure::read("ENABLED"));
				$this->Contac->Commitment->commitmentsCreatedsFirebaseNotification($contacId); 
				$this->__sendNotificationContacFinished($contacId); 
				$response = array("state" => true, "message" => __("Acta finalizada correctamente."));
			} 		
		} 
		return $response; 
	}

	private function __sendNotificationToRateContac($contacId, $contacPending = array()){
		$userInfo = array();
		$listPendings = Set::combine($contacPending, '{n}.User.email', '{n}');
		foreach ($listPendings as $key => $approval) {  
			$userInfo[] = array(
				"id"          => $approval["ApprovalContac"]["id"],
				"user_id"     => $approval["User"]["id"], 
				"contac_id"   => $approval["Contac"]["id"],
				"email"       => $approval["User"]["email"]
			); 
		}
		$this->__notificationRateContac($userInfo);
	} 

	//NOTIFICACIÓN PARA CALIFICAR EL ACTA LUEGO DE ENVIAR LA SOLICITUD, RECIBIENDO LA INFORMACIÓN DE LOS USUARIOS
	private function __notificationRateContac($userInfo = array()){
		foreach ($userInfo as $user) {
			$options = array(
				'to'       => $user["email"],
				'template' => 'contacs_pending_approved',
				'subject'  => __('Tienes un acta pendiente por aprobar').' - '.Configure::read('Application.name'),
				'vars'     =>  array('id' => $user["id"],'contacId' => $user["contac_id"], 'userId' => isset($user["user_id"]) ? $user["user_id"] : NULL),
			);
			$this->sendMail($options); 			 
		} 
	}

	//FUNCION PARA LAS COMPAÑIAS QUE UTILICEN EL ENVIO DE CONTACT POR URL
	private function __send_contact_api($urlAPI = null, $dataInfo = array()){
		$HttpSocket = new HttpSocket();
		//ORGANIZACION DE URL PARA LOS ARCHIVOS
		//http://127.0.0.1/reecapmeeting/document/Contac
		$contactFiles = array();
		foreach ($dataInfo["contac"]["ContacsDocument"] as $key => $value) {
			$contactFiles[] = $this->webroot."document".DS."Contac".DS.$value["document"];
			
		}
		$contactImages = array();
		foreach ($dataInfo["contac"]["ContacsFile"] as $key => $value) {
			$contactImages[] = $this->webroot."files".DS."Contac".DS.$value["img"];
			
		}
		 
		$data = array(
			'contact' => array(
							"number" => $dataInfo["contac"]["Contac"]["number"],
							"description" => $dataInfo["contac"]["Contac"]["description"],
							"start_date" => $dataInfo["contac"]["Contac"]["start_date"],
							"end_date" => $dataInfo["contac"]["Contac"]["end_date"],
							"duration" => $dataInfo["contac"]["Contac"]["duration"],
							"email_copy" => $dataInfo["contac"]["Contac"]["copies"],
							"client" => $dataInfo["contac"]["Client"]["name"],
							"project" => $dataInfo["contac"]["Project"]["name"],
							"team" => $dataInfo["contac"]["Team"]["name"],
								),
			 'files' => $contactFiles,	
			 'images' => $contactImages,
			 'commitments' => $dataInfo["commitments"]
			);
		try {
            $results = $HttpSocket->post($urlAPI, $data);
       
          
        } catch (Exception $e) {
            
        }
		
		
		 


		 
	}
private function updateCommitmentTaskee($dataInfo = array()){

		$conditions = array(
					"Commitment.contac_id" => $dataInfo["contac"]["Contac"]["id"]
		);
		$fields = array(
			"Commitment.project_id"=>$dataInfo["contac"]["Project"]["id"],
			"Commitment.client_id"=>$dataInfo["contac"]["Client"]["id"],
			"Commitment.state"=>"1"
		);
		$this->Contac->Commitment->updateAll($fields,$conditions);

	}


	//NOTIFICACIÓN CUANDO EL ACTA FINALIZA
	private function __sendNotificationContacFinished($contacId, $finishApprovalId = null){
		$this->__deleteCookieContac($contacId); 
		$info = $this->Contac->getAllInfoToContacPDF($contacId);
		if(empty($info['contac']['Contac']['was_sent'])) {
			if($info["contac"]["Team"]["contact_send_url"]){
				$this->__send_contact_api($info["contac"]["Team"]["contact_send_url"],$info);
            	$this->updateCommitmentTaskee($info);
			}
			if(!isset($finishApprovalId)){
				$full_name       = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
				$team            = $this->Contac->getNameTeam($contacId);
				$description     = sprintf(__("El usuario %s ha finalizado acta asociada a la empresa %s."), $full_name, $team);
				$descriptionEng  = sprintf(__("The user %s it is finished the minute associate with the company %s."), $full_name, $team);
				$this->buildInfoLog($info["contac"]["Contac"]["id"], $description, $descriptionEng, NULL, NULL, NULL, $info["contac"]["Contac"]["id"], $info["contac"]["Contac"]["team_id"]); 
			}

			 
			$listEmails = Set::combine($info["listEmails"], '{n}.User.email', '{n}');
			foreach ($listEmails as $email) { 
				if($email["User"]["lang"] == NULL || $email["User"]["lang"] == "" || empty($email["User"]["lang"])){
					$email["User"]["lang"] = "esp";
				} 
				$template = $email["User"]["lang"].'/contact_finished';
			    if($email["User"]["lang"] == "esp"){
					$subject  = sprintf(__("Acta %s finalizada - %s - %s"), $info["contac"]["Contac"]["number"], $info["contac"]["Client"]["name"], $info["contac"]["Project"]["name"]);
				} else {
					$subject  = sprintf(__("Minute %s finished - %s - %s"), $info["contac"]["Contac"]["number"], $info["contac"]["Client"]["name"], $info["contac"]["Project"]["name"]);
				}
				$options = array(
					'to'       => $email["User"]["email"],
					'template' => $template,
					'subject'  => $subject. ' - ' .Configure::read('Application.name'),
					'vars'     =>  array('info'=>$info),
				);
				$this->sendMail($options);  
			}
			$this->__markContactAsSent($contacId);  
		}
	}  

	private function __markContactAsSent($contacId = null) {
		$this->Contac->updateAll(
		    array('Contac.was_sent' => 1),
		    array('Contac.id' => $contacId)
		);
	}

	//RECORDATORIO DE CALIFICACIÓN
	public function reminder_approval_contac(){
		set_time_limit(300000);
		$this->autoRender = false;
		$conditions 	  = array("ApprovalContac.state" => Configure::read("DISABLED"), "Contac.state" => Configure::read("APPROVAL")); 
		$approvalContacs  = $this->Contac->ApprovalContac->find("all", compact("conditions")); 
		$template         = "";
		$subject          = "";
		if(!empty($approvalContacs)) {
			$userInfo  = $this->Contac->buildDataReminderApprovalContac($approvalContacs); 
			foreach ($userInfo as $email => $user) { 
				if($user["User"]["lang"] == NULL || $user["User"]["lang"] == "" || empty($user["User"]["lang"])){
					$user["User"]["lang"] = "esp";
				} 
				$template = $user["User"]["lang"].'/contac_approvals';
				if($user["User"]["lang"] == "esp"){
					$subject = __('Actas pendientes por aprobar').' - '.Configure::read('Application.name');
				} else {
					$subject = __('Minutes pending approval').' - '.Configure::read('Application.name');
				} 
			 	$options = array(
					'to'       => $email,
					'template' => $template,
					'subject'  => $subject,
					'vars'     =>  array( 
						'userId'      => $user["User"]["id"],  
						"contacs"     => $user["contac_id"]
					),
				);
				$this->sendMail($options); 	
			}			
		}
	}

	//PROMEDIO DE CALIFICACION PASADA LA FECHA
	public function average_conctac(){
		set_time_limit(300000);
		$fechaActual       = date("Y-m-d"); 
		$this->autoRender  = false; 
		$group             = array("ApprovalContac.contac_id");
		$conditions        = array("ApprovalContac.include" => Configure::read("ENABLED"), "Contac.state" => Configure::read("APPROVAL"), "ApprovalContac.limit_date < " => $fechaActual, "ApprovalContac.contac_id !=" => NULL);
		$approvalContacs   = $this->Contac->ApprovalContac->find("all", compact("conditions","group")); 
		$contacIds         = array();
		if(!empty($approvalContacs)){
			$contacIds = Set::classicExtract($approvalContacs, '{n}.ApprovalContac.contac_id');
			if(!empty($contacIds)){
				$infoContacts = $this->Contac->calculateAverageContac($contacIds); 
				$this->__processToApprovedOrRefuse($infoContacts);
			}
		}
	}

	private function __processToApprovedOrRefuse($infoContacts = array()){ 
		if(!empty($infoContacts["ContacsApproveds"])){ 
			$this->Contac->updateContacApproved($infoContacts["ContacsApproveds"]);
			foreach ($infoContacts["ContacsApproveds"] as $contacId) {
				$this->Contac->Commitment->commitmentsCreatedsFirebaseNotification($contacId); 
			 	$this->__sendNotificationContacFinishedAfterGetAverage($contacId);
			}
		}
		if(!empty($infoContacts["ContacsNotApproveds"])){ 
			foreach ($infoContacts["ContacsNotApproveds"] as $contacId) {
				$response = $this->Contac->saveFirebaseContacRefuse($contacId); 
				$this->__notificationContacUserCreator($contacId, $response); 
			}
		}
		$excludeContacsIds = array_merge($infoContacts["ContacsApproveds"],$infoContacts["ContacsNotApproveds"]);
		$this->Contac->excludeContacIdsCron($excludeContacsIds); 
	}

	private function __sendNotificationContacFinishedAfterGetAverage($contacId){
		$this->__deleteCookieContac($contacId);
		$info = $this->Contac->getAllInfoToContacPDF($contacId); 
		if(empty($info['contac']['Contac']['was_sent'])) {
			if($info["contac"]["Team"]["contact_send_url"]){
				$this->__send_contact_api($info["contac"]["Team"]["contact_send_url"],$info);
            	$this->updateCommitmentTaskee($info);
			}

			$template  = "";
			$subject   = "";
			$listEmails = Set::combine($info["listEmails"], '{n}.User.email', '{n}');

			foreach ($listEmails as $email) {
				if($email["User"]["lang"] == NULL || $email["User"]["lang"] == "" || empty($email["User"]["lang"])){
					$email["User"]["lang"] = "esp";
				} 
				$template = $email["User"]["lang"].'/contact_finished';
				if($email["User"]["lang"] == "esp"){
					$subject = sprintf(__("Acta %s finalizada - %s - %s"), $info["contac"]["Contac"]["number"], $info["contac"]["Client"]["name"], $info["contac"]["Project"]["name"]);
				} else {
					$subject = sprintf(__("Minute %s finished - %s - %s"), $info["contac"]["Contac"]["number"], $info["contac"]["Client"]["name"], $info["contac"]["Project"]["name"]);
				}  
				$options = array(
					'to'       => $email["User"]["email"],
					'template' => $template,
					'subject'  => $subject.' - '.Configure::read('Application.name'),
					'vars'     =>  array('info' => $info),
				);
				$this->sendMail($options); 
			}
			$this->__markContactAsSent($contacId); 
		}
	}  

	//NOTIFICACIÓN AL CREADOR DEL ACTA CUANDO SU ACTA ES RECHAZADA
	private function __notificationContacUserCreator($contacId, $emails){
		$conditions = array("Contac.id" => $contacId);
		$recursive  = 0;
		$user       = $this->Contac->find("first", compact("conditions","recursive"));		
		$template   = "";
		$subject    = "";
		if(AuthComponent::user()){
			$template = $this->Session->read("Config.language").'/contac_refuse_after_rate';
			$subject  = __('Acta rechazada').' - '.Configure::read('Application.name');
			$this->set_lang_notifications($this->Session->read("Config.language")); 
		} else {
			$template = $user["User"]["lang"].'/contac_refuse_after_rate';
			$subject  = __('Minute rejected').' - '.Configure::read('Application.name'); 
		} 
		$options = array(
			'to'       =>  $emails["userCreator"],
			'template' =>  $template,
			'subject'  =>  $subject,
			'vars'     =>  array("contacId" => $contacId, "userCreator" => 1),
		);
		$this->sendMail($options); 
	}

    public function download_contac($id = null){ 
    	$this->layout = false;
    	set_time_limit(300);
    	$id = EncryptDecrypt::decrypt($id);

		if (!$this->Contac->exists($id)) {
			
			$this->showMessageExceptions();
		}
    
		$contac = $this->Contac->getAllInfoToContacPDF($id);  
    
   
		$this->Session->write("actionPassword", "download_contac");
		$this->Session->write("ContacId", $id);
		$this->Session->write("approvalAction", EncryptDecrypt::encrypt($id) . ".pdf"); 
     
		$this->__validatePasswordContac($contac["contac"]['Contac']['password'],$this->Session->read('passwordEnter'),true);
    
 	 	if(!empty($contac["contac"]['Contac']['password'])){
			$this->pdfConfig = array(
				'download' 	    => true,
				'filename' 	    => __('Acta #') . $contac["contac"]["Contac"]["number"] . '.pdf', 
	        	'protect' 	    => true,
	         	'userPassword'  => $contac["contac"]['Contac']['password'],	         	  
			);
	 	} else {
	 		$this->pdfConfig = array(
				'download' 	    => true,
				'filename' 	    => __('Acta #') . $contac["contac"]["Contac"]["number"] . '.pdf',        	  
			);
	 	}	 
     	
	 	$this->set(compact('contac'));
	 	
	 	 
    }
	
	public function generar_pdf(){

	
    }

    public function list_users_to_approval(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->loadModel("UserTeam");
		$this->layout  = 'ajax'; 
		$approvalUsers = isset($this->request->data["usersApprovals"])    ?  $this->request->data["usersApprovals"] : null;
		$usersIds 	   = isset($this->request->data["usersIds"])    ?  $this->request->data["usersIds"] : null;
		$employeeIds   = isset($this->request->data["employeeIds"]) ?  $this->request->data["employeeIds"] : null;
		$userAll	   = $this->__getUserToApproval($usersIds, $employeeIds);
		$usersAdd      = array();
		$usersActual   = array_merge(!is_null($usersIds) ? $usersIds : array(), !is_null($employeeIds) ? $employeeIds : array()); 
		$this->set(compact('userAll','approvalUsers')); 
    }  

    private function __getUserToApproval($usersIds, $employeeIds){ 
		$conditions    = array("UserTeam.user_id" => $usersIds, "UserTeam.type_user" => Configure::read("COLLABORATOR"));
		$fields        = array("User.id","User.firstname","User.lastname","User.email");
		$users         = $this->UserTeam->find("all", compact("conditions","fields")); 
		$conditions    = array("UserTeam.user_id" => $employeeIds, "UserTeam.type_user" => Configure::read("EXTERNAL")); 
		$externos      = $this->UserTeam->find("all", compact("conditions","fields"));  
		$userAll       = array();
		foreach ($users as $id => $user) {
			$userAll[$user["User"]["id"]] = $user["User"]["firstname"] .  ' '  . $user["User"]["lastname"] . ' - ' . $user["User"]["email"];	 
		}
		foreach ($externos as $id => $user) {
			$userAll[$user["User"]["id"]] = $user["User"]["firstname"] .  ' '  . $user["User"]["lastname"] . ' - ' . $user["User"]["email"];	 
		} 
		return $userAll;
    }

    public function saveLogDeleteCommitmentBorrador(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;   
	 	$conditions     = array("User.id" => $this->request->data["assistanId"]);
	 	$firstname      = $this->Contac->User->field("firstname", $conditions);
	 	$lastname       = $this->Contac->User->field("lastname", $conditions);
	 	$conditions     = array("Commitment.id" => $this->request->data["commitmentId"]);
		$commitment     = $this->Contac->Commitment->find("first", compact("conditions"));
		$full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		$descriptionEng = sprintf(__("The user %s you have removed an commitment: Deadline: %s, Description: %s"), $full_name, $this->request->data["date"], $this->request->data["description"]);
		$description    = sprintf(__("El usuario %s ha eliminado un compromiso: Fecha: %s, Descripción: %s"), $full_name, $this->request->data["date"], $this->request->data["description"]);
 		$this->buildInfoLog($commitment["Commitment"]["id"], $description, $descriptionEng, NULL, NULL, NULL, $commitment["Contac"]["id"], $commitment["Contac"]["team_id"]); 
		$this->Contac->Commitment->delete($commitment["Commitment"]["id"]); 
    }

    public function find_upload_team(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout = 'ajax'; 
		$conditions   = array("Contac.id" => $this->request->data["contac_id"]);
		$recursive    = 0;
		$contac       = $this->Contac->find("first", compact("conditions","recursive"));
		$this->set(compact('contac'));  
    }

    //Recuperar contraseña del acta
    public function request_password_contac($id = null){
    	$id = EncryptDecrypt::decrypt($id);
		if (!$this->Contac->exists($id)) {
			$this->showMessageExceptions();
		}
		$this->autoRender = false; 
		$conditions = array("Contac.id" => $id);
		$recursive  = -1;
		$contac 	= $this->Contac->find("first", compact("conditions","recursive"));
	 	$user = AuthComponent::user("firstname") . ' ' . AuthComponent::user("lastname"); 
		$opts = array(
			'to'       => AuthComponent::user("email"),
			'subject'  =>__('Contraseña del acta').' - '.Configure::read('Application.name'),
			'vars'     => array('user' => $user, 'password' => $contac["Contac"]["password"], "id" => $contac["Contac"]["id"]),
			'template' => 'request_password_contac',
		); 
		$this->sendMail($opts);
		$this->Flash->success(__('Se ha enviado una notificación a tu correo electrónico con la información de la contraseña del acta.'));
		return $this->redirect(array('action' => 'index','controller' => 'contacs')); 	 
    }
  
    private function __createCookieContacTimer($contacId, $duration){
    	$cookieName = "time".$contacId; 
		setcookie($cookieName, $duration, time() + (86400 * 30), '/'); // 86400 = 1 day 
    	$this->deleteCookieInitial(); 
    }
 
    private function __deleteCookieContac($contacId){
    	$time = "time".$contacId;  
    	if(isset($_COOKIE[$time])) {
    		setcookie($time, "", time() + (86400 * 30), '/'); // 86400 = 1 day 
    	} 
    }  
  
    public function delete_files_documents(){
    	$this->autoRender = false;
    	$fechaActual      = date('Y-m-d H:i:s');
    	$conditions       = array("ContacsFile.contac_id" => NULL);
    	$recursive        = -1;
    	$images           = $this->Contac->ContacsFile->find("all", compact("conditions","recursive"));
    	$imgsIds          = array();
    	if (!empty($images)) {
            foreach ($images as $image) {
                $timeDiff  = strtotime($fechaActual) - strtotime($image["ContacsFile"]["created"]); 
                if($timeDiff >= 7200) { 
                	$directorio    = WWW_ROOT."files/Contac/";
					unlink($directorio.$image["ContacsFile"]["img"]);
					$imgsIds[] = $image["ContacsFile"]["id"]; 
                }                
            } 
            $this->Contac->ContacsFile->deleteAll(array('ContacsFile.id' => $imgsIds), false);
        } 
    	$conditions = array("ContacsDocument.contac_id" => NULL);
    	$documents  = $this->Contac->ContacsDocument->find("all", compact("conditions","recursive"));
    	$this->__deleteDocumentsBeforeMoreTime($documents, $fechaActual);
    }

    private function __deleteDocumentsBeforeMoreTime($documents = array(), $fechaActual){
    	$docsIds = array();
    	if (!empty($documents)) {
            foreach ($documents as $document) {
                $timeDiff  = strtotime($fechaActual) - strtotime($document["ContacsDocument"]["created"]);
                if($timeDiff >= 7200) { 
                	$directorio    = WWW_ROOT."document/Contac/";
					unlink($directorio.$document["ContacsDocument"]["document"]);
					$docsIds[] = $document["ContacsDocument"]["id"]; 
                }                
            } 
            $this->Contac->ContacsDocument->deleteAll(array('ContacsDocument.id' => $docsIds), false);
        } 
    }

    public function delete_image_dropzone(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$conditions       = array("ContacsFile.id" => $this->request->data["id"]);
		$recursive        = -1;
		$img              = $this->Contac->ContacsFile->find("first", compact("conditions","recursive"));
		$directorio       = WWW_ROOT."files/Contac/";
		if(!empty($img)){
			unlink($directorio.$img["ContacsFile"]["img"]);
        	$this->Contac->ContacsFile->deleteAll(array('ContacsFile.id' => $img["ContacsFile"]["id"]), false);
        }
    }

     public function delete_document_dropzone(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$conditions       = array("ContacsDocument.id" => $this->request->data["id"]);
		$recursive        = -1;
		$doc              = $this->Contac->ContacsDocument->find("first", compact("conditions","recursive"));
		$directorio       = WWW_ROOT."document/Contac/";
		if(!empty($doc)){
			unlink($directorio.$doc["ContacsDocument"]["document"]); 
        	$this->Contac->ContacsDocument->deleteAll(array('ContacsDocument.id' => $doc["ContacsDocument"]["id"]), false);
		}
    }

    public function show_last_contac(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';   
		$this->Contac->unbindModel(array('hasMany' => array('Commitment','Assistant','CommentContac','ApprovalContac'))); 
		$order            = array("Contac.id DESC");
		$conditions       = array("Contac.team_id" => isset($this->request->data["teamId"]) ? $this->request->data["teamId"] : NULL, "NOT" => array("Contac.id" => $this->Session->read("ContacId")), "Contac.state" => Configure::read("DISABLED"));
		$lastContac       = $this->Contac->find("first", compact("conditions","order"));
		$this->set(compact('lastContac'));  
    }
  
    private function __updateMeetingWithContac($contacId, $meetingId){ 
    	$this->Session->write("MeetingContac", array()); 
    	if(!empty($meetingId)){ 
    		$this->Contac->Assistant->Meeting->updateAll(
	            array('Meeting.contac_id'  => $contacId),
	            array('Meeting.id'   	   => $meetingId)
	    	);  
    	}
    }

    public function strip_description(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false; 
    	$description 	  = str_replace(array("<p>","</p>","<ul>","</ul>","<li>","</li>","<ol>","</ol>","&nbsp;"), array('',' ','',' ','',' ','',' ',''), strip_tags($this->request->data["description"], "<p><ul><li><ol>")); 	
    	$words       	  = explode(" ", $description);  
		$response    	  = true;
		foreach ($words as $word) { 
			if(!empty($word)) { 
				if(strlen($word) > 80){
					$response = false;
					break;
				}  
			}
		} 
		$this->outJSON(array("response" => $response)); 
    }

    public function show_commitments(){
    	if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout  = 'ajax';
		$commitmentsFb = array();
		if(!empty($this->request->data)){
			unset($this->request->data["commitments"]["0"]);
			$commitmentsFb = $this->request->data["commitments"];
		}
		$commitments = array();
		if(!empty($commitmentsFb)){
			$commitmentIds = Set::extract($commitmentsFb, '{n}.id');
			$commitments   = $this->Contac->getCommimentsView($commitmentIds); 
		} 
		$this->set(compact('commitments')); 
    }

	private  function openAudio($contact_Id,$texto){
	 	$arrayAudios = array();
 		$nombre_fichero = WWW_ROOT.'files/audio/'.$contact_Id.'/'.'1.mp3';
 		if (file_exists($nombre_fichero)) {
 			for ($i=1; $i < 10; $i++) { 
 				$nombre_fichero = WWW_ROOT.'files/audio/'.$contact_Id.'/'.$i.'.mp3';
 				if (file_exists($nombre_fichero)) {
 					$arrayAudios[] = $nombre_fichero = 'files/audio/'.$contact_Id.'/'.$i.'.mp3';
 				}else{
 					return $arrayAudios;
 					break;
 				}
 			}
		   return $arrayAudios;
		} else {
			$textos = $this->validateSize($texto);
             
           
			$numero = 1;
			foreach ($textos as $key => $value) {
				if($this->generateAudio($contact_Id,$value,$key+1)){

					 $arrayAudios[] = $nombre_fichero = 'files/audio/'.$contact_Id.'/'.$numero.'.mp3';
				}else{
				 
				}
				$numero++;
			}
		}
		return $arrayAudios;
 	}
 	private function validateSize($texto){
    	$iniPos = 0;
 		$finishPos = 3500;
 		$maxPos = 3500;
 		$MaximunPosition = strlen($texto);
 		$arrayPartes = array();
 		$partitiones = ceil( strlen($texto) / 3600);
 		if($MaximunPosition <= 3500){
 			$arrayPartes[] = $texto;
 		}else{
 			for ($i=1; $i <= $partitiones; $i++) { 
 			if($finishPos > $MaximunPosition){
 				$finishPos = $MaximunPosition - 4;
 			}
 			$lineaCorte = strpos($texto,'</p>',$finishPos);
 			$truncated = substr($texto,$iniPos,strpos($texto,'</p>',$finishPos));
 			$iniPos+= $lineaCorte ;
 			$finishPos = $lineaCorte + $finishPos;
 			$arrayPartes[] = $truncated;
 		}

 		}
		 return $arrayPartes;
    
     
    	 
 		
		  
 	}

 	public function generateAudio($contact_Id,$texto,$number){
    
  
     
 		
 		$ClearText = $this->Contac->htmClear(html_entity_decode($texto));

		$ch = curl_init(); https:
		curl_setopt($ch, CURLOPT_URL, 'https://api.us-south.text-to-speech.watson.cloud.ibm.com/instances/f97000a6-2d7d-4034-91aa-ddfc583ec7be/v1/synthesize?voice=es-ES_EnriqueVoice');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"text\":\"$ClearText\"}");
		curl_setopt($ch, CURLOPT_USERPWD, 'apikey' . ':' . 'ibuKTE5sl5wBniRf8-q2sm_j49W0Fsd2ro4QYjnCTNHw');
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Accept: audio/mp3';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
		    echo 'Error:' . curl_error($ch);
		}else{
			$estructura = WWW_ROOT.'files/audio/'.$contact_Id.'/';
			if(!mkdir($estructura, 0777, true)) {
			}
		    $fp = fopen(WWW_ROOT.'files/audio/'.$contact_Id.'/'.$number.'.mp3', 'w');
		    fwrite($fp, $result);
		    fclose($fp);
		    $nombre_fichero = WWW_ROOT.'files/audio/'.$contact_Id.'/'.$number.'.mp3';
		    if (file_exists($nombre_fichero)) {
		       curl_close($ch);
			   return 1;
			} else {
			   curl_close($ch);
			   return 0;
			}
		}
		curl_close($ch);
		return 0;

 	}



}  
