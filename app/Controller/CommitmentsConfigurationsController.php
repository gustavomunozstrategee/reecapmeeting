<?php
App::uses('AppController', 'Controller');
 
class CommitmentsConfigurationsController extends AppController { 

	public function save_time_commitment(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$time      		  = isset($this->request->data["optionTime"])  ? $this->request->data["optionTime"]  : null;
		if($time == 0){
			$defaultTime  = isset($this->request->data["defaultTime"]) ? $this->request->data["defaultTime"] : null;
			if($defaultTime == 0){
				$response = array("state" => false, "message" => __("El tiempo es requerido."));
			} else {
				$this->__saveTimeReminderCommitment($defaultTime);
				$response = array("state" => true, "message" => __("La configuración se ha guardado correctamente."));
			}
		} else {
			$this->__saveTimeReminderCommitment($time);
			$response = array("state" => true, "message" => __("La configuración se ha guardado correctamente."));
		}
		$this->outJSON($response); 
	}

	private function __saveTimeReminderCommitment($time){ 
		$recursive         = -1;
		$conditions 	   = array("CommitmentsConfiguration.user_id" => authcomponent::user("id"));
		$configurationHour = $this->CommitmentsConfiguration->find("first", compact("conditions","recursive"));
		$date              = $this->__addTimeToDateActual($time);
		$configurationHour["CommitmentsConfiguration"]["time"]    = $time;
		$configurationHour["CommitmentsConfiguration"]["user_id"] = authcomponent::user("id"); 
		$configurationHour["CommitmentsConfiguration"]["date"]    = $date;
		$this->CommitmentsConfiguration->save($configurationHour);
	}

	public function save_hour_commitment(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout      = 'ajax';
		$this->autoRender  = false;
		$recursive         = -1;
		$conditions 	   = array("CommitmentsConfiguration.user_id" => authcomponent::user("id"));
		$configurationHour = $this->CommitmentsConfiguration->find("first", compact("conditions","recursive"));
		$hour      		   = isset($this->request->data["hour"])  ? $this->request->data["hour"]  : null; 
		$configurationHour["CommitmentsConfiguration"]["hour"] = $hour;
		$configurationHour["CommitmentsConfiguration"]["user_id"] = authcomponent::user("id");
		$this->CommitmentsConfiguration->save($configurationHour);
		$response = array("state" => true, "message" => __("La configuración se ha guardado correctamente."));
		$this->outJSON($response); 		 
	}

	private function __addTimeToDateActual($time){
		$fecha 	     = date('Y-m-d'); 
		$nuevafecha  = strtotime("+{$time} day", strtotime($fecha)) ;
		$nuevafecha  = date ('Y-m-d', $nuevafecha);
		return $nuevafecha;
	}
  
}
