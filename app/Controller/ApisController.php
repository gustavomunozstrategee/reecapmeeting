<?php
App::uses('AppController', 'Controller');

App::uses('AuthComponent', 'Controller/Component');
App::uses('Encryption', 'Vendor'); 

class ApisController extends AppController {

	public $components = array('Paginator');
	public $uses = array();

	public function login() {
		if($this->request->is('post') && !empty($this->request->data['email']) 
			&& !empty($this->request->data['password'])) {

			$this->loadModel('User');
			App::uses('AuthComponent', 'Controller/Component');
			$conditions = array(
				'LOWER(User.email)' => strtolower($this->request->data['email']),
				'User.password' => AuthComponent::password($this->request->data['password']),
			);
			$recursive = -1;
			$user = $this->User->find('first', compact('conditions','recursive'));
			if(!empty($user)) {
				$validateActive = $this->__validateActive($user);
				if(!empty($validateActive)) {
					if(!empty($this->request->data['firebase_node'])) {
						$this->User->saveUserOnFirebase($user, $this->request->data['firebase_node']);
					}
					$user['User']['app_version_code'] = Configure::read('APP_VERSION_CODE'); 
					$user['User']['app_version_name'] = Configure::read('APP_VERSION_NAME'); 
					$user['User']['session_id'] = $user['User']['id'].'_'.uniqid(); 
					$user = array('User' => $user['User']);
					$user = $this->clearFields($user);
					$result = $this->getResult($user);
					return $this->out($result, $user, 'Response');

				} else {
					return $this->outFalseResponse($validateActive['msg']);
				}
			} else {
				$error = $this->getValidationErrors(array('password'=>array('message'=>'0003')));
				return $this->outFalseResponse($error);
			}
		}
		return $this->outFalseResponse();
	}

	private function __validateActive($user = array()) {
		$out = array('result' => true);
		if($user['User']['state'] == Configure::read('DISABLED')){
			$msg = $this->getValidationErrors(array('password'=>array('message'=>'0001')));
			$out = array('result' => false, 'msg' => $msg);

		} else if($user['User']['state'] == Configure::read('DISABLED_MANUAL')) {
			$msg = $this->getValidationErrors(array('password'=>array('message'=>'0002')));
			$out = array('result' => false, 'msg' => $msg);
		}
		return $out;
	}

	public function list_users() {
		if($this->request->is('post')) {
			$this->loadModel('User');
			try {
				$conditions = $this->User->listUserApisConditions($this->request->data); 
				$conditions = $this->__validateListUserPermissions($conditions);  
				$conditions['User.state'] = Configure::read('ENABLED');

				$limit  = Configure::read('USER_PAGINATION_LIMIT');  
				$fields = array('DISTINCT User.id', 'User.*');
				$order  = array('User.chat_name'=>'ASC');
				$recursive = 0;
				$this->User->bindUserTeamInfo();
				$this->Paginator->settings = compact('limit', 'fields', 'order', 'recursive');
		        $users = $this->Paginator->paginate($this->User, $conditions);

				$users = $this->clearFields($users);
				$result  = $this->getResult($users);
				return $this->out($result, $users, 'Response');
			} catch (Exception $e) {}
		}
		return $this->outFalseResponse();
	}

	private function __validateListUserPermissions($generalUserConditions = array()) {
		if(!empty($this->request->data['validate_permissions'])) {
			$this->loadModel('UserTeam');
	 		$conditions = array(
				'UserTeam.team_id' => Configure::read('Application.team_id'),
				'UserTeam.user_id' => $this->request->data['user_id']
			);
			$userRolePermission = $this->UserTeam->find('first',compact('conditions'));
			$userRolePermission = $userRolePermission['UserTeam'];

	 		$this->loadModel('Position');
	 		$conditions = array('Position.id' => $userRolePermission['position_id']);
			$roleInfo = $this->Position->find('first',compact('conditions'));
			$userRolePermission['Position'] = $roleInfo['Position'];
			
			$rolePermissionId = $roleInfo['Position']['permission_taskee']; 
			$teamId = Configure::read('Application.team_id');
			$departmentId = $userRolePermission['department_id'];

			//cuando el rol es administrador general
			if($rolePermissionId == 1){
				$generalUserConditions['UserTeam.team_id'] = $teamId; 
				$generalUserConditions['UserTeam.type_user'] = 1; 

			//cuando el rol es admin de area
			}elseif($rolePermissionId == 2){
				$generalUserConditions['UserTeam.team_id'] = $teamId; 
				$generalUserConditions['Department.id'] = $departmentId;

			//cuando es permiso normal
			}else{
				$generalUserConditions['UserTeam.team_id'] = $teamId; 
				$generalUserConditions['Department.id'] = $departmentId;
				$generalUserConditions['User.id'] = $this->request->data['user_id'];
			}
		}

		if(!empty($this->request->data['exclude_me'])) {
			$generalUserConditions['User.id <>'] = $this->request->data['user_id']; 
		}
		return $generalUserConditions;
	}

	public function list_chat_users() {
		if($this->request->is('post')) {
			$this->loadModel('User');
			try {
				$this->__setChatListRequestData();
				$conditions = $this->User->listUserApisConditions($this->request->data);  
				$limit  = Configure::read('USER_PAGINATION_LIMIT');  
				$fields = array('DISTINCT User.id', 'User.*');
				$order  = array('User.chat_name'=>'ASC');
				$recursive = -1;
				$this->Paginator->settings = compact('limit', 'fields', 'order', 'recursive');
		        $users = $this->Paginator->paginate($this->User, $conditions);
				$users = $this->clearFields($users);

		        $users = $this->findChatGroups($users); 

				$result  = $this->getResult($users);
				return $this->out($result, $users, 'Response');
			} catch (Exception $e) {}
		}
		return $this->outFalseResponse();
	}

	private function __setChatListRequestData() {
		if(!empty($this->request->data['chat_user_ids'])) {
			$this->request->data['chat_user_ids_'] = $this->request->data['chat_user_ids'];
		}
		if(empty($this->request->data['chat_user_ids_'])) {
			$this->request->data['chat_user_ids_'] = '00';
		}
	}

	private function findChatGroups($users = array()) {
		$this->loadModel('ChatGroup');
		$userId = $this->request->data['user_id'];
		$conditions = array('OR' => array(
			'ChatGroup.user_id' => $userId,
			"FIND_IN_SET({$userId}, ChatGroup.members)",
		));
		$recursive = -1;
		$groups = $this->ChatGroup->find('all', compact('conditions', 'recursive'));
		return array(
			'users' => $users,
			'groups' => $groups,
		);			
	}

	public function list_user_groups($users = array()) {
		if($this->request->is('post')) {
			$this->loadModel('ChatGroup');
			$userId = $this->request->data['user_id'];
			$conditions = array('OR' => array(
				'ChatGroup.user_id' => $userId,
				"FIND_IN_SET({$userId}, ChatGroup.members)",
			));
			$recursive = -1;
			$groups = $this->ChatGroup->find('all', compact('conditions', 'recursive'));
			
			$groups = $this->clearFields($groups);
			$result = $this->getResult($groups);
			return $this->out($result, $groups, 'Response');
		}
		return $this->outFalseResponse();
	}

	public function edit_profile() {
		if ($this->request->is('post') && !empty($this->request->data)) {	
			$this->loadModel('User');

			$this->User->__attachUploadImage($this->request->data, $this->request->form);
			$userSaved = $this->User->save($this->request->data, array('validate'=>false));
			if ($userSaved) {
				if(!empty($this->request->data['firebase_node'])) {
					$this->User->saveUserOnFirebase($userSaved, $this->request->data['firebase_node']);
				}
				$user   = $this->clearFields($userSaved['User']);
				$result = $this->getResult($user);
				return $this->out($result, $user, 'User');

			} else {
				$error = $this->getValidationErrors($this->User->validationErrors);
				return $this->outFalseResponse($error);
			}
		}
		return $this->outFalseResponse();
	}

	public function change_password() {
		if($this->request->is('post') && !empty($this->request->data['current_password']) 
			&& !empty($this->request->data['password'])) {
			$this->loadModel('User');
			$conditions = array(
				'User.id' => $this->request->data['id'],
				'User.password' => AuthComponent::password($this->request->data['current_password']),
			);
			$recursive = -1;
			$user = $this->User->find('first', compact('conditions', 'recursive'));
			if(!empty($user)) {
				if($this->User->save($this->request->data, array('validate'=>false))) {
					$user = Set::extract($user, 'User');
					$user = $this->clearFields($user);
					$result = $this->getResult($user);
					return $this->out($result, $user);
				} else {
					return $this->outFalseResponse();	
				}
			} else {
				$error = $this->getValidationErrors(array('password'=>array('message'=>'0004')));
				return $this->outFalseResponse($error);
			}
		}
		return $this->outFalseResponse();
	}

	public function list_group_users() {
		if($this->request->is('post') && !empty($this->request->data['members_'])) {
			try {
				$this->loadModel('User');
				$conditions = array('User.id' => $this->request->data['members_']);    
				$recursive = -1;
		        $users = $this->User->find('all',compact('conditions', 'recursive')); 
				
				$users = $this->clearFields($users);
				$result = $this->getResult($users);
				return $this->out($result, $users, 'Response');
			} catch (Exception $e) {}
		}
		return $this->outFalseResponse();
	}

	public function restore_password() {
		if($this->request->is('post') && !empty($this->request->data['email'])) {
			$this->loadModel('User');
			set_time_limit(604800);
			$conditions = array('LOWER(User.email)' => strtolower($this->request->data['email']));
			$recursive = -1;
			$user = $this->User->find('first', compact('conditions', 'recursive'));
			if(!empty($user)) {
				$hash = $this->__generateHash();
				$saveData = array('User'=>array('id'=>$user['User']['id'], 'hash_change_password'=>$hash));
				if($this->User->save($saveData, array('validate'=>false))) {
					$result = $this->getResult($this->request->data);
					$this->__restorePasswordEmail($user, $hash);
					return $this->out($result, $this->request->data);

				} else {
					$error = $this->getValidationErrors(array('id'=>array('message'=>'0006')));
					return $this->outFalseResponse($error);	
				}
			} else {
				$error = $this->getValidationErrors(array('id'=>array('message'=>'0005')));
				return $this->outFalseResponse($error);
			}
		}
		return $this->outFalseResponse();
	}

	private function __generateHash() {
		do{
			$this->loadModel('User');
		    $hash       = rand(10000,999999);
		    $conditions = array('User.hash_change_password' => $hash);
		    $hashExists = $this->User->field('id', $conditions);

		} while (!empty($hashExists));
		return $hash;
	}

	private function __restorePasswordEmail($user, $hash) {
		$opts = array(
		    'to'       => $user['User']['email'],
		    'subject'  => __('Restablecer contraseña'),
		    'vars'     => compact('user', 'hash'),
		    'template' => 'mobile_restore_password',
		);
		$this->sendMail($opts);	
	}

	public function validate_restore_password_code() {
		if($this->request->is('post') && !empty($this->request->data['email'])) {
			$this->loadModel('User');
			$conditions = array(
				'User.hash_change_password'  => $this->request->data['code'],
				'LOWER(User.email)' => strtolower($this->request->data['email']),
			);
			$recursive = -1;
			$user = $this->User->find('first', compact('conditions', 'recursive'));
			if(!empty($user)) {
				$user   = Set::extract($user, 'User');
				$user   = $this->clearFields($user);
				$result = $this->getResult($user);
				return $this->out($result, $user, 'User');
			}
		}
		return $this->outFalseResponse();
	}

	public function new_password() {
		if($this->request->is('post') && !empty($this->request->data['password'])
			&& !empty($this->request->data['id'])) {
				$this->loadModel('User'); 
				if($this->User->save($this->request->data, array('validate'=>false))) {
					$user   = $this->clearFields($this->request->data);
					$result = $this->getResult($user);
					return $this->out($result, $user, 'Response');

				} else {
					$error = $this->getValidationErrors(array('id'=>array('message'=>'0009')));
					return $this->outFalseResponse($error);	
				}
		}
		return $this->outFalseResponse();
	}

	public function list_commitments() {

		if($this->request->is('post') && !empty($this->request->data)) {
			try {
				$this->loadModel('Commitment');
				$conditions = $this->Commitment->apiCommitmentConditions($this->request->data);

				$recursive = 0;
				$fields = array('User.*', 'Project.*', 'Client.*', 'Commitment.*');
				$order = $this->Commitment->orderCommitmentApiList($this->request->data);
				$limit = Configure::read('PAGINATION_LIMIT');
				$this->Commitment->Behaviors->load('Containable');
				$this->Paginator->settings = compact('limit','recursive','order','fields');
				$commitments = $this->Paginator->paginate($this->Commitment, $conditions);

				if(!empty($commitments)) {
					$commitments = $this->clearFields($commitments);
					$result = $this->getResult($commitments);
					return $this->out($result, $commitments, 'Response');
				}
			} catch (Exception $e) {}
		}
		return $this->outFalseResponse();
	}

	public function view_commitment() {
		if (!empty($this->request->data)) {
			$this->loadModel('Commitment');
			$conditions = array('Commitment.id' => $this->request->data['commitment_id']);
			$recursive  = 0;
			$commitment = $this->Commitment->find('first', compact('conditions', 'contain', 'recursive'));
			$commitment = $this->clearFields($commitment);
			$result = $this->getResult($commitment);
			return $this->out($result, $commitment, 'Response');
		}
		return $this->outFalseResponse();
	}

	public function list_minutes() {
		if($this->request->is('post')) {
			try {
				$this->loadModel('Contac');
				$conditions = $this->Contac->apiMinuteConditions($this->request->data); 
				$conditions['Contac.state'] = Configure::read('DISABLED');
				$limit = Configure::read('PAGINATION_LIMIT');  
				$this->Contac->Behaviors->load('Containable');
				$contain = array('User', 'Client', 'Project');
				$fields = array('DISTINCT Contac.id', 'User.*', 'Contac.*', 'Client.*', 'Project.*');
				$order = array('Contac.created'=>'DESC');
				$recursive = 0;
				$this->Paginator->settings = compact('limit','contain', 'fields', 'order', 'recursive');
		        $contacs = $this->Paginator->paginate($this->Contac, $conditions); 
				
				$contacs = $this->clearFields($contacs);
				$result  = $this->getResult($contacs);
				return $this->out($result, $contacs, 'Response');
			} catch (Exception $e) {}
		}
		return $this->outFalseResponse();
	}

	public function view_minute() {
		if (!empty($this->request->data)) {
			set_time_limit(604800);
			$this->loadModel('Contac');
			$conditions = array('Contac.id' => $this->request->data['minute_id']);
			$recursive = 0;
			$contact = $this->Contac->find('first', compact('conditions', 'contain', 'recursive'));
			if(!empty($contact['Contac']['id'])) {
				$users = $this->Contac->Assistant->getAssistantsContacFinished($contact['Contac']['id']); 
				if(!empty($users['collaborators'])) {
					$contact['Contac']['collaborators'] = implode("\n\n", $users['collaborators']);
				}
				if(!empty($users['assistants'])) {
					$contact['Contac']['assistants'] = implode("\n\n", $users['assistants']);
				}

				$audioURL = array();
				try {
			    	if($contact['Contac']["state"] == Configure::read('DISABLED')){
						$audioURL = $this->openAudio($contact['Contac']['id'], $contact['Contac']['description']);					 
					}
				} catch (Exception $e) {}
				$contact['Contac']['audio_url'] = $audioURL;
			}
			$result = $this->getResult($contact);
			return $this->out($result, $contact, 'Response');
		}
		return $this->outFalseResponse();
	}

	public function save_tokens() {
		if($this->request->is('post') && !empty($this->request->data)) {
			$this->loadModel('Token');
			$conditions = array(
				'Token.token' => $this->request->data['token_id'],
				'Token.user_id' => $this->request->data['user_id'],
			);
			$tokenId = $this->Token->field('id', $conditions);
			if(empty($tokenId)) {
				$saveData = array(
					'token' => $this->request->data['token_id'],
					'user_id' => $this->request->data['user_id'],
				);
				if($this->Token->save($saveData)) {
					$token  = array('id'=>$this->Token->id);
					$result = $this->getResult($token);
					return $this->out($result, $token);
				}
			} else {
				return $this->out(array('result'=>true), $this->request->data);
			}
		}
		return $this->outFalseResponse();	
	}

	public function delete_token() {
		if($this->request->is('post') && !empty($this->request->data)) {
			$this->loadModel('Token');
			$conditions = array(
				'Token.token' => $this->request->data['token_id'],
			);
			$this->Token->deleteAll($conditions);
			$result = array('result' => true);
			return $this->out($result, array());
		}
		return $this->outFalseResponse();
	}

	public function upload_file() {
		if ($this->request->is('post') && !empty($this->request->data)) {
			$this->loadModel('UsersFile');
			if(!empty($this->request->form['file_name'])) {
				$this->request->data['file_name'] = $this->request->form['file_name'];
			}
			$this->UsersFile->create();
			$this->__convertAudio();
			$savedData = $tempSavedData = $this->UsersFile->save($this->request->data);

			if ($savedData) {
				$savedData = $this->__convertVideo($savedData);
				$savedData = $this->UsersFile->optimizeProfileImage($savedData);
				$out = array(
					'id'        => $this->UsersFile->id,
					'file_name' => $savedData['UsersFile']['file_name'],
					'extension' => strtolower($savedData['UsersFile']['file_extension']),
					'thumbnail' => '',
				);
				if(!empty($tempSavedData['UsersFile']['file_name'])) {
					$nameContent = explode(".", $tempSavedData['UsersFile']['file_name']);
					$thumbname = "thumb_{$nameContent['0']}.png";
					$file = APP.'webroot'.DS.'files'.DS.'UsersFiles'.DS.$thumbname;
					if(file_exists($file)) {
						$out['thumbnail'] = $thumbname;
					}
				}
				$result = $this->getResult($out);
				return $this->out($result, $out, 'UsersFile');

			} else {
				$error = $this->getValidationErrors($this->UsersFile->validationErrors);
				return $this->outFalseResponse($error);
			}
		}
		return $this->outFalseResponse();
	}

	private function __convertVideo($savedData = array()) {
		if($this->request->data['app_asset_type'] == 'video') {
			$nameContent = explode(".", $savedData['UsersFile']['file_name']);
			$ext = end($nameContent);
			if(!empty($ext) && strtolower($ext) != "mp4") {
				try{
					$newFileName = uniqid().".mp4";
					$from = APP."webroot/files/UsersFiles/{$savedData['UsersFile']['file_name']}";
					$to   = APP."webroot/files/UsersFiles/{$newFileName}";
					//exec("ffmpeg -i {$from} -b 1500k -vcodec libx264 -vpre veryfast -vpre baseline -g 30 {$to}");
					//exec("ffmpeg -i {$from} -vcodec h264 -acodec aac -strict -2 -preset ultrafast {$to}");
					exec("ffmpeg -i {$from} -c:a copy -c:v libx264 -preset ultrafast -profile:v baseline {$to}");
					if(file_exists($to)) {
						$savedData['UsersFile']['file_name']      = "{$newFileName}";
						$savedData['UsersFile']['extension']      = "mp4";
						$savedData['UsersFile']['file_extension'] = "mp4";
						$savedData['UsersFile']['type']           = "video/mp4";
						$this->UsersFile->save($savedData);
						unlink($from);
					}
					 
				} catch (Exception $e) { }
			}
		}
		return $savedData;
	}

	private function __convertAudio() {
		if($this->request->data['app_asset_type'] == 'audio') {
			$nameContent = explode(".", $this->request->data['file_name']['name']);
			$ext = end($nameContent);
			if(!empty($ext) && strtolower($ext) != "mp3") {
				try{
					$newFileName = uniqid().".mp3";
					$from = "{$this->request->data['file_name']['tmp_name']}";
					$to   = APP."webroot/files/UsersFiles/{$newFileName}";
					exec("ffmpeg -i {$from} -f mp3 -ab 192000 -vn {$to}");
					unset($this->request->data['file_name']);
					$this->request->data['file_name']      = "{$newFileName}";
					$this->request->data['extension']      = "mp3";
					$this->request->data['file_extension'] = "mp3";
					$this->request->data['type'] = "audio/mpeg";
					 
				} catch (Exception $e) { }
			}
		}
	}

	public function change_state() {
		if($this->request->is('post') && !empty($this->request->data['commitment_id'])
			&& !empty($this->request->data['state'])) {
			$this->loadModel('Commitment');
			$this->Commitment->updateAll(
			    array('Commitment.state' => $this->request->data['state']),
			    array('Commitment.id'    => $this->request->data['commitment_id'])
			);
			$result = array('result'=>true);
			return $this->out($result, array());
		}
		return $this->outFalseResponse();
	}

	public function load_search_minute_info() {
		if($this->request->is('post')) {
			$projects = $this->__listProjects();			
			$projects = array('ProjectList' => $projects);	
			$projects = $this->clearFields($projects);
			$result = $this->getResult($projects);
			return $this->out($result, $projects, 'Response');
		}
		return $this->outFalseResponse();
	}

	public function add_group() {
		if($this->request->is('post') && !empty($this->request->data)) {
			$this->loadModel('ChatGroup');
			if(!empty($this->request->data['members_'])) {
				$this->request->data['members_'][$this->request->data['user_id']] = $this->request->data['user_id'];
				$this->request->data['members'] = implode(',', $this->request->data['members_']);
			}
			$this->request->data['uniqid'] = $this->request->data['user_id'].'_group_'.uniqid();
			$savedData = $this->ChatGroup->save($this->request->data); 
			if($savedData) {
				$result = $this->getResult($savedData);
				return $this->out($result, $savedData, 'Response');

			} else {
				$error = $this->getValidationErrors(array('id'=>array('message'=>'0006')));
				return $this->outFalseResponse($error);	
			}
		}
		return $this->outFalseResponse();	
	}

	public function view_group() {
		if (!empty($this->request->data)) {
			$this->loadModel('ChatGroup');
			$conditions = array('ChatGroup.uniqid' => $this->request->data['group_id']);
			$recursive = -1;
			$chatGroup = $this->ChatGroup->find('first', compact('conditions', 'recursive'));
			if(!empty($chatGroup)) {
				if(!empty($chatGroup['ChatGroup']['members'])) {
					$this->loadModel('User');
					$listMembers = explode(',', $chatGroup['ChatGroup']['members']);
					$conditions = array('User.id' => $listMembers);
					$recursive = -1;
					$users = $this->User->find('all', compact('conditions', 'recursive'));
					$chatGroup['ChatGroup']['users'] = $users;
					$chatGroup['ChatGroup']['total_members'] = count($listMembers);
					$chatGroup['ChatGroup']['count_members'] = (count($listMembers) - 1);//no cuenta al administrador como miembro
					$chatGroup['ChatGroup']['members'] = array_combine($listMembers, $listMembers);
					$chatGroup['ChatGroup']['list_members'] = $listMembers;
				}

				$result = $this->getResult($chatGroup);
				return $this->out($result, $chatGroup, 'Response');
			}
		}
		return $this->outFalseResponse();
	}

	public function edit_group() {
		if($this->request->is('post') && !empty($this->request->data)) {
			$this->loadModel('ChatGroup');
			if(!empty($this->request->data['members_'])) {
				$this->request->data['members_'][$this->request->data['user_id']] = $this->request->data['user_id'];
				$this->request->data['members'] = implode(',', $this->request->data['members_']);
			}

			$uniqueId = $this->request->data['id'];
			$conditions = array('ChatGroup.uniqid' => $uniqueId);
			$id = $this->ChatGroup->field('id', $conditions);
			$this->request->data['id'] = $id;
			$savedData = $this->ChatGroup->save($this->request->data); 

			if($savedData) {
				if(!empty($savedData['ChatGroup']['members_'])) {
					unset($savedData['ChatGroup']['members_']);
				}
				if(!empty($savedData['ChatGroup']['members'])) {
					unset($savedData['ChatGroup']['members']);
				}
				$result = $this->getResult($savedData);
				return $this->out($result, $savedData, 'Response');

			} else {
				$error = $this->getValidationErrors(array('id'=>array('message'=>'0006')));
				return $this->outFalseResponse($error);	
			}
		}
		return $this->outFalseResponse();	
	}

	public function find_add_commitment_info() {
		if($this->request->is('post')) {
			$out = array(
				'types' => array_chunk(Configure::read('TipoCompromiso'), 1, true),
				'priorities' => array_chunk(Configure::read('PrioridadTareas'), 1, true),
				'projects' => $this->__listProjects(),
			);
			$result = $this->getResult($out);
			return $this->out($result, $out, 'Response');
		}
		return $this->outFalseResponse();
	}

	private function __listProjects() {
		$this->loadModel('Project');
		$conditions = array('Project.team_id' => Configure::read('Application.team_id'));
		$order = array('Project.name' => 'ASC');
		$recursive = 0;
		$this->Project->Behaviors->load('Containable');
		$contain = array('Client');
		$projects = $this->Project->find('all', compact('conditions', 'order', 'recursive', 'contain'));
		return $projects; 
	}

	public function save_commitment() {
		if($this->request->is('post') && !empty($this->request->data)) {
			$this->loadModel('Commitment');
			$this->Commitment->create();
			if(empty($this->request->data['user_id'])) {
				$this->request->data['user_id'] = $this->request->data['session_user_id']; 
			}
			$validate = $this->__validateCommitment();
			if(!empty($validate['result'])) {
				$savedData = $this->Commitment->save($this->request->data); 
				if($savedData) {
					$result = $this->getResult($savedData);
					return $this->out($result, $savedData, 'Response');

				} else {
					$error = $this->getValidationErrors(array('id'=>array('message'=>'0006')));
					return $this->outFalseResponse($error);	
				}
			} else {
				$error = $validate['msg'];
				return $this->outFalseResponse($error);	
			}
		}
		return $this->outFalseResponse();	
	}

	private function __validateCommitment() {
		$out =  array('result' => true);
		if(strtotime($this->request->data['delivery_date']) < strtotime(date('Y-m-d'))) {
			$out =  array('result' => false, 'msg' => $this->getValidationErrors(array('id'=>array('message'=>'0007'))));
		}
		return $out;
	}










    public  function openAudio($contact_Id, $texto){
        $arrayAudios = array();
        $nombre_fichero = WWW_ROOT.'files/audio/'.$contact_Id.'/'.'1.mp3';
        if (file_exists($nombre_fichero)) {
            for ($i=1; $i < 10; $i++) { 
                $nombre_fichero = WWW_ROOT.'files/audio/'.$contact_Id.'/'.$i.'.mp3';
                if (file_exists($nombre_fichero)) {
                    $arrayAudios[] = $nombre_fichero = 'files/audio/'.$contact_Id.'/'.$i.'.mp3';
                }else{
                    return $arrayAudios;
                    break;
                }
            }
            return $arrayAudios;
        } else {
            $textos = $this->validateSize($texto);;
            $numero = 1;
            foreach ($textos as $key => $value) {
                if($this->generateAudio($contact_Id,$value,$key+1)){
                    $arrayAudios[] = $nombre_fichero = 'files/audio/'.$contact_Id.'/'.$numero.'.mp3';
                }else{

                }
                $numero++;
            }
        }
        return $arrayAudios;
    }
    
    private function validateSize($texto){
        $iniPos = 0;
        $finishPos = 3500;
        $maxPos = 3500;
        $MaximunPosition = strlen($texto);
        $arrayPartes = array();
        $partitiones = ceil( strlen($texto) / 3600);
        if($MaximunPosition <= 3500){
            $arrayPartes[] = $texto;

        }else{
            for ($i=1; $i <= $partitiones; $i++) { 
                if($finishPos > $MaximunPosition){
                    $finishPos = $MaximunPosition - 4;
                }
                $lineaCorte = strpos($texto,'</p>',$finishPos);
                $truncated = substr($texto,$iniPos,strpos($texto,'</p>',$finishPos));
                $iniPos+= $lineaCorte ;
                $finishPos = $lineaCorte + $finishPos;
                $arrayPartes[] =  strip_tags($truncated);
            }
        }
        return $arrayPartes;
    }

    public function generateAudio($contact_Id, $texto, $number){
        $ClearText = $this->Contac->htmClear(html_entity_decode($texto));
        $ch = curl_init(); https:
        curl_setopt($ch, CURLOPT_URL, 'https://api.us-south.text-to-speech.watson.cloud.ibm.com/instances/f97000a6-2d7d-4034-91aa-ddfc583ec7be/v1/synthesize?voice=es-ES_EnriqueVoice');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"text\":\"$ClearText\"}");
        curl_setopt($ch, CURLOPT_USERPWD, 'apikey' . ':' . 'ibuKTE5sl5wBniRf8-q2sm_j49W0Fsd2ro4QYjnCTNHw');
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: audio/mp3';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }else{
            $estructura = WWW_ROOT.'files/audio/'.$contact_Id.'/';

            if(@!mkdir($estructura, 0777, true)) {}
            $fp = fopen(WWW_ROOT.'files/audio/'.$contact_Id.'/'.$number.'.mp3', 'w');
            fwrite($fp, $result);
            fclose($fp);
            $nombre_fichero = WWW_ROOT.'files/audio/'.$contact_Id.'/'.$number.'.mp3';
            if (file_exists($nombre_fichero)) {
                curl_close($ch);
                return 1;
            } else {
                curl_close($ch);
                return 0;
            }
        }
        curl_close($ch);
        return 0;
    }

}