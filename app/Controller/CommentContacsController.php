<?php
App::uses('AppController', 'Controller');
 
class CommentContacsController extends AppController {
 
	public $components = array('Paginator');

	public function index() {
		$this->CommentContac->recursive = 0;
		$this->set('commentContacs', $this->Paginator->paginate());
	} 

	public function comments_contac($contacId = null){
		$contacId 				   = EncryptDecrypt::decrypt($contacId); 
        $limit                     = 10;
        $order                     = array('CommentContac.modified'=>'DESC');
        $conditions                = array('CommentContac.contac_id' => $contacId); 
        $this->Paginator->settings = compact('conditions','limit','order');
        $commentContacs            = $this->Paginator->paginate();  
		$contac  = $this->CommentContac->Contac->getAllInfoToContacPDF($contacId); 
		$this->Session->write("actionPassword", "comments_contac");
		$this->Session->write("ContacId", $contacId);
		$this->Session->write("approvalAction", EncryptDecrypt::encrypt($contacId)); 
		$this->__validatePasswordContac($contac["contac"]['Contac']['password'],$this->Session->read('passwordEnter'),true);
		$this->set(compact('contac','commentContacs')); 
	} 

	private function __validatePasswordContac($password,$validate,$update) {
		if ($validate == Configure::read('DISABLED')) {
			if ($password != '')  {
				$this->Session->write('passwordContac',Configure::read('ENABLED')); 
				$this->redirect(array('action' => 'password','controller' => 'contacs'));
			}
		} else {
			$this->Session->write('passwordContac',Configure::read('DISABLED'));
		}
		return true;
	} 
}
