<?php
App::uses('AppController', 'Controller');
 
class AssistantsController extends AppController {
 
	public $components = array('Paginator','RequestHandler');
	public $helpers    = array('Js');
 
	public function index() {
		$this->Assistant->recursive = 0;
		$this->set('assistants', $this->Paginator->paginate());
	}

	public function searchInvited(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		// $this->autoRender = false;	 
		$fieldInvited     = isset($this->request->data["invited"])  ? trim($this->request->data["invited"]) : null;
		$clientId         = isset($this->request->data["clientId"]) ? trim($this->request->data["clientId"]) : null; 
		$teamId           = isset($this->request->data["teamId"])   ? trim($this->request->data["teamId"]) : null; 
		$users = $this->Assistant->searchInvited($fieldInvited, $clientId, $teamId);	 
		$this->set(compact("users"));
	}

	public function add_users_invited(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';  
		$id               = isset($this->request->data["id"])       ? trim($this->request->data["id"]) : null;	
		$typeUser         = isset($this->request->data["typeUser"]) ? $this->request->data["typeUser"] : null;
		$this->__addMySelfMeeting($typeUser);
		$user 			  = $this->Assistant->searcTypeUser($typeUser, $id);
		$usersInviteds    = $this->Session->read('Inviteds');  
		$userData = array(
		 	"name"        => $user["user"]["User"]["firstname"] . ' ' .  $user["user"]["User"]["lastname"],
		 	"email"       => $user["user"]["User"]["email"],
		 	"type_user"   => $typeUser,
		 	"user_id"     => EncryptDecrypt::encrypt($user["user"]["User"]["id"]),
		 	"image"		  => $user["user"]["User"]["img"]
		);  
		$this->Session->write("Inviteds.{$user["id"]}", $userData);
		$userInvitedsAdded = $this->Session->read('Inviteds');
		$this->set(compact('userInvitedsAdded')); 
	}
 
	private function __addMySelfMeeting($typeUser){
		$recursive  = -1;
		$conditions = array("User.id" => Authcomponent::user("id"));
		$user 		= $this->Assistant->User->find("first", compact("conditions","recursive")); 
		$user["User"]["id"] = EncryptDecrypt::encrypt($user["User"]["id"]);
		$usersInviteds    = $this->Session->read('Inviteds'); 
		$userData = array(
		 	"name"        => $user["User"]["firstname"] . ' ' .  $user["User"]["lastname"],
		 	"email"       => $user["User"]["email"],
		 	"type_user"   => EncryptDecrypt::encrypt(configure::read("COLLABORATOR")),
		 	"user_id"     => $user["User"]["id"],
		 	"image"		  => $user["User"]["img"]
		); 
		$this->Session->write("Inviteds.{$user["User"]["id"]}", $userData);
	}

	public function list_assistant_meeting($meetingId = null){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$meetingId = EncryptDecrypt::decrypt($meetingId);
		$this->Assistant->unbindModel(array('belongsTo' => array('Contac','Meeting')));
		$this->layout = 'ajax';		 		 
		$conditions   = array('Assistant.meeting_id' => $meetingId);		 
		$order        = array('Assistant.created DESC');
		$limit        = 10;
		$this->Paginator->settings = compact('conditions','limit','order');
        $assistants   =  $this->Paginator->paginate();    
		$this->set(compact('assistants','meetingId')); 
	}

	public function removeUserFromMeeting(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false; 
		$id   			  = isset($this->request->data["dataId"]) ? $this->request->data["dataId"] : null; 
		$usersInviteds    = $this->Session->read('Inviteds');
		unset($usersInviteds[$id]);
		$this->Session->write("Inviteds", $usersInviteds); 
	}

	public function list_assistants(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout      = 'ajax';
		$userInvitedsAdded = $this->Session->read('Inviteds');
		$this->set(compact('userInvitedsAdded'));  
	}

	// //LISTAR LOS TODOS LOS USUARIOS QUE SE VAYAN AÑADIENDO
	public function list_assistants_edit(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		// $this->Session->destroy();
		$this->layout = 'ajax';
		$assistants = $this->Session->read("StoredAssistant"); 
		$this->set(compact('assistants')); 
	}

	//AÑADIR USUARIOS NUEVOS EN EL EDITAR REUNION
	public function add_users_invited_meeting_edit(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout 	  = "ajax";
		$this->autoRender = false;
		if (!$this->Assistant->Meeting->exists(EncryptDecrypt::decrypt($this->request->data["meetingId"]))) {
			$this->showMessageExceptions();
		}
		$this->__validateUseChoose($this->request->data); 
	}

	private function __validateUseChoose($data = array()){ 
		$conditions = array("Assistant.user_id" => EncryptDecrypt::decrypt($data["id"]), 'Assistant.meeting_id' => EncryptDecrypt::decrypt($data["meetingId"]));
		$user 		= $this->Assistant->find("first", compact("conditions")); 
		if(empty($user)){
			$this->__saveAssistantUserMeetingEdit($data);
		} else {
			$this->__updateAssistantUserMeetingEdit(EncryptDecrypt::decrypt($data["meetingId"]), $user);
		} 
	}  

	private function __saveAssistantUserMeetingEdit($data = array()){
		$assistants = $this->Session->read("StoredAssistant");
		$recursive  = -1;
		$conditions = array("User.id" => EncryptDecrypt::decrypt($data["id"]));
		$user       = $this->Assistant->User->find("first", compact("conditions","recursive"));
		$id 		= $user["User"]["id"];
		$newAssistant  = array(
			"id"          => NULL,
			"meeting_id"  => EncryptDecrypt::decrypt($data["meetingId"]),
			"user_id"     => $user["User"]["id"], 
			"type_user"   => $data["typeUser"],
			"name"        => $user["User"]["firstname"] . ' ' . $user["User"]["lastname"],
			"email"       => $user["User"]["email"],
			"image"       => $user["User"]["img"],
			"state"       => configure::read("NEW_ASSISTANT")
		);
		$this->Session->write("StoredAssistant.{$id}", $newAssistant); 
	}

	private function __updateAssistantUserMeetingEdit($meetingId, $data = array()){
		$assistants = $this->Session->read("StoredAssistant");
		$recursive  = -1;
		$conditions = array("User.id" => $data["User"]["id"]);
		$user       = $this->Assistant->User->find("first", compact("conditions","recursive"));
		$id 		= $user["User"]["id"];
		$newAssistant  = array(
			"id"          => NULL,
			"meeting_id"  => $meetingId,
			"user_id"     => $user["User"]["id"], 
			"type_user"   => $data["typeUser"],
			"name"        => $user["User"]["firstname"] . ' ' . $user["User"]["lastname"],
			"email"       => $user["User"]["email"],
			"state"       => configure::read("ENABLED")
		);
		$this->Session->write("StoredAssistant.{$id}", $newAssistant);
	} 
	
	public function store_assistant_actual(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout 	  = "ajax";
		$this->autoRender = false;
		if (!$this->Assistant->Meeting->exists(EncryptDecrypt::decrypt($this->request->data["meetingId"]))) {
			$this->showMessageExceptions();
		}
		$this->Assistant->unbindModel(array('belongsTo' => array('Contac','Meeting'))); 
	 	$meetingId  = isset($this->request->data["meetingId"]) ? EncryptDecrypt::decrypt($this->request->data["meetingId"]) : null;
 		$conditions = array("Assistant.meeting_id" => $meetingId);
 		$assistants = $this->Assistant->find("all", compact("conditions")); 
 		$this->__getAssistantEditMeeting($assistants, $meetingId); 
	}

	private function __getAssistantEditMeeting($assistants = array(), $meetingId){
		try {
			if(!empty($assistants)){
				foreach ($assistants as $assistant) { 
					$storeAssistants[$assistant["Assistant"]["user_id"]] = array(
						"id"          => $assistant["Assistant"]["id"],
						"meeting_id"  => $meetingId,
						"user_id"     => $assistant["Assistant"]["user_id"], 
						"type_user"   => $assistant["Assistant"]["type_user"],
						"name"        => $assistant["User"]["firstname"] . ' ' .$assistant["User"]["lastname"],
						"email"       => $assistant["User"]["email"],
						"image"       => $assistant["User"]["img"],
						"state"       => configure::read("ENABLED")
					);  
				}
				$this->__storeSessionEditMeetingAssistant($storeAssistants);
			}
		} catch (Exception $e) {}
	}

	private function __storeSessionEditMeetingAssistant($assistants = array()){  
		$this->Session->write("StoredAssistant", $assistants);
		$sessions =  $this->Session->read("StoredAssistant"); 
	}

	//METODO PARA ALMACENAR EN UNA SESION LOS USUARIOS QUE SE VAN ELIMINAR
	public function add_In_Session_Assistant_Delete(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}
		$this->layout 	  = "ajax";
		$this->autoRender = false;
		$assistantId 	  = isset($this->request->data["assistantId"]) ? EncryptDecrypt::decrypt($this->request->data["assistantId"]) : null; 
		$meetingId 	  	  = isset($this->request->data["meetingId"])   ? EncryptDecrypt::decrypt($this->request->data["meetingId"]) : null; 
		$this->__updateAssistantDelete($meetingId, $assistantId); 
	} 

	private function __updateAssistantDelete($meetingId, $id){ 
		$recursive  = -1;  
		$conditions = array("User.id" => $id);
		$user       = $this->Assistant->User->find("first", compact("conditions","recursive"));
		$ids 		= $user["User"]["id"];
		$assistants = $this->Session->read("StoredAssistant");
		$assistants[$ids]["state"] = configure::read("DELETE_ASSISTANT");
		$this->Session->write("StoredAssistant", $assistants); 
	}

	public function delete_asistants_not_save(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		}  
		$this->layout 	  = "ajax";
		$this->autoRender = false;
		if (!$this->Assistant->Meeting->exists(EncryptDecrypt::decrypt($this->request->data["meetingId"]))) {
			$this->showMessageExceptions();
		}
	 	$meetingId  = isset($this->request->data["meetingId"]) ? EncryptDecrypt::decrypt($this->request->data["meetingId"]) : null;
	 	$conditions = array("Assistant.meeting_id" => $meetingId);
 		$assistants = $this->Assistant->find("all", compact("conditions")); 
 		$this->__getAssistantEditMeeting($assistants, $meetingId); 
		$sessions =  $this->Session->read("StoredAssistant"); 
	}
}
