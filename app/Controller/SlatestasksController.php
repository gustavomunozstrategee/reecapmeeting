<?php
App::uses('AppController', 'Controller');
/**
 * SlatesTasks Controller
 *
 * @property SlatesTask $SlatesTask
 * @property PaginatorComponent $Paginator
 */
class SlatesTasksController extends AppController {
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
		public function beforeFilter() {
        $this->Auth->allow('notificationTaskActualDayFinish',"notificationTaskDiaSiguiente","notificationTaskVencida");
        parent::beforeFilter();  
    }

    public function notificationTaskActualDayFinish(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('SlatesTasks');
		$this->loadModel('Slate');
		$conditions = array(
				 
				'SlatesTask.deadline_date  ='=>  date("Y-m-d"),
                "SlatesTask.state" => "1",
		);
		$slates = $this->SlatesTask->find("all",compact("conditions","recursive"));
		foreach ($slates as $key => $value) {
			 $infoNotification = array(
							"email" => $value["User"]["email"],
							"date" => $value["SlatesTask"]["deadline_date"],
							"compromiso" => $value["SlatesTask"]["description"],
							"lista" => $value["Slate"]["name"]
						);
				$this->task_notifications($infoNotification,"Tarea que debes terminar hoy"); 
		}
	}
	public function notificationTaskVencida(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('SlatesTasks');
		$this->loadModel('Slate');
		$group      = array('SlatesTask.id');
		$conditions = array(	 
			'SlatesTask.deadline_date  <' => date("Y-m-d"),
                	'SlatesTask.state' 	      => '1',
                	'UserTeam.user_state'         => Configure::read('DISABLED'),
		);
		$this->SlatesTask->bindUserTeam();
		$slates = $this->SlatesTask->find("all",compact("conditions","recursive",'group'));
		foreach ($slates as $key => $value) {
			$infoNotification = array(
				"email"      => $value["User"]["email"],
				"date" 	     => $value["SlatesTask"]["deadline_date"],
				"compromiso" => $value["SlatesTask"]["description"],
				"lista"      => $value["Slate"]["name"]
			);
			$this->task_notifications($infoNotification,"Tarea vencida"); 
		}
	}

	public function notificationTaskDiaSiguiente(){
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('SlatesTasks');
		$this->loadModel('Slate');
		$conditions = array(
				 
				'SlatesTask.deadline_date  ='=>  date("Y-m-d",strtotime(date("Y-m-d")."+ 1 days")),
                "SlatesTask.state" => "1",
		);
		$slates = $this->SlatesTask->find("all",compact("conditions","recursive"));
		foreach ($slates as $key => $value) {
			$infoNotification = array(
							"email" => $value["User"]["email"],
							"date" => $value["SlatesTask"]["deadline_date"],
							"compromiso" => $value["SlatesTask"]["description"],
							"lista" => $value["Slate"]["name"]
						);
			$this->task_notifications($infoNotification,"Tarea que debes terminar mañana"); 
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SlatesTask->recursive = 0;
		$this->set('slatesTasks', $this->Paginator->paginate());
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SlatesTask->exists($id)) {
			throw new NotFoundException(__('Invalid slates task'));
		}
		$options = array('conditions' => array('SlatesTask.' . $this->SlatesTask->primaryKey => $id));
		$this->set('slatesTask', $this->SlatesTask->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id) {
		$this->layout = false;
		$this->autoRender = false;
		$id = EncryptDecrypt::decrypt($this->request->data["SlatesTask"]["slateId"]);
		$this->loadModel('Slate');
		 $conditions = array(
                "Slate.id" => $id
        );
        $Slate = $this->Slate->find("first",compact("conditions"));
        $this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
		if ($this->request->is('post')){
			$this->request->data["SlatesTask"]["slate_id"] = $id;
			$this->request->data["SlatesTask"]["state"] = 1;
			$this->request->data["SlatesTask"]["date_notidication"] = $this->request->data["SlatesTask"]["deadline_date"];
			$this->SlatesTask->create();
			if ($this->SlatesTask->save($this->request->data)) {
				$this->loadModel('User');
				$conditions = array(
					'User.id' => $this->request->data["SlatesTask"]["user_id"],
				);
				$emailNotification = $this->User->find("first",compact("conditions"));
				$infoNotification = array(
							"email" => $emailNotification["User"]["email"],
							"date" => $this->request->data["SlatesTask"]["deadline_date"],
							"compromiso" => $this->request->data["SlatesTask"]["description"],
							"lista" => $Slate["Slate"]["name"]
						);
				$this->task_notifications($infoNotification,"Nueva Tarea");
				return "1";
				 
			} else {
				  // $errors = $this->SlatesTask->validationErrors;
				    
				  return "0";
			}
		}
		 
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		
		 $id = EncryptDecrypt::decrypt($id);

	 
        $this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
 		$this->set(compact("projects","Slate"));
		 
		if (!$this->SlatesTask->exists($id)) {
			throw new NotFoundException(__('Invalid slates task'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data["SlatesTask"]["id"] = $id;
			$slateId = $this->request->data["SlatesTask"]["slate_id"];
			if ($this->SlatesTask->save($this->request->data)) {
				echo $id;
				$this->loadModel('Slate');
				 $conditions = array(
		                "SlatesTask.id" => $id
		        );
		        $Slate = $this->SlatesTask->find("first",compact("conditions"));
		         
				$this->loadModel('User');
				$conditions = array(
					'User.id' => $this->request->data["SlatesTask"]["user_id"],
				);
				$emailNotification = $this->User->find("first",compact("conditions"));
				$infoNotification = array(
							"email" => $emailNotification["User"]["email"],
							"date" => $this->request->data["SlatesTask"]["deadline_date"],
							"compromiso" => $this->request->data["SlatesTask"]["description"],
							"lista" => $Slate["Slate"]["name"]
						);
				$this->task_notifications($infoNotification,"Tarea Editada");



				$this->Flash->success(__('Tarea editada correctamente'));
				 
				return $this->redirect(array("controller"=>"slates",'action' => 'view/'.EncryptDecrypt::encrypt($slateId)));
			} else {
				$this->Flash->error(__('The slates task could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SlatesTask.' . $this->SlatesTask->primaryKey => $id));
			$this->request->data = $this->SlatesTask->find('first', $options);
		}
		$this->list_permission();
        //LISTADO DE USUARIOS
        $this->loadModel('SlatesUser');
        $this->SlatesUser->recursive = 0;
        $conditions = array(
                "SlatesUser.slate_id" => $this->request->data["SlatesTask"]["slate_id"]
        );
        $users = $this->SlatesUser->find("all",compact("conditions","recursive"));
        $listUsers = array();
        foreach ($users as $key => $value) {
        	$listUsers  += array(
        			$value["User"]["id"] => $value["User"]["firstname"]." ".$value["User"]["lastname"]
        			);
        }
        $users = $listUsers;
        $this->set(compact("users"));


        
		
	}


	public function edit_notification($id = null) {
		 $id = EncryptDecrypt::decrypt($id);
		if (!$this->SlatesTask->exists($id)) {
			throw new NotFoundException(__('Invalid slates task'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data["SlatesTask"]["id"] = $id;
			$slateId = $this->request->data["SlatesTask"]["slate_id"];
			 
			
			if ($this->SlatesTask->save($this->request->data)) {
				$this->Flash->success(__('Tarea editada correctamente'));
				 
				return $this->redirect(array("controller"=>"slates",'action' => 'view/'.EncryptDecrypt::encrypt($slateId)));
			} else {
				$this->Flash->error(__('The slates task could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SlatesTask.' . $this->SlatesTask->primaryKey => $id));
			$this->request->data = $this->SlatesTask->find('first', $options);
        	$this->request->data['SlatesTask']['date_notidication'] = substr($this->request->data['SlatesTask']['date_notidication'],0,-3);
		}
		$this->list_permission();
        //LISTADO DE USUARIOS
        $this->loadModel('SlatesUser');
        $this->SlatesUser->recursive = 0;
        $conditions = array(
                "SlatesUser.slate_id" => $this->request->data["SlatesTask"]["slate_id"]
        );
        $users = $this->SlatesUser->find("all",compact("conditions","recursive"));
        $listUsers = array();
        foreach ($users as $key => $value) {
        	$listUsers  += array(
        			$value["User"]["id"] => $value["User"]["firstname"]." ".$value["User"]["lastname"]
        			);
        }
        $users = $listUsers;
        $this->set(compact("users"));
        $this->loadModel('Project');
		$this->Project->recursive = 2;
 		$projects =  $this->Project->query("SELECT P.id, CONCAT('(',C.name,') ',P.name) as name from projects as P JOIN clients as C ON P.client_id = C.id");
 		$projects = Hash::combine($projects, '{n}.P.id', '{n}.0.name') ;
 		$this->set(compact("projects"));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SlatesTask->id = $id;
		if (!$this->SlatesTask->exists()) {
			throw new NotFoundException(__('Invalid slates task'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SlatesTask->delete()) {
			$this->Flash->success(__('The slates task has been deleted.'));
		} else {
			$this->Flash->error(__('The slates task could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	private function list_permission(){
        $this->loadModel('UserTeam');
        $this->UserTeam->recursive = 0;
        $conditions = array(
                'UserTeam.user_id' => AuthComponent::user("id"),
                "UserTeam.team_id" => Configure::read("Application.team_id")
        );
        $userPosition = $this->UserTeam->find("first",compact("conditions","recursive"));
        $this->set(compact("userPosition"));

       }

       private function task_notifications($commitments_info,$subject){
		$template = 'task_notification';
		$emails = array("johandiaz@strategee.us");
				$subject  = sprintf(__($subject." - Taskee"));
				$options = array(
					'to'       => $commitments_info["email"],
					'template' => $template,
					'subject'  => Configure::read('Application.name'). ' - '.$subject,
					'vars'     =>  array('info'=>$commitments_info),
				);
				$this->sendMail($options);  
				return true;
	}






}


