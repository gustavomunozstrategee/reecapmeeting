<?php
App::uses('AppController', 'Controller');
 
class TagsController extends AppController {
 
	public $components = array('Paginator');
 
	public function index() {
		$this->validateTeamSession();
		$this->check_team_permission_action(array("index"), array("tags")); 
		$teams        = $this->loadTeams();
		// $teamIds      = $this->getTeamIds();
		$teamIds      = EncryptDecrypt::decrypt($this->Session->read("TEAM"));
		$limit        = 10;
        $order        = array('Tag.modified'=>'DESC'); 
        $conditions   = array('Team.id' => $teamIds);  
        $conditions[] = $this->Tag->buildConditions($this->request->query);
        $this->Paginator->settings = compact('conditions','limit','order');
        $tags         = $this->Paginator->paginate(); 
        $this->set(compact('tags','teams')); 
	}
 
	public function view($id = null) {
		$id = EncryptDecrypt::decrypt($id);
		if (!$this->Tag->exists($id)) {
			$this->showMessageExceptions();
		}
		$conditions   = array("Tag.id" => $id);
		$tag          = $this->Tag->find('first', compact("conditions")); 
		$this->check_team_permission_action(array("view"), array("tags"), $tag["Team"]["id"]);  
		$fields       = array("Tag.meeting_id");
		$meetingsIds  = $this->Tag->find("list", compact("fields","conditions"));
		$recursive    = 0;
		$conditions   = array("Meeting.id" => $meetingsIds);
		$limit        = 10;
		$order        = array('Meeting.modified DESC');
		$this->Paginator->settings = compact('conditions','limit','recursive','order');
        $meetings     = $this->Paginator->paginate($this->Tag->Meeting);  
	 	$this->set(compact('tag','meetings')); 
	}  

	public function listTags(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$recursive        = -1;
		$fields           = array("DISTINCT Tag.name");
		$tags             = $this->Tag->find("all", compact("fields","recursive"));
		$tagsJson         = array();
		foreach ($tags as $tag) {
			$tagsJson[] = $tag["Tag"]["name"];
		} 
		$this->outJSON($tagsJson);  
	}


	public function list_tags_meeting_edit(){
		if (!$this->request->is('ajax')) {
			$this->showMessageExceptions();
		} 
		$this->layout     = 'ajax';
		$this->autoRender = false;
		$meetingId        = $this->request->query["meeting_id"];
		$meetingId        = EncryptDecrypt::decrypt($meetingId);
		$recursive        = -1;
		$fields           = array("DISTINCT Tag.name");
		$tags             = $this->Tag->find("all", compact("fields","recursive"));
		$tagsJson         = array();
		$tagsOfMeeting    = array();
		$conditions       = array("Tag.meeting_id" => $meetingId);
		$tagsMeetingEdit  = $this->Tag->find("all", compact("conditions","recursive","fields"));
		foreach ($tags as $tag) {
			$tagsJson[] = $tag["Tag"]["name"];
		} 
		foreach ($tagsMeetingEdit as $tag) {
			$tagsOfMeeting[] = $tag["Tag"]["name"];
		} 
		$tagsInfo = array("tagsJson" => $tagsJson, "tagsOfMeeting" => $tagsOfMeeting);
		$this->outJSON($tagsInfo); 
	}
}
