<?php
App::uses('AppModel', 'Model');

class Transaction extends AppModel {

    public $displayField = 'name';

    public $belongsTo = array(
        'User'   => array('className' => 'User','foreignKey'   => 'user_id',), 
        'Plan'   => array('className' => 'Plan','foreignKey'   => 'plan_id',)
    );

    public $hasMany = array(
        'Payment' => array('className' => 'Payment','foreignKey' => 'transaction_id','dependent' => false,)
    );


    public function buildConditions($params=array()) {
        $conditions = array();
        if(!empty($params['q'])) {
            $params['q'] = trim($params['q']);
            $params['q'] = strtolower($params['q']);
            $conditions['OR'] = array(
                'LOWER(Transaction.value) LIKE'=>"%{$params['q']}%",
            );
        }
        return $conditions;
    }

    public function createTransactionInProgressToPlanToChange($planInfo = array(), $referenceCode){ 
        $userId = AuthComponent::user('id');
        $out = array( 
            "value"          => $planInfo["plan_value_new"],
            "referenceCode"  => $referenceCode,
            "state"          => configure::read('TRANSACTION_PENDING'), 
            "email"          => AuthComponent::user('email'),
            "user_id"        => AuthComponent::user('id'),
            "name"           => AuthComponent::user('firstname') . ' ' . AuthComponent::user('lastname'),
            "notify"         => configure::read("ENABLED"),
            "plan_id"        => $planInfo["plan_id_new"]
        ); 
        $this->save($out);
        Cache::write("plans_{$userId}", $planInfo, "PLANS");
        Cache::delete("transactions_{$userId}",'TRANSACTION_PENDINGS'); 
    } 

}