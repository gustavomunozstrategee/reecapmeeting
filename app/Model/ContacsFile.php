<?php
App::uses('AppModel', 'Model');

class ContacsFile extends AppModel {

	public $actsAs = array(
	   'Upload.Upload' => array(
	       'img'         => array(
	         'pathMethod'   => 'flat',
	         'nameCallback' => 'renameFile',
	         'path'         => '{ROOT}{DS}webroot{DS}files{DS}Contac{DS}',
	       )
	    )
	);

	public $belongsTo = array(
		'Contac' => array('className' => 'Contac','foreignKey' => 'contac_id',)
	);

}
