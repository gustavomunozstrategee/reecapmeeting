<?php
App::uses('AppModel', 'Model');

class ContacsDocument extends AppModel {

	public $actsAs = array(
	   'Upload.Upload' => array(
	       'document'         => array(
	         'pathMethod'   => 'flat',
	         'nameCallback' => 'renameFile',
	         'path'         => '{ROOT}{DS}webroot{DS}document{DS}Contac{DS}',
	       )
	    )
	);


	public $belongsTo = array(
		'Contac' => array('className' => 'Contac','foreignKey' => 'contac_id',)
	);

}
