<?php
App::uses('AppModel', 'Model');

class UsersFile extends AppModel {


	public $belongsTo = array(
		'User' => array(
			'className'  => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		)
	);

	public $actsAs = array(
      'Upload.Upload' => array(
	      'file_name' => array(
	      	'pathMethod'   => 'flat',
	      	'nameCallback' => 'mobileRenameFile',
	      	'path'         => '{ROOT}{DS}webroot{DS}files{DS}UsersFiles{DS}',
	      ),
      	) 
    );

   	/*
	* Optimizar las imagenes que se suben
	* @param $user: array - datos del usuario que quiere actualizar la imagen
	* @return void
  	*/
	public function optimizeProfileImage($data=array()) {
		try{
			if(!empty($data['UsersFile']['file_name']) && !empty($data['UsersFile']['type'])) {
				$path = APP.'webroot'.DS.'files'.DS.'UsersFiles'.DS;
				$path = $path.$data['UsersFile']['file_name'];
				$typeFile = strtolower($data['UsersFile']['type']);
				
				App::uses('File', 'Utility');
				$File = new File($path);
				$mime = $File->mime();
				if(in_array($typeFile, Configure::read('IMAGE_ASSET_TYPES')) || in_array($mime, Configure::read('IMAGE_ASSET_TYPES'))) {
					if(file_exists($path)) {
						App::import('Vendor', 'Picture', array('file' => 'Picture.php'));
						$Picture = new Picture();
						$target  = $Picture->resizeCrop($path, 0, 0);
						if(file_exists($target)) {
							$target = explode(DS, $target);
							$image  = end($target);
							if($image != $data['UsersFile']['file_name']) {
								$nameContent = explode(".", $image);
								$ext = end($nameContent);

								$saveData = array('UsersFile' => array(
									'file_name' => $image,
									'file_extension' => $ext,
									'id' => $data['UsersFile']['id'],
								));

								$data['UsersFile']['file_name'] = $image;
								$data['UsersFile']['file_extension'] = $ext;
								$data['UsersFile']['extension'] = $ext;
								$this->save($saveData, array('validate'=>false));
							}
						}
					}
				}
			}
		} catch (Exception $e) {}
		return $data;
	}

}
