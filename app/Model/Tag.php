<?php
App::uses('AppModel', 'Model');
 
class Tag extends AppModel {
 
	public $belongsTo = array(
		'Meeting' => array(
			'className' => 'Meeting',
			'foreignKey' => 'meeting_id', 
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id', 
		),
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => false,
			'conditions' => array('Meeting.team_id = Team.id'), 
		)
	);

	public function saveTags($data = array(), $meetingId){ 
		if(!empty($data["Meeting"]['tag'])){
	 		$tagsAdded     = explode(",",$data["Meeting"]['tag']);
	 		$conditions    = array("Tag.name" => $tagsAdded);
	 		$fields        = array("DISTINCT Tag.name");
	 		$recursive     = -1;
	 		$tags          = $this->find("all", compact("conditions","recursive","fields"));
	 		$tagsExists    = Set::combine($tags, '{n}.Tag.name', '{n}.Tag.name');
            $tagsNotExists = array_diff($tagsAdded, $tagsExists); 
            if(!empty($tagsNotExists)){
            	$this->__saveTagsNews($tagsNotExists, $meetingId);
            }
            if(!empty($tagsExists)){
            	$this->__updateTagsWithMeeting($tagsExists, $meetingId);
            } 
		} 
	}

	private function __updateTagsWithMeeting($tagsExists = array(), $meetingId){		
		foreach ($tagsExists as $tagId => $tagName) {
		 	$tagsUpdate[] = array( 
		 		"name"       => $tagName,
		 		"meeting_id" => $meetingId,
		 		"user_id"    => Authcomponent::user("id")
		 	); 
		}
		if(!empty($tagsUpdate)){
			$this->saveAll($tagsUpdate);
		} 
	}

	private function __saveTagsNews($tagsNotExists = array(), $meetingId){ 
		$tagsNews = array(); 
		foreach ($tagsNotExists as $key => $tagName) {
		 	$tagsNews[] = array(
		 		"name"       => $tagName,
		 		"meeting_id" => $meetingId,
		 		"user_id"    => Authcomponent::user("id")
		 	); 
		}
		if(!empty($tagsNews)){
	 		$this->saveAll($tagsNews);
	 	}
	}

	public function saveTagsFollowingMeetings($data = array(), $meetingIds){
		if(!empty($data["Meeting"]['tag'])){
	 		$tagsAdded     = explode(",",$data["Meeting"]['tag']);
	 		$conditions    = array("Tag.name" => $tagsAdded);
	 		$fields        = array("DISTINCT Tag.name");
	 		$recursive     = -1;
	 		$tags          = $this->find("all", compact("conditions","recursive","fields"));
	 		$tagsExists    = Set::combine($tags, '{n}.Tag.name', '{n}.Tag.name');
            $tagsNotExists = array_diff($tagsAdded, $tagsExists); 
            if(!empty($tagsNotExists)){
            	$this->__saveTagsNewsFollowing($tagsNotExists, $meetingIds);
            }
            if(!empty($tagsExists)){
            	$this->__updateTagsWithMeetingFollowing($tagsExists, $meetingIds);
            } 
		} 
	}

	private function __saveTagsNewsFollowing($tagsNotExists = array(), $meetingIds){ 
		$tagsNews = array();
		foreach ($meetingIds as $key => $meetingId) { 
			foreach ($tagsNotExists as $key => $tagName) {
			 	$tagsNews[] = array(
			 		"name"       => $tagName,
			 		"meeting_id" => $meetingId,
			 		"user_id"    => Authcomponent::user("id")
			 	); 
			}
		}
		if(!empty($tagsNews)){ 
	 		$this->saveAll($tagsNews);
	 	}
	}

	private function __updateTagsWithMeetingFollowing($tagsExists = array(), $meetingIds){		
		foreach ($meetingIds as $key => $meetingId) {
			foreach ($tagsExists as $tagId => $tagName) {
			 	$tagsUpdate[] = array( 
			 		"name"       => $tagName,
			 		"meeting_id" => $meetingId,
			 		"user_id"    => Authcomponent::user("id")
			 	);  
			}			 
		}
		if(!empty($tagsUpdate)){
			$this->saveAll($tagsUpdate);
		} 
	}

	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				'LOWER(CONVERT(Tag.name USING latin1)) LIKE' => "%{$params['q']}%", 
			); 
		}
		if(!empty($params['teams'])) {
			$conditions['Team.id'] = $params['teams'];
		}
		return $conditions;
	} 
}
