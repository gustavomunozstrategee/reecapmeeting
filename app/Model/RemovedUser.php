<?php
App::uses('AppModel', 'Model');
 
class RemovedUser extends AppModel {
 
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'OwnUser' => array(
			'className' => 'User',
			'foreignKey' => 'own_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = trim($params['q']);
			$params['q'] = strtolower($params['q']);
			$conditions = array('OR' => array(
				'Team.name LIKE'      => "%{$params['q']}%",  
				'User.email LIKE'     => "%{$params['q']}%",  
			));
		}  
		return $conditions;
	}
}
