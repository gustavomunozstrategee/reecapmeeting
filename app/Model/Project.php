<?php
App::uses('AppModel', 'Model');

class Project extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'img' => array(
                'pathMethod'   => 'flat',
                'nameCallback' => 'renameFile',
                'path' 		   => '{ROOT}{DS}webroot{DS}files{DS}Project{DS}',
              	'deleteOnUpdate'       => true,
		     	'deleteFolderOnDelete' => false,
            )
        )
    );

	public $validate = array(
		'name' => array('notBlank' => array('rule' => array('notBlank'),'message' => 'El nombre del proyecto es requerido.'),
			array('rule'   => 'validateProjectExistInClient',
				'required' => true,
				'on'       => 'create', 
				'message'  => 'El nombre del proyecto digitado ya esta registrado con el cliente seleccionado.'
			),

			array('rule'   => 'validateEditProjectExistInClient',
				'required' => true,
				'on'       => 'update', 
				'message'  => 'El nombre del proyecto ya esta asociado a otro cliente.'
			),
		),
		'description' => array(
			array('rule' => 'notBlank','message' => 'La descripción es requerida.','required' => true),
			array('rule' => 'validateTextareaLenght','required' => true,'message'  => 'La descripción debe tener máximo 300 caracteres.'),  
		),
		'number' => array(
			array(
				'rule'     => 'notBlank',
				'required' => true,
				'message'  => 'El número del acta es requerido.',
				'on'       => 'create'
			),
			array(
				'rule'     => 'numeric',
				'required' => true,
				'message'  => 'El número del acta debe ser numérico.',
				'on'       => 'create'
			),
			array(
				'rule'     => 'validateNumberContac',
				'required' => true,
				'on'       => 'create',
				'message'  => 'El número del acta debe ser mayor a cero.'
			)
		),
		'client_id' => array('numeric' => array('rule' => array('numeric'),'message' => 'El cliente es requerido.'),
		),
		'img' => array(       
			'checkImgProject' => array(
                'rule'    => 'checkImgProject',
                'message' => 'La imagen del proyecto es requerida.',
                'on'      => 'create',
            ),    
            'isUnderPhpSizeLimit' => array(
                'rule'    => 'isUnderPhpSizeLimit',
                'message' => 'El archivo excede el límite de tamaño de archivo de subida.',
            ), 
            'isValidMimeType' => array(
                'rule'    => array('isValidMimeType', array('image/jpeg', 'image/png', 'image/gif', 'image/jpg'), false),
                'message' => 'La imagen no es jpg, gif ni png.',
            ),
            'isBelowMaxSize' => array(
                'rule'    => array('isBelowMaxSize', 10485760),
                'message' => 'El tamaño de imagen es demasiado grande. Solo se permiten imágenes de tamaño de 10MB.'
            ), 
            'isValidExtension' => array( 
                'rule'    => array('isValidExtension', array('jpg', 'png', 'jpeg','gif'), false),
                'message' => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            ), 
            'validateMimeType' => array(
               'rule'       => 'validateMimeType', 
               'message'    => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            )
        ), 
		// 'users' => array(
		// 	array('rule' => 'validateUsers','required' => true,'message'  => 'Se debe seleccionar mínimo un usuario.'),  
		// ),
	);

	public function checkImgProject($data){
		if($data["img"]["error"] == 4){
			return false;
		} else {
			return true;
		}
	}

	public function validateUsers($data){		 
		if(!is_array($data['users'])){
			return false;
		} else {
			return true;  
		} 
	}

	public function validateMimeType($data){  
        return $this->validateMimeTypeImg($data);  
    }

	public $belongsTo = array(
		'Client' => array('className' => 'Client','foreignKey' => 'client_id'), 
		'Team'   => array('className' => 'Team','foreignKey' => 'team_id'),  
	);

	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));	
			$conditions['OR'] = array(
				'LOWER(CONVERT(Project.name USING latin1))         LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Project.description USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Client.name USING latin1))          LIKE' => "%{$params['q']}%", 
			); 
		}
		if(!empty($params['teams'])) {
			$conditions['Team.id'] = $params['teams'];
		}
		return $conditions;
	}

	public function bindContac() {
 		$binds = array('hasMany'=>array(
 			'Contac' => array('className' => 'Contac', 'conditions'=> array('Contac.state' => Configure::read("DISABLED")), 'foreignKey'=> 'project_id'), 
 		));
		$this->bindModel($binds);
 	}

 	public function bindContacNotDone() {
 		$binds = array('hasMany'=>array(
 			'Contac' => array('className' => 'Contac', 'conditions'=> array(), 'foreignKey'=> 'project_id'), 
 		));
		$this->bindModel($binds);
 	}

 	public function bindContacNumber() {
 		$binds = array('hasMany'=>array(
 			'Contac' => array('className' => 'Contac', 'conditions'=> array('Contac.state' => Configure::read("DISABLED")), 'foreignKey'=> 'project_id', 'fields' => 'COUNT("Contac") as total_contac'), 
 		));
		$this->bindModel($binds);
 	}

 	public function bindContacNumberNotDone() {
 		$binds = array('hasMany'=>array(
 			'NotContac' => array('className' => 'Contac', 'conditions'=> array('NotContac.state !=' => Configure::read("DISABLED")), 'foreignKey'=> 'project_id', 'fields' => 'COUNT("Contac") as total_contac_no_realizas'), 
 		));
		$this->bindModel($binds);
 	}

 	public function validateNumberContac($data){
		$value = isset($data['number']) ? $data['number'] : null; 
		if ($value > 0){
            return true;
       	} 
   	 	return false;
	}

	public function validateProjectExistInClient($data){ 
		$name            = isset($data['name'])     ? $data['name'] : null;
		$clientId        = isset($this->data["Project"]["client_id"]) ? $this->data["Project"]["client_id"] : null;
		$conditions      = array("Project.client_id" => $clientId, "Project.name" => $name ); 
		$projectInClient = $this->find("first", compact("conditions")); 
		if (!empty($projectInClient)) {
			return false;
		}
		return true; 
	}

	public function validateEditProjectExistInClient($data){ 
		$name      = isset($data['name']) ? $data['name'] : null;
		$clientId   = isset($this->data["Project"]["client_id"]) ? $this->data["Project"]["client_id"] : null;
		$id         = isset($this->data["Project"]["id"]) ? $this->data["Project"]["id"] : null;
		$conditions = array("Project.client_id" => $clientId, "Project.name" => $name); 
		$userEmployeeInClient = $this->find("first", compact("conditions"));
		if(!empty($userEmployeeInClient)){
		 	if ($userEmployeeInClient["Project"]["id"] == $id) { 
		 		return true;  
		 	} else {
		 		$conditions = array("Project.client_id" => $clientId, "Project.name" => $name);
		 		$userEmployeeInOtherClient = $this->find("first", compact("conditions"));
		 		if(!empty($userEmployeeInOtherClient)){ 
		 			return false;
		 		} else { 
		 			return true;
		 		} 
		 	}
		} else {
			return true; 
		} 
	}

	public function validExistenceProject($name,$client){
		$conditions = array('Project.name' => $name,'Project.client_id' => $client);	
		return $this->find('count',compact('conditions'));
	}

	public function findProjectsClientSelect($client_id){
		$conditions   = array('Project.client_id' => $client_id,'Project.state' => Configure::read('ENABLED'));
		return $this->find('list',compact('conditions'));
	}

	public function detaillProject($project_id){
		$conditions = array('Project.id' => $project_id);
		return $this->find('first', compact('conditions'));
	}

	public function validateTextareaLenght(){
		$text = str_replace(array("\r\n","\r"), "", $this->data["Project"]["description"]);
		if($text > 300){
			return false;
		} else {
			return true;
		}
	}

	public function getTotalProject(){
		$limit      = 1;
		$conditions = array("Project.user_id" => Authcomponent::user("id"));
		$count      = $this->find("count", compact("conditions","limit"));
		return $count;
	} 

	public function findProjectEmailCopies($data = array()) {
		$conditions = array('Project.id' => $data['Contac']['project_id']);
		$copies = $this->field('email_copies', $conditions);
		if(empty($copies) && !empty($data['Contac']['copies'])) {
			return $data['Contac']['copies'];

		}elseif(!empty($copies) && empty($data['Contac']['copies'])) {
			return $copies;

		}elseif(!empty($copies) && !empty($data['Contac']['copies'])) {
			return $copies.','.$data['Contac']['copies'];
		}else {
			return null;
		}
	}

}
