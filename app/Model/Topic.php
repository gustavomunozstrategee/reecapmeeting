<?php
App::uses('AppModel', 'Model');
 
class Topic extends AppModel { 

	public $validate = array( 
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'La descripción es requerida.', 
			),
		),
		'time' => array(
			'validateTimeValue' => array(
				'rule' => array('validateTimeValue'),
				'message' => 'El tiempo es requerido.',
			),
			'numeric' => array(
				'rule' => 'numeric',
	        	'message' => 'Solo se permiten números.'
        	)
		), 
	);
 
	public $belongsTo = array(
		'Meeting' => array(
			'className' => 'Meeting',
			'foreignKey' => 'meeting_id', 
		)
	);

	public function saveTopics($meetingId, $data){
		$topics = array();
		if (isset($data["Topic"]) || !empty($data["Topic"])) {
			foreach ($data["Topic"] as $topic) {
				if(!empty($topic["time"])){
					$topics[] = array(
						"meeting_id"  => $meetingId,
						"description" => $topic["description"],
						"time"        => $topic["time"]
					);
				} 
			} 
			if(!empty($topics)){
				$this->saveAll($topics);
			}
		} 
	}

	public function saveTopicsMeetingFollowing($topicsMeetings = array()){
		$topics = array();
		foreach ($topicsMeetings as $topicsMeeting) {
			if (!empty($topicsMeeting["Topics"])) {
				foreach ($topicsMeeting["Topics"] as $topic) {
					$topics[] = array(
						"meeting_id"  => $topicsMeeting["meeting_id"],
						"description" => $topic["description"],
						"time"        => $topic["time"]
					); 
				}
			}
		}
		if(!empty($topics)){
			$this->saveAll($topics);
		} 
	}

	public function validateTimeValue(){ 
		if($this->data["Topic"]["time"] == 0 || $this->data["Topic"]["time"] == "NaN"){
			return false;
		} else {
			return true;
		}
	}

	//METODO GLOBAL PARA EDITAR LOS TEMAS ESPECIFICOS
	public function editTopicsMeeting($data = array()){
		$this->deleteAll(array('Topic.meeting_id' =>  $data["Meeting"]["id"]), false);
		$topics = array();
		if (isset($data["Topic"]) || !empty($data["Topic"])) {
			foreach ($data["Topic"] as $topic) {
				if(!empty($topic["time"])){
					$topics[] = array(
						"meeting_id"  => $data["Meeting"]["id"],
						"description" => $topic["description"],
						"time"        => $topic["time"]
					);
				} 
			} 
			if(!empty($topics)){
				$this->saveAll($topics);
			}
		} 
	}
}
