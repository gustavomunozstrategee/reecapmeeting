<?php
App::uses('AppModel', 'Model');

class ProjectsPrivacity extends AppModel {

	public $useTable = 'projects_privacity'; 

	public $belongsTo = array(
		'Project' => array('className' => 'Project','foreignKey' => 'project_id',),
		'User' 	  => array('className' => 'User','foreignKey' => 'user_id',)
	);
 
	public function saveUsers($users, $project_id, $action = null){ 
		$saveData = array();
		if(!empty($users)){
			foreach ($users as $user) {
			 	$saveData[] = array( 
			 		"user_id"      => $user,
			 		"project_id"   => $project_id, 
			 	);
			} 
		}
		if(is_null($action)){
			$saveData[] = array( 
		 		"user_id"      => AuthComponent::user("id"),
		 		"project_id"   => $project_id, 
		 	); 
		}
		if(!empty($saveData)){
			$this->saveMany($saveData, array('deep' => true)); 
		}
	}

	public function checkUserRemoved($usersExists, $data){
		$usersExist   = Set::combine($usersExists, '{n}.ProjectsPrivacity.user_id', '{n}.ProjectsPrivacity.user_id');
		$removedUsers = array_filter(array_diff($usersExist, $data["Project"]["users"]));
 	 	if(!empty($removedUsers)){ 
			foreach ($removedUsers as $userId) {
				$this->deleteAll(array('ProjectsPrivacity.project_id' => $data["Project"]["id"], 'ProjectsPrivacity.user_id' => $userId),false);		 
			} 
		} 
	}

	public function getPrivacityContacs(){
		$conditions = array('User.id' => Authcomponent::user('id'));
		$privacitys = $this->find('all', compact('conditions'));
		$projectIds = array();
		$projects   = array();
		if (!empty($privacitys)) {
			$projectIds = Set::combine($privacitys,'{n}.Project.id','{n}.Project.id');	 
			foreach ($privacitys as $privacity) {
				$projects[$privacity['Project']['id']] = $privacity['Project']['name'];
			}
		} 
 		return array("projectIds" => $projectIds, "projects" => $projects);
	} 

	public function getPrivacityProjectContacs($project_id){
		$conditions = array("User.id" => Authcomponent::user("id"), "ProjectsPrivacity.project_id" => $project_id);
		$privacitys = $this->find("first", compact("conditions"));  
		$projectIds = array(); 
		if (!empty($privacitys)) {
			$projectIds = $privacitys["Project"]["id"];	 
		} 
 		return $projectIds;
	} 

}
