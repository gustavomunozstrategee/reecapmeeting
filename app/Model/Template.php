<?php
App::uses('AppModel', 'Model');

class Template extends AppModel {

	public $validate = array(
		'description' => array('notBlank' => array('rule' => array('notBlank'),'message' => 'La descripción es requerida.'),
		),
	);


	public $belongsTo = array(
		'User' => array('className' => 'User','foreignKey' => 'user_id',)
	);

	public function listTemplateEmpresa($teamId){
		$conditions 	  = array('Template.team_id' => $teamId);
		return $templates = $this->find('all',compact('conditions'));
	}

	public function dataTemplate($template_id){
		$conditions 	 = array('Template.id' => $template_id);
		return $template = $this->find('first',compact('conditions'));
	}

}
