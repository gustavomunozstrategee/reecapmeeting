<?php
App::uses('AppModel', 'Model'); 


class Contac extends AppModel {

	public $virtualFields = array('name'=> "Contac.description");

	public $validate = array(
		'team_id'     => array('notBlank' => array('rule' => array('notBlank'),'message' => 'La empresa es requerida.'),),
		'project_id'  => array('notBlank' => array('rule' => array('notBlank'),'message' => 'El proyecto es requerido.'),),
		'client_id'   => array('notBlank' => array('rule' => array('notBlank'),'message' => 'El cliente es requerido.'),),
		'description' => array('notBlank' => 
			array('rule' => array('notBlank'),'message' => ''),

		),
		'description' => array(
			array('rule' => 'notBlank','message' => 'La descripción es requerida.'),
			array('rule' => 'checkDescriptionCkEditorEmpty','message'  => 'La descripción es requerida.'),
			// array('rule' => 'checkLengthCharacter','message'  => 'Cada palabra de la descripción del acta debe tener máximo 80 caracteres.'),
		), 
		'start_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'La fecha inicio es requerida.', 
			),
			'validateEqualDatesMeeting' => array(
				'rule' => array('validateEqualDatesMeeting'),
				'message' => 'La fecha inicio y la fecha fin no pueden ser iguales.', 
			), 
			'validateStartDate' => array(
				'rule' => array('validateStartDate'),
				'message' => 'La fecha inicio no puede ser mayor a la fecha actual.', 
			), 
		),
		'end_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'La fecha fin es requerida.', 
			),
			'validateEndDateWithStartDate' => array(
				'rule' => array('validateEndDateWithStartDate'),
				'message' => 'La fecha fin no puede ser menor que la fecha inicio.', 
			) 
		),
		'funcionarios' => array(
			array('rule' => 'userChoose','message' => 'Debes seleccionar mínimo un colaborador.')
		)
	);

	public $belongsTo = array(
		'User'       => array('className' => 'User','foreignKey' => 'user_id',),
		'Client'     => array('className' => 'Client','foreignKey' => 'client_id',),
		'Project'    => array('className' => 'Project','foreignKey' => 'project_id',),
		'Team'  	 => array('className' => 'Team','foreignKey'  => 'team_id',) 
	);

	public $hasMany = array(
		'ContacsFile' => array(
			'className' => 'ContacsFile',
			'foreignKey' => 'contac_id',
			'dependent' => false
		),
		'ContacsDocument' => array(
			'className' => 'ContacsDocument',
			'foreignKey' => 'contac_id',
			'dependent' => false
		),
		'Commitment' => array(
			'className' => 'Commitment',
			'foreignKey' => 'contac_id',
			'dependent' => false
		),
		'Assistant' => array(
			'className' => 'Assistant',
			'foreignKey' => 'contac_id',
			'dependent' => false
		),
		'ApprovalContac' => array(
			'className' => 'ApprovalContac',
			'foreignKey' => 'contac_id',
			'dependent' => false
		),
		'CommentContac' => array(
			'className' => 'CommentContac',
			'foreignKey' => 'contac_id',
			'dependent' => false
		),
		'QualificationContact' => array(
			'className' => 'QualificationContact',
			'foreignKey' => 'contac_id',
			'dependent' => false
		),
	);

	public $actsAs = array(
    	'Upload.Upload' => array(
		      'file_white_brand' => array(
		      	'pathMethod'           => 'flat',
		      	'nameCallback'         => 'renameFile',
		      	'path'                 => '{ROOT}{DS}webroot{DS}document{DS}WhiteBrand{DS}',
		      	'deleteOnUpdate'       => true,
		        'deleteFolderOnDelete' => true, 
			)
      	)
    );

    public function bindCommentContac() {
 		$binds = array('hasMany'=>array(
 			'CommentContac' => array('className' => 'CommentContac', 'foreignKey'=> 'contac_id', 'limit' => 1), 
 		));
		$this->bindModel($binds);
 	}

    public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				'LOWER(CONVERT(Project.name USING latin1))   LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Client.name USING latin1))    LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Contac.number USING latin1))  LIKE' => "%{$params['q']}%", 
			); 
		}
		if(!empty($params['teams'])) {
			$conditions['Contac.team_id'] = $params['teams'];
		}
		if(!empty($params['project_id'])) {
			$conditions['Contac.project_id'] = $params['project_id'];
		}
		return $conditions;
	}

	public function validateStartDate(){
		$dateInitial = date("Y-m-d", strtotime($this->data["Contac"]["start_date"])); 
		if($dateInitial > date("Y-m-d")){
			return false;
		} else {
			return true;
		} 
	}

	public function saveFirebase($identificador = null, $datos = array()){
		if($identificador != null){
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false));
	    	$HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.contact').$identificador.'.json', json_encode($datos));
	    }
    }

    public function updateFirebase($identificador = null, $datos = array()){
    	if($identificador != null){ 
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false));
    	 	$HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.contact').$identificador.'.json', json_encode($datos));
	    }
    }


    public function getFirebase($identificador = null, $datos = array()){
    	if($identificador != null){ 
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false));
    	 	$result = $HttpSocket->get(Configure::read('Firebase.Url').Configure::read('Firebase.contact').$identificador.'.json');

    	 	return $result->body;
	    }
    }

    public function updateFirebaseContac($identificador = null, $campo, $dato){
    	if($identificador != null){ 
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false));
    	 	$HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.contact').$identificador.'/'.$campo.'.json', json_encode($dato));
	    }
    }

    public function deleteFirebase($identificador = null){
    	if ($identificador != null) {
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false));
    		$HttpSocket->delete(Configure::read('Firebase.Url').Configure::read('Firebase.contact').$identificador.'.json');
    	}
    }

    public function numberContac($project_id = null, $contacId){
    	$order      = array("Contac.id DESC");
		$conditions = array('Contac.project_id' => $project_id, "Contac.state" => Configure::read("DISABLED"), "NOT" => array("Contac.id" => $contacId));
		$fields     = array('MAX(Contac.number) AS number');
		$number     = $this->find("first", compact("conditions","order","fields")); 
		if(!empty($number["0"]["number"]) || $number["0"]["number"] > 0){
			return $number["0"]["number"] + 1;
		} else {
			$conditions = array("Project.id" => $project_id);
			$project    = $this->Project->find("first", compact("conditions"));
			return $project["Project"]["number"]; 
		}
	}

	public function codigoFirebase($id = null){
        $conditions = array('Contac.id' => $id);
        $identificador = $this->find('first', compact('conditions'));
        return $identificador['Contac']['firebase'];
	}

	public function getCommiments($contactId){
		$this->Commitment->unbindModel(array('belongsTo' => array('Contac','Project','Client')));
		$fields = array(
			"Commitment.id",
			"Commitment.delivery_date",
			"Commitment.description",
			"User.id","User.firstname",
			"User.lastname",
			"User.email"
 		);
		$conditions  = array("Commitment.contac_id" => $contactId);
		$commitments = $this->Commitment->find("all", compact("conditions","fields"));
		return $commitments;
	} 

	public function getCommimentsView($commitmentIds){
		$this->Commitment->unbindModel(array('belongsTo' => array('Contac','Project','Client','Team')));
		$conditions  = array("Commitment.id" => $commitmentIds);
		$commitments = $this->Commitment->find("all", compact("conditions"));
		return $commitments;
	} 

	public function getAllInfoToContacPDF($contacId){
		$this->unbindModel(array('hasMany' => array('Commitment','ApprovalContac','Assistant','CommentContac','QualificationContact'))); 
		$conditions      = array("Contac.id" => $contacId);
		$contac          = $this->find("first", compact("conditions"));  
		if(!empty($contac)){
			$users           = $this->Assistant->getAssistantsContacFinished($contac["Contac"]["id"]); 
			$commitments     = $this->getCommiments($contac["Contac"]["id"]); 
			$fields          = array("User.email","User.lang");
			$conditions      = array("User.id" => $users["usersId"]);
			$emailsUsers     = $this->User->find("all", compact("fields","conditions")); 
			$copies          = $this->__getCopiesEmailContac($contac);
			$listEmails      = array_filter(array_merge($emailsUsers, $copies));  
			return array(
				"contac"      => $contac,  
				"users"       => $users, 
				"commitments" => $commitments,
				"listEmails"  => $listEmails,
				"plan"        => $this->Team->checkPermissionInPlan($contac["Contac"]["team_id"])
			);
		} else {
			return array();
		}
	}

	private function __getCopiesEmailContac($contac){
		$copies 	= explode(',', $contac['Contac']['copies']); 
		$copiesInfo = array();
		if(!empty($copies["0"])){
			foreach ($copies as $email) {
				$copiesInfo[] = array(
					"User" => array("email" => $email, "lang" => "esp")
				); 
			}
			return $copiesInfo;
		} else {
			return array();
		}		
	}

	public function updateContac($contactId, $state){
		$recursive  = -1;
		$conditions = array("Contac.id" => $contactId);
		$contac     = $this->find("first", compact("conditions","recursive"));
		if($state == Configure::read("APPROVAL")){
			$contac["Contac"]["state"] = $state;
			$contac["Contac"]["approval"]    = Configure::read("ENABLED");
			$contac["Contac"]["in_approval"] = Configure::read("ENABLED");
			$contac["Contac"]["user_edit"]   = NULL;
		} else if ($state == Configure::read("ENABLED")) {
			$contac["Contac"]["state"]    = Configure::read("DISABLED");
			$contac["Contac"]["approval"] = NULL;
			$contac["Contac"]["in_approval"] = NULL;
		} 
		$contac["Contac"]["firebase"] = NULL; 
		$this->save($contac, array('validate' => false));
		if($contac["Contac"]["state"] == Configure::read("DISABLED")){
			$numberContac = $this->numberContac($contac["Contac"]["project_id"], $contac["Contac"]["id"]); 
			$contac["Contac"]["number"] = $numberContac;
			$this->save($contac, array('validate' => false));
		} 
	}

	public function validateInfoContac($contacId){
		$this->unbindModel(array('hasMany'   => array('ApprovalContac','QualificationContact','CommentContac'))); 
		$this->unbindModel(array('belongsTo' => array('Project','Client'))); 
		$conditions = array("Contac.id" => $contacId);
		$contac     = $this->find("first", compact("conditions"));
		if($contac["Contac"]["approval"] == Configure::read("ENABLED")){
			$users = $this->ApprovalContac->getUsers($contacId);
			if(empty($users)){
				$response = array("state" => false, "message" => __("Se debe seleccionar mínimo un usuario para solicitar la aprobación del acta."));
			} else if ($users["0"]["ApprovalContac"]["limit_date"] == NULL) {
				$response = array("state" => false, "message" => __("Se debe definir la fecha límite de la calificación."));
			} else {
				if(($contac["Contac"]["start_date"] == "" || $contac["Contac"]["start_date"] == "0000-00-00 00:00:00") || ($contac["Contac"]["end_date"] == "" || $contac["Contac"]["end_date"] == "0000-00-00 00:00:00")){
					$response = array("state" => false, "message" => __("La fecha inicio y la fecha fin del acta son requeridas."));

				} else if($contac["Contac"]["start_date"] == $contac["Contac"]["end_date"]){
					$response = array("state" => false, "message" => __("La fecha inicio y la fecha fin no pueden ser iguales."));

				} else if ($contac["Contac"]["end_date"] <= $contac["Contac"]["start_date"]){
					$response = array("state" => false, "message" => __("La fecha fin del acta no puede ser menor que la fecha inicio."));
					 
				} else if ($users["0"]["ApprovalContac"]["limit_date"] <= $contac["Contac"]["end_date"]){
					$response = array("state" => false, "message" => __("La fecha límite para calificar no debe ser menor o igual a la fecha fin del acta.")); 
				} else {
					$response = $this->__validateDataContacToFinished($contac); 
				} 
			} 
		} else { 
			$response = $this->__validateDataContacToFinished($contac);
		} 
		return $response;
	}
	 
	private function __validateDataContacToFinished($contac){
		$response         = array();
		$totalUsers       = 0; 
		$totalDate        = 0; 
		$totalDescription = 0;  
		$dateInitial 	  = date("Y-m-d", strtotime($contac["Contac"]["start_date"])); 
		if(!strlen(trim($contac["Contac"]["description"]))) {
			$response = array("state" => false, "message" => __("Aún se debe definir una descripción para el acta."));

		}  else if ($contac["Contac"]["start_date"] == "" || $contac["Contac"]["start_date"] == "0000-00-00 00:00:00") { 
			$response = array("state" => false, "message" => __("Aún se debe definir la fecha inicio para el acta."));

		}  else if ($dateInitial > date("Y-m-d")) {
			$response = array("state" => false, "message" => __("La fecha inicio no puede ser mayor que la fecha actual."));

		}  else if ($contac["Contac"]["end_date"] == "" || $contac["Contac"]["end_date"] == "0000-00-00 00:00:00") {
			$response = array("state" => false, "message" => __("Aún se debe definir la fecha fin para el acta."));

		} else if($contac["Contac"]["start_date"] == $contac["Contac"]["end_date"]){
			$response = array("state" => false, "message" => __("La fecha inicio y la fecha fin no pueden ser iguales."));

		} else if ($contac["Contac"]["end_date"] <= $contac["Contac"]["start_date"]){
			$response = array("state" => false, "message" => __("La fecha fin del acta no puede ser menor que la fecha inicio.")); 

		} else if ($contac["Contac"]["client_id"] == 0 || $contac["Contac"]["client_id"] == NULL) {
			$response = array("state" => false, "message" => __("Aún se debe definir el cliente para el acta."));

		} else if ($contac["Contac"]["project_id"] == 0 || $contac["Contac"]["project_id"] == NULL) {
			$response = array("state" => false, "message" => __("Aún se debe definir el proyecto para el acta."));

		} else if(empty($contac["Assistant"])){
			$response = array("state" => false, "message" => __("Aún se debe definir mínimo un asistente al acta."));

		} else if(!empty($contac["Assistant"])){
			if(!empty($contac["Commitment"])) {
				foreach ($contac["Commitment"] as $commitment) {
					if(empty($commitment["delivery_date"])) {
						$totalDate++;
					}
					if(!strlen(trim($commitment["description"]))) {
						$totalDescription++;
					} 
				}
				if($totalDate > 0) {
					$response = array("state" => false, "message" => __("Aún se debe definir la fecha límite del compromiso."));
				} else if($totalDescription > 0) {
					$response = array("state" => false, "message" => __("Aún se debe definir la descripción del compromiso."));
				} else {
					$status = $this->checkWordCharactersCommitments($contac["Commitment"]); 
	                if($status == false){
	                    $response = array("state" => false, "message" => __("Cada palabra de la descripción de los compromisos debe tener máximo 80 caracteres."));
	                } else {  
	                    $response = array("state" => true); 
	                } 
				}	 
			} else {
				$response = array("state" => true); 
			}
		} 
		if($response["state"] == true){
			$response = $this->__checkLength($contac["Contac"]["description"]);
		} 
		return $response; 
	}

	public function buildDataReminderApprovalContac($approvalContacs = array()){
		$userInfo = array(); 
		foreach ($approvalContacs as $approvalContac) { 
			if($approvalContac["ApprovalContac"]["limit_date"] == date("Y-m-d")){
				$userInfo[$approvalContac["User"]["email"]]["contac_id"][$approvalContac["ApprovalContac"]["contac_id"]]["limit_date"] = $approvalContac["ApprovalContac"]["limit_date"];
				$userInfo[$approvalContac["User"]["email"]]["contac_id"][$approvalContac["ApprovalContac"]["contac_id"]]["project"] = $approvalContac["Project"]["name"];
				$userInfo[$approvalContac["User"]["email"]]["contac_id"][$approvalContac["ApprovalContac"]["contac_id"]]["client"] = $approvalContac["Client"]["name"];
				$userInfo[$approvalContac["User"]["email"]]["User"]["id"]   = $approvalContac["User"]["id"];
				$userInfo[$approvalContac["User"]["email"]]["User"]["lang"] = $approvalContac["User"]["lang"];
				$userInfo[$approvalContac["User"]["email"]][] = array( 
					"user_id" => $approvalContac["User"]["id"] 
				);  
			} 
		}   
		return $userInfo;
	}

	public function calculateAverageContac($contacId){
	 	$this->ApprovalContac->updateAll(
            array(
            	'ApprovalContac.calification'   => 1,
            	'ApprovalContac.state' 		    => Configure::read("ENABLED")
        	), 
            array('ApprovalContac.contac_id'    => $contacId, "ApprovalContac.state" => Configure::read("DISABLED"))
        );			 
		$recursive   = -1;
		$group       = array("ApprovalContac.contac_id");
		$fields      = array("AVG(ApprovalContac.calification) AS AverageRating","ApprovalContac.contac_id");
		$conditions  = array("ApprovalContac.contac_id" => $contacId);
		$approvalAvg = $this->ApprovalContac->find("all", compact("conditions","recursive","group","fields"));
		foreach ($approvalAvg as $avg) {
			$this->ApprovalContac->updateAll(
	            array(
	            	'ApprovalContac.average_rating' => $avg["0"]["AverageRating"], 
	        	), 
	            array('ApprovalContac.contac_id'    => $avg["ApprovalContac"]["contac_id"])
        	);			 
		}		 
		return $this->__verifyContacApproveOrRefuse($contacId);
	}

	private function __verifyContacApproveOrRefuse($contacId = array()){
		$limit      = 10;
		$group      = array("ApprovalContac.contac_id");
		$fields     = array('ApprovalContac.*','COUNT(ApprovalContac.id) as TotalUsers','SUM(ApprovalContac.calification = 1) AS NumberApproveds');
		$recursive  = -1;
		$conditions = array("ApprovalContac.contac_id" => $contacId); 
		$approvals  = $this->ApprovalContac->find("all", compact("conditions","recursive","group","limit","fields"));
		$approveds  = array();
		$refuses    = array();
		foreach ($approvals as $approval) {
			if($approval["0"]["TotalUsers"] == $approval["0"]["NumberApproveds"]){
				$approveds[] = $approval["ApprovalContac"]["contac_id"];
			} else {
				$refuses[] = $approval["ApprovalContac"]["contac_id"];
			}			 
		}
  		return array("ContacsApproveds" => $approveds, "ContacsNotApproveds" => $refuses);
	}
  
 	public function updateContacApproved($contacIds = array()){
 		foreach ($contacIds as $contacId) {
	 		$this->updateAll(
	            array(
	            	'Contac.approval'    => NULL,
	            	'Contac.in_approval' => NULL,
	            	'Contac.firebase'    => NULL,
	            	'Contac.state'       => Configure::read("DISABLED")
	        	), 
	            array('Contac.id'     => $contacId)
	        ); 	
	 		$conditions   = array("Contac.id" => $contacId);
	 		$projectId    = $this->field("project_id", $conditions);
 			$numberContac = $this->numberContac($projectId, $contacId);   
	        $this->updateAll(array('Contac.number' => $numberContac), array('Contac.id' => $contacId)); 			 
 		} 
 	}

 	public function saveFirebaseContacRefuse($contacId){
		$this->unbindModel(array('hasMany' => array('Commitment','ApprovalContac','ContacsFile','ContacsDocument','QualificationContact')));  		
 		$conditions        = array("Contac.id" => $contacId);
 		$contac            = $this->find("first", compact("conditions"));
 		$commitments       = $this->getCommiments($contacId);
 		$approvalsUsers    = $this->getApprovalUsers($contacId);
 		$approvalsUserData = $this->__returnApprovalUsers($approvalsUsers);
 		$commitmentsData   = $this->__returnCommiments($commitments); 
 		$assistants        = $this->__returnAssistants($contac);
 		$dataToFirebase    = array("contac" => $contac, "approvalsUserData" => $approvalsUserData, "commitmentsData" => $commitmentsData, "assistants" => $assistants); 		
 		$listEmails        = $this->__getEmailsUserApprovals($approvalsUsers); 
 		$result 		   = $this->__buildDataInfoFirebase($dataToFirebase); 
 		if($contac["Contac"]["firebase"] == ""){
	 		$codFibase  = 'COD'.uniqid();
 			$contac["Contac"]["firebase"]    = $codFibase;
 			$contac["Contac"]["state"]       = Configure::read("ENABLED");
 			$contac["Contac"]["approval"]    = Configure::read("ENABLED");
 			$contac["Contac"]["in_approval"] = Configure::read("NEW_APPROVAL");
 			$this->save($contac, array('validate' => false));
	 		$this->saveFirebase($codFibase, $result);
 		}
 		return array("emailsApproval" => $listEmails, "userCreator" => $contac["User"]["email"]);
 	}

 	public function getApprovalUsers($contacId){
 		$conditions = array("ApprovalContac.contac_id" => $contacId);
 		$approvals  = $this->ApprovalContac->find("all", compact("conditions"));
 		return $approvals;	
 	}

 	private function __returnApprovalUsers($approvalsUsers = array()){
 		$approvalsUserData  = array();
 		$emailsApprovalUser = array();
 		$date = array();
 		foreach ($approvalsUsers as $approvalsUser) {
 			$date["date"] 		  = $approvalsUser["ApprovalContac"]["limit_date"]; 			 
			$approvalsUserData[]  = $approvalsUser["User"]["id"];
			$emailsApprovalUser[] = $approvalsUser["User"]["email"]; 			 
 		} 
 		return array("approvalsUsers" => $approvalsUserData, "dates" => $date, "emailsApproval" => $emailsApprovalUser);
 	}

 	private function __returnCommiments($commitments = array()){
 		$commitmentsData = array(); 
 		$i = 1;
 		foreach ($commitments as $commitment) { 			 
			$commitmentsData["commitments"][$i] = array(
				"asistente"   => $commitment["User"]["id"],
				"description" => $commitment["Commitment"]["description"],
				"fecha"       => $commitment["Commitment"]["delivery_date"],
				"id"   		  => $commitment["Commitment"]["id"],
			); 
			$i++;
 		} 
 		return $commitmentsData;
 	}

 	private function __returnAssistants($contac = array()){
 		$funcionarios = array();
 		$externos     = array();
 		if(!empty($contac["Assistant"])){
	 		foreach ($contac["Assistant"] as $assistant) {
 				if ($assistant["type_user"] == Configure::read("COLLABORATOR")) {
 					$funcionarios[] = $assistant["user_id"];
 				} else {
 					$externos[]     = $assistant["user_id"]; 					
 				}
	 		} 
	 		return array("funcionarios" => $funcionarios, "externos" => $externos);
 		}
 	}

 	private function __buildDataInfoFirebase($data = array()){ 
 		$dataToFirebase = array(
 			"id"                    => $data["contac"]["Contac"]["id"],
 			"approvalusers" 		=> !empty($data["approvalsUserData"]["approvalsUsers"]) ? $data["approvalsUserData"]["approvalsUsers"] : array(),
 			"client_id" 			=> $data["contac"]["Contac"]["client_id"],
 			"commitments"   		=> !empty($data["commitmentsData"]["commitments"]) ? $data["commitmentsData"]["commitments"] : array(),
 			"copies" 	    		=> $data["contac"]["Contac"]["copies"],
 			"description"   		=> $data["contac"]["Contac"]["description"],
 			"duration"      		=> $data["contac"]["Contac"]["duration"],
 			"end_date"      		=> $data["contac"]["Contac"]["end_date"],
 			"externos"      		=> $data["assistants"]["externos"],
 			"funcionarios"  		=> $data["assistants"]["funcionarios"],
 			"limit_date"   			=> $data["approvalsUserData"]["dates"]["date"],
 			"not_file_white_brand"  => $data["contac"]["Contac"]["file_white_brand"],
 			"file_white_brand"  	=> $data["contac"]["Contac"]["file_white_brand"],
 			"password"       	    => $data["contac"]["Contac"]["password"],
 			"project_id"  			=> $data["contac"]["Contac"]["project_id"],
 			"start_date"  			=> $data["contac"]["Contac"]["start_date"],
 			"team_id"  			    => $data["contac"]["Contac"]["team_id"],
 			"state"  				=> Configure::read("ENABLED"),
 			"timestap"  		    => uniqid(),
 			"approval"              => Configure::read("ENABLED"),
 			"user_id"  		   		=> $data["contac"]["Contac"]["user_id"],
 			"modified"  		    => $data["contac"]["Contac"]["modified"],
		);
		return $dataToFirebase;
 	}

 	private function __getEmailsUserApprovals($approvalsUserData = array()){
 		$emailsUserToApproved = array();
 		foreach ($approvalsUserData as $key => $approvalUser) { 
			$emailUser = $approvalUser["User"]["email"];
			$emailsUserToApproved[$emailUser] = array(
				"user_id" => $approvalUser["User"]["id"]
			); 
 		}
 		return $emailsUserToApproved; 
 	}

 	public function validateInfo($password = null, $comment, $contacId){
 		$recursive  = -1;
 		$conditions = array("Contac.id" => $contacId);
		$contac     = $this->find("first", compact("conditions","recursive"));
 		if(isset($password)){ 
			if(trim($password == "")){ 
				$response = array("state" => false, "message" => __("La contraseña es requerida."));
			} else if ($password != $contac["Contac"]["password"]) {
				$response = array("state" => false, "message" => __("Contraseña incorrecta."));				
			} else if (preg_match('/^\s*$/', $comment)) {
				$response = array("state" => false, "message" => __("El motivo es requerido."));
			} else {
				$response = array("state" => true);
			}
		} else {  
			if (preg_match('/^\s*$/', $comment)) { 
				$response = array("state" => false, "message" => __("El motivo es requerido."));
			} else { 
				$response = array("state" => true); 
			}
		} 
		return $response;
 	}

 	public function saveCalificationContac($comment, $approvalId, $contacId){
 		$recursive  	= -1;
		$conditions 	= array("ApprovalContac.id" => $approvalId, "ApprovalContac.user_id" => AuthComponent::user("id"));
		$contacToRefuse = $this->ApprovalContac->find("first", compact("conditions","recursive"));
 		$contacToRefuse["ApprovalContac"]["calification"] = 0;
		$contacToRefuse["ApprovalContac"]["state"] = Configure::read("ENABLED");
		$this->ApprovalContac->save($contacToRefuse);
 		$data = array(
 			"comment" 	    => $comment,
 			"contac_id"     => $contacId,
 			"user_id"       => $contacToRefuse["ApprovalContac"]["user_id"],
 			"state"   	    => Configure::read("DISABLED")
		); 
 		$this->CommentContac->save($data);
 		return array("state" => true, "message" => __("El acta se ha calificado correctamente."));
 	}

 	public function excludeContacIdsCron($contacIds = array()){
 		$this->ApprovalContac->updateAll(
            array(
        	 	'ApprovalContac.include' => Configure::read("DISABLED")
        	), 
            array('ApprovalContac.contac_id' => $contacIds)
        );		
 	}

 	public function detailContac($contac_id,$fields){
 		$conditions = array('Contac.id' => $contac_id);
 		return $this->find('first',compact('conditions','fields'));
 	}

 	public function findPasswordContac($password, $contacId){
 		$fields      = array('Contac.id');
 		$conditions  = array('Contac.id' => $contacId,'Contac.password' => $password);
 		$datos       = $this->find('first',compact('fields','conditions'));
 		if (isset($datos['Contac']['id'])) {
 			return $datos['Contac']['id'];
 		} else {
 			return false;
 		}
 	}  

	//log en editar ya despues de estar creado
 	public function storeInfoContacBeforeEdit($id, $data = array()){  
 		$response     = $this->__validateInfoProjectAndClient($data);
		$projectId    = $this->__validateProjectId($data); 	 
 		$externos     = $this->getUsersLog(!empty($data["Contac"]["externos"])     ? $data["Contac"]["externos"]: ""); 
 		$funcionarios = $this->getUsersLog(!empty($data["Contac"]["funcionarios"]) ? $data["Contac"]["funcionarios"]: ""); 
 		$descripcion = array(  
	 		"duration"       => $data["Contac"]["duration"], 
	 		//"password"	     => !empty($data["Contac"]["password"])   ? $data["Contac"]["password"]   : __(" No protegida "), 	
	 		"start_date"     => !empty($data["Contac"]["start_date"]) ? $data["Contac"]["start_date"] : __(" Sin fecha inicio "),
	 		//"client_id"      => !empty($data["Contac"]["client_id"])  ? $data["Contac"]["client_id"]  : __(" Sin cliente "), 
	 		//"team_id"      	 => !empty($data["Contac"]["team_id"])    ? $data["Contac"]["team_id"]    : __(" Sin equipo de trabajo "), 
	 		//"external_users" => $externos, 
	 		//"collaborators"  => $funcionarios, 
	 		"end_date"       => !empty($data["Contac"]["end_date"]) 	 ? $data["Contac"]["end_date"]    : __(" Sin fecha fin "),
	 		"state"          => __(" Borrador "), 
	 		"description"    => !empty($data["Contac"]["description"]) ? $data["Contac"]["description"] : __(" Sin descripción "), 
	 		//"project_id"     => !empty($projectId) ? $projectId : __( " sin proyecto "),
	 		"project"        => $response["nameProject"],
	 		"client"         => $response["nameClient"],
	 		"copies"         => !empty($data["Contac"]["copies"])  ? $data["Contac"]["copies"]  : __(" Sin copias de correo electrónico "),
 		);   
	 	return json_encode($descripcion); 
 	} 

 	// private function __getUpdateAssistant($id, $data){
 	// 	$conditions  = array("Assistant.contac_id" => $id);
		// $fields      = array("Assistant.user_id");
 	// 	$usersId     = $this->Assistant->find("list", compact("fields","conditions")); 
 	// 	$fields      = array("Assistant.employee_id");
 	// 	$employeeId  = $this->Assistant->find("list", compact("fields","conditions")); 
 	// 	$data["Contac"]["funcionarios"] = array_filter($usersId);
 	// 	$data["Contac"]["externos"]     = array_filter($employeeId);
 	// 	return $data;
 	// }

 	//LOG CUANDO SE CREA EL BORRADOR POR PRIMERA VEZ
 	public function saveLogInAddContac($ultimoId, $beforeEdit){ 		
 		$dataBeforeSave = $this->storeInfoContacBeforeEdit($ultimoId, $beforeEdit); 
	    $full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
	    $team           = $this->getNameTeam($ultimoId);
    	$description    = sprintf(__("El usuario %s ha creado un acta como borrador asociado a la empresa %s."),  $full_name, $team);
	    $descriptionEng = sprintf(__("The user %s has created an minute as draft associated with the company %s."), $full_name, $team);
	    $this->buildInfoLog($ultimoId, $description, $descriptionEng, $dataBeforeSave, NULL, NULL, $ultimoId, $beforeEdit["Contac"]["team_id"]);
 	} 

 	public function getNameTeam($id){
 		$conditions = array("Contac.id" => $id);
 		$teamId     = $this->field("team_id", $conditions);
 		$conditions = array("Team.id" => $teamId);
 		$team       = $this->Team->field("name", $conditions);
 		return $team;
 	}

 	//obtenemos el nombre del proyecto y del cliente
 	private function __validateInfoProjectAndClient($data){ 
 		$conditions  = array("Client.id" => $data["Contac"]["client_id"]);
 		$nameClient  = $this->Client->field("name", $conditions);
 		$conditions  = array("Project.id" => $data["Contac"]["project_id"]);
 		$nameProject = $this->Project->field("name", $conditions);
 		if($nameProject == NULL){
 			$nameProject = __("Sin proyecto seleccionado.");
 		}
 		if($nameClient == NULL){
 			$nameClient = __("Sin cliente seleccionado.");
 		} 	 	
 		return array("nameProject" => $nameProject, "nameClient" => $nameClient);
 	}

 	//validamos si el id del proyecto es 0, quiere decir que no tiene asignado ninguno
 	private function __validateProjectId($data){
 		$projectId = "";
 		if($data["Contac"]["project_id"] == 0 || $data["Contac"]["project_id"] == NULL){
 			$projectId = NULL;
 		} else {
 			$projectId = $data["Contac"]["project_id"];
 		}
 		return $projectId;
 	}

 	public function getUsersLog($userIds){
 		$conditions   = array("User.id" => $userIds);
 		$fields       = array("User.name");
 		$usersContac  = $this->User->find("list", compact("conditions","fields"));
 		$users  = implode(' , ',$usersContac);
 		if(!empty($users)){
 		 	return $users;
 		} else {
 			return $users = __(" Sin funcionarios ");
 		} 
 	} 

 	public function saveAllLogContac($id, $beforeEdit, $dataNew){ 
 		$afterEdit      = $this->storeInfoContacBeforeEdit($id, $dataNew); 
	    $full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
	    $team           = $this->getNameTeam($id);
    	$description    = sprintf(__("El usuario %s ha editado el acta como borrador asociado a la empresa %s."),  $full_name, $team);
	    $descriptionEng = sprintf(__("The user %s has edit an minute as draft associated with the company %s."), $full_name, $team);
	    $this->buildInfoLog($id, $description, $descriptionEng, $beforeEdit, $afterEdit, "saveAllLogContac", $id, $dataNew["Contac"]["team_id"]);
 	}

 	public function saveAllLogContacAutoSave($id, $beforeEdit, $dataNew){
 		$afterEdit      = $this->storeInfoContacBeforeEdit($id, $dataNew);  		
    	$full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
	    $team           = $this->getNameTeam($id);
    	$description    = sprintf(__("El usuario %s ha editado el acta como borrador asociado a la empresa %s."),  $full_name, $team);
	    $descriptionEng = sprintf(__("The user %s has edit an minute as draft associated with the company %s."), $full_name, $team);
 		$log 			= ClassRegistry::init('Log');
    	$conditions     = array("Log.action" => "saveAllLogContacAutoSave", "Log.user_id" => AuthComponent::user("id"), "Log.contac_id" => $dataNew["Contac"]["id"]);
    	$logExist       = $log->find("first", compact("conditions"));
    	if(!empty($logExist)){ 
    		$logExist["Log"]["description"] = $description;
    		$logExist["Log"]["before_edit"] = $beforeEdit;
    		$logExist["Log"]["after_edit"]  = $afterEdit; 
    		$logExist["Log"]["modified"]    = date('Y-m-d H:i:s');
    		$log->save($logExist); 
    	} else {
	    	$this->buildInfoLog($id, $description, $descriptionEng, $beforeEdit, $afterEdit, "saveAllLogContacAutoSave", $id, $dataNew["Contac"]["team_id"]); 
    	}  
 	}

 	public function saveLogDescartarBorrador($contacId){
 		// $conditions     = array("Contac.id" => $contacId);
 		// $contac         = $this->find("first", compact("conditions"));
 		// $full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
	    // $team           = $this->getNameTeam($id);
    	// $description    = sprintf(__("El usuario %s ha descartado el acta asociado a la empresa %s."),  $full_name, $team);
	    // $descriptionEng = sprintf(__("The user %s You’ve discarded the minute associated with the company %s."), $full_name, $team);
	    // $this->buildInfoLog($contacId, $description, $descriptionEng, NULL, NULL, NULL, $contacId, $contac["Contac"]["team_id"]);
 	}

 	public function userChoose($data){ 
		if(!is_array($data['funcionarios'])){
			return false;
		}
		return true; 
	}

	public function validateEqualDatesMeeting(){
		if($this->data["Contac"]["start_date"] == $this->data["Contac"]["end_date"]){
			return false;
		} else {
			return true;
		} 
	}

	public function validateEndDateWithStartDate(){
		if($this->data["Contac"]["end_date"] < $this->data["Contac"]["start_date"]){
			return false;
		} else {
			return true;
		} 
	}

	public function update_assistants_contac($assistants = array(), $contacId){
		$recursive    = -1;
		$conditions   = array("Assistant.contac_id" => $contacId, "Assistant.type_user" => Configure::read("COLLABORATOR"), "Assistant.meeting_id" => NULL); 
		$userInList   = $this->Assistant->find("all", compact("conditions","recursive"));
		$conditions   = array("Assistant.contac_id" => $contacId, "Assistant.type_user" => Configure::read("EXTERNAL"), "Assistant.meeting_id" => NULL); 
		$userExternal = $this->Assistant->find("all", compact("conditions","recursive"));
		if(!empty($assistants["funcionarios"])){
			$usersIds   = array_combine($assistants["funcionarios"], $assistants["funcionarios"]);
			$usersExist = Set::combine($userInList, '{n}.Assistant.user_id', '{n}.Assistant.user_id');
			$usersNoExistInList = array_diff($usersIds, $usersExist);
			if(!empty($usersNoExistInList)){
				$this->save_official_user($usersNoExistInList, $contacId, $assistants);
			}
		}
		$this->__checkUserRemoved($userInList, $assistants, $contacId);  
		if(!empty($assistants["externos"])){
			$usersIds   = array_combine($assistants["externos"], $assistants["externos"]);
			$usersExist = Set::combine($userExternal, '{n}.Assistant.user_id', '{n}.Assistant.user_id');
			$usersNoExistInList = array_diff($usersIds, $usersExist);
			if(!empty($usersNoExistInList)){
				$this->save_employee_user($usersNoExistInList, $contacId, $assistants);
			}
		}  
		$this->__checkEmployeeRemoved($userExternal, $assistants, $contacId); 
	}

	public function save_official_user($usersOfficial, $contacId, $data = array()){
		foreach ($usersOfficial as $user) {
		 	$users[] = array( 
		 		"user_id"     => $user,
		 		"contac_id"   => $contacId,
		 		"team_id"     => $data["team_id"],
		 		"type_user"   => Configure::read("COLLABORATOR")
		 	);
		} 
		$this->Assistant->saveMany($users, array('deep' => true));
		$this->logAssistantContacBorrador($usersOfficial, $contacId);
	}

	public function save_employee_user($usersOfficial, $contacId, $data = array()){
		foreach ($usersOfficial as $user) {
		 	$users[] = array( 
		 		"user_id"     => $user,
		 		"contac_id"   => $contacId,
		 		"team_id"     => $data["team_id"],
		 		"type_user"   => Configure::read("EXTERNAL")
		 	);
		} 
		$this->Assistant->saveMany($users, array('deep' => true)); 
		$this->logAssistantContacBorrador($usersOfficial, $contacId);
	}

	private function __checkUserRemoved($usersExists, $usersAdded, $contacId){
	 	if(!is_array($usersAdded["funcionarios"])){
	 		$usersAdded["funcionarios"] = array();
			$usersExist   = Set::combine($usersExists, '{n}.Assistant.user_id', '{n}.Assistant.user_id');
			$removedUsers = array_filter(array_diff($usersExist, $usersAdded["funcionarios"]));
			if(!empty($removedUsers)){
				$this->logAssistantsRemoved($contacId, $removedUsers);
				foreach ($removedUsers as $userId) {
					$this->Assistant->deleteAll(array('Assistant.contac_id' => $contacId, 'Assistant.user_id' => $userId, "Assistant.meeting_id" => NULL),false);		 
				} 
			}
	 	} else {
	 		$usersExist   = Set::combine($usersExists, '{n}.Assistant.user_id', '{n}.Assistant.user_id');
			$removedUsers = array_filter(array_diff($usersExist, $usersAdded["funcionarios"]));
	 	 	if(!empty($removedUsers)){
	 	 		$this->logAssistantsRemoved($contacId, $removedUsers);
				foreach ($removedUsers as $userId) {
					$this->Assistant->deleteAll(array('Assistant.contac_id' => $contacId, 'Assistant.user_id' => $userId, "Assistant.meeting_id" => NULL),false);		 
				} 
			} 
	 	}
	}

	private function __checkEmployeeRemoved($usersExists, $usersAdded, $contacId){
	 	if(!is_array($usersAdded["externos"])){
	 		$usersAdded["externos"] = array();
			$usersExist   = Set::combine($usersExists, '{n}.Assistant.user_id', '{n}.Assistant.user_id');
			$removedUsers = array_filter(array_diff($usersExist, $usersAdded["externos"]));
			if(!empty($removedUsers)){
				$this->logAssistantsRemoved($contacId, $removedUsers);
				foreach ($removedUsers as $userId) {
					$this->Assistant->deleteAll(array('Assistant.contac_id' => $contacId, 'Assistant.user_id' => $userId, "Assistant.meeting_id" => NULL),false);		 
				} 
			}
	 	} else {
	 		$usersExist   = Set::combine($usersExists, '{n}.Assistant.user_id', '{n}.Assistant.user_id');
			$removedUsers = array_filter(array_diff($usersExist, $usersAdded["externos"]));
	 	 	if(!empty($removedUsers)){
	 	 		$this->logAssistantsRemoved($contacId, $removedUsers);
				foreach ($removedUsers as $userId) {
					$this->Assistant->deleteAll(array('Assistant.contac_id' => $contacId, 'Assistant.user_id' => $userId, "Assistant.meeting_id" => NULL),false);		 
				} 
			} 
	 	} 
	} 

	//LOGS DE COLABORADORES Y EXTERNOS NUEVOS EN EL BORRADOR DEL ACTA
	public function logAssistantContacBorrador($users = array(), $contacId){
	 	$conditions  = array("Assistant.contac_id" => $contacId, "Assistant.user_id" => $users);
 		$assistants  = $this->Assistant->find("all", compact("conditions")); 
 		$full_name   = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
 		$team        = $this->getNameTeam($contacId);
		if(!empty($assistants)){
			foreach ($assistants as $assistant) {  
 				$employee       = $assistant["User"]["firstname"] . ' ' . $assistant["User"]["lastname"];
				$description    = sprintf(__("El usuario %s ha añadido al funcionario %s en una acta asociada a la empresa %s."), $full_name, $employee, $team);
				$descriptionEng = sprintf(__("The user %s You’ve added the employee %s in a minute associated with the company %s."), $full_name, $employee, $team);
 				$this->buildInfoLog($assistant["Assistant"]["id"], $description, $descriptionEng, __("Sin modificaciones"), __("Sin modificaciones"), "logAssistantContacBorrador", $contacId, $assistant["Contac"]["team_id"]);			 	 
			}
		}  
	}

	//LOGS CUANDO SE ELIMINA UN ASISTENTE EXTERNO O COLABORADOR EN EL BORRADOR DEL ACTA
	public function logAssistantsRemoved($contacId, $users){
		$conditions  = array("Assistant.contac_id" => $contacId, "Assistant.user_id" => $users);
 		$assistants  = $this->Assistant->find("all", compact("conditions")); 
 	 	$full_name   = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
 		$team        = $this->getNameTeam($contacId);
		if(!empty($assistants)){
			foreach ($assistants as $assistant) {  
				$employee       = $assistant["User"]["firstname"] . ' ' . $assistant["User"]["lastname"];
				$description    = sprintf(__("El usuario %s ha removido al funcionario %s en una acta asociada a la empresa %s."), $full_name, $employee, $team);
				$descriptionEng = sprintf(__("The user %s You’ve removed the employee %s in a minute associated with the company %s."), $full_name, $employee, $team);
 				$this->buildInfoLog($assistant["Assistant"]["id"], $description, $descriptionEng, __("Sin modificaciones"), __("Sin modificaciones"), "logAssistantsRemoved", $contacId, $assistant["Contac"]["team_id"]);			 	 
			}
		}  
	}

	//LOGS DE COLABORADORES Y EXTERNOS EN EL ACTA CREADA Y FINALIZADA
 	public function logAssistantContacAdd($contacId){
 	    $conditions  = array("Assistant.contac_id" => $contacId);
 		$assistants  = $this->Assistant->find("all", compact("conditions"));
 		$full_name   = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
 		$team        = $this->getNameTeam($contacId);
		if(!empty($assistants)){
			foreach ($assistants as $assistant) { 
				$employee       = $assistant["User"]["firstname"] . ' ' . $assistant["User"]["lastname"];
				$description    = sprintf(__("El usuario %s ha añadido al funcionario %s en una acta asociada a la empresa %s."), $full_name, $employee, $team);
				$descriptionEng = sprintf(__("The user %s You’ve added the employee %s in a minute associated with the company %s."), $full_name, $employee, $team);
 				$this->buildInfoLog($assistant["Assistant"]["id"], $description, $descriptionEng, __("Sin modificaciones"), __("Sin modificaciones"), "logAssistantContacAdd", $contacId, $assistant["Contac"]["team_id"]);			 	 
			}
		}  
 	}

 	//LOGS COMPROMISOS PARA EL CREAR ACTA
 	public function logCommitmentsContac($contacId){
 		$this->Commitment->unbindModel(array('belongsTo' => array('Project')));
 		$conditions  = array("Commitment.contac_id" => $contacId);
 		$commitments = $this->Commitment->find("all", compact("conditions")); 
 		$full_name   = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		if(!empty($commitments)){
			foreach ($commitments as $commitment) { 
				$employee       = $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"];
				$description    = sprintf(__("El usuario %s ha creado un compromiso para el funcionario %s para el día %s."), $full_name, $employee, $commitment["Commitment"]["delivery_date"]);
				$descriptionEng = sprintf(__("The user %s You’ve created a commitment %s for employee for the day %s."), $full_name, $employee, $commitment["Commitment"]["delivery_date"]);
 				$this->buildInfoLog($commitment["Commitment"]["id"], $description, $descriptionEng, __("Sin modificaciones"), __("Sin modificaciones"), "logCommitmentsContac", $contacId, $commitment["Contac"]["team_id"]); 
			}
		} 
 	} 

 	//LOGS COMPROMISOS BORRADOR
 	public function logCommitmentsContacBorrador($contacId){
 		$this->Commitment->unbindModel(array('belongsTo' => array('Project')));
 		$conditions  = array("Commitment.contac_id" => $contacId);
 		$commitments = $this->Commitment->find("all", compact("conditions"));
 		$full_name   = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
		if(!empty($commitments)){
			foreach ($commitments as $commitment) { 
		 		$employee       = $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"];
				$description    = sprintf(__("El usuario %s ha creado un compromiso para el funcionario %s para el día %s."), $full_name, $employee, $commitment["Commitment"]["delivery_date"]);
				$descriptionEng = sprintf(__("The user %s You’ve created a commitment %s for employee for the day %s."), $full_name, $employee, $commitment["Commitment"]["delivery_date"]);
 				$this->buildInfoLog($commitment["Commitment"]["id"], $description, $descriptionEng, __("Sin modificaciones"), __("Sin modificaciones"), "logCommitmentsContacBorrador", $contacId, $commitment["Contac"]["team_id"]); 
			}
		} 
 	} 

 	public function getLastContactId($teamId){
 		$conditions    = array("Contac.state" => Configure::read('DISABLED'),'Contac.team_id' => $teamId);
		$order         = array("Contac.id DESC");
		$fields        = array("Contac.id");
		$lastContacId  = $this->find('first', compact("order","conditions","fields"));
		return $lastContacId;
 	}

 	public function getMeetingContac($contacId){
 		$recursive     = -1;
 		$conditions    = array("Meeting.contac_id" => $contacId);
 		$meeting       = $this->Assistant->Meeting->find("first", compact("conditions","recursive")); 
 		if(!empty($meeting)){
	 		$meetingContac = $this->Assistant->Meeting->getInfoMeetingToContact(isset($meeting["Meeting"]["id"]) ? $meeting["Meeting"]["id"]  : array());
	 		if(!empty($meetingContac["Meeting"])) {
	 			return $meetingContac; 
	 		} else {
				return array(); 
	 		} 
 		} else {
 			return array();
 		}
 	}

 	public function checkDescriptionCkEditorEmpty(){ 
        $result   = preg_replace('~(?><(\w++)[^>]*+>(?>\s++|&nbsp;|<br\s*+/?>)*</\1>|(?>\s++|&nbsp;|<br\s*+/?>)+)+$~xi', '', $this->data["Contac"]["description"]);
        if(empty($result)){
        	return false;
        } else {
        	return true;
        }
 	}

 	public function checkLengthCharacter(){		
 		$response 	 = true;	 
 		$description = str_replace(array("<p>","</p>","<ul>","</ul>","<li>","</li>","<ol>","</ol>","&nbsp;"), array('',' ','',' ','',' ','',' ',''), strip_tags($this->data["Contac"]["description"], "<p><ul><li><ol>")); 	
    	$words       = explode(" ", $description); 
		foreach ($words as $word) { 
			if (!empty($word)) {  
				if(strlen($word) > 80){
					$response = false;
					break;
				} 
			}
		}  
		return $response;
 	}

 	private function __checkLength($descriptionContac){
 		$data = array("Contac" => array("description" => $descriptionContac));
 		$this->set($data);
		if($this->validates()) {
		    return array("state" => true);
		} else {
			$errors = $this->validationErrors;
			return array("state" => false, "message" => $errors["description"]["0"]); 
		} 
 	}
	function htmClear($string){
    $filtrar = str_replace("<p>", "", $string);
    $filtrar = str_replace("</p>", ". ", $filtrar);
    $filtrar = str_replace(" ,", ",", $filtrar);
    $filtrar = str_replace("<li>", "", $filtrar);
    $filtrar = str_replace("</li>", ". ", $filtrar);

     $filtrar = str_replace("<strong>", "", $filtrar);
      $filtrar = str_replace("</strong>", "", $filtrar);

      $filtrar = str_replace('"', "", $filtrar);

      

    return $filtrar;
}

function eliminar_acentos($cadena){
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );
        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );
        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );
 
        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );
 
        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );
 
        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ñ', 'ñ', 'Ç', 'ç'),
        array('N', 'n', 'C', 'c'),
        $cadena
        );
        
        return $cadena;
    }

    public function apiMinuteConditions($params = array()) {
		$conditions = array();
		$conditions['Contac.team_id'] = Configure::read('Application.team_id');

		if(!empty($params['user_id'])) {
			//$conditions['Contac.user_id'] = $params['user_id'];
		}
		if(!empty($params['number'])) {
			$conditions['Contac.number'] = $params['number'];
		}
		if(!empty($params['project_id'])) {
			$conditions['Contac.project_id'] = $params['project_id'];
		}
		return $conditions;
    }


}

 