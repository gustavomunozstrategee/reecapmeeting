<?php

App::uses('AppModel', 'Model');
 

class Coupon extends AppModel {

	 public $validate = array( 

	    'name' => array(
			array(
				'rule'     => 'notBlank', 
				'message'  => 'Escribe el nombre del cupón.'
			) 
		), 

		'code' => array( 
			array(
				'rule'     => 'isUnique', 
				'message'  => 'Este código ya lo tiene asignado otro cupón.'
			) 
		), 

		'phone' => array(
			array('rule' => 'notBlank','message' => 'El teléfono es requerido.'),
			array('rule' => 'numeric','message'  => 'El teléfono debe de ser tipo numérico.'),
			array('rule' => array('minLength', '7'),'message' => 'El teléfono debe tener mínimo 7 caracteres.'),
		),
 
		// 'discount_average' => array(
		// 	array(
		// 		'rule'     => 'notBlank', 
		// 		'message'  => 'Digite el valor del descuento.'
		// 	),
		// 	array(
		// 		'rule' 	   => 'validateValue',
		// 		'message'  => 'El valor de descuento del cupón debe ser mayor a cero.'
		// 	)
		// ), 

		// 'description' => array(
		// 	array(
		// 		'rule'     => 'notBlank', 
		// 		'message'  => 'La descripción es requerida.'
		// 	)  
		// ),  

		'start_date' => array(
			array(
				'rule'     => 'validateStarDateEndDate', 
				'message'  => ''
			)  
		),  

		'addressee_name' => array(
			array(
				'rule'     => 'notBlank', 
				'message'  => 'Por favor ingrese el nombre del destinatario.'
			),
			array('rule' => '/^\D+$/', 'message' => 'El nombre debe tener solo letras.')
		), 
 
		'email_user' => array(
			array(
				'rule'     => 'notBlank', 
				'message'  => 'Por favor ingrese un correo electrónico.'
			),
			array(
				'rule'     => 'email', 
				'message'  => 'Por favor ingrese un correo electrónico válido.'
			),
			array(
				'rule'     => 'userHavePlan',
				'message'  => 'No puedes enviar el cupón a este usuario porque ya tiene un plan asignado.'
			), 
			array(
				'rule'     => 'userHaveCoupon',
				'message'  => 'No puedes enviar el cupón a este usuario porque ya tiene un cupón asignado.'
			),
			array(
				'rule'     => 'userAdminCoupon',
				'message'  => 'No puedes enviar cupones a usuarios administradores.'
			)  
		), 

	);



	public $belongsTo = array(
		'Plan' => array(
			'className' => 'Plan',
			'foreignKey' => 'plan_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function validateUserHaveOneCoupon($email){
		$recursive  = -1;
		$conditions = array("Coupon.email_user" => $email, "Coupon.usage_coupon" => configure::read("CUPON_NO_USADO"), 'Coupon.end_date <' => date("Y-m-d"));
		$coupon     = $this->find("first", compact("conditions","recursive"));
		return $coupon;
	}

	public function validateValue($data){
		$valueCoupon = isset($data['discount_average']) ? $data['discount_average'] : null;	
		if ($valueCoupon > 0){
            return true;
       	} 
   	 	return false;
	}

	public function buildConditions($params=array()) {
		$conditions = array();
		if(!empty($params['q'])) { 
			$params['q']      =  $this->normalizeChars($params['q']);
			$params['q']      =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				'LOWER(CONVERT(Coupon.name USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Coupon.code USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Coupon.description USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Coupon.discount_average USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Coupon.email_user USING latin1))  LIKE' => "%{$params['q']}%", 
			);  
		}
		return $conditions;
	}

	public function getInfoCoupon($codeCoupon){
		$this->unbindModel(array('hasMany' => array('Transaction')));
		$conditions = array("Coupon.code" => $codeCoupon, "Coupon.email_user" => authcomponent::user("email"));
		$couponInfo = $this->find("first", compact("conditions"));
		$response = array();
		if (!empty($couponInfo)) {
			if($couponInfo["Coupon"]["state"] == configure::read("ENABLED")){
				if ($couponInfo["Coupon"]["usage_coupon"] == configure::read("CUPON_NO_USADO")) {
					if($couponInfo["Coupon"]["end_date"] > date('Y-m-d')){
						$response = array("state" => true, "message" => __("El cupón se ha redimido correctamente."), "Detail" => $couponInfo); 
					} else {
						$response = array("state" => false, "message" => __("Este cupón ha expirado."));
					}					 
				} else { 
					$response = array("state" => false, "message" => __("Este cupón ya fue utilizado."));
				}
			} else {
				$response = array("state" => false, "message" => __("Este cupón está deshabilitado, por favor comunícate con el administrador."));
			}
		} else {
			$response = array("state" => false, "message" => __("No existe un cupón con esta referencia."));
		}
		return $response; 
	}

	public function usedCoupon($couponId){
		$this->updateAll(
            array('Coupon.usage_coupon' => Configure::read('CUPON_USADO'), "Coupon.date_use" => "'".date("Y-m-d H:i:s")."'"),
            array('Coupon.id'           => $couponId)
        );
	}

	public function disabledStateCoupon($couponId){
		$this->updateAll(
            array('Coupon.state' => Configure::read('DISABLED')), 
            array('Coupon.id'    => $couponId)
        );
	}

	public function userHavePlan($data){
		$email        = isset($data['email_user']) ? $data['email_user'] : null;
		$conditions   = array("User.email" => $email);
		$userId       = $this->Plan->PlanUser->User->field("id", $conditions);
		$conditions   = array("PlanUser.user_id" => $userId);
		$userHavePlan = $this->Plan->PlanUser->find("first", compact("conditions"));
		if (empty($userHavePlan)){
            return true;
       	} 
   	 	return false;
	}

	public function userHaveCoupon($data){
		$email        = isset($data['email_user']) ? $data['email_user'] : null;
		$conditions   = array("Coupon.email_user" => $email);
		$userHavePlan = $this->find("first", compact("conditions")); 
		if (empty($userHavePlan)){
   	 		return true;
       	} 
    	return false;
	}

	public function userAdminCoupon($data){
		$email      = isset($data['email_user']) ? $data['email_user'] : null;
		$conditions = array("User.email" => $email, "User.role_id" => Configure::read("ADMIN_ROLE_ID"));
		$userAdmin  = $this->Plan->PlanUser->User->find("first", compact("conditions")); 
		if (empty($userAdmin)){
   	 		return true;
       	} 
    	return false;
	}

	public function getStartAndEndDates($date, $data){
		$dates  = explode(" ", $date); 
		$data['Coupon']['start_date']   = date ("Y-m-d", strtotime($dates['0']));
		$data['Coupon']['end_date']     = date ("Y-m-d", strtotime($dates['2'])); 
		return $data;
	} 

	public function validateStarDateEndDate(){
		if($this->data["Coupon"]["start_date"] == $this->data["Coupon"]["end_date"]){
			$this->invalidate('daterangedates','La fecha inicio no puede ser igual que la fecha fin.');
			return false;
		} else {
			return true;
		} 
	}
}

