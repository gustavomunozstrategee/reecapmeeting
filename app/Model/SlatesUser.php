<?php
App::uses('AppModel', 'Model');
/**
 * ListsUser Model
 *
 * @property List $List
 * @property User $User
 */
class SlatesUser extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Slate' => array(
			'className' => 'Slate',
			'foreignKey' => 'slate_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
