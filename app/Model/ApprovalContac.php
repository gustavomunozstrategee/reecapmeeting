<?php
App::uses('AppModel', 'Model'); 
class ApprovalContac extends AppModel {
 
	public $belongsTo = array(
		'Contac' => array(
			'className' => 'Contac',
			'foreignKey' => 'contac_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'User' => array(
			'className'  => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		), 
		'Client'   => array('className' => 'Client','foreignKey'  => false, 'conditions' => array("Contac.client_id = Client.id")),
		'Project'  => array('className' => 'Project','foreignKey' => false, 'conditions' => array("Contac.project_id = Project.id")),
		'Team'     => array('className' => 'Team','foreignKey'    => false, 'conditions' => array("Contac.team_id = Team.id")),
		'Creator'  => array('className' => 'User','foreignKey'    => false, 'conditions' => array("Contac.user_id = Creator.id")),
	);

 	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = trim($params['q']);
			$params['q'] = strtolower($params['q']);
			$conditions = array('OR'  => array(
				'Project.name  LIKE'  => "%{$params['q']}%",
				'Client.name  LIKE'   => "%{$params['q']}%",
				'Contac.number LIKE'  => "%{$params['q']}%"
			));
		}
		if(!empty($params['teams'])) {
			$conditions['Contac.team_id'] = $params['teams'];
		}
		return $conditions;
	}

	public function saveAllApprovalUsers($data, $contacId){ 
   		$datos = array();
		$i     = 0;
		if(isset($data["approval"])){
			if (!empty($data["approvalusers"])) { 
				$this->deleteAll(array('ApprovalContac.contac_id' => $contacId), false);
				foreach ($data["approvalusers"] as $approvalUser) { 
					$datos[$i]['user_id']     = $approvalUser;
					$datos[$i]['limit_date']  = $data["limit_date"];
					$datos[$i]['contac_id']   = $contacId; 
					$i++;
				} 
				$this->saveAll($datos);
				$this->__saveLogApprovalContac($contacId);
			} 
		}
	}

	private function __saveLogApprovalContac($contacId){
		$this->unbindModel(array('belongsTo' => array('Client','Creator')));
		$fields     = array("ApprovalContac.*","User.id","User.firstname","User.lastname","Contac.team_id", "Project.*");
		$conditions = array("ApprovalContac.contac_id" => $contacId);
		$approvals  = $this->find("all", compact("conditions","fields"));
		if(!empty($approvals)){
			foreach ($approvals as $approval) { 
				$user           = $approval["User"]["firstname"] . ' ' . $approval["User"]["lastname"];
				$own            = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
				$description    = sprintf(__("El usuario %s ha solicitado la aprobación del acta asociada al proyecto %s de parte del usuario %s."), $own, $approval["Project"]["name"], $user);
				$descriptionEng = sprintf(__("The user %s You’ve requested the approval of the minute associated with the project %s from the user %s."), $own, $approval["Project"]["name"], $user);
				$this->buildInfoLog($approval["ApprovalContac"]["id"], $description, $descriptionEng, NULL, NULL, "saveAllApprovalUsers", $contacId, $approval["Contac"]["team_id"]); 
			}
		} 
	}

	public function calculateAverageUsers($contacId){
		$recursive  = -1;
		$group      = array("ApprovalContac.contac_id");
		$fields     = array("AVG(ApprovalContac.calification) AS AverageRating","ApprovalContac.contac_id");
		$conditions = array("ApprovalContac.contac_id" => $contacId);
		$contacToApproved = $this->Contac->ApprovalContac->find("all", compact("conditions","recursive","group","fields"));
		$this->updateAll(
            array(
            	'ApprovalContac.average_rating' => $contacToApproved["0"]["0"]["AverageRating"], 
        	), 
            array('ApprovalContac.contac_id'    => $contacId)
        );
        $conditions = array("ApprovalContac.state" => configure::read("DISABLED"), "ApprovalContac.contac_id" => $contacId);
        $countRemainingApprovalContac =  $this->find("count", compact("conditions"));
        $approved   = $this->__validateAllUserRated($contacId);
        return array("remainingUser" => $countRemainingApprovalContac, "contacId" => $contacId, "approved" => $approved); 
	}

	private function __validateAllUserRated($contacId){
		$recursive  	   = -1;
		$conditions 	   = array("ApprovalContac.contac_id" => $contacId);
		$contacToApproveds = $this->Contac->ApprovalContac->find("all", compact("conditions","recursive"));
		$countUsers 	   = count($contacToApproveds);
		$numberCalification = 0;
		foreach ($contacToApproveds as $contacToApproved) {
			if ($contacToApproved["ApprovalContac"]["calification"] == configure::read("ENABLED")) {
		 		$numberCalification++;
			}			 
		}
		if($countUsers == $numberCalification){
			$approved = true;
		} else {
			$approved = false;
		}
		return $approved; 
	}

	public function saveNewApproval($contacId){
		$this->updateAll(
            array(
            	'ApprovalContac.calification'   => NULL, 
            	'ApprovalContac.average_rating' => NULL, 
            	'ApprovalContac.state' 			=> configure::read("DISABLED"), 
        	), 
            array('ApprovalContac.contac_id'    => $contacId)
        );
        $this->Contac->updateAll(
            array(
            	'Contac.state' 		 => configure::read("APPROVAL"), 
            	'Contac.in_approval' => configure::read("ENABLED"), 
            	'Contac.firebase'	 => NULL, 
        	), 
            array('Contac.id'  => $contacId)
        );
		$response = array("state" => true, "message" => __("Se ha solicitado nuevamente la calificación del acta. Se ha enviado la notificación a los usuarios para que califiquen el acta nuevemente."));
		return $response;
	}

	public function saveLogCalification($approvalId){
		$this->unbindModel(array('belongsTo' => array('Client')));
		$conditions     = array("ApprovalContac.id" => $approvalId);
		$approval       = $this->find("first", compact("conditions"));  
		$user           = $approval["User"]["firstname"] . ' ' . $approval["User"]["lastname"];
		$project        = $approval["Project"]["name"];
		$description    = sprintf(__("El funcionario %s ha aprobado el acta asociada al proyecto %s."), $user, $project);
		$descriptionEng = sprintf(__("The employee %s has approved the minute associate with the project %s."), $user, $project);
		$this->buildInfoLog($approval["ApprovalContac"]["id"], $description, $descriptionEng, NULL, NULL, NULL, $approval["Contac"]["id"], $approval["Contac"]["team_id"]); 
	}

	public function saveLogLastCalificator($approvalId){
		$conditions     = array("ApprovalContac.id" => $approvalId);
		$approval       = $this->find("first", compact("conditions"));
		$user           = $approval["User"]["firstname"] . ' ' . $approval["User"]["lastname"]; 
		$description    = sprintf(__("El funcionario %s fue el último usuario en calificar el acta y esta quedó finalizada y con el número %s."), $user, $approval["Contac"]["number"]);
		$descriptionEng = sprintf(__("The employee %s was the last user to check the minute. It is ready and has the id %s."), $user, $approval["Contac"]["number"]);
		$this->buildInfoLog($approval["ApprovalContac"]["id"], $description, $descriptionEng, NULL, NULL, NULL, $approval["Contac"]["id"], $approval["Contac"]["team_id"]); 
	}

	public function saveLogRefuseContac($approvalId){
		$conditions     = array("ApprovalContac.id" => $approvalId);
		$approval       = $this->find("first", compact("conditions"));
		$project        = $approval["Project"]["name"];
		$user           = $approval["User"]["firstname"] . ' ' . $approval["User"]["lastname"];
		$description    = sprintf(__("El funcionario %s ha rechazado el acta asociada al proyecto %s."), $user, $project);
		$descriptionEng = sprintf(__("The employee %s has rejected the minute with associate with the project %s."), $user, $project);
		$this->buildInfoLog($approval["ApprovalContac"]["id"], $description, $descriptionEng, NULL, NULL, NULL, $approval["Contac"]["id"], $approval["Contac"]["team_id"]); 
	}
	  
	public function notificationFirebaseContacPendingApproved($contacId){
		$this->unbindModel(array('belongsTo' => array('Client','Creator')));
		$conditions = array("ApprovalContac.contac_id" => $contacId);
		$approvals  = $this->find("all", compact("conditions"));
		if(!empty($approvals)){
			foreach ($approvals as $approval) {
				$url    = "contacs/approval_contacs";
			 	$msgEsp = "Tienes un acta pendiente por aprobar del proyecto ". $approval["Project"]["name"].".";
			 	$msgEng = __("You’ve a pending minute for approval of the project "). $approval["Project"]["name"].".";
		 		$this->notificacionesUsuarios($approval['ApprovalContac']['user_id'], $msgEsp, $msgEng, $url); 
			} 
		}
	}

	public function updateListUserApproval($contacId, $data = array()){
 		$recursive    = -1;
		$conditions   = array("ApprovalContac.contac_id" => $contacId); 
		$userInList   = $this->find("all", compact("conditions","recursive"));
		if(!empty($data["Contac"]["approvalusers"])) {
			$usersIds   = array_combine($data["Contac"]["approvalusers"], $data["Contac"]["approvalusers"]);
			$usersExist = Set::combine($userInList, '{n}.ApprovalContac.user_id', '{n}.ApprovalContac.user_id');
			$usersNoExistInList = array_diff($usersIds, $usersExist);
			if(!empty($usersNoExistInList)){
				$this->__saveNewApprovals($usersNoExistInList, $contacId, $data);
			}
		} 
		$this->__checkUserApprovalRemoved($userInList, $data, $contacId);
	}

	private function __saveNewApprovals($usersApprovals, $contacId, $data){ 
		foreach ($usersApprovals as $userId) {  
		 	$users[] = array( 
		 		"limit_date" => $data['Contac']['limit_date'],
		 		"contac_id"  => $contacId,
		 		"user_id"    => $userId
		 	);
		} 
		$this->saveMany($users, array('deep' => true)); 
	}

	private function __checkUserApprovalRemoved($usersApprovals, $data = array(), $contacId){ 
		$usersApproval  = Set::combine($usersApprovals, '{n}.ApprovalContac.user_id', '{n}.ApprovalContac.user_id');
		$removedUsers   = array_diff(!empty($usersApproval) ? $usersApproval : array(), !empty($data["Contac"]["approvalusers"]) ? $data["Contac"]["approvalusers"]: array());
		if(!empty($removedUsers)){ 
			foreach ($removedUsers as $userId) {
				$this->deleteAll(array('ApprovalContac.contac_id' => $contacId, 'ApprovalContac.user_id' => $userId),false);		 
			} 
		} 		 
		if($data["Contac"]["limit_date"] == "0000-00-00"){
			$data["Contac"]["limit_date"] = NULL;	
		} 
		$saveData = array();
		if(!empty($usersApprovals)){
			foreach ($usersApprovals as $usersApproval) {
			 	$saveData[] = array("ApprovalContac" => array(
				 	"id" 		 => $usersApproval["ApprovalContac"]["id"],
				 	"limit_date" => $data["Contac"]["limit_date"], 
				 	)
			 	);
			}
			$this->saveMany($saveData, array('deep' => true));  
		}
	}

	public function getUsers($contacId){
		$recursive   = -1;
		$conditions  = array("ApprovalContac.contac_id" => $contacId);
		$approval    = $this->find("all", compact("conditions","recursive"));
		return $approval;
	}
 
}
