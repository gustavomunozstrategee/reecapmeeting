<?php
App::uses('AppModel', 'Model');

class Slate extends AppModel {

	public $belongsTo = array(
		'User' => array('className' => 'User','foreignKey' => 'user_id',)
	);
	public $hasMany = array(
		'SlatesUser' => array(
			'className' => 'SlatesUser',
			'foreignKey' => 'slate_id',
			'dependent' => false
		),
		'SlatesTask' => array(
			'className' => 'SlatesTask',
			'foreignKey' => 'slate_id',
			'dependent' => false
		),
	);

	public function buildConditions($params=array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = strtolower($params['q']);
			$conditions['OR'] = array(
				'LOWER(List.id) LIKE'=>"%{$params['q']}%",
			);
		}
		return $conditions;
	}

}
