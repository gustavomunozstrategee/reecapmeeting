<?php
App::uses('AuthComponent', 'Controller/Component'); 
App::uses('AppModel', 'Model'); 
class User extends AppModel {

	public $virtualFields = array(
		'name'=> "CONCAT(TRIM(User.firstname), ' ', TRIM(User.lastname))",
	    'chat_name' => 'IF(`User`.`firstname` IS NOT NULL AND `User`.`firstname` <> "", CONCAT(`User`.`firstname`, " ", `User`.`lastname`) , "ZZZZZ")'
	);

	public $validate = array(
		// 'company_name' => array(array('rule' => 'notBlank','message' => 'El nombre de la empresa es requerido.'),),
		// 'nit' => array(
		// 	array('rule' => 'notBlank','message' => 'El nit es requerido.'),
		// 	// array('rule' => 'numeric','message' => 'El nit debe de ser tipo numérico.'),
		// 	array('rule' =>  array('maxLength', '15'),'message' => 'El nit debe tener máximo 15 dígitos.'),
		// 	// array('rule' => 'isUnique','message' => 'El nit ingresado ya existe, por favor verifica.'),
		// ),
		'firstname' => array(
			array('rule' => 'notBlank','message' => 'El nombre es requerido.'),
			array('rule' => '/^\D+$/', 'message' => 'El nombre debe tener solo letras.')
		),
		// 'address'   => array(array('rule' => 'notBlank','message' => 'La dirección es requerida.'),),
		'lastname'  => array(
			array('rule' => 'notBlank','message' => 'Debes definir al menos un apellido.'),
			array('rule' => '/^\D+$/', 'message' => 'El apellido debe tener solo letras.')
		),
		'telephone' => array(
			// array('rule' => 'notBlank','message' => 'El teléfono es requerido.'),
			// array('rule' => 'numeric','message'  => 'El teléfono debe de ser tipo numérico.'),
			// array('rule' => 'isUnique','message' => 'El teléfono ingresado ya existe, por favor valida.'),
			array('rule' => array('minLength', '7'),'message' => 'El teléfono debe tener mínimo 7 caracteres.'),
		),
		'email'     => array(
			array(
				'rule'     => 'notBlank',
				'on'       => 'create',
				'required' => true,
				'message'  => 'Por favor ingrese un correo electrónico.'
			),
			array('rule' => 'email','message'    => 'Ingrese una dirección de correo electrónico válida.'),
			array('rule' => 'isUnique','message' => 'El correo electrónico ya existe, por favor verifica.')
		),
		'password' => array(
			array('rule' => 'notBlank','message' => 'La contraseña es requerida.','on' => 'create'),
			array('rule' => array('minLength', '8'),'message' => 'La contraseña debe tener como mínimo 8 caracteres.'),
			array('rule' => 'matchPasswords','required' => true,'message'  => '','on'=> 'create'), 
		),
		'confirm_password' => array(
				'rule'     => 'notBlank',
				//'required' => true,
				'on'       => 'create',
				'message'  => 'Por favor confirma tu contraseña.',
			array(
				'rule'     => array('minLength','8'),
				//'required' => true,
				'message'  => 'La contraseña debe tener como mínimo 8 caracteres.',
				'on'       => 'create'
			) 
		),
		'img' => array(          
            'isUnderPhpSizeLimit' => array(
                'rule'    => 'isUnderPhpSizeLimit',
                'message' => 'El archivo excede el límite de tamaño de archivo de subida.',
            ), 
            'isValidMimeType' => array(
                'rule'    => array('isValidMimeType', array('image/jpeg', 'image/png', 'image/gif'), false),
                'message' => 'La imagen no es jpg, gif ni png.',
            ),
            'isBelowMaxSize' => array(
                'rule'    => array('isBelowMaxSize', 10485760),
                'message' => 'El tamaño de imagen es demasiado grande. Solo se permiten imágenes de tamaño de 10MB.'
            ), 
            'isValidExtension' => array( 
                'rule'    => array('isValidExtension', array('jpg', 'png', 'jpeg','gif'), false),
                'message' => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            ), 
            'validateMimeType' => array(
               'rule'       => 'validateMimeType', 
               'message'    => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            )
        ), 
		'contraNueva' => array(
			array('rule' => array('minLength', '8'),'message' => 'La contraseña debe tener como mínimo 8 caracteres.')
		),
		'accepting_policies' => array(
            'notBlank' => array(
                'rule'     => 'notBlank',
                'required' => true,
                'message'  => 'La política de privacidad es requerida.',
                'on'       => 'create'
            )
        ),
        'accepting_data_collection' => array(
            'notBlank' => array(
                'rule'     => 'notBlank',
                'required' => true,
                'message'  => 'La política de datos es requerida.',
                'on'       => 'create'
            )
        )
	);

	public $actsAs = array(
	   'Upload.Upload' => array(
	       'img'         => array(
	         'pathMethod'   => 'flat',
	         'nameCallback' => 'renameFile',
	         'path'         => '{ROOT}{DS}webroot{DS}files{DS}User{DS}',
	         'deleteOnUpdate'       => false,
		     'deleteFolderOnDelete' => false,
	       )
	    )
	 );

	public function validateMimeType($data){  
        return $this->validateMimeTypeImg($data);  
    }

	public function fileSelected($file) {
		$file = $file["img"];
	   	return (is_array($file) && array_key_exists('name', $file) && !empty($file['name']));
	}

	public function beforeSave($options = array()) {
	    $actionsForm = Router::getParams()["action"];
		if($actionsForm !== "add" && $actionsForm !== "add_admin_user" && $actionsForm !== "register_user_from_suite" &&  $actionsForm !== "GoogleLoginResult" &&  $actionsForm !== "OutlookLoginResult"){
			unset($this->data["User"]["email"]); 
		}
		if (isset($this->data[$this->alias]['password']) && !empty($this->data[$this->alias]['password'])) {
		 	$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}

	public function generateHashChangePassword() {
		$salt    = Configure::read('Security.salt');
		$salt_v2 = Configure::read('Security.cipherSeed');
		$rand    = mt_rand(1,999999999);
		$rand_v2 = mt_rand(1,999999999);
		$hash    = hash('sha256',$salt.$rand.$salt_v2.$rand_v2);
		return $hash;
	}
 
   	public $hasOne = array(
        'PlanUser'   => array('className' => 'PlanUser','foreignKey' => 'user_id','dependent' => false, 'conditions' => array("PlanUser.state" => 1))   
   	);
 
	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				"LOWER(CONVERT({$this->virtualFields["name"]} USING latin1)) LIKE" => "%{$params['q']}%", 
				'LOWER(CONVERT(User.email USING latin1))        LIKE' => "%{$params['q']}%", 
			); 
		} 
		return $conditions;
	}

	public function matchPasswords($data){
      	if ($data['password'] == $this->data['User']['confirm_password']){
            return true;
       	}
      	 $this->invalidate('confirm_password',__('Tus contraseñas no coinciden.'));
       	 return false;
	} 

	public function checkUserIsAdmin($id){
		$conditions = array("User.id" => $id, "User.role_id" => Configure::read("BUSINESS_ROLE_ID"));
		$userAdmin  = $this->find("first", compact("conditions")); 
		if (!empty($userAdmin)) {
			return false;
		} else {
			return true;
		} 
	}

	public function validEmailExist($email){
		$conditions = array('User.email' => $email);	
		return $this->find('count',compact('conditions'));
	}

	public function detaillFuncionario($funcionario_id){
		$conditions = array('User.id' => $funcionario_id);
		return $this->find('first', compact('conditions'));
	}

	public function saveFirebaseNotificaciones($user_id, $nodoPrincipal, $datos = array()){
		if($nodoPrincipal != null){ 
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false)); 
	    	$HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.notificaciones').$user_id.'/'.$nodoPrincipal.'.json', json_encode($datos));
	    }
	}

    public function updateFirebaseNotificaciones($user_id, $nodoPrincipal, $campo, $dato){
    	if($nodoPrincipal != null){ 
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false)); 
	    	$HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.notificaciones').$user_id.'/'.$nodoPrincipal.'/'.$campo.'.json', json_encode($dato));
	    }
	}

	public function updateSession($data = array(), $Session) { 
		if(!empty($data['User'])) {
      		if($data['User']['id'] == AuthComponent::user('id')) {
       		 	$conditions = array('User.id'=>AuthComponent::user('id'));
	 			$recursive  = -1;
        	 	$user       = $this->find('first', compact('conditions','recursive')); 
        		foreach ($user['User'] as $key => $value) {
	          		$Session->write("Auth.User.{$key}", $value);  
	        	}
	      	}
    	}
	}

	public function __attachUploadImage(&$data = null, $form=null) {
		if(!empty($form['img'])) {
		    $data['img'] = $form['img'];
		}
	}

	public function optimizeProfileImage($data=array()) {
		try{
			if(!empty($data['User']['img']) && !empty($data['User']['type'])) {
				$typeFile = strtolower($data['User']['type']);
				if(in_array($typeFile, Configure::read('IMAGE_ASSET_TYPES'))) {
					$path = APP.'webroot'.DS.'files'.DS.'User'.DS;
					$path = $path.$data['User']['img'];
					if(file_exists($path)) {
						App::import('Vendor', 'Picture', array('file' => 'Picture.php'));
						$Picture = new Picture();
						$target  = $Picture->resizeCrop($path, 200, 200);
						if(file_exists($target)) {
							$target = explode(DS, $target);
							$image  = end($target);
							if($image != $data['User']['img']) {
								$nameContent = explode(".", $image);
								$ext = end($nameContent);

								$saveData = array('User' => array(
									'img' => $image,
									'file_extension' => $ext,
									'id' => $data['User']['id'],
								));

								$data['User']['img'] = $image;
								$data['User']['file_extension'] = $ext;
								$data['User']['extension'] = $ext;
								$this->save($saveData, array('validate'=>false, 'callbacks' => false));
							}
						}
					}
				}
			}
		} catch (Exception $e) {}
		return $data;
	}

	public function saveUserOnFirebase($data = array(), $companyDb = null){
		if(!empty($companyDb)){
			App::uses('HttpSocket', 'Network/Http');
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false)); 
			if(!empty($data['User']['id'])) {
				$data = $this->__clearInfoBeforeSaveOnFirebase($data);
				if(!empty($data)) {
					$id = $data['User']['id'];
					try {
						foreach ($data['User'] as $field => $value) {
							if(!empty($value)) {
					    		$url = Configure::read('FIREBASE_APP.URL').'/'.Configure::read('FIREBASE_APP.DATABASE')."/{$companyDb}/".Configure::read('FIREBASE_APP.USERS_NODE');
					    		$url .= "/{$id}/{$field}.json";
					    		$HttpSocket->put($url, json_encode($value));
							}
						}
					} catch (Exception $e) {}
				}
			}
		}
	}

	private function __clearInfoBeforeSaveOnFirebase($data = array()) {
		$out = array('User' => array(
			'id' => $data['User']['id'],
			'user_id' => $data['User']['id'],
		));

		if(!empty($data['User']['firstname'])) {
			$out['User']['name'] = $data['User']['firstname']." ";
		}
		if(!empty($data['User']['lastname'])) {
			if(!empty($out['User']['name'])) {
				$out['User']['name'] .= $data['User']['lastname'];
			
			} else {
				$out['User']['name'] = $data['User']['lastname'];

			}
		}
		if(!empty($data['User']['img']) && !is_array($data['User']['img'])) {
			$out['User']['img'] = $data['User']['img'];
		}
		return $out;
	}

	public function listUserApisConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = Encryption::normalizeChars($params['q']);
			$params['q'] = ltrim(rtrim(strtolower($params['q'])));
			$conditions['OR'] = array(
				"CONVERT(LOWER(CONCAT(User.firstname, ' ', User.lastname)) USING latin1) LIKE" => "%{$params['q']}%",
				'CONVERT(LOWER(User.email) USING latin1) LIKE' => "%{$params['q']}%",
			); 
		}
		if(!empty($params['team_id'])) {
			$conditions['UserTeam.team_id'] = $params['team_id'];
		}
		if(!empty($params['chat_user_ids_'])) {
			$conditions['User.id'] = $params['chat_user_ids_'];
		}
		return $conditions;
	}

	public function bindUserTeamInfo(){
		$binds = array('hasOne'=>array(
		  'UserTeam' => array('className' => 'UserTeam', 'conditions'=> array('UserTeam.user_id = User.id'), 'foreignKey'=> false), 
		  'Department' => array('className' => 'Department', 'conditions'=> array('UserTeam.department_id = Department.id'), 'foreignKey'=> false), 
		  'Team' => array('className' => 'Team', 'conditions'=> array('UserTeam.team_id = Team.id'), 'foreignKey'=> false), 
		));
		$this->bindModel($binds);
	}
}
