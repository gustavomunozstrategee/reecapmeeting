<?php
App::uses('AppModel', 'Model');
App::uses('HttpSocket', 'Network/Http');
 
class UsersSession extends AppModel {

    public $useTable = 'users_sessions';

    public function updateUserIdOnSession($userId = null, $sessionId = null) {
        $this->updateAll(
            array('UsersSession.user_id' => $userId),
            array('UsersSession.id'      => "{$sessionId}")
        );
    }

    public function saveFirebase($sessionId = null){
        $HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false)); 
        $data = array('message' => __('Han iniciado sesión en otro ordenador.'),
                        'state' => Configure::read('ENABLED'));
        $HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.sesion').$sessionId.'.json', json_encode($data));

    }

    public function updateFirebase($sessionId = null){
        $HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false)); 
        $data = array('message' => __('Han iniciado sesión en otro ordenador.'),
                        'state' => Configure::read('DISABLED'));
        $HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.sesion').$sessionId.'.json', json_encode($data));

    }

}
