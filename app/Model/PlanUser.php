<?php
App::uses('AppModel', 'Model');

class PlanUser extends AppModel {

	public $belongsTo = array(
		'User' => array('className' => 'User','foreignKey' => 'user_id',),
		'Plan' => array('className' => 'Plan','foreignKey' => 'plan_id',)
	);

	public function buildConditions($params=array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = strtolower($params['q']);
			$conditions['OR'] = array(
				'LOWER(PlanUser.id) LIKE'=>"%{$params['q']}%",
			);
		}
		return $conditions;
	}

	public function savePlanUser($planId, $userId){
		$recursive 			 = -1;
		$conditions          = array("Plan.id" => $planId);
		$priceFirstPlanTobuy = $this->Plan->find("first", compact("conditions","recursive")); 
		$dateValidity = $this->calculateValidityNextYear(date('Y-m-d'));
		$dataPlanUser = array(
			"user_id"       => $userId,
			"plan_id"       => $planId,
			"price"         => $priceFirstPlanTobuy["Plan"]["price"] * configure::read("CONF_PLAN_TIME"),
			// "number_users"  => $priceFirstPlanTobuy["Plan"]["users"],
			"number_users"  => 10000,
			"buy_date"      => date('Y-m-d'),
			"validity"      => $dateValidity
		);
		$this->save($dataPlanUser); 
	}

	public function calculateValidityNextYear($dateStart){
		$dateValidity = date('Y-m-d', strtotime("+12 months $dateStart"));
		return $dateValidity;		 
	}

	public function calculateValidatePlanTrial($dateStart){

		$dateValidity = date('Y-m-d', strtotime("+18 year", strtotime($dateStart)));
		return $dateValidity;		 
	}

	public function savePlanTrial($planId, $userId){
		$conditions = array("PlanUser.user_id" => $userId, "NOT" => array("PlanUser.plan_id" => $planId));
		$fields     = array("PlanUser.id");
		$plansOld   = $this->find("list", compact("conditions","fields")); 
		$this->updateAll(
            array('PlanUser.state' => Configure::read('DISABLED'), 'PlanUser.expired' => Configure::read('DISABLED')), 
            array('PlanUser.id'    => $plansOld, 'PlanUser.user_id' => $userId)
        );
		$recursive    = -1;
		$conditions   = array("Plan.id" => $planId);
		$planTrial    = $this->Plan->find("first", compact("conditions","recursive"));
		$dateValidity = $this->calculateValidatePlanTrial(date('Y-m-d')); 

		$dataPlanUser = array(
			"user_id"       => $userId,
			"plan_id"       => $planId,
			"price"         => $planTrial["Plan"]["price"],
			"number_users"  => $planTrial["Plan"]["users"],
			"buy_date"      => date('Y-m-d'),
			"validity"      => $dateValidity
		); 
		$this->save($dataPlanUser);  
	}

	public function savePlanDefaultSuite($userId, $plan = array(), $end_date){
		$dataPlanUser = array(
			"user_id"       => $userId,
			"plan_id"       => $plan["Plan"]["id"],
			"price"         => $plan["Plan"]["price"],
			"number_users"  => $plan["Plan"]["users"],
			"buy_date"      => date('Y-m-d'),
			"validity"      => $end_date
		); 
		$this->save($dataPlanUser);
	}

	//PLAN ACTUAL PARA EL CREAR Y EDITAR ACTA
	public function getPlanActualUserContac(){
		$conditions   = array("PlanUser.user_id" => AuthComponent::user('id'), "PlanUser.state" => Configure::read('ENABLED'), 'PlanUser.expired' => configure::read("DISABLED")); 
		$planActual   = $this->find("first", compact("conditions"));
		return $planActual;
	}

}