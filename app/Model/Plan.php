<?php
App::uses('AppModel', 'Model');

class Plan extends AppModel {

	public $validate = array(
		'name'         => array(array('rule' => 'notBlank','message' => 'El nombre del plan es requerido.'),),
		'description'  => array(array('rule' => 'notBlank','message' => 'La descripción del plan es requerido.'),),
		'users' => array(
			array('rule' => 'notBlank','message' => 'El número de usuarios es requerido.'),
			array('rule' => 'numeric','message'  => 'El número de usuarios debe ser tipo numérico.'),
			array('rule' => 'validateNumberUsers','message'  => 'El número de usuarios debe ser mayor a cero.')
		),
		'price' => array(
			array('rule' => 'notBlank','message' => 'El precio es requerido.'),
			array('rule' => 'numeric','message'  => 'El precio debe de ser tipo numérico.'),
			array('rule' => 'validatePrice','message'     => 'El precio del plan debe ser mayor o igual a cero.'),
			array('rule' => 'validateMinPrice','message'  => 'El precio del plan  no puede ser menor a 3 dólares.'),
			array('rule' => 'validateMaxPrice','message'  => 'El precio del plan  no puede ser mayor a 3000 dólares.')
		),
		'white_brand' => array(
			array('rule' => 'notBlank','message' => 'Debes seleccionar si el plan tendrá marca blanca.')
		),
		'template' => array(
			array('rule' => 'notBlank','message' => 'Debes seleccionar si el plan tendrá permiso de crear una plantilla.')
		) 
	);
 
	public $hasOne = array( 
		'PlanUser' => array('className' => 'PlanUser','foreignKey'   => 'plan_id','dependent' => false,)
	);
	
 
	public $hasMany = array(
		'Transaction' => array('className' => 'Transaction','foreignKey' => 'plan_id','dependent' => false,),
		'Payment'     => array('className' => 'Payment','foreignKey' => 'plan_id','dependent' => false,)
	);

	public function buildConditions($params=array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q']      =  $this->normalizeChars($params['q']);
			$params['q']      =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				'LOWER(CONVERT(Plan.name USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Plan.description USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Plan.price USING latin1))  LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Plan.users USING latin1))  LIKE' => "%{$params['q']}%", 
			); 
		}
		return $conditions;
	}

	public function validateNumberUsers($data){ 
		$value = isset($data['users']) ? $data['users'] : null; 
		if ($value > 0){
            return true;
       	} 
   	 	return false;
	}

	public function validatePrice($data){ 
		$value = isset($data['price']) ? $data['price'] : null; 
		if ($value >= 0){
            return true;
       	} 
   	 	return false;
	}

	public function validateMinPrice($data){   
		if($this->data["Plan"]["trial"] == 1){
			return true;
		} else {
			$value = isset($data['price']) ? $data['price'] : null; 
			if ($value < 3){
	   	 		return false;
	       	} 
	        return true; 
		}
	}

	public function validateMaxPrice($data){ 
		$value = isset($data['price']) ? $data['price'] : null; 
		if ($value > 3000){
   	 		return false;
       	} 
        return true;
	} 

	public function validateValueGeneralPayu($valuePlan){
		if(!empty($valuePlan)){ 
	    	if($valuePlan > Configure::read("VALOR_MINIMO_PAYU")){
	    		$response = array("state" => true);
	    	} else {
	    		$response = array("message" => __("El valor debe superar mínimo los 3 dólares."), "state" => false);	
	    	}	    	
	    } else {
			$response = array("message" => __("Error en procesar la petición."), "state" => false);	
	    }
	    return $response;
	}

	public function getPlanActual(){ 
		$this->unbindModel(array('hasMany' => array('Transaction','Payment')));
		$conditions     = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("ENABLED"), "PlanUser.expired" => Configure::read("DISABLED"));
		$planActual     = $this->find("first", compact("conditions"));
		if(empty($planActual)){ 
			$this->unbindModel(array('hasMany' => array('Transaction','Payment'))); 
			$conditions  = array("PlanUser.user_id" => AuthComponent::user("id"), "PlanUser.state" => Configure::read("DISABLED"), "PlanUser.expired" => Configure::read("ENABLED"));
			$planExpired = $this->find("first", compact("conditions"));
			return $planExpired;
		} else {
			return $planActual;  
		}
	}

	public function validatePriceToPlanActualWithNewPlan($plan = array(), $newPriceToPlanToChange){
		$response = array(); 
		if ($newPriceToPlanToChange < $plan["PlanUser"]["price"] && $plan["PlanUser"]["expired"] == configure::read("DISABLED") && $plan["PlanUser"]["state"] == configure::read("ENABLED")) {
			$response = array("state" => false, "message" => __("El precio del nuevo plan que seleccionaste no puede ser menor que del plan actual.")); 
		} else {
			$response = array("state" => true, "price_old" => $plan["PlanUser"]["price"], "price_new" => $newPriceToPlanToChange, "trial" => $plan["Plan"]["trial"]);
		} 
		return $response; 
	}

	public function getPriceDependingDayTheUse($planActual = array(), $pricePlans = array()){
		$fecha_actual = date('Y-m-d');
       	$dias         = strtotime($fecha_actual) - strtotime($planActual['PlanUser']['buy_date']);
       	$dias         = $dias/86400; //Sacar los dias que se a utilizado el plan que tiene
       	$valorDia 	  = $pricePlans["price_old"] / 365; // Valor del dia del plan
       	if ($dias != 0) { 
       		$precioDescuento = $valorDia * $dias; // descuento para cobrar el otro plan
   			$precioCobrar    = $pricePlans["price_new"] - $precioDescuento;
       	}else{ 
   			if($pricePlans["trial"] == configure::read("ENABLED")){
   				$precioCobrar = $pricePlans["price_new"];
   			} else {
   				$precioCobrar = $pricePlans["price_new"]; 
   			}
       	} 
       	return number_format($precioCobrar,2,".", '');
	} 

	public function getJsonTransactionToChangePlan($requestQuery = array(), $transactionInfo = array()){
		$data['Transaction'] = array( 
			"id"					=> $transactionInfo["Transaction"]["id"],
			"value"                 => $transactionInfo["Transaction"]["value"], 
			"referenceCode"         => $requestQuery['reference_sale'], 
			"payment_method_type"   => $requestQuery['payment_method_type'],
			"payment_method_name"   => $requestQuery['payment_method_name'],
			"payment_request_state" => $requestQuery['payment_request_state'], 
			"payment_method" 		=> $requestQuery['payment_request_state'], 
			"state"                 => $requestQuery['response_message_pol'],
			"user_id"               => $transactionInfo["Transaction"]["user_id"],
			"email"                 => $transactionInfo["Transaction"]["email"],
			"plan_id"               => $transactionInfo["Transaction"]["plan_id"], 
			"created"               => date('Y-m-d H:i:s'),
			"notify"                => configure::read("DISABLED"),
			"transactionId"         => $requestQuery['transaction_id'] 
		);
		$userId = $data['Transaction']["user_id"]; 
		if($data['Transaction']["state"] == configure::read("TRANSACTION_APROBADA")){
			$this->__saveTransaction($data);			 		
		} else if ($data['Transaction']["state"] == configure::read("TRANSACTION_RECHAZADA")) {
			$this->Transaction->save($data);
			Cache::delete("transactions_{$userId}",'TRANSACTION_PENDINGS');
			Cache::write("planAdquired_{$userId}", $data, 'PLAN_ADQUIRED');
		}
		return $data; 
	}

	private function __saveTransaction($data){
		$userId = $data['Transaction']['user_id']; 
		$this->Transaction->save($data);
		$transactionId = $this->Transaction->id;	 
		$this->__savePayment($data, $transactionId);
		Cache::delete("transactions_{$userId}",'TRANSACTION_PENDINGS');  
	} 

	private function __savePayment($data, $transactionId){
		$saveData = array();
		$saveData = array(
			"value"          => $data['Transaction']['value'],  
			"user_id"        => $data['Transaction']['user_id'],
			"plan_id"        => $data['Transaction']['plan_id'],
			"transaction_id" => $transactionId
		);
		$this->Payment->save($saveData);
		$this->__changeToPlanNew($data);   
	}  

 	private function __changeToPlanNew($data){ 
 		$userId      = $data['Transaction']['user_id']; 
 		$planInfo    = Cache::read("plans_{$userId}", "PLANS"); 
 		$conditions  = array("Plan.id" => $planInfo["plan_id_new"]);
 		$numberUsers = $this->field("users", $conditions);
 		$this->PlanUser->updateAll(
            array('PlanUser.state' => Configure::read('DISABLED'), 'PlanUser.expired' => Configure::read('DISABLED')), 
            array('Plan.id'        => $planInfo["plan_id_old"], 'PlanUser.user_id' => $userId)
        );
        $dateValidity     = $this->PlanUser->calculateValidityNextYear(date('Y-m-d'));
        $saveDataPlanUser = array(
        	"user_id"      => $userId,
        	"plan_id"      => $planInfo["plan_id_new"],
        	"number_users" => $numberUsers,
        	"price" 	   => $planInfo["plan_value_new"],
        	"buy_date" 	   => date('Y-m-d'),
        	"validity" 	   => $dateValidity,
        	"state" 	   => Configure::read('ENABLED'),
    	);
    	$this->PlanUser->save($saveDataPlanUser);
    	Cache::write("planAdquired_{$userId}", 2, 'PLAN_ADQUIRED'); 
    	Cache::delete("plans_{$userId}", "PLANS");  
 	}

	public function savePlanGift($couponInfo = array()){
		$saveData = array(
			"user_id"      => AuthComponent::user("id"),
			"plan_id"      => $couponInfo["Detail"]["Plan"]["id"],
			"number_users" => $couponInfo["Detail"]["Plan"]["users"],
			"price"        => $couponInfo["Detail"]["Plan"]["price"],
			"buy_date"     => date('Y-m-d'),
			"validity"     => $couponInfo["Detail"]["Coupon"]["end_date"],
			"state"        => Configure::read("ENABLED")
		);
		$this->__activateMemberTeam(AuthComponent::user("id"));
		$this->PlanUser->save($saveData); 
	}


	private function __activateMemberTeam($userId){
		$conditions = array('PlanUser.user_id' => $userId);
		$countPlan  = $this->PlanUser->find("count", compact("conditions"));
		if($countPlan == 0){
			$Team     = ClassRegistry::init('Team');
			$UserTeam = ClassRegistry::init('UserTeam'); 
			$recursive  = -1;
			$conditions = array("Team.user_id" => $userId);
			$teams      = $Team->find("all", compact("conditions","recursive"));
			$this->PlanUser->User->updateAll(
            	array('User.step_initial' => Configure::read('ENABLED')), 
            	array('User.id'   		  => $userId)
   		 	);
			if(!empty($teams)){
				foreach ($teams as $team) {
				 	$data[] = array(
				 		"user_id" 	  => $userId,
				 		"own_user_id" => $userId,
				 		"type_user"   => Configure::read("ENABLED"),
				 		"position_id" => 1, //Id del permiso "Control total"
				 		"client_id"   => NULL,
				 		"team_id"     => $team["Team"]["id"]
				 	);
				} 
				$UserTeam->saveAll($data, array('validate'=>false)); 
			}
		} 
	} 

}