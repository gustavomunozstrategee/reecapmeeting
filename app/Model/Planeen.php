<?php
App::uses('AppModel', 'Model');

class Planeen extends AppModel {

	public $belongsTo = array(
		'User' => array('className' => 'User','foreignKey' => 'user_id',)
	);
	public $hasMany = array(
		'PlaneensUser' => array(
			'className' => 'PlaneensUser',
			'foreignKey' => 'planeen_id',
			'dependent' => false
		),
		'PlaneensTask' => array(
			'className' => 'PlaneensTask',
			'foreignKey' => 'planeen_id',
			'dependent' => false
		),
	);

	public function buildConditions($params=array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = strtolower($params['q']);
			$conditions['OR'] = array(
				'LOWER(List.id) LIKE'=>"%{$params['q']}%",
			);
		}
		return $conditions;
	}

}
