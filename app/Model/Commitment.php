<?php
App::uses('AppModel', 'Model');

class Commitment extends AppModel {

	public $virtualFields = array('name'=> "Commitment.description");

	public $validate = array(
		'user_id'       => array('notBlank' => array('rule' => array('notBlank'),'message' => 'El usuario de los compromisos es requerido'),),
		'description'   => array('notBlank' => array('rule' => array('notBlank'),'message' => 'La descripción de los compromisos es requerida'),),
		'delivery_date' => array('notBlank' => array('rule' => array('notBlank'),'message' => 'La fecha límite del compromiso es requerida'),),
    	'project_id' => array('notBlank' => array('rule' => array('notBlank'),'message' => 'Debe seleccionar un proyecto'),),
	);

	public $belongsTo = array(
		'Contac'   => array('className' => 'Contac',  'foreignKey'  => 'contac_id',), 
		'Team'     => array('className' => 'Team',    'foreignKey'  => 'team_id',), 
		'User'     => array('className' => 'User',    'foreignKey'  => 'user_id',),
		'Project'  => array('className' => 'Project', 'foreignKey'  => 'project_id',),
		'Client'  => array('className' => 'Client', 'foreignKey'  => 'client_id',),
	);

	public function bindUserTeam(){
		$binds = array('hasOne'=>array(	
			'UserTeam' => array('className' => 'UserTeam','conditions'=> array('UserTeam.user_id = Commitment.user_id',"UserTeam.team_id = Commitment.team_id"), 'foreignKey' => false, 'type' => 'LEFT'), 
		));
		$this->bindModel($binds);
	}

	public function buildConditions($params=array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = strtolower($params['q']);
			$conditions['OR'] = array(
				'LOWER(Commitment.id) LIKE'=>"%{$params['q']}%",
			);
		}
		if(!empty($params['client_id'])){
			$conditions['Project.client_id'] = $params['client_id'];
		}
		if(!empty($params['teams'])){
			$conditions['Contac.team_id'] = $params['teams'];
		} 
		return $conditions;
	} 

	public function getUserWithCommitments($commitments = array()){
		$usersCommitments 	  = array();
		$employeesCommitments = array();
		if(!empty($commitments)){
			foreach ($commitments as $commitment) { 
				$usersCommitments[$commitment["User"]["id"]]["email"] = $commitment["User"]["email"];				 			 
				$usersCommitments[$commitment["User"]["id"]]["lang"]  = $commitment["User"]["lang"];				 			 
				$usersCommitments[$commitment["User"]["id"]]["commitments"][] = $commitment["Commitment"]; 
			}
			return array("usersCommitments" => $usersCommitments);
		} 
	}

	public function saveLogCommitmentDone($commitment = array()){
		$conditions     = array("User.id" => $commitment["Commitment"]["user_id"]);
		$firstName      = $this->User->field("firstname", $conditions);
		$lastname       = $this->User->field("lastname", $conditions);
		$full_name      = $firstName. ' ' .$lastname;
		$description    = sprintf(__("El funcionario %s ha marcado como realizado su compromiso %s."), $full_name, $commitment["Commitment"]["description"]);
		$descriptionEng = sprintf(__("The employee %s has marked as done his commitment %s."), $full_name, $commitment["Commitment"]["description"]);
		$this->buildInfoLog($commitment["Commitment"]["id"], $description, $descriptionEng, NULL, NULL, NULL, NULL, NULL);  
	}

	private function __updateBussinesUserCommitmentLog($commitment = array()){
		$conditions   = array("Contac.id" => $commitment["Commitment"]["contac_id"]);
		$userId       = $this->Contac->field("user_id", $conditions);
		$conditions   = array("User.id" => $userId);
		$bussinesUser = $this->User->field("bussines_user", $conditions);
		$log = ClassRegistry::init('Log'); 
		$log->updateAll(
            array('Log.bussines_user' => $bussinesUser),
            array('Log.registry_id' => $commitment["Commitment"]["id"], 'Log.user_id' => NULL, 'Log.bussines_user' => NULL, 'Log.action' => "mark_commitment")
    	);
	}

	public function saveLogsCommitmentsUserInSession($commitmentIds = array()){
		$recursive   = 0;
		$conditions  = array("Commitment.id" => $commitmentIds);
		$commitments = $this->find("all", compact("conditions","recursive"));
		$full_name   = AuthComponent::user("firstname") . ' ' .AuthComponent::user("lastname");
		if(!empty($commitments)){
			foreach ($commitments as $commitment) {
				$description    = sprintf(__("El funcionario %s ha marcado como realizado su compromiso %s."), $full_name, $commitment["Commitment"]["description"]);
				$descriptionEng = sprintf(__("The employee %s has marked as done his commitment %s."), $full_name, $commitment["Commitment"]["description"]);
	 			$this->buildInfoLog($commitment["Commitment"]["id"], $description, $descriptionEng, NULL, NULL, NULL, $commitment["Contac"]["id"], $commitment["Contac"]["team_id"]); 
			} 			
		}
	}

	public function commitmentsCreatedsFirebaseNotification($contacId){
		$recursive   = -1;
		$group 		 = array("Commitment.user_id");
	 	$fields      = array("Commitment.user_id","COUNT(Commitment.id) as totalCommmitments");	
		$conditions  = array("Commitment.contac_id" => $contacId);
		$commitments = $this->find("all", compact("conditions","recursive","group","fields")); 
		if(!empty($commitments)){
			foreach ($commitments as $commitment) {
				$url    = "commitments/index/";
				$msgEsp = sprintf("Tienes %s compromiso(s) por realizar.", $commitment[0]['totalCommmitments']);
			 	$msgEng = __("You have")." ".$commitment[0]['totalCommmitments']." ".__("commitment(s) to do.");
		 		$this->notificacionesUsuarios($commitment['Commitment']['user_id'], $msgEsp, $msgEng, $url);
			}
		} 
	} 


	//ACTUALIZAR COMPROMISOS FIREBASE
	public function update_commitments_firebase($commitments = array(), $contacId, $idFirebase){ 
     
		$conditions = array("Contac.id" => $contacId);
    
	   $teamId 	= $this->Contac->field("team_id", $conditions);
		
		if(!empty($commitments)){
			$datos = $firebaseCommitment = array();
			foreach ($commitments as $key => $commitment) {	
				if(!empty($commitment['asistente']) && !empty($commitment['fecha']) && !empty($commitment['description'])) {
					$datos['Commitment']['user_id'] 	  = $commitment['asistente'];
					$datos['Commitment']['delivery_date'] = $commitment['fecha'];
					$datos['Commitment']['description']   = $commitment['description'];
					$datos['Commitment']['contac_id']     = $contacId;
					$datos['Commitment']['team_id']       = $teamId;
					$this->create();
					$this->save($datos);
					$firebaseCommitment[$this->id] = $this->__updateCommitmentsFirebase($this->id, $commitment); 
				}			 
			} 
			if(!empty($firebaseCommitment)) {
				$this->Contac->logCommitmentsContacBorrador($contacId);
				$this->updateFirebaseContacCommitment($idFirebase, "commitments", $firebaseCommitment);   
			}
		} 
	}

	private function __updateCommitmentsFirebase($commitmentId, $commitment){
		$firebaseCommitment = array(
			"asistente"   => $commitment['asistente'],
			"fecha"		  => $commitment['fecha'],
			"description" => $commitment['description'],
			"id"          => $commitmentId
		);  
		return $firebaseCommitment;
	}

	public function updateFirebaseContacCommitment($identificador = null, $campo, $dato){
    	if($identificador != null){ 
			$HttpSocket = new HttpSocket(array("ssl_verify_peer" => false, 'ssl_verify_host' => false, "ssl_allow_self_signed" => false));
    	 	$HttpSocket->put(Configure::read('Firebase.Url').Configure::read('Firebase.contact').$identificador.'/'.$campo.'.json', json_encode($dato));
	    }
    }

    public function commitmentsActual($contacId){
    	$this->unbindModel(array('belongsTo' => array('Client','Project')));
    	$conditions  = array("Commitment.contac_id" => $contacId);
    	$commitments = $this->find("all", compact("conditions")); 
    	return $commitments; 
    }	

    public function compareCommitmentsNewLog($contacId, $commitmentsActual){
    	$commitments    = $this->commitmentsActual($contacId);
    	$log 			= ClassRegistry::init('Log');
    	$conditions     = array("Log.action" => "logCommitmentsContac", "Log.user_id" => AuthComponent::user("id"), "Log.contac_id" => $contacId);
    	$logExist       = $log->find("all", compact("conditions")); 
    	$logNew         = array();
    	if(!empty($logExist)){
			try {
	    		foreach ($commitments as $commitment) {
					$data = array("date" => $commitment["Commitment"]["delivery_date"], "user" => $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"], "description" => $commitment["Commitment"]["description"]);
					$log->updateAll(
		            	array('Log.after_edit' => "'" . addslashes(json_encode($data)) . "'", 'Log.modified' => "'" . date('Y-m-d H:i:s') . "'"),
		            	array('Log.registry_id' => $commitment["Commitment"]["id"], 'Log.contac_id' => $contacId, "Log.action" => "logCommitmentsContac")
		    		); 
				}  				
			} catch (Exception $e) {}
    	} else {
    		$this->__createLogNewUserEditCommitment($commitmentsActual, $commitments, $contacId); 
    	} 
    } 

    private function __createLogNewUserEditCommitment($commitmentsActual, $commitments, $contacId){
    	foreach ($commitmentsActual as $commitment) {
			$data           = array("date" => $commitment["Commitment"]["delivery_date"], "user" => $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"], "description" => $commitment["Commitment"]["description"]); 
			$full_name      = AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname");
			$description    = sprintf(__("El funcionario %s ha editado un compromiso %s."), $full_name, $commitment["Commitment"]["description"]);
			$descriptionEng = sprintf(__("The employee %s has edit a commitment %s."), $full_name, $commitment["Commitment"]["description"]);
			$this->buildInfoLog($commitment["Commitment"]["id"], $description, $descriptionEng, addslashes(json_encode($data)), __("sin modificaciones"), "logCommitmentsContac", $contacId, $commitment["Contac"]["team_id"]);
		} 
		foreach ($commitments as $commitment) {
			$log  = ClassRegistry::init('Log');
			$data = array("date" => $commitment["Commitment"]["delivery_date"], "user" => $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"], "description" => $commitment["Commitment"]["description"]);
			$log->updateAll(
            	array('Log.after_edit' => "'" . addslashes(json_encode($data)) . "'", 'Log.modified' => "'" . date('Y-m-d H:i:s') . "'"),
            	array('Log.registry_id' => $commitment["Commitment"]["id"], 'Log.contac_id' => $contacId, "Log.action" => "logCommitmentsContac")
    		); 
		}  
    }

    public function bindModelTeam(){
    	$binds = array('hasOne'=>array(	
			'Team' => array('className' => 'Team','conditions'=> array('Team.id = Contac.team_id'), 'foreignKey'=>false), 
		));
		$this->bindModel($binds);
    }

	public function apiCommitmentConditions($params = array()) {
		$conditions = array();
		if(!empty($params['user_id'])) {
			$conditions['Commitment.user_id'] = $params['user_id'];
		}
		if(!empty($params['state'])) {
			$conditions['Commitment.state'] = $params['state'];
		}
		if(!empty($params['hight_priority'])) {
			$conditions['Commitment.priority'] = Configure::read('HIGHT_PRIORITY');
		}
		if(!empty($params['pending_commitments'])) {
			$conditions['Commitment.state'] = Configure::read('ENABLED');
		}
		if(!empty($params['q'])) {
			$params['q'] = Encryption::normalizeChars($params['q']);
			$params['q'] = ltrim(rtrim(strtolower($params['q'])));
			$conditions['OR'] = array(
				'CONVERT(LOWER(Commitment.description) USING latin1) LIKE' => "%{$params['q']}%",
			); 
		}
		return $conditions;
	}

	public function orderCommitmentApiList($params = array()) {
		$order = array(
			'Commitment.state'    => 'ASC',
			'Commitment.priority' => 'ASC',
			"ABS(DATEDIFF(CONCAT(Commitment.deadline_date,' ',Commitment.deadline_hour), NOW()))"
		);
		return $order;
	}

}
