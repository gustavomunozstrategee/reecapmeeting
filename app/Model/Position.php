<?php
App::uses('AppModel', 'Model');
 
class Position extends AppModel {
 
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'El nombre del rol es requerido.', 
			),
		), 
	);
 
	public $belongsTo = array(
		'User' => array(
			'className'  => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		)
	);
 
	public $hasMany = array(
		'UserTeam' => array(
			'className'    => 'UserTeam',
			'foreignKey'   => 'position_id',
			'dependent'    => false,
			'conditions'   => '',
			'fields' 	   => '',
			'order' 	   => '',
			'limit' 	   => '',
			'offset' 	   => '',
			'exclusive'    => '',
			'finderQuery'  => '',
			'counterQuery' => ''
		) 
	); 
 
	public $hasAndBelongsToMany = array(
		'Restriction' => array(
			'className' => 'Restriction',
			'joinTable' => 'positions_restrictions',
			'foreignKey' => 'position_id',
			'associationForeignKey' => 'restriction_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	); 

	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				'LOWER(CONVERT(Position.name USING latin1))        LIKE' => "%{$params['q']}%", 
			); 
		}  
		return $conditions;
	} 

	public function loadAllPositions($team_id =null){
		// if (!is_null($team_id)) {
		// 	$conditions    = array("Position.user_id" => AuthComponent::user("id"),"Position.team_id" => $team_id, "NOT" => array("Position.id" => 1));
		// }else{
		// 	$conditions    = array("Position.user_id" => AuthComponent::user("id"),"NOT" => array("Position.id" => 1));
		// }
		$recursive     = -1;
		$conditions    = array('Position.team_id' => $team_id);
		$fields        = array('Position.id','Position.name');
		$positionsUser = $this->find('list', compact('fields','conditions','recursive'));
		return $positionsUser;
	}

	public function getTotalPosition(){
		$limit      = 1;
		$conditions = array('Position.team_id' => Configure::read('Application.team_id'));
		$count      = $this->find('count', compact('conditions','limit'));
		return $count;
	} 
}
