<?php
App::uses('AppModel', 'Model');

class QualificationContact extends AppModel {

	public $displayField = 'id';


	public $belongsTo = array(
		'User' => array('className' => 'User','foreignKey' => 'user_id',), 
		'Contac' => array('className' => 'Contac','foreignKey' => 'contac_id',)
	);

	public function buildConditions($params=array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = strtolower($params['q']);
			$conditions['OR'] = array(
				'LOWER(QualificationContact.id) LIKE'=>"%{$params['q']}%",
			);
		}
		return $conditions;
	}

}
