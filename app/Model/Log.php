<?php
App::uses('AppModel', 'Model');
 
class Log extends AppModel { 
 
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
	); 

	public function buildConditions($params = array()) { 
		$conditions = array();
		if(!empty($params['date'])) {
			$params['date'] = trim($params['date']);
			$params['date'] = strtolower($params['date']);
			$conditions = array('OR' => array(
				'Log.date ='  => "{$params['date']}" 
			));
		} 
		if(!empty($params['user_id'])) {
		  $conditions['Log.user_id'] = $params['user_id'];
		}
		if(!empty($params['model'])) {
		  $conditions['Log.model'] = $params['model'];
		} 
		if(!empty($params['teams'])) {
		  $conditions['Log.team_id'] = $params['teams'];
		}   
		return $conditions;
	}
}
