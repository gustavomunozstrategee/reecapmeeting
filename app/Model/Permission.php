<?php
App::uses('AppModel', 'Model');

class Permission extends AppModel {

	public $hasMany = array(
		'Menu' => array('className' => 'Menu','foreignKey' => 'permission_id','dependent' => false,)
	);
 
	public $hasAndBelongsToMany = array(
		'Plan' => array(
			'className' => 'Plan',
			'joinTable' => 'plans_permissions',
			'foreignKey' => 'permission_id',
			'associationForeignKey' => 'plan_id',
			'unique' => 'keepExisting',
		)
	);


}
