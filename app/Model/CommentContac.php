<?php
App::uses('AppModel', 'Model');
 
class CommentContac extends AppModel {
 
	public $validate = array(
		'comment' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'El comentario es requerido.', 
			),
		), 
	); 

	public $belongsTo = array(
		'Contac' => array(
			'className' => 'Contac',
			'foreignKey' => 'contac_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
