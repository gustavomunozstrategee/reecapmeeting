<?php
App::uses('AuthComponent', 'Controller/Component'); 
App::uses('AppModel', 'Model'); 

class ContactsUs extends AppModel {
	public $useTable = false;
 

	public $validate = array(
		'name_conctact' => array(
			array('rule' => 'notBlank','message' => 'El nombre es requerido.'),
			array('rule' => '/^\D+$/', 'message' => 'El nombre debe tener solo letras.')
		),
		'lastname'      => array(
			array('rule' => 'notBlank','message' => 'El apellido es requerido.'),
			array('rule' => '/^\D+$/', 'message' => 'El apellido debe tener solo letras.')
		),  
		'business'      => array(array('rule' => 'notBlank','message' => 'El nombre de la empresa es requerido.'),), 
		'number_user'   => array(array('rule' => 'notBlank','message' => 'La cantidad de usuarios es requerida.'),),  
		'phone' => array(
			array('rule' => 'notBlank','message' => 'El teléfono es requerido.'),
			array('rule' => 'numeric','message'  => 'El teléfono debe de ser tipo numérico.'),
			array('rule' => array('minLength', '7'),'message' => 'El teléfono debe tener mínimo 7 caracteres.'), 
		),
		'number_user' => array(
			array('rule' => 'notBlank','message' => 'La cantidad de usuarios es requerida.'),
			array('rule' => 'numeric','message'  => 'La cantidad de usuarios debe de ser tipo numérico.'),
			array('rule' => 'validateNumberUsers','message'  => 'La cantidad de usuarios debe ser mayor a 30.'),
		),
		'country' 		=> array(array('rule' => 'notBlank','message' => 'El país es requerido.'),),  
		'city' 		    => array(array('rule' => 'notBlank','message' => 'La ciudad es requerida.'),),  
		'subject' 		=> array(array('rule' => 'notBlank','message' => 'El asunto es requerido.'),
			// array('rule' => array('maxLength', '300'),'message' => 'El asunto debe tener máximo 300 caracteres.'),
			array('rule' => array('countMaxLength'),'message' => 'El asunto debe tener máximo 300 caracteres.'), 

		),  
		'email'     => array(
			array(
				'rule'     => 'notBlank',
				'on'       => 'create',
				'required' => true,
				'message'  => 'Por favor ingrese un correo electrónico.'
			),
			array('rule' => 'email','message'    => 'Ingrese una dirección de correo electrónico válida.'), 
		), 
	); 

	public function validateNumberUsers(){ 
		if ($this->data["ContactsUs"]["number_user"] > 30){
            return true;
       	} 
   	 	return false;
	}

	public function countMaxLength($data){
		$text = str_replace(array("\r\n","\r"), "", $data["subject"]);
		if($text > 300){
			return false;
		} else {
			return true;
		} 
	}
}
