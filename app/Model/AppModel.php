<?php
 
App::uses('Model', 'Model');
 
class AppModel extends Model {

    public function renameFile($field, $currentName, $data, $options = array()) { 
        $rand_v2     = uniqid();
        $nameContent = explode(".", $currentName);
        $ext         = end($nameContent);
        $newName     = $this->alias."_{$rand_v2}.{$ext}";
        if($this->alias == "ContacsDocument"){ 
            $newName     =  "ActaDocument_{$rand_v2}.{$ext}";
        }  
        return $newName;
    }

    public function validateEmail($email){
        $validate = false;
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $validate = true;
        }
        return $validate;
    }

    public function generatePassword(){
        $rand    = mt_rand(1000,9999);
        return $rand;
    }

    public function buildInfoLog($registryId, $description, $descriptionEng, $beforeEdit = null, $afterEdit = null, $defaultAction = null, $contacId = null, $teamId = null){  
        $log = ClassRegistry::init('Log');
        $data["Log"] = array(
            "controller"      => Router::getParams()["controller"],
            "action"          => isset($defaultAction) ? $defaultAction : Router::getParams()["action"],
            "model"           => $this->name,
            "registry_id"     => $registryId,
            "contac_id"       => isset($contacId)   ? $contacId : NULL,
            "description"     => $description,
            "description_eng" => $descriptionEng,
            "before_edit"     => isset($beforeEdit) ? $beforeEdit : NULL,
            "after_edit"      => isset($afterEdit)  ? $afterEdit  : NULL,
            "user_id"         => !empty(AuthComponent::user("id")) ? AuthComponent::user("id"): NULL,
            "team_id"         => $teamId,
            "date"            => date("Y-m-d"),
            "created"         => date("Y-m-d H:i:s")
        ); 
        $log->create();
        $log->save($data);
        return $log->id; 
    } 

    public function notificacionesUsuarios($userId, $msgEsp, $msgEng, $type = null){ 
        $User = ClassRegistry::init('User'); 
        $nodoPrincipal        = 'NOT'.uniqid();
        $datos['mensaje']     = $msgEsp;
        $datos['mensaje_eng'] = $msgEng;
        $datos['nodo_id']     = $nodoPrincipal;
        $datos['state']       = Configure::read('ENABLED');
        $datos['type']        = $type;
        $User->saveFirebaseNotificaciones($userId, $nodoPrincipal, $datos);
    }

    public function checkWordCharactersCommitments($commitments = array()){
        if(isset($commitments["Commitments"]["NUEVO"])){
            unset($commitments["Commitments"]["NUEVO"]); 
        } 
        $response = true;
        foreach ($commitments as $commitment) { 
            $description = str_replace(array("<p>","</p>","<ul>","</ul>","<li>","</li>","<ol>","</ol>","&nbsp;"), array('',' ','',' ','',' ','',' ',''), strip_tags($commitment["description"], "<p><ul><li><ol>"));  
            $words       = explode(" ", $description);  
            foreach ($words as $word) { 
                if(!empty($word)){
                    if(strlen($word) > 80){ 
                        $response = false; 
                        break;
                    }  
                }
            }  
        }
        return $response;  
    } 

    public function normalizeChars($string) {
        $replace = array(
            'ъ'=>'-', 'Ь'=>'-', 'Ъ'=>'-', 'ь'=>'-',
            'Ă'=>'A', 'Ą'=>'A', 'À'=>'A', 'Ã'=>'A', 'Á'=>'A', 'Æ'=>'A', 'Â'=>'A', 'Å'=>'A', 'Ä'=>'Ae',
            'Þ'=>'B',
            'Ć'=>'C', 'ץ'=>'C', 'Ç'=>'C',
            'È'=>'E', 'Ę'=>'E', 'É'=>'E', 'Ë'=>'E', 'Ê'=>'E',
            'Ğ'=>'G',
            'İ'=>'I', 'Ï'=>'I', 'Î'=>'I', 'Í'=>'I', 'Ì'=>'I',
            'Ł'=>'L',
            'Ñ'=>'N', 'Ń'=>'N',
            'Ø'=>'O', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe',
            'Ş'=>'S', 'Ś'=>'S', 'Ș'=>'S', 'Š'=>'S',
            'Ț'=>'T',
            'Ù'=>'U', 'Û'=>'U', 'Ú'=>'U', 'Ü'=>'Ue',
            'Ý'=>'Y',
            'Ź'=>'Z', 'Ž'=>'Z', 'Ż'=>'Z',
            'â'=>'a', 'ǎ'=>'a', 'ą'=>'a', 'á'=>'a', 'ă'=>'a', 'ã'=>'a', 'Ǎ'=>'a', 'а'=>'a', 'А'=>'a', 'å'=>'a', 'à'=>'a', 'א'=>'a', 'Ǻ'=>'a', 'Ā'=>'a', 'ǻ'=>'a', 'ā'=>'a', 'ä'=>'ae', 'æ'=>'ae', 'Ǽ'=>'ae', 'ǽ'=>'ae',
            'б'=>'b', 'ב'=>'b', 'Б'=>'b', 'þ'=>'b',
            'ĉ'=>'c', 'Ĉ'=>'c', 'Ċ'=>'c', 'ć'=>'c', 'ç'=>'c', 'ц'=>'c', 'צ'=>'c', 'ċ'=>'c', 'Ц'=>'c', 'Č'=>'c', 'č'=>'c', 'Ч'=>'ch', 'ч'=>'ch',
            'ד'=>'d', 'ď'=>'d', 'Đ'=>'d', 'Ď'=>'d', 'đ'=>'d', 
            'ф'=>'f', 'ƒ'=>'f', 'Ф'=>'f','д'=>'d', 'Д'=>'D', 'ð'=>'d',
            'є'=>'e', 'ע'=>'e', 'е'=>'e', 'Е'=>'e', 'Ə'=>'e', 'ę'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'Ē'=>'e', 'Ė'=>'e', 'ė'=>'e', 'ě'=>'e', 'Ě'=>'e', 'Є'=>'e', 'Ĕ'=>'e', 'ê'=>'e', 'ə'=>'e', 'è'=>'e', 'ë'=>'e', 'é'=>'e',
            'ġ'=>'g', 'Ģ'=>'g', 'Ġ'=>'g', 'Ĝ'=>'g', 'Г'=>'g', 'г'=>'g', 'ĝ'=>'g', 'ğ'=>'g', 'ג'=>'g', 'Ґ'=>'g', 'ґ'=>'g', 'ģ'=>'g',
            'ח'=>'h', 'ħ'=>'h', 'Х'=>'h', 'Ħ'=>'h', 'Ĥ'=>'h', 'ĥ'=>'h', 'х'=>'h', 'ה'=>'h',
            'î'=>'i', 'ï'=>'i', 'í'=>'i', 'ì'=>'i', 'į'=>'i', 'ĭ'=>'i', 'ı'=>'i', 'Ĭ'=>'i', 'И'=>'i', 'ĩ'=>'i', 'ǐ'=>'i', 'Ĩ'=>'i', 'Ǐ'=>'i', 'и'=>'i', 'Į'=>'i', 'י'=>'i', 'Ї'=>'i', 'Ī'=>'i', 'І'=>'i', 'ї'=>'i', 'і'=>'i', 'ī'=>'i', 'ĳ'=>'ij', 'Ĳ'=>'ij',
            'й'=>'j', 'Й'=>'j', 'Ĵ'=>'j', 'ĵ'=>'j', 'я'=>'ja', 'Я'=>'ja', 'Э'=>'je', 'э'=>'je', 'ё'=>'jo', 'Ё'=>'jo', 'ю'=>'ju', 'Ю'=>'ju',
            'ĸ'=>'k', 'כ'=>'k', 'Ķ'=>'k', 'К'=>'k', 'к'=>'k', 'ķ'=>'k', 'ך'=>'k',
            'Ŀ'=>'l', 'ŀ'=>'l', 'Л'=>'l', 'ł'=>'l', 'ļ'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l', 'Ļ'=>'l', 'л'=>'l', 'Ľ'=>'l', 'ľ'=>'l', 'ל'=>'l',
            'מ'=>'m', 'М'=>'m', 'ם'=>'m', 'м'=>'m',
            'ñ'=>'n', 'н'=>'n', 'Ņ'=>'n', 'ן'=>'n', 'ŋ'=>'n', 'נ'=>'n', 'Н'=>'n', 'ń'=>'n', 'Ŋ'=>'n', 'ņ'=>'n', 'ŉ'=>'n', 'Ň'=>'n', 'ň'=>'n',
            'о'=>'o', 'О'=>'o', 'ő'=>'o', 'õ'=>'o', 'ô'=>'o', 'Ő'=>'o', 'ŏ'=>'o', 'Ŏ'=>'o', 'Ō'=>'o', 'ō'=>'o', 'ø'=>'o', 'ǿ'=>'o', 'ǒ'=>'o', 'ò'=>'o', 'Ǿ'=>'o', 'Ǒ'=>'o', 'ơ'=>'o', 'ó'=>'o', 'Ơ'=>'o', 'œ'=>'oe', 'Œ'=>'oe', 'ö'=>'oe',
            'פ'=>'p', 'ף'=>'p', 'п'=>'p', 'П'=>'p',
            'ק'=>'q',
            'ŕ'=>'r', 'ř'=>'r', 'Ř'=>'r', 'ŗ'=>'r', 'Ŗ'=>'r', 'ר'=>'r', 'Ŕ'=>'r', 'Р'=>'r', 'р'=>'r',
            'ș'=>'s', 'с'=>'s', 'Ŝ'=>'s', 'š'=>'s', 'ś'=>'s', 'ס'=>'s', 'ş'=>'s', 'С'=>'s', 'ŝ'=>'s', 'Щ'=>'sch', 'щ'=>'sch', 'ш'=>'sh', 'Ш'=>'sh', 'ß'=>'ss',
            'т'=>'t', 'ט'=>'t', 'ŧ'=>'t', 'ת'=>'t', 'ť'=>'t', 'ţ'=>'t', 'Ţ'=>'t', 'Т'=>'t', 'ț'=>'t', 'Ŧ'=>'t', 'Ť'=>'t', '™'=>'tm',
            'ū'=>'u', 'у'=>'u', 'Ũ'=>'u', 'ũ'=>'u', 'Ư'=>'u', 'ư'=>'u', 'Ū'=>'u', 'Ǔ'=>'u', 'ų'=>'u', 'Ų'=>'u', 'ŭ'=>'u', 'Ŭ'=>'u', 'Ů'=>'u', 'ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'Ǖ'=>'u', 'ǔ'=>'u', 'Ǜ'=>'u', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'У'=>'u', 'ǚ'=>'u', 'ǜ'=>'u', 'Ǚ'=>'u', 'Ǘ'=>'u', 'ǖ'=>'u', 'ǘ'=>'u', 'ü'=>'ue',
            'в'=>'v', 'ו'=>'v', 'В'=>'v',
            'ש'=>'w', 'ŵ'=>'w', 'Ŵ'=>'w',
            'ы'=>'y', 'ŷ'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'Ÿ'=>'y', 'Ŷ'=>'y',
            'Ы'=>'y', 'ž'=>'z', 'З'=>'z', 'з'=>'z', 'ź'=>'z', 'ז'=>'z', 'ż'=>'z', 'ſ'=>'z', 'Ж'=>'zh', 'ж'=>'zh'
        );
        return strtr($string, $replace);
    }

    public function validateMimeTypeImg($dataImage){  
      $imgCheck   = array();
      $imgCheck   = $dataImage;
      $models     = array_keys($imgCheck);
      $img        = end($models);  
      if($imgCheck[$img]["error"] == "4"){
          return true;
      }  
      App::Import('Utility', 'Validation'); 
        if(!empty($imgCheck[$img]["error"] != "4")){ 
            if(!Validation::mimeType($imgCheck[$img], array("image/png","image/jpeg","image/jpg","image/gif"))){  
                return false;
            } else {
              return true;
            }            
        }   
    }

    public function mobileRenameFile($field, $currentName, $data, $options = array()) {

        $randV1 = uniqid();
        $randV2 = uniqid();
        
        /*proceso para conocer la extencion del archivo, se debe hacer asi
        porque el iphone aveces no dice la extension del archivo 
        y se debe buscar la forma de conocer la extension*/
        if(!empty($data[$this->alias][$field]) && is_array($data[$this->alias][$field])) {          
            App::uses('File', 'Utility');
            $File = new File($data[$this->alias][$field]['tmp_name']);
            $ext = $File->ext();
        }
        if(empty($ext) && !empty($data[$this->alias]['file_extension'])) {
            $ext = strtolower($data[$this->alias]['file_extension']);

        }elseif(empty($ext) && !empty($data[$this->alias]['real_filename'])) {
            $nameContent = explode(".", $data[$this->alias]['real_filename']);
            $ext = end(strtolower($nameContent));
        
        }elseif(empty($ext)) {
            $nameContent = explode(".", $currentName);
            $ext = end($nameContent);
        }

        $newName = $this->alias."{$randV1}_{$randV2}.{$ext}";
        return $newName;
    }
}
 