<?php
App::uses('AppModel', 'Model'); 

class MeetingFollowing extends AppModel { 
 
	public $belongsTo = array(
		'Meeting' => array(
			'className' => 'Meeting',
			'foreignKey' => 'meeting_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function saveMeetingFollowing($meetingIds, $meetingsFollowingData){
		$meetingsFollingsData = array();
		foreach ($meetingsFollowingData as $meetingId => $meetingsFollowing) {
		 	$meetingsFollingsData[] = array(
		 		"meeting_id"   => $meetingIds[$meetingId],
		 		"meeting_room" => $meetingsFollowing["meeting_room"],
		 		"start_date"   => $meetingsFollowing["start_date"],
		 		"end_date"     => $meetingsFollowing["end_date"]
		 	);
		}
		$this->saveAll($meetingsFollingsData); 
	}
}
