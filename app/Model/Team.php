<?php
App::uses('AppModel', 'Model');
 
class Team extends AppModel {
 
	public $validate = array(
		'name' => array(
			array('rule' => 'notBlank','message' => 'El nombre de la empresa es requerido.'),
			array('rule' => 'validateNameTeamExist','message'  => 'Ya existe una empresa con este nombre.','on' => 'create'),
			array('rule' => 'validateNameTeamExistEdit','message'  => 'Ya existe una empresa con este nombre.','on' => 'update')
		),
		'img' => array(          
            'isUnderPhpSizeLimit' => array(
                'rule'    => 'isUnderPhpSizeLimit',
                'message' => 'El archivo excede el límite de tamaño de archivo de subida.',
            ), 
            'isValidMimeType' => array(
                'rule'    => array('isValidMimeType', array('image/jpeg', 'image/png', 'image/gif'), false),
                'message' => 'La imagen no es jpg, gif ni png.',
            ),
            'isBelowMaxSize' => array(
                'rule'    => array('isBelowMaxSize', 10485760),
                'message' => 'El tamaño de imagen es demasiado grande. Solo se permiten imágenes de tamaño de 10MB.'
            ), 
            'isValidExtension' => array( 
                'rule'    => array('isValidExtension', array('jpg', 'png', 'jpeg','gif'), false),
                'message' => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            ), 
            'validateMimeType' => array(
               'rule'       => 'validateMimeType', 
               'message'    => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            )
        ), 
	); 


	public $actsAs = array(
        'Upload.Upload' => array(
            'img' => array(
                'pathMethod'   => 'flat',
                'nameCallback' => 'renameFile',
                'path' 		   => '{ROOT}{DS}webroot{DS}document{DS}WhiteBrand{DS}',
              	'deleteOnUpdate'       => true,
		     	'deleteFolderOnDelete' => false,
            )
        )
    );

    public function validateMimeType($data){  
        return $this->validateMimeTypeImg($data);  
    }

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id', 
		)
	);
 
	public $hasMany = array(
		'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'team_id', 
		),
		'Contac' => array(
			'className' => 'Contac',
			'foreignKey' => 'team_id', 
		),
		'Log' => array(
			'className' => 'Log',
			'foreignKey' => 'team_id', 
		),  
		'UserTeam' => array(
			'className' => 'UserTeam',
			'foreignKey' => 'team_id', 
		) 
	);

	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				'LOWER(CONVERT(Team.name USING latin1))        LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Team.description USING latin1)) LIKE' => "%{$params['q']}%", 
			); 
		}  
		return $conditions;
	}

	public function getTeams($teamIds = array()){
		$this->unbindModel(array('hasMany' => array('Contac','Log','UserTeam')));
		$conditions   = array('Team.id' => array_keys($teamIds)); 
		$teams        = $this->find('list',compact('conditions','recursive')); 
		return $teams;
	}

	public function validateNameTeamExist(){
		$conditions = array("Team.name" => $this->data["Team"]["name"], "Team.user_id" => Authcomponent::user("id"));
		$teamExist  = $this->find("first", compact("conditions"));
		if(!empty($teamExist)){
			return false;
		} else {
			return true;
		}
	}

	public function validateNameTeamExistEdit(){
		$conditions = array("Team.name" => $this->data["Team"]["name"], "Team.user_id" => Authcomponent::user("id"), "NOT" => array("Team.id" => $this->data["Team"]["id"]));
		$teamExist  = $this->find("first", compact("conditions"));
		if(!empty($teamExist)){
			return false;
		} else {
			return true;
		}
	}

	public function checkPermissionInPlan($teamId){
		$conditions    = array("Team.id" => $teamId);
		$userId        = $this->field("user_id", $conditions);
		$conditions    = array("PlanUser.user_id" => $userId, "PlanUser.state" => Configure::read("ENABLED"), "PlanUser.expired" => Configure::read("DISABLED"));
		$planActual    = $this->User->PlanUser->find("first", compact("conditions"));
		return $planActual;		
	}

	public function verifyWhiteLabel($teamId){
		$conditions = array("Team.id" => $teamId);
		$img        = $this->field("img", $conditions);
		return $img;
	}

	public function getTotalTeam($userId){
		$limit      = 1;
		$conditions = array("Team.user_id" => $userId);
		$count      = $this->find("count", compact("conditions","limit"));
		return $count;
	} 

	public function userHaveTeam($userId){
		$limit      = 1;
		$conditions = array(
			'UserTeam.user_id'    => $userId, 
			'UserTeam.state'      => Configure::read('ENABLED'),
			'UserTeam.user_state' => Configure::read('DISABLED'),
		);
		$teamId = $this->UserTeam->field('id', $conditions);
		if(empty($teamId)) {
			return false;
		}
		return true;
	} 

}
