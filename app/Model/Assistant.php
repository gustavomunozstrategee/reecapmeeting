<?php
App::uses('AppModel', 'Model');

class Assistant extends AppModel {

	public $belongsTo = array(
		'Contac'   => array('className' => 'Contac','foreignKey'   => 'contac_id',),
		'Meeting'  => array('className' => 'Meeting','foreignKey'  => 'meeting_id',),
		'User'     => array('className' => 'User','foreignKey'     => 'user_id',), 
		'Team'     => array('className' => 'Team','foreignKey'     => 'team_id',), 
	);

	public function searchInvited($fieldInvited, $clientId = null, $teamId = null){  
		if($fieldInvited != ""){  
			$conditions   = array("OR" => array("LOWER({$this->User->virtualFields["name"]}) LIKE" => '%' . trim(strtolower($fieldInvited)) . '%', "LOWER(User.email) LIKE" => '%' . trim(strtolower($fieldInvited)) . '%'), 'UserTeam.team_id' => $teamId, 'UserTeam.position_id !=' => NULL, 'UserTeam.user_state' => configure::read("DISABLED"));
		    $usersInBussiness = $this->Meeting->Team->UserTeam->find("all", compact("conditions","recursive")); 
			if(!empty($usersInBussiness)){ 
				return $usersInBussiness;
				foreach ($usersInBussiness as $functionary) { 
					echo '<div class="suggest-element"><a data-name="'.EncryptDecrypt::encrypt($functionary["UserTeam"]["type_user"]).'" class="inviteds" id="'. EncryptDecrypt::encrypt($functionary["User"]['id']).'">'.$functionary["User"]['firstname'] . ' ' . $functionary["User"]['lastname'] . '<br>' . $functionary["User"]['email'].'</a></div>'; 
				}
			} else { 
			 	$conditions = array("OR" => array("LOWER({$this->User->virtualFields["name"]}) LIKE" => '%' . trim(strtolower($fieldInvited)) . '%', "LOWER(User.email) LIKE" => '%' . trim(strtolower($fieldInvited)) . '%'), 'UserTeam.team_id' => $teamId, 'Client.id' => $clientId, 'UserTeam.user_state' => configure::read("DISABLED"));
			 	$employees  = $this->Meeting->Team->UserTeam->find("all", compact("conditions")); 
			 	return $employees;
				foreach ($employees as $employee) {
					echo '<div class="suggest-element"><a data-name="'.EncryptDecrypt::encrypt($employee["UserTeam"]["type_user"]).'" class="inviteds" id="'. EncryptDecrypt::encrypt($employee["User"]['id']).'">'.$employee["User"]['firstname'] . ' ' . $employee["User"]['lastname'] . '<br>' . $employee["User"]['email'].'</a></div>'; 
				}
			} 
		}
	}

	public function searcTypeUser($typeUser, $id){
		$id 	    = EncryptDecrypt::decrypt($id); 
	 	$recursive  = -1; 
	 	$conditions = array("User.id" => $id);
	 	$user 	    = $this->User->find("first", compact("conditions","recursive")); 
		return array("user" => $user, "id" => EncryptDecrypt::encrypt($id)); 
	}

	public function saveAllUsersInviteds($teamId, $meetingId, $users = array()){
		$assistants = array();
		foreach ($users as $user) {
			$assistants[] = array(
				"meeting_id"  => $meetingId,
				"team_id"     => $teamId,
				"user_id"     => EncryptDecrypt::decrypt($user["user_id"]),
				"type_user"   => EncryptDecrypt::decrypt($user["type_user"])
			); 
		}  
		if (!empty($assistants)) {
			$this->saveAll($assistants);
		}  
	}

	public function saveAllUsersInvitedsMeetingFollowing($teamId, $meetingIds, $users = array()){
		$assistants = array();
		foreach ($meetingIds as $key => $meetingId) {
			foreach ($users as $user) {
				$assistants[] = array(
					"meeting_id"  => $meetingId,
					"team_id"     => $teamId,
					"user_id"     => EncryptDecrypt::decrypt($user["user_id"]),
					"type_user"   => EncryptDecrypt::decrypt($user["type_user"])
				); 
			}			 
		}
	    $this->saveAll($assistants); 
	}

	//CREAR ACTA - GUARDAR LOS ASISTENTES
	public function saveAsistentesContac($data = array(), $contacId){
		$collaborators = array();
		$assistants    = array();
		$this->deleteAll(array('Assistant.contac_id' => $contacId), false);
		foreach ($data["funcionarios"] as $collaborator) {
		 	$collaborators[] = array(
		 		"contac_id" => $contacId,
		 		"user_id"   => $collaborator,
		 		"team_id"   => $data["team_id"],
		 		"type_user" => configure::read("COLLABORATOR") 
		 	);
		}
		$this->saveAll($collaborators);
		if(!empty($data["externos"])){
			foreach ($data["externos"] as $assistant) {
			 	$assistants[] = array(
			 		"contac_id" => $contacId,
			 		"user_id"   => $assistant,
			 		"team_id"   => $data["team_id"],
			 		"type_user" => configure::read("EXTERNAL") 
			 	);
			}
			$this->saveAll($assistants);
		} 
	} 

	public function getAssistantsContacFinished($contacId){
		$this->unbindModel(array('belongsTo' => array('Team','Meeting')));
		$collaborators = array();
		$assistants    = array();
		$usersIds      = array();
		$conditions    = array("Assistant.contac_id" => $contacId);
		$usersContact  = $this->find("all", compact("conditions"));
		foreach ($usersContact as $assistant) {
		 	$usersIds[] = $assistant["User"]["id"];
		 	if($assistant["Assistant"]["type_user"] == configure::read("COLLABORATOR")){
		 		$collaborators[] = $assistant["User"]["firstname"] .  ' '  . $assistant["User"]["lastname"] . ' - ' . $assistant["User"]["email"];	 
		 	} else {
		 		$assistants[]    = $assistant["User"]["firstname"] .  ' '  . $assistant["User"]["lastname"] . ' - ' . $assistant["User"]["email"];	 
		 	}
		}
		return array("collaborators" => $collaborators, "assistants" => $assistants, "usersId" => $usersIds); 
	}

	public function bindClient() {
 		$binds = array('belongsTo'=>array(
 			'Client' => array('className' => 'Client', 'conditions'=> array('Meeting.client_id = Client.id'), 'foreignKey'=> false), 
 		));
		$this->bindModel($binds);
 	}

 	public function getAssistantsFromContac($contacId){
 		$conditions = array("Assistant.contac_id" => $contacId, "Assistant.type_user" => configure::read("EXTERNAL"));
 		return $this->find("count", compact("conditions"));
 	}
}
