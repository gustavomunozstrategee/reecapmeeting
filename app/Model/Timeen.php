<?php
App::uses('AppModel', 'Model');

class Timeen extends AppModel {

	public $validate = array(
		'user_id'       => array('notBlank' => array('rule' => array('notBlank'),'message' => 'El usuario de los compromisos es requerido'),),
		'description'   => array('notBlank' => array('rule' => array('notBlank'),'message' => 'La descripción de los compromisos es requerida'),),
		'project_id' => array('notBlank' => array('rule' => array('notBlank'),'message' => 'Debe seleccionar un proyecto'),),
		'fecha' => array('notBlank' => array('rule' => array('notBlank'),'message' => 'Debe seleccionar un proyecto'),),
	);

	public $belongsTo = array(
		'User'     => array('className' => 'User',    'foreignKey'  => 'user_id',),
		'Project'  => array('className' => 'Project', 'foreignKey'  => 'project_id',),
		
	);

	 

	

}
