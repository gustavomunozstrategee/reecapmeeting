<?php
App::uses('AppModel', 'Model');
 
class Meeting extends AppModel {
 
	public $validate = array(
		'meeting_type' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'El tipo de reunión es requerido.', 
			),
		),
		'meeting_room' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'La modalidad de la reunión es requerida.', 
			),
		),
		'team_id' => array(
			'notBlank' => array(
				'rule'    => array('notBlank'),
				'message' => 'La empresa es requerida.', 
			),
		),
		'client_id' => array(
			'notBlank' => array(
				'rule'    => array('notBlank'),
				'message' => 'El cliente es requerido.', 
			),
		),
		'subject' => array(
			'notBlank' => array(
				'rule'    => array('notBlank'),
				'message' => 'El asunto es requerido.', 
			),
		),
		'location' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'La ubicación o el lugar de la reunión es requerida.', 
			),
		),
		'description' => array(
			array('rule' => 'notBlank','message' => 'La descripción es requerida.'),
			// array('rule' => array('maxLengthBytes', '300'),'message' => 'La descripción debe tener máximo 300 caracteres.'), 	
			array('rule' => array('countMaxLength'),'message' => 'La descripción debe tener máximo 300 caracteres.'), 	
		), 
		'start_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'La fecha inicio es requerida.', 
			),
			'validateEqualDatesMeeting' => array(
				'rule' => array('validateEqualDatesMeeting'),
				'message' => 'La fecha inicio y la fecha fin no pueden ser iguales.', 
			),
			'validateDateYesterday'  => array(
				'rule' => array('validateDateYesterday'),
				'message' => 'La fecha inicio no se puede agendar de un día que ya pasó.',
				'on'	=> 'create', 
			),
		),
		'end_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'La fecha fin es requerida.', 
			),
			'validateEndDateWithStartDate' => array(
				'rule' => array('validateEndDateWithStartDate'),
				'message' => 'La fecha fin no puede ser menor que la fecha inicio.', 
			) 
		),
		'password' => array(
			array('rule' => 'notBlank','message' => 'La contraseña es requerida.'),
			array('rule' => array('maxLength', '4'),'message' => 'La contraseña debe tener como máximo 4 caracteres.'),  
		), 
		
	); 
	
	public $hasMany = array(
		'Assistant' => array(
			'className'  => 'Assistant',
			'foreignKey' => 'meeting_id', 
		),
		'Tag' => array(
			'className'  => 'Tag',
			'foreignKey' => 'meeting_id', 
		),
		'MeetingFollowing' => array(
			'className'  => 'MeetingFollowing',
			'foreignKey' => 'meeting_id', 
		),
		'Topic' => array(
			'className'  => 'Topic',
			'foreignKey' => 'meeting_id', 
		)
	);

	public $belongsTo = array(
		'User'   => array('className' => 'User',  'foreignKey'    => 'user_id',),
		'Client' => array('className' => 'Client','foreignKey'    => 'client_id',), 
		'Team'   => array('className' => 'Team',  'foreignKey'    => 'team_id',) 
	);

	public function beforeSave($options = array()) {
	 	$actionsForm = Router::getParams()["action"];
		$startDate = $this->data["Meeting"]["start_date"];
		$datetime  = new DateTime($startDate);
		$date 	   = $datetime->format('Y-m-d'); 
		$this->data["Meeting"]["user_id"] = authcomponent::user("id"); 
		$this->data["Meeting"]["date"] = $date;
		return true;	  
	}

	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] = trim($params['q']);
			$params['q'] = strtolower($params['q']);
			$conditions = array('OR' => array(
				'Meeting.created ='  => "{$params['q']}"
			)); 
		} 
		if(!empty($params['teams'])) {
			$conditions['Meeting.team_id'] = $params['teams'];
		} 
		return $conditions;
	}

	public function countMaxLength($data){
		$text = str_replace(array("\r\n","\r"), "", $data["description"]);
		if($text > 300){
			return false;
		} else {
			return true;
		} 
	}

	public function getMeetingsIds($meetingFormData, $meetingsFollowingData){
		$meetings     = array();
		$meetingsIds  = array();
		foreach ($meetingsFollowingData as $meetingsFollowing) {
			$datetime = new DateTime($meetingsFollowing["start_date"]);
			$meetings = array(
				"meeting_type" => $meetingFormData["Meeting"]["meeting_type"],
				"subject"	   => $meetingFormData["Meeting"]["subject"],
				"location"     => !empty($meetingFormData["Meeting"]["location"]) ? $meetingFormData["Meeting"]["location"] : __("No suministrada"),
				"description"  => $meetingFormData["Meeting"]["description"],
				"meeting_room" => $meetingsFollowing["meeting_room"],
				"start_date"   => $meetingsFollowing["start_date"],
				"end_date"     => $meetingsFollowing["end_date"],
				"private"      => $meetingFormData["Meeting"]["private"],
				"password"     => isset($meetingFormData["Meeting"]["password"]) ? $meetingFormData["Meeting"]["password"] : "",
				"team_id"      => $meetingFormData["Meeting"]["team_id"],
				"client_id"    => $meetingFormData["Meeting"]["client_id"],
				"user_id"      => authcomponent::user("id"),
				"created"      => $datetime->format('Y-m-d'),
				"created_from" => __("gmail")
			); 
			$this->create();
			$this->save($meetings, array('validate'=>false));
		    $meetingsIds[]   = $this->getInsertID();		     
		    $topicsMeeting[] = $this->__storeTopics($this->id, $meetingsFollowing);
		}
		return array("meetingsIds" => $meetingsIds, "topicsMeeting" => $topicsMeeting); 
	}

	private function __storeTopics($meetingId, $meetingsFollowing){		
		$topicsMeeting = array();
		$topicsMeeting = array("meeting_id" => $this->id, "Topics" => isset($meetingsFollowing["Topics"]) ? $meetingsFollowing["Topics"] : array());
		return $topicsMeeting;
	}

	public function getRecordToUpdateCalendarOutlook($events = array(), $meetingsDb = array()){
		$eventsGetOutlook = array();
		if(!empty($events)){
			if(isset($events["value"])){
				foreach ($events["value"] as $event) {
					$createDate    = new DateTime($event["Start"]["DateTime"]);
	        		$createDateEnd = new DateTime($event["End"]["DateTime"]);
	        		$initialDate   = $createDate->format('Y-m-d H:i:00');
	        		$finalDate     = $createDateEnd->format('Y-m-d H:i:00');
					$eventsGetOutlook[] = array(
						"subject"     => $event["Subject"],
			 			"location"    => !empty($event["Location"]["DisplayName"]) ? $event["Location"]["DisplayName"] : __("No disponible"),
			 			"description" => !empty($event["Body"]["Content"]) ? $event["Body"]["Content"] : __("Sin descripción"),
			 			"start_date"  => $initialDate,
			 			"end_date"    => $finalDate,
			 			"email_type"  => "outlook",
			 			"user_id"	  => authcomponent::user("id")
					);	 
				}  
			}
		}
		if(!empty($meetingsDb)){ 
			$this->__saveMeetingsDb($meetingsDb, $eventsGetOutlook); 
		} else { 
			$this->__saveEventOutlook($eventsGetOutlook);
		}	 
	}

	private function __saveMeetingsDb($meetingsDb, $eventsGetOutlook){
		$result = array();
		foreach ($eventsGetOutlook as $event) {
		 	foreach ($meetingsDb as $meeting) {
		 	 	if ($meeting["subject"] != $event["subject"]) {
	 	 			$result[] = $event;   
	 	 			break;
		 	 	}
		 	}	
		}	 
		if(!empty($result)){
			$this->saveAll($result, array('validate'=>false)); 
		} 
	} 

	private function __saveEventOutlook($events = array()){
		if(!empty($events)){
			$this->saveAll($events, array('validate'=>false)); 
		}
	}

	public function buildDataFromMeetingsDatabase($meetingsDb, $events){  
		$meetingData = array();
		if(!empty($events)){
			foreach ($events as $eventId => $event) { 
				if(empty($meetingsDb)){ 
					$meetingData[] = $event;
				} else {
				 	foreach ($meetingsDb as $meetingId => $meetings) { 
				 	 	if($event["title"] == $meetings["subject"]) {
				 	 	 	$meetingData[] = $eventId;
				 	 	 	break;  	 	 
				 	 	} 
				 	}				
				}
			} 
			return $this->__saveMeetingsOutlook($events, $meetingData);
		} else {
			$events = array();
			return $this->__saveMeetingsOutlook($events, $meetingData);			
		}
	}

	private function __saveMeetingsOutlook($events, $meetingData){  
		foreach ($meetingData as $meetingKey) {
			foreach ($events as $eventId => $event) {
				if (isset($events[$eventId])) {
		 	 	 	if ($meetingKey == $eventId) {
			 	 	   unset($events[$eventId]);
			 	 	   break;
		 	 	 	}				 
				}
			}
		}
		return $events;
	}

	public function detailMeetingRelations($meetingId){
		$recursive = 2;  
		$this->Assistant->unbindModel(array('belongsTo' => array('Contac','Meeting','Team')));
		$this->Tag->unbindModel(array('belongsTo' => array('User','Team','Meeting')));
		$this->Topic->unbindModel(array('belongsTo' => array('Meeting')));
		$this->Team->unbindModel(array('belongsTo' => array('User')));
		$this->Team->unbindModel(array('hasMany' => array('Contac','Log','Client','UserTeam')));  
		$this->Client->unbindModel(array('hasMany' => array('Project')));
		$this->Client->unbindModel(array('belongsTo' => array('User','Team')));
		$conditions = array("Meeting.id" => $meetingId);
		$meeting    = $this->find("first", compact("conditions","recursive"));  
		return $meeting;
	}

	public function searchDetailMeeting($meetingId){
		$this->unbindModel(array('hasMany' => array('MeetingFollowing','Tag','Assistant')));
		$this->unbindModel(array('belongsTo' => array('User','Team','Client')));
		$recursive  = 1;
		$conditions = array("Meeting.id" => $meetingId);
		$meeting    = $this->find("all", compact("conditions","recursive"));
		return $meeting; 
	}

	public function getInfoMeetingToContact($meetingId){
		$conditions = array("Meeting.id" => $meetingId);
		$meeting    = $this->find("first", compact("conditions"));	 
		$usersIds   = Set::classicExtract($meeting, 'Assistant.{n}.user_id'); 
		$users      = $this->Team->UserTeam->getUsersMeeting(isset($meeting["Team"]["id"]) ? $meeting["Team"]["id"] : array(), $usersIds); 
		$dataToContact = array(
			"Meeting"    => !empty($meeting["Meeting"]) ? $meeting["Meeting"]: array(),
			"Team"    	 => !empty($meeting["Team"])    ? $meeting["Team"]:    array(),
			"User"       => !empty($meeting["User"])    ? $meeting["User"]:    array(),
			"Tag"        => !empty($meeting["Tag"])     ? $meeting["Tag"]:     array(),
			"Topic"      => !empty($meeting["Topic"])   ? $meeting["Topic"]:   array(),
			"Client"     => !empty($meeting["Client"])  ? $meeting["Client"]:  array(),
			"Assistant"  => $users["Assistant"], 
			"collaborators"  => !empty($users["collaborators"])  ? $users["collaborators"]:  array(),
			"assistants"     => !empty($users["assistants"])     ? $users["assistants"]:     array(), 
		); 
		return $dataToContact;
	} 

	public function countMeetingCreated(){
		$conditions = array("Meeting.user_id" => authcomponent::user("id"));
		$meetings   = $this->find("count", compact("conditions"));
		return $meetings;
	}

	public function notificationFirebaseMeetingCreated($meetingId){
		$this->Assistant->unbindModel(array('belongsTo' => array('Contac')));
		$conditions = array("Assistant.meeting_id" => $meetingId);
		$meetings   = $this->Assistant->find("all", compact("conditions"));
		foreach ($meetings as $meeting) {
			$dateEsp  = $this->__showDateMeetingEsp($meeting["Meeting"]["start_date"]);
			$date     = $this->__showDateMeeting($meeting["Meeting"]["start_date"]);
		 	$msgEsp   = sprintf("Tienes una reunión con el asunto %s, %s.", $meeting["Meeting"]["subject"], $dateEsp);
		 	$msgEng   = __("You’ve a meeting with the subject")." ".$meeting["Meeting"]["subject"].", ".$date.".";
		 	$url      = "meetings/view/". EncryptDecrypt::encrypt($meeting["Meeting"]["id"]);
	 		$this->notificacionesUsuarios($meeting['User']['id'], $msgEsp, $msgEng, $url); 
		} 
	}

	public function notificationFirebaseMeetingFollowingCreated($meetingIds = array()){
		$this->Assistant->unbindModel(array('belongsTo' => array('Contac')));
		$conditions = array("Assistant.meeting_id" => $meetingIds, 'Assistant.user_id !=' => NULL);
		$meetings   = $this->Assistant->find("all", compact("conditions"));
		foreach ($meetings as $meeting) {
			$date    = $this->__showDateMeeting($meeting["Meeting"]["start_date"]);
			$dateEsp = $this->__showDateMeetingEsp($meeting["Meeting"]["start_date"]);
		 	$msgEsp  = sprintf("Tienes una reunión con el asunto %s, %s.", $meeting["Meeting"]["subject"], $dateEsp);
		 	$msgEng  = __("You’ve a meeting with the subject")." ".$meeting["Meeting"]["subject"].", ".$date.".";
	 		$url     = "meetings/view/". EncryptDecrypt::encrypt($meeting["Meeting"]["id"]);
	 		$this->notificacionesUsuarios($meeting['User']['id'], $msgEsp, $msgEng, $url); 
		}  
	}

	private function __showDateMeetingEsp($startDate){
		$out = "";
		$createDate = new DateTime($startDate);
		$cretaeHour = new DateTime($startDate);
		$dateNew    = $createDate->format('Y-m-d');
		$hourNew    = $createDate->format('h:i A'); 
		$tomorrow   = date("Y-m-d", strtotime("+1 day"));
		if($dateNew == date('Y-m-d')){
			$out = "hoy a las " . $hourNew;
		} else if ($dateNew == $tomorrow) {
			$out = "mañana a las " . $hourNew;
		} else {
			$out = "el " . $dateNew . ' ' . $hourNew;
		} 
		return $out; 
	}

	private function __showDateMeeting($startDate){
		$out = "";
		$createDate = new DateTime($startDate);
		$cretaeHour = new DateTime($startDate);
		$dateNew    = $createDate->format('Y-m-d');
		$hourNew    = $createDate->format('h:i A'); 
		$tomorrow   = date("Y-m-d", strtotime("+1 day"));
		if($dateNew == date('Y-m-d')){
			$out = "today at " . $hourNew;
		} else if ($dateNew == $tomorrow) {
			$out = "tomorrow at " . $hourNew;
		} else {
			$out = "the " . $dateNew . ' ' . $hourNew;
		} 
		return $out; 
	}

	public function saveCalendarIdMeeting($meetingId, $idCalendar){
		$this->updateAll(array('Meeting.ical_uid' => "'" .$idCalendar. "'"), array('Meeting.id' => $meetingId));
	}

	public function saveCalendarIdMeetingFollowing($meetingIds = array(), $idCalendar = array()){ 
		foreach ($meetingIds as $calendarId => $meetingId) { 
		 	$this->updateAll(array('Meeting.ical_uid' => "'" .$idCalendar[$calendarId]. "'"), array('Meeting.id' => $meetingId));
		} 
	}

	public function saveFromCreatedCalendar($meetingId, $fromApi){
		$this->updateAll(array('Meeting.created_from' => "'" .$fromApi. "'"), array('Meeting.id' => $meetingId));
	}

	public function updateCreatedFromOutlookMeetingFollowing($meetingIds = array()){ 
		$this->updateAll(array('Meeting.created_from' => "'" .__("outlook"). "'"), array('Meeting.id' => $meetingIds));
	}

	public function buildaRequestDataMeetingEdit($data = array()){ 
		$this->updateTagsMeeting($data);
		$this->Topic->editTopicsMeeting($data);
 		$recursive        = -1;
 		$conditions       = array("MeetingFollowing.meeting_id" => $data["Meeting"]["id"]);
 		$meetingFollowing = $this->MeetingFollowing->find("first", compact("conditions","recursive"));
	 	if(configure::read("TYPE_MEETING.{$data["Meeting"]["meeting_type"]}") == "Reunión de seguimiento"){
	 		if(!empty($meetingFollowing)){ 
	 			$this->updateOrCreatedMeetingFollowing("update", $data, $meetingFollowing);
	 		} else {
	 			$this->updateOrCreatedMeetingFollowing("create", $data);
	 		} 
	 	} else if (configure::read("TYPE_MEETING.{$data["Meeting"]["meeting_type"]}") == "Reunión"){
	 		if(!empty($meetingFollowing)){
	 	 		$this->deleteMeetingFollowing($meetingFollowing["MeetingFollowing"]["id"]);
	 		}
	 	}
	 	return $this->setFieldsPasswordAndLocation($data); 
	}

	public function updateOrCreatedMeetingFollowing($action, $data = array(), $meetingFollowing = null){
		$saveData = array();
		if($action == "update") {
			$saveData = array(
				"id" 		   => $meetingFollowing["MeetingFollowing"]["id"],
				"meeting_id"   => $data["Meeting"]["id"],
				"start_date"   => $data["Meeting"]["start_date"],
				"end_date"     => $data["Meeting"]["end_date"],
				"meeting_room" => $data["Meeting"]["meeting_room"]
			);
			$this->MeetingFollowing->save($saveData, array('validate'=>false));
		} else if($action == "create") {
			$saveData = array( 
				"meeting_id"   => $data["Meeting"]["id"],
				"start_date"   => $data["Meeting"]["start_date"],
				"end_date"     => $data["Meeting"]["end_date"],
				"meeting_room" => $data["Meeting"]["meeting_room"]
			);
			$this->MeetingFollowing->save($saveData, array('validate'=>false));
		}
	}

	public function deleteMeetingFollowing($id){
		$this->MeetingFollowing->delete($id);
	}

	public function setFieldsPasswordAndLocation($data = array()){
		if($data["Meeting"]["meeting_room"] != 1){
			$data["Meeting"]["location"] = NULL;
		}
		if($data["Meeting"]["private"] == 1){
			$data["Meeting"]["password"] = NULL;
		} 
		return $data; 
	}

	public function validateEqualDatesMeeting(){
		if($this->data["Meeting"]["start_date"] == $this->data["Meeting"]["end_date"]){
			return false;
		} else {
			return true;
		} 
	}

	public function validateEndDateWithStartDate(){
		if($this->data["Meeting"]["end_date"] < $this->data["Meeting"]["start_date"]){
			return false;
		} else {
			return true;
		} 
	}

	public function validateDateYesterday(){
		$today = date('Y-m-d');
		if ($this->data["Meeting"]["start_date"] < $today) {
			return false; 
		} else {
			return true;
		} 
	}

	public function updateTagsMeeting($data = array()){	
		$recursive    = -1;
		$conditions   = array("Tag.meeting_id" => $data["Meeting"]["id"]); 
		$tagsInList   = $this->Tag->find("all", compact("conditions","recursive")); 
		$tags 	      = explode(",", $data["Meeting"]['tag']);
		if(!empty($data["Meeting"]['tag'])){
			$tagIds      = array_combine($tags, $tags);
			$tagExist    = Set::combine($tagsInList, '{n}.Tag.name', '{n}.Tag.name');
			$tagNotExist = array_diff($tagIds, $tagExist);
			if(!empty($tagNotExist)){
				$this->__saveTagNews($tagNotExist, $data["Meeting"]["id"]);
			}
		}
		$this->__checkTagRemoved($tagsInList, $tags, $data["Meeting"]["id"]); 	 
	} 

	private function __saveTagNews($tags, $meetingId){
		$tagsUpdate = array();
		foreach ($tags as $tag) {
		 	$tagsUpdate[] = array( 
		 		"name"       => $tag,
		 		"meeting_id" => $meetingId,
		 		"user_id"    => Authcomponent::user("id")
		 	); 
		}
		$this->Tag->saveAll($tagsUpdate);
	}

	private function __checkTagRemoved($tagsBd, $tagsAdds, $meetingId){
		if(!empty($tagsAdds)){
			$tagExist    = Set::combine($tagsBd, '{n}.Tag.name', '{n}.Tag.name');
			$removedTags = array_filter(array_diff($tagExist, $tagsAdds)); 
			if(!empty($removedTags)){ 
				foreach ($removedTags as $nameTag) {
					$this->Tag->deleteAll(array('Tag.meeting_id' => $meetingId, 'Tag.name' => $nameTag),false);		 
				} 
			}
		} 
	}

	public function getInfoMeetingBeforeEdit($id){
		$recursive  = -1;
		$conditions = array("Meeting.id" => $id);
		$meeting    = $this->find("first", compact("conditions","recursive"));
		$descripcion = array(  
	 		"type_meeting" => $meeting["Meeting"]["meeting_type"], 
	 		"subject"	   => !empty($meeting["Meeting"]["subject"])     ? $meeting["Meeting"]["subject"]     : __(" sin asunto "), 	
	 		"location"	   => !empty($meeting["Meeting"]["location"])    ? $meeting["Meeting"]["location"]    : __(" sin localización "), 	
	 		"modality"     => $this->getMeetingRoom($meeting["Meeting"]["meeting_room"]),
	 		"description"  => !empty($meeting["Meeting"]["description"]) ? $meeting["Meeting"]["description"] : __(" sin descripción "), 	
	 		"private"      => ($meeting["Meeting"]["private"] == 1) ? __("pública") : __("privada"),
	 		"password"	   => !empty($meeting["Meeting"]["password"])    ? $meeting["Meeting"]["password"]   : __(" sin contraseña "), 	
	 		"start_date"   => !empty($meeting["Meeting"]["start_date"])  ? $meeting["Meeting"]["start_date"] : __(" sin fecha inicio "),
	 		"end_date"     => !empty($meeting["Meeting"]["end_date"])    ? $meeting["Meeting"]["end_date"]   : __(" sin fecha fin "),	 		 
 		); 
	 	return var_export($descripcion, true); 
	}

	private function getMeetingRoom($modality){  
		if(in_array($modality, array_keys(configure::read("MODALITY_MEETING")))) {
			return configure::read("MODALITY_MEETING.{$modality}");
		} 
	}

	public function logAfterEditMeeting($id, $beforeEdit = array()){
		$conditions = array("Meeting.id" => $id);
		$meeting    = $this->field("subject", $conditions);
		$log 		= ClassRegistry::init('Log');
		$log->deleteAll(array('Log.action' => "update_meeting"), false); 
		$afterEditMeeting = $this->getInfoMeetingBeforeEdit($id);
		$description = __("El usuario ") .AuthComponent::user("firstname"). ' ' .AuthComponent::user("lastname") . __(" ha editado la reunión  ") . $meeting.".";
		// $this->buildInfoLog($id, $description, $beforeEdit, $afterEditMeeting, "update_meeting", NULL); 
	}

	public function validateMeetingHaveAssistant($assistants = array()){ 
		$countDelete   = 0;
		$countAssitant = 0;
		if(!empty($assistants)){
			foreach ($assistants as $userId => $assistant) {
				$countAssitant += count($userId);
				if($assistant["state"] == configure::read("DELETE_ASSISTANT")) {
					$countDelete++;
				}
			}
		}
		if($countDelete == $countAssitant){
			return array("message" => __("Se debe añadir mínimo un usuario a la reunión."), "state" => false,"validation" => false); 
		} else {
			return array("state" => true);
		}
	}

	public function infoToEditMeeting($data = array()){
		$users = $this->getAssistantToEditMeeting($data);
	    $dates = $this->__buildDateMeetingEdit($data["Meeting"]["start_date"], $data["Meeting"]["end_date"]);	
		$availableUser = "";
	    if($data["Meeting"]["private"] == 1){
	        $availableUser = "transparent";
	    } else if ($data["Meeting"]["private"] == 2) {
	        $availableUser  = "opaque";
	    }
	    $conditions = array("Meeting.id" => $data["Meeting"]["id"]);
	    $idEvent    = $this->field("ical_uid", $conditions);
		$info = array(
			"subject"      => $data["Meeting"]["subject"],
			"location"     => isset($data["Meeting"]["location"]) ? $data["Meeting"]["location"] : __("No suministrada"),
			"description"  => $data["Meeting"]["description"],
			"startDate"    => $dates["fechaInicioFinal"],
			"endDate"      => $dates["fechaFinFinal"],
			"users"        => $users,
			"transparency" => $availableUser,
			"idEvent"      => $idEvent
		);
		return $info; 
	}

	public function getAssistantToEditMeeting($data = array()){
		$this->Assistant->unbindModel(array('belongsTo' => array('Contac','Meeting')));
		$conditions = array("Assistant.meeting_id" => $data["Meeting"]["id"]);
		$assistants = $this->Assistant->find("all", compact("conditions")); 
		$users 	    = array();
		$info       = array();
		foreach ($assistants as $assistant) { 
			$users[$assistant["User"]["id"]] = array(
				"email" => $assistant["User"]["email"],
				"name"  => $assistant["User"]["firstname"] . ' ' . $assistant["User"]["lastname"]
			); 
		}
		return $users;
	}

	private function __buildDateMeetingEdit($startDate, $endDate){
	 	$createDateInicio  = new DateTime($startDate);
	    $createDateFin     = new DateTime($endDate);
	    $createHoraInicio  = new DateTime($startDate);
	    $createHoraFin     = new DateTime($endDate);
	    $fechaInicio       = $createDateInicio->format('Y-m-d');
	    $fechaFin          = $createDateFin->format('Y-m-d');
	    $horaInicio        = $createHoraInicio->format('H:i');
	    $horaFin           = $createHoraFin->format('H:i');
	    $segundos          = ":00-05:00";
	    $t = "T"; 
	    $fechaInicioFinal = $fechaInicio.$t.$horaInicio.$segundos;
	    $fechaFinFinal    = $fechaFin.$t.$horaFin.$segundos;
	    return array("fechaInicioFinal" => $fechaInicioFinal, "fechaFinFinal" => $fechaFinFinal);
	}
}
