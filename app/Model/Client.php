<?php
App::uses('AppModel', 'Model');

class Client extends AppModel {

	public $displayField = 'name';

	public $actsAs = array(
      	'Upload.Upload' => array(
      		'img' => array(
	          	'pathMethod'           => 'flat',
	          	'nameCallback'         => 'renameFile',
	          	'path'                 => '{ROOT}{DS}webroot{DS}files{DS}Client{DS}',
	          	'deleteOnUpdate'       => true,
		        'deleteFolderOnDelete' => true,
          	)
		)
    );

	public $validate = array(
		'name' 		  => array(
			array('rule' => 'notBlank','message' => 'El nombre es requerido.'), 
			array('rule' => 'validateExistClient','required'    => true,'message'  => 'Ya has creado un cliente con este nombre asociado a la empresa seleccionada.','on'=> 'update'),  
			array('rule' => 'validateExistClientAdd','required' => true,'message'  => 'Ya has creado un cliente con este nombre asociado a la empresa seleccionada.','on'=> 'create'),  
		),
		'description' => array(
			array('rule' => 'notBlank','message' => 'La descripción es requerida.'),
			array('rule' => 'validateTextareaLenght','required' => true,'message'  => 'La descripción debe tener máximo 300 caracteres.'),  
		),
		'team_id' => array(
			array('rule' => 'notBlank','message' => 'La empresa es requerida.','on' => 'create'),  
		),
		'img' => array(          
            'isUnderPhpSizeLimit' => array(
                'rule'    => 'isUnderPhpSizeLimit',
                'message' => 'El archivo excede el límite de tamaño de archivo de subida.',
            ), 
            'isValidMimeType' => array(
                'rule'    => array('isValidMimeType', array('image/jpeg', 'image/png', 'image/gif'), false),
                'message' => 'La imagen no es jpg, gif ni png.',
            ),
            'isBelowMaxSize' => array(
                'rule'    => array('isBelowMaxSize', 10485760),
                'message' => 'El tamaño de imagen es demasiado grande. Solo se permiten imágenes de tamaño de 10MB.'
            ), 
            'isValidExtension' => array( 
                'rule'    => array('isValidExtension', array('jpg', 'png', 'jpeg','gif'), false),
                'message' => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            ), 
            'validateMimeType' => array(
               'rule'       => 'validateMimeType', 
               'message'    => 'La imagen no tiene la extension jpg, gif, png o jpeg.', 
            )
        ), 
	);

	public function validateMimeType($data){  
        return $this->validateMimeTypeImg($data);  
    }

	public $belongsTo = array(
		'User'     => array('className' => 'User','foreignKey' => 'user_id',),
		'Team'     => array('className' => 'Team','foreignKey' => 'team_id',)
	);

	public $hasMany = array( 
		'Project'  => array('className' => 'Project','foreignKey'  => 'client_id','dependent' => false,)
	);
	
	public function fileSelected($file) {
		$file = $file["img"];
	   return (is_array($file) && array_key_exists('name', $file) && !empty($file['name']));
	}

	public function buildConditions($params = array()) {
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
				'LOWER(CONVERT(Client.name USING latin1))        LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Client.description USING latin1)) LIKE' => "%{$params['q']}%", 
			); 
		}
		if(!empty($params['teams'])) {
			$conditions['Client.team_id'] = $params['teams'];
		}
		return $conditions;
	}

	public function bindContac() {
 		$binds = array('hasMany'=>array(
 			'Contac' => array('className' => 'Contac', 'conditions'=> array('Contac.state' => Configure::read("DISABLED")), 'foreignKey'=> 'client_id'), 
 		));
		$this->bindModel($binds);
 	}

 	public function bindContacNotDone() {
 		$binds = array('hasMany'=>array(
 			'Contac' => array('className' => 'Contac', 'conditions'=> array(), 'foreignKey'=> 'client_id'), 
 		));
		$this->bindModel($binds);
 	}
  
 	public function validExistenceClient($name, $teamId){
		$conditions = array('Client.name' => $name,'Client.team_id' => $teamId);	
		return $this->find('count',compact('conditions'));
	}

	public function detaillClient($client_id){
		$conditions     = array('Client.id' => $client_id);
		return $cliente = $this->find('first', compact('conditions'));
	}

	public function detaillClientSelect($client_id){ 
		$conditions  = array("Client.id" => $client_id);
        $clientsTeam = $this->find("all", compact("conditions"));
        $clients 	 = array();
	    if(!empty($clientsTeam)){
	    	foreach ($clientsTeam as $client) {
	    		$clients[$client["Client"]["id"]] = $client["Client"]["name"] . ' - ' .$client["Team"]["name"];
	    	}
	    }
        return $clients;
	}

	public function validateExistClient($data){
		$conditions  = array('Client.name' => $this->data["Client"]["name"],'Client.team_id' => $this->data["Client"]["team_id"], "NOT" => array("Client.id" => $this->data["Client"]["id"]));
		$existClient = $this->find('count',compact('conditions')); 
		if($existClient == 1){
			return false;
		} else {
			return true;
		} 
	}

	public function validateExistClientAdd($data){
		$conditions  = array('Client.name' => $this->data["Client"]["name"],'Client.team_id' => $this->data["Client"]["team_id"]);
		$existClient = $this->find('count',compact('conditions')); 
		if($existClient == 1){
			return false;
		} else {
			return true;
		} 
	}

	public function getClientTeam($teamId){
	 	$conditions  = array("Client.team_id" => $teamId, 'Client.state' => Configure::read('ENABLED'));
        $clientsTeam = $this->find("all", compact("conditions"));
        $clients 	 = array();
	    if(!empty($clientsTeam)){
	    	foreach ($clientsTeam as $client) {
	    		$clients[$client["Client"]["id"]] = $client["Client"]["name"] . ' - ' .$client["Team"]["name"];
	    	}
	    }
        return $clients;
	} 

	public function validateTextareaLenght(){
		$text = str_replace(array("\r\n","\r"), "", $this->data["Client"]["description"]);
		if($text > 300){
			return false;
		} else {
			return true;
		}
	}

	public function getTotalClient(){
		$limit      = 1;
		$conditions = array("Client.user_id" => Authcomponent::user("id"));
		$count      = $this->find("count", compact("conditions","limit"));
		return $count;
	} 

}
