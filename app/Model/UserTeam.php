<?php
App::uses('AppModel', 'Model');
 
class UserTeam extends AppModel {
 	
 	var $displayField  = 'User.name';

	public $validate = array(		 
		'type_user' => array(
			'notBlank' => array(
				'rule' 	  => array('notBlank'),
				'message' => 'El tipo de usuario es requerido.', 
			),
		), 
		'team_id' => array(
			array('rule' => 'validateTeams','message'  => 'La empresa es requerido.'),  
		),
		'client_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'El cliente es requerido.', 
			),
		), 
		'position_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'El rol es requerido.', 
			),
		), 
		'email'  => array(
			array(
				'rule'     => 'notBlank',
				'on'       => 'create',
				'required' => true,
				'message'  => 'Por favor ingrese un correo.'
			),
			array(
				'rule'    => 'email',
				'message' => 'Ingrese una dirección de correo electrónico válida.' 
			), 
		), 
	);

 
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id', 
		),
		'OwnUser' => array(
			'className' => 'User',
			'foreignKey' => 'own_user_id', 
		),
		'Position' => array(
			'className' => 'Position',
			'foreignKey' => 'position_id', 
		),
		'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'client_id', 
		),
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id', 
		)
	);

	public function validateTeams($data){ 
		if(!is_array($data['team_id'])){
			return false;
		} else {
			return true;  
		} 
	}

	public function buildConditions($params = array()) { 
		$conditions = array();
		if(!empty($params['q'])) {
			$params['q'] =  $this->normalizeChars($params['q']);
			$params['q'] =  trim(strtolower($params['q']));			
			$conditions['OR'] = array(
            	'LOWER(CONCAT(User.firstname," ", User.lastname)) LIKE' => "%{$params['q']}%", 
				// "LOWER(CONVERT({$this->User->virtualFields["name"]} USING latin1)) LIKE" => "%{$params['q']}%", 
				'LOWER(CONVERT(Team.name USING latin1))       LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Client.name USING latin1))     LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(Position.name USING latin1))   LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(User.email USING latin1))      LIKE' => "%{$params['q']}%", 
				'LOWER(CONVERT(OwnUser.email USING latin1))   LIKE' => "%{$params['q']}%", 
			); 
		} 
		if(!empty($params['teams'])) {
			$conditions['Team.id'] = $params['teams'];
		} 
		return $conditions;
	}

	public function getCollaborators($teamId){
		$recursive     = 0;
		$conditions    = array("UserTeam.team_id" => $teamId, "UserTeam.type_user" => 1, "UserTeam.state" => configure::read("ENABLED"), 'UserTeam.user_state' => configure::read("DISABLED"));
		$fields        = array("User.id","User.firstname","User.lastname","User.email");
		$users  	   = $this->find("all", compact("conditions","recursive","fields"));
		$collaborators = array();
		if(!empty($users)){
			foreach ($users as $user) {
				$collaborators[$user["User"]["id"]] = $user["User"]["firstname"] .  ' '  . $user["User"]["lastname"] . ' - ' . $user["User"]["email"];	 
			} 
		} else {
			$collaborators = array();
		}
		return $collaborators;
	}

	public function getCollaborators_NOTME($teamId,$userId){
		$recursive     = 0;
		$conditions    = array(
        	"UserTeam.team_id" 	  => $teamId, 
        	"UserTeam.type_user"  => 1,
        	"UserTeam.state" 	  => configure::read("ENABLED"), 
        	'UserTeam.user_state' => configure::read("DISABLED"),
       	 	"UserTeam.user_id != "=> $userId	
        );
		$fields        = array("User.id","User.firstname","User.lastname","User.email");
		$users  	   = $this->find("all", compact("conditions","recursive","fields"));
		$collaborators = array();
		if(!empty($users)){
			foreach ($users as $user) {
				$collaborators[$user["User"]["id"]] = $user["User"]["firstname"] .  ' '  . $user["User"]["lastname"] . ' - ' . $user["User"]["email"];	 
			} 
		} else {
			$collaborators = array();
		}
		return $collaborators;
	}

	public function getAssistants($teamId, $clientId){
		$recursive     = 0;
		$conditions    = array("UserTeam.team_id" => $teamId, "UserTeam.type_user" => 2, "UserTeam.state" => configure::read("ENABLED"), 'UserTeam.user_state' => configure::read("DISABLED"), "UserTeam.client_id" => $clientId);
		$fields        = array("User.id","User.firstname","User.lastname","User.email");
		$users  	   = $this->find("all", compact("conditions","recursive","fields"));
		$assistants    = array();
		if(!empty($users)){
			foreach ($users as $user) {
				$assistants[$user["User"]["id"]] = $user["User"]["firstname"] .  ' '  . $user["User"]["lastname"] . ' - ' . $user["User"]["email"];	 
			} 
		} else {
			$assistants    = array();
		} 
		return $assistants;
	}

	public function getUsersCommitments($teamId, $clientId){
		$conditions = array("UserTeam.team_id" => $teamId, "UserTeam.type_user" => configure::read("COLLABORATOR"), "UserTeam.state" =>configure::read("ENABLED"), 'UserTeam.user_state' => configure::read("DISABLED")); 
		$fields 	= array("User.id","User.firstname","User.lastname","User.email");
		$users  	= $this->find("all", compact("conditions","recursive","fields"));
		$conditions = array("UserTeam.team_id" => $teamId, "UserTeam.type_user" => configure::read("EXTERNAL"), "UserTeam.state" =>configure::read("ENABLED"), 'UserTeam.user_state' => configure::read("DISABLED"), "UserTeam.client_id" => $clientId); 
		$fields 	= array("User.id","User.firstname","User.lastname","User.email");
		$externals  = $this->find("all", compact("conditions","recursive","fields"));
		$assistants = array();		 
		foreach ($users as $user) {
			$assistants[$user["User"]["id"]] = $user["User"]["firstname"] .  ' '  . $user["User"]["lastname"] . ' - ' . $user["User"]["email"];	 
		} 
		foreach ($externals as $external) {
			$assistants[$external["User"]["id"]] = $external["User"]["firstname"] .  ' '  . $external["User"]["lastname"] . ' - ' . $external["User"]["email"];	 
		} 		 
		return $assistants;
	} 

	public function getUsersMeeting($teamId, $userIds){ 
		$conditions    = array("UserTeam.team_id" => $teamId, "UserTeam.user_id" => $userIds, 'UserTeam.user_state' => configure::read("DISABLED"));
		$users 		   = $this->find("all", compact("conditions"));
		$allUser       = array();
		$collaborators = array();
		$assistants    = array(); 
		foreach ($users as $user) {
			if($user["UserTeam"]["type_user"] == configure::read("COLLABORATOR")){ 
				$allUser[] = array(
					"name"   => $user["User"]["firstname"] . ' ' . $user["User"]["lastname"],
					"email"  => $user["User"]["email"],
					"client" => __("Colaborador"),
					"image"	 => $user["User"]["img"]
				); 
			 	$collaborators[] = $user["UserTeam"]["user_id"];
			} else {
				$allUser[] = array(
					"name"   => $user["User"]["firstname"] . ' ' . $user["User"]["lastname"],
					"email"  => $user["User"]["email"],
					"client" => $user["Client"]["name"],
					"image"	 => $user["User"]["img"]
				); 
			 	$assistants[]  = $user["UserTeam"]["user_id"]; 
			}
		}
		return array("collaborators" => $collaborators, "assistants" => $assistants, "Assistant" => $allUser); 
	}

	public function getAllTeams($teamId = null){
		$teams 		    = array();
		$conditions     = array('Team.user_id' => AuthComponent::user('id'));
		if (!is_null($teamId)) {
			$team["Team.id"] = $teamId;
		}  
		$teamsUser      = $this->Team->find("all", compact("conditions")); 
		if (!empty($teamsUser)) {
			$teams = array();
		 	foreach ($teamsUser as $teamUser) { 
		 		$teams[$teamUser["Team"]["id"]] = $teamUser["Team"]["name"]; 
		 	} 
		} 
		return $teams;
	} 

	public function getTotalUser(){
		$limit      = 10;
		$conditions = array("UserTeam.own_user_id" => AuthComponent::user('id'), "UserTeam.user_id !=" => AuthComponent::user('id'));
		$count      = $this->find("count", compact("conditions","limit"));
		return $count;
	}
}
