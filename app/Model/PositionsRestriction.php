<?php
App::uses('AppModel', 'Model');
 
class PositionsRestriction extends AppModel {
 
	public $belongsTo = array(
		'Restriction' => array(
			'className' => 'Restriction',
			'foreignKey' => 'restriction_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Position' => array(
			'className' => 'Position',
			'foreignKey' => 'position_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
