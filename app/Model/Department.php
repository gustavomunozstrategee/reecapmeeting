<?php
App::uses('AppModel', 'Model');
 
class Department extends AppModel {

	public $hasMany = array(
		'UserTeam' => array(
			'className'    => 'UserTeam',
			'foreignKey'   => 'department_id',
		) 
	); 

	public function loadDepartments(){
		$fields = array('Department.id','Department.name');
		return $this->find('list', compact('fields'));
	}
 
	
}
