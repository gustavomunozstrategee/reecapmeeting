$( document ).ready(function() {

    $( ".finish_commiment" ).change(function() {
		  Swal.fire({
			  title: '¿Esta seguro de dar como terminado el compromiso?',
			  text: "¡No podrás revertir esto!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#60c400',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Confirmar',
			  cancelButtonText: '¡No, Cancelar!',
			}).then((result) => {
			  if (result.isConfirmed) {
			  		$.post( "finished_commiment", { id: $(this).data("id") })
					  .done(function( data ) {
					  	if(data){
					  		Swal.fire(
						      '¡Bien!',
						      'Compromiso completado correctamente.',
						      'success'
						    )
						    setTimeout(function(){
						     location.href = "";
						 	}, 4000);
						   
					  	}else{

					  	}
					    
				  	});
			  }else{
			  	$("#check-"+$(this).data("id")).prop("checked",false);
			  }

			})
	});
	$(".solicitarCambioFecha" ).click(function() {
		var id = $(this).data("id");
		var dataPre = $(this).data("date")
		Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Desea solicitar un cambio de fecha?",
		  html: '<br><label class="pull-left">Nueva Fecha:</label><input id="datepickerSEF" readonly class="form-control"><br><label class="pull-left">Motivo:</label><textarea id="textSEF" class="form-control" rows="5"></textarea><br>',
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Solicitar Cambio',
		  preConfirm: (login) => {
		     if($('#datepickerSEF').val() == ""){
		     	 Swal.showValidationMessage(
		          `Debe ingresar una fecha`
		        )
		     }
		     if($('#textSEF').val() == ""){
		     	Swal.showValidationMessage(
		          `Debe ingresar un motivo de rechazo`
		        )

		     }
		  },
		  onOpen:function(){

		  	var date = new Date();
			var currentMonth = date.getMonth();
			var currentDate = date.getDate();
			var currentYear = date.getFullYear(); 
			$('#datepickerSEF').daterangepicker({
				drops: 'up',
				minDate: new Date(currentYear, currentMonth, currentDate),
				"singleDatePicker": true,
	    		"timePicker": false,
	    		"timePicker24Hour": false,
	    		isInvalidDate: function(ele) {
			        var currDate = moment(ele._d).format('YY-MM-DD');
			        return ["17-09-09"].indexOf(currDate) != -1;
			    },
				locale: {
					format: 'YYYY-MM-DD HH:mm'
				},
				"startDate": dataPre
			}); 
			 
		  }
		}).then(function(result) {
			if (result.isConfirmed) {
				console.log("SI ENTRO")
				var fecha = $('#datepickerSEF').val();
				var texto = $('#textSEF').val();
				$.post( "commiment_cambio_fecha", { fecha:fecha, motivo:texto, id:id })
				  .done(function( data ) {
				  	if(data == 1){
				  	 	Swal.fire(
					      '¡Bien!',
					      'Solicitud enviada correctamente.',
					      'success'
					    )
					    setTimeout(function() {
					    	window.location.reload();
					    }, 1500);
				  	} else if(data == 2) { 

				  		Swal.fire(
					      '¡Atención!',
					      'La nueva fecha de edición solicitada no puede ser igual a la fecha inicial del compromiso.',
					      'error'
					    )

				  	} else if(data == 3) { 

				  		Swal.fire(
					      '¡Atención!',
					      'La nueva fecha de edición solicitada no puede ser inferior a la fecha actual (hoy).',
					      'error'
					    )

				  	} else {
				  		Swal.fire(
					      '¡Atención!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
				  	}
			  	});
			}

		})

	});


	$(".solicitarCambioDescripcion" ).click(function() {
		var id = $(this).data("id");
		Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Desea solicitar una edición?",
		  html: '<br><label class="pull-left">¿Cual debe ser la descripción?:</label><textarea id="textSEF" class="form-control" rows="5"></textarea><br>',
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Solicitar Cambio',
		  preConfirm: (login) => {
		     
		     if($('#textSEF').val() == ""){
		     	Swal.showValidationMessage(
		          `Debe ingresar una descripcion`
		        )

		     }
		  },
		  
		}).then(function(result) {
			if (result.isConfirmed) {
				 
				var descripcion = $('#textSEF').val();
				$.post( "commiment_cambio_descripcion", {descripcion:descripcion, id:id })
				  .done(function( data ) {
				  	if(data == 1){
				  	 	Swal.fire(
					      '¡Bien!',
					      'Solicitud enviada correctamente.',
					      'success'
					    )
				  	}else{
				  		Swal.fire(
					      '¡Bien!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
				  	}
			  	});
			}

		})

	});

	$(".rechazar_compromiso" ).click(function() {
		var id = $(this).data("id");
		Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Desea rechazar el  compromiso?",
		  html: '<br><label class="pull-left">Motivo:</label><textarea id="textSEF" class="form-control" rows="5"></textarea><br>',
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Rechazar Compromiso',
		  preConfirm: (login) => {
		     
		     if($('#textSEF').val() == ""){
		     	Swal.showValidationMessage(
		          `Debe ingresar un motivo de rechazo`
		        )

		     }
		  },
		  
		}).then(function(result) {
			if (result.isConfirmed) {
				 
				var descripcion = $('#textSEF').val();
				$.post( "commiment_rechazo", {descripcion:descripcion, id:id })
				  .done(function( data ) {
				  	if(data == 1){
				  	 	Swal.fire(
					      '¡Bien!',
					      'Solicitud enviada correctamente.',
					      'success'
					    )
				  	}else{
				  		Swal.fire(
					      '¡Bien!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
				  	}
			  	});
			}

		})

	});






	 
	$( ".rechazarSolicitud" ).click(function() { 
		var id = $(this).data("id");
		$.post( "rechazarSolicitud", {id:id })
		  .done(function( data ) {
		  	if(data == 1){
		  		$(".divRemove_" + id).remove();
		  	 	Swal.fire(
			      '¡Bien!',
			      'Se rechazo correctamente la solicitud.',
			      'success'
			    )

		  	}else{
		  		Swal.fire(
			      '¡Bien!',
			      'Ha ocurrido un error, intente nuevamente.',
			      'error'
			    )
		  	}
	  	});
		console.log("Solicitud Rechazada")
	});

	$( ".aceptarSolicitud" ).click(function() { 
		var id = $(this).data("id");
		$.post( "aceptarSolicitud", {id:id })
		  .done(function( data ) {
		  	if(data == 1){
		  		$(".divRemove_" + id).remove();
		  	 	Swal.fire(
			      '¡Bien!',
			      'Solicitud aceptada correctamente.',
			      'success'
			    )

		  	}else{
		  		Swal.fire(
			      '¡Bien!',
			      'Ha ocurrido un error, intente nuevamente.',
			      'error'
			    )
		  	}
	  	});
		console.log("Solicitud Aceptada")
	});

	$( ".filtro" ).change(function() { 
		location.href = "?order=" +$(this).val()
	});
	$('#reporte').daterangepicker({
        "singleDatePicker": false,
            "timePicker": false,
            "timePicker24Hour": false,
            locale: {
                format: 'YYYY-MM-DD'
            }
             
    });


    $(".cambioNotificacion" ).click(function() {
		var id = $(this).data("id");
		var dataPre = $(this).data("date")
		Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Desea configurar fecha y hora de  notificación?",
		  html: '<br><label class="pull-left">Fecha Notificación:</label><input id="datepickerSEF" readonly class="form-control"><br><label class="pull-left"><br>',
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Aceptar',
		  preConfirm: (login) => {
		     if($('#datepickerSEF').val() == ""){
		     	 Swal.showValidationMessage(
		          `Debe ingresar una fecha`
		        )
		     }
		  },
		  onOpen:function(){
		  	if(dataPre != "0000-00-00"){
				$('#datepickerSEF').daterangepicker({
					drops: 'up',
					"singleDatePicker": true,
		    		"timePicker": false,
		    		"timePicker24Hour": false,
					locale: {
						format: 'YYYY-MM-DD HH:mm'
					},
					"startDate": dataPre
				});
		  	}else{
		  		$('#datepickerSEF').daterangepicker({
		  			drops: 'up',
					"singleDatePicker": true,
		    		"timePicker": false,
		    		"timePicker24Hour": false,
					locale: {
						format: 'YYYY-MM-DD HH:mm'
					},
				});
		  	}
		  	
			 
		  }
		}).then(function(result) {
			if (result.isConfirmed) {
				var fecha = $('#datepickerSEF').val();
				$.post( "configurarNotificacion", { fecha:fecha,id:id })
				  .done(function( data ) {
				  	if(data == 1){
				  	 	Swal.fire(
					      '¡Bien!',
					      'Notificación configurada correctamente.',
					      'success'
					    )
					     setTimeout(function(){
						     location.href = "";
						 	}, 4000);
				  	}else{
				  		Swal.fire(
					      '¡Bien!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
				  	}
			  	});
			}

		})

	});


	$('body').on('click', '#EnviarRequerimiento', function(event) {
	 	$('#TicketAddForm').parsley().validate()
	 	if($('#TicketAddForm').parsley().isValid()) {
	 		var formData = $('#TicketAddForm').serialize();
			$.ajax({
				dataType:'json',
                type: "POST",
                url: GLOBAL_DATA.URL_FLEX_POINT+"tickets/create_ticket_from_apps",
                data: formData,
                beforeSend: function(){
                	$("#EnviarRequerimiento").prop('disabled', true);
                    $('#preloader').show();
                },
                complete:function(data){

                },
                success: function(data){
                    $('#preloader').hide();
                    if(data.error == false) {
                    	swal(data.message); 
                    	$("#EnviarRequerimiento").prop('disabled', false);                
                 		var url = GLOBAL_DATA.APP_BASE+'/users/login_to_flex_clients/'+'?ticket_id='+data.ticket_id;
 						window.open(url, '_blank');
 						$("#TicketAddForm")[0].reset();
                    } else {
                    	$("#EnviarRequerimiento").prop('disabled', false);  
                    	swal(data.message);
                    } 
                },
                error: function(data){
                    alert("Problemas al tratar de enviar el formulario");
                }
            });  
	 	}
	});



});