var urlFreePlan;
AddPlan = { 

     initElements:function(){ 
          $(function() { 
               $('.btn-buy-plan').on('click', function() { 
                    ELEMENT = $(this);
                    AddPlan.redirectToSaveDataTransaction(ELEMENT);                  
               });  
               $(".btn-change-plan").on('click', function() {
                    ELEMENT = $(this); 
                    AddPlan.changePlanActual(ELEMENT);                
               });
               $('.btn-redeem-coupon').on('click', function() {             
                    AddPlan.reedemCoupon();                  
               });
               $('#save_cache_users_teams').on('click', function() {             
                    AddPlan.saveUsersTeamsInCacheToRenovated();                  
               });
               $('#renovated_users').on('click', function() {             
                    AddPlan.confirmRenovateUsers();                  
               }); 
               $('#reactivate_users').on('click', function() {             
                    AddPlan.chooseProcessToPlan("renovate_all");                  
               }); 
               $('#cancel_reactivate_users').on('click', function() {             
                    AddPlan.chooseProcessToPlan("only_renovated");                  
               });   
               $('body').on('click', '.choose_all', function() {    
                    $(".choose_users_to_renovated").prop("checked", $(this).prop("checked"));                  
               }); 
               AddPlan.chooseRenovateUsersPlanFree();                     
               AddPlan.saveUserToPlanFree();                     
          }); 
     }, 
 
     redirectToSaveDataTransaction:function(){ 
          var planId = ELEMENT.attr("data-id"); 
          $.ajax({ 
          type: "POST",
          url:  GLOBAL_DATA.APP_BASE+'/plans/createReferenceCodeToBuyPlan/',
          data: {planId:planId},          
          dataType: 'json',
               success:  function(data) { 
                    if(typeof data.message !== 'undefined' && data.state == false){
                         swal({type:"error", title:copys_js.error, text:data.message});
                    } else {
                         location.href = "../payus?typePlan="+GLOBAL_DATA.PLAN_INICIAL+""; 
                    }           
               }          
          });
     }, 

     changePlanActual:function(){
          var planId = ELEMENT.attr("data-id");
          $.ajax({ 
          type: "POST",
          url:  GLOBAL_DATA.APP_BASE+'/plans/changePlan/',
          data: {planId:planId},          
          dataType: 'json',
               success:  function(data) { 
                    if(typeof data.message !== 'undefined' && data.state == false){
                         swal({type:"error", title:copys_js.error, text:data.message});
                    } else { 
                         if(data.price_less == true){ 
                              //cuando el plan que se adquiere tiene menos usuarios que el que se tenia actualmente
                              AddPlan.loadUsersTeams(planId);
                         } else if (data.price_less == false) {  
                              //cuando el plan que se adquiere tiene mas usuarios que el que se tenia actualmente
                              $("body").find("#modal_renovated_users").modal("show");
                         } else if (data.state == true) {
                              // cuando solamente es un upgrade 
                              AddPlan.countUsersToRenovated();
                         }  
                    }           
               }          
          });       
     },

     reedemCoupon:function(){
          var codeCoupon = $("#code_coupon").val(); 
          if(codeCoupon != ''){
               $.ajax({ 
               type: "POST",
               url:  GLOBAL_DATA.APP_BASE+'/coupons/getCoupon/',
               data: {codeCoupon:codeCoupon},          
               dataType: 'json',
                    beforeSend: function(e){ 
                         $("body").find(".btn-redeem-coupon").prop( "disabled", true ); 
                         $("#show-loading-content").removeAttr("style");
                    }, 
                    success:  function(data) {  
                         if(data.state == false){
                              swal({type:"error", title:copys_js.error, text:data.message});
                              $("body").find(".btn-redeem-coupon").prop( "disabled", false );
                              $("#show-loading-content").hide(); 
                         } else { 
                              $("body").find(".btn-redeem-coupon").prop( "disabled", false );
                              $("#show-loading-content").removeAttr("style");                         
                              location.href = GLOBAL_DATA.APP_BASE+'/plans/planning_management';                              
                         }     
                    }          
               });
          } else {
               swal({type:"error", title:copys_js.error, text:copys_js.enter_coupon_code});
          }
     },

     loadUsersTeams:function(planId){
          $.ajax({ 
               type: "POST",
               url:  GLOBAL_DATA.APP_BASE+'/user_teams/load_users/',
               data: {planId:planId},          
               dataType: 'html',
               beforeSend: function(e){  
                     
               }, 
               success:function(data) {  
                    $("body").find("#modal_choose_users").modal("show");
                    $("body").find("#show_content_users_teams").html(data);
               }          
          });
     },

     saveUsersTeamsInCacheToRenovated:function(){ 
          var userIds = [];
          $('input[type=checkbox][name="users_teams[]"]:checked').each(function() {
             userIds.push($(this).val()); 
          }); 
          $.ajax({ 
               type: "POST",
               url:  GLOBAL_DATA.APP_BASE+'/user_teams/store_users_to_renovated/',
               data: {userIds:userIds},          
               dataType: 'json', 
               success:function(data) {  
                    if(data.state == false){
                         swal({type:"error", title:copys_js.error, text:data.message});
                    } else {
                        location.href = "../payus?typePlan="+GLOBAL_DATA.CAMBIAR_PLAN+"";   
                    } 
               }          
          }); 
     },

     confirmRenovateUsers:function(){
          AddPlan.chooseProcessToPlan("renovate_all");
          location.href = "../payus?typePlan="+GLOBAL_DATA.CAMBIAR_PLAN+"";
     },

     countUsersToRenovated:function(){
          $.ajax({ 
               type: "POST",
               url:  GLOBAL_DATA.APP_BASE+'/user_teams/check_have_users_disabled/',
               data: {},          
               dataType: 'json', 
               success:function(data) {  
                    if(data.state == true){
                         $("body").find("#count_users_to_renovated").modal("show");
                         $("body").find("#count_users_disabled").html(data.message); 
                    } else if (data.state == false){
                         AddPlan.chooseProcessToPlan("upgrade"); 
                         location.href = "../payus?typePlan="+GLOBAL_DATA.CAMBIAR_PLAN+""; 
                    }
               }          
          });
     },

     chooseProcessToPlan:function(type){
          $.post(GLOBAL_DATA.APP_BASE+'/plans/put_process_plan/',{type},function (resultado){});
     },

     chooseRenovateUsersPlanFree:function(){
          $('body').on('click', '.adquired_plan_free', function(e) {
               e.preventDefault();
               urlFreePlan = $(this).attr("href");
               var planId  = $(this).attr("data-id");    
               $.ajax({ 
                    type: "POST",
                    url:  GLOBAL_DATA.APP_BASE+'/user_teams/load_users_to_plan_free/',
                    data: {planId:planId},          
                    dataType: 'html',
                    beforeSend: function(e){  
                          
                    }, 
                    success:function(data) {  
                         $("body").find("#modal_choose_users_free").modal("show");
                         $("body").find("#show_content_users_teams_free").html(data);
                    }          
               });                    
          }); 
     },

     saveUserToPlanFree:function(){
          $('body').on('click', '#save_users_free', function(e) {
               var userIds = [];
               $('input[type=checkbox][name="users_teams[]"]:checked').each(function() {
                  userIds.push($(this).val()); 
               });
               $.ajax({ 
                    type: "POST",
                    url:  GLOBAL_DATA.APP_BASE+'/user_teams/save_user_free/',
                    data: {userIds:userIds},          
                    dataType: 'json',
                    beforeSend: function(e){  
                          
                    }, 
                    success:function(data) {  
                         if(data.state == false){
                              swal({type:"error", title:copys_js.error, text:data.message});
                         } else {
                              location.href = urlFreePlan; 
                         } 
                    }          
               });      
          }); 
     }
} 

AddPlan.initElements();