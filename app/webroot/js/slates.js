$( document ).ready(function() {
 
	$( ".editListTask" ).change(function() {
      id = $(this).data("id")
      role_id = $(this).val()
      $.post( "../editPermission", {id:id, role_id:role_id })
          .done(function( data ) {
            if(data == 1){
              Swal.fire(
                '¡Bien!',
                'Permisos editados correctamente.',
                'success'
              )
            }else{
              Swal.fire(
                'Error!',
                'Ha ocurrido un error, intente nuevamente.',
                'error'
              )
            }
          });

      
    });

    $( ".removeUser" ).click(function() {
      id = $(this).data("id")
      $.post( "../removeUser", {id:id})
          .done(function( data ) {
            if(data == 1){
            	$(".remove_"+id).remove()
              Swal.fire(
                '¡Bien!',
                'Usuario removido correctamente.',
                'success'
              )
            }else{
              Swal.fire(
                '¡Bien!',
                'Ha ocurrido un error, intente nuevamente.',
                'error'
              )
            }
          });

      
    });



    $(".deleteList").click(function(){
    	var idDELETE = $(this).data("id")
    	Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Desea eliminar la lista?",
		  text: "Los participantes saldrán de la lista y las tareas seran eliminadas permanentemente",
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Aceptar',
		}).then(function(result) {
			 
			if (result.isConfirmed) {
				 
	 
				$.post( "../deleteSlate", {id:idDELETE})
				  .done(function( data ) {
				  	if(data == 1){
				  	 	Swal.fire(
					      '¡Bien!',
					      'Lista eliminada correctamente.',
					      'success'
					    )
					     setTimeout(function(){
						     location.href = "../";
						 	}, 4000);
				  	}else{
				  		Swal.fire(
					      '¡Bien!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
				  	}
			  	});
			}

		})

    });

    $(".outList").click(function(){
    	var idDELETE = $(this).data("id")
    	Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Desea salir de la lista?",
		  text: "Sus tareas continuarán y el administrador deberá indicar el nuevo responsable",
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Aceptar',
		}).then(function(result) {
			 
			if (result.isConfirmed) {
				 
	 
				$.post( "../outList", {id:idDELETE})
				  .done(function( data ) {
				  	if(data == 1){
				  	 	Swal.fire(
					      '¡Bien!',
					      'Has salido de la lista correctamente.',
					      'success'
					    )
					     setTimeout(function(){
						     location.href = "../";
						 	}, 4000);
				  	}else{
				  		Swal.fire(
					      '¡Bien!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
				  	}
			  	});
			}

		})

    });



     
    	$("#listSlates").on("click", ".checkTask", function(){
    	var idDELETE = $(this).data("id");
    	var SlateTask = $("#SlatesTaskSlateId").val();
    	Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Esta seguro de marcar como terminada esta tarea?",
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Aceptar',
		}).then(function(result) {
			 
			if (result.isConfirmed) {
				 
	 
				$.post( "../checkTask", {id:idDELETE})
				  .done(function( data ) {
				  	if(data == 1){
				  		 refresh(SlateTask);
				  		$(".task_remove_"+idDELETE).remove();
				  	 	Swal.fire(
					      '¡Bien!',
					      'Tarea marcada como terminada correctamente.',
					      'success'
					    )
					      
				  	}else{
				  		Swal.fire(
					      '¡Bien!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
					    $("#cb"+idDELETE).prop( "checked", false )
				  	}
			  	});
			}else{
				$("#cb"+idDELETE).prop( "checked", false )
			}

		})

    });






    $("#listSlates").on("click", ".editTask", function(){
    	var id = $(this).data("id");
    	$.ajax({
            type: "GET",
            url: "../../slatestasks/edit_notification/"+id,
            
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            success: function (response) {
                $('#ModalLoad').html(response); //add the partial view into the modal content.
                $('#ModalLoad').modal('show'); //display the modal popup.
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

    });

    


    $("#listSlates").on("click", ".deleteTask", function(){
    	var idDELETE = $(this).data("id")
    	var SlateTask = $("#SlatesTaskSlateId").val();
    	Swal.fire({
		  type: 'question',
		  icon: 'warning',
		  title:"¿Esta seguro de eliminar esta tarea?",
		  customClass: 'swal2-overflow',
		  showCancelButton: true,
		  confirmButtonColor: '#60c400',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Aceptar',
		}).then(function(result) {
			 
			if (result.isConfirmed) {
				 
	 
				$.post( "../deleteTask", {id:idDELETE})
				  .done(function( data ) {
				  	if(data == 1){
				  		 refresh(SlateTask);
				  		$(".task_remove_"+idDELETE).remove();
				  	 	Swal.fire(
					      '¡Bien!',
					      'Tarea eliminada correctamente.',
					      'success'
					    )
					      
				  	}else{
				  		Swal.fire(
					      '¡Bien!',
					      'Ha ocurrido un error, intente nuevamente.',
					      'error'
					    )
					    $("#cb"+idDELETE).prop( "checked", false )
				  	}
			  	});
			}else{
				$("#cb"+idDELETE).prop( "checked", false )
			}

		})

    });





    	

    





});


function refresh(idE){
      $.ajax({
            type: "POST",
            url: "../listSlates",
            data: {id: idE},
            beforeSend: function(){
            },
            complete:function(data){
            },
            success: function(data){
                $("#listSlates").html(data);
            },
            error: function(data){
                alert("Problemas al tratar de enviar el formulario");
            }
        });
          $.ajax({
                type: "POST",
                url: "../Slatesfinished",
                data: {id: idE},
                beforeSend: function(){
                },
                complete:function(data){
                },
                success: function(data){
                    $("#listSlatesFinish").html(data);
                },
                error: function(data){
                    alert("Problemas al tratar de enviar el formulario");
                }
            });
    }

 