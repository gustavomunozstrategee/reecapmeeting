 
EDITA = {
	initElementEdit: function() { 
		EDITA.showApprovalUser(); 
		EDITA.getContactToFinish();
		EDITA.finishContac();
		EDITA.validateContacHavePasswordApprovedContac();
		EDITA.validateContacHavePasswordRefuseContac(); 
		EDITA.approvedContac(); 
		EDITA.refuseContac(); 
        EDITA.getNewApprovalContac();
        EDITA.saveNewApproval();
        //EDITA.initCronoActa();
		//EDITA.pararCronoActa();
		//EDITA.continuarCronoActa();
		//EDITA.continuarCronoActaEdit();
		//EDITA.reiniciarCronoActa();
		EDITA.loadView();
		EDITA.finishEditBorrador();
		EDITA.descartarBorrador();
		//PRECARGAR USUARIOS PARA CALIFICAR EL ACTA
		EDITA.appendUsersToApproval();
		EDITA.appendEmployeesToApproval();
		//FIN PRECARGAR USUARIOS PARA CALIFICAR EL ACTA
		EDITA.viewApprovedContac();
		if(GLOBAL_DATA.action == "add"){ 
			EDITA.preloadDataFromMeetingToContac();
		} 
		if(GLOBAL_DATA.action == "detail_contac_to_approved"){
			EDITA.validateCharactersTextArea();
		}
	},

	validateCharactersTextArea:function(){
		$(document).on('keydown keyup keypress paste', 'textarea', function(e) {  
	        var key = e.keyCode;
	        var textArea = $(this).val(),
	        textLenght   = textArea.length,
	        limit  = 100,
	        remain = textLenght + '/' + (limit - textLenght); 
	        $('#showCharacterPassword').text(remain);   
	        $('#showCharacterWithoutPassword').text(remain); 
	        if (textLenght >= 100) { 
	           if(key == 13 || key == 32){
	                $(this).val((textArea).substring(0, limit));
	                return false;
	           } else {
	                $(this).val((textArea).substring(0, limit)); 
	           } 
	        } 
	    }); 
	},

	viewApprovedContac(){
		$('.btn_approved').click(function(event) {
			var contac_id = $(this).data('id');
			$.ajax({ 
	          type: "POST",
	          url:  GLOBAL_DATA.APP_BASE+'/contacs/view_hystory_approved/'+contac_id,
	          data: {},          
	          dataType: 'html',
	          	success:  function(data) {
	          		$("#viewApprovedContacModal").modal("show");
					$(".resultHystoryApproved").html(data);
	          	}          
	 		});			
		});
	},

	finishEditBorrador(){
		$('.btn_cerrarBorrador').click(function(event) {
			var contac_id = $(this).data('uid');
			swal({
			    title: copys_js.ten_cuidado,
			    text: copys_js.finish_draft_edition,
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: "#DD6B55",
			    confirmButtonText: copys_js.btn_confirm,
	    		cancelButtonText: copys_js.btn_cancel,
			},
			function(){
				$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/update_state_contac',{contac_id:contac_id},function (resultado){
					location.reload();
				});
			});
		});
	},

	descartarBorrador(){
		$('.btn_descartarBorrador').click(function(event) {
			var contac_id = $(this).data('uid');
			var firebase  = $(this).data('firebase');
			swal({
			    title: copys_js.ten_cuidado,
			    text: copys_js.discard_draft,
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: "#DD6B55",
			    confirmButtonText: copys_js.btn_confirm,
	    		cancelButtonText: copys_js.btn_cancel,
			},
			function(){
				$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/descartar_borrador',{contac_id:contac_id,firebase:firebase},function (resultado){
					location.reload();
				});
			});
		});
	},

	showApprovalUser:function(){ 
		if($("#requestApproval").is(':checked')){
			$("#content-approval").show();
		} else {
			$("#content-approval").hide();
		}
    	$("#requestApproval").change(function() {
    		if($("#ContacFuncionarios").val() != "" || $("#ContacExternos").val() != "") {
	    		if(this.checked) {
	    			$("#content-approval").show(); 
	    		} else {
	    			$("#content-approval").hide();
	    		}
    		} else {
    			$('#requestApproval').prop('checked', false);
				swal({title: "", type: "info", text: copys_js.please_select_colaborator});
    		}
		});
	},

	loadView(){
		$('.btn_recargar').click(function(event) {
			location.reload();
		});
	},
 

	//SOLICITAR APROBACIÓN DEL ACTA POR PRIMERA VEZ
	getContactToFinish:function(){
		$('body').on('click', '.btn-get-contac-to-finish', function (event){
			$(".btn-finish-contac").attr('data-id', $(this).attr('data-id'));
			var contacId = $(this).attr('data-id');
			$.ajax({ 
	          type: "POST",
	          url:  GLOBAL_DATA.APP_BASE+'/contacs/validateContactHavePassword/',
	          data: {contacId:contacId},          
	          dataType: 'json',
	          	success:  function(data) {
	          		if(data.state == false){
	          			$("#finishContac").modal("show");
	          			$("#password").hide(); 
	          		} else if(data.state == true) {
	          			$("#passwordContacModalFinish").modal("show");
	          			$("#password").show();
	          		}
	          	}          
	 		});	
		});		
	},	

	finishContac:function(){
		$('body').on('keypress', '.txt-end-minute-pass-index', function (evt){
		    if(window.event){
		        keynum = evt.keyCode;
		    } else{
		        keynum = evt.which;
		    } 
			if(keynum == 13) {
				$('.btn-end-minute-pass-index').click();
			}
		});
		$('body').on('click', '.btn-finish-contac', function (event){
			var contacId = $(this).attr('data-id');
			var password = $("#password").val();
			var data 	 = {};
			if($('#password').is(':visible')) {
      			data = {contacId:contacId, password:password};
    		} else {
    			data = {contacId:contacId};
    		}     
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/save_contac/',
	          data: data,          
	          dataType: 'json',
	           	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find(".btn-finish-contac").prop('disabled', true); 
                },
	          	success:  function(data) { 
	          		if(data.state == false){
	          			if(data.required == true){
	          				$("body").find("p.errorP").hide();
                    		$("body").find(".btn-finish-contac").prop('disabled', false);
                    		swal({type:"error", title:copys_js.error, text:data.message});
	          			} else {
	                    	$("body").find("p.errorP").hide();
	                    	$("body").find(".btn-finish-contac").prop('disabled', false); 
		          			swal({type:"info", title:"", text:data.message}); 
		          			url = GLOBAL_DATA.APP_BASE+'/contacs/edit/'+contacId;
		          			setTimeout("location.href = url", 3000); 
	          			}
	          		} else if(data.state == true) {
                    	$("body").find("p.errorP").hide();
                    	$("body").find(".btn-finish-contac").prop('disabled', true);
	          			swal({type:"info", title:"", text:data.message});
						setTimeout("location.href = ''", 3000); 
	          		}
	          	}          
	 		});
		});
	},  
	//FIN PROCESO SOLICITAR APROBACIÓN DEL ACTA

	//validación contraseña del acta al momento de querer aprobar
	validateContacHavePasswordApprovedContac:function(){
		$('body').on('click', '#btn-approval-contac', function (event){
			var contacId = $(this).attr('data-id');
			$(".btn-approved").attr('data-approval', $(this).attr('data-approval')); 
			$(".btn-approved").attr('data-contac', $(this).attr('data-id')); 
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/validateContactHavePassword/',
	          data: {contacId:contacId},          
	          dataType: 'json',
	          	success:  function(data) { 
	          		if (data.state == true) {
	          			$("#passwordContacModal").modal("show");	          		
	          		} else if (data.state == false) {
	          			$("#passwordContacModalWithOutPassword").modal("show");
	          		}
	          	}          
	 		});
		});
	},

	//validación contraseña del acta al momento de querer rechazar desde la plataforma
	validateContacHavePasswordRefuseContac:function(){
		$('body').on('click', '#btn-denied-contac', function (event){
			var contacId = $(this).attr('data-id');
			$(".btn-refuse-contac").attr('data-approval', $(this).attr('data-approval'));
			$(".btn-refuse-contac").attr('data-contac', $(this).attr('data-id')); 
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/validateContactHavePassword/',
	          data: {contacId:contacId},          
	          dataType: 'json',
	          	success:  function(data) { 
	          		if (data.state == true) {
	          			$("#refuseContacWithPassword").modal("show");	          		
	          		} else if (data.state == false) {
	          			$("#refuseContacWithoutPassword").modal("show");
	          		}
	          	}          
	 		});
		});
	},

	//PROCESO APROBAR EL ACTA 
	approvedContac:function(){
		$("#passwordContacModal").on('keypress',function(e) {
		    if(e.which == 13) {
		        $(".btn-approved").click();
		    }
		});
		$('body').on('click', '.btn-approved', function (event){		 
			var contacId   = $(this).attr('data-contac');
			var approvalId = $(this).attr('data-approval');
			var password   = $("#password").val();
			var data 	   = {};
			var approved   = true;
			if($('#password').is(':visible')) {
      			data = {contacId:contacId, approvalId:approvalId, approved:approved, password:password};
    		} else {
    			data = {contacId:contacId, approvalId:approvalId, approved:approved};
    		}    
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/rateContac/',
	          data: data,          
	          dataType: 'json',
	          	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find(".btn-approved").prop('disabled', true); 
                },
	          	success:  function(data) {  
	          		if (data.state == false) {
	          			$("body").find("p.errorP").hide();
                    	$("body").find(".btn-approved").prop('disabled', false);
      			      	swal({type:"error", title:copys_js.error, text:data.message});    		
	          		} else if(data.state == true) {
          				$("body").find("p.errorP").hide();
                    	$("body").find(".btn-approved").prop('disabled', false);
      			      	swal({type:"success", title:copys_js.exito, text:data.message});
						location.href = GLOBAL_DATA.APP_BASE+'/contacs/index/';
	          		} 
	          	}          
	 		});
		});
	},

	refuseContac:function(){
		$('body').on('click', '.btn-refuse-contac', function (event){		 
			var contacId   = $(this).attr('data-contac');
			var approvalId = $(this).attr('data-approval');
			var password   = $("#passwordRefuse").val();			
			var data 	   = {};
			if($('#passwordRefuse').is(':visible')) {
				comment = $("#comment").val(); 
      			data    = {contacId:contacId, approvalId:approvalId, comment:comment, password:password};
    		} else {
    			comment = $("#commentContac").val(); 
    			data    = {contacId:contacId, approvalId:approvalId, comment:comment};
    		}    
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/refuse_contac/',
	          data: data,          
	          dataType: 'json',
	          	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find(".btn-refuse-contac").prop('disabled', true); 
                },
	          	success:  function(data) {  
	          		if (data.state == false) {
	          			$("body").find("p.errorP").hide();
                    	$("body").find(".btn-refuse-contac").prop('disabled', false);
      			      	swal({type:"error", title:copys_js.error, text:data.message});    		
	          		} else if(data.state == true) {
          				$("body").find("p.errorP").hide();
                    	$("body").find(".btn-refuse-contac").prop('disabled', false);
      			      	swal({type:"success", title:copys_js.exito, text:data.message});
						location.href = GLOBAL_DATA.APP_BASE+'/contacs/index/';
	          		} 
	          	}          
	 		});
		});
	},

	//SOLICITAR NUEVAMENTE APROBACIÓN DEL ACTA
	getNewApprovalContac:function(){
		$('body').on('click', '.btn-get-contac-to-finish-again', function (event){
			$(".btn-new-approval-contac").attr('data-id', $(this).attr('data-id'));
			var contacId = $(this).attr('data-id');
			$.ajax({ 
	          type: "POST",
	          url:  GLOBAL_DATA.APP_BASE+'/contacs/validateContactHavePassword/',
	          data: {contacId:contacId},          
	          dataType: 'json',
	          	success:  function(data) {
	          		if(data.state == false){
	          			$("#NewApprovalContac").modal("show");
	          			$("#passNewApproval").hide(); 	          			 
	          		} else if(data.state == true) {
	          			$("#NewApprovalContacPassword").modal("show");
	          			$("#passNewApproval").show(); 
	          		}
	          	}          
	 		});	
		});		
	},

	saveNewApproval:function(){
		$('body').on('click', '.btn-new-approval-contac', function (event){
			var contacId = $(this).attr('data-id');
			var password = $("#passNewApproval").val();
			var data     = {};
			if($('#passNewApproval').is(':visible')) { 
				data = {contacId:contacId, password:password};
			} else {
				data = {contacId:contacId};
			} 
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/new_approval/',
	          data: data,          
	          dataType: 'json',
	           	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find(".btn-new-approval-contac").prop('disabled', true); 
                },
	          	success:  function(data) {
	          		if(data.state == false){
                    	$("body").find("p.errorP").hide();
                    	$("body").find(".btn-new-approval-contac").prop('disabled', false); 
	          			swal({type:"error", title:copys_js.error, text:data.message});
	          		} else if(data.state == true) {
                    	$("body").find("p.errorP").hide();
                    	$("body").find(".btn-new-approval-contac").prop('disabled', true);
	          			swal({type:"success", title:copys_js.exito, text:data.message});
						setTimeout("location.href = ''", 2500); 
	          		}
	          	}          
	 		});
		});
	}, 
	// FIN SOLICITAR NUEVAMENTE APROBACIÓN DEL ACTA
	
	initCronoActa:function(){
		$('body').on('click', '#inicio', function (event){  
			inicio();
		});
	},

	pararCronoActa:function(){
		$('body').on('click', '#parar', function (event){  
			parar();
		});
	},

	continuarCronoActa:function(){
		$('body').on('click', '#continuar', function (event){  
			inicio();
		});
	},

	reiniciarCronoActa:function(){
		$('body').on('click', '#reinicio', function (event){  
			reinicio();
		});
	},

	continuarCronoActaEdit:function(){
		$('body').on('click', '#continuarEdit', function (event){ 
			inicio();
		});
	},

	// PROCESO PARA LLENAR EL SELECT DE LOS USUARIOS PARA APROBAR
	appendUsersToApproval:function(){
		$('body').on('change', '#ContacFuncionarios', function (event){  
	        var funcionarios    = document.getElementById('ContacFuncionarios');
	        var externos        = document.getElementById('ContacExternos');
	        var approvals       = document.getElementById('ContacApprovalusers');
		    var usersIds    	= [];
		    var employeeIds 	= []; 
	     	var usersApprovals  = [];
	     	if (funcionarios !== null) {
			    for(i = 0; i < funcionarios.length; i++){
			        if(funcionarios.options[i].selected){
			            usersIds.push(funcionarios.options[i].value);
			        }
			    }
			}
		    if (externos !== null) {
			    for(i = 0; i < externos.length; i++){
			        if(externos.options[i].selected){
			            employeeIds.push(externos.options[i].value);
			        }
			    }
			}
			if (approvals !== null) { 
			    for(i = 0; i < approvals.length; i++){
			        if(approvals.options[i].selected){
			            usersApprovals.push(approvals.options[i].value);
			        }
			    }
		    }  
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/list_users_to_approval/',
	          data: {usersIds:usersIds, employeeIds:employeeIds, usersApprovals:usersApprovals},          
	          dataType: 'html',
	          	success:  function(data) {  
	          		$("#load_content_user_approval").html(data);
	          		$('.select_approval_user').select2({
						language: 'es'
					}); 
	          	}          
	 		}); 
		});		
	},

	appendEmployeesToApproval:function(){
		$('body').on('change', '#ContacExternos', function (event){ 
	 	  	var funcionarios   = document.getElementById('ContacFuncionarios');
    		var externos       = document.getElementById('ContacExternos');
    		var approvals      = document.getElementById('ContacApprovalusers');
		    var usersIds       = [];
		    var employeeIds    = [];
		    var usersApprovals = [];
		    if (funcionarios !== null) { 
			    for(i = 0; i < funcionarios.length; i++){
			        if(funcionarios.options[i].selected){
			            usersIds.push(funcionarios.options[i].value);
			        }
			    }
			} 
			if (externos !== null) {
			    for(i = 0; i < externos.length; i++){
			        if(externos.options[i].selected){
			            employeeIds.push(externos.options[i].value);
			        }
			    } 
			}
		    if (approvals !== null) { 
			    for(i = 0; i < approvals.length; i++){
			        if(approvals.options[i].selected){
			            usersApprovals.push(approvals.options[i].value);
			        }
			    }
		    }  
		    $.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/contacs/list_users_to_approval/',
	          data: {usersIds:usersIds, employeeIds:employeeIds, usersApprovals:usersApprovals},          
	          dataType: 'html',
	          	success:  function(data) {  
	          		$("#load_content_user_approval").html(data);
	          		$('.select_approval_user').select2({
						language: 'es'
					}); 
	          	}          
	 		}); 
		});		
	}, 

	preloadDataFromMeetingToContac:function(){
		var users = [];
		if(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING != null){
	    	$("#ContacTeamId").val(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING);
	    } 
	    if(GLOBAL_DATA.CONTAC_START_DATE_MEETING != null){
	    	$("#ContacStartDate").val(GLOBAL_DATA.CONTAC_START_DATE_MEETING);
	    }
		if(GLOBAL_DATA.CONTAC_CLIENT_ID_MEETING != null){
			$("#ContacClientId").val(GLOBAL_DATA.CONTAC_CLIENT_ID_MEETING);
		} 
		if(GLOBAL_DATA.CONTAC_MEETING_ID != null){
			$("#ContacMeetingId").val(GLOBAL_DATA.CONTAC_MEETING_ID);
		}     
	}
	
}

EDITA.initElementEdit();
