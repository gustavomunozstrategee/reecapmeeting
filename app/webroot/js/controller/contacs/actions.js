var COUNT = 0;
var initContadorGuardado = 0; 
var cargarDatos = 0;
AJAX_GUARDAR_BORRADOR = null; 
 
$(window).on("load",function() {
 $(".body-loading").delay(1000).fadeOut("slow");
 	if(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING != null){
 		var project_id     = $("#ContacProjectId").val();
 		if(GLOBAL_DATA.action == "add" || GLOBAL_DATA.action == "edit"){
			showImageProject(project_id);
		}
 	}
}); 
 
EDIT = {
	initFunctionAddEdit: function(){
		EDIT.showProtectedPassword();  
		EDIT.actionReadyDocument();
		EDIT.initLibCampos();
		EDIT.changeCliente();
		EDIT.dateTimeContact();
		EDIT.dateContac();
		EDIT.selectedDatePicker();
		EDIT.removeCommitment();
		EDIT.btnBorrador();
		EDIT.btnGuardar();
		EDIT.dropzoneImagenes();
		EDIT.dropzoneDocument();
		EDIT.btnModalAddClient();
		EDIT.btnSaveAddCliente();
		EDIT.btnModalAddProject();
		EDIT.btnSaveAddProject(); 
		EDIT.btnModalAddPlantilla();
		EDIT.btnSaveAddTemplate(); 
		EDIT.btnSaveEditTemplate(); 
		EDIT.btnModalEditPlantilla(); 
		EDIT.selectPlantillaUser();
		EDIT.btnRemoveTemplate();
		//Cargar los clientes dependiendo del equipo de trabajo seleccionado
		EDIT.changeTeamAndLoadClient(); 
		EDIT.chooseTeamWork(); 
		EDIT.showCalendarDates();
		EDIT.uploadImageBrandWhite();
		EDIT.selectAllValues();
		EDIT.changeProject();
	},

	initElementAdd: function() {
		EDIT.initFunctionAddEdit();
		EDIT.unfinishedMeeting();
		EDIT.btnGenerate();
		EDIT.showPassword();
		EDIT.hidePassword(); 
		EDIT.initialChrono();
		EDIT.stopChrono();
		EDIT.continueChrono(); 
		EDIT.viewInfoMeetingActual(); 
		EDIT.btnAutoGuardadoAdd();  
		EDIT.showApprovalFields(); 
		EDIT.protectedActa();
	},

	initElementEdit: function() { 
		if (sessionStorage.getItem('visitedEditPage')) {
		    sessionStorage.removeItem('visitedEditPage'); // Elimina el indicador para que no se recargue la página otra vez
    		location.reload();
  		}
		EDIT.showPassword();
		EDIT.hidePassword(); 
		EDIT.btnGenerate();
		EDIT.protectedActa(); 
		EDIT.initFunctionAddEdit();
		EDIT.deleteImagen();
		EDIT.deleteDocumeto();  
		EDIT.cerrarPagina();
		EDIT.passwordContact();
		EDIT.openModalPreloadInfoFirebase();
		EDIT.changeCliente();
		EDIT.viewInfoMeetingContacSelected(); 
	},

	initElementView: function() {
		EDIT.initLibCamposView();
		EDIT.actionReadyDocument();
		EDIT.dateContac();
		EDIT.findAsistentesFuncionario();
		EDIT.passwordContact();
		EDIT.cargarImagenes();
		EDIT.cargarDocumentos();
		if (GLOBAL_DATA.stateContac == GLOBAL_DATA.ENABLED) {
			EDIT.openModalPreloadInfoFirebase();
		}
	},

	showCalendarDates:function(){
		$('body').on('click', '.input-group-addon', function (event){
			$(this).parent('.input-group').children('.date_all').click(); 
		}); 
	},

	//SELECCIONAR EQUIPO DE TRABAJO
	chooseTeamWork:function(){ 
		var teamChoose 	   = $("#ContacTeamId").val();
		var clienteChoosed = $("#ContacClientId").val();
		var project_id     = $("#ContacProjectId").val();
		if(GLOBAL_DATA.action == "add"){			 
			EDIT.permissionUploadWhiteLabel(teamChoose);
			EDIT.permissionUploadTemplate(teamChoose);
			EDIT.loadUsersApproval(); 
			if(project_id.length > 0){
				showImageProject(project_id);
			} 
			setTimeout(function() {
				EDIT.templateList();
			}, 500); 
		}
		$('#modalChooseTeamWork').modal("show"); 
		// if(teamChoose.length < 1){
		// 	$('#modalChooseTeamWork').modal("show"); 
		// }
		/*
		if(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING != null){ 
    	 	$("#ContacTeamId").change(); 
	    } else {
			$("#ContacTeamId").change(function () { 
				var team = $("#ContacTeamId").val();
				if(team.length < 1){
					swal({type:"info", title:"", text:copys_js.choose_team});
					return false;
				}
				EDIT.showLastContac($("#ContacTeamId").val());
				$('#modalChooseTeamWork').modal("hide");
			}); 
	    }
	    */
	},

	openModalPreloadInfoFirebase: function(){
		$('#modalPreloadFirebase').modal("show");
	},

	closeModalPreloadInfoFirebase: function(){
		if (cargarDatos == 0){
			
			EDIT.templateList();
			var clienteChoosed = $("#ContacClientId").val();
			var teamId   	   = $("#ContacTeamId").val();
			var project_id     = $("#ContacProjectId").val();
			if(GLOBAL_DATA.action == "edit"){
				EDIT.permissionUploadWhiteLabel(teamId);
				EDIT.permissionUploadTemplate(teamId); 
				EDIT.showLastContac(teamId);
				EDIT.loadUsersApproval(true);  
				showImageProject(project_id); 
			}
			cargarDatos = -1;
			if(GLOBAL_DATA.action == "view"){
				EDIT.permissionUploadWhiteLabel(valores.team_id); 
			 	setTimeout(function() {
        			$("body").find('a.showImages').magnificPopup({type:'image'}); 
    			}, 500); 
			}
			setTimeout(function() {
				$('body').find("#modalPreloadFirebase").modal("hide");
			}, 1000);
		}
	},

	actionReadyDocument: function(){
		$('body').keypress(function(event) {
			if (event.charCode == '13') {
				return false;
			}
		});
	},

	confirmLeave: function(){
		var contac_id = $('#ContacId').val();
		EDIT.autoGuardado(); 
	},

	cerrarPagina: function(){  
		var prevKey="";
		$(document).keydown(function (e) { 
		    if (e.key.toUpperCase() == "W" && prevKey == "CONTROL") {                
		        window.onbeforeunload = EDIT.confirmLeave();
		    }
		    else if (e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
		        window.onbeforeunload = EDIT.confirmLeave();
		    }
		    else if (e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
		        window.onbeforeunload = EDIT.confirmLeave();
		    }
		    prevKey = e.key.toUpperCase();
		}); 
	},

	initLibCampos: function(){
		$('.select2_all').select2({
			language: 'es',
			dropdownParent: $('#modalChooseTeamWork'),
			
		});
		$('.selectclient').select2({
			language: 'es',
		});
		$('#copiasarray').tagsinput();
		if(GLOBAL_DATA.action == 'add'){
			EDIT.implementsCkeditor('ContacDescription');
			$('.selectExternos').hide();
			$('#btnBorrador').hide();
			$('.selectProject').hide();
			$('#ContacProjectId').hide();
			$('.lbl_proyecto').hide();
			$('.lbl_password').hide();
			$('#ContacPassword').hide();
			$('.lbl_generatePassword').hide();
			$('.lbl_verPassword').hide();
			$('.lbl_ocultarPassword').hide();
			$('#btn_generatePassword').hide();
			$('#btn_verPassword').hide();
			$('#btn_ocultarPassword').hide();
			$('#btn_generatePassword').prop({'disabled': true});
			$('#content-assistents-add').hide();
		} else { 
			$('.select2_project').select2({
				language: 'es'
			});
		}
		
	},

	initLibCamposView: function(){
		$('.select2_all').select2({
			language: 'es'
		});
		$(".select2_all").prop("disabled", true);
    	$(".select2_convert").prop("disabled", true);
	},

	findAsistentesFuncionario(){
		if (GLOBAL_DATA.firebaseCodigo == GLOBAL_DATA.DISABLED) { 
			$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/find_commitments_contac_finalizado',{},function (resultado){
				$("body").find("#content-table").append(resultado);
				EDIT.initLibCamposView();
			}); 
		}
	},

	implementsCkeditor: function(id_texto){ 
		if(GLOBAL_DATA.LANG == "esp"){
			CKEDITOR.replace(id_texto, {
				language: 'es'
			} );
		} else {
			CKEDITOR.replace(id_texto, {
				language: 'en'
			} );
		}
	},


	btnModalAddPlantilla: function(){
		$('body').on('click', '#btn_modalAddPlantilla', function(event) { 
			var teamId = $("#ContacTeamId").val();
			$.post(GLOBAL_DATA.app_root+'Templates/add_template_acta',{teamId:teamId},function (resultado){
				$('#resultadoViewAddTemplate').html(resultado);
				EDIT.implementsCkeditor('TemplateDescription');
				setTimeout(function(){  
					$('#modalAddTemplate').modal("show");
				}, 500);
			});
		});
	},

	btnSaveAddTemplate: function (){
		$('body').on('click', '#btn_saveAddTemplate', function (event){
			event.preventDefault();
			var description = CKEDITOR.instances['TemplateDescription'].getData();
			var name 	    = $('#TemplateName').val();
			var teamId      = $("#TemplateTeamId").val();
			$.post(GLOBAL_DATA.app_root+'Templates/add_template_acta_save',{description:description, name:name, teamId:teamId},function (resultado){
				var obj = JSON.parse(resultado); 
				if (obj.errors != "") {
					swal({type:"error", title:copys_js.error, text:obj.errors});
				}
				if (obj.success != 0) {
					EDIT.alertCamposAjaxSucces(obj.success);
					EDIT.templateList();
					$('#modalAddTemplate').modal("hide");
				}
			});
	    });
	},

	btnModalEditPlantilla:function(){
		$('body').on('click', '.btn_edit_template', function(event) { 
			var edit       = 'false';
			var teamId     = $("#ContacTeamId").val();
			var templateId = $(this).attr('data-uid'); 
			$.post(GLOBAL_DATA.app_root+'Templates/edit',{teamId:teamId, edit:edit, templateId:templateId },function (resultado){
				$('#resultadoViewAddTemplate').html(resultado);
				EDIT.implementsCkeditor('TemplateDescription');
				setTimeout(function(){  
					$('#modalAddTemplate').modal("show");
				}, 500);
			});
		});
	},

	btnSaveEditTemplate: function (){
		$('body').on('click', '#btn_editAddTemplate', function (event){
			event.preventDefault();
			var description = CKEDITOR.instances['TemplateDescription'].getData();
			var name 	    = $('#TemplateName').val();
			var teamId      = $("#TemplateTeamId").val();
			var templateId  = $("#TemplateId").val();
			var edit  		= 'true';
			$.post(GLOBAL_DATA.app_root+'Templates/edit',{templateId:templateId, edit:edit, description:description, name:name, teamId:teamId},function (resultado){
				var obj = JSON.parse(resultado); 
				if (obj.errors != "") {
					swal({type:"error", title:copys_js.error, text:obj.errors});
				}
				if (obj.success != 0) {
					EDIT.alertCamposAjaxSucces(obj.success);
					EDIT.templateList();
					$('#modalAddTemplate').modal("hide");
				}
			});
	    });
	},

	//Listar plantillas de actas
	templateList: function(){ 
		if(GLOBAL_DATA.action != "view"){
			var teamId = $("#ContacTeamId").val(); 
			$.post(GLOBAL_DATA.app_root+'Templates/template_list',{teamId:teamId},function (resultado){
				$("body").find('.listTemplate').html(resultado);
			});
		}
	},

	btnRemoveTemplate: function (){
		$('body').on('click', '.btn_remove_template',function(event) {
			var template_id = $(this).data('uid');
			swal({
			    title: copys_js.ten_cuidado,
			    text: copys_js.delete_template,
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: "#DD6B55",
			    confirmButtonText: copys_js.btn_confirm,
			    cancelButtonText: copys_js.btn_cancel,
			},
			function(){
				$.post(GLOBAL_DATA.app_root+'Templates/deleteTemplateId',{template_id:template_id},function (resultado){
					EDIT.templateList();
					EDIT.alertCamposAjaxSucces(resultado);
				});
			});
		});
	},

	selectPlantillaUser: function(){
		$('body').on('change', '.click_template',function(event) {
			var elementos = document.getElementsByName('click_template');
			var valor = "";
			for(i=0;i<elementos.length;i++) {
		        if (elementos[i].checked) { 
		            valor = elementos[i].value;
		        } 
		    }
		    EDIT.showPlantilla(valor);
		});
	},

	showPlantilla: function (plantilla_id){
		$.post(GLOBAL_DATA.app_root+'Templates/show_plantilla',{plantilla_id:plantilla_id},function (resultado){
			CKEDITOR.instances['ContacDescription'].destroy(true);
			$('#ContacDescription').val(resultado);
			EDIT.implementsCkeditor('ContacDescription');
		});
	},  

	btnModalAddEmployee: function (){
		$('#btn_modalAddEmployee').click(function(event) {
			var cliente = $("#ContacClientId").val();
			$.post(GLOBAL_DATA.app_root+'Employees/add_employees',{cliente:cliente},function (resultado){
				$('#resultadoViewAddEmployee').html(resultado);
				$('#modalAddEmployee').modal("show");
			});
		});
	}, 
	
	btnModalAddProject: function (){
		$('#btn_modalAddProject').click(function(event) { 
			var cliente = $("#ContacClientId").val();
			$.post(GLOBAL_DATA.app_root+'projects/add_project',{cliente:cliente},function (resultado){
				$('#resultadoViewAddProject').html(resultado);
				$('#modalAddProject').modal("show");
				$('#ProjectUsers').select2({
					language: 'es'
				});
				setTimeout(function() {
					Utilities.asignBuckupEmailVisual();    
				}, 500);
			});
		});
	},

	//CREAR PROYECTO DESDE EL ACTA
	btnSaveAddProject:function(){
		$('body').on('click', '#btn_saveAddProyecto', function (event){
			event.preventDefault();
			$.ajax({ 
				type: "POST",
				contentType:false,
				cache: false,
				processData:false,
				url: GLOBAL_DATA.app_root+'projects/add_project_save',
				data: new FormData($("#ProjectAddProjectForm")[0]),
				dataType: 'json',
				success:  function(data) {
					if (data.errors != "") { 
						swal({type:"error", title:copys_js.error, text:data.errors}); 
					}
					if (data.success != 0) {
						swal({type:"success", title:copys_js.exito, text:data.success});  
					}
					if (data.id != 0) {
						EDIT.updateProjecttList(data.id);
						showImageProject(data.id);
					}
				}
	 		});
	    });
	},

	//ACTUALIZAR LA LISTA DE PROYECTOS
	updateProjecttList: function(project_id){
		$.post(GLOBAL_DATA.app_root+'projects/find_last_projects',{project_id:project_id},function (resultado){
			$('#ContacProjectId').append(resultado);
			$('#ContacProjectId').val(project_id);
			$('.select2_all').select2({
				language: 'es'
			});
			$('#ContacProjectId').selectpicker('refresh');  
			$('#modalAddProject').modal("hide");
			//EDIT.changeAsistentes();
		});
	},

	btnModalAddClient: function (){
		$('#btn_modalAddClient').click(function(event) {
			var teamId = $("#ContacTeamId").val(); 
			$.post(GLOBAL_DATA.app_root+'clients/add_client',{teamId:teamId},function (resultado){
				$('#resultadoViewAddClient').html(resultado);
				$('#modalAddClient').modal("show");
			});
		});
	},

	btnSaveAddCliente:function(){
		$('body').on('click', '#btn_saveAddCliente', function (event){
			event.preventDefault();
			$.ajax({ 
				type: "POST",
				contentType:false,
				cache: false,
				processData:false,
				url: GLOBAL_DATA.app_root+'clients/add_client_save',
				data:  new FormData($("#ClientAddClientForm")[0]),
				dataType: 'json',
				success:  function(data) {
					if (data.errors != "") {
						swal({type:"error", title:copys_js.error, text:data.errors});
					}
					if (data.success != 0) {
						swal({type:"success", title:copys_js.exito, text:data.success});
					}
					if (data.id != 0) {
						EDIT.updateClientList(data.id);
						// showImageClient(data.id);
					}
				}
	 		});	        
	    });
	},

	//ACTUALIZAR LISTA DE CLIENTES Y PROYECTOS CUANDO SE CREA UN CLIENTE NUEVO
	updateClientList: function(client_id){
		teamId = $("#ContacTeamId").val();
		$.post(GLOBAL_DATA.app_root+'clients/find_last_client',{client_id:client_id},function (resultado){
			$('#ContacClientId').append(resultado);
			$('#ContacClientId').val(client_id);
			$('#ContacProjectId').val('');
			$('.select2_all').select2({
				language: 'es'
			});
			$('#modalAddClient').modal("hide");
			$('#ContacClientId').selectpicker('refresh'); 
			EDIT.loadExternalAssistants(teamId, client_id);
			EDIT.loadAssistantsCommitments(teamId, client_id); 
			EDIT.changeDataProject();
		});
	},

	btnGenerate: function(){
		$('#btn_generatePassword').click(function(event) {
			$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/generatePassword',{},function (resultado){
				$('#ContacPassword').val(resultado);
			});
		});
	}, 

	showPassword:function(){
		$('#btn_verPassword').click(function(event) {
			$('#ContacPassword').prop('type', 'text');
			$('.lbl_ocultarPassword').show();
			$('#btn_ocultarPassword').show();
			$('#btn_verPassword').hide();
			$('.lbl_verPassword').hide();
		});
	}, 

	hidePassword:function(){
		$('#btn_ocultarPassword').click(function(event) {
			$('#ContacPassword').prop('type', 'password');
			$('.lbl_verPassword').show();
			$('.lbl_ocultarPassword').hide();
			$('#btn_ocultarPassword').hide();
			$('#btn_verPassword').show();
		});
	},

	protectedActa: function(){
		$('#ContacProteger').change(function() { 
			if ($(this).is(':checked')) {
				$("#content-password").show();
				$('.lbl_password').show();
				$('.lbl_generatePassword').show();
				$('#ContacPassword').show();
				$('#btn_generatePassword').show();
				$('.lbl_verPassword').show();
				$('#btn_verPassword').show();
				$('#btn_generatePassword').prop({
					'disabled': false,
				});
			} else {
				$("#content-password").show();
				$('.lbl_password').hide();
				$('.lbl_generatePassword').hide();
				$('#ContacPassword').hide();
				$('#ContacPassword').val('');
				$('#btn_generatePassword').hide();
				$('.lbl_verPassword').hide();
				$('.lbl_ocultarPassword').hide();
				$('#btn_generatePassword').hide();
				$('#btn_verPassword').hide();
				$('#btn_ocultarPassword').hide();
				$("#ContacPassword").attr("readonly",false);
			}
		});
		if(GLOBAL_DATA.action == "edit"){
			if ($("#ContacProteger").is(':checked')) {
				$("#content-password").show();
				$('.lbl_password').show();
				$('.lbl_generatePassword').show();
				$('#ContacPassword').show();
				$('#btn_generatePassword').show();
				$('.lbl_verPassword').show();
				$('#btn_verPassword').show();
				$('#btn_generatePassword').prop({
					'disabled': false,
				});
				$('#btn_ocultarPassword').hide();
			} else {
				$("#content-password").show();
				$('.lbl_password').hide();
				$('.lbl_generatePassword').hide();
				$('#ContacPassword').hide();
				$('#ContacPassword').val('');
				$('#btn_generatePassword').hide();
				$('.lbl_verPassword').hide();
				$('.lbl_ocultarPassword').hide();
				$('#btn_generatePassword').hide();
				$('#btn_verPassword').hide();
				$('#btn_ocultarPassword').hide();
				$("#ContacPassword").attr("readonly",false);
			}
		}
	},
 

	//FUNCION PARA CARGAR LOS CLIENTES DEPENDIENDO DEL EQUIPO DE TRABAJO
	changeTeamAndLoadClient:function(){
		$.fn.selectpicker.Constructor.BootstrapVersion = '4';
		$("#ContacTeamId").change(function () { 
			return;
			var teamId  = $(this).val(); 
			var cliente = $("#ContacClientId").val(); 
			$.post(GLOBAL_DATA.app_root+'teams/find_clients',{teamId:teamId},function (resultado){
				$('#load_clients_team').html(resultado);
				//$('#ContacClientId').select2({
				//	language: 'es'
				//s});
				$('#ContacClientId').selectpicker();  
				EDIT.changeDataProject(); 
			});
			EDIT.permissionUploadWhiteLabel(teamId);
			EDIT.permissionUploadTemplate(teamId);
			if(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING != null){
				EDIT.loadCollaborator(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING);
				EDIT.loadExternalAssistants(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING, GLOBAL_DATA.CONTAC_CLIENT_ID_MEETING);
				EDIT.loadAssistantsCommitments(GLOBAL_DATA.CONTAC_TEAM_ID_MEETING, GLOBAL_DATA.CONTAC_CLIENT_ID_MEETING);
				EDIT.templateList();  
				EDIT.loadUsersApproval();
			} else { 
				EDIT.loadCollaborator(teamId);
				EDIT.loadExternalAssistants(teamId, cliente); 
				EDIT.loadAssistantsCommitments(teamId, cliente);
				EDIT.templateList(); 
				Utilities.showLoading(true);
				EDIT.autoGuardadoAdd();
			}
		});
	},

	//FUNCION PARA VERIFICAR SI EL EQUIPO DE TRABAJO SELECCIONADO TIENE PERMISOS DE SUBIR LA MARCA BLANCA
	permissionUploadWhiteLabel:function(teamId){
		var action = GLOBAL_DATA.action;
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/teams/check_permission_team_upload_white_label/',
          data: {teamId:teamId, action:action},          
          dataType: 'html',
          	success:  function(data) {  
				$("#permission_white_label").html(data); 
				$("body").find('a.showImages').magnificPopup({type:'image'});
          	}          
 		}); 
	},

	//FUNCION PARA VERIFICAR SI EL EQUIPO DE TRABAJO SELECCIONADO TIENE PERMISOS DE CREAR PLANTILLAS
	permissionUploadTemplate:function(teamId){
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/teams/check_permission_team_create_template/',
          data: {teamId:teamId},          
          dataType: 'html',
          	success:  function(data) {  
				$("#permission_create_template").html(data); 
          	}          
 		}); 
	},

	//CARGAR COLABORADORES DEL EQUIPO DE TRABAJO
	loadCollaborator:function(teamId){
		$.post(GLOBAL_DATA.app_root+'user_teams/find_collaborators',{teamId:teamId},function (resultado){
			$('#load_collaborators_team').html(resultado);
			$('#ContacFuncionarios').select2({
				language: 'es'
			});  
		});
	},
 
 	//CARGAR LOS PROYECTOS DEL CLIENTE
	changeCliente: function(){
		$('body').on('change', '#ContacClientId', function() {
			var cliente = $("#ContacClientId").val();
			var teamId  = $("#ContacTeamId").val();
			$.post(GLOBAL_DATA.app_root+'projects/find_projects',{cliente:cliente},function (resultado){
				$('#load_projects_client').html(resultado);
				//$('#ContacProjectId').select2({
				//	language: 'es'
				//}); 
				$("#ContacProjectId").selectpicker();
				var projectId = $("#ContacProjectId").val();  
				showImageProject(projectId);
			}); 
			EDIT.loadExternalAssistants(teamId, cliente);
			EDIT.loadAssistantsCommitments(teamId, cliente); 
		});
	},

	//CARGAR LOS PROYECTOS DEL CLIENTE
	changeProject: function(){
		$('body').on('change', '#ContacProjectId', function() {
			showImageProject($(this).val());		 
		});
	},

	//Cargar los asistentes y colaboradores para el acta
	loadExternalAssistants:function(teamId, clientId){
		$.post(GLOBAL_DATA.app_root+'user_teams/find_assistants',{teamId:teamId, clientId:clientId},function (resultado){
			$('#load_assitants_team').html(resultado);
			$('#ContacExternos').select2({
				language: 'es'
			});
			EDIT.loadUsersApproval(); 
		});
	},

	//CAMBIAR EL PROYECTO CUANDO SE SELECCIONE UN CLIENTE
	changeDataProject: function(){ 
		var cliente = $("#ContacClientId").val(); 
		$.post(GLOBAL_DATA.app_root+'projects/find_projects',{cliente:cliente},function (resultado){
			$('#load_projects_client').html(resultado);
			//$('#ContacProjectId').select2({
			//	language: 'es'
			//}); 
			$('#ContacProjectId').selectpicker()
			var projectId = $("#ContacProjectId").val(); 
			// showImageClient(cliente);
		}); 
	},

	//LISTA DE USUARIOS PARA LOS COMPROMISOS
	loadAssistantsCommitments:function(teamId, clientId){
		$.post(GLOBAL_DATA.app_root+'user_teams/load_users_commitments',{teamId:teamId, clientId:clientId},function (resultado){
			$('#load_content_users_commitment').html(resultado);
			$('.selectclient').select2({
				language: 'es'
			});
			$("#CommitmentsNUEVOAsistente").selectpicker();
		});
	},

	//LISTA DE USUARIOS PARA SOLICITAR APROBACIÓN DEL ACTA CUANDO CAMBIA EL CLIENTE
	loadUsersApproval:function(execute = null){
		var funcionarios    = document.getElementById('ContacFuncionarios');
        var externos        = document.getElementById('ContacExternos');
        var approvals       = document.getElementById('ContacApprovalusers');
	    var usersIds    	= [];
	    var employeeIds 	= []; 
     	var usersApprovals  = [];
     	if (funcionarios !== null) {
		    for(i = 0; i < funcionarios.length; i++){
		        if(funcionarios.options[i].selected){
		            usersIds.push(funcionarios.options[i].value);
		        }
		    }
		}
	    if (externos !== null) {
		    for(i = 0; i < externos.length; i++){
		        if(externos.options[i].selected){
		            employeeIds.push(externos.options[i].value);
		        }
		    }
		}
		if (approvals !== null) { 
		    for(i = 0; i < approvals.length; i++){
		        if(approvals.options[i].selected){
		            usersApprovals.push(approvals.options[i].value);
		        }
		    }
	    }   
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/contacs/list_users_to_approval/',
          data: {usersIds:usersIds, employeeIds:employeeIds, usersApprovals:usersApprovals},          
          dataType: 'html',
          	success:  function(data) {  
          		$("#load_content_user_approval").html(data);
          		$('#ContacApprovalusers').select2({
					language: 'es'
				});
				if(execute == true){
					var contacId = $("#ContacId").val(); 
					EDIT.loadApprovalUsers(contacId);
				} 
          	}          
 		}); 
	},

	changeAsistentes: function(){
		var cliente = $("#ContacClientId").val();
		$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/find_external_assistants',{cliente:cliente},function (resultado){
			$('.selectExternoDatos').html(resultado);
			$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/find_asistentes_commitments',{cliente:cliente},function (resultado){
				$('.selectclient').val('');
				$('.selectclient').html(resultado);
				$('.select2_all').select2({
					language: 'es'
				});
				$('.selectExternos').show();
				$('#content-assistents-add').show();
			});
			if(GLOBAL_DATA.action == "add"){
				var employees  = [];
				if(GLOBAL_DATA.CONTAC_ASSISTANTS_MEETING != null){
				  	$.each(GLOBAL_DATA.CONTAC_ASSISTANTS_MEETING, function(index, value){
			  		 	employees[index] = value;
				  	});
				  	$("#externos").val(employees);
				  	$.ajax({ 
			          type: "POST",
			          url: GLOBAL_DATA.APP_BASE+'/contacs/list_users_to_approval/',
			          data: {usersIds:GLOBAL_DATA.CONTAC_USERS_MEETING, employeeIds:GLOBAL_DATA.CONTAC_ASSISTANTS_MEETING},          
			          dataType: 'html',
			          	success:  function(data) {  
			          		$("#ContacApprovalusers").html(data);
			          	}          
			 		}); 
			  	} 
			  	var employeesChoosed = [];
			  	$('#ContacExternosSelected option').each(function(){
				 	employeesChoosed.push($(this).text());				 	
				});  
				$("#externos").val(employeesChoosed); 
			}
		});		
	},

	deleteImagen:function(){
		$("body").on('click', '.btn_eliminarImagen', function(event) {
	    	event.preventDefault();
	    	var id = $(this).attr("data-uid");
	    	var name = $(this).attr("data-name");

	    	var notificacionContac = firebase.database().ref(GLOBAL_DATA.firebaseContact + GLOBAL_DATA.firebaseCodigo);
	    	notificacionContac.on('value', function(snapshot) {
			    valores = snapshot.val();
			});

			swal({
			    title: copys_js.ten_cuidado,
			    text: copys_js.delete_image,
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: "#DD6B55",
			    confirmButtonText: copys_js.btn_confirm,
			    cancelButtonText: copys_js.btn_cancel,
			},
			function(){
				EDIT.campoDescripcionContac();
				$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/deleteImagenContac',{id:id,name:name,valores:valores},function (resultado){
					EDIT.cargarImagenesDelete();
				});
			});
	    });
	},

	deleteDocumeto:function(){
		$("body").on('click', '.btn_eliminarDocumento', function(event) {
	    	event.preventDefault();
	    	var id = $(this).attr("data-uid");
	    	var name = $(this).attr("data-name");

	    	var notificacionContac = firebase.database().ref(GLOBAL_DATA.firebaseContact + GLOBAL_DATA.firebaseCodigo);
	    	notificacionContac.on('value', function(snapshot) {
			    valores = snapshot.val();
			});

	    	swal({
			    title: copys_js.ten_cuidado,
			    text: copys_js.delete_document,
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: "#DD6B55",
			    confirmButtonText: copys_js.btn_confirm,
			    cancelButtonText: copys_js.btn_cancel,
			},
			function(){
				EDIT.campoDescripcionContac();
				$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/deleteDocumetoContac',{id:id,name:name,valores:valores},function (resultado){
					EDIT.cargarDocumentosDelete();
				});
			});
	    });
	},

	dateTimeContact: function(){
		$('#datetimepickerOn>input').daterangepicker({
			"singleDatePicker": true,
    		"timePicker": true,
    		"timePicker24Hour": true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});
		$('#datetimepickerOff>input').daterangepicker({
			"singleDatePicker": true,
    		"timePicker": true,
    		"timePicker24Hour": true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});
	},

	dateContac: function(){
		moment.locale('es');
		$('.date_all_contac>input').daterangepicker({
			singleDatePicker: true,
			locale: {
				format: 'YYYY-MM-DD'
			}
		});

		$('#ContacLimitDate').daterangepicker({
			singleDatePicker: true,
			locale: {
				format: 'YYYY-MM-DD'
			}
		});
	},

	selectedDatePicker: function(){
		$('.addcompromiso').on('click',function() {
			var descriptionCommitment = $("#CommitmentsNUEVODescription").val();
			EDIT.stripFieldsTags(descriptionCommitment, function(result){
	 			if(result.response == false){
	 				swal({type:"error", title:copys_js.error, text:copys_js.max_word_lenght});
	 				return false;
	 			}
				var dateCommitment = $("#CommitmentsNUEVOFecha").val();
				if (descriptionCommitment == null || descriptionCommitment.length == 0 || /^\s+$/.test(descriptionCommitment)) {
					swal({type:"error", title:copys_js.error, text:copys_js.description_commitment});
				
				} else if (dateCommitment == null || dateCommitment == "") {
					swal({type:"error", title:copys_js.error, text:copys_js.date_commitment});

				}else if ($('#CommitmentsNUEVOAsistente').val() == "") {
					swal({type:"error", title:copys_js.error, text:copys_js.please_select_responsable});
				} else {

					if (GLOBAL_DATA.action == 'edit') {
						EDIT.campoDescripcionContac();
					} 
					var descripcionNuevo = $('#CommitmentsNUEVODescription').val();
					var fechaNuevo       = $('#CommitmentsNUEVOFecha').val();
					var usuarioNuevo     = $('#CommitmentsNUEVOAsistente').val();
					if(GLOBAL_DATA.action == 'edit'){
						id = saveCommitment(descripcionNuevo, fechaNuevo, usuarioNuevo);
						COUNT = id;
					} else {
						COUNT = $("#commitments tr").length;
						COUNT = COUNT + 1; 
					}

					$('.selectpicker_comimment').selectpicker('destroy');

					var stringClone = $('tr#nuevoCompromiso').html();
					stringClone     = stringClone.replace(/NUEVO/g, COUNT);
					var clon        = $(stringClone);
					var date = new Date();
					date.setDate(date.getDate());	

					$('.hijosF').append($("<tr id='hijos"+COUNT+"' class='tr-height100 hijo_"+COUNT+" commitment-row'></tr>"));
					$('.hijo_'+COUNT).html(clon).css('opacity', 0).slideDown(400).animate({ opacity: 1 },{ queue: false, duration: 'slow' });
					$('.date').removeClass('booststrap-datetimepicker-widget');
					$('.hijo_'+COUNT+' .select2-container').remove();
					$('.hijo_'+COUNT+' .selectclient').removeClass("select2-hidden-accessible");
					$('.hijo_'+COUNT+' .addcompromiso').remove();
					EDIT.dateContac();

					//$('.hijo_'+COUNT+' .selectclient').select2();	 
					$('#Commitments'+COUNT+'Description').val(descripcionNuevo);
					if(GLOBAL_DATA.action == 'edit'){
						$('#Commitments'+COUNT+'Id').val(id);
					}

					$('#Commitments'+COUNT+'Asistente').val(usuarioNuevo);
					$('#Commitments'+COUNT+'Fecha').val(fechaNuevo);
					//$('#Commitments'+COUNT+'Asistente').selectpicker();

					$('#CommitmentsNUEVODescription').val('');
					$('#CommitmentsNUEVOFecha').val('');
					$("body").find('button[data-id="'+COUNT+'"]').removeClass('d-none');
					$('.selectpicker_comimment').selectpicker();
					if(GLOBAL_DATA.action == 'edit'){
						EDIT.guardarFirebaseDatos();
					}
				}
	 		});  
		});
	},

	removeCommitment: function(){
		//EDITAR ACTA
		$("body").on('click', '.btn-remove-commitments', function(event) {
			var id 		     = $(this).data('id');
			var commitmentId = $(this).attr("data-id");
			var assistanId   = $("#Commitments"+id+"Asistente").val();
			var date         = $("#Commitments"+id+"Fecha").val();
			var description  = $("#Commitments"+id+"Description").val(); 
			$.ajax({
				url: GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/saveLogDeleteCommitmentBorrador',
				data:{assistanId:assistanId, commitmentId:commitmentId, date:date, description:description},
				method:'POST',
				beforeSend: function(e){
                	$(".btn-remove-commitments").attr("disabled", true);
            	},
				success:function(resultado){
					$(".btn-remove-commitments").attr("disabled", false);
				} 
			});  	 
			EDIT.campoDescripcionContac();
			$('#hijos'+id).remove();
			$('#hijos'+commitmentId).remove();
			EDIT.guardarFirebaseDatos();
		}); 
		$("body").on('click', '.btn-remove-nuevo-compromiso', function(event) {
			var id = $(this).data('id');
			if(GLOBAL_DATA.action == "add"){
			}  
				$('#hijos'+id).remove(); 
			if (id != 'NUEVO') { 
				$('#nuevoCompromiso'+id).remove(); 
			} else {

				$('#CommitmentsNUEVODescription').val('');
				$('#CommitmentsNUEVOFecha').val('');
			}
		});
		$("body").on('click', '.btn-visible-remove-commitment', function(event) {
			
		});
	},

	unfinishedMeeting: function(){
		$('#ContacReunion').change(function() {
			if ($(this).is(':checked')) { 
				$('.filled').text('');
				$('.parsley-error').removeClass('parsley-error');
				$('.parsley-success').removeClass('parsley-success');
				$('#btnGuardar').hide(); 
				$('#btnBorrador').show();
				$("#content-approval-solicitud").show(); 
			} else {
				$('#btnGuardar').show();
				$('#datetimepickerOff').show();
				$('.lbl_endDate').show();
				$('#btnBorrador').hide();
				$("#content-approval-solicitud").hide(); 
				$("#content-approval").hide();
				$("#requestApproval").attr('checked', false);
			}
		});
	},

	cargarImagenes:function() {
		$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/upload_images',{},function (resultado){
			$("#imageDropzone").html(resultado);			
    		$("body").find('a.showImages').magnificPopup({type:'image'}); 
		});
	},

	cargarDocumentos:function() {
		$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/upload_documents',{},function (resultado){
			$("#documentDropzone").html(resultado);
    		$("body").find('a.showImages').magnificPopup({type:'image'});
		});
	},

	cargarImagenesDelete:function() {
		$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/upload_images_delete',{},function (resultado){
			$("#imageDropzone").html(resultado);
		}); 
	},

	cargarDocumentosDelete:function() {
		$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/upload_documents_delete',{},function (resultado){
			$("#documentDropzone").html(resultado);
		});
	},

	activateAutoGuardado: function(){ 
		if (initContadorGuardado == 0) {
			EDIT.autoGuardado();
			initContadorGuardado = 1;
		} else { 
			CKEDITOR.instances['ContacDescription'].destroy(true);
			return false;
		} 
	},

	autoGuardadoAdd: function(){
		var description = CKEDITOR.instances['ContacDescription'].getData();  
		var data = new FormData($("#ContacAddForm")[0]);
		data.append('description', description); 
		EDIT.stripFieldsTags(description, function(result){
 			if(result.response == false){
 				swal({type:"error", title:copys_js.error, text:copys_js.max_word_lenght});
 				return false;
 			} 
			if($("#requestApproval").is(':checked')){
			 	var usersApproved = $("#ContacApprovalusers").val();
			 	var dateLimit     = $("#ContacLimitDate").val();
			 	var endDate       = $("#ContacEndDate").val();
			 	var startDate     = $("#ContacStartDate").val();
		 		var date 		  = endDate.split(' ')[0];
		 		var d 	          = new Date();
				var month         = d.getMonth()+1;
				var day           = d.getDate();
				var dateToday     = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
             	var now           = new Date(Date.now());        	 
			    var dateTime      = dateToday + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
			    GLOBAL_DATA.DATE_TIME = dateTime;
		 		if(usersApproved == null){
			 		swal({type:"error", title:copys_js.error, text:copys_js.minimum_one_user});

			 	} else if (dateLimit == null || dateLimit == "") {
			 		swal({type:"error", title:copys_js.error, text:copys_js.define_the_date});	

			 	} else if(dateLimit <= date){
			 	 	swal({type:"error", title:copys_js.error, text:copys_js.date_not_equal});

			 	} else if(startDate.trim() === "" || endDate.trim() === ""){
	 				swal({type:"error", title:copys_js.error, text:copys_js.require_dates});
	 				return false;
	 			}else{
	 				// if(startDate === endDate){
	 				// 	swal({type:"error", title:copys_js.error, text:copys_js.dates_contac_equal});
	 				// 	return false;
		 			// }
		 			dateIni = startDate.split(' ')[0];
		 			if(dateIni > GLOBAL_DATA.DATE_TIME) {
	 					swal({type:"error", title:copys_js.error, text:copys_js.date_today_more});
	 					return false;
		 			} 
		 			startDate = new Date(startDate);
	 				endDate   = new Date(endDate);
		 			if (endDate < startDate){
		 				swal({type:"error", title:copys_js.error, text:copys_js.end_date_less_start_date});
		 			} else {
		 				$.ajax({
							url: GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/autoguardar',
							data: data,
							contentType:false,
							cache: false,
							processData:false,
							method:'POST', 
							beforeSend: function(e){
							    $( "#btnGuardar" ).prop( "disabled", true ); 
								$( "#btnAutoGuardado" ).prop( "disabled", true ); 
								$( "#btnBorrador" ).prop( "disabled", true ); 
							},
							success:function(resultado){ 
							  	EDIT.redirectToEdit(resultado);
							}, 
							error:function(resultado){
								$( "#btnGuardar" ).prop( "disabled", false ); 
								$( "#btnAutoGuardado" ).prop( "disabled", false ); 
								$( "#btnBorrador" ).prop( "disabled", false ); 
								Utilities.showMessaje(copys_js.error_save,"error")
							}
						});
		 			}
	 			} 
			} else {
				var d 	          = new Date();
				var month         = d.getMonth()+1;
				var day           = d.getDate();
				var dateToday     = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
             	var now           = new Date(Date.now());        	 
			    var dateTime      = dateToday + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
			    GLOBAL_DATA.DATE_TIME = dateTime;			    
				var startDate = $("#ContacStartDate").val();
	 			var endDate   = $("#ContacEndDate").val();
	 			console.log(GLOBAL_DATA.DATE_TIME);
	 			if(startDate.trim() === "" || endDate.trim() === ""){
	 				swal({type:"error", title:copys_js.error, text:copys_js.require_dates});
	 				return false;
	 			}else{
	 				// if(startDate === endDate){
	 				// 	swal({type:"error", title:copys_js.error, text:copys_js.dates_contac_equal});
	 				// 	return false;
		 			// }
		 			dateIni = startDate.split(' ')[0]; 
		 			if(dateIni > GLOBAL_DATA.DATE_TIME) {
	 					swal({type:"error", title:copys_js.error, text:copys_js.date_today_more});
	 					return false;
		 			} 
		 			startDate = new Date(startDate);
	 				endDate   = new Date(endDate);
		 			if (endDate < startDate){
		 				swal({type:"error", title:copys_js.error, text:copys_js.end_date_less_start_date});
		 			} else {
		 				$.ajax({
							url: GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/autoguardar',
							data: data,
							contentType:false,
							cache: false,
							processData:false,
							method:'POST', 
							beforeSend: function(e){
							    $( "#btnGuardar" ).prop( "disabled", true ); 
								$( "#btnAutoGuardado" ).prop( "disabled", true ); 
								$( "#btnBorrador" ).prop( "disabled", true ); 
							},
							success:function(resultado){ 
								EDIT.redirectToEdit(resultado);
							}, 
							error:function(resultado){
								$( "#btnGuardar" ).prop( "disabled", false ); 
								$( "#btnAutoGuardado" ).prop( "disabled", false ); 
								$( "#btnBorrador" ).prop( "disabled", false ); 
								Utilities.showMessaje(copys_js.error_save,"error")
							}
						});
		 			}
	 			}
			} 
	 	});		
	},

	redirectToEdit:function(resultado) {
		if(isNaN(resultado)) {
			try {
				var response = $.parseJSON(resultado); 
				var url = GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/edit/'+response.id;
				if(response.password && response.password!="") {
					url +='/'+response.password
				}
			}catch(err) {
			  var url = GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/edit/'+resultado;
			}
		} else {		
			var url = GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/edit/'+resultado;
		}
		location.href = url;	
	},

	callFunctionAutoSave: function(data){
		//$("#start_contac").click();
		$.ajax({
			url: GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/autoguardar',
			data: data,
			contentType:false,
			cache: false,
			processData:false,
			method:'POST', 
			beforeSend: function(e){
				$( "#btnGuardar" ).prop( "disabled", true ); 
				$( "#btnAutoGuardado" ).prop( "disabled", true ); 
				$( "#btnBorrador" ).prop( "disabled", true ); 
			},
			success:function(resultado){ 
			  	EDIT.redirectToEdit(resultado);
			}, 
			error:function(resultado){
				$( "#btnGuardar" ).prop( "disabled", false ); 
				$( "#btnAutoGuardado" ).prop( "disabled", false ); 
				$( "#btnBorrador" ).prop( "disabled", false ); 
				Utilities.showMessaje(copys_js.error_save,"error")
			}
		});  
	},

	autoGuardado: function(){ 
		// var count   = 10;
	    INTERVAL_AUTO_GUARDADO = setInterval(function() {
	        // count--; 
	        // if(count == 0) {
	            var description = CKEDITOR.instances['ContacDescription'].getData(); 
				var data = new FormData($("#ContacEditForm")[0]);
				data.append('description', description); 
				$.ajax({
					url: GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/autoguardar',
					data:data,
					contentType:false,
					cache: false,
					processData:false,
					method:'POST',
					success:function(resultado){
				  		EDIT.alertCamposAjaxSucces(copys_js.save_data);				 
					},
					error:function(resultado){
						Utilities.showMessaje(copys_js.error_save,"error")
					}
				});  
	            // count = 10;
	        // }
	    }, 10000); 
	},

	guardarFirebaseDatos: function(){
		var description = CKEDITOR.instances['ContacDescription'].getData();
		if (GLOBAL_DATA.action == 'add') {
			var data = new FormData($("#ContacAddForm")[0]);
			data.append('description', description);  
		} else {
			var data = new FormData($("#ContacEditForm")[0]);
			data.append('description', description); 
		}
		$.ajax({
			url: GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/autoguardar',
			data:data,
			contentType:false,
			cache: false,
			processData:false,
			method:'POST',
			complete:function(resultado){

			},
			error:function(resultado){
			}
		});
	},

	getRealCommitments:function(compromisos) {
		var comprommisosReales = [];
		var primerCompromiso = {};
		var otrosCompromisos = [];
		for (i in compromisos) {
			if(compromisos[i]) {
				comprommisosReales.push(compromisos[i]);
			}
		}
		for (j in comprommisosReales) {
			if(j == 0) {
				primerCompromiso = comprommisosReales[j];
			} else {
				otrosCompromisos.push(comprommisosReales[j]);
			}
		}
		return {primerCompromiso:primerCompromiso, otrosCompromisos:otrosCompromisos};
	},

	showPrimerCompromiso:function(primerCompromiso) {
		$('#CommitmentsNUEVODescription').val(primerCompromiso.description);
		$('#CommitmentsNUEVOFecha').val(primerCompromiso.fecha);
		$('#CommitmentsNUEVOAsistente').val(primerCompromiso.asistente);
		$('#CommitmentsNUEVOId').val(primerCompromiso.id);
	},

	uploadCommitments:function(compromisos){ 
		$('.hijosF').empty();
		for (i in compromisos) {  
			var compromisoActual = compromisos[i];

			let count = 0;
			for (var c in compromisoActual) {
			    count = count + 1;
			}

			if(i != "undefined" && count > 2 && compromisos[i].id != "") {
				$(".addcompromiso").attr({"href" : "#hijos"+compromisos[i].id});
				var stringClone = $('tr#divclone').html();
				stringClone     = stringClone.replace(/GROUPID/g, compromisos[i].id);
				var clon        = $(stringClone);
				var date        = new Date();
				date.setDate(date.getDate());
				$('.hijosF').append($("<tr id='hijos"+compromisos[i].id+"' class='tr-height100 hijo_"+compromisos[i].id+"'></tr>"));
				$('.hijo_'+compromisos[i].id).html(clon).css('opacity', 0).slideDown(400).animate({ opacity: 1 },{ queue: false, duration: 'slow' });
				$('.date').removeClass('booststrap-datetimepicker-widget');
				$('.hijo_'+compromisos[i].id+' .select2-container').remove();
				$('.hijo_'+compromisos[i].id+' .selectclient').removeClass("select2-hidden-accessible");
				$('.hijo_'+compromisos[i].id+' .addcompromiso').remove();
				EDIT.dateContac();
				$('#Commitments'+compromisos[i].id+'Id').val(compromisos[i].id);
				$('#Commitments'+compromisos[i].id+'Fecha').val(compromisos[i].fecha);
				$('#Commitments'+compromisos[i].id+'Asistente').val(compromisos[i].asistente);
				$('#Commitments'+compromisos[i].id+'Description').val(compromisos[i].description);  
				
				$('.hijo_'+compromisos[i].id+' .selectclient').selectpicker();
				$('#Commitments'+compromisos[i].id+'Asistente').selectpicker();
			}
		} 


	},

	updateCompromisoNuevo:function() {

	},

	listCommitmentsView:function(commitments){
		$.ajax({ 
          	type: "POST",
          	url: GLOBAL_DATA.APP_BASE+'/contacs/show_commitments/', 
          	data: {commitments:commitments},          
          	dataType: 'html',
        	success:  function(data) { 
        		$('#ShowCommitments').remove();      		 
        		$("#content-table").append(data);
        	}          
        }); 
	},

	stripFieldsTags:function(description,response){
		$.ajax({ 
          	type: "POST",
          	url: GLOBAL_DATA.APP_BASE+'/contacs/strip_description/', 
          	data: {description:description},          
          	dataType: 'json',
        	success:  function(data) {
        		response(data);
        	}          
        }); 
	},

	btnAutoGuardadoAdd: function (){
		console.log("ESTUVO AQUI CARGANDO");
		$('#btnAutoGuardado').click(function(event) {
			if ($("#ContacTeamId").val() == ""){
				return swal({type:"error", title:copys_js.error, text:copys_js.please_select_company});
			}
			if ($("#ContacProteger").is(':checked')){
				if($("#ContacPassword").val() == "") {
					return swal({type:"error", title:copys_js.error, text:copys_js.please_enter_contact_password});
				}
			}
			EDIT.autoGuardadoAdd();
		});
	},

	btnGuardar:function() {
		$('#btnGuardar').click(function(event) {
			if($("#requestApproval").is(':checked')){
				var d 	          = new Date();
				var month         = d.getMonth()+1;
				var day           = d.getDate();
				var dateToday     = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
		     	var now           = new Date(Date.now());        	 
			    var dateTime      = dateToday + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
			    GLOBAL_DATA.DATE_TIME = dateTime;			    
			 	var usersApproved = $("#ContacApprovalusers").val();
			 	var dateLimit     = $("#ContacLimitDate").val();
			 	var endDate       = $("#ContacEndDate").val();
			 	var startDate     = $("#ContacStartDate").val();
		 		var date 		  = endDate.split(' ')[0];
		 		if(usersApproved == null){
			 		swal({type:"error", title:copys_js.error, text:copys_js.minimum_one_user});
			 	
			 	} else if (dateLimit == null || dateLimit == "") {
			 		swal({type:"error", title:copys_js.error, text:copys_js.define_the_date});	

			 	} else if(dateLimit <= date){
			 	 	swal({type:"error", title:copys_js.error, text:copys_js.date_not_equal});

			 	} else if(startDate.trim() === "" || endDate.trim() === ""){
						swal({type:"error", title:copys_js.error, text:copys_js.require_dates});
						return false;
				}else{
					if(startDate === endDate){
						swal({type:"error", title:copys_js.error, text:copys_js.dates_contac_equal});
						return false;
		 			}
		 			dateIni = startDate.split(' ')[0]; 
		 			if(dateIni > GLOBAL_DATA.DATE_TIME) {
						swal({type:"error", title:copys_js.error, text:copys_js.date_today_more});
						return false;
					} 
		 			startDate = new Date(startDate);
					endDate   = new Date(endDate);
		 			if (endDate < startDate){
		 				swal({type:"error", title:copys_js.error, text:copys_js.end_date_less_start_date});
		 			} else {
		 				EDIT.clearIntervalAutoGuardado();
		 				EDIT.submitContactForm();
		 			}
				} 
			} else { 
				EDIT.clearIntervalAutoGuardado();
				EDIT.submitContactForm();
			}
		});
	},

	clearIntervalAutoGuardado:function() {
		try {
			if(typeof INTERVAL_AUTO_GUARDADO != "undefined") {
				clearTimeout(INTERVAL_AUTO_GUARDADO);
				if(AJAX_GUARDAR_BORRADOR) {
					AJAX_GUARDAR_BORRADOR.abort();
				}
			}
		}catch(err) {}
	},

	submitContactForm:function() {
		if($("#ContacAddForm") && $("#ContacAddForm").length && $("#ContacAddForm").length > 0) {
			$("#ContacAddForm").submit();

		} else {

			$("#ContacEditForm").submit();
		}
	},

 	//guardar borrador
	btnBorrador: function(){
		//$('#btnBorrador').click(function(event) {
		$('#btnSaveBorrador').click(function(event) {
		 	var description = CKEDITOR.instances['ContacDescription'].getData(); 	
	 		EDIT.stripFieldsTags(description, function(result){ 
	 			if(result.response == false){
	 				swal({type:"error", title:copys_js.error, text:copys_js.max_word_lenght});
	 				return false;
	 			} 
		 		if (GLOBAL_DATA.action == 'add') {				 
					var data = new FormData($("#ContacAddForm")[0]);
					data.append('description', description); 
				} else { 
					var data = new FormData($("#ContacEditForm")[0]);
					data.append('description', description); 
				}
				if($("#requestApproval").is(':checked')){
					var d 	          = new Date();
					var month         = d.getMonth()+1;
					var day           = d.getDate();
					var dateToday     = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
	             	var now           = new Date(Date.now());        	 
				    var dateTime      = dateToday + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
				    GLOBAL_DATA.DATE_TIME = dateTime;			    
				 	var usersApproved = $("#ContacApprovalusers").val();
				 	var dateLimit     = $("#ContacLimitDate").val();
				 	var endDate       = $("#ContacEndDate").val();
				 	var startDate     = $("#ContacStartDate").val();
			 		var date 		  = endDate.split(' ')[0];
			 		if(usersApproved == null){
				 		swal({type:"error", title:copys_js.error, text:copys_js.minimum_one_user});
				 	} else if (dateLimit == null || dateLimit == "") {
				 		swal({type:"error", title:copys_js.error, text:copys_js.define_the_date});	

				 	} else if(dateLimit <= date){
				 	 	swal({type:"error", title:copys_js.error, text:copys_js.date_not_equal});
				 	} else if(startDate.trim() === "" || endDate.trim() === ""){
		 				swal({type:"error", title:copys_js.error, text:copys_js.require_dates});
		 				return false;
		 			}else{
		 				if(startDate === endDate){
		 					swal({type:"error", title:copys_js.error, text:copys_js.dates_contac_equal});
		 					return false;
			 			}
			 			dateIni = startDate.split(' ')[0]; 
			 			if(dateIni > GLOBAL_DATA.DATE_TIME) {
	 						swal({type:"error", title:copys_js.error, text:copys_js.date_today_more});
	 						return false;
		 				} 
			 			startDate = new Date(startDate);
		 				endDate   = new Date(endDate);
			 			if (endDate < startDate){
			 				swal({type:"error", title:copys_js.error, text:copys_js.end_date_less_start_date});
			 			} else {
			 				EDIT.callFunctionBorrador(data);
			 			}
		 			} 
				} else { 
					var d 	          = new Date();
					var month         = d.getMonth()+1;
					var day           = d.getDate();
					var dateToday     = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
	             	var now           = new Date(Date.now());        	 
				    var dateTime      = dateToday + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
				    GLOBAL_DATA.DATE_TIME = dateTime;
					var startDate = $("#ContacStartDate").val();
		 			var endDate   = $("#ContacEndDate").val();
		 			if(startDate.trim() === "" || endDate.trim() === ""){
		 				swal({type:"error", title:copys_js.error, text:copys_js.require_dates});
		 				return false;
		 			}else{
		 				if(startDate === endDate){
		 					swal({type:"error", title:copys_js.error, text:copys_js.dates_contac_equal});
		 					return false;
			 			}
			 			dateIni = startDate.split(' ')[0]; 
			 			if(dateIni > GLOBAL_DATA.DATE_TIME) {
	 						swal({type:"error", title:copys_js.error, text:copys_js.date_today_more});
	 						return false;
		 				} 
			 			startDate = new Date(startDate);
		 				endDate   = new Date(endDate);
			 			if (endDate < startDate){
			 				swal({type:"error", title:copys_js.error, text:copys_js.end_date_less_start_date});
			 			} else {
			 				EDIT.callFunctionBorrador(data);
			 			}
		 			}
				}
	 		}); 
		});
	},

	callFunctionBorrador(data){
		//$("#start_contac").click();
		if(AJAX_GUARDAR_BORRADOR) {
			AJAX_GUARDAR_BORRADOR.abort();
		}
		AJAX_GUARDAR_BORRADOR = $.ajax({
		 	url: GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/borrador',
			data: data,
			contentType:false,
			cache: false,
			processData:false,
		 	method:'POST',
		 	beforeSend: function(e){
		 		$( "#btnGuardar" ).prop( "disabled", true ); 
				$( "#btnAutoGuardado" ).prop( "disabled", true ); 
				$( "#btnBorrador" ).prop( "disabled", true ); 
		 	},
		 	success:function(resultado){ 
	 			location.href = GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/index';
	 		},
	 		complete:function() {
	 			AJAX_GUARDAR_BORRADOR = null;
	 		}
		});  
	},

	campoDescripcionContac: function (){
		if (GLOBAL_DATA.action == 'edit') {
			var description = CKEDITOR.instances['ContacDescription'].getData(); 
			$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/campoDescripcionContacSave',{description:description},function (resultado){});
		} else {
			EDIT.autoGuardadoAdd();
		}
	},

	dropzoneImagenes: function() {
		Dropzone.options.dropzoneImage = {
			url:GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/saveImage', // Cuando existe este parametro, la libreria hacer caso omiso al action
			maxFilesize: 60,
			timeout: 86400000,
			addRemoveLinks: true,
			autoProcessQueue: true, //Permite que no se envíe el formulario a menos que haya hecho clic en el botón
			maxFiles: 60,
			parallelUploads: 20,   
			dictDefaultMessage: "",
			dictResponseError: 'Server not Configured',
			acceptedFiles: ".png,.jpg,.jpeg",
			init:function(){
				var submitButton   = document.querySelector(".btnArchivos");
				var buttonBorrador = document.querySelector(".btnArchivosBorrador");
				var addButton      = document.querySelector("#btnGuardar"); 
				var self = this; 
				// config
				self.options.addRemoveLinks = true;
				self.options.dictRemoveFile = copys_js.delete_files; 
				//New file added
				self.on("addedfile", function (file) {
					var extencion = file.type.split('/');  
				    loadMime(file, function(mime) { 
				    	if(mime == false){
		    				swal({type:"error", title:copys_js.error, text:copys_js.allow_only_images}); 
							// self.removeFile(file);
							file.previewElement.remove(); 
							return false;
				    	} 
				    });
				});

				submitButton.addEventListener("click", function() {
			      self.processQueue(); // Tell Dropzone to process all queued files.
			    });

				if(buttonBorrador != null) {
				    buttonBorrador.addEventListener("click", function() { 
				      self.processQueue(); // Tell Dropzone to process all queued files.
				    });
				}

			    if(addButton != null) {
				    addButton.addEventListener("click", function() {
				      self.processQueue(); // Tell Dropzone to process all queued files.
				    });			    	
			    } 
			    self.on("success", function(file, response) {
                  var obj = jQuery.parseJSON(response);
                  file._removeLink.id = obj;
                  var buttonRemoved   = file._removeLink.id;
                  if(GLOBAL_DATA.action == "edit"){
          		  	EDIT.cargarImagenesDelete(); 
                  }
                })
				// Send file starts
				self.on("sending", function (file) {
					$( "#btnGuardar" ).prop( "disabled", true ); 
					$( "#btnAutoGuardado" ).prop( "disabled", true ); 
					$( "#btnBorrador" ).prop( "disabled", true ); 
					$('.meter').show();
					try {
						xhr.ontimeout = (() => {
							Utilities.showMessaje(copys_js.error_internet_saving_file,'error');
						});
					}catch(err) {}
				});

				self.on("totaluploadprogress", function (progress) {
					$('.roller').width(progress + '%');
				});

				self.on("complete", function (progress) {
					$('.meter').delay(999).slideUp(999);
					$('.dz-error').remove();
					$('.dz-success').remove();
				 	// if(addButton == null) {
						// $('.dz-success').remove();
				 	// } 
					if(self.getUploadingFiles().length == 0){ 
				 		$( "#btnGuardar" ).prop("disabled", false); 
				 		$( "#btnAutoGuardado" ).prop("disabled", false); 
				 		$( "#btnBorrador" ).prop("disabled", false); 
					}
				});

				self.on("removedfile", function (file) {
					var id = file._removeLink.id;
					$.ajax({ 
		              type: "POST",
		              url: GLOBAL_DATA.APP_BASE+'/contacs/delete_image_dropzone/',
		              data: {id:id},          
		              dataType: 'json',
		                success:  function(data) {}          
		            }); 
				});
			}
		};
	},

	dropzoneDocument: function() {
		Dropzone.options.dropzoneDocument = {
			url:GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/saveDocument', // Cuando existe este parametro, la libreria hacer caso omiso al action
			maxFiles: 60,
			timeout: 86400000,
			parallelUploads: 20,
			addRemoveLinks: true,
			autoProcessQueue: true, //Permite que no se envíe el formulario a menos que haya hecho clic en el botón
			maxFilesize: 60,
			dictDefaultMessage: "",
			dictResponseError: 'Server not Configured',
			acceptedFiles: ".pdf,.doc,.docx,.PDF",
			init:function(){
				var submitButton   = document.querySelector(".btnArchivos");
				var addButton      = document.querySelector("#btnGuardar");
				var buttonBorrador = document.querySelector(".btnArchivosBorrador");
				var self = this;
				// config
				self.options.addRemoveLinks = true;
				self.options.dictRemoveFile = copys_js.delete_files;

				//New file added
				self.on("addedfile", function (file) {
					var extencion = file.name.split('.');

					if (extencion != '') {
						if(!EDIT.isDocument(file.name)){ 
							swal({type:"error", title:copys_js.error, text:copys_js.allow_only_document});
							self.removeFile(file);
							return false;
						}
					}else{ 
						swal({type:"error", title:copys_js.error, text:copys_js.allow_only_document});
						return false;
					} 
				});

				submitButton.addEventListener("click", function() {
			      self.processQueue(); // Tell Dropzone to process all queued files.
			    });

			    if(buttonBorrador != null) {
				    buttonBorrador.addEventListener("click", function() {
				      self.processQueue(); // Tell Dropzone to process all queued files.
				    });
				}

			 	if(addButton != null) {
	 				addButton.addEventListener("click", function() {
				      self.processQueue(); // Tell Dropzone to process all queued files.
				    });
	 			}

	 			self.on("success", function(file, response) {
                  var obj = jQuery.parseJSON(response);
                  file._removeLink.id = obj;
                  var buttonRemoved   = file._removeLink.id;
                  if(GLOBAL_DATA.action == "edit"){
          		  	EDIT.cargarDocumentosDelete(); 
                  }
                });

				// Send file starts
				self.on("sending", function (file, xhr, formData) {
					$( "#btnGuardar" ).prop( "disabled", true ); 
					$( "#btnAutoGuardado" ).prop( "disabled", true ); 
					$( "#btnBorrador" ).prop( "disabled", true ); 
					$('.meter').show();
					try {
						xhr.ontimeout = (() => {
							Utilities.showMessaje(copys_js.error_internet_saving_file,'error');
						});
					}catch(err) {}
				});

				self.on("totaluploadprogress", function (progress) {
					$('.roller').width(progress + '%');
				});

				self.on("complete", function (progress) {
					$('.meter').delay(999).slideUp(999);
					$('.dz-error').remove();
					$('.dz-success').remove();
					// if(addButton == null) {
					// 	$('.dz-success').remove();
					// }
					if(self.getUploadingFiles().length == 0){ 
				 		$( "#btnGuardar" ).prop("disabled", false); 
				 		$( "#btnAutoGuardado" ).prop("disabled", false); 
				 		$( "#btnBorrador" ).prop("disabled", false); 
					}
				}); 

				self.on("removedfile", function (file) {
					var id = file._removeLink.id;
					$.ajax({ 
		              type: "POST",
		              url: GLOBAL_DATA.APP_BASE+'/contacs/delete_document_dropzone/',
		              data: {id:id},          
		              dataType: 'json',
		                success:  function(data) {}          
		            }); 
				});
			}
		};
	},

	// alertCamposAjaxFail:function(mensaje){
	// 	var alerta ='<div class="alert alert-danger alert-dismissible alertModal" role="alert" style="margin-top: 50px">';
	// 	alerta += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	// 	alerta += mensaje;
	// 	alerta += '</div>';
	// 	$('.alertConfirmDato').empty();
	// 	$('.alertConfirmDato').append(alerta); 
	// },

	alertCamposAjaxSucces:function(mensaje){
		Utilities.showMessaje(mensaje,"success");
		return;
		var alerta ='<div class="alert-success-recapmeeting">';
		    alerta +='<div class="alert alert-success alert-dismissible alertModal" role="alert">';
			alerta += '<button type="button" class="close closeAutoSave" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
			alerta += mensaje;
			alerta += '</div>';
		alerta += '</div>';
		$('.alertConfirmDato').empty();		
		$('.alertConfirmDato').append(alerta);
		setTimeout(function() {
			$(".closeAutoSave").click(); 
		}, 3000); 
	}, 

	isDocument: function(extension) {
		var fileInfo  = extension.split('.'); 
		ext  = fileInfo[fileInfo.length -1]; 
		if (typeof ext === "undefined" || ext == "" || ext == null) {
 		 	return false;
    	} else {
			var allowedExtensions = ["pdf","doc","docx"];
			if(allowedExtensions.indexOf(ext.toLowerCase()) != -1){
				return true;
			} else {
				return false;
			}  
    	}
	},
	
	uploadImageBrandWhite: function(){
		$('body').on('change', '.imageFile', function() { 
	      	var file      = $(this)[0].files[0];
	      	var fileName  = file.name; 
	      	var idField   = $(this).attr("id");			 
	      	EDIT.isImage(function(result){  
		      	if(result == true) {
		        	var fileSize = file.size;
		        	var sizeMB   = (fileSize / (1024*1024)).toFixed(2);
			        if (sizeMB > 10) {
			          	swal({type:"error", title:copys_js.error, text:copys_js.archive_size_high_client});
			          	$(this).val('');
			          	$("#ClientImg").val('');
		        		$("#ProjectImg").val('');
			        }
		      	}else{
		        	swal({type:"error", title:copys_js.error, text:copys_js.allow_only_images});
		        	$("#ClientImg").val('');
		        	$("#ProjectImg").val('');
		        	$(this).val('');
		      	}
	      	}, idField);
    	});
	},

	//MOSTRAR EL DETALLE DE LA REUNIÓN EN EL ACTA SI SE INICIO DESDE EL CALENDARIO
	showMeetingDay:function(){		
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/contacs/get_info_calendar_meeting/',
          data: {},          
          dataType: 'html',
          	success:  function(data) { 
          		$('#show-content-meeting-day-choose').html(data);
          		$("#meetingcontac").modal("show");	
          	}          
 		});	
	}, 

	//MOSTRAR EL DETALLE DE LA REUNIÓN EN EL ACTA SI SE INICIO DESDE EL CALENDARIO
	showMeetingContacSelected:function(){		
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/contacs/get_info_meeting_contac/',
          data: {},          
          dataType: 'html',
          	success:  function(data) { 
          		$('#show-content-meeting-day-choose').html(data);
          		$("#meetingcontac").modal("show");	
          	}          
 		});	
	}, 


	 initialChrono:function(){
        $("body").on("click", ".startTimer", function () { 
            var id = $(this).attr('data-id');
            timer.init(id);
            timer.start(id);  
        });
    },

    stopChrono:function(){
        $("body").on("click", ".stopTimer", function () { 
            var id = $(this).attr('data-id'); 
            timer.stop(id);  
        });
    },

    continueChrono:function(){
        $("body").on("click", ".continueTimer", function () { 
            var id = $(this).attr('data-id'); 
            timer.tick(id);  
        });
    }, 

    viewInfoMeetingActual:function(){
        $("body").on("click", ".view-meeting-contact", function () { 
            EDIT.showMeetingDay();
        });
    },

    viewInfoMeetingContacSelected:function(){ 
        $("body").on("click", "#view-meeting-contact-select", function () {  
            EDIT.showMeetingContacSelected();
        });
    }, 

    passwordContact:function(){
    	if (GLOBAL_DATA.passwordContac == GLOBAL_DATA.ENABLED) {
    		location.href = GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/password'; 
    	}
    },

    enterPassword:function(){

    	$("body").on('submit', '#ContacPasswordForm', function(event) {
    		event.preventDefault();
    		$("#btnPassword").click();
    	});

    	$('#btnPassword').click(function(event) {
    		var password = $('#ContacPassword').val();
	    	$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/enterPassword',{password:password},function (data){
	    		var obj = JSON.parse(data); 
	    		if (obj.errors != "") {
	    			$('#ContacPassword').val("");
	    			swal({type:"error", title:copys_js.error, text:obj.errors}); 
				} else {
					if(obj.action == "comments_contac"){
						url = GLOBAL_DATA.APP_BASE+'/comment_contacs/'+obj.action+'/'+obj.id; 
						location.href = url;
					} else {
						location.href = GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/'+obj.action+'/'+obj.id; 
					} 
				}
	    	});
    	});
    },


    //Mostrar el campo contraseña luego de enviar el post
    showProtectedPassword:function(){
    	if ($("#ContacProteger").is(':checked')) {
    		$("#content-password").show();
			$('.lbl_password').show();
			$('.lbl_generatePassword').show();
			$('#ContacPassword').show();
			$('#btn_generatePassword').show();
			$('.lbl_verPassword').show();
			$('#btn_verPassword').show();
			$('#btn_generatePassword').prop({
				'disabled': false,
			});
    	} else {  
			$("#ContacPassword").val(""); 
    	}
    }, 

    //mostrar la funcionalida de solicitar aprobación del acta
    showApprovalFields:function(){
    	if ($("#ContacReunion").is(':checked')) {
			$('.filled').text('');
			$('.parsley-error').removeClass('parsley-error');
			$('.parsley-success').removeClass('parsley-success');
			$('#btnGuardar').hide();
			$('#datetimepickerOff').hide();
			$('.lbl_endDate').hide();
			$('#btnBorrador').show();
			$("#content-approval-solicitud").show();
			$("#ContacEndDate").val("");  
    	} else {
			$('#btnGuardar').show();
			$('#datetimepickerOff').show();
			$('.lbl_endDate').show();
			$('#btnBorrador').hide();
			$("#content-approval-solicitud").hide(); 
			$("#content-approval").hide();
			$("#requestApproval").attr('checked', false); 
    	}
    },

    //MOSTRAR LA ÚLTIMA ACTA DE ACUERDO AL EQUIPO DE TRABAJO SELECCIONADO
    showLastContac:function(teamId){
    	if(GLOBAL_DATA.action != "view"){
			$.ajax({
				url: GLOBAL_DATA.APP_BASE+'/contacs/show_last_contac/',
				data: {teamId:teamId}, 
				dataType: 'html',
				method:'POST',
			 	async: false,  
				success:function(data){  
					$("#show_last_contac").html(data);
				}, 
			});
    	}
    }, 

    //CARGAR LOS USUARIOS QUE SE ESCOGIERON PARA APROBAR EL ACTA
    loadApprovalUsers:function(contacId){  
    	if(GLOBAL_DATA.action != "view"){
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/approval_contacs/users_to_approved/',
	          data: {contacId:contacId},          
	          dataType: 'json',
	          	success:  function(data) { 
					$("#ContacApprovalusers").val(data).trigger("change");  
	          	}          
	 		}); 
		}
    },

    isImage:function(response, idField) {  
     	if(idField == "ProjectImg"){
      		var formData = new FormData($("#ProjectAddProjectForm")[0]); 
      		var urlApp   = GLOBAL_DATA.APP_BASE+'/projects/verify_img/';
     	} else {
      		var formData = new FormData($("#ClientAddClientForm")[0]);
      		var urlApp   = GLOBAL_DATA.APP_BASE+'/clients/verify_img/';	   
     	} 
	    $.ajax({ 
	      type: "POST",
	      contentType:false,
	      cache: false,
	      processData:false,
	      url: urlApp,
	      data:  formData,
	      dataType: 'json',
	      beforeSend:function(){
	      	$("#btn_saveAddCliente").prop("disabled", true);
	      	$("#btn_saveAddProyecto").prop("disabled", true);
	      },
	      success:  function(data) {
	      	$("#btn_saveAddCliente").prop("disabled", false);
	      	$("#btn_saveAddProyecto").prop("disabled", false);
	        response(data);
	      }
	    }); 
  	},     

  	selectAllValues:function(){
  		$("body").on('click', '#ProjectSelectAll', function(event) { 
		    if($(this).is(':checked') ){
		        $("#ProjectUsers > option").prop("selected","selected");
		        $("#ProjectUsers").trigger("change");
		    }else{
		        $("#ProjectUsers > option").removeAttr("selected");
	         	$("#ProjectUsers").trigger("change");
	     	}
		}); 
	}
}
//GUARDAR EL COMPROMISO AÑADIDO EN EL EDITAR ACTA
function saveCommitment(description, limitDate, user){
	var result = "";
   	$.ajax({
		url: GLOBAL_DATA.APP_BASE+'/commitments/save_commitments_edit_contac/',
		data: {description:description, limitDate:limitDate, user:user}, 
		dataType: 'json',
		method:'POST',
	 	async: false,  
		success:function(resultado){  
			result = resultado; 
		}, 
	});
   return result;
}

//MOSTRAR LA IMAGEN DEL CLIENTE EN EL ACTA
function showImageClient(id){
	$.ajax({
		url: GLOBAL_DATA.APP_BASE+'/clients/show_image_client/',
		data: {id:id}, 
		dataType: 'html',
		method:'POST',
	 	async: false,  
		success:function(resultado){  
			$("#show_img_client").html(resultado);
		}, 
	});
}

//MOSTRAR LA IMAGEN DEL PROYECTO EN EL ACTA
function showImageProject(id){
	$.ajax({
		url: GLOBAL_DATA.APP_BASE+'/projects/show_image_project/',
		data: {id:id}, 
		dataType: 'html',
		method:'POST',
	 	async: false,  
		success:function(resultado){   
			if(id != null){
				$("#show_img_project").html(resultado);
				$(".form-project").removeClass("form-project-max");
				$("#show_img_project").addClass("show-image");
				$("body").find('a.showImages').magnificPopup({type:'image'});
			} else {
				$("#show_img_project").html("");
				$("#show_img_project").removeClass("show-image");
				$(".form-project").addClass("form-project-max");
			}
		}, 
	});
}

function loadMime(file, callback) {
    
    //List of known mimes
    var mimes = [
        {
            mime: 'image/jpeg',
            pattern: [0xFF, 0xD8, 0xFF],
            mask: [0xFF, 0xFF, 0xFF],
        },
        {
            mime: 'image/png',
            pattern: [0x89, 0x50, 0x4E, 0x47],
            mask: [0xFF, 0xFF, 0xFF, 0xFF],
        }
        // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern
    ];

    function check(bytes, mime) {
        for (var i = 0, l = mime.mask.length; i < l; ++i) {
            if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
                return false;
            }
        }
        return true;
    }

    var blob = file.slice(0, 4); //read the first 4 bytes of the file

    var reader = new FileReader();
    reader.onloadend = function(e) {
        if (e.target.readyState === FileReader.DONE) {
            var bytes = new Uint8Array(e.target.result);
            for (var i=0, l = mimes.length; i<l; ++i) {
                if (check(bytes, mimes[i])) return callback(true);
                	//return callback("Mime: " + mimes[i].mime + " <br> Browser:" + file.type);
            }
            // return callback("Mime: unknown <br> Browser:" + file.type);
            return callback(false);
        }
    };
    reader.readAsArrayBuffer(blob);
}
 

