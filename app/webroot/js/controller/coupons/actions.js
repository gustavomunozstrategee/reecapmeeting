Coupons = {
 
    InitElements:function(){
        $(document).ready(function(){  
            if(GLOBAL_DATA.action == "add"){
              Coupons.configurationDateRange(); 
            }
            $(document).on('click', '.generar-codigo', function(){ 
                Coupons.generateCodeCoupon();        
            });
            $(document).on('click', '.btn-disabled-coupon', function(){
                ELEMENT = $(this);
                Coupons.showModalConfirmationCouponDisabled(ELEMENT);        
            });  
            $('.send_reject').on('click', function() {  
                var couponId  = $(this).attr('data-id');
                Coupons.sendReasonDisabledCoupon(couponId); 
            }); 
        });
    },

    generateCodeCoupon:function(){
      $.ajax({ 
        type: "POST",
        url: GLOBAL_DATA.APP_BASE+'/coupons/generateCodeCoupon/',
        data: {},          
        dataType: 'json',
          success:  function(data) {
              $("#CouponCode").val(data); 
          }
      }); 
    },

    showModalConfirmationCouponDisabled:function(){
      $('#reasonDisabledCoupon').modal('show');
      $(".send_reject").attr('data-id', ELEMENT.attr('data-id'));
    },

    sendReasonDisabledCoupon:function(couponId){
        var reason  = $("#ReasonCouponDisabled").val(); 
        if(reason == null || reason.length == 0 || /^\s+$/.test(reason)) {
            swal({type:"error", title:copys_js.error, text:copys_js.coupon_required});
        } else {
            $.ajax({ 
              type: "POST",
              url: GLOBAL_DATA.APP_BASE+'/coupons/disabledCoupon/',
              data: {reason:reason, couponId:couponId},          
              dataType: 'json',
                beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find(".send_reject").addClass("disabled").attr("disabled");
                    $("body").find(".btn-disabled-coupon").addClass("disabled").attr("disabled");
                    $("body").find(".btn-disabled-coupon").removeAttr("data-dismiss");
                },
                complete:  function(data) { 
                    $("body").find("p.errorP").hide();
                    location.href = GLOBAL_DATA.APP_BASE+'/coupons/index';   
                }
            }); 
        } 
    },

    configurationDateRange:function(){
        $('input[name="daterange"]').daterangepicker({ 
          locale: {
              format: 'DD-MM-YYYY',
              "applyLabel": copys_js.btn_confirm,
              "cancelLabel": copys_js.btn_cancel,
              "fromLabel": "From",
              "toLabel": "To",
              "customRangeLabel": "Custom",
              "daysOfWeek": [
                  copys_js.Do,
                  copys_js.Lu,
                  copys_js.Ma,
                  copys_js.Mi,
                  copys_js.Ju,
                  copys_js.Vi,
                  copys_js.Sá
              ],
              "monthNames": [
                  copys_js.Enero,
                  copys_js.Febrero,
                  copys_js.Marzo,
                  copys_js.Abril,
                  copys_js.Mayo,
                  copys_js.Junio,
                  copys_js.Julio,
                  copys_js.Agosto,
                  copys_js.Septiembre,
                  copys_js.Octubre,
                  copys_js.Noviembre,
                  copys_js.Diciembre
              ],
          }
      });
    }
}

Coupons.InitElements();