var commitmentsIds = []; 
function onlyNumberField(evt){ 
    if(window.event){//asignamos el valor de la tecla a keynum
        keynum = evt.keyCode; //IE
    } else{
        keynum = evt.which; //FF
    } 
    //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
    if((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6 || keynum == 44 || keynum == 46 || keynum == 45){
        return true;
    } else {
        return false;
    }
}

function listCommitmentsDotDone(teamIds){ 
	$.ajax({ 
      type: "GET",
      url:  GLOBAL_DATA.APP_BASE+'/commitments/commitments_not_done/',
      data: {teamIds:teamIds},          
      dataType: 'html',
      	success:  function(data) {       		 
			$("#show-content-commitments-not-done").html(data);        		 
      	}          
	});
}

function listCommitmentsWeekActual(teamIds){
	$.ajax({ 
      type: "GET",
      url: GLOBAL_DATA.APP_BASE+'/commitments/commitments_week_actual/',
      data: {teamIds:teamIds},          
      dataType: 'html',
      	success:  function(data) {       		 
			$("#show-content-commitments-not-done-week").html(data);        		 
      	}          
	});
}

function listCommitmentsCompleted(teamIds){
	$.ajax({ 
      type: "GET",
      url: GLOBAL_DATA.APP_BASE+'/commitments/commitments_completeds/',
      data: {teamIds:teamIds},          
      dataType: 'html',
      	success:  function(data) {       		 
			$("#show-content-commitments-completed").html(data);        		 
      	}          
	});
}

function verifyCommitment(){
	$.ajax({ 
      type: "POST",
      url: GLOBAL_DATA.APP_BASE+'/commitments/verify_commitment_to_done/',
      data: {},          
      dataType: 'json',
      	success:  function(data) { 
      		if(data == true){ 
  				$("#markAsReadCommitment").modal("show");  
      		} 
      	}          
	});
}

function validateCommitmentSelected(){
	$.ajax({ 
      type: "POST",
      url: GLOBAL_DATA.APP_BASE+'/commitments/read_commitments_select/',
      data: {},          
      dataType: 'json',
      	success:  function(data) {  
      		if(data == true){  
  				$("#marcar-realizado-compromiso").show();
      		} else {
  				$("#marcar-realizado-compromiso").hide();
      		}
      	}          
	});
}

var Compromisos = {
	
	initElements:function(){
		if (GLOBAL_DATA.action == 'mark_commitment_as_done'){
			Compromisos.markAsDoneCommitment();	 
			verifyCommitment();
		} else {
			listCommitmentsDotDone();
			listCommitmentsWeekActual();
			listCommitmentsCompleted();
			Compromisos.showTimeCommitments();		 
			Compromisos.showContentDefaultTimeToCommitmentReminder();		 
			Compromisos.saveTimeCommitments();		 
			Compromisos.showHourCommitments();		 
			Compromisos.saveHourReminderCommitments();	
			Compromisos.showModalConfirmationCommitment();
			Compromisos.selectCommitmentId();
			validateCommitmentSelected();
			Compromisos.saveCommitementToDone();			
			Compromisos.searchCommitmentsForTeam();			
		} 
	},

	showTimeCommitments:function(){
		$("body").on("click", "#recordatorio-compromiso-tiempo", function () {				  
			$("#modal-recordatorio-tiempo").modal("show"); 
		});  	
	},

	showContentDefaultTimeToCommitmentReminder:function(){
		$("body").on("change", "#choose-time-commitment", function () {	
			var optionTime = $("#choose-time-commitment").val();
			if(optionTime == 0){
				$("#show-content-time-default-commitment").show();
			} else {
				$("#show-content-time-default-commitment").hide(); 
			} 
		});
	},

	saveTimeCommitments:function(){
		$("body").on("click", "#btn-guardar-tiempo-recordatorio-compromisos", function () {
			var optionTime  = $("#choose-time-commitment").val();
			var defaultTime = $("#default-time-commitment").val();
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/commitments_configurations/save_time_commitment/',
	          data: {optionTime:optionTime, defaultTime:defaultTime},          
	          dataType: 'json',
	          	success:  function(data) {  
	          		if(data.state == false){
	          			swal({type:"error", title:copys_js.error, text:data.message}); 
	          		} else if (data.state == true) {
          				swal({type:"success", title:copys_js.exito, text:data.message});
          				$("#modal-recordatorio-tiempo").modal("hide"); 
	          		}
	          	}          
	 		});
		});
	},

	showHourCommitments:function(){
		$("body").on("click", "#recordatorio-compromiso-empresa-hora", function () {				  
			$("#modal-recordatorio-hora").modal("show"); 
		});  	
	},

	saveHourReminderCommitments:function(){
		$("body").on("click", "#btn-save-hour-reminder-commitment", function () {
			var hour = $("#hour-reminder-commitment").val();
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/commitments_configurations/save_hour_commitment/',
	          data: {hour:hour},          
	          dataType: 'json',
	          	success:  function(data) {  
          		 	if (data.state == true) {
          				swal({type:"success", title:copys_js.exito, text:data.message});
          				$("#modal-recordatorio-hora").modal("hide"); 
	          		}
	          	}          
	 		});
		});		
	},

	markAsDoneCommitment:function(){
		$("body").on("click", ".btn-mark-as-read", function () {
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/commitments/mark_commitment/',
	          data: {},          
	          dataType: 'json',
         	 	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find(".btn-mark-as-read").prop('disabled', true); 
                },
	          	success:  function(data) {  
          		 	if (data.state == true) {
          		 		$("body").find("p.errorP").hide(); 
          				$("body").find(".btn-mark-as-read").prop('disabled', false);
          				$("#markAsReadCommitment").modal("hide");
          				location.href = GLOBAL_DATA.APP_BASE+'/commitments/index/'; 
	          		}
	          	}          
	 		});
		});	
	},

	selectCommitmentId:function(){	 		
		$("body").on("click", ".chk-commitments", function () { 
			var commitmentsIds = $(this).val(); 
			if ($(this).is(':checked')) {         
				var add = true;
	           	$.ajax({ 
		          type: "POST",
		          url: GLOBAL_DATA.APP_BASE+'/commitments/save_commitment_id_selected/',
		          data: {commitmentsIds:commitmentsIds, add:add},          
		          dataType: 'json', 
		          	complete:  function(data) {
		          		validateCommitmentSelected();
		          		listCommitmentsDotDone();
						listCommitmentsWeekActual();
						listCommitmentsCompleted();
		          	}          
		 		});            	
            } else {
            	var edit = false;
            	$.ajax({ 
		          type: "POST",
		          url: GLOBAL_DATA.APP_BASE+'/commitments/save_commitment_id_selected/',
		          data: {commitmentsIds:commitmentsIds, edit:edit},          
		          dataType: 'json', 
		          	complete:  function(data) {
		          		validateCommitmentSelected();
		          		listCommitmentsDotDone();
						listCommitmentsWeekActual();
						listCommitmentsCompleted();
		          	}          
		 		}); 
            }        	
        });  
	},

	showModalConfirmationCommitment:function(){ 
		$("body").on("click", "#marcar-realizado-compromiso", function () { 
			$("#modal-marcar-como-realizado").modal("show"); 
		}); 		
	},

	saveCommitementToDone:function(){
		$("body").on("click", "#btn-save-commitment-done", function () { 
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/commitments/save_commitments/',
	          data: {},          
	          dataType: 'json',
         	 	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find("#btn-save-commitment-done").prop('disabled', true); 
                    $("body").find("#btn-cancel-commitment-done").prop('disabled', true); 
                },
	          	success:  function(data) {  
          		 	if (data.state == true) {
          		 		$("body").find("p.errorP").hide();
          				swal({type:"success", title:copys_js.exito, text:data.message});
          				$("body").find("#btn-save-commitment-done").prop('disabled', false);
          				$("body").find("#btn-cancel-commitment-done").prop('disabled', false);
          				$("#modal-marcar-como-realizado").modal("hide"); 
	          		}
	          	},
	          	complete: function(data){
	          		validateCommitmentSelected();
	          		listCommitmentsDotDone();
					listCommitmentsWeekActual();
					listCommitmentsCompleted();
	          	}          
	 		}); 
		});
	},

	searchCommitmentsForTeam:function(){
		$("body").on("change", "#teams_commitments_list", function () { 
			var teamIds = $('#teams_commitments_list').val(); 
			if(teamIds != null){
				listCommitmentsDotDone(teamIds);
				listCommitmentsWeekActual(teamIds);
				listCommitmentsCompleted(teamIds); 
			} else {
				listCommitmentsDotDone();
				listCommitmentsWeekActual();
				listCommitmentsCompleted(); 
			}
		});
	}
}

Compromisos.initElements();