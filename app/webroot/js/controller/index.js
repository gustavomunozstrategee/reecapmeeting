$("body").on('click', '.btn_estado', function(event) {
	event.preventDefault();
	var id = $(this).data('id');
	swal({
	    title: copys_js.ten_cuidado,
	    text: copys_js.continue_action,
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: copys_js.btn_confirm,
	    cancelButtonText: copys_js.btn_cancel,
	},
	function(){
		$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/changeState',{id:id},function (resultado){
			location.reload();
		});
	});
});

$(document).on('keydown keyup keypress paste', '.textarea_client', function(e) {
    var textArea = $('.textarea_client').val(),
    textLenght   = textArea.length,
    limit        = 300,
    remain       = textLenght + '/' + (limit - textLenght); 
	  var key 	   = e.keyCode;
    $('#showCharacterClient').html(remain);
    if (textLenght >= 300) { 
       if(key == 13 || key == 32){
            $('.textarea_client').val((textArea).substring(0, limit));
            return false;
       } else {
            $('.textarea_client').val((textArea).substring(0, limit)); 
       } 
    }  
}); 


$(document).on('keydown keyup keypress paste', '.textarea_project', function(e) {
    var textArea = $('.textarea_project').val(),
    textLenght   = textArea.length,
    limit        = 300,
    remain       = textLenght + '/' + (limit - textLenght); 
    var key      = e.keyCode;
    $('#showCharacterProject').html(remain); 
	if (textLenght >= 300) { 
       if(key == 13 || key == 32){
            $('.textarea_project').val((textArea).substring(0, limit));
            return false;
       } else {
            $('.textarea_project').val((textArea).substring(0, limit)); 
       } 
    }  
}); 
 
 