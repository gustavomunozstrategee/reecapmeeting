
 Projects = { 

	initElements:function(){ 
		$(document).ready(function(){ 
			$('#ProjectUsers').select2({
				language: 'es'
			});  
			var clientId = $("#ProjectClientId").val();
			var users    = $("#ProjectUsers").val();
			if(users == null){
				Projects.loadUsers(clientId); 
			} 
			Projects.loadUserWithCliente();
			Projects.selectAllValues();	
			Projects.updatePhoto();		
		}); 
	},

	loadUsers:function(clientId){  
 		$.ajax({ 
          	type: "POST",
          	url: GLOBAL_DATA.APP_BASE+'/user_teams/load_all_users/',
          	data: {clientId:clientId},       
          	dataType: 'html',
          	success:function(data){
          		$("#content_users").html(data);
          		$('#ProjectUsers').select2({
					language: 'es'
				});          		 
          	}
 		});	 
	}, 

	loadUserWithCliente:function(){
		$('body').on('change', '#ProjectClientId',function(event) {
			Projects.loadUsers($(this).val());
			$("#ProjectSelectAll").removeAttr("checked");
		});
	},

	selectAllValues:function(){
		$("#ProjectSelectAll").click(function(){
		    if($("#ProjectSelectAll").is(':checked') ){
		        $("#ProjectUsers > option").prop("selected","selected");
		        $("#ProjectUsers").trigger("change");
		    }else{
		        $("#ProjectUsers > option").removeAttr("selected");
	         	$("#ProjectUsers").trigger("change");
		     }
		}); 
	},

	updatePhoto:function() {
    $('#ProjectImg').change(function() { 
      var file = $("#ProjectImg")[0].files[0];
      var fileName  = file.name; 
      Projects.isImage(function(result){          
        if(result == true){
          var fileSize = file.size;
          var sizeMB = (fileSize / (1024*1024)).toFixed(2);
          if (sizeMB > 10) {
            swal({type:"error", title:copys_js.error, text:copys_js.archive_size_high_client});
            $('#ProjectImg').val('');
          }
        }else{
          swal({type:"error", title:copys_js.error, text:copys_js.allow_only_images});
          $('#ProjectImg').val('');
        }  
      }); 
    });
  },

  isImage:function(response) { 
    if(GLOBAL_DATA.action == "add"){
      var formData = new FormData($("#ProjectAddForm")[0]);
    } else {
      var formData = new FormData($("#ProjectEditForm")[0]);
    } 
    $.ajax({ 
      type: "POST",
      contentType:false,
      cache: false,
      processData:false,
      url: GLOBAL_DATA.APP_BASE+'/projects/verify_img/',
      data:  formData,
      dataType: 'json',
      success:  function(data) {
        response(data);
      }
    }); 
  }
} 
Projects.initElements();