Logs = {
	init:function(){
		Logs.initDatePickerLog();
		Logs.showDetails();
	},

	initDatePickerLog: function(){ 
		$('#datetimepickerStartLog').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			language: "es"
		}); 
	},

	showDetails: function() {
	    $(function(){
	        $('.btn-show-details').click(function(){
	          var details = $($(this).parent('').find('.tbl-details').clone());
	          details.removeClass('hidden');
	          $("#modalDetailLog").find('.modal-body').html("");  
	          $("#modalDetailLog").find('.modal-body').html(details);  
	          $("#modalDetailLog").modal('show');  
	        });
	    });
	}

}
    