
 Transactions = { 

	initElements:function(){ 
		$(document).ready(function(){ 
			$("#closeNotificationTransaction").on('click', function() {
				ELEMENT = $(this);
				Transactions.disableNotificationTransaction(ELEMENT);			 				
			});
			$(".btn-view-detail-transaction").on('click', function() {
				ELEMENT = $(this);
				Transactions.showDetailTransaction(ELEMENT);			 				
			});    
		}); 
	},

	disableNotificationTransaction:function(ELEMENT){
		var transactionId = ELEMENT.attr("data-id");		 
 		$.ajax({ 
          	type: "POST",
          	url: GLOBAL_DATA.APP_BASE+'/transactions/disableNotificationTransactionPending/',
          	data: {transactionId:transactionId},       
          	dataType: 'json',
          	success:function(data){}
 		});	 
	}, 

	showDetailTransaction:function(){
		var transactionId = ELEMENT.attr("data-id");
		var planId        = ELEMENT.attr("data-plan");
		$.ajax({ 
          	type: "POST",
          	url: GLOBAL_DATA.APP_BASE+'/transactions/view_detail/',
          	data: {transactionId:transactionId, planId:planId},       
          	dataType: 'html',
          	success:function(data){
          		$('#DetailTransaction').modal('show');
				$('#show-content-transaction').html(data);          		
          	}
 		});
	} 
} 

Transactions.initElements();