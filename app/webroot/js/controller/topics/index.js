function onlyNumberField(evt){ 
    if(window.event){//asignamos el valor de la tecla a keynum
        keynum = evt.keyCode; //IE
    } else{
        keynum = evt.which; //FF
    } 
    //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
    if((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6 || keynum == 44 || keynum == 46 || keynum == 45){
        return true;
    } else {
        return false;
    }
}

function listFollowing(){
	$.ajax({
	    type: "POST",
        url: GLOBAL_DATA.APP_BASE+'/meetings/meeting_following',
        data: {},
        dataType: 'html',
        success: function(data) { 
  			$('#list-following-meetings').html(data);	           
        }
    }); 	
}

function listInvited(){
	$.ajax({
        type: "POST",
        url: GLOBAL_DATA.APP_BASE+'/assistants/list_assistants',
        data: {},
        dataType: 'html',
        success: function(data) { 
  			$('#show-content-inviteds').html(data);	            
        }
    }); 
} 

Topics = {
    
	initElements:function(){
		$(document).ready(function(){ 			
			//Topics.initGeoComplete(); 
			Topics.showContentTopics(); 
			Topics.cleanClients(); 
		    passwordOption = $(".password-meeting").val();
		    publicMeeting  = $(".public-meeting").val();
		    modalityOption = $("#MeetingMeetingRoom").val(); 	 		        		
		 	Topics.FieldLocation(modalityOption);		        		
			Topics.addTopics();
			Topics.dateTimeMeeting();
			if($(".password-meeting").is(':checked')) {  
    		    $('#content-password').show();
 				$("#pass-meeting").attr("disabled", false);   
        	} else {  
        	    $('#content-password').hide();
 				$("#pass-meeting").attr("disabled", true);   
        	}   
			$('#invited').keyup(function(){
				ELEMENT = $(this);
				Topics.searchInviteds(ELEMENT);
			}); 
			$('.password-meeting').change(function(){  
			 	publicMeeting  = $(".public-meeting").val();
		 	 	passwordOption = $(".password-meeting").val();
				Topics.FieldPassword(passwordOption,publicMeeting); 
			});
			$('.public-meeting').change(function(){
			 	$('#content-password').hide();
			 	$("#pass-meeting").attr("disabled", true); 
			});
			$('.generate-password').click(function(){
		 		Topics.generatePassword();
			});
			$('.select-location').change(function(){
		 	 	modalityOption = $("#MeetingMeetingRoom").val();
				Topics.FieldLocation(modalityOption);
			});
			$('.btn-save-meeting').click(function(){		 	 	 
				Topics.getFormMeeting();
			}); 
			$('.meeting-type').change(function(){
				Topics.showContentTopics(); 		
			}); 
			$('.btn-add-new-date').click(function(){		 	 	 
				Topics.addMeetingFollowing();
			}); 
			$("body").on("click", ".btn-add-topic-meeting-following", function () {
				$(".btn-add-topics-meetings").attr('data-id', $(this).attr('data-id'));	 
				$("#modalTopics").modal("show"); 
			});  		 	 	 
			$("body").on("click", ".btn-add-topics-meetings", function () {		 	 	 
				ELEMENT = $(this);
				Topics.addTopicsToNewMeeting(ELEMENT);
			});
			$("body").on("click", ".btn-view-topic-meeting-following", function () {
				ELEMENT = $(this);
				Topics.showTopicsMeetingsFollowing(ELEMENT);
				$("#modalTopicsDetail").modal("show");
			});
			$("body").on("click", ".btn-save-meeting-following", function () { 
				Topics.getFormMeetingFollowing(); 
			});
			$("body").on("click", ".delete-user-meeting-following", function () {
				ELEMENT = $(this); 
				Topics.deleteUserToMeetingFollowing(ELEMENT); 
			});
			$("body").on("click", ".btn-auth-gmail", function () { 
				//location.href = GLOBAL_DATA.APP_BASE+'/meetings/addEventInGmail/';
				location.href = GLOBAL_DATA.APP_BASE+'/meetings/create_meeting_gmail/';
				// Topics.addEventGmail(); 
			}); 
			$("body").on("click", ".btn-auth-gmail-followings-meetings", function () { 
				Topics.addEventGmailFollowing(); 
			}); 
			$("body").on("click", ".show-password", function () {
				ELEMENT = $(this);  
				Topics.showPassword(ELEMENT);
				$(".show-password").hide();
				$(".hide-password").show();
			}); 
			$("body").on("click", ".hide-password", function () {
				ELEMENT = $(this);  
				Topics.hidePassword(ELEMENT);
				$(".show-password").show();
				$(".hide-password").hide();
			}); 
			$("body").on("click", ".btn-add-meetings-data-without-sync", function () {				  
				Topics.saveMeetingWithoutSync(); 
			});
			$("body").on("click", ".btn-add-meetings-followings-data-without-sync", function () {				  
				Topics.saveMeetingFollowingWithoutSync(); 
			});
			$("body").on("change", "#MeetingTeamId", function () {

			});

			$("body").on('click', '.btn-delete-topic-meeting-following', function(event) {
				var url 	= 	$(this).attr("href");
				var dateId 	= 	$(this).data("id");
				event.preventDefault();
				swal({
			        title: copys_js.btn_confirm,
			        text: copys_js.verify_action,
			        type: "warning",
			        showCancelButton: true,
			        confirmButtonColor: "#00a65a",
			        confirmButtonText: copys_js.btn_confirm,
			        cancelButtonText: copys_js.btn_cancel,
			        closeOnConfirm: true,
			        closeOnCancel: true
			    }, function (isConfirm) {
			        if (isConfirm) {
			            Utilities.ajaxRequestPost(url,{dateId:dateId},function(response){
			            	Utilities.showLoading(false);
			            	listFollowing();
			            })
			        }
			    });
			});
			Topics.searchClients();  	
		}); 
	},

	addTopics:function(){
		var COUNT = 0;	 
		var
	    gradeTable    = $('#grade-table'),
	    gradeBody     = gradeTable.find('tbody'),
	    gradeTemplate = _.template($('#grade-template').remove().text()),
	    numberRows    = gradeTable.find('tbody > tr').length;
		gradeTable
	    .on('click', 'a.add', function(e) {
	        e.preventDefault();
	        COUNT++; 
	        $(gradeTemplate({key: numberRows++}))
	            .hide()
	            .appendTo(gradeBody)
	            .fadeIn('fast');
	         	var last_id = $('input[type="text"]:last').attr('id',COUNT);
	     	$(".time-duration").timingfield({ 
	  			maxHour:        23,		 
	  			width:          263,		 
	  			hoursText:      'H',		 
	  			minutesText:    'M' 
			}); 
			$( ".timingfield_seconds" ).hide();
	    })
	    .on('click', 'a.remove', function(e) {
	        e.preventDefault();
	        $(this)
	            .closest('tr')
	            .fadeOut('fast', function() {
	                $(this).remove();
	            });
	    });
	    if (numberRows === 0) {
	        gradeTable.find('a.add').click();
	    }
		$(".time-duration").timingfield({ 
		  	maxHour:        23,		 
		  	width:          263,		 
		  	hoursText:      'H',		 
		  	minutesText:    'M' 
		});    
		$( ".timingfield_seconds" ).hide();
	},

	searchInviteds:function(ELEMENT){
		var teamId   = $("#MeetingTeamId").val();
		var clientId = $("#MeetingClientId").val();   
        $('#suggestions').css({'width': '100%','height': 'auto','overflow-y':'auto'}); 
		var invited    = ELEMENT.val(); 
        var dataString = {invited:invited, clientId:clientId, teamId:teamId}; 
        $.ajax({
            type: "POST",
            url: GLOBAL_DATA.APP_BASE+'/assistants/searchInvited',
            data: dataString,
            success: function(data) { 
                //Escribimos las sugerencias que nos manda la consulta
                $('#suggestions').fadeIn(1000).html(data);
                //Al hacer click en alguna de las sugerencias
                $('.inviteds').on('click', function(){
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id       = $(this).attr('id');
                    var typeUser = $(this).attr('data-name');
                    Topics.addInvited(id, typeUser); 
                    //Editamos el valor del input con data de la sugerencia pulsada
                    $('#invited').val(""); 
                    //Hacemos desaparecer el resto de sugerencias 
                    $('#suggestions').hide();
                });              
            }
        });
	},

	addInvited:function(id, typeUser){
		$.ajax({
            type: "POST",
            url: GLOBAL_DATA.APP_BASE+'/assistants/add_users_invited',
            data: {id:id, typeUser:typeUser},
            dataType: 'html',
            beforeSend: function(e){
                $("body").find("p.loadAddAssistants").show(); 
            },
            success: function(data) { 
            	$("body").find("p.loadAddAssistants").hide();
      			$('#show-content-inviteds').html(data);
      			$('#suggestions').hide();	            
            }
            
        }); 
	},

	dateTimeMeeting: function(){
		
		$('#datetimepickerStart>input').daterangepicker({
			"singleDatePicker": true,
    		"timePicker": true,
    		"timePicker24Hour": true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});
		$('#datetimepickerEnd>input').daterangepicker({
			"singleDatePicker": true,
    		"timePicker": true,
    		"timePicker24Hour": true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});
	},

	generatePassword:function(){
		$.ajax({
            type: "POST",
            url: GLOBAL_DATA.APP_BASE+'/meetings/generatePasswordMeeting',
            data: {},
            dataType: 'json',
            success: function(data) { 
  			 	$("#pass-meeting").val(data);             
            }
        });
	},

	FieldPassword:function($passwordOption, publicMeetingn){
		if (passwordOption == 2) {
            $('#content-password').show();
 			$("#pass-meeting").attr("disabled", false); 
        } else if (publicMeetingn == 1){
        	$('#content-password').hide();
        	$("#pass-meeting").attr("disabled", true);
        }
	}, 

	FieldLocation:function($modalityOption){
		if (modalityOption == 1) {
            Topics.showFieldLocation(); 
        } else {
        	$("#MeetingLocation").attr("disabled", true);
        	$('#location-meeting').hide();
        }
	}, 

	showFieldLocation:function(){
 		$('#location-meeting').show();
 		$("#MeetingLocation").attr("disabled", false);
	},

	getFormMeeting:function(){
		var dataForm = $("#MeetingAddForm").serialize(); 
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/addMeeting/',
          data: dataForm,          
          dataType: 'json',
          	success:  function(data) {
          		if(data.state == false && data.validation == false){
          			swal({type:"error", title:copys_js.error, text:data.message});
          		} else if (data.validation == true) {
          			var messages = data.message.join('\r\n');
          			swal({type:"error", title:copys_js.error, text:messages});
          		} else if (data.state == false) {
          			swal({type:"error", title:copys_js.error, text:data.message});
          		}  else if (data.state == true) {
          			if(data.message != null){
          				swal({type:"success", title:copys_js.exito, text:data.message});
          			}
          			$("#AutenticationEmail").modal("show"); 
          		}
          	}          
 		});
	},

	getFormMeetingFollowing:function(){
		var dataForm = $("#MeetingAddForm").serialize(); 
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/saveMeetingFollowing/',
          data: dataForm,          
          dataType: 'json',
          	success:  function(data) {
          		if(data.state == false && data.validation == false){
          			swal({type:"error", title:copys_js.error, text:data.message});
          		} else if (data.validation == true) {
          			var messages = data.message.join('\r\n');
          			swal({type:"error", title:copys_js.error, text:messages});
          		} else if (data.state == false) {
          			swal({type:"error", title:copys_js.error, text:data.message});
          		}  else if (data.state == true) {
          			$("#AutenticationEmailMeetingFollowing").modal("show"); 
          		}
          	}          
 		});
	},

	addMeetingFollowing:function(){
		var dateStart     = $("#MeetingStartDate").val(); 
		var dateEnd       = $("#MeetingEndDate").val(); 
		var modality      = $("#MeetingMeetingRoom").val(); 
		var modalityName  = $("#MeetingMeetingRoom option:selected").text(); 
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/addMeetingFollowing/',
          data: {dateStart:dateStart, dateEnd:dateEnd, modality:modality, modalityName:modalityName},          
          dataType: 'json',
          	beforeSend: function(e){
                $("body").find("p.loadAddMeetingFollowing").show();
                $("body").find(".btn-add-new-date").prop('disabled', true);  
            },
          	success:  function(data) {
          		if (data.state == false) {
          			$("body").find("p.loadAddMeetingFollowing").hide();
					$("body").find(".btn-add-new-date").prop('disabled', false);  
          			swal({type:"error", title:copys_js.error, text:data.message});
          		} else if(data.state == true){
          			var dateStart = $("#MeetingStartDate").val(""); 
					var dateEnd   = $("#MeetingEndDate").val("");
          			$("body").find("p.loadAddMeetingFollowing").hide();
					$("body").find(".btn-add-new-date").prop('disabled', false);  
          			listFollowing();
          		} 
          	}          
 		});		
	},

	addTopicsToNewMeeting:function(ELEMENT){
		var dataId   = ELEMENT.attr('data-id');
		var dataForm = $("#topics-following").serialize(); 
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/addTopicsToFollowing/'+dataId,
          data: dataForm,          
          dataType: 'json',
          	success:  function(data) { 
          		if (data.state == false) {
          			swal({type:"error", title:copys_js.error, text:data.message});

          		} else if (data.state == true) {
      		 		$("#description-topics").val(""); 
      		 		$("#add-topic-following").val(""); 
      		 		$(".hour").val("0"); 
      		 		$(".minute").val("0"); 
      		 		$("#TopicTime").val("0");
      		 		$("#modalTopics").modal("hide");
      		 		swal({type:"success", title:copys_js.exito, text:data.message});  
          		}
          	}          
 		});	
	},

	showTopicsMeetingsFollowing:function(ELEMENT){
		var dataId   = ELEMENT.attr('data-id');
		$.ajax({
		    type: "POST",
	        url: GLOBAL_DATA.APP_BASE+'/meetings/show_topics_meeting_following',
	        data: {dataId:dataId},
	        dataType: 'html',
	        success: function(data) { 
	  			$('#show-content-topics-meetings-followings').html(data);	            
	        }
    	}); 
	},

	deleteSessionMeetingsFollowing:function(){
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/deleteAllSessionMeeting/',
          data: {},          
          dataType: 'json',
          	complete:  function(data) {
 				listFollowing();
          	}          
 		});	
	},

	deleteUserToMeetingFollowing:function(ELEMENT){
		var dataId = ELEMENT.attr('data-id');
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/assistants/removeUserFromMeeting/',
          data: {dataId:dataId},          
          dataType: 'json',
          	complete:function(data) {
			 	listInvited();
          	}          
 		});	
	},

	addEventGmail:function(){
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/generateIdMeeting/',
          data: {},          
          dataType: 'json',
          	complete:function(data) {
				location.href = GLOBAL_DATA.APP_BASE+'/meetings/create_meeting_gmail/';			 	 
          	}          
 		});			
	},

	addEventGmailFollowing:function(){
		location.href = GLOBAL_DATA.APP_BASE+'/meetings/addEventInGmailMeetingFollowing/';
	}, 

	showPassword:function(){
		$('#pass-meeting').prop('type', 'text'); 
	}, 

	hidePassword:function(){
		$('#pass-meeting').prop('type', 'password'); 
	}, 


	saveMeetingWithoutSync:function(){
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/addMeetingWithoutSync/',
          data: {},          
          dataType: 'json',
          	beforeSend: function(e){
                $("body").find("p.errorP").show();
                $("body").find(".btn-auth-gmail").prop('disabled', true); 
                $("body").find(".btn-outlook").prop('disabled', true); 
                $("body").find(".btn-add-meetings-data-without-sync").prop('disabled', true); 
            },
          	complete:function(data) {
				location.href = GLOBAL_DATA.APP_BASE+'/meetings/index/';			 	 
          	}          
 		});	
	},

	saveMeetingFollowingWithoutSync:function(){
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/addMeetingFollingWithoutSync/',
          data: {},          
          dataType: 'json',
          	beforeSend: function(e){
                $("body").find("p.errorP").show();
                $("body").find(".btn-auth-gmail-followings-meetings").prop('disabled', true); 
                $("body").find(".btn-outlook-following").prop('disabled', true); 
                $("body").find(".btn-add-meetings-followings-data-without-sync").prop('disabled', true); 
            },
          	complete:function(data) {
				location.href = GLOBAL_DATA.APP_BASE+'/meetings/index/';			 	 
          	}          
 		});	
	}, 

	initGeoComplete:function(){
     	$("#MeetingLocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo"
        });
        $('#MeetingLocation').change(function(){  
     		$("#MeetingLocation").trigger("geocode");
     	});
	},

	showContentTopics:function(){
		var option = $('.meeting-type').val(); 
	  	if(option == "Reunión"){ 
	  		$("#content-agend").show();
	  		$("#show-content-date-meetings").hide();
	  		$("#meeting-normal").show();
  			$("#meeting-following").hide();
  			$("body").find('.topic-time').removeClass('time-duration'); 
	  		Topics.deleteSessionMeetingsFollowing();
	  	} else if (option == "Reunión de seguimiento") {	  	 
		 	$("#show-content-date-meetings").show();
		 	$("#content-agend").hide();
		 	$("#meeting-normal").hide();			  		
		 	$("#meeting-following").show();
		 	$("body").find('.topic-time').addClass('time-duration');
		 	$("body").find(".time-duration").timingfield({ 
		  		maxHour:        23,		 
		  		width:          280,		 
		  		hoursText:      'H',		 
		  		minutesText:    'M' 
			}); 
			$( ".timingfield_seconds" ).hide();   
	  	}		  
	},

	searchClients:function(){
		$("body").on("change", "#MeetingTeamId", function () {
			var teamId = $(this).val();
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/meetings/search_teams_clients/',
	          data: {teamId:teamId},          
	          dataType: 'html', 
	          	success:function(data) {
				 	$("#show_content_client").html(data);
				 	listInvited();		 	 
	          	}          
	 		});	
		}); 
	},

	cleanClients:function(){
		$("body").on("change", "#MeetingClientId", function () { 
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/meetings/clean_clients_assistants/',
	          data: {},          
	          dataType: 'json', 
	          	success:function(data) { 
				 	listInvited();		 	 
	          	}          
	 		});	
		}); 
	} 
}

Topics.initElements();
