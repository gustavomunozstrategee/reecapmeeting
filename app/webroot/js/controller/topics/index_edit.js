var meetingId = $("#MeetingId").val();
var teamId    = $("#MeetingTeamId").val();
var clientId  = $("#MeetingClientId").val();


jQuery(document).ready(function($) { 
	Functions.listInviteds();  
});
 
Functions = {
	initElements:function(){
		Functions.deleteAssistantNotSave();   
		Functions.storeAssistantsActual(); 
	 	modalityOption = $("#MeetingMeetingRoom").val(); 	 		        		
	 	Functions.fieldLocation(modalityOption);	
		Functions.listTags();
		Functions.findInvited();
		Functions.deleteInvited();
		Functions.initTimerToTopics(); 
		Functions.dateTimeMeeting(); 
		//Functions.initGeoComplete(); 		
		Functions.showFieldLocationWithModalityMeeting();		  
		Functions.showPassword();		  
		Functions.hidePassword();		  
		Functions.showFieldPasswordMeeting();		  
		Functions.generatePassword();		  
		Functions.authMeeting();		  
		Functions.authGmailApi();		  
		Functions.saveSessionInfoMeetingOutlook();
		Functions.searchClients();
		Functions.cleanClients();   	  
	}, 

	//BUSCAR TAGS DEL SISTEMA Y PONER POR DEFECTO LOS DE LA REUNIÓN
	listTags:function(){ 
		var objectMeetingId = {meeting_id : meetingId};
		$.getJSON(GLOBAL_DATA.APP_BASE+'/tags/list_tags_meeting_edit', objectMeetingId, function(json, textStatus) { 
			$('.tagEdit').tagEditor({
		   		autocomplete: {
			       delay: 0, // show suggestions immediately
			       position: { collision: 'flip' }, // automatic menu position up/down
			       source: json.tagsJson
		   		},
		   		initialTags: json.tagsOfMeeting,
		   		forceLowercase: false,
		   		placeholder: ''
			});	
		});
	},

	//BUSCAR INVITADOS EN EDITAR REUNION
	findInvited:function(){
		$('#invited').keyup(function(){ 
			ELEMENT = $(this);
			Functions.searchInviteds(ELEMENT);
		}); 
	},

	//MOSTRAR RESULTADOS DE LA BUSQUEDA
	searchInviteds:function(){
		$('#suggestions').css({'width': '100%','height': 'auto','overflow':'auto'}); 
		var teamId     = $("#MeetingTeamId").val();
		var clientId   = $("#MeetingClientId").val();
		var invited    = ELEMENT.val();        
        var dataString = {invited:invited, clientId:clientId, teamId:teamId};
        $.ajax({
            type: "POST",
            url: GLOBAL_DATA.APP_BASE+'/assistants/searchInvited',
            data: dataString,
            success: function(data) { 
                //Escribimos las sugerencias que nos manda la consulta
                $('#suggestions').fadeIn(1000).html(data);
                //Al hacer click en alguna de las sugerencias
                $('.inviteds').on('click', function(){
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id       = $(this).attr('id');
                    var typeUser = $(this).attr('data-name'); 
                    Functions.addInvited(id, typeUser, meetingId); 
                    //Editamos el valor del input con data de la sugerencia pulsada
                    $('#invited').val(""); 
                    //Hacemos desaparecer el resto de sugerencias 
                    $('#suggestions').hide();
                });              
            }
        });
	},

	//AÑADIR INVITADO A LA REUNIÓN
	addInvited:function(id, typeUser, meetingId){
		$.ajax({
            type: "POST",
            url: GLOBAL_DATA.APP_BASE+'/assistants/add_users_invited_meeting_edit',
            data: {id:id, typeUser:typeUser, meetingId:meetingId},
            dataType: 'json',
    	 	beforeSend: function(e){
                $("body").find("p.loadEditAssistants").show(); 
            },
            complete: function(data) { 	
            	$("body").find("p.loadEditAssistants").hide(); 			 
				Functions.listInviteds();           
			 	$('#suggestions').hide();
            }
        }); 
	},

	//ELIMINAR INVITADO DE UNA REUNIÓN
	deleteInvited:function(){
		$("body").on("click", ".delete-assistant-edit-meeting", function () {
			$(this).closest('tr').remove();
			var assistantId = $(this).attr('data-id');
			$.ajax({
	            type: "POST",
	            url: GLOBAL_DATA.APP_BASE+'/assistants/add_In_Session_Assistant_Delete',
	            data: {assistantId:assistantId, meetingId:meetingId},
	            dataType: 'json',
	            complete: function(data) { 
	            	Functions.listInviteds();
	             	$('#suggestions').hide();         
	            }
        	}); 
		});
	}, 
 
	initTimerToTopics:function(){
		var COUNT = 0;	 
		var
	    gradeTable    = $('#grade-table'),
	    gradeBody     = gradeTable.find('tbody'),
	    gradeTemplate = _.template($('#grade-template').remove().text()),
	    numberRows    = gradeTable.find('tbody > tr').length;
		gradeTable
	    .on('click', 'a.add', function(e) {
	        e.preventDefault();
	        COUNT++; 
	        $(gradeTemplate({key: numberRows++}))
	            .hide()
	            .appendTo(gradeBody)
	            .fadeIn('fast');
	         	var last_id = $('input[type="text"]:last').attr('id',COUNT);
	     	$(".time-duration").timingfield({ 
	  			maxHour:        23,		 
	  			width:          263,		 
	  			hoursText:      'H',		 
	  			minutesText:    'M' 
			}); 
			$( ".timingfield_seconds" ).hide();
	    })
	    .on('click', 'a.remove', function(e) {
	        e.preventDefault();
	        $(this)
	            .closest('tr')
	            .fadeOut('fast', function() {
	                $(this).remove();
	            });
	    });
	    if (numberRows === 0) {
	        gradeTable.find('a.add').click();
	    }
		$(".time-duration").timingfield({ 
		  	maxHour:        23,		 
		  	width:          263,		 
		  	hoursText:      'H',		 
		  	minutesText:    'M' 
		});    
		$( ".timingfield_seconds").hide();
	},
 
	dateTimeMeeting: function(){
		$('#datetimepickerStart>input').daterangepicker({
			"singleDatePicker": true,
    		"timePicker": true,
    		"timePicker24Hour": true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});
		$('#datetimepickerEnd>input').daterangepicker({
			"singleDatePicker": true,
    		"timePicker": true,
    		"timePicker24Hour": true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});
	},

	fieldLocation:function($modalityOption){
		if (modalityOption == 1) { 
            Functions.showFieldLocation(); 
        } else {
        	$("#MeetingLocation").attr("disabled", true);
        	$('#location-meeting').hide();
        }
	}, 

	showFieldLocation:function(){
 		$('#location-meeting').show();
 		$("#MeetingLocation").attr("disabled", false);
	},

	showFieldLocationWithModalityMeeting:function(){
		$('.select-location').change(function(){
	 	 	modalityOption = $("#MeetingMeetingRoom").val();
			Functions.fieldLocation(modalityOption);
		});		
	},

	initGeoComplete:function(){
     	$("#MeetingLocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo"
        });
     	$('#MeetingLocation').change(function(){  
     		$("#MeetingLocation").trigger("geocode");
     	});
	},

	showPassword:function(){
		$("body").on("click", ".show-password", function () {  
			$('#pass-meeting').prop('type', 'text'); 
			$(".show-password").hide();
			$(".hide-password").show();
		}); 
	}, 

	hidePassword:function(){
		$("body").on("click", ".hide-password", function () {  
			$('#pass-meeting').prop('type', 'password');
			$(".show-password").show();
			$(".hide-password").hide();
		}); 		 
	}, 

	showFieldPasswordMeeting:function(){
		$('.password-meeting').change(function(){  
		 	publicMeeting  = $(".public-meeting").val();
	 	 	passwordOption = $(".password-meeting").val();
			Functions.fieldPasswordMeeting(passwordOption,publicMeeting); 
		});
		$('.public-meeting').change(function(){
		 	$('#content-password').hide();
		 	$("#pass-meeting").attr("disabled", true); 
		});
		if($(".password-meeting").is(':checked')) {  
		    $('#content-password').show();
			$("#pass-meeting").attr("disabled", false);   
    	} else {  
    	    $('#content-password').hide();
			$("#pass-meeting").attr("disabled", true);   
    	}   
	},

	fieldPasswordMeeting:function(passwordOption, publicMeeting){
		if (passwordOption == 2) {
            $('#content-password').show();
 			$("#pass-meeting").attr("disabled", false); 
        } else if (publicMeeting == 1){
        	$('#content-password').hide();
        	$("#pass-meeting").attr("disabled", true);
        }
	},

	generatePassword:function(){
		$('.generate-password').click(function(){
			$.ajax({
	            type: "POST",
	            url: GLOBAL_DATA.APP_BASE+'/meetings/generatePasswordMeeting',
	            data: {},
	            dataType: 'json',
	            success: function(data) { 
	  			 	$("#pass-meeting").val(data);             
	            }
	        });	 		 
		});
	},

	authMeeting:function(){
		$("body").on("click", ".btn-auth-meeting-edit", function () { 
			var formData = $("#MeetingEditForm").serialize(); 
			$.ajax({
	            type: "POST",
	            url: GLOBAL_DATA.APP_BASE+'/meetings/validate_info_meeting_edit',
	            data: formData,
	            dataType: 'json',
	            success: function(data) { 
  			 	 	if(data.state == false && data.validation == false){
          				swal({type:"error", title:copys_js.error, text:data.message});
	          		} else if (data.validation == true) {
	          			var messages = data.message.join('\r\n');
	          			swal({type:"error", title:copys_js.error, text:messages});
	          		} else if (data.state == false) {
	          			swal({type:"error", title:copys_js.error, text:data.message});
	          		}  else if (data.state == true) {
	          			$("#AutenticationEmailMeetingEdit").modal("show"); 
	          		}           
	            }
	        }); 
		});
	},
	
	authGmailApi:function(){
		$("body").on("click", ".btn-auth-gmail-edit-meeting", function () { 
			location.href = GLOBAL_DATA.APP_BASE+'/meetings/edit_meeting_gmail/';			 
		}); 
	},
 
	saveSessionInfoMeetingOutlook:function(){
		$("body").on("click", ".auth-outlook-edit-meeting", function () { 
			var formData = $("#MeetingEditForm").serialize();
			$.ajax({
	            type: "POST",
	            url: GLOBAL_DATA.APP_BASE+'/meetings/save_session_edit_meeting',
	            data: formData,
	            dataType: 'json',
	            success: function(data) { 
  			            
	            }
	        });
		}); 
	},

	listInviteds:function(){
		$.ajax({
	        type: "POST",
	        url: GLOBAL_DATA.APP_BASE+'/assistants/list_assistants_edit',
	        data: {meetingId:meetingId},
	        dataType: 'html',
	        beforeSend: function(e){
                $("body").find("p.errorP").show(); 
            },
	        success:function(data) { 
	        	$("body").find("p.errorP").hide(); 
	  			$('#show-content-inviteds').html(data);
	  			$('#suggestions').hide();	            
	        }
	    }); 
	}, 

    storeAssistantsActual:function(){
		$.ajax({
	        type: "POST",
	        url: GLOBAL_DATA.APP_BASE+'/assistants/store_assistant_actual',
	        data: {meetingId:meetingId},
	        dataType: 'json',
	        beforeSend: function(e){},
	        complete: function(data){	        	 
	        	
	        }
	    });
	},

	deleteAssistantNotSave:function(){
		$.ajax({
	        type: "POST",
	        url: GLOBAL_DATA.APP_BASE+'/assistants/delete_asistants_not_save',
	        data: {meetingId:meetingId},
	        dataType: 'html',
	        beforeSend: function(e){},
	        complete: function(data){}
	    });
	}, 

	searchClients:function(){
		$("body").on("change", "#MeetingTeamId", function () {
			var teamId = $(this).val();
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/meetings/search_teams_clients/',
	          data: {teamId:teamId},          
	          dataType: 'html', 
	          	success:function(data) {
				 	$("#show_content_client").html(data);
				 	Functions.listInviteds();		 	 
	          	}          
	 		});	
		}); 
	},

	cleanClients:function(){
		$("body").on("change", "#MeetingClientId", function () { 
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/meetings/clean_clients_assistants_edit/',
	          data: {},          
	          dataType: 'json', 
	          	success:function(data) { 
				 	Functions.listInviteds();		 	 
	          	}          
	 		});	
		}); 
	}   
}
Functions.initElements();