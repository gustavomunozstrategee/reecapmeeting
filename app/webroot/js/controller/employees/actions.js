AppEmployees = {

    listar:function(){ 

  	var employeeId = $("#employee-id").val();

 		$.ajax({ 

          type: "POST",

          url: GLOBAL_DATA.APP_BASE+'/commitments/my_commitments/'+employeeId,

          data: {},       

          dataType: 'html', 

          	success:  function(data) { 

		    	$('#show-content-commitments').html(data);		    	     

  		 	}         

 		}); 

 	},



  listarContacsEmployee:function(){ 

    var employeeId = $("#employee-id").val();

    $.ajax({ 

          type: "POST",

          url: GLOBAL_DATA.APP_BASE+'/contacs/contacts_employee/'+employeeId,

          data: {},       

          dataType: 'html', 

            success:  function(data) { 

          $('#show-content-employees-contacs').html(data);               

        }         

    }); 

  },



  InitSelect2:function(){ 

     $("#SearchClientId").select2();

   },



	initElements:function(){ 

		$(document).ready(function(){

      if (GLOBAL_DATA.action == 'view') {

        AppEmployees.listar();    
        AppEmployees.listarContacsEmployee();  

      } else {
        AppEmployees.InitSelect2();
      }    		

		}); 

	}



} 

AppEmployees.initElements();