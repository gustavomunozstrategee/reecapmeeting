EDIT = {
	listar:function(){ 
    	var userId = $("#user-id").val();
 		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/commitments/commitments_employee/'+userId,
          data: {},       
          dataType: 'html', 
          	success:  function(data) { 
		    	$('#show-content-commitments-users').html(data);		    	     
  		 	}         
 		}); 
 	},

 	listarMyContac:function(){ 
    	var userId = $("#user-id").val();
 		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/contacs/my_contacs/'+userId,
          data: {},       
          dataType: 'html', 
          	success:  function(data) { 
		    	$('#show-content-conctacs-users').html(data);		    	     
  		 	}         
 		}); 
 	},

 	listarStatisticalNumber:function(){ 
    	var userId = $("#user-id").val();
 		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/contacs/statistical_numbers/'+userId,
          data: {},       
          dataType: 'html', 
          	success:  function(data) { 
		    	$('#show-content-conctacs-numbers').html(data);		    	     
  		 	}         
 		}); 
 	},

	initElement: function() {
		//EDIT.listar(); 
		//EDIT.listarMyContac(); 
		//EDIT.listarStatisticalNumber(); 
		EDIT.clickPassword();
		EDIT.updatePhoto();
	}, 

	clickPassword:function() {
		$('#btn_password').click(function(event) {
			var contraActual    = $('#password').val();
			var contraNueva     = $('#passwordn').val();
			var contraconfirmar = $('#re_passwordn').val();
			if (contraActual != '' && contraNueva != '' && contraconfirmar != '') {
				if(contraNueva.length > 5){
					$("#password").removeClass( "parsley-error" );
						$("#passwordn").removeClass( "parsley-error" );
						$("#re_passwordn").removeClass( "parsley-error" );
					if (contraNueva == contraconfirmar) {
						$.ajax({ 
				          type: "POST",
				          url:  GLOBAL_DATA.APP_BASE+'/users/update_password/',
				          data: {contraActual:contraActual,contraNueva:contraNueva,contraconfirmar:contraconfirmar},          
				          dataType: 'json',
				          	success:  function(data) { 
				          		if(data.state == true){
				          			swal({type:"success", title:copys_js.exito, text:data.message});
				          			$('#modalPassword').modal().hide(); 
			          			 	$('#password').val('');
			          			  	$('#passwordn').val('');
			          			   	$('#re_passwordn').val(''); 
			          			   	setTimeout(function() {
			          			   		location.href = GLOBAL_DATA.APP_BASE+'/users/edit/';
			          			   	}, 1000); 
				          		} else {
			          			 	swal({type:"error", title:copys_js.error, text:data.message});
				          		}           
				          	}          
				 		}); 
					} else {
						swal({type:"error", title:copys_js.error, text:copys_js.new_passwords_do_not_match});
						$('#re_passwordn').val('');
					}
				} else {
					swal({type:"error", title:copys_js.error, text:copys_js.new_password_max_characters});
					$('#passwordn').val('');
					$('#re_passwordn').val('');
				}
			} else {
				if (contraconfirmar != '') {
					$("#re_passwordn").removeClass( "parsley-error" );
				} else {
					swal({type:"info", title:"", text:copys_js.confirm_your_password});
					$("#re_passwordn").addClass( "parsley-error" );
				}
				if (contraNueva != '') {
					$("#passwordn").removeClass( "parsley-error" );
				} else {
					swal({type:"info", title:"", text:copys_js.enter_new_password});
					$("#passwordn").addClass( "parsley-error" );
				}
				if (contraActual != '') {
					$("#password").removeClass( "parsley-error" );
				} else {
					swal({type:"info", title:"", text:copys_js.enter_current_password});
					$("#password").addClass( "parsley-error" );
				}
			}
		});
	},

	updatePhoto:function(){
		$('#UserImg').change(function() {
			var file = $("#UserImg")[0].files[0];
			var fileName = file.name; 
			EDIT.isImage(function(result){ 
				if(result == true){
				var fileSize = file.size;
				var sizeMB   = (fileSize / (1024*1024)).toFixed(2); 
					if (sizeMB > 10) {
						swal({type:"error", title:copys_js.error, text:copys_js.archive_size_high_client});
						return $('#UserImg').val('');
					}
				} else {
					swal({type:"error", title:copys_js.error, text:copys_js.allow_only_images});
					return $('#UserImg').val('');
				}
				if($("#UserImg")[0]){
					EDIT.showProfileImagePreview($("#UserImg")[0]);
				}
			});  
		});
	},

	showProfileImagePreview:function(input){
	    if (input.files && input.files[0]) {
	    	if (window.File && window.FileReader && window.FileList && window.Blob) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $('.preview-profile-image').attr('src', e.target.result).fadeIn('slow');
		        }
		        reader.readAsDataURL(input.files[0]);
	    	}
	    }
	}, 

	isImage:function(response) {  
	  	var formData = new FormData($("#UserEditForm")[0]);
	    $.ajax({ 
	      type: "POST",
	      contentType:false,
	      cache: false,
	      processData:false,
	      url: GLOBAL_DATA.APP_BASE+'/users/verify_img/',
	      data:  formData,
	      dataType: 'json',
	      success:  function(data) {
	        response(data);
	      }
	    }); 
  	}

} 