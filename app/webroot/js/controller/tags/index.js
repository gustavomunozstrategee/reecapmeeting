$(document).ready(function() {
	$.getJSON(GLOBAL_DATA.APP_BASE+'/tags/listTags', function(json, textStatus) { 
		$('#tag').tagEditor({
	   		autocomplete: {
		       delay: 0, // show suggestions immediately
		       position: { collision: 'flip' }, // automatic menu position up/down
		       source: json
	   		},
	   		forceLowercase: false,
	   		placeholder: ''
		});	
	});
});