Functions = {
	initElements:function(){
		Functions.loadTeamsClient();  
		Functions.typeUserChoose();  
		Functions.searchUserToAssigned();  
		Functions.saveUserAssigned(); 
		Functions.selectAllValues(); 
		Functions.completeStep(); 
		$("#UserTeamTeamId").select2(); 
	}, 

	loadTeamsClient:function(){
		$("body").on("change", "#UserTeamClientId", function () { 
		 	var clientId   = $(this).val();
			 $.ajax({
	            type: "POST",
	            url: GLOBAL_DATA.APP_BASE+'/user_teams/load_team_client',
	            data: {clientId: clientId},
	            dataType: 'html',
	            success: function(data) { 	 
	            	$("body").find("#team_field").html(data);  
	            }
	        }); 
		});
	},

	typeUserChoose:function(){
		$("body").on("change", "#UserTeamTypeUser", function () {
			var typeUser = $(this).val();
			Functions.checkDataCreateds(typeUser, function(result) { 
				if(result.state == false){
					location.href = result.url;
				} else {
					$.ajax({
			            type: "POST",
			            url: GLOBAL_DATA.APP_BASE+'/user_teams/load_positions',
			            data: {typeUser: typeUser},
			            dataType: 'html',
			            success: function(data) { 	 
			            	$("body").find("#show_process_assigned").html(data); 
			            	$("#UserTeamTeamId").select2(); 

			            	$('#UserTeamClientId').select2(); 
			            }
			        });  
				} 
			});
		});
	},

	checkDataCreateds:function(typeUser, response){
		$.ajax({ 
          	type: "POST",
          	url: GLOBAL_DATA.APP_BASE+'/user_teams/check_data_createds/', 
          	data: {typeUser:typeUser},          
          	dataType: 'json',
        	success:  function(data) {
        		response(data);
        	}          
        }); 
	},

	searchUserToAssigned:function(){
		$("body").on("click", ".assign_user_team", function () { 
		 	var typeUser = $("#UserTeamTypeUser").val();
			var email    = $("#UserTeamEmail").val();
			var teamId   = [$("#TeamSelection").val()];
			var data 	 = {email:email, teamId:teamId, type_user:typeUser};
			if ($("#UserTeamClientId").val() != '') {
				data.clientId = $("#UserTeamClientId").val();					
			} else {
				swal({type:"error", title:copys_js.error, text:copys_js.client_required});
				return false;
			}
			$.ajax({
	            type: "POST",
	            url: GLOBAL_DATA.APP_BASE+'/user_teams/search_user',
	            data: data,
	            dataType: 'json',
	    	 	beforeSend: function(e){ 
	                $(".assign_user_team").prop("disabled", true);
	            },
	            success: function(data) { 	 
	            	if(data.state == false){
	            		$("#save_user_assigned").hide();
			 	 	 	$(".assign_user_team").prop("disabled", false);
		 	 	 		var messages = data.message.join('\r\n');
			 	 	 	swal({type:"error", title:copys_js.error, text:messages});
	            	} else {
	            		$("#save_user_assigned").show();
	            		$(".assign_user_team").prop("disabled", false);
	            	}
            	}
	        }); 
		}); 
	},

	saveUserAssigned:function(){
		$("body").on("click", ".save_user", function (e) { 
			e.preventDefault();
			var formData = $("#UserTeamAddForm").serialize();
			$.ajax({
	            type: "POST",
	            url:  GLOBAL_DATA.APP_BASE+'/user_teams/save_user',
	            data: formData,
	            dataType: 'json',
	    	 	beforeSend: function(e){ 
	                $(".save_user").prop("disabled", true); 
	            },
	            success: function(data) { 	 
	            	if(data.state == false){
	            		$(".save_user").prop("disabled", false); 
	            		var messages = data.message.join('\r\n');
			 	 	 	swal({type:"error", title:copys_js.error, text:messages});
	            	} else {
	            		$(".save_user").prop("disabled", true); 
	            		$("#UserTeamAddForm").submit();
	            		$('#preloader').show();
	            	}
            	}
	        }); 
		});
	},

	selectAllValues:function(){
		$("body").on("click", "#UserTeamSelectAll", function (e) {  
		    if($("#UserTeamSelectAll").is(':checked') ){
		        $("#UserTeamTeamId > option").prop("selected","selected");
		        $("#UserTeamTeamId").trigger("change");
		    }else{
		        $("#UserTeamTeamId > option").removeAttr("selected");
	         	$("#UserTeamTeamId").trigger("change");
		     }
		}); 
	},

	completeStep:function(){
		$("body").on("click", "#FinishStep", function (e) { 
			var url = $(this).attr("href");
			e.preventDefault();
			$.ajax({
	            type: "POST",
	            url:  GLOBAL_DATA.APP_BASE+'/user_teams/complete_step',
	            data: {},
	            dataType: 'json',
	    	 	beforeSend: function(e){ 
	                $("#stepCompleted").prop("disabled", true); 
	            },
	            success: function(data) { 	 
	            	location.href = url;
            	}
	        }); 
		});
	}

} 


Functions.initElements();