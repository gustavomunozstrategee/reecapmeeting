EDIT = {
  initElement: function() {
    EDIT.updatePhoto();
    EDIT.listarProjectsClients();
  },
  
  listarProjectsClients:function(){ 
    var clientId = $("#client-id").val();
    $.ajax({ 
      type: "POST",
      url: GLOBAL_DATA.APP_BASE+'/projects/projects_clients/'+clientId,
      data: {},       
      dataType: 'html', 
      success:  function(data) { 
        $('#show-content-projects-users').html(data);               
      }         
    }); 
  },

  updatePhoto:function() {
    $('#ClientImg').change(function() { 
      var file = $("#ClientImg")[0].files[0];
      var fileName  = file.name; 
      EDIT.isImage(function(result){          
        if(result == true)
        {
          var fileSize = file.size;
          var sizeMB = (fileSize / (1024*1024)).toFixed(2);
          if (sizeMB > 10) {
            swal({type:"error", title:copys_js.error, text:copys_js.archive_size_high_client});
            $('#ClientImg').val('');
          }
        }else{
          swal({type:"error", title:copys_js.error, text:copys_js.allow_only_images});
          $('#ClientImg').val('');
        } 
        if($("#ClientImg")[0]){
          EDIT.showProfileImagePreview($("#ClientImg")[0]);
        }
      }); 
    });
  },

  showProfileImagePreview:function(input){
    if (input.files && input.files[0]) {
      if (window.File && window.FileReader && window.FileList && window.Blob) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('.preview-customer-image').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
  },
  
  isImage:function(response) { 
    if(GLOBAL_DATA.action == "add"){
      var formData = new FormData($("#ClientAddForm")[0]);
    } else {
      var formData = new FormData($("#ClientEditForm")[0]);
    } 
    $.ajax({ 
      type: "POST",
      contentType:false,
      cache: false,
      processData:false,
      url: GLOBAL_DATA.APP_BASE+'/clients/verify_img/',
      data:  formData,
      dataType: 'json',
      success:  function(data) {
        response(data);
      }
    }); 
  }
}