 
$(document).ready(function() { 
	if(GLOBAL_DATA.ParameterSearchDashboad != null){	 
		$("input[type=checkbox]").prop('checked', true);  
	}
});

var EDIT = {
	viewSearch: function(){
		EDIT.allSelectCheckbox();
		EDIT.btnLimpriarFiltros();
	},

	allSelectCheckbox: function(){
		$("#SearchParametrosTodo").change(function () {
		    if ($(this).is(':checked')) {
		        $("input[type=checkbox]").prop('checked', true);
		    } else {
		    	$("input[type=checkbox]").prop('checked', false);
		    }
		});

		$("#SearchParametrosProject").change(function () {
		    if (!$(this).is(':checked')) {
		    	$("#SearchParametrosProject").prop('checked', false);
		    	$("#SearchParametrosTodo").prop('checked', false);
		    }
		});

		$("#SearchParametrosClient").change(function () {
		    if (!$(this).is(':checked')) {
		    	$("#SearchParametrosClient").prop('checked', false);
		    	$("#SearchParametrosTodo").prop('checked', false);
		    }
		});

		$("#SearchParametrosUserTeam").change(function () {
		    if (!$(this).is(':checked')) {
		    	$("#SearchParametrosUserTeam").prop('checked', false);
		    	$("#SearchParametrosTodo").prop('checked', false);
		    }
		}); 

		$("#SearchParametrosContac").change(function () {
		    if (!$(this).is(':checked')) {
		    	$("#SearchParametrosContac").prop('checked', false);
		    	$("#SearchParametrosTodo").prop('checked', false);
		    }
		});

		$("#SearchParametrosCommitment").change(function () {
		    if (!$(this).is(':checked')) {
		    	$("#SearchParametrosCommitment").prop('checked', false);
		    	$("#SearchParametrosTodo").prop('checked', false);
		    }
		});

		$("#SearchParametrosMeeting").change(function () {
		    if (!$(this).is(':checked')) {
		    	$("#SearchParametrosMeeting").prop('checked', false);
		    	$("#SearchParametrosTodo").prop('checked', false);
		    }
		});
	},

	btnLimpriarFiltros: function(){
		$('#btn_limpiarFiltros').click(function(event) {
			$("input[type=checkbox]").prop('checked', false);
			$('#SearchFrase').val('');
			$('#SearchExcluir').val('');
			$('#SearchTeamId').val('');
		});
	}
}