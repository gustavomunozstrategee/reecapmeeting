var commitmentsIds = []; 

function verifyCommitment(){
	$.ajax({ 
      type: "POST",
      url: GLOBAL_DATA.APP_BASE+'/commitments/verify_commitment_to_done/',
      data: {},          
      dataType: 'json',
      	success:  function(data) { 
      		if(data == true){ 
  				$("#markAsReadCommitment").modal("show");  
      		} 
      	}          
	});
}

function listNumberCommitments(){
	$.ajax({ 
      type: "POST",
      url: GLOBAL_DATA.APP_BASE+'/commitments/number_commitments/',
      data: {},          
      dataType: 'json',
      	success:  function(data) { 
      		$("#com-created").html(data.createds);
			$("#com-completed").html(data.completeds);
			$("#com-pendings").html(data.pendings);
			$("#com-expireds").html(data.expireds);
      	}          
	});
}

function validateCommitmentSelected(){
	$.ajax({ 
      type: "POST",
      url: GLOBAL_DATA.APP_BASE+'/commitments/read_commitments_select/',
      data: {},          
      dataType: 'json',
      	success:  function(data) {  
      		if(data == true){  
  				$("#marcar-realizado-compromiso").show();
      		} else {
  				$("#marcar-realizado-compromiso").hide();
      		}
      	}          
	});
}

var Pages = {

	initElements: function() {
		$("body").on("click", ".commitments-created", function () { 
			Pages.getCommitmentsCreateds(); 
		});

		$("body").on("click", ".commitments-completed", function () { 
			Pages.getCommitmentsCompleteds(); 
		});

		$("body").on("click", ".commitments-pendings", function () { 
			Pages.getCommitmentsPendings(); 
		});

		$("body").on("click", ".commitments-expireds", function () { 
			Pages.getCommitmentsExpireds(); 
		});

		$("body").on("click", ".btn-sync-calendar", function () { 
			Pages.deleteCredential(); 
			Pages.syncCalendarGmail(); 
		});
		validateCommitmentSelected();
		listNumberCommitments();
		Pages.markAsDoneCommitment();
		Pages.selectCommitmentId();
		Pages.showModalConfirmationCommitment();
		Pages.saveCommitementToDone(); 
		Pages.changePlanRecommend(); 
		Pages.completeCommitment();
	},

	getCommitmentsCreateds:function(){
		$.ajax({ 
	      type: "POST",
	      url: GLOBAL_DATA.APP_BASE+'/commitments/commitments_createds/',
	      data: {},          
	      dataType: 'html',
	      	success:  function(data) {       		 
				$("#show-content-pages-commitments").html(data);
				Pages.removeClassActive();
				$("#commitments-created").addClass("active");        		 
	      	}          
		}); 
	},

	getCommitmentsCompleteds:function(){
		$.ajax({ 
	      type: "POST",
	      url: GLOBAL_DATA.APP_BASE+'/commitments/commitments_completeds/',
	      data: {},          
	      dataType: 'html',
	      	success:  function(data) {       		 
				$("#show-content-pages-commitments").html(data);
				$("#count-commitement-completeds").remove();				
				Pages.removeClassActive();
				$("#commitments-completed").addClass("active");        		 
	      	}          
		}); 
	},

	getCommitmentsPendings:function(){
		$.ajax({ 
	      type: "POST",
	      url: GLOBAL_DATA.APP_BASE+'/commitments/commitments_not_done/',
	      data: {},          
	      dataType: 'html',
	      	success:  function(data) {       		 
				$("#show-content-pages-commitments").html(data);
				$("#count-commitement-dot-done").remove();
				Pages.removeClassActive();
				$("#commitments-pendings").addClass("active");        		 
	      	}          
		}); 
	},

	getCommitmentsExpireds:function(){
		$.ajax({ 
	      type: "POST",
	      url: GLOBAL_DATA.APP_BASE+'/commitments/commitments_expireds/',
	      data: {},          
	      dataType: 'html',
	      	success:  function(data) {       		 
				$("#show-content-pages-commitments").html(data);
				Pages.removeClassActive();
				$("#commitments-expireds").addClass("active");        		 
	      	}          
		}); 
	},

	removeClassActive:function(){
		$("#commitments-created").removeClass("active");
		$("#commitments-completed").removeClass("active"); 
		$("#commitments-pendings").removeClass("active"); 
	    $("#commitments-expireds").removeClass("active"); 
	},

	completeCommitment: function(){
		$(".terminateCommitment").click(function(event) {
			var dateId = $(this).data("id");
				event.preventDefault();
				swal({
			        title: copys_js.btn_confirm,
			        text: copys_js.complete_commitment,
			        type: "warning",
			        showCancelButton: true,
			        confirmButtonColor: "#00a65a",
			        confirmButtonText: copys_js.btn_confirm,
			        cancelButtonText: copys_js.btn_cancel,
			        closeOnConfirm: true,
			        closeOnCancel: true
			    }, function (isConfirm) {
			        if (isConfirm) {
			            Utilities.ajaxRequestPost(GLOBAL_DATA.APP_BASE+'/commitments/complete_commitment',{dataId:dateId},function(response){
			            	Utilities.showLoading(false);
			            	location.reload();
			            })
			        }
			});
		});
	},

	markAsDoneCommitment:function(){
		$("body").on("click", ".btn-mark-as-read", function () {
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/commitments/mark_commitment/',
	          data: {},          
	          dataType: 'json',
         	 	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find(".btn-mark-as-read").prop('disabled', true); 
                },
	          	success:  function(data) {  
          		 	if (data.state == true) {
          		 		$("body").find("p.errorP").hide();
          				swal({type:"info", title:copys_js.informacion, text:data.message});
          				$("body").find(".btn-mark-as-read").prop('disabled', false);
          				$("#markAsReadCommitment").modal("hide"); 
	          		}
	          	}          
	 		});
		});	
	},

	selectCommitmentId:function(){	 		
		$("body").on("click", ".chk-commitments", function () { 
			var commitmentsIds = $(this).val(); 
			if ($(this).is(':checked')) {         
				var add = true;
	           	$.ajax({ 
		          type: "POST",
		          url: GLOBAL_DATA.APP_BASE+'/commitments/save_commitment_id_selected/',
		          data: {commitmentsIds:commitmentsIds, add:add},          
		          dataType: 'json', 
		          	complete:  function(data) {
		          		validateCommitmentSelected();
		          	}          
		 		});            	
            } else {
            	var edit = false;
            	$.ajax({ 
		          type: "POST",
		          url: GLOBAL_DATA.APP_BASE+'/commitments/save_commitment_id_selected/',
		          data: {commitmentsIds:commitmentsIds, edit:edit},          
		          dataType: 'json', 
		          	complete:  function(data) {
		          		validateCommitmentSelected();
		          	}          
		 		}); 
            }        	
        });  
	},

	showModalConfirmationCommitment:function(){ 
		$("body").on("click", "#marcar-realizado-compromiso", function () { 
			$("#modal-marcar-como-realizado").modal("show"); 
		}); 		
	},

	saveCommitementToDone:function(){
		$("body").on("click", "#btn-save-commitment-done", function () { 
			$.ajax({ 
	          type: "POST",
	          url: GLOBAL_DATA.APP_BASE+'/commitments/save_commitments/',
	          data: {},          
	          dataType: 'json',
         	 	beforeSend: function(e){
                    $("body").find("p.errorP").show();
                    $("body").find("#btn-save-commitment-done").prop('disabled', true); 
                    $("body").find("#btn-cancel-commitment-done").prop('disabled', true); 
                },
	          	success:  function(data) {  
          		 	if (data.state == true) {
          		 		$("body").find("p.errorP").hide();
          				swal({type:"success", title:copys_js.exito, text:data.message});
          				$("body").find("#btn-save-commitment-done").prop('disabled', false);
          				$("body").find("#btn-cancel-commitment-done").prop('disabled', false);
          				$("#modal-marcar-como-realizado").modal("hide"); 
	          		}
	          	},
	          	complete: function(data){
	          		$(".active").click();
	          		validateCommitmentSelected();
	          		listNumberCommitments(); 
	          	}          
	 		}); 
		});
	},

	syncCalendarGmail:function(){
	    location.href = GLOBAL_DATA.APP_BASE+'/meetings/listMeetingGmail/';
	},
 

	deleteCredential:function(){
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/delete_credentials',
          data: {},          
          dataType: 'json',
          	complete:  function(data) {}          
 		});
	},

	changePlanRecommend:function(){ 
		$("body").on('click', '.btn_recomendado', function(event) {
			event.preventDefault();
			var id = $(this).data('id');
			swal({
			    title: copys_js.ten_cuidado,
			    text: copys_js.continue_action,
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: "#DD6B55",
			    confirmButtonText: copys_js.btn_confirm,
			    cancelButtonText: copys_js.btn_cancel,
			},
			function(){
				$.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/changeRecommended',{id:id},function (resultado){
					location.reload();
				});
			});
		});
	}
}

Pages.initElements();