Meetings = { 
	listMeetings:function(){  
	 	$('#my-calendar').fullCalendar({
	 		events : GLOBAL_DATA.APP_BASE+'/meetings/listAllMeeting',
	 		header: {
		        left: 'prev,next',
		        center: 'title',
		        right: 'month,listYear'
		      },
 		 	eventClick:  function(event, jsEvent, view) {  
 		 		console.log(event)

 		 		

            	$('#show-content-meetings-calendar').html(""); 
            	$('#content-meetings').hide();
            	if (event.type == "" || event.type == null) {
            		event.type = copys_js.not_available;
            	} 
    			var htmlDetail   = '';
    			let button       = '';
 		 		var dateActual   = new Date($.now());
 		 		var startMeeting = new Date(event.startDate);
 		 		dateActual 	     = new Date(dateActual); 
 		 	    if(dateActual >= startMeeting && event.email_type == null && event.contac_id == null && GLOBAL_DATA.action == "index"){
 		 			//button = '<a data-toggle="tooltip" data-placement="top" title="'+copys_js.start_meeting+'"  data-id="'+event.id+'" href="'+GLOBAL_DATA.APP_BASE+'/contacs/add'+'" class="btn btn-blue btn-save-meetings-to-contac">' + "<span class='la la-play la-2x' aria-hidden='true'></span>" +'</a>';
 		 			button = '';
 		 		} else {
 		 			button = '';
 		 		}

 		 		$('[data-toggle="tooltip"]').tooltip();    

 		 		$.each(event.permissions, function(index, val) {
 		 			var buttonAction = $("<a></a>");
 		 			 		 			
 		 			$.each(event.permissions[index], function(indexBtn, valBtn) {
 		 				if (indexBtn == "html") {
 		 					buttonAction.html(valBtn);
 		 				}else{
 		 					buttonAction.attr(indexBtn,valBtn); 		 					
 		 				}
 		 			});
 		 			button+=buttonAction.prop("outerHTML");
 		 			

 		 		});
 		 		console.log(button);
    			htmlDetail += "<tr><td class='table-grid-10 td-word-wrap'>" + event.type +	"</td><td class='table-grid-30 td-word-wrap'>" + event.title +	"</td><td class='table-grid-15 td-word-wrap'>" + event.startDate + "</td><td class='table-grid-15 td-word-wrap'>" + event.endDate +	"</td><td class='table-grid-10 td-word-wrap'>"	+  event.location +"</td><td class='table-grid-20 text-center'>"	+  button +"</td></tr>"; 
            	if(htmlDetail != ''){
            		$('#show-content-detail-meeting').html(htmlDetail); 
            		$('#calendarModalDetailMeeting').modal(); 
            		setTimeout(function() {
            			$('[data-toggle="tooltip"]').tooltip();            			
            		}, 1000);
            	}
        	},
    	 	dayClick: function(date, jsEvent, view) {  
    	 		var events   = $('#my-calendar').fullCalendar('clientEvents'); 			 
    	 		var button   = ''; 
    	 		var html     = '';
    	 		for (var i = 0; i < events.length; i++) { 
	 				button = '<a data-id="'+events[i].id+'" class="btn btn-blue btn-xs btn-view-detail-meeting-day">'+copys_js.see_detail+'</a>';
	 		 		if (moment(date).format('YYYY-MM-DD') == moment(events[i].start).format('YYYY-MM-DD')) { 
	 		 			html += 
	 		 			"<tr><td class='td-word-wrap'>" 
	 		 			+ events[i].title + 
	 		 			"</td><td class='td-word-wrap'>" 
	 		 			+ events[i].location +
	 		 			"</td><td class='text-center'>" 
	 		 			+ button +
	 		 			"</td></tr>";
	                }	            	 
        		}
        		$('[data-toggle="tooltip"]').tooltip();    
        		if(html != ''){
	        		$('#content-meetings').show();
	    	 		$('#show-content-meetings-calendar').html(html); 
	            	$('#calendarModal').modal(); 
        		} 
    	 	}
		});

	},
    
	initElements:function(){
		$(document).ready(function(){			
			Meetings.dateTimeMeeting();
			if(GLOBAL_DATA.action == "meetings_invitations" || GLOBAL_DATA.action == "index"){
				Meetings.listMeetings();
			}              
			$(document).on('click', '.btn-sync-calendar', function(){ 
				if(GLOBAL_DATA.action == "meetings_invitations" || GLOBAL_DATA.action == "index"){          
					Meetings.syncCalendarGmail();
				}
	        }); 
	        $("body").on("click", ".btn-view-detail-meeting-day", function () { 
	        	ELEMENT = $(this);
	        	Meetings.showDetailDay(ELEMENT);
	        	$("#calendarModalDetailMeetingDay").modal("show");				 
			}); 
		  	$("body").on("click", ".btn-save-meetings-to-contac", function (event) {
		  		event.preventDefault(); 
	        	ELEMENT = $(this);
	        	Meetings.infoOfMeetingToCreateContac(ELEMENT);	        	 				 
			}); 
		}); 
	}, 

	dateTimeMeeting: function(){
		
		$('#datetimepickerStart>input').daterangepicker({
			"singleDatePicker": true,
    		"timePicker": true,
    		"timePicker24Hour": true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
				// format: moment().format("YYYY-MM-DD HH:mm");
			}
		});
	}, 

	syncCalendarGmail:function(){
		Meetings.deleteCredential();
	    location.href = GLOBAL_DATA.APP_BASE+'/meetings/listMeetingGmail/';
	},

	showDetailDay:function(ELEMENT){
		var dataId =  ELEMENT.attr('data-id');
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/show_detail_day_meeting/',
          data: {dataId:dataId},          
          dataType: 'html',
          	success:  function(data) { 
          		$('#show-content-detail-meeting-days').html(data);
          	}          
 		});	
	},

	infoOfMeetingToCreateContac:function(){
		var dataId =  ELEMENT.attr('data-id'); 
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/createSessionToMeetingContac/',
          data: {dataId:dataId},          
          dataType: 'json',
          	success:  function(data) { 
      		 	if(data == true){
  		 		 	location.href = GLOBAL_DATA.APP_BASE+'/contacs/add/';
      		 	}
          	}          
 		});
	},

	listAssistantsMeeting:function(ELEMENT){
		var meetingId = ELEMENT.attr('data-id');
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/view/'+meetingId,
          data: {},          
          dataType: 'html',
          	success:  function(data) {
          		$("#modalAssistantsMeeting").modal("show") 
  		 	 	$('#show-content-meetings-detail').html(data);
          	}          
 		});
	},

	deleteCredential:function(){
		$.ajax({ 
          type: "POST",
          url: GLOBAL_DATA.APP_BASE+'/meetings/delete_credentials',
          data: {},          
          dataType: 'json',
          	complete:  function(data) {}          
 		});
	}
}

Meetings.initElements();
