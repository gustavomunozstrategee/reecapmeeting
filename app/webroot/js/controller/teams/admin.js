var Team = {
	init: function() {
		this.getPermision(); 
		this.callToClick();
	},
	getPermision: function(){
		$(".adminTeam").click(function(event) {
			event.preventDefault();
			var teamId = $(this).data("id");
			var url    = $(this).data("url");
			Utilities.ajaxRequestPost(url,{"teamId":teamId},function(data){
				Utilities.showLoading(false);
				$("#modalActionTeam").modal("show");
				$(".content-permision").html(data);
			})
			console.log(teamId)
		});
	},
	callToClick:function(){
		$("body").on('click', '.new-message', function(event) {
			event.preventDefault();
			location.href = $(this).data("url")
		});
	}
};

Team.init();