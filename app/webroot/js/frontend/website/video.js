$(".active-modal-fake").click(function(){
    var urlVideo = $(this).attr("data-url");
    if(player == undefined){
        swal({type:"error", title:copys_js.error, text:copys_js.youtube_error});
    	return;
    } 
    player.loadVideoById(urlVideo);
    $(".modal-fake").addClass("show-modal-fake");
});
$(".modal-fake").click(function(){
    player.stopVideo();
    $(".modal-fake").removeClass("show-modal-fake");
});