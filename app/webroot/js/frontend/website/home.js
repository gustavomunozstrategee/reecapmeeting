$(document).ready(function() {

    // $('.slider-plans').slick({
    //     dots:false,
    //     autoplay: true,
    //     autoplaySpeed: 5000,
    //     infinite: true,
    //     adaptiveHeight: true,
    //     slidesToShow: 3,
    //     slidesToScroll: 1,
    //     responsive: [
    //         {
    //           breakpoint: 1024,
    //           settings: {
    //             slidesToShow: 2,
    //             slidesToScroll: 1,
    //             infinite: true,
    //             dots: true
    //           }
    //         },
    //         {
    //           breakpoint: 600,
    //           settings: {
    //             slidesToShow: 2,
    //             slidesToScroll: 1
    //           }
    //         },
    //         {
    //           breakpoint: 480,
    //           settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //           }
    //         }
    //       ]	
    // });

    $(".evento-hover").mouseover(function() {
        var elementClass = $(this).attr('data-class');
        $(".contenedores").each(
            function() {
                $(this).removeClass('activate');
            }
        );
        $("." + elementClass).addClass('activate');
    });

    $(".link-steps").on("click", function() {
        if (!$(this).hasClass('activado')) {
            var elementStep = $(this).attr("data-class");
            $(".element-step").each(
                function() {
                    $(this).hide("slow");
                }
            );
            $(".link-steps").each(
                function() {
                    $(this).removeClass('activado');
                }
            );
            $(this).addClass('activado');
            $("." + elementStep).show("slow");
        }

    });

    $(".compromises .tab a").click(function() { 
        $(this).addClass("active");
    });

    $("#controlCountry").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    })

    var textarea = document.querySelector("textarea");
    if (textarea != null) {
        var maxlength = 300;
        var currentLength = textarea.value.length;
        var stringCharacters = currentLength + '/' + (maxlength - currentLength);
        $("#subjectCharacters").html(stringCharacters);
        textarea.addEventListener("input", function() {
            var maxlength = 300;
            var currentLength = this.value.length;
            var stringCharacters = currentLength + '/' + (maxlength - currentLength);
            $("body").find("#subjectCharacters").html(stringCharacters);
        });
    }

    $('#send_information_plan_solicited').click(function(e) {
        var formData = $("#form_plan").serialize();
        var fields   = $("#form_plan").serializeArray()
        for (i in fields) {
            if(fields[i].value == "") {
                return swal({type:"error", title:copys_js.error, text:copys_js.all_fields_are_required});
            }
        };
        $.ajax({
            type: "POST",
            url: GLOBAL_DATA.app_root + 'pages/request_plan',
            data: formData,
            dataType: 'json',
            beforeSend: function(e) {
                $("#send_information_plan_solicited").attr("disabled", true);
            },
            success: function(resultado) {
                if (resultado.state == true) {
                    $("#buyPlans").modal('hide')
                    swal({type:"success", title:copys_js.exito, text:copys_js.request_plan_sent_wait_response});
                    $("#send_information_plan_solicited").attr("disabled", false);
                    $("#form_plan").trigger("reset");
                } else {
                    $("#send_information_plan_solicited").attr("disabled", false);
                    swal({type:"error", title:copys_js.error, text:resultado.message["0"]});
                }
            }
        });
    });
});

$('.ancla-home').click(function(e) {
    e.preventDefault(); //evitar el eventos del enlace normal

    if (localStorage.legales == "no") {
        var strAncla = $(this).attr('href'); //id del ancla
        $('body,html').stop(true, true).animate({
            scrollTop: $(strAncla).offset().top
        }, 1000);
    } else {
        var ruta = $(this).attr("data-href"); 
        window.location.href = ruta;
    }

});

//solo números
$(".only-numbers-field").bind('keypress', function(e) {
    if (e.keyCode == '9' || e.keyCode == '16') {
        return;
    }
    var code;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    if (e.which == 46)
        return false;
    if (code == 8 || code == 46)
        return true;
    if (code < 48 || code > 57)
        return false;
});
$(".only-numbers-field").bind("paste", function(e) {
    e.preventDefault();
});
$(".only-numbers-field").bind('mouseenter', function(e) {
    var val = $(this).val();
    if (val != '0') {
        val = val.replace(/[^0-9]+/g, "")
        $(this).val(val);
    }
});

$('[data-toggle="tooltip"]').tooltip()

//solo letras
$(".only-letters-field").bind('keypress', function(e) {
    e = e || window.event;
    var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
    var charStr = String.fromCharCode(charCode);
    if (/\d/.test(charStr)) {
        return false;
    }
});

$(".only-letters-field").bind("paste", function(e) {
    e.preventDefault();
});


