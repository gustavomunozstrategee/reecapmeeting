$(document).ready(function() {
	$(".addActive a").click(function(){
		var tabs = $(".addActive a");
		for (var i = 0; i<tabs.length; i++){
		 	$(tabs[i]).removeClass("active")
		 }
		$(this).addClass("active");
	});

	$('.slider-items').slick({
		dots:true,
		arrows: false,
	  autoplay: true,
	  autoplaySpeed: 2000,
		infinite: true,
		adaptiveHeight: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,	
	  responsive: [
		{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
			infinite: true,
			dots: true
		  }
		},

		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	  
	});

	$('.slider-raiting-employes').slick({
		dots:true,
		arrows: false,
	  autoplay: true,
	  autoplaySpeed: 2000,
		infinite: true,
		adaptiveHeight: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,	
	  responsive: [
		{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
			infinite: true,
			dots: true
		  }
		},

		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	  
	});


	$('.slider-step-actions').slick({
		dots:false,
		arrows: false,
	  autoplay: true,
	  autoplaySpeed: 3000,
		infinite: true,
		adaptiveHeight: true,
	  slidesToShow: 1,
		slidesToScroll: 1,
		verticalSwiping: true,
		vertical: true,	
	});

});