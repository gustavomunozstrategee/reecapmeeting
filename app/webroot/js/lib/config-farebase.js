var config = {
  apiKey: "AIzaSyC9kp0DBuqHquAQh7HzhJj-Yt48iRt1-6E",
  authDomain: "contact-now-saas.firebaseio.com",
  databaseURL: "https://contact-now-saas.firebaseio.com/",
  projectId: "contact-now-saas",
  storageBucket: "contact-now-saas.appspot.com",
  messagingSenderId: "951877212028 "
};
firebase.initializeApp(config);

if (GLOBAL_DATA.user_id != GLOBAL_DATA.DISABLED) {

  var notificationDatos = firebase.database().ref(GLOBAL_DATA.firebaseNotificaciones + GLOBAL_DATA.user_id);
  notificationDatos.on('value', function(snapshot) {
    valores = snapshot.val(); 
    $(".resultadoNotificaciones").html("");
    if (valores != null) {
      var arr = this.ObjectToArray(valores);
      var arrayFinal = arr.k;
      valores = arrayFinal.reverse();
      this.alertPaintNotificaciones(valores);
    } else {
      var datos = '';
      datos += '<li>';
      datos += '<a>'+copys_js.not_have_notifications+'</a>';
      datos += '</li>';
      $(".resultadoNotificaciones").html(datos);
    }
  });

  var notificationSesion = firebase.database().ref(GLOBAL_DATA.firebaseSesion + GLOBAL_DATA.sesionID);
  notificationSesion.on('value', function(snapshot) {
    valores = snapshot.val();
    if (valores != null && valores.state == GLOBAL_DATA.ENABLED) {
      this.alertCamposAjaxFail(valores.message);
    }
  });

  if (GLOBAL_DATA.firebaseCodigo != GLOBAL_DATA.DISABLED) {
    var notificacionContac = firebase.database().ref(GLOBAL_DATA.firebaseContact + GLOBAL_DATA.firebaseCodigo);
    if(GLOBAL_DATA.action == 'edit'){
    valores = data_firebase;
    snapshot = {
      key: data_firebase.firebase_code,
    }
    if(GLOBAL_DATA.controller == 'contacs'){
      if (GLOBAL_DATA.action != 'password') {
        this.initFirebaseVistas(valores, snapshot);
      }
    } else {
      this.resetVariablesContac();
    }
    // notificacionContac.once('value', function(snapshot) {
    //   valores = snapshot.val();
    //   if (valores != null) {
        
    //     if(GLOBAL_DATA.controller == 'contacs'){
    //       if (GLOBAL_DATA.action != 'password') {
    //         this.initFirebaseVistas(valores, snapshot);
    //       }
    //     } else {
    //       this.resetVariablesContac();
    //     }
    //   }
    // });
    }else{
      notificacionContac.on('value', function(snapshot) {
      valores = snapshot.val();
      if (valores != null) {
        if(GLOBAL_DATA.controller == 'contacs'){
          if (GLOBAL_DATA.action != 'password') {
            this.initFirebaseVistas(valores, snapshot);
          }
        } else {
          this.resetVariablesContac();
        }
      }
    });
    }
  } 
}

function initFirebaseVistas(valores, snapshot){
  var contac_id = valores.id;
  if(contac_id === undefined){
    contac_id = GLOBAL_DATA.contacId;
  } 
  var externosArray     = [];
  var funcionariosArray = [];
  var usersApproved     = [];
  if(valores.externos != null){
    for (var i = 0; i < valores.externos.length; i++) {
      externosArray[i] = valores.externos[i];
    } 
  }
  if(valores.approvalusers != null){
    for (var i = 0; i < valores.approvalusers.length; i++) {
      usersApproved[i] = valores.approvalusers[i];
    } 
  }
  if(valores.funcionarios != null){
    for (var l = 0; l < valores.funcionarios.length; l++) {
      funcionariosArray[l] = valores.funcionarios[l];
    }
  }
  var compromisos = valores.commitments; 

  if(GLOBAL_DATA.action == 'view'){ 
    EDIT.listCommitmentsView(compromisos);
    this.uploadInfo(contac_id, externosArray);
    list_assistants(contac_id); 
  if (valores.description != 'Borrador') {
    $('#lbl_description').empty();
    $('#lbl_description').html(valores.description);
  }
  $('#lbl_copias').text(valores.copies);
  $('#lbl_fecha_inicial').text(valores.start_date);
  $('#lbl_fecha_fin').text(valores.end_date);
  $('#lbl_modified').text(valores.modified);  
  EDIT.permissionUploadWhiteLabel(valores.team_id);
  EDIT.initLibCamposView();

} else if(GLOBAL_DATA.action == "edit") {
    EDIT.uploadCommitments(compromisos);
    if(valores.approval != null){
      if(valores.approval == 1){
        $("#content-approval").show();
        $("#requestApproval").prop('checked', true); 
        $("#ContacLimitDate").val(valores.limit_date); 
        $("#ContacApprovalusers").val(usersApproved).trigger("change"); 
      } 
    } else {
      $("#content-approval").hide();
    }      
    EDIT.activateAutoGuardado();
    try {
      if(typeof snapshot != "undefined" && typeof snapshot.key != "undefined") {
        $('.txtFirebaseCode').val(snapshot.key);
      } 
    }catch(err) {}

    $('#ContacClientId').val(valores.client_id);
    $('#ContacProjectId').val(valores.project_id);
    $('#ContacTeamId').val(valores.team_id);
    $("#ContacPassword").val(valores.password);

    if(valores.password != ""){
      $("#ContacProteger").prop('checked', true);
    }  

    if (valores.description != 'Borrador') {
      $('#ContacDescription').val(valores.description);
    }
    EDIT.implementsCkeditor('ContacDescription');

    $('#ContacStartDate').val(valores.start_date); 
    $('#ContacEndDate').val(valores.end_date); 
    $('#ContacCopiesI').val('');
    $('#ContacCopiesI').val(valores.copies);
    $('#ContacCopiesI').tagsinput();
    $('#ContacExternos').val(externosArray);
    $('#ContacFuncionarios').val(funcionariosArray);
    EDIT.cargarImagenesDelete();
    EDIT.cargarDocumentosDelete();
  }
  $('.select2_all').select2({
    language: 'es'
  });
  EDIT.closeModalPreloadInfoFirebase();
}

function uploadInfo(contac_id,externosArray) {
  if (contac_id != undefined) {
    $.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/find_upload_client',{contac_id:contac_id},function (resultado){
      $('#lbl_cliente').html(resultado);
    });

    $.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/find_upload_team',{contac_id:contac_id},function (resultado){
      $('#lbl_team').html(resultado);
    });

    $.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/find_upload_project',{contac_id:contac_id},function (resultado){
      $('#lbl_proyecto').html(resultado)
    }); 
    if(GLOBAL_DATA.action != "view"){
      EDIT.templateList();
    }
    EDIT.cargarImagenes();
    EDIT.cargarDocumentos();  
  }
}

function resetVariablesContac() {
  $.post(GLOBAL_DATA.app_root+GLOBAL_DATA.controller+'/resetVariablesFirebase',{},function (resultado){});
}

function alertCamposAjaxFail(mensaje){
  var alerta ='<div class="alert-fail-recapmeeting">';
  alerta +='<div class="alert alert-danger alert-dismissible alertModal" role="alert">';
  alerta += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
  alerta += mensaje;
  alerta += '</div>';
  alerta += '</div>'; 
  $('.alertConfirmDato').empty();
  $('.alertConfirmDato').append(alerta);
}

function alertPaintNotificaciones(valores) { 
  var contadorNoleido = 0;
  var contadorLeido   = 0;
  var datos           = '';
  var urlAccess       = '';
  var exist           = true;
  var totalLeidas     = 0;
  if (valores != null) {
    for(i in valores) {
      if(valores[i].state == '3'){
        contadorLeido++;
      }
      if (valores[i].state == '1' || valores[i].state == '3') { 
        if(valores[i].type == null){
          urlAccess = GLOBAL_DATA.app_root+'pages/index_page';
          exist = false;
        } else {
          urlAccess = GLOBAL_DATA.app_root+valores[i].type; 
        } 
        contadorNoleido++;
        var msg = valores[i].mensaje;
        if(GLOBAL_DATA.LANG == "eng"){
          msg = valores[i].mensaje_eng;
        }
        datos += '<li>\
            <a class="notVista" data-id="'+exist+'" href="'+urlAccess+'" data-firebase="'+valores[i].nodo_id+'">\
              <div class="message-icon">\
                  <i class="la la-bell"></i>\
              </div>\
              <div class="message-body pr-2">\
                  <div class="message-body-heading">\
                      '+msg+'\
                  </div>\
              </div>\
            </a>\
        </li>';
      }
    }
  } 
  if (contadorNoleido < 1) {   
    datos = '<li>\
      <a rel="nofollow" href="#" class="dropdown-item all-notifications text-center">'+copys_js.not_have_notifications+'</a>\
    </li>';
    $(".pulse-badge-container").removeClass("badge-pulse");
    $(".pulse-icon-container").removeClass("animated infinite swing");
    $(".notificacion-content").html(datos);

  } else {
    $(".pulse-badge-container").addClass("badge-pulse");                                  
    $(".pulse-icon-container").addClass("animated infinite swing");
    
    $(".resultadoNotificaciones").html(datos);
    totalLeidas = contadorNoleido - contadorLeido;
    if(totalLeidas < 0) {
      totalLeidas = 0;
    }
    $("#contNuevo").html("("+totalLeidas+")");
  }
}

$("body").on('click', '.notVista', function(event) { 
  var firebase_id       = $(this).data('firebase');
  var existNotification = $(this).data('id');
  if(existNotification == false){
    alertCamposAjaxFail(copys_js.page_not_found);  
  }
  $.post(GLOBAL_DATA.app_root+'pages/notificacionLeida',{firebase_id:firebase_id},function (resultado){});
});

function ObjectToArray(o){
  var k = Object.values(o);
  var v = Object.values(o);

  var c = function(l){
    this.k = [];
    this.v = [];
    this.length = l;
  };

  var r = new c(k.length);
  for (var i = 0; i < k.length; i++){
    r.k[i] = k[i];
    r.v[i] = v[i];
  }
  return r;
}

$("body").on('click', '#ReadAllNotification', function(event) { 
  var notificationDatos = firebase.database().ref(GLOBAL_DATA.firebaseNotificaciones + GLOBAL_DATA.user_id);
  notificationDatos.on('value', function(snapshot) {
    notifications = snapshot.val(); 
    if (notifications != null) {  
      var arr        = this.ObjectToArray(notifications);
      var arrayFinal = arr.k;
      notifications  = arrayFinal.reverse(); 

      $.post(GLOBAL_DATA.app_root+'pages/read_all_notifications',{notifications:notifications},function (resultado){});
    }
  }); 
});


function list_assistants(contac_id){
    $.post(GLOBAL_DATA.app_root+'contacs/list_assistants_view',{contac_id:contac_id},function (resultado){
      $('#list_assistants').html(resultado);
    });
}

function getContactInfo(ContactInfo){
    $.post(GLOBAL_DATA.app_root+'contacs/get_contact_info',{Contac:ContactInfo},function (result){
      console.log("RESULT CONTACT", result);
    });
}