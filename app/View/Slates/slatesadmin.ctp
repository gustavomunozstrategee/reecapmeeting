<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>



 <div class="container-fluid">
    <!-- Begin Page Header-->
    
    <!-- End Page Header -->
    <?php echo $this->element('nav_admin_taskeee'); ?>

    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
        <div class="widget widget-18 has-shadow">
            <!-- Begin Widget Header -->
            <div class="widget-header bordered d-flex align-items-center">
                <h2><?php echo __("Mis listas") ?></h2> 
            </div>
            <!-- End Widget Header -->
            <div class="widget-body">

                <?php if(!empty($listas)) { ?>

                    <div class='form-group mt-5'> 
                        <?php echo $this->Form->label('Commitment.user_id',__('Asignar listas al siguiente usuario'), array('class'=>'control-label'));?>
                        <?php echo $this->Form->input('user_id', array('class' => 'form-control border-input select','label'=>false,'options' => $collaborators,'div'=>false,"empty"=>__("Seleccionar usuario"),"style"=>"")); ?>
                    </div>

                    <div class="btn-group btn-group-sm">
                        <button type="button" class="tabledit-edit-button btn btn-lg btn-success td-actions reasignar-listas">
                            <?php echo __('Asignar listas'); ?>
                        </button>
                    </div>

                    <div class="form-group mt-5 media-left align-self-center mr-3"> 
                        <input type="checkbox" class="seleccionar_todos" value="1" > <?php echo __('Seleccionar todos'); ?>                                                                                             
                    </div>

                <?php } ?>

                <div class="input-group">
                    <span class="input-group-addon pr-0 pl-0">
                        <a class="btn" href="#">
                            <i class="la la-search la-2x"></i>
                        </a>
                    </span>
                    <input id="buscador" type="text" class="form-control no-ppading-right no-padding-left" placeholder="Buscar Lista...">
                </div>
                 
                <ul class="list-group w-100">
                  <?php if(!empty($listas)): ?>
                    <?php foreach ($listas as $key => $value): ?>
                        <li class="list-group-item item">
                          <div class="other-message">
                              <div class="media ">
                                  <div class="media-left align-self-center mr-3"> 
                                      <input type="checkbox" class="seleccionar_tareas" value="<?php echo $value['Slate']['id'] ?>" name="data[Slate][id][]">                                                                                                
                                  </div> 
                                  <div class="media-left align-self-center mr-3">
                                     <i class="la la-list-alt la-2x align-middle pr-2"></i>
                                  </div>
                                  <div class="media-body align-self-center">
                                      <div class="other-message-sender nombres"><?php echo $value["Slate"]["name"] ?></div>
                                      <div class="other-message-time ">
                                        <i class="la la-users "></i>
                                        <?php foreach ($value["Slate"]["SlatesUser"] as $key2 => $value2): ?>

                                          <?php 
                                            if($key2 > 0){
                                              echo " - ";
                                            }
                                            echo $value2["User"]["firstname"]. " ".$value2["User"]["lastname"];
                                            if($value2["role_id"] == 0){
                                              echo " (Admin)";
                                            }elseif($value2["role_id"] == 1){
                                              echo " (Colaborador)";
                                            }elseif($value2["role_id"] == 2){
                                              echo " (Editor)";
                                            }
                                          ?> 
                                        <?php endforeach ?>
                                     
                                      </div>
                                  </div>
                                  <div class="media-right align-self-center">
                                      <div class="actions">
                                          <a href="<?php echo $this->webroot ?>slates/view_slate_admin/<?php echo EncryptDecrypt::encrypt($value["Slate"]["id"]) ?>"><i class="la la-eye reply"></i></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </li>
                    <?php endforeach;?>

                  <?php else: ?>
                    <div class="alert alert-outline-success" role="alert">
                      <strong></strong> <?php echo __("Aun no tienes  listas de tareas") ?>
                  </div>
                  <?php endif; ?> 
                </ul>
            </div>
        </div>
    </div>
</div>

<?php 
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
   $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  } 
?>

<script type="text/javascript">
  $(document).ready(function(){ 
    $('#buscador').keyup(function(){
      var nombres  = $('.nombres');
      var buscando = $(this).val();
      var item     = '';
      for( var i = 0; i < nombres.length; i++ ){
      item = $(nombres[i]).html().toLowerCase();
      for(var x = 0; x < item.length; x++ ){
          if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
            $(nombres[i]).parents('.item').show(); 
          } else {
            $(nombres[i]).parents('.item').hide();
          }
        }
      }
    });
  });
</script>

<?php $this->start('AppScript'); ?>
<script type="text/javascript">    
    $( document ).ready(function() {
        $('.select').select2(
            {language: 'es',}
        );


        $('body').on('change', '.seleccionar_tareas', function (event){  
            var countCheckbox = $("[name='data[Slate][id][]']:checked").length;
            var totalListas   = 0;
            $.each($("[name='data[Slate][id][]']"), function(){
                totalListas++;
            }); 

            $('.seleccionar_todos').prop('checked', false);
            
            if(totalListas == countCheckbox){
                $('.seleccionar_todos').prop('checked', 'checked'); 
            } 
        });

        $('body').on('change', '.seleccionar_todos', function (event){  
            if($(this).is(':checked')) {
                $.each($("[name='data[Slate][id][]']"), function(){
                    $(this).prop('checked', 'checked');
                });
            } else {
                $.each($("[name='data[Slate][id][]']"), function(){
                    $(this).prop('checked', false); 
                });
            }
        });

        $('body').on('click', '.reasignar-listas', function(event) {
            var listas = [];

            $.each($("[name='data[Slate][id][]']:checked"), function(){
                listas.push($(this).val());
            });

            if( !$('#user_id').val() ) {
                Swal.fire({
                  icon: 'error',
                  title: '¡Atención!',
                  text: 'Se debe seleccionar un usuario.',
                }) 
                return false;
            } 

            if(listas.length == 0){
                Swal.fire({
                  icon: 'error',
                  title: '¡Atención!',
                  text: 'Se debe seleccionar una lista de tareas.',
                }) 
            } else {

                Swal.fire({
                  title: '¿Está seguro de asignar las listas de tareas seleccionadas?',
                  text: "¡No podrás revertir esto!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#60c400',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Confirmar',
                  cancelButtonText: '¡No, Cancelar!',
                }).then((result) => {
                  if (result.isConfirmed) {

                    $.ajax({
                        type: "POST",
                        url: GLOBAL_DATA.APP_BASE+"/slates/reasignar_tareas",
                        dataType:'json',
                        data: {
                            ids: listas,
                            user_id: $('#user_id').val(),
                        },
                        beforeSend: function(){
                            $('#preloader').show();
                        },
                        complete:function(data){

                        },
                        success: function(data){
                            if(data.error == true){
                              $('#preloader').hide();
                              Swal.fire(
                                '¡Alerta!',
                                data.message,
                                'warning'
                              )
                            } else {
                              $('#preloader').hide();
                              Swal.fire(
                                '¡Bien!',
                                'Se han asignado las listas de tareas correctamente.',
                                'success'
                              )
                              setTimeout(function(){
                               location.href = "";
                              }, 1500);
                            }
                        },
                        error: function(data){
                            alert("Problemas al tratar de enviar el formulario");
                        }
                    }); 
                  } else {
                    // $("#check-"+$(this).data("id")).prop("checked",false);
                  }
                })    


            }

        });
 
    });
</script>
<?php $this->end(); ?>
 
 

<style type="text/css">
    .swal2-overflow {
    overflow-x: visible;
    overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}


#select2-user_id-container{
    border: solid 0.5px darkgrey;border-radius: 4px;
}
</style>
 