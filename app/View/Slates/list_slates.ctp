 

<ul class="ticket list-group ">
    <!-- 01 -->
    <?php if(!empty($tareas)): ?>
    <?php foreach($tareas as $ke=>$value): ?>
    <li class="list-group-item  item task_remove_<?php echo $value["SlatesTask"]["id"] ?>">
        <?php if($this->request->is('mobile')){ ?>
          <div>
        <?php } else { ?>
          <div class="media">
        <?php } ?>
           <?php if($value["SlatesTask"]["user_id"] == AuthComponent::user("id") || $SlateRoleId == "0" || $SlateRoleId == "1"): ?>
            
          <div class="media-left align-self-center pr-4">
                <div class="styled-checkbox mt-2">
                        <input data-id="<?php echo $value["SlatesTask"]["id"] ?>" type="checkbox"  id="cb<?php echo $value["SlatesTask"]["id"] ?>" class="checkTask">
                        <label for="cb<?php echo $value["SlatesTask"]["id"] ?>"></label>
                    </div>
            </div>
             <?php endif?>
            <div class="media-left align-self-center pr-4">
                <img src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" class="user-img rounded-circle" alt="user img" style="width: 60px;height: 60px">
            </div>
            <div class="media-body align-self-center">
                <div class="username">
                    <h4><?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></h4>
                </div>
                <div class="msg">
                    <p class="nombres">
                       <?php echo $value["SlatesTask"]["description"] ?>
                    </p>
                </div>
                <div class="status">
                  <i class="la la-calendar-check-o"></i> <?php echo $value["SlatesTask"]["deadline_date"] ?>
                  <?php if($this->request->is('mobile')){ ?>
                    <br>
                  <?php } ?>
                  <i class="la la-bell"></i> <?php echo $value["SlatesTask"]["date_notidication"] ?>
                </div>
            </div>

            <div class="media-right pr-3 align-self-center">

              <?php if($this->request->is('mobile')){ ?>
                  <div class="like">
                <?php } else { ?>
                  <div class="like text-center">
                <?php } ?>

                <?php if($SlateRoleId == 0 || $SlateRoleId == 1): ?>
                  <a class="editTaskNe" href="<?php echo $this->webroot ?>slatestasks/edit/<?php echo EncryptDecrypt::encrypt($value["SlatesTask"]["id"]) ?>">
                    <i class="la la-edit la-2x"></i>
                  </a>
                <?php endif; ?>

                <?php if($value["SlatesTask"]["user_id"] == AuthComponent::user("id") || $SlateRoleId == 0 || $SlateRoleId == 1): ?>
                  <a class="editTask" data-id="<?php echo EncryptDecrypt::encrypt($value["SlatesTask"]["id"]) ?>" href="javascript:void(0)" >
                    <i class="la la-bell la-2x"></i>
                  </a>
                <?php endif?>

                <?php if($SlateRoleId == 0 || $SlateRoleId == 1 ): ?>
                  <a class="deleteTask" data-id="<?php echo EncryptDecrypt::encrypt($value["SlatesTask"]["id"]) ?>" href="javascript:void(0)" >
                    <i class="la la-remove la-2x"></i>
                  </a>
                <?php endif?>

                </div>
            </div>            
        </div>
    </li>

  <?php endforeach; ?>
  <li class="item">
  </li>
  <?php else: ?>
    <div class="alert alert-outline-success" role="alert">
      <strong></strong> <?php echo __("Aun no tienes  tareas en esta  lista.") ?>
  </div>
  <?php endif; ?>
</ul>