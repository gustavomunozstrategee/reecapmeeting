

<div class="row form-group">
    <div class="col-md-12" >
        <div class="btn-group btn-group-sm" style="float: right; margin-top: 28px">
            <button type="button" class="tabledit-edit-button btn btn-lg btn-danger td-actions eliminar-tareas"> 
                <i class="la la-trash p-1 mr-0 text-white"></i> Eliminar tareas 
            </button> 
        </div>  
    </div>                    
</div>

<div class="form-group mt-5 media-left align-self-center mr-3"> 
    <input type="checkbox" class="seleccionar_todos" value="1" > <?php echo __('Seleccionar todos'); ?>                                                                                             
</div>


<ul class="ticket list-group ">
    <!-- 01 -->
    <?php if(!empty($tareas)): ?>
    <?php foreach($tareas as $ke=>$value): ?>
    <li class="list-group-item  item task_remove_<?php echo $value["SlatesTask"]["id"] ?>">
        <div class="media ">

            <?php if($value['SlatesTask']['user_id'] == AuthComponent::user('id')){ ?>
                <div class="media-left align-self-center mr-3"> 
                    <input type="checkbox" class="seleccionar_tareas" value="<?php echo $value['SlatesTask']['id'] ?>" name="data[SlatesTask][id][]">                                                                                                
                </div> 
            <?php } ?>

          <div class="media-left align-self-center pr-4">
                 
            </div>
            <div class="media-left align-self-center pr-4">
                <img src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" class="user-img rounded-circle" alt="user img" style="width: 60px;height: 60px">
            </div>
            <div class="media-body align-self-center">
                <div class="username">
                    <h4><?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></h4>
                </div>
                <div class="msg">
                    <p class="nombres">
                       <?php echo $value["SlatesTask"]["description"] ?>
                    </p>
                </div>
                <div class="status">
                  <i class="la la-calendar-check-o"></i> <?php echo $value["SlatesTask"]["deadline_date"] ?>
                  <?php if($this->request->is('mobile')){ ?>
                    <br>
                  <?php } ?>
                  <i class="la la-bell"></i> <?php echo $value["SlatesTask"]["date_notidication"] ?>
                </div>
            </div>
            
        </div>
    </li>

  <?php endforeach; ?>
  <li class="item">
  </li>
  <?php else: ?>
    <div class="alert alert-outline-success" role="alert">
      <strong></strong> <?php echo __("Aun no tienes  tareas en esta  lista.") ?>
  </div>
  <?php endif; ?>
</ul>



<script type="text/javascript">    
    $( document ).ready(function() {

        $('body').on('change', '.seleccionar_todos', function (event){  
            if($(this).is(':checked')) {
                $.each($("[name='data[SlatesTask][id][]']"), function(){
                    $(this).prop('checked', 'checked');
                });
            } else {
                $.each($("[name='data[SlatesTask][id][]']"), function(){
                    $(this).prop('checked', false); 
                });
            }
        });

        $('body').on('click', '.eliminar-tareas', function(event) {
            var tareas = [];

            $.each($("[name='data[SlatesTask][id][]']:checked"), function(){
                tareas.push($(this).val());
            });   

            if(tareas.length == 0){
                Swal.fire({
                  icon: 'error',
                  title: '¡Atención!',
                  text: 'Se debe seleccionar una tarea.',
                }) 
            } else {
                Swal.fire({
                  title: '¿Está seguro de eliminar las tareas seleccionadas?',
                  text: "¡No podrás revertir esto!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#60c400',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Confirmar',
                  cancelButtonText: '¡No, Cancelar!',
                }).then((result) => {
                  if (result.isConfirmed) {

                    $.ajax({
                        type: "POST",
                        url: GLOBAL_DATA.APP_BASE+"/slates/eliminar_tareas",
                        data: {
                            ids: tareas,
                        },
                        beforeSend: function(){
                            $('#preloader').show();
                        },
                        complete:function(data){
                            
                        },
                        success: function(data){
                            $('#preloader').hide();
                            Swal.fire(
                              '¡Bien!',
                              'Se han eliminado las tarea(s) correctamente.',
                              'success'
                            )
                            setTimeout(function(){
                                window.location.reload();
                            }, 1500);
                        },
                        error: function(data){
                            alert("Problemas al tratar de enviar el formulario");
                        }
                    }); 
                  } else {
                    // $("#check-"+$(this).data("id")).prop("checked",false);
                  }
                })                
            } 
        }); 
    }); 
</script>
