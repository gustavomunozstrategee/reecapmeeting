<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<div class="container-fluid">


    <!-- Begin Page Header-->
    <!-- End Page Header -->
     <?php echo $this->element('nav_admin_taskeee'); ?>

    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                      <div class="widget widget-08 has-shadow">
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header  d-flex align-items-center">
                                        <h2><?php echo __($lista[0]["Slate"]["name"]) ?></h2> 
                                    </div>
                                    
                                    <div class="col-md-12">
                                          
                                          <div class="media-body align-self-center ">
                                        <div class="other-message-time ">
                                        <i class="la la-users "></i>
                                          <?php foreach ($lista[0]["Slate"]["SlatesUser"] as $key2 => $value2): ?>

                                            <?php 
                                              if($key2 > 0){
                                                echo " - ";
                                              }
                                              echo $value2["User"]["firstname"]. " ".$value2["User"]["lastname"];
                                              if($value2["role_id"] == 0){
                                                echo " (Admin)";
                                              }elseif($value2["role_id"] == 2){
                                                echo " (Colaborador)";
                                              }elseif($value2["role_id"] == 1){
                                                echo " (Editor)";
                                              }
                                            ?> 
                                          <?php endforeach ?>
                                        </div>
                                      </div>
                                        </div>
                                        <hr>
                                    <!-- End Widget Header -->
                                    <!-- Begin Widget Body -->
                                    <div class="widget-body">
                                       <div class="input-group">
                                            <input id="buscador" type="text" class="form-control no-ppading-right no-padding-left" placeholder=" Buscar Tarea...">
                                        </div>                                   


                                        <div class="collapse col-md-12" id="collapseExample">
                                        <hr>
                                        <?php echo $this->Form->create('SlatesTask', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40','url'=>'/slatestasks/add/'.$idList)); ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class='form-group'>
                                                  <?php echo $this->Form->input('slateId', array('class'=>'control-label','value'=>$idList,"style"=>"display:none","label"=>false));?>
                                                   
                                            <?php echo $this->Form->label('description',__('Descripción de la tarea'), array('class'=>'control-label'));?>
                                            <?php echo $this->Form->input('description', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2")); ?>
                                            
                                             
                                            </div>
                                            </div>
                                             
                                            
                                        </div>
                         
                                        <div class="row">
                                          <div class="col-md-4">
                                                <div class="form-group">
                                                         <?php echo $this->Form->label('Meeting.start_date',__('Fecha de Terminación'), array('class'=>'control-label f-blue'));?>
                                                        <div class='input-group date ' id=''>
                                                          
                                                            <?php echo $this->Form->input('deadline_date', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class='form-group'>
                                                    <?php echo $this->Form->label('SlatesTask.user_id',__('Usuario responsable'), array('class'=>'control-label select2'));?>
                                                    <?php echo $this->Form->input('user_id', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class='form-group'>
                                                    <?php echo $this->Form->label('SlatesTask.project_id',__('Proyecto'), array('class'=>'control-label'));?>
                                                    <?php echo $this->Form->input('project_id', array('class' => 'form-control border-input select','label'=>false,'div'=>false,"empty"=>__("Sin proyecto"),"style"=>"")); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 pull-right">
                                                <button id="btnEnviar" name="btnEnviar" type='submit' class='btn btn-success pull-right'><?php echo __("Crear Tarea") ?></button>
                                            </div>
                                        </div>
                                        <?php echo $this->Form->end(); ?>
                                        <hr>
                                        <p class="respuesta" style="margin-top: 20px;">
                                        </div>
                                    
                                        <div class="tab-content" id="myTabContent">
                                          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="widget-body no-padding" id="listSlates" style="margin-top: 20px">
                                            </div>
                                          </div>
                                          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                              <div class="widget-body no-padding" id="listSlatesFinish" style="margin-top: 20px">
                                            </div>
                                          </div>
                                          <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

<div id="modalEdit" >
<div class="modal fade" id="ModalLoad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  
</div>

<?php
    
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
    echo $this->Html->css('frontend/admin/components/table.css'); 
     echo $this->Html->script('slates.js', ['block' => 'AppScript']);
     
?>

 

<script type="text/javascript">
    $(document).ready(function(){
  $('#buscador').keyup(function(){
     var nombres = $('.nombres');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.item').show(); 
              }else{
                   $(nombres[i]).parents('.item').hide();
              }
          }
     }
  });
  $('.select').select2({
       
       
      
    });

});

</script>
 <script type="text/javascript">
    $( document ).ready(function() {
            $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });

        $("#SlatesTaskViewForm").bind("submit",function(){
        var btnEnviar = $("#btnEnviar");
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:$(this).serialize(),
            beforeSend: function(){
                btnEnviar.val("Enviando"); // Para input de tipo button
                btnEnviar.attr("disabled","disabled");
            },
            complete:function(data){
                btnEnviar.val("Enviar formulario");
                btnEnviar.removeAttr("disabled");
            },
            success: function(data){
                $(".respuesta").html(data);
                if(data =='1'){
                    refresh1();
                    $("#SlatesTaskViewForm")[0].reset();
                   $(".respuesta").html('<div class="alert alert-success" role="alert">Tarea añadida correctamente.</div>');
                 }else{
                  $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
                 }
            },
            error: function(data){
               $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
            }
        });
        return false;
    });

        $.ajax({
            type: "POST",
            url: "../list_slates_admin",
            data: {id: "<?php echo $idList ?>"},
            beforeSend: function(){
            },
            complete:function(data){
            },
            success: function(data){
                $("#listSlates").html(data);
            },
            error: function(data){
                alert("Problemas al tratar de enviar el formulario");
            }
        }); 
    });

    function refresh1(){
      $.ajax({
            type: "POST",
            url: "../list_slates_admin",
            data: {id: "<?php echo $idList ?>"},
            beforeSend: function(){
            },
            complete:function(data){
            },
            success: function(data){
                $("#listSlates").html(data);
            },
            error: function(data){
                alert("Problemas al tratar de enviar el formulario");
            }
        }); 
    }
    
</script>
<style>
 #select2-SlatesTaskProjectId-container{
            border: solid 0.5px darkgrey;border-radius: 4px;
            }
</style>
<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
.widget-body{
    overflow: initial !important;
}

</style>
 