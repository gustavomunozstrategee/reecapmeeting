 

<ul class="ticket list-group ">
    <!-- 01 -->
    <?php if(!empty($tareas)): ?>
    <?php foreach($tareas as $ke=>$value): ?>
    <li class="list-group-item  item task_remove_<?php echo $value["SlatesTask"]["id"] ?>">
        <div class="media "> 
            <div class="media-left align-self-center pr-4">
                <img src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" class="user-img rounded-circle" alt="user img" style="width: 60px;height: 60px">
            </div>
            <div class="media-body align-self-center">
                <div class="username">
                    <h4><?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></h4>
                </div>
                <div class="msg">
                    <p class="nombres">
                       <?php echo $value["SlatesTask"]["description"] ?>
                    </p>
                </div>
                <div class="status">
                  <i class="la la-calendar-check-o"></i> <?php echo $value["SlatesTask"]["deadline_date"] ?>
                  <?php if($this->request->is('mobile')){ ?>
                    <br>
                  <?php } ?>
                  <i class="la la-bell"></i> <?php echo $value["SlatesTask"]["date_notidication"] ?>
                </div>
            </div> 
        </div>
    </li>

  <?php endforeach; ?>
  <li class="item">
  </li>
  <?php else: ?>
    <div class="alert alert-outline-success" role="alert">
      <strong></strong> <?php echo __("Aun no tienes  tareas en esta  lista.") ?>
  </div>
  <?php endif; ?>
</ul>