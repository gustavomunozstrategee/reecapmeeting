<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Visualización del rol'); ?>
                <?php echo $this->element("team_name"); ?>
				<a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Listar");?>"  href="<?php echo $this->Html->url(array('action'=>'index'));?>">
					<i class="flaticon-menu"></i>
				</a> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Roles") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
	            <div class="table-responsive">
	                <table class="table">
	                    <tbody>
	                        <tr>
	                            <td><b><?php echo __('Nombre'); ?></b></td>
	                            <td>
	                                <?php echo h($position['Position']['name']); ?>&nbsp;
	                            </td>
	                        </tr>
	                        <tr>
	                        	<td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
	                        </tr>
	                        <tr>
	                            <td><b><?php echo __('Permisos'); ?></b></td>
	                            <td>
									<?php foreach ($position["Restriction"] as $permission): ?>
										<?php if (in_array($permission["id"], array(17,18))) { continue;} ?>
										<p><?php echo h(__($permission['name'])); ?></p> 
									<?php endforeach; ?> 
	                            </td>
	                        </tr>
	                        <tr>
	                        	<td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
	                        </tr>
	                        <tr>
	                            <td><b><?php echo __('Creado'); ?></b></td>
	                            <td>
	                                <?php echo h($position['Position']['created']); ?>&nbsp;
	                            </td>
	                        </tr>
	                        <tr>
	                        	<td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
	                        </tr>
	                        <tr>
	                            <td><b><?php echo __('Modificado'); ?></b></td>
	                            <td>
	                                <?php echo h($position['Position']['modified']); ?>&nbsp;
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	            </div>
            	<div class="em-separator separator-dashed"></div>
            </div>
        </div>
    </div>
</div>
