<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Roles'); ?>
                <?php echo $this->element("team_name"); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar rol");?>" href="<?php echo $this->Html->url(array('controller'=>'positions','action' => 'add')); ?>">
                    <i class="flaticon-add"> </i>
                </a>  
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Roles") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row"> 
                        <div class="col-md-6">
                            <div class="input-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <?php if(empty($positions) && !empty($this->request->query['q'])): ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?></p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'),array('class' => 'f-link-search')); ?> 
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="widget-body">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th><?php echo __('Nombre'); ?></th>
                                <th><?php echo __('Modificado'); ?></th>
                                <th><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($positions)){ ?>
                                <?php foreach ($positions as $position): ?>
                                    <tr>
                                        <td><?php echo $this->Text->truncate(h($position['Position']['name']), 70); ?></td>
                                        <td><?php echo h($position['Position']['modified']); ?></td>
                                        <td class="td-actions">
                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($position['Position']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                                <i class="fa fa-eye"></i>
                                            </a> 
                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'edit', EncryptDecrypt::encrypt($position['Position']['id']))); ?>" title="<?php echo __('Editar'); ?>" class="btn-xs" data-toggle="tooltip">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="3" class="text-center"><?php echo __('No existen roles.')?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>