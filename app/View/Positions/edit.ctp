
<?php $this->Html->css('dragula/dragula.min.css', ['block' => 'styles_section']); ?>
<?php $this->Html->css('dragula_positions/example.css', ['block' => 'styles_section']); ?>
<?php echo $this->Html->css('lib/select2.min.css', ['block' => 'styles_section']);?>
<?php //$this->Html->css("frontend/admin/pages/position_add_edit.css", array("block" => "styles_section")); ?> 


<?php  $permission = $this->Utilities->check_team_permission_action(array("index","add"), array("user_teams")); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Roles") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Roles") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Editar rol") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->



<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                
                <h2>
                    <?php echo __('Editar rol'); ?> <?php echo $this->element("team_name"); ?>
                </h2>
                <?php if(!empty($permission["index"])): ?>  
                    <div class="widget-options">
                        <div class="" role="">
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><button type="button" class="btn btn-secondary "><i class="la la-list"></i><?php echo __("Listar");?></button></a>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('Position', array('role' => 'form','data-parsley-validate','novalidate' => true),array('class' => 'form-inline')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-warinig"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label required f-blue font-bold'));?></p>
                            <div class='form-group'>
                               <?php echo $this->Form->input('id'); ?>
                               <?php echo $this->Form->label('Position.name', __('Nombre'), array('class'=>'control-label required f-blue font-bold'));?>
                               <?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'div'=>false,'maxlength' => "80")); ?>
                             </div> 
                             <div class='form-group border-bottom '>
                                <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input resize-none', 'label'=>false,'div'=>false, "type" => "hidden","value" => EncryptDecrypt::decrypt($this->Session->read("TEAM")))); ?>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                            <div class="form-group">
                              <h4 class="text-center text-info">
                                  <p class="f-blue"><?php echo __("Arrastra hacia la columna derecha los permisos que quieres asignar al rol.");?></p>
                              </h4>
                              <div class="row">
                                 <div class="col-md-6 col-sm-6 col-lg-6 mt-3">
                                    <div class="widget widget-18 has-shadow p0">
                                      <div class="widget-header border no-actions align-items-center">
                                        <h3 class="text-center"> <?php echo __('Permisos para asignar') ?> <i class="la la-arrow-right"></i></h3>

                                      </div>
                                      <div class="widget-body border p-0">

                                        <ul class="list-group w-100 visible-scroll" id="left">
                                          <?php foreach ($permissions as $permission): ?>
                                            <li class="list-group-item pt-0 pb-1 user-permission-recapmeeting" data-id="<?php echo $permission["Restriction"]["id"]; ?>" data-position="<?php echo EncryptDecrypt::encrypt($id);?>">
                                                <div class="other-message p-2">
                                                    <div class="media">
                                                      <div class="media-left align-self-center mr-3">
                                                         <i class="la la-cogs la-2x"></i>
                                                      </div>          
                                                      <div class="media-body align-self-center">
                                                          <div class="other-message-sender">
                                                            <?php echo __($permission["Restriction"]["name"]) ?>
                                                          </div>                  
                                                      </div>
                                                    </div>
                                                </div>
                                            </li>  
                                          <?php endforeach; ?>
                                        </ul>
                                        
                                      </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 col-sm-6 col-lg-6 mt-3">
                                   <div class="widget widget-18 has-shadow">
                                      <div class="widget-header border no-actions align-items-center">
                                        <h3 class="text-center"> <i class="la la-arrow-left"></i> <?php echo __('Permisos asignados') ?></h3>
                                      </div>
                                      <div class="widget-body border  p-0">

                                        <ul class="list-group w-100 visible-scroll" id="right">
                                          <?php if(!empty($assignedPermissions)):?>
                                            <?php foreach ($assignedPermissions as $assignedPermission): ?>  
                                              <li class="list-group-item pt-0 pb-1 user-permission-recapmeeting user-in-permision" data-id="<?php echo $assignedPermission["Restriction"]["id"]; ?>" data-position="<?php echo EncryptDecrypt::encrypt($id);?>">
                                                  <div class="other-message p-2">
                                                      <div class="media">
                                                        <div class="media-left align-self-center mr-3">
                                                           <i class="la la-cogs la-2x"></i>
                                                        </div>       
                                                        <div class="media-body align-self-center">
                                                            <div class="other-message-sender">
                                                              <?php echo __($assignedPermission["Restriction"]["name"]) ?>
                                                            </div>                  
                                                        </div>
                                                      </div>
                                                  </div>
                                              </li>  
                                            <?php endforeach; ?>
                                          <?php endif?>
                                        </ul>
                                        
                                      </div>
                                    </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">

                              <?php if(Configure::read("Application.smf") == "1"): ?>

                                <div class="col-md-12 col-sm-12 col-lg-12 ">
                                    <h4 class="text-center text-info">
                                        <p class="f-blue"><?php echo __("Permisos de administración de compromisos") ?></p>
                                    </h4>
                                      <div class='form-group'>
                                       <?php echo $this->Form->label('Position.name', __('Tipo de permiso'), array('class'=>'control-label required f-blue font-bold'));?>
                                       <?php echo $this->Form->input('Position.permission_taskee', array('class' => 'form-control border-input','label'=>false,'div'=>false,"options" => Configure::read("permission_taskee"))); ?>
                                     </div> 
                                   </div>



                                   <div class="col-md-12 col-sm-12 col-lg-12 ">
                                    <h4 class="text-center text-info">
                                        <p class="f-blue"><?php echo __("Configuración de SMV") ?></p>
                                    </h4>
                                      <div class='form-group'>
                                       <?php echo $this->Form->label('Position.name', __('$ SMF ALTA'), array('class'=>'control-label required f-blue font-bold'));?>
                                       <?php echo $this->Form->input('smf_alta', array('class' => 'form-control border-input','label'=>false,'div'=>false,'maxlength' => "80")); ?>
                                     </div> 
                                   </div>
                                   <div class="col-md-12 col-sm-12 col-lg-12 ">
                                      <div class='form-group'>
                                       <?php echo $this->Form->label('Position.name', __('$ SMF ESTANDAR'), array('class'=>'control-label required f-blue font-bold'));?>
                                       <?php echo $this->Form->input('smf_estandar', array('class' => 'form-control border-input','label'=>false,'div'=>false,'maxlength' => "80")); ?>
                                     </div> 
                                   </div>
                                   <div class="col-md-12 col-sm-12 col-lg-12 ">
                                      <div class='form-group'>
                                       <?php echo $this->Form->label('Position.name', __('$ COTIDIANA ALTA'), array('class'=>'control-label required f-blue font-bold'));?>
                                       <?php echo $this->Form->input('cotidiana_alta', array('class' => 'form-control border-input','label'=>false,'div'=>false,'maxlength' => "80")); ?>
                                     </div> 
                                   </div>
                                   <div class="col-md-12 col-sm-12 col-lg-12 ">
                                      <div class='form-group'>
                                       <?php echo $this->Form->label('Position.name', __('$ COTIDIANA ESTANDAR'), array('class'=>'control-label required f-blue font-bold'));?>
                                       <?php echo $this->Form->input('cotidiana_estandar', array('class' => 'form-control border-input','label'=>false,'div'=>false,'maxlength' => "80")); ?>
                                     </div> 
                                   </div>
                                 <?php endif ?>
                              
                            </div>
                          </div>
                            <div>
                                <button type='submit' class='btn btn-primary pull-right'><?php echo __('Guardar') ?></button>
                            </div> 
                        </div>
                    </div>
                    
                <?php echo $this->Form->end(); ?>

            </div>
            <div class="widget-footer">

            </div>
        </div>
    </div>
</div>

<!-- ********************************** -->

<?php
	$this->Html->script('lib/dragula/dragula.min.js',			    ['block' => 'AppScript']);
	$this->Html->script('controller/positions/drag_and_drop_permissions.js', ['block' => 'AppScript']); 
?>







