<div id="update-content-commitment-expired">
    <div class="header-col header-col-home">
        <h3><?php echo __('Compromisos vencidos'); ?></h3>
    </div>
    <div class="scroll-list-compromises scroll-list-compromises-home">
    <table cellpadding="0" cellspacing="0" class="table f-table-grid">
        <tbody>
            <?php if(!empty($commitments)):?>
            <?php foreach ($commitments as $commitment): ?>
            <tr>
                <td class="col-40-table border-right-table">
                    <div class="description-table">
                        <p class="p-word-wrap-table"><?php echo $this->Text->truncate(h($commitment['Commitment']['description']),200); ?></p>
                    </div>
                </td>
                <td class="col-40-table">
                    <p class="p-word-wrap-table"><?php echo h($commitment['Team']['name']); ?></p>
                    <p class="p-word-wrap-table"><?php echo h($commitment['Project']['name']); ?></p>
                    <p class="p-word-wrap-table"><?php echo h($commitment['Commitment']['delivery_date']); ?></p>
                    <a  href="<?php echo $this->Html->url(array('controller' => 'contacs','action' => 'view', EncryptDecrypt::encrypt($commitment['Commitment']['contac_id']))); ?>" class="flex-auto text-center btn btn-table">
                        <?php echo __('Ver acta'); ?>
                    </a>
                </td>

                <td class="col-20-table">
                  <input type="checkbox" name="commitment_id[]" class="chk-commitments" id="chk-commitments" value="<?php echo EncryptDecrypt::encrypt($commitment['Commitment']['id']) ?>" <?php echo ($commitment['Commitment']['selected'] == 1 ? 'checked' : '');?>> 
                </td>

            </tr>
            <?php endforeach ?>
            <?php else: ?>
            <tr>
                <td class="text-center" colspan="14"><?php echo __("No hay compromisos vencidos.");?></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
    <div class="table-pagination">
        <div>
            <?php
            $this->Paginator->options(array('update' => '#update-content-commitment-expired','url'=>array('controller'=>'commitments','action'=>'commitments_expireds')));
            ?>
             <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
        </div>
        <div>
            <ul class="pagination f-paginationrecapmeeting">
                <?php
                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled',['model'=>'Project']));
                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
    <?php echo $this->Js->writeBuffer(); ?>
</div>
