
<?php $permissionAction = $this->Utilities->check_team_permission_action(array("index_all_commitments"), array("commitments"));  ?>
 <div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><?php echo __('Compromisos'); ?></h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>"><i class="ti ti-home"></i></a> </li>
                    <li class="breadcrumb-item active"><?php echo __('Compromisos'); ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row flex-row">
	

<div class="col-xl-12">
    <!-- Justified Tabs -->
    <div class="widget widget-07 has-shadow">
        <div class="widget-header bordered d-flex align-items-center">
            <h2><?php echo __('Compromisos'); ?></h2>
            <div class="widget-options">
            	<div class="dropdown">
            		<i style="cursor: pointer;" href="#" class="la la-cog dropdown-toggle"></i>
				  <div class="dropdown-menu">
				  	<?php if (Authcomponent::user("role_id") == Configure::read('BUSINESS_ROLE_ID')): ?>
						<a href="#" class="dropdown-item" id="recordatorio-compromiso-tiempo"><?php echo __("Configuración del tiempo para el envío del recordatorio de los compromisos"); ?></a>
							<hr>
						<a href="#" class="dropdown-item" id="recordatorio-compromiso-empresa-hora"><?php echo __("Configurar la hora para enviar el recordatorio de los compromisos"); ?></a>
					<?php elseif(Authcomponent::user("role_id") == Configure::read('EMPLOYEE_ROLE_ID')): ?>
						<a href="#" class="dropdown-item" id="recordatorio-compromiso-tiempo"><?php echo __("Configuración del tiempo para el envío del recordatorio de los compromisos."); ?></a>
					<?php endif ?>
				  </div>
				</div>
            </div>
        </div>
        <div class="widget-body">
        	<div class="row">
        		<div class="col-md-12">
        			<form class="form-horizontal">
                        <div class="form-group row d-flex align-items-center mt-3 mb-5">
                            <div class="col-lg-12">
                                <label class="form-control-label"><?php  echo __("Empresa") ?></label>
                                <?php 
                                    if(!empty($this->request->query["teams"])) {
                                        echo $this->Form->input('teams', array('empty'=>__('Selecciona una empresa'), 'class' => 'form-control selectpicker show-menu-arrow','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams_commitments_list', 'data-live-search' => 'true', 'value' => $this->request->query["teams"]));
                                    } else { 
                                        echo $this->Form->input('teams', array('empty'=>__('Selecciona una empresa'), 'class' => 'form-control selectpicker show-menu-arrow','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams_commitments_list', 'data-live-search' => 'true'));
                                    } 
                                ?> 
                            </div>
                        </div>
                    </form>
        		</div>
	        </div>
	        
            <ul class="nav nav-tabs nav-fill" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="just-tab-1" data-toggle="tab" href="#j-tab-1" role="tab" aria-controls="j-tab-1" aria-selected="true">
                    	<?php echo __('Compromisos pendientes'); ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link show" id="just-tab-2" data-toggle="tab" href="#j-tab-2" role="tab" aria-controls="j-tab-2" aria-selected="false">
                    	<?php echo __('Compromisos para esta semana'); ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link show" id="just-tab-3" data-toggle="tab" href="#j-tab-3" role="tab" aria-controls="j-tab-3" aria-selected="false">
                    	<?php echo __('Compromisos completados'); ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content pt-3">
            	<div class="row" >
	        		<div class="col-md-12">
	        			<a id="marcar-realizado-compromiso" class="btn btn-primary" style="color: white !important"> <?php echo __("Marcar compromiso(s) como realizado"); ?></a>
	        		</div>
	       		 </div>
                <div class="tab-pane fade active show" id="j-tab-1" role="tabpanel" aria-labelledby="just-tab-1">
                    <div class="col-commitments" id="show-content-commitments-not-done"></div>
                </div>
                <div class="tab-pane fade" id="j-tab-2" role="tabpanel" aria-labelledby="just-tab-2">
                    <div class="col-commitments" id="show-content-commitments-not-done-week"></div>
                </div>
                <div class="tab-pane fade" id="j-tab-3" role="tabpanel" aria-labelledby="just-tab-3">
                   <div class="col-commitments" id="show-content-commitments-completed"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Justified Tabs -->
</div>
</div>

<hr>
<div class="backg-white">
	<div id="modal-recordatorio-hora" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
		      	<div class="modal-header">
                    <h4 class="modal-title"><?php echo __("Configuración de la hora para enviar los recordatorios de los compromisos."); ?></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
		      <form>
		      	<div class="modal-body">
		        	<strong><p><?php echo __("Seleccione la hora en que desea que se envíen los recordatorios de los compromisos."); ?></p></strong>
		        	<select id="hour-reminder-commitment" class="form-control">
				  		<option value="06:00"><?php echo __("06:00 AM") ?></option>
				  		<option value="18:00"><?php echo __("06:00 PM")?></option>
					</select>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo __("Cancelar"); ?></button>
		        	<button type="button" class="btn btn-success" id="btn-save-hour-reminder-commitment"><?php echo __("Guardar"); ?>  </button>
		     	</div>
		      </form>
		    </div>
	  	</div>
	</div>
	 
	<div id="modal-recordatorio-tiempo" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
		    	<div class="modal-header">
                    <h4 class="modal-title"><?php echo __("Configuración del tiempo para enviar los recordatorio(s) de el/los compromiso(s)"); ?></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
		      	 
		      	<form>
			      	<div class="modal-body">
			        	<strong><p><?php echo __("Selecciona cada cuánto deseas recibir la notificación de el/los compromiso(s) pendiente(s)."); ?></p></strong>
			        	<select id="choose-time-commitment" class="form-control">
					  		<option value="1" selected=""><?php echo __("Un día")?></option>
					  		<option value="2"> <?php echo __("Dos días")?></option>
					  		<option value="3"> <?php echo __("Tres días")?></option>
					  		<option value="7"> <?php echo __("Semanal")?></option>
					  		<option value="15"><?php echo __("Quincenal")?></option>
					  		<option value="30"><?php echo __("Mensual")?></option>
					  		<option value="0"> <?php echo __("Configurar otro tiempo")?></option>
						</select>
						<div id="show-content-time-default-commitment" style="display: none;">
							<?php echo $this->Form->label('time',__('Escribe los días de cada cuanto deseas recibir la notificación de el/los compromiso(s) pendiente(s).'));?>
	 						<?php echo $this->Form->input('time', array('type'=>'text', 'label'=>false,'div'=>false, 'class' => 'form-control', 'onkeypress'=>"return onlyNumberField(event)",'id' => 'default-time-commitment','maxlength' => 3)); ?>
						</div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo __("Cancelar"); ?></button>
			        	<button type="button" class="btn btn-success" id="btn-guardar-tiempo-recordatorio-compromisos"><?php echo __("Guardar"); ?>  </button>
			     	</div>
		      	</form>
		    </div>
	  	</div>
	</div>
	
	<div id="modal-marcar-como-realizado" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="sa-icon sa-success animate" style="display: block;">
                        <span class="sa-line sa-tip animateSuccessTip"></span>
                        <span class="sa-line sa-long animateSuccessLong"></span>
                        <div class="sa-placeholder"></div>
                        <div class="sa-fix"></div>
                    </div>
                    <div class="section-title mt-5 mb-2">
                        <p class="text-gradient-02">
                        	<?php echo __("¿Deseas confirmar la realización de el/los compromiso(s) seleccionado(s) como realizado(s)?"); ?>
                        </p>
                    </div>
                    <button type="button" class="btn btn-danger" id="btn-cancel-commitment-done" data-dismiss="modal"><?php echo __("Cancelar"); ?></button>
	        	<button type="button" class="btn btn-success" id="btn-save-commitment-done"><?php echo __("Guardar"); ?>  </button>
                </div>
            </div>
        </div>
    </div>

	<div id="modal-marcar-como-realizado2" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
		    <div class="modal-content">
		    	<div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                	<h2>s</h2>
                </div>
		      	 
		      <form> 
		      	<div class="modal-footer">
		        	
		     	</div>
		      </form>
		    </div>
	  	</div>
	</div>
	<div class="container-row">
	           
	</div> 
</div>
<?php 
	$this->Html->script("controller/commitments/index.js", ['block' => 'AppScript']); 
	$this->Html->script('lib/sweetalert.min.js', ['block' => 'AppScript']);	
?>