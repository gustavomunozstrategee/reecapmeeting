<div id="update-content-user">
        <div class="height-scroll visible-scroll height-20">
            <?php if(!empty($commitments)) {?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th><?php echo __('Usuario') ?></th>
                            <th><?php echo __('Descripción') ?></th>
                            <th><?php echo __('Fecha de entrega') ?></th>
                            <th><?php echo __('Estado') ?></th>
                            <th><?php echo __('Proyecto') ?></th>
                            <th><?php echo __('Acta #'); ?></th>
                            <th><?php echo __('Acciones'); ?></th>
                            <th></th>
                        </thead>
                        <?php foreach ($commitments as $commitment): ?>
                            <tr>
                                <td><?php echo h($commitment['User']['firstname'] . ' ' . $commitment['User']['lastname']); ?></td>
                                <td><?php echo h($commitment['Commitment']['description']); ?></td>
                                <td><?php echo h($commitment['Commitment']['delivery_date']); ?></td>
                                <td><?php echo $this->Utilities->showStateCommitment($commitment['Commitment']['state']); ?></td>
                                <td><?php echo h($commitment['Project']['name']); ?></td>
                                <td><?php echo h($commitment['Contac']['number']); ?></td>
                                <td colspan="2" class="td-actions">
                                    <?php $permission = $this->Utilities->check_team_permission_action(array("index"), array("contacs"), $commitment['Commitment']['team_id']); ?>
                                    <?php if(isset($permission["index"])) : ?>  
                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view','controller'=>'contacs', EncryptDecrypt::encrypt($commitment['Contac']['id']))); ?>" title="<?php echo __('Ver acta'); ?>" class="btn-xs">
                                            <i class="fa fa-eye f-blue"></i>
                                        </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                </div>
            <?php } else { ?>
                <div>
                    <div class="text-center paddingxy-2em">
                        <i class="flaticon-checked table-icon-empty"></i>
                        <?php echo __("No hay compromisos pendientes.");?>
                    </div>
                </div>
            <?php } ?>
        </div>
  
        <div class="table-pagination">
            <div>
                <?php $this->Paginator->options(array('update' => '#update-content-user','url'=>array('controller'=>'commitments','action'=>'commitments_employee', EncryptDecrypt::encrypt($userId))));?>
                <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
            </div>
            <div>
                <ul class="pagination f-paginationrecapmeeting">
                    <?php
                    echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                    echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
        </div>
    <?php echo $this->Js->writeBuffer(); ?>
</div>