<div id="update-content-commitment-week-actual">
     
    <div class="scroll-list-compromises scroll-list-compromises-home">
    <table cellpadding="0" cellspacing="0" class="table f-table-commitments f-table-grid">
        <tbody>
            <?php if(!empty($commitments)):?>
                <ul class="list-group w-100 mt-3">
            <?php foreach ($commitments as $commitment): ?>
                <li class="list-group-item">
                <div class="media">
                    <div class="event-date align-self-center mr-3">
                        <div class="styled-checkbox">
                                                            <input type="checkbox" name="commitment_id[]" class="chk-commitments" id="cks-<?php echo EncryptDecrypt::encrypt($commitment['Commitment']['id']) ?>" value="<?php echo EncryptDecrypt::encrypt($commitment['Commitment']['id']) ?>" <?php echo ($commitment['Commitment']['selected'] == 1 ? 'checked' : '');?>> 
                                                            <label for="cks-<?php echo EncryptDecrypt::encrypt($commitment['Commitment']['id']) ?>"></label>
                                                        </div>
                        
                    </div>
                    <div class="media-body align-self-center">
                        <div class="event-title text-secondary"><?php echo h($commitment['Team']['name']); ?> - <?php echo h($commitment['Project']['name']); ?>
                            <a style="font-size: 22px"  data-toggle="tooltip" data-placement="right" data-original-title="<?php echo __("Ver acta") ?>"   href="<?php echo $this->Html->url(array('controller' => 'contacs','action' => 'view', EncryptDecrypt::encrypt($commitment['Commitment']['contac_id']))); ?>">
                             <i class="ti ti-clipboard"></i>
                            </a>
                        </div>

                        <div class="event-desc mr-3">
                            <span><?php echo $this->Text->truncate(h($commitment['Commitment']['description']),200); ?></span>
                        </div>
                        <div class="event-desc">
                            <i class="la la-calendar"></i>
                            <span><?php echo h($commitment['Commitment']['delivery_date']); ?></span>
                            
                        </div>
                    </div>
                </div>
            </li>
            <?php endforeach ?>
             </ul>
            <?php else: ?>
            <tr>
                <td class="text-center"><?php echo __("No hay compromisos por realizar para esta semana.");?></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
    <div class="table-pagination">
        <div>
            <?php
                if(!empty($teamGet)){
                    $this->Paginator->options(array('update' => '#update-content-commitment-week-actual','url'=>array('controller'=>'commitments','action'=>'commitments_week_actual', '?'=> array('teamIds'=>$teamGet))));
                } else {
                    $this->Paginator->options(array('update' => '#update-content-commitment-week-actual','url'=>array('controller'=>'commitments','action'=>'commitments_week_actual')));
                }
            ?>
                 <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
        </div>
        <div>
            <ul class="pagination f-paginationrecapmeeting">
                <?php
                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled',['model'=>'Project']));
                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
    <?php echo $this->Js->writeBuffer(); ?>
</div>