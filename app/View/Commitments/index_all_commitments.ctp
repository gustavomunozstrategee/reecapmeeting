<?php echo $this->Html->css('lib/select2.min.css', ['block' => 'AppCss']);?>
<div class="backg-white">
<div class="flex-space-between-start">
    <div>
        <h1 class="f-title"><?php echo __('Compromisos'); ?></h1>
        <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Gestionar compromisos")?>" href="<?php echo $this->Html->url(array('controller'=>'commitments','action' => 'index')); ?>">
            <i class="flaticon-list"> </i>
        </a> 
    </div>

    <!-- Buscadores -->
<div class="f-datapicker mt-20"> 
    <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inline', 'type'=>'GET', 'role'=>'form'));?>
    <div class="form-group">   
        <?php echo $this->Form->input('client_id', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => 'Nombre', 'type' => 'select', 'options' => $clients, 'empty' => __("Selecciona un cliente"), 'selected' => !empty($this->request->query['client_id']) ? $this->request->query['client_id'] : '')); ?>
    </div>

    <div class="form-group f-input-select-white">
        <?php echo $this->Form->label('Client.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
        <?php if(!empty($this->request->query["teams"])):?>
            <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams','multiple' => true, 'value' => $this->request->query["teams"])); ?>
        <?php else: ?>
            <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams','multiple' => true)); ?>
        <?php endif; ?>
    </div>
     
    <button type="submit" class="btn btn-blue" id="search_team"><?php echo __('Buscar');?></button>
    <?php if(empty($commitments) && !empty($this->request->query['client_id'])) : ?>
        <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
        <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
    <?php endif ?>
    <?php echo $this->Form->end(); ?>
</div> 
</div>


<!-- <div class="flex-space-between-start">
    <div>
        <h1 class="f-title">
            <?php echo __('Compromisos'); ?>
        </h1> 
        <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Gestionar compromisos")?>" href="<?php echo $this->Html->url(array('controller'=>'commitments','action' => 'index')); ?>">
            <i class="flaticon-list"> </i>
        </a> 
    </div>
    
    <div>
        <div class="f-content-search">
            <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inline', 'type'=>'GET', 'role'=>'form'));?>
            <div class="input-group">
               <?php echo $this->Form->input('client_id', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => 'Nombre', 'type' => 'select', 'options' => $clients, 'empty' => __("Selecciona un cliente"), 'selected' => !empty($this->request->query['client_id']) ? $this->request->query['client_id'] : '')); ?>
               <div class="input-group-btn">
                 <button type="submit" class="btn"><span class="glyphicon glyphicon-search f-blue"></span></button>
               </div>
            <?php if(empty($commitments) && !empty($this->request->query['q'])) : ?>
            <p class="mxy-10">
                <?php echo __('No se encontraron datos.') ?>
            </p>
            <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index_all_commitments'),array('class' => 'f-link-search')); ?>
            <?php endif ?>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
</div> -->

<section class="section-table">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                        <thead class="text-primary">
                            <tr>
                                <th class="table-grid-20"><?php echo __('Usuario'); ?></th>
                                <th class="table-grid-40"><?php echo __('Descripción'); ?></th>
                                <th class="table-grid-10"><?php echo __('Fecha límite'); ?></th>
                                <th class="table-grid-10"><?php echo __('Cliente'); ?></th>
                                <th class="table-grid-10"><?php echo __('Empresa'); ?></th>
                                <th class="table-grid-10"><?php echo __('Estado') ?></th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($commitments)):?>
                                <?php foreach ($commitments as $commitment): ?>
                                    <tr> 
                                        <?php if(empty($commitment['User']['firstname'])): ?>
                                            <td class="table-grid-20 td-word-wrap"><?php echo h($commitment['User']['email']); ?></td> 
                                        <?php else: ?>
                                            <td class="table-grid-20 td-word-wrap"><?php echo h($commitment['User']['firstname'] . ' ' . $commitment['User']['lastname']); ?></td> 
                                        <?php endif; ?>
                                        <td class="table-grid-40 td-word-wrap"><?php echo $this->Text->truncate(h($commitment['Commitment']['description']),100); ?></td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo h($commitment['Commitment']['delivery_date']); ?></td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($commitment['Client']['name']),100); ?></td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($commitment['Team']['name']),100); ?></td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo $this->Utilities->showStateCommitment($commitment['Commitment']['state']); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="6"><?php echo __('No existen compromisos.')?></td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--pagination-->
            <div class="table-pagination">
                <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                <div>
                    <ul class="pagination f-paginationrecapmeeting">
                        <?php
                        echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                        echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                        ?>
                    </ul>
                </div>
            </div>
            <!--End pagination-->
        </div>
    </div>
</section>

</div>

 <?php
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']); 
    $this->Html->scriptBlock("$('#SearchClientId').select2();", ['block' => 'AppScript']);
 ?>

