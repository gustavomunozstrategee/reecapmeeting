<?php if(empty($this->Session->read("CommitmentId")) || is_null($this->Session->read("CommitmentId"))):?>
	<center><strong><?php echo __("No hay compromisos")?></strong></center>
<?php endif; ?>
<div id="markAsReadCommitment" class="modal fade">
	<div class="modal-dialog modal-recapmeeting">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
	            <h4 class="modal-title"><?php echo __("¿Deseas marcar este compromiso como realizado?")?></h4>
        		<p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p> 
	        </div> 
	        <div class="modal-footer"> 
	            <button type="button" class="btn btn-modal-recapmeeting btn-mark-as-read"><?php echo __("Aceptar")?></button>
	            <button type="button" class="btn btn-blue" data-dismiss="modal"><?php echo __("Cancelar")?></button>
	        </div>
	    </div>
	</div>
</div>
 
<?php 
	$this->Html->script("controller/commitments/index.js",  ['block' => 'AppScript']); 
	$this->Html->script('lib/sweetalert.min.js', ['block' => 'AppScript']);	
?>