<div id="update-content-commitment-createds">
    <div class="header-col header-col-home">
        <h3><?php echo __('Compromisos creados'); ?></h3>
    </div>
    <div class="scroll-list-compromises scroll-list-compromises-home">
    <table cellpadding="0" cellspacing="0" class="table f-table-grid">
        <tbody>
            <?php if(!empty($commitments)):?>
            <?php foreach ($commitments as $commitment): ?>
            <tr>
                <td class="col-50-table border-right-table">
                    <div class="description-table">
                        <p class="p-word-wrap-table"><?php echo $this->Text->truncate(h($commitment['Commitment']['description']),200); ?></p>
                    </div>
                </td>
                <td class="col-50-table col-50">
                    <p class="p-word-wrap-table"><?php echo h($commitment['Team']['name']); ?></p>
                    <p class="p-word-wrap-table"><?php echo h($commitment['Project']['name']); ?></p>
                    <p class="p-word-wrap-table"><?php echo h($commitment['Commitment']['delivery_date']); ?></p>
                    <a class="btn btn-table" href="<?php echo $this->Html->url(array('controller' => 'contacs','action' => 'view', EncryptDecrypt::encrypt($commitment['Commitment']['contac_id']))); ?>">
                        <?php echo __('Ver acta'); ?>
                    </a>
                </td>
            </tr>
            <?php endforeach ?>
            <?php else: ?>
            <tr>
                <td class="text-center">
                  <?php echo __("No hay compromisos creados.");?>                      
                </td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>
    <div class="table-pagination">
    <div>
        <?php
        $this->Paginator->options(array('update' => '#update-content-commitment-createds','url'=>array('controller'=>'commitments','action'=>'commitments_createds')));
        ?>
        <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
    </div>
        <div>
            <ul class="pagination f-paginationrecapmeeting">
                <?php
                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled',['model'=>'Project']));
                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
    <?php echo $this->Js->writeBuffer(); ?>
</div>