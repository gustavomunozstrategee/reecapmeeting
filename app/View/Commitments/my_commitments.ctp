<div id="update-content-employee">
    <div>
        <div>
            <?php if(!empty($commitments)):?>
            <?php foreach ($commitments as $commitment): ?>
            <div>
                <div class="row-list-meeting">
                    <div class="col-50 f-blue"><?php echo __('Usuario') ?></div>
                    <?php if($commitment['Employee']['id'] == null):?>
                        <div class="col-50"><?php echo h($commitment['User']['firstname'] . ' ' . $commitment['User']['lastname']); ?></div>
                        <?php else:?>
                        <div class="col-50"><?php echo h($commitment['Employee']['name']); ?></div>
                    <?php endif; ?>
                </div>
                <div class="row-list-meeting">
                    <div class="col-50 f-blue"><?php echo __('Descripción') ?></div>
                    <div class="col-50"><?php echo h($commitment['Commitment']['description']); ?></div>
                </div>
                <div class="row-list-meeting">
                    <div class="col-50 f-blue"><?php echo __('Fecha de entrega') ?></div>
                    <div class="col-50"><?php echo h($commitment['Commitment']['delivery_date']); ?></div>
                </div>
                <div class="row-list-meeting">
                    <div class="col-50 f-blue"><?php echo __('Estado') ?></div>
                    <div class="col-50"><?php echo $this->Utilities->showStateCommitment($commitment['Commitment']['state']); ?></div>
                </div>
            </div>
            <?php endforeach ?>
            <?php else: ?>
            <div>
                <div class="text-center paddingxy-2em">
                    <i class="flaticon-checked table-icon-empty"></i>
                    <?php echo __("No hay compromisos pendientes.");?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="div-pagination">
        <div>
            <?php
            $this->Paginator->options(array('update' => '#update-content-employee','url'=>array('controller'=>'commitments','action'=>'my_commitments', EncryptDecrypt::encrypt($employeeId))));
            ?>
            
                <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
            
        </div>
        <div>
            <ul class="pagination f-paginationrecapmeeting">
                <?php
                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
    <?php echo $this->Js->writeBuffer(); ?>
</div>