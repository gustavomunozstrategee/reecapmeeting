<div class="paddingxy-2em">
    <div class="flex-start">
        <div class="col-50">
            <label class="f-blue"><?php echo  __('Valor'); ?></label>
        </div>
        <div class="col-50">
            <p><?php echo h(!empty($transaction['Transaction']['value']) ?  ' USD $'. $transaction['Transaction']['value'] : ' USD $' . 0); ?></p>
        </div>
    </div>
    <div class="flex-start">
        <div class="col-50">
            <label class="f-blue"><?php echo  __('Medio de pago'); ?></label>
        </div>
        <div class="col-50">
            <p><?php echo $transaction["Transaction"]["payment_method_name"]?></p>
        </div>
    </div>
    <div class="flex-start">
        <div class="col-50">
            <label class="f-blue"><?php echo  __('Id de la transacción'); ?></label>
        </div>
        <div class="col-50">
            <p><?php echo $transaction["Transaction"]["transactionId"]?></p>
        </div>
    </div>
    <div class="flex-start">
        <div class="col-50">
            <label class="f-blue"><?php echo  __('Plan asociado'); ?></label>
        </div>
        <div class="col-50">
            <p><?php echo $transaction["Plan"]["name"]?></p>
        </div>
    </div>
    <div class="flex-start">
        <div class="col-50">
            <label class="f-blue"><?php echo  __("Fecha transacción"); ?></label>
        </div>
        <div class="col-50">
            <p><?php echo $this->Time->format('d-m-Y h:i A', $transaction["Transaction"]["created"])?></p>
        </div>
    </div>

    <div class="flex-start">
        <div class="col-50">
            <label class="f-blue"><?php echo  __("Estado transacción"); ?></label>
        </div>
        <div class="col-50">
            <p><?php echo $this->Utilities->showStateTransaction($transaction['Transaction']['state'])?></p>
        </div>
    </div>

    <div class="flex-start">
        <div class="col-50">
            <?php if($transaction["Transaction"]["state"] == configure::read("TRANSACTION_APROBADA")):?>
            <label class="f-blue"><?php echo  __("Vigencia"); ?></label>
            <?php endif; ?>
        </div>
        <div class="col-50">
            <?php if($transaction["Transaction"]["state"] == configure::read("TRANSACTION_APROBADA") && !empty($planInfo)):?>
            <p><?php echo $planInfo["PlanUser"]["buy_date"] . ' - ' . $planInfo["PlanUser"]["validity"]?></p>
            <?php else: ?>
            <p><?php echo __("No existe plan asociado"); ?></p>
            <?php endif; ?>
        </div>
    </div>

</div>

