<div class="backg-white">    
    <div class="flex-space-between-start">
        <div>
            <h1 class="f-title">
                <?php echo __('Transacciones'); ?>
            </h1>
            <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Planes");?>"   href="<?php echo $this->Html->url(array('controller'=>'plans','action'=>'planning_management'));?>">
            <i class="flaticon-choices"></i>
            </a>
        </div>

        <div>
            <div class="text-right f-content-search">
                <?php echo $this->Form->create('User', array('role' => 'form','type'=>'GET','class'=>'form-inline')); ?>
                <div class="input-group">
                    <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Buscar por valor'), 'class'=>'form-control','label'=>false,'div'=>false)) ?>
                    <div class="input-group-btn">
                        <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
                <?php if(empty($transactions) && !empty($this->request->query['q'])) : ?>
                <p><?php echo __('No se encontraron datos.') ?> </p>
                    <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'),array('class' => 'f-link-search')); ?>
               
                <?php endif ?>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <section class="section-table">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table cellpadding="0" cellspacing="0" class="table table-striped table-hover f-table-recapmeeting">
                            <thead class="f-blue">
                                <tr>
                                    <th><?php echo $this->Paginator->sort('value', __('Valor transacción')); ?></th>
                                    <th><?php echo $this->Paginator->sort('state', __('Estado transacción')); ?></th>
                                    <th><?php echo $this->Paginator->sort('created', __('Fecha en que se realizó la transacción')); ?></th>
                                    <th class="text-center"><?php echo __("Acciones"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($transactions)):?>
                                <?php foreach ($transactions as $transaction): ?>
                                <tr>
                                    <td><?php echo h(!empty($transaction['Transaction']['value']) ?  ' USD $'. $transaction['Transaction']['value'] : ' USD $' . 0); ?>&nbsp;</td>
                                    <td><?php echo $this->Utilities->showStateTransaction($transaction['Transaction']['state'])?> </td>
                                    <td>
                                        <?php echo h($this->Time->format('d-m-Y h:i A', $transaction['Transaction']['created'])); ?>
                                    </td>
                                    <td class="td-actions text-center"><?php echo $this->Utilities->showButtonDetailTransaction($transaction['Transaction']['state'], $transaction['Transaction']['id'], $transaction['Plan']['id'])?></td>
                                </tr>
                                <?php endforeach; ?>
                                <?php else: ?>
                                <tr>
                                    <td class="text-center" colspan="4"><?php echo __('No existen transacciones.')?></td>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--Pagination-->
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                            echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                            echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
                <!--End pagination-->
            </div>
        </div>
    </section>
    <div class="modal fade" id="DetailTransaction">
        <div class="modal-dialog modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="myModalLabel" class="modal-title"><?php echo __('Detalle de la transacción') ?></h4>
                </div>
                <div class="modal-body">
                    <div id="show-content-transaction"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-modal-recapmeeting btn-close-transaction-detail" data-dismiss="modal"><?php echo __('Cerrar') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('controller/transactions/index.js',	['block' => 'AppScript']); ?>
