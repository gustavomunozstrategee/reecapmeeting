<table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
    <tr align="center">
        <td style="width:50px;"></td>
        <td style="width:500px;">
            <br>
            <p><?php echo __("Hola, Para recuperar tu contraseña por favor utiliza el siguiente código: ").$hash;?></p>
        </td>
        <td style="width:50px;"></td>
    </tr>
</table>