<!DOCTYPE html>
<html lang="es">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="utf-8" />
</head>
<body style="background-color:#f8f8f8;">
    <table align="center" style="width: 600px; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;background-color:#ffffff;">
        <tr>
            <td>
                <table style="width: 600px;  display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr>
                        <td>
                          <img src="<?php echo  Router::url("/",true).'img/frontend/website/email/bg-email.png'?>">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr align="center">
                        <td style="width:50px;"></td>
                        <td style="width:500px;">
                            <p style="color:#808080;"><?php echo __("Información de la transacción.")?></p>
                        </td>
                        <td style="width:50px;"></td>
                    </tr>
                </table>

                <table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr align="left">
                       <td style="width:50px;"></td>
                       <td style="width:500px;">
                         <p style="color:#031730;"><b><?php echo __("Valor de la transacción")?></b>&nbsp;<span style="color:#808080;">$<?php echo h($data['Transaction']['value'] . " USD"); ?></span></p>
                         <p style="color:#031730;"><b><?php echo __("Método de pago")?></b>&nbsp;<span style="color:#808080;"><?php echo h(!empty($data['Transaction']['payment_method_name']) ? $data['Transaction']['payment_method_name'] : __("No se utilizó ninguno")); ?></span></p>
                         <p style="color:#031730;"><b><?php echo __("Estado")?></b>&nbsp;<span style="color:#808080;"><?php 
                            $state = "";
                            if($data['Transaction']['state'] == configure::read("TRANSACTION_APROBADA")){
                                $state = __("Aprobada");                                            
                            } else if($data['Transaction']['state'] == configure::read("TRANSACTION_RECHAZADA")) {
                                $state = __("Rechazada"); 
                            } else if($data['Transaction']['state'] == configure::read("TRANSACTION_PENDING")) {
                                $state = __("Pendiente"); 
                            } else if($data['Transaction']['state'] == configure::read("TRANSACTION_CANCEL")) {
                                $state = __("Cancelada"); 
                            }
                        ?>
                        <?php echo h(__($state)); ?></span></p>
                         <p style="color:#031730;"><b><?php echo __("Fecha de la transacción")?></b>&nbsp;<span style="color:#808080;"><?php echo h($data['Transaction']['created']); ?></span></p>
                       </td>
                       <td style="width:50px;"></td>
                   </tr>  
               </table>

               <br>
               <table border="0" align="center" cellpadding="0" cellspacing="0" class="mbtn20 mtop10" cellmargin="0">
                <tr>
                    <td width="200"></td>
                    <td width="200" align="center"><a href="<?php echo Router::url('/', true) ?>transactions/index/" style="color:#1e212a; text-decoration:underline;"><span style="font-family:arial; font-size:14px;color:#1e212a;font-weight:bold;"><?php echo __("Ir a las transacciones") ?></span></a></td>
                    <td width="200"></td>
                </tr>
               </table>
             
              
                <table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr align="center">
                        <td style="width:50px;"></td>
                        <td style="width:500px;">
                           <p style="color:#969696;"><b><?php echo __("Saludos,") ?></b><br><?php echo __("El equipo de RecapMeeting.") ?></p>
                        </td>
                        <td style="width:50px;"></td>
                    </tr>
                </table>
                <br>
                <table border="0" align="center" cellpadding="0" cellspacing="0" class="mbtn20 mtop10" cellmargin="0">     
                    <tr>
                        <td width="25"></td>
                        <td width="550" height="30" align="center">                
                            <span style="color:#969696;font-size:12px;"><?php echo __('A solution of StrategeeSuite.com ')?><?php echo __('© 1993-') ?><?php echo date('Y'); ?></span> <a href="http://strategeesuite.com/legal" style="color:#969696;font-size:12px;text-decoration:none;"><span><?php echo __('Strategee, LLC. or its affiliates.') ?></span></a>
                        </td>
                        <td width="25"></td>
                    </tr>
                </table>
                <br>      
                <table style="width: 600px;background-color:#FBFBFB; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr align="center">
               
                        <td style="width:200px;">
                           
                        </td>

                        <td style="width:30px;">
                           <a href="<?php echo configure::read("INSTAGRAM_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/instagram.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                            <a href="<?php echo configure::read("FACEBOOK_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/facebook.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("YOUTUBE_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/youtube.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                            <a href="<?php echo configure::read("TWITER_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/twitter.png'?>"></a>
                        </td>

                        <td style="width:40px;">
                           <b style="color:#21C910;">|</b>
                        </td>

                        <td style="width:40px;">
                            <a href="" target="_blank" style="color:#21C910;"><?php echo __("www.recapmeeting.com") ?></a>
                        </td>

                        <td style="width:200px;">
                          <a href="https://strategeesuite.com/legal" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/logo-strategeesuite.png'?>"></a>
                        </td>
                    </tr>
                </table> 
            </td>
        </tr>
    </table>
</body>
</html>














