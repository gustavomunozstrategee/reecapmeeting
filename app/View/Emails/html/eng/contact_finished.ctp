<table align="center" bgcolor="#ffffff" border="1" width="600" cellspacing="5" cellpadding="5">

    <!-- <tr colspan="3" align="center">
        <td colspan="3">
            <img src="<?php //echo Router::url("/", true) . 'img/frontend/website/email/bg-email.png' ?>"style="width:100%;">
        </td>
    </tr> -->

    <tr colspan="3" valign="top" align="center">
        <td colspan="3"><strong style="color:#1e212a;font-size:18px;"><?php echo __("Minute information") ?></strong></td>
    </tr>

    <?php if(!empty($info["contac"]["Team"]["img"])): ?>
        <tr colspan="3" valign="top" align="left">
            <td colspan="1" style="width:300px;" align="center" valign="middle">
                <img src="<?php echo Router::url("/", true) . "document/WhiteBrand/{$info["contac"]["Team"]["img"]}" ?>" style="width: 200px;">    
            </td>

            <?php if (!file_exists(WWW_ROOT."files/Project/{$info["contac"]["Project"]["img"]}")): ?>
                <td colspan="2" style="width:300px;" align="center" valign="middle">  
                    <img src="<?php echo Router::url("/", true) . 'img/frontend/website/email/logo.jpg' ?>" style="width: 200px;">
                </td>
             <?php else: ?>
                <td colspan="2" style="width:300px;" align="center" valign="middle">
                    <img src="<?php echo Router::url("/", true) . "files/Project/{$info["contac"]["Project"]["img"]}" ?>" style="width: 200px;">
                </td>
             <?php endif; ?>
        </tr>
    <?php else: ?>
        <tr colspan="3" align="center">
            <?php if (!file_exists(WWW_ROOT."files/Project/{$info["contac"]["Project"]["img"]}")): ?>
                <td colspan="3" style="width:300px;" align="center" valign="middle">  
                    <img src="<?php echo Router::url("/", true) . 'img/frontend/website/email/logo.jpg' ?>" style="width: 200px;">
                </td>
             <?php else: ?>
                <td colspan="3" style="width:300px;" align="center" valign="middle">
                    <img src="<?php echo Router::url("/", true) . "files/Project/{$info["contac"]["Project"]["img"]}" ?>" style="width: 200px;">
                </td>
            <?php endif; ?>
        </tr>
    <?php endif; ?>


    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Minute number'); ?></strong></td>
        <td colspan="2">
            <?php if ($info['contac']['Contac']['state'] == Configure::read('ENABLED')): ?>
                <?php echo $this->Utilities->stateContac($info['contac']['Contac']['state']); ?>
            <?php else: ?>
                <?php echo h($info['contac']['Contac']['number']); ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Audio'); ?></strong></td>
        <td colspan="2">
             <a  href="<?php echo Router::url("/", true) ."contacs/audioPlay/".$info['contac']['Contac']["id"] ?>"> <img width="80%" src="<?php echo Router::url("/", true) . 'img/frontend/website/email/play.png' ?>"></a>
        </td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Company'); ?></strong></td>
        <td colspan="2"><?php echo h($info['contac']['Team']['name']); ?></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Start date and hour'); ?></strong></td>
        <td colspan="2"><?php echo h($this->Time->format('d-m-Y h:i A', $info['contac']['Contac']['start_date'])); ?></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('End date and time'); ?></strong></td>
        <td colspan="2"><?php echo h($this->Time->format('d-m-Y h:i A', $info['contac']['Contac']['end_date'])); ?></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Creation date'); ?></strong></td>
        <td colspan="2">
            <?php if ($info['contac']['Contac']['state'] == Configure::read('ENABLED')): ?>
                <?php echo h($this->Time->format('d-m-Y h:i A', $info['contac']['Contac']['created'])); ?>
            <?php else: ?>
                <?php echo h($this->Time->format('d-m-Y h:i A', $info['contac']['Contac']['modified'])); ?>
            <?php endif; ?>
        </td>
    </tr>

    <?php if ($info['contac']['Contac']['state'] == Configure::read('ENABLED')): ?>
        <tr colspan="3" valign="top" align="left">
            <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Date and time of last modification'); ?></strong></td>
            <td colspan="2">
                <?php echo h($info['contac']['Contac']['modified']); ?>
            </td>
        </tr>
    <?php endif; ?>



    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('State'); ?></strong></td>
        <td colspan="2"><?php echo __("Finished"); ?></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Client'); ?></strong></td>
        <td colspan="2"><p id="lbl_cliente"><?php echo $info['contac']['Client']['name']; ?></p></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:#101E2D;text-transform: uppercase;" ><?php echo __('Project'); ?></strong></td>
        <td colspan="2">
            <p id="lbl_proyecto">
                <?php echo $info['contac']['Project']['name']; ?>
            </p>
        </td>
    </tr>


    <tr colspan="3" valign="top" align="left" bgcolor="#101E2D">
        <td colspan="3"><strong style="color:#FFFFFF;text-transform: uppercase;" ><?php echo __('Client assistants'); ?></strong></td>
    </tr>

    <?php if (!empty($info['users']["assistants"])): ?>
        <tr colspan="3" valign="top" align="left">
            <td colspan="3"><span>
                    <?php foreach ($info['users']["assistants"] as $externo): ?>                                     
                        <?php echo $externo . "<br />\n"; ?>
                    <?php endforeach; ?></span>
            </td>
        <?php else: ?>
            <td colspan="3">
                <p id="selectExternos">
                    <?php echo __("No external assistants added to the minute.") ?>
                </p></strong></td>
        </tr>
    <?php endif; ?>


    <tr colspan="3" valign="top" align="left" bgcolor="#101E2D">
        <td colspan="3"><strong style="color:#FFFFFF;text-transform: uppercase;" ><?php echo __('Users') ?></strong></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <?php if (!empty($info['users']["collaborators"])): ?>
            <td colspan="3">
                <?php foreach ($info['users']["collaborators"] as $user): ?>                                     
                    <?php echo $user . "<br />\n"; ?>
                <?php endforeach; ?>
            </td>                                    
        <?php else: ?>
            <td colspan="3">
                <p id="selectExternos">
                    <?php echo __("No employees on the minute.") ?>
                </p>
            </td>
        <?php endif; ?>
    </tr>

    <tr colspan="3" valign="top" align="left" bgcolor="#101E2D">
        <td colspan="3"><strong style="color:#FFFFFF;text-transform: uppercase;" ><?php echo __('Backup to Email'); ?></strong></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <?php $copies = explode(',', $info['contac']['Contac']['copies']); ?>
        <?php if (!empty($copies["0"])): ?> 
            <td colspan="3">
                <p id="lbl_copias"><?php echo implode("<br />", $copies); ?></p>  
            </td>
        <?php else: ?>
            <td colspan="3">
                <p id="lbl_copias"><?php echo __("There are no Email backups added to the minute.") ?></p>  
            </td>
        <?php endif; ?>                             
    </tr>


    <tr colspan="3" valign="top" align="left">
        <td colspan="1"><strong style="color:black;text-transform: uppercase;" ><?php echo __('Minute creator'); ?></strong></td>
        <td colspan="2"><?php echo $info['contac']['User']['firstname'] . ' ' . $info['contac']['User']['lastname']; ?></td>
    </tr>


    <tr colspan="3" valign="top" align="center" bgcolor="#101E2D">
        <td colspan="3"><strong style="color:#FFFFFF;text-transform: uppercase;" ><?php echo __('Commitments'); ?></strong></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td rowspan="1" bgcolor="#101E2D"><strong style="color:#1e212a;width:300px;text-transform: uppercase;"><?php echo __('Description'); ?></strong></td>
        <td rowspan="1" bgcolor="#101E2D"><strong style="color:#1e212a;width:200px;text-transform: uppercase;" id="lbl_cliente"><?php echo __('User'); ?></strong></td>
        <td rowspan="1" bgcolor="#101E2D"><strong style="color:#1e212a;width:100px;text-transform: uppercase;" id="lbl_cliente"><?php echo __('Deadline'); ?></strong></td>
    </tr>


    <?php if (!empty($info['commitments'])): ?>
        <?php foreach ($info['commitments'] as $commitment): ?>
            <tr> 
                <td style="border-collapse: collapse;width:300px;"><?php echo $commitment["Commitment"]["description"]; ?></td>
                <?php
                $user = "";

                $user = $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"] . ' - ' . $commitment["User"]["email"];
                ?>
                <td style="border-collapse: collapse;width:200px;"><?php echo $user ?></td> 
                <td style="border-collapse: collapse;width:100px;"><?php echo h($this->Time->format('d-m-Y', $commitment["Commitment"]["delivery_date"])); ?></td> 
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="3">
                <span class="text-center">
                    <?php echo __("There are not commitments added on the minute.") ?>
                </span>
            </td>                           
        </tr>
    <?php endif; ?>  


    <tr colspan="3" valign="top" align="center" bgcolor="#101E2D">
        <td colspan="3"><strong style="color:#FFFFFF;text-transform: uppercase;" ><?php echo __('Description') ?></strong></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <td colspan="3"><p id="lbl_description"><?php echo $info['contac']['Contac']['description']; ?></p></td>
    </tr>



    <tr colspan="3" valign="top" align="center" bgcolor="#101E2D">
        <td colspan="3"><strong style="color:#FFFFFF;text-transform: uppercase;" ><?php echo __('Minute images') ?></strong></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <?php if (!empty($info['contac']["ContacsFile"])): ?>
            <td colspan="3">
                <div> 
                <?php foreach ($info['contac']["ContacsFile"] as $file): ?>  
                    <img style="height:auto; width:100px; float:left; padding:10px;" src="<?php echo Router::url("/", true) . "files/Contac/{$file["img"]}" ?>"> <span style="width:10px; float:left"> </span>                                           
                <?php endforeach; ?>
                </div>
            </td>                                    
        <?php else: ?>
            <td colspan="3"> 
                <?php echo __("No images on minute.") ?> 
            </td>
        <?php endif; ?>
    </tr>


    <tr colspan="3" valign="top" align="center" bgcolor="#101E2D">
        <td colspan="3"><strong style="color:#FFFFFF;text-transform: uppercase;" ><?php echo __('Minute documents') ?></strong></td>
    </tr>

    <tr colspan="3" valign="top" align="left">
        <?php if (!empty($info['contac']["ContacsDocument"])): ?>
            <td colspan="3">
                <div style="display: flex;flex-direction: row;flex-wrap: wrap;justify-content: flex-start;align-items: flex-start;">
                <?php $numberDocument = 1; ?>
                <?php foreach ($info['contac']["ContacsDocument"] as $document): ?>
                    <?php
                    $filename = $document['document'];
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    ?> 
                    <?php if ($extension == "docx" || $extension == "doc"): ?>                           
                        <a style="color: #0f1c2c; font-size: 14px; display:inline-block;margin-right:15px;"  target="_blank" href="<?php echo Router::url("/", true) . "document/Contac/{$document['document']}" ?>">
                            <img src="<?php echo Router::url("/", true) . "img/word.png" ?>" height="auto" width="60px">                                             
                        </a> 
                    <?php elseif ($extension == "pdf" || $extension == "PDF"): ?>
                        <a style="color: #0f1c2c; font-size: 14px; display:inline-block;margin-right:15px;"  target="_blank" href="<?php echo Router::url("/", true) . "document/Contac/{$document['document']}" ?>">
                            <img src="<?php echo Router::url("/", true) . "img/pdf.png" ?>"  height="auto" width="60px">     
                        </a>
                    <?php else : ?>
                        <a style="color: #0f1c2c; font-size: 14px; display:inline-block;margin-right:15px;"  target="_blank" href="<?php echo Router::url("/", true) . "document/Contac/{$document['document']}" ?>">
                            <img src="<?php echo Router::url("/", true) . "document/Contac/default.png" ?>"  height="auto" width="60">   
                        </a>
                    <?php endif; ?>

                    <?php $numberDocument++; ?>
                <?php endforeach; ?> 
                </div>
            </td>                                    
        <?php else: ?>
            <td colspan="3"> 
                <?php echo __("No documents on minute") ?> 
            </td>
        <?php endif; ?>
    </tr> 

    <tr colspan="3" valign="top" align="center" bgcolor="#101E2D">
        <td colspan="3" align="center">
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="mbtn20 mtop10" cellmargin="0">
                <tr>
                    <td width="200"></td>
                    <td width="200" align="center"><a href="<?php echo Router::url('/', true) ?>contacs/download_contac/<?php echo EncryptDecrypt::encrypt($info["contac"]["Contac"]["id"]); ?>.pdf" style="color:#1e212a; text-decoration:underline;"><span style="font-family:arial; font-size:14px;color:#1e212a;font-weight:bold;"><?php echo __("Download PDF") ?></span></a></td>
                    <td width="200"></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr colspan="3" valign="top" align="center">
        <td colspan="3">
          <span style="color:orangered;font-size:16px;"><?php echo __('A solution of StrategeeSuite.com ')?><?php echo __('© 1993-') ?><?php echo date('Y'); ?></span> <a href="http://strategeesuite.com/legal" style="color:orangered;font-size:16px;text-decoration:none;"><span><?php echo __('Strategee, LLC. or its affiliates.') ?></span></a>
        </td>
    </tr>


    

</table>



























