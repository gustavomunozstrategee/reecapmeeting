<table align="center" bgcolor="#ffffff" border="0" width="600" cellspacing="5" cellpadding="5">

<tr colspan="3" valign="top" align="center">
    <td colspan="3">
        <img src="<?php echo  Router::url("/",true).'img/frontend/website/email/bg-email.png'?>" style="width:100%;">
    </td>
</tr>

<tr colspan="3" valign="top" align="center">
  <td colspan="3"><strong style="color:#1e212a;font-size:18px;"><?php echo __('You’ve pending commitments to do') ?></strong></td>
</tr>


<tr colspan="3" valign="top" align="center">
<td colspan="1" style="width:200px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Description') ?></strong></td>
<td colspan="1" style="width:200px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Deadline') ?></strong></td>
<td colspan="1" style="width:200px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Actions') ?></strong></td>
</tr>
<br>

                       <?php foreach ($commitments as $commitment):?>
                         <tr colspan="3">
                             <td colspan="1" style="width:200px;border-collapse:collapse;text-align: center;"><span style="color:#808080;"><?php echo $commitment["description"]; ?></span></td>
                             <td colspan="1" style="width:200px;border-collapse:collapse;text-align: center;"><span style="color:#808080;"><?php echo $commitment["delivery_date"]?></span></td>
                             <td colspan="1" style="width:200px;border-collapse:collapse;text-align: center;" width="200" align="center"><a href="<?php echo Router::url('/', true) ?>commitments/commitment_pendings/<?php echo EncryptDecrypt::encrypt($commitment["id"]);?>"  style="color:#1e212a; text-decoration:underline;"><span style="font-family:arial; font-size:14px;color:#1e212a;font-weight:bold;"><?php echo __('Realizar') ?></span></a></td>
                        </tr>
                        <?php endforeach; ?>  

                        <tr colspan="3" valign="top" align="center">
                          <td colspan="3">
                            <span style="color:#969696;font-size:12px;"><?php echo __('A solution of StrategeeSuite.com ')?><?php echo __('© 1993-') ?><?php echo date('Y'); ?></span> <a href="http://strategeesuite.com/legal" style="color:#969696;font-size:12px;text-decoration:none;"><span><?php echo __('Strategee, LLC. or its affiliates.') ?></span></a>
                          </td>
                        </tr>

                   <tr colspan="3" align="center">
                      <td colspan="3">
                        <table style="width: 600px;background-color:#FBFBFB; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                        <tr align="center">
               
                        <td style="width:200px;">
                           
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("INSTAGRAM_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/instagram.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("FACEBOOK_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/facebook.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("YOUTUBE_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/youtube.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                            <a href="<?php echo configure::read("TWITER_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/twitter.png'?>"></a>
                        </td>

                        <td style="width:40px;">
                           <b style="color:#21C910;">|</b>
                        </td>

                        <td style="width:40px;">
                            <a href="https://recapmeeting.com/" target="_blank" style="color:#21C910;"><?php echo __("www.recapmeeting.com") ?></a>
                        </td>

                        <td style="width:200px;">
                          <a href="https://strategeesuite.com/legal" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/logo-strategeesuite.png'?>"></a>
                        </td>
                    </tr>
                </table> 
                </td>
            </tr>

</table>
















