<table align="center" bgcolor="#ffffff" border="1" width="600" cellspacing="5" cellpadding="5">

<tr colspan="2">
   <td colspan="2">
      <img src="<?php echo  Router::url("/",true).'img/frontend/website/email/bg-email.png'?>" style="width:100%;">
   </td>
</tr>

<tr colspan="2" valign="top" align="center">
  <td colspan="2"><strong style="color:#1e212a;font-size:18px;"><?php echo __("Información de la reunión de seguimiento citada")?></strong></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td colspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Creador de la reunión'); ?></strong></td>
  <td colspan="1" style="width:300px;border-collapse:collapse;"><?php echo h($meetingDetail['User']['firstname'] . ' ' . $meetingDetail['User']['lastname']); ?></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td colspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Empresa'); ?></strong></td>
  <td colspan="1" style="width:300px;border-collapse:collapse;"><span><?php echo h($meetingDetail['Team']['name']); ?></span></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td rowspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Cliente'); ?></strong></td>
  <td rowspan="1" style="width:300px;border-collapse:collapse;"><span><?php echo h($meetingDetail['Client']['name']); ?></span></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td rowspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Asunto'); ?></strong></td>
  <td rowspan="1" style="width:300px;border-collapse:collapse;"><span><?php echo h($meetingDetail['Meeting']['subject']); ?></span></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td rowspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Tipo de reunión'); ?></strong></td>
  <td rowspan="1" style="width:300px;border-collapse:collapse;"><span><?php echo h(__($meetingDetail['Meeting']['meeting_type'])); ?></span></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td colspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Lugar de la reunión'); ?></strong></td>
  <td colspan="1" style="width:300px;border-collapse:collapse;"><span><?php echo h($meetingDetail['Meeting']['location']); ?></span></td>
</tr>

<tr colspan="2" valign="top" align="center" bgcolor="#f2f2f2">
  <td colspan="2"><strong style="color:#1e212a;"><?php echo __('Descripción'); ?></strong></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td colspan="2"><span style="color:#1e212a;"><?php echo h($meetingDetail['Meeting']['description']); ?></span></td>
</tr>

<?php if ($meetingDetail['Meeting']['private'] != 1): ?>
<tr colspan="2" valign="top" align="left">
  <td colspan="1"><strong style="color:#1e212a;"><?php echo __('Reunión privada, la contraseña es:'); ?></strong></td>
  <td colspan="1"><span><?php echo h($meetingDetail['Meeting']['password']); ?></span></td>
</tr>
<?php endif ?>

<tr colspan="2" valign="top" align="center" bgcolor="#f2f2f2">
  <td colspan="2"><strong style="color:#1e212a;"><?php echo __('Asistentes') ?></strong></td>
</tr>

<tr colspan="2" valign="top" align="left">
  <td colspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Nombre') ?></strong></td>
  <td colspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e121a;"><?php echo __('Correo electrónico') ?></strong></td>
</tr>

<?php foreach ($meetingDetail['Assistant'] as $value): ?>
<tr colspan="2" valign="top" align="left">
  <td colspan="1" style="width:300px;border-collapse:collapse;"><span><?php echo !empty($value['User']['firstname']) ? $value['User']['name'] : __("Nombre no asignado por parte del usuario.") ?></span></td>
  <td colspan="1" style="width:300px;border-collapse:collapse;"><span><?php echo $value['User']['email'] ?></span></td>
</tr>
<?php endforeach ?>


<tr colspan="2" valign="top" align="center" bgcolor="#f2f2f2">
  <td colspan="2"><strong style="color:#1e212a;"><?php echo __('Tags') ?></strong></td>
</tr>


<tr colspan="2" valign="top" align="left">
  <td colspan="2">
    <?php if (isset($meetingDetail['Tag'][0])): ?>
    <?php foreach ($meetingDetail['Tag'] as $value): ?>
       <span  style="color:#1e212a;border:1px solid #1e212a;border-radius:5px;float:left;margin:5px;padding:5px;"><?php echo h($value['name']) ?></span>
    <?php endforeach ?>
    <?php else: ?>
       <span><?php echo __('No hay tags registrados.') ?></span>
    <?php endif; ?>
  </td>
</tr>

<tr colspan="2" valign="top" align="center" bgcolor="#f2f2f2">
  <td colspan="2"><strong style="color:#1e212a;"><?php echo __('Fecha y hora de los días citados a la reunión') ?></strong></td>
</tr>

<?php foreach ($meetingsFollowings as $meetingsFollowing): ?>
  <tr colspan="2" valign="top" align="center" bgcolor="#f2f2f2">
    <td colspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Fecha y hora inicial') ?></strong></td>
    <td colspan="1" style="width:300px;border-collapse:collapse;"><strong style="color:#1e212a;"><?php echo __('Fecha y hora final') ?></strong></td>
  </tr>

  <tr colspan="2" valign="top" align="center" bgcolor="#f2f2f2">
    <td colspan="1" style="width:300px;border-collapse:collapse;"><?php echo  h($this->Time->format('d-m-Y h:i A', $meetingsFollowing['Meeting']['start_date'])) ?></td>
    <td colspan="1" style="width:300px;border-collapse:collapse;"><?php echo  h($this->Time->format('d-m-Y h:i A', $meetingsFollowing['Meeting']['end_date'])) ?></td>
  </tr>

    <?php if(!empty($meetingsFollowing["Topic"])):?>
      <tr colspan="2" valign="top" align="center" bgcolor="#f2f2f2">
        <td colspan="2"><strong style="color:#1e212a;"><?php echo __('Temas específicos') ?></strong></td>
      </tr>
      <tr colspan="2">
        <td colspan="1" style="width:300px;border-collapse:collapse;"><?php echo __('Descripción') ?></td>
        <td colspan="1" style="width:300px;border-collapse:collapse;"><?php echo __('Tiempo') ?></td>
      </tr>
      <?php foreach ($meetingsFollowing["Topic"] as $topic): ?>
        <tr colspan="2">
          <td colspan="1" style="width:300px;border-collapse:collapse;"><?php echo h($topic['description']) ?></td>
          <td colspan="1" style="width:300px;border-collapse:collapse;"><?php echo  $this->Utilities->conversorSegundosHoras($topic["time"]); ?></td>
        </tr>
      <?php endforeach; ?>
    <?php else: ?>
      <td colspan="2" aling="center">        
        <span><?php echo __('No hay temas específicos en esta reunión.') ?></span>
      </td> 
    <?php endif; ?>
<?php endforeach; ?>

<tr colspan="2" align="center">
    <td colspan="2" style="width:300px;border-collapse:collapse;">
       <span style="color:#969696;font-size:12px;"><?php echo __('A solution of StrategeeSuite.com ')?><?php echo __('© 1993-') ?><?php echo date('Y'); ?></span> <a href="http://strategeesuite.com/legal" style="color:#969696;font-size:12px;text-decoration:none;"><span><?php echo __('Strategee, LLC. or its affiliates.') ?></span></a>
    </td>
</tr>

<tr colspan="2" align="center">
<td colspan="2" align="center">
<table style="width: 600px;background-color:#FBFBFB; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                   
                    <tr align="center">
               
                        <td style="width:200px;">
                           
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("INSTAGRAM_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/instagram.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("FACEBOOK_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/facebook.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("YOUTUBE_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/youtube.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                            <a href="<?php echo configure::read("TWITER_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/twitter.png'?>"></a>
                        </td>

                        <td style="width:40px;">
                           <b style="color:#21C910;">|</b>
                        </td>

                        <td style="width:40px;">
                            <a href="https://recapmeeting.com/" target="_blank" style="color:#21C910;"><?php echo __("www.recapmeeting.com") ?></a>
                        </td>

                        <td style="width:200px;">
                          <a href="https://strategeesuite.com/legal" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/logo-strategeesuite.png'?>"></a>
                        </td>

                    </tr>
                </table>  

 </td>
</tr>

</table>



 
          
              
                  

              
                       







