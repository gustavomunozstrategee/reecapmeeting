
<!DOCTYPE html>
<html lang="es">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="utf-8" />
</head>
<body style="background-color:#f8f8f8;">
    <table align="center" style="width: 600px; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;background-color:#ffffff;">
        <tr>
            <td>
                <table style="width: 600px;  display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr>
                        <td>
                             <img src="<?php echo  Router::url("/",true).'img/frontend/website/email/bg-email.png'?>">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
				<table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
				    <tr align="center">
				        <td style="width:50px;"></td>
				        <td style="width:500px;">
				            <br>
				            <p><?php echo __("Hola, Para recuperar tu contraseña por favor utiliza el siguiente código: ").$hash;?></p>
				        </td>
				        <td style="width:50px;"></td>
				    </tr>
				</table>
                <br>

                <table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr align="center">
                        <td style="width:50px;"></td>
                        <td style="width:500px;">
                           <p style="color:#969696;"><b><?php echo __("Saludos,") ?></b><br><?php echo __("El equipo de RecapMeeting.") ?></p>
                        </td>
                        <td style="width:50px;"></td>
                    </tr>
                </table>
                <br>   
                <table border="0" align="center" cellpadding="0" cellspacing="0" class="mbtn20 mtop10" cellmargin="0">     
                    <tr>
                        <td width="25"></td>
                        <td width="550" height="30" align="center">                
                            <span style="color:#969696;font-size:12px;"><?php echo __('A solution of StrategeeSuite.com ')?><?php echo __('© 1993-') ?><?php echo date('Y'); ?></span> <a href="http://strategeesuite.com/legal" style="color:#969696;font-size:12px;text-decoration:none;"><span><?php echo __('Strategee, LLC. or its affiliates.') ?></span></a>
                        </td>
                        <td width="25"></td>
                    </tr>
                </table>
                <br>  
            </td>
        </tr>
    </table>
</body>
</html>










