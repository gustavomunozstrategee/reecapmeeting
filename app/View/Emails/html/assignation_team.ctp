<!DOCTYPE html>
<html lang="es">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="utf-8" />
</head>
<body style="background-color:#f8f8f8;">
    <table align="center" style="width: 600px; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;background-color:#ffffff;">
        <tr>
            <td>
                <table style="width: 600px;  display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr>
                        <td>
                             <img src="<?php echo  Router::url("/",true).'img/frontend/website/email/bg-email.png'?>">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <?php if($userState == "user_new"):?>
                    <table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                        <tr align="center">
                            <td style="width:25px;"></td>
                            <td style="width:550px;">
                                <?php $assigned = true; ?>
                                <br>
                                <b style="color:#808080;"><?php echo __('Hola')?></b>
                                <p style="color:#808080;"><?php echo $subject?><br>
                                <?php echo __("Activa tu cuenta y completa el registro para que disfrutes de las novedades de cada empresa a la que seas invitado como esta.") ?></p>
                            </td>
                            <td style="width:25px;"></td>
                        </tr>
                    </table>
                    <br>
                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="mbtn20 mtop10" cellmargin="0">
                        <tr>
                            <td width="200"></td>
                            <td width="200" align="center"><a href="<?php echo Router::url('/', true) ?>users/activar_cuenta/<?php echo base64_encode($correo);?>/<?php echo $assigned;?>" style="color:#1e212a; text-decoration:underline;"><span style="font-family:arial; font-size:14px;color:#1e212a;font-weight:bold;"><?php echo __("Activar cuenta") ?></span></a></td>
                            <td width="200"></td>
                        </tr>
                    </table>
                <?php else: ?>
                    <table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                        <tr align="center">
                            <td style="width:50px;"></td>
                            <td style="width:500px;">
                                <br>
                                <b style="color:#808080;"><?php echo __('Hola') . ' ' . $userInvited?></b>
                                <p style="color:#808080;"><?php echo $subject?><br>
                                <?php echo __("Ingresa para que disfrutes de las novedades de cada empresa a la que seas invitado como este.") ?></p>
                            </td>
                            <td style="width:50px;"></td>
                        </tr>
                    </table>
                    <br>
                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="mbtn20 mtop10" cellmargin="0">
                        <tr>
                            <td width="200"></td>
                            <td width="200" align="center"><a href="<?php echo Router::url('/', true) ?>users/login" style="color:#1e212a; text-decoration:underline;font-weight:bold;"><span style="font-family:arial; font-size:14px;color:#1e212a;"><?php echo __("Iniciar") ?></span></a></td>
                            <td width="200"></td>
                        </tr>
                    </table> 
                <?php endif;?>
                <br>
                <table style="width: 600px; background-color:#ffffff; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr align="center">
                        <td style="width:50px;"></td>
                        <td style="width:500px;">
                           <p style="color:#969696;"><b><?php echo __("Saludos,") ?></b><br><?php echo __("El equipo de RecapMeeting.") ?></p>
                        </td>
                        <td style="width:50px;"></td>
                    </tr>
                </table>
                <br>   
                <table border="0" align="center" cellpadding="0" cellspacing="0" class="mbtn20 mtop10" cellmargin="0">     
                    <tr>
                        <td width="25"></td>
                        <td width="550" height="30" align="center">                
                            <span style="color:#969696;font-size:12px;"><?php echo __('A solution of StrategeeSuite.com ')?><?php echo __('© 1993-') ?><?php echo date('Y'); ?></span> <a href="http://strategeesuite.com/legal" style="color:#969696;font-size:12px;text-decoration:none;"><span><?php echo __('Strategee, LLC. or its affiliates.') ?></span></a>
                        </td>
                        <td width="25"></td>
                    </tr>
                </table>
                <br>            
                <table style="width: 600px;background-color:#FBFBFB; color:#111E2D; display:block; margin: 0 auto; border-collapse: collapse; font-family: Helvetica, Arial, Sans-Serif;">
                    <tr align="center">
               
                        <td style="width:200px;">
                           
                        </td>

                        <td style="width:30px;">
                           <a href="<?php echo configure::read("INSTAGRAM_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/instagram.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                          <a href="<?php echo configure::read("FACEBOOK_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/facebook.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                            <a href="<?php echo configure::read("YOUTUBE_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/youtube.png'?>"></a>
                        </td>

                        <td style="width:30px;">
                            <a href="<?php echo configure::read("TWITER_URL_SUITE")?>" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/twitter.png'?>"></a>
                        </td>
                        
                        <td style="width:40px;">
                           <b style="color:#21C910;">|</b>
                        </td>

                        <td style="width:40px;">
                            <a href="https://recapmeeting.com/" target="_blank" style="color:#21C910;"><?php echo __("www.recapmeeting.com") ?></a>
                        </td>

                        <td style="width:200px;">
                          <a href="https://strategeesuite.com/legal" target="_blank"><img src="<?php echo  Router::url("/",true).'img/frontend/website/email/logo-strategeesuite.png'?>"></a>
                        </td>
                    </tr>
                </table>    
            </td>
        </tr>
    </table>
</body>
</html>








