<?php $permissionAction = $this->Utilities->check_team_permission_action(array("index"), array("contacs")); ?>
<!-- Begin Page Header-->
<div class="row">
	<div class="page-header">
		<div class="d-flex align-items-center">
			<h2 class="page-header-title"><?php echo __("Información del acta")?></h2>
			<div>
				<ul class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo Router::url("/",true) ?>">
							<i class="ti ti-home"></i>
						</a>
					</li>
					<li class="breadcrumb-item">
						<a href="<?php echo $this->Html->url(array('action'=>'index',"controller"=>"contacs"));?>"><?php echo __("Actas") ?></a>
					</li>
					<li class="breadcrumb-item active"><?php echo __("Información del acta")?></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- End Page Header -->
<div class="row flex-row">
	<div class="col-12 col-md-9 col-lg-9">
		<div class="widget has-shadow">

			<div class="widget-header bordered no-actions d-flex align-items-center">

				<h2>
					<?php echo __("Calificar acta") ?>
				</h2>

				<div class="widget-options">
					<?php if(isset($permissionAction["index"])) : ?>
						<div class="" role="">
								<a href="<?php echo $this->Html->url(array("controller" => "contacs","action" => "edit", EncryptDecrypt::encrypt($contac['contac']['Contac']['id'])))?>"  data-toggle="tooltip" data-placement="right"  title="<?php echo __("Editar acta rechazada");?>" class="btn-circle-icon" ><i class="flaticon-draw"></i></a>
						</div>
					<?php endif;?>
				</div>
			</div>
			<div class="widget-body px-5">
				<div class="form-horizontal" id="content-table">
					<div class="form-group row d-flex align-items-center border-bottom">
                        <label class="col-lg-3 form-control-label">
                            <?php echo __('Estado'); ?>
                        </label>
                        <div class="col-lg-9">
                            <?php echo $this->Utilities->stateContac($contac['contac']['Contac']['state']); ?>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center border-bottom">
                        <label class="col-lg-3 form-control-label">
                            <?php echo __('Fecha y hora de la última modificación'); ?>
                        </label>
                        <div class="col-lg-9">
                            <?php echo h($this->Time->format('Y-m-d H:i:s', $contac['contac']['Contac']['modified'])); ?>
                        </div>
                    </div>
					<div class="form-group row d-flex align-items-center border-bottom">
						<label class="col-lg-3 form-control-label"><?php echo __('Empresa'); ?></label>
						<div class="col-lg-9">
							<label id="lbl_team"><?php echo $contac['contac']['Team']['name']; ?> </label>

						</div>
					</div>
					<?php if(!empty($contac['contac']['Team']['img'])):?>
						<div class="form-group row d-flex align-items-center border-bottom">
							<label class="col-lg-3 form-control-label">
								<?php echo __('Logo'); ?>
							</label>
							<div class="col-lg-9">
								<a href="<?php echo $this->Html->url('/document/WhiteBrand/'.$contac['contac']['Team']['img']); ?>" class="showImages">                    
									<img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$contac['contac']['Team']['img']); ?>" width="100px" class="img-responsive logo-add hidden-xs" alt=""/>
								</a>
							</div>
						</div>
					<?php endif; ?>
					<div class="form-group row d-flex align-items-center border-bottom">
						<label class="col-lg-3 form-control-label">
							<?php echo __('Cliente'); ?>
							<div id="show_img_client"></div>
						</label>
						<div class="col-lg-9">
							<label id="lbl_cliente"><?php echo $contac['contac']['Client']['name']; ?></label>
						</div>
					</div>
					<div class="form-group row d-flex align-items-center border-bottom">
						<label class="col-lg-3 form-control-label">
							<?php echo __('Proyecto'); ?>
							<div id="show_img_project"></div> 
						</label>
						<div class="col-lg-9">
							<label id="lbl_proyecto"><?php echo $contac['contac']['Project']['name']; ?></label>
						</div>
					</div>
					<div class="form-group row d-flex align-items-center border-bottom">
						<label class="col-lg-3 form-control-label">
							<?php echo __('Número del acta'); ?>
						</label>
						<div class="col-lg-9">
							<?php if ($contac['contac']['Contac']['state'] == Configure::read('ENABLED') || $contac['contac']['Contac']['state'] == Configure::read('APPROVAL')): ?>
								<?php echo $this->Utilities->stateContac($contac['contac']['Contac']['state']); ?>
							<?php else: ?>
								<?php echo h($contac['contac']['Contac']['number']); ?>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 font-weight-bolder form-control-label ">
                            <?php echo __('Duración de la reunión'); ?>
                        </label>
                    </div>
                    <div class="form-group row d-flex align-items-center ">
                        <label class="col-lg-3 form-control-label">
                            <?php echo __('Fecha y hora del inicio de la reunión'); ?> 
                        </label>
                        <div class="col-lg-9">
                            <?php echo h($this->Time->format('d-m-Y h:i A', $contac['contac']['Contac']['start_date'])); ?>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center border-bottom">
                        <label class="col-lg-3 form-control-label">
                            <?php echo __('Fecha y hora fin de la reunión'); ?> 
                        </label>
                        <div class="col-lg-9">
                            <?php echo h($this->Time->format('d-m-Y h:i A', $contac['contac']['Contac']['end_date'])); ?>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center border-bottom">
                        <label class="col-lg-3 form-control-label">
                            <?php echo __('Asistentes a la reunión'); ?> 
                        </label>
                        <div class="col-lg-9">
                                <div class="input-form">
                                    <h3 class="form-control-label">
                                        <?php echo __('Colaboradores'); ?>     
                                    </h3>
                                </div>
                                
                                <?php if(!empty($contac["users"]['assistants'])): ?>
                                    <ul class="list-group">
                                        <?php foreach ($contac["users"]['collaborators'] as $collaborator): ?>                                             
                                            <?php echo "<li class='mb-3 list-group-item'>". $collaborator . "</li>"; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <p>
                                        <?php echo __("No se agregaron colaboradores al acta."); ?>
                                    </p>
                                <?php endif; ?>

                                <div class="input-form">
                                    <h3 class="form-control-label">
                                        <?php echo __('Asistentes externos'); ?>
                                    </h3>
                                </div>

                                <?php if(!empty($contac["users"]['assistants'])): ?>
                                    <ul class="list-group">
                                        <?php foreach ($contac["users"]['collaborators'] as $assistant): ?>
                                            <?php echo "<li class='mb-3 list-group-item'>". $assistant . "</li>\n"; ?>                      
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                      <p><?php echo __("No se agregaron asistentes externos al acta."); ?></p>
                                <?php endif; ?> 
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center border-bottom">
                        <label class="col-lg-3 form-control-label">
                            <?php echo __('Copias al correo electrónico') ?>
                        </label>
                        <div class="col-lg-9">
                            <?php if ($contac['contac']['Contac']['state'] == Configure::read('DISABLED')) { ?>
                                <?php $copies = explode(',', $contac['contac']['Contac']['copies']); ?>
                                <div class="col-70-flex">
                                    <?php if(!empty($copies["0"])): ?>
                                        <div class="form-group">
                                            <p><?php echo implode("<br />", $copies); ?></p>
                                        </div>
                                    <?php else: ?>
                                        <div class="form-group">
                                            <p><?php echo __("No hay copias de correo electrónico agregadas al acta.") ?></p>
                                        </div> 
                                    <?php endif;?>
                               </div>
                            <?php } else { ?>
                                <div class="col-70-flex">
                                    <div class="form-group">
                                        <p id="lbl_copias">
                                            <?php echo __("No hay copias de correo electrónico agregadas al acta.") ?>
                                        </p>
                                    </div>
                              </div>
                            <?php } ?> 
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center border-bottom mb-2">
                        <label class="col-lg-3 form-control-label">
                            <?php echo __('Creador del acta'); ?>
                        </label>
                        <div class="col-lg-9">
                            <?php if($contac['contac']['Contac']['user_id'] == Authcomponent::user("id")):?>
                                <?php echo $this->Html->link($contac['contac']['User']['firstname'].' '.$contac['contac']['User']['lastname'], array('controller' => 'users', 'action' => 'view', EncryptDecrypt::encrypt($contac['contac']['User']['id'])), array('class' => '')); ?>
                            <?php else: ?>
                                 <?php echo $contac['contac']['User']['firstname'].' '.$contac['contac']['User']['lastname']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <h3 class="col-lg-12 form-control-label form-control-label text-center">
                            <?php echo __('Descripción') ?>
                        </h3>
                    </div>
                    <div class="form-group row d-flex align-items-center border-bottom">
                        <div class="col-lg-12 div-word-wrap p-5">  
                                <?php echo $contac['contac']['Contac']['description'] ?>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <h3 class="col-lg-12 form-control-label form-control-label text-center">
                            <?php echo __('Compromisos') ?>
                        </h3>
                    </div>
                    <div class="form-group row d-flex align-items-center px-5 border-bottom">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<th class="col-25-commiments" style="width: 20%;">
										<label class="f-blue"><?php echo __("Fecha límite") ?></label>
									</th> 						
									<th class="col-30-commiments">
										<label class="f-blue"><?php echo __("Usuario"); ?></label>
									</th>
									<th class="col-30-commiments">
										<label class="f-blue"><?php echo __("Descripción") ?></label>
									</th>
								</thead>
								<tbody>
									<?php if(!empty($commitments)):?>
										<?php foreach ($commitments as $commitment): ?> 
											<tr>
												<td class="col-25-commiments div-word-wrap" style="width: 20%;"><?php echo $commitment["Commitment"]["delivery_date"]; ?></td>
												<td class="col-30-commiments div-word-wrap"><?php echo !empty($commitment["User"]["firstname"]) ? $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"] . ' - ' . $commitment["User"]["email"] : __("Nombre no asignado por parte del usuario") . ' - ' . $commitment["User"]["email"]?></td>
												<td class="col-30-commiments div-word-wrap"><?php echo $commitment["Commitment"]["description"]; ?></td>
											</tr>
										<?php endforeach; ?>
									<?php else: ?>
										<tr>
											<td colspan="3" align="center">
												<p>
													<?php echo __("No hay compromisos agregados al acta.")?>
												</p>
											</td>
										</tr>
									<?php endif;?> 		
								</tbody>
							</table>
						</div>
					</div>

				</div>

			</div>
			<div class="widget-body">
                <div class="form-horizontal px-5">
                	<div class="form-group row d-flex align-items-center">
                        <h3 class="col-lg-12 form-control-label form-control-label text-center">
                           <?php echo __('Imágenes agregadas al acta.') ?> &nbsp;
                        </h3>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <?php echo $this->Html->image('frontend/admin/icon-png.svg', array('height' => 'auto;','width' => '60px;')); ?>
                        <?php echo $this->Html->image('frontend/admin/icon-jpg.svg', array('height' => 'auto;','width' => '60px;')); ?>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div id="imageDropzone" class="imageDropzone-div row scroll-uploadimg border-bottom w-100">
                        	<?php if(!empty($contac['contac']["ContacsFile"])):?>
								<?php foreach ($contac['contac']["ContacsFile"] as $file): ?>	
									<div class="col-md-3">
										<a href="<?php echo $this->Html->url('/files/Contac/'.$file['img']); ?>" class="showImages">
											<div class="img-size-upload">  
												<div class="thumbnail">
													<?php echo $this->Html->image("/files/Contac/{$file['img']}",array('class' => 'img img-responsive img-thumbnail'))?> 					 	 
												</div>
											</div>
										</a>
									</div>
								<?php endforeach; ?>										 
							<?php else: ?>
								<p class="text-center">
									<?php echo __("No hay imágenes en el acta.")?>
								</p>

							<?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center py-4">
                        <h3 class="col-lg-12 form-control-label form-control-label text-center">
                           <?php echo __('Documentos agregados al acta.') ?> &nbsp;
                        </h3>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <?php echo $this->Html->image('frontend/admin/icon-pdf.svg', array('height' => 'auto;','width' => '60px;')); ?>
                        <?php echo $this->Html->image('frontend/admin/icon-word.svg', array('height' => 'auto;','width' => '60px;')); ?>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div id="documentDropzone" class="imageDropzone-div row scroll-uploadimg border-bottom w-100">
                        	<?php if(!empty($contac['contac']["ContacsDocument"])):?>
								<?php $numberDocument = 1;?>
								<?php foreach ($contac['contac']["ContacsDocument"] as $document): ?>
									<div class="col-md-3">
										<?php 
											$filename  = $document['document'];
											$extension = pathinfo($filename, PATHINFO_EXTENSION);
										?> 
									<?php if($extension == "docx" || $extension == "doc"): ?>							 
										<a class="document-upload"  target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
											<?php echo $this->Html->image('frontend/admin/icon-word.svg',array('width' => '100px','class'=>'img-responsive')); ?>
										</a> 
									<?php elseif ($extension == "pdf" || $extension == "PDF"): ?>
										<a class="document-upload"   target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
											<?php echo $this->Html->image('frontend/admin/icon-pdf.svg',array('width' => '100px','class'=>'img-responsive')); ?>
										</a>
									<?php else :?>
										<a class="document-upload"   target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
											<?php echo $this->Html->image('/document/Contac/default.png',array('width' => '100px','class'=>'img-responsive')); ?>
										</a>
									<?php endif; ?>
										<?php $numberDocument++;?>
									</div>
								<?php endforeach; ?> 								 
							<?php else: ?>
								<p class="text-center">
									<?php echo __("No hay documentos en el acta.")?>
								</p>
							<?php endif; ?>  
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<div class="col-md-3 col-lg-3 col-xl-3">
		<div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
            	<h2>
            		<?php echo __('Comentarios sobre el acta'); ?>
            	</h2>
            </div>
            <div class="widget-body">
            	<?php if (!empty($commentContacs)): ?>
            		<?php foreach ($commentContacs as $key => $commentContac): ?>
            			<div class="form-group row d-flex align-items-center border-bottom">
	                        <label class="col-lg-12 form-control-label">
	                            <?php echo __("Usuario calificador: ")?>
	                        </label>
	                        <div class="col-lg-12">
	                            <?php echo $commentContac["User"]["firstname"] . ' ' . $commentContac["User"]["lastname"]?>
	                        </div>
	                        <div class="col-lg-12">
	                        	<p class="state-acta">
	                        		<?php echo __("Rechazada")?>
	                        	</p>
	                        </div>
	                        <label class="col-lg-12 form-control-label">
	                        	<p class="state-acta ">
	                        		<?php echo __("Comentario") ?>
	                        	</p>
	                        </label>
	                        <div class="col-lg-12 form-control-div">
	                        	<p class="state-acta ">
	                        		<?php echo h($commentContac["CommentContac"]["comment"]) ?>
	                        	</p>
	                        </div>
	                    </div>
            			
            		<?php endforeach ?>
            	<?php else: ?>
            	<?php endif ?>
            </div>
        </div>
	</div>
</div>


<?php 
$this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']);
$this->Html->script('lib/sweetalert.min.js',				   ['block' => 'AppScript']);
?>





