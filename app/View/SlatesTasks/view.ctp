<div class="slatesTasks view">
<h2><?php echo __('Slates Task'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Slate'); ?></dt>
		<dd>
			<?php echo $this->Html->link($slatesTask['Slate']['name'], array('controller' => 'slates', 'action' => 'view', $slatesTask['Slate']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deadline Date'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['deadline_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Notidication'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['date_notidication']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Id'); ?></dt>
		<dd>
			<?php echo h($slatesTask['SlatesTask']['user_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Slates Task'), array('action' => 'edit', $slatesTask['SlatesTask']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Slates Task'), array('action' => 'delete', $slatesTask['SlatesTask']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $slatesTask['SlatesTask']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Slates Tasks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slates Task'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Slates'), array('controller' => 'slates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slate'), array('controller' => 'slates', 'action' => 'add')); ?> </li>
	</ul>
</div>
