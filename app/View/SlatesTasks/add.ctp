<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<div class="container-fluid">
                        <!-- Begin Page Header-->
                        <!-- End Page Header -->
                         <?php echo $this->element('nav_taskee'); ?>
                            <div class="col-xl-9">

<div class="col-xl-12">
 
                                <!-- Begin Widget 07 -->
                                <div class="widget widget-07 has-shadow">
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header bordered d-flex align-items-center">
                                        <h2><?php echo __("Adicionar Tarea a Lista: ").$Slate["Slate"]["name"] ?></h2>
                                        <div class="widget-options">
                                            <a href="<?php echo $this->webroot?>slates/view/<?php echo EncryptDecrypt::encrypt($Slate["Slate"]["id"]) ?>">
                                                <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-danger ripple"><i class="fa fa-plus-circle"></i><?php echo __("Cancelar") ?>
                                                </button>

                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- End Widget Header -->
                                    <!-- Begin Widget Body -->
                                    <div class="widget-body">

                                    	<?php echo $this->Form->create('SlatesTask', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40')); ?>
                         
                        <div class="row">
                            <div class="col-md-12">
                                <div class='form-group'>
                            <?php echo $this->Form->label('description',__('Descripción de la tarea'), array('class'=>'control-label'));?>
                            <?php echo $this->Form->input('description', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2")); ?>
                            
                             
                            </div>
                            </div>
                             
                            
                        </div>
                         
                        <div class="row">
                        	<div class="col-md-4">
                                

                                <div class="form-group">
                                         <?php echo $this->Form->label('Meeting.start_date',__('Fecha de Terminación'), array('class'=>'control-label f-blue'));?>
                                        <div class='input-group date ' id=''>
                                        	
                                            <?php echo $this->Form->input('deadline_date', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                            </span>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('SlatesTask.user_id',__('Usuario responsable'), array('class'=>'control-label select2'));?>
                                    <?php echo $this->Form->input('user_id', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('SlatesTask.project_id',__('Proyecto'), array('class'=>'control-label'));?>
                                    <?php echo $this->Form->input('project_id', array('class' => 'form-control border-input select','label'=>false,'div'=>false,"empty"=>__("Sin proyecto"),"style"=>"")); ?>
                                </div>
                            </div>

                            


                        </div>
                        
                        
                        
                        
                        
                        <div class="row">
                            <div class="col-md-12 pull-right">
                                <button type='submit' class='btn btn-primary pull-right'><?php echo __("Crear Tarea") ?></button>
                            </div>
                        </div>
                		<?php echo $this->Form->end(); ?>
                                         
                                        
                                    </div>
                                    <!-- End Widget Body -->
                                    <!-- Begin Widget Footer -->
                                     
                                 

                                    <!-- End Widget Footer -->
                                </div>
                                <!-- End Widget 07 -->
                            </div>
                                
                           
                            
                            </div>
                        <!-- End Row -->
                    </div>
 


<?php
    echo $this->element('modals_contact');
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
    echo $this->Html->css('frontend/admin/components/table.css'); 
?>


<script type="text/javascript">
    $( document ).ready(function() {
            $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });
    });
    
</script>
<style>
 #select2-CommitmentProjectId-container{
            border: solid 0.5px darkgrey;border-radius: 4px;
            }
</style>
 

