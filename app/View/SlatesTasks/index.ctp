<div class="slatesTasks index">
	<h2><?php echo __('Slates Tasks'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('slate_id'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('deadline_date'); ?></th>
			<th><?php echo $this->Paginator->sort('date_notidication'); ?></th>
			<th><?php echo $this->Paginator->sort('state'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($slatesTasks as $slatesTask): ?>
	<tr>
		<td><?php echo h($slatesTask['SlatesTask']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($slatesTask['Slate']['name'], array('controller' => 'slates', 'action' => 'view', $slatesTask['Slate']['id'])); ?>
		</td>
		<td><?php echo h($slatesTask['SlatesTask']['description']); ?>&nbsp;</td>
		<td><?php echo h($slatesTask['SlatesTask']['deadline_date']); ?>&nbsp;</td>
		<td><?php echo h($slatesTask['SlatesTask']['date_notidication']); ?>&nbsp;</td>
		<td><?php echo h($slatesTask['SlatesTask']['state']); ?>&nbsp;</td>
		<td><?php echo h($slatesTask['SlatesTask']['created']); ?>&nbsp;</td>
		<td><?php echo h($slatesTask['SlatesTask']['modified']); ?>&nbsp;</td>
		<td><?php echo h($slatesTask['SlatesTask']['user_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $slatesTask['SlatesTask']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $slatesTask['SlatesTask']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $slatesTask['SlatesTask']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $slatesTask['SlatesTask']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Slates Task'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Slates'), array('controller' => 'slates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slate'), array('controller' => 'slates', 'action' => 'add')); ?> </li>
	</ul>
</div>
