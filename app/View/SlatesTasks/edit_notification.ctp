<?php echo $this->Form->create('SlatesTask', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40')); ?>
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"><h4 class="modal-title" id="myModalLabel"><?php echo __("Editar Notificación") ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body contenidoModal">
        <?php echo $this->Form->label('description',__('Descripción de la tarea'), array('class'=>'control-label'));?>

        <?php echo $this->Form->input('slate_id', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2","hidden"=>"true")); ?>

        <?php echo $this->Form->input('description', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2","disabled")); ?>
        <div class="row">
          <div class="col-md-12">
                <div class="form-group">
                         <?php echo $this->Form->label('Meeting.start_date',__('Fecha de Notificación'), array('class'=>'control-label f-blue'));?>
                        <div class='input-group date ' id=''>
                          
                            <?php echo $this->Form->input('date_notidication', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                            </span>
                        </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
         
        <button type='submit' class='btn btn-success pull-right'><?php echo __("Guardar Cambios") ?></button>
      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>



<script type="text/javascript">
    $( document ).ready(function() {
         
            $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,

                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });

             
        
    });
    
</script>
<style>
 #select2-CommitmentProjectId-container{
            border: solid 0.5px darkgrey;border-radius: 4px;
            }
</style>
 
