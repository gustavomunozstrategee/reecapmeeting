<?php 

App::uses('HtmlHelper', 'View/Helper');

class UtilitiesHelper extends HtmlHelper {

	public $helpers = array('Html'); 

	public function UtilitiesHelper(){ 
		App::import("model", "UserTeam");
		App::import("model", "ApprovalContac");
		App::import("model", "PlanUser");
		App::import("model", "Contac");
		App::import("model", "QualificationContact");
		App::import("model", "Meeting");   
		App::import("model", "ProjectsPrivacity");   
		$this->__Contac 			  = new Contac(); 
		$this->__QualificationContact = new QualificationContact();  
		$this->__PlanUser 			  = new PlanUser();  
		$this->__UserTeam 			  = new UserTeam();  
		$this->__Meeting 			  = new Meeting();  
		$this->__ProjectsPrivacity 	  = new ProjectsPrivacity();  
		$this->__ApprovalContac 	  = new ApprovalContac();
	}
	
	public function getMonthAndDay($fecha){
    $diassemana = array(
        "Monday" => "Lunes",
        "Tuesday" => "Martes",
        "Wednesday" => "Miércoles",
        "Thursday" => "Jueves",
        "Friday" => "Viernes",
        "Saturday" => "Sábado",
        "Sunday" => "Domingo",
    );
    $meses = array(
        "January" => "Enero",
        "February" => "Febrero",
        "March" => "Marzo",
        "April" => "Abril",
        "May" => "Mayo",
        "June" => "Junio",
        "July" => "Julio",
        "August" => "Agosto",
        "September" => "Septiembre",
        "October" => "Octubre",
        "November" => "Noviembre",
        "December" => "Diciembre",
    );
    return  $diassemana[$fecha->format('l')]." ". $fecha->format('j')." de ".$meses[$fecha->format('F')]. " del ".$fecha->format('Y') ;
    }


	public function getNumberContactByProyect($project_id){

		$this->__Contac->recursive = -1;
		$total = $this->__Contac->find("count",array("conditions"=>array("Contac.project_id" => $project_id)));
		return $total;

	}


	public function getClientsByExtern($user_id){
		$clients = array();
		$info_user = $this->__UserTeam->findAllByUserId($user_id);

		$number = 1;

		if (!empty($info_user)) {
			foreach ($info_user as $key => $info) {
				if (!empty($info["Client"]["name"])) {
					$clients[] = "<li style='list-style: disc'>".$info["Client"]["name"]."</li>";
				}
			}
		}

		if (!empty($clients)) {
			return implode("<br>", $clients);
		}else{
			return __("Ninguno");
		}
		
	}
	public function calculateBonificationSMF($userPosition,$categorie,$dateFinish,$categorie_priority){
		$dias = "";
		$hoy = date("Y/m/d");
		$dias = (strtotime($hoy)-strtotime($dateFinish))/86400;$dias = floor($dias);
		$fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
		$fecha_entrada = strtotime($dateFinish);
		if($fecha_actual > $fecha_entrada){
			$dias = abs($dias) * -1;
			 
		}else{
			 $dias = abs($dias);
		}
		if($categorie == 3){
			return 0;
		}elseif($categorie == 2){
			if($categorie_priority == 1){
				if($dias <= -1){
					return ((abs($dias) * 3000)) * -1;
				}else{
					return ($dias * 3000) + $userPosition["Position"]["smf_alta"];
				}
			}else{
				if($dias <= -1){
					return ((abs($dias) * 2000)) * -1;
				}else{
					return ($dias * 3000) + $userPosition["Position"]["smf_estandar"];
				}
			}
			return 0;
		}elseif($categorie == 1){
			if($categorie_priority == 1){
				if($dias <= -1){
					return ((abs($dias) * 1200)) * -1;
				}else{
					return ($dias * 1200) + $userPosition["Position"]["cotidiana_alta"];
				}
			}else{
				if($dias <= -1){
					return ((abs($dias) * 800)) * -1;
				}else{
					return ($dias * 800) + $userPosition["Position"]["cotidiana_estandar"];
				}
			}
		}
		return 0;
	}

	public function permissionViewContac($data = array()){
		$response = false;
		if($data["Contac"]["user_id"] == AuthComponent::user("id")){
			$response = true;
		} else {
			$conditions = array("ProjectsPrivacity.project_id" => $data["Contac"]["project_id"], "ProjectsPrivacity.user_id" => AuthComponent::user("id"));
			$permission = $this->__ProjectsPrivacity->field("id", $conditions);
			if(!empty($permission)){
				$response = true;
			} else {
				$response = false;
			}
		} 
		return $response;
	}

	public function getMeetingContac($contacId){
		$conditions = array("Meeting.contac_id" => $contacId);
		$meeting    = $this->__Meeting->field("id", $conditions);
		return $meeting;
	}

	public function viewQualification($qualification){
		$out = __('Rechazada');
		if ($qualification == 1) {
			$out = __('Aprobada');
		}
		return $out;
	}

	public function valideStateFinalizedoApproved($state, $contac_id){
		$out = null;
		if ($state == Configure::read('DISABLED')) {
			$conditions = array('QualificationContact.contac_id' => $contac_id);
			$row = $this->__QualificationContact->find('count',compact('conditions'));
			if ($row > 0) {
				$out = '<a data-toggle="tooltip" data-placement="top" href="#" data-id="'.EncryptDecrypt::encrypt($contac_id).'" class="btn-xs btn_approved" title="'.__('Ver calificaciones').'"><i class="fa fa-check-square"></i></a>';
			}
		}
		return $out;
	}
  
	public function showState($state){
		$out = __('Habilitado');
		if($state == Configure::read('DISABLED')) {
			$out = __('Deshabilitado');
		}
		return $out;
	}

	public function showStateWhiteBrand($state){
		if($state == Configure::read('HAVE_WHITE_BRAND')) {
			$out = __('Sí');
		} else {
			$out = __('No');
		}	 
		return $out; 
	}

	public function showStateTemplate($state){
		if($state == Configure::read('HAVE_WHITE_BRAND')) {
			$out = __('Sí');
		} else {
			$out = __('No');
		}	 
		return $out; 
	}

	public function stateEnabledContac($contac_id,$state,$user_edit){
		$contac_id = EncryptDecrypt::decrypt($contac_id);
		$user_edit = EncryptDecrypt::decrypt($user_edit); 
		if ($state == Configure::read('CONTAC_EDITAR')) {
			if ($user_edit == Configure::read('DISABLED')) {
				$this->__Contac->updateAll(
		            array('Contac.state' => Configure::read('ENABLED')),
		            array('Contac.id' => $contac_id)
		    	);
			}
		}
	}

	public function showStateCommitment($state){ 
		if($state == Configure::read('COMMITMENT_DONE')) {
			$out = __('Realizado');
		} else if ($state == Configure::read('COMMITMENT_NOT_DONE')){
			$out = __('No realizado');
		} else if ($state == Configure::read('COMMITMENT_EXPIRED')){
			$out = __("Vencido");
		}
		return $out;
	}

	public function showStateCoupon($usagestate, $state, $dateExpired){ 
		if ($dateExpired < date("Y-m-d")){
			$out = __('Vencido');
		} else if($usagestate == Configure::read('CUPON_USADO')) {
			$out = __('Redimido');
		} else if($usagestate == Configure::read('CUPON_NO_USADO')) {
			$out = __('No redimido');
		}
		if($state == Configure::read('DISABLED')) {
			$out = __('Deshabilitado');
		} 
		return $out;  
	}

	public function showStateTransaction($state){
		if($state == Configure::read('TRANSACTION_APROBADA')) {
			$out = __('Aprobada');
		} else if ($state == Configure::read('TRANSACTION_RECHAZADA')){
			$out = __('Rechazada');
		} else if ($state == Configure::read('TRANSACTION_PENDING')){
			$out = __("Pendiente");
		} else if ($state == Configure::read('TRANSACTION_CANCEL')){
			$out = __("Cancelada");
		}
		return $out;
	}

	public function showStateModalityMeeting($state){
		if($state == Configure::read('MEETING_PRESENTIAL')) {
			$out = __('Reunión virtual');
		} else if ($state == Configure::read('MEETING_TELEPHONE')){
			$out = __('Reunión teléfonica');
		} else if ($state == Configure::read('MEETING_VIRTUAL')){
			$out = __("Reunión virtual");
		} else {
			$out = __("No disponible");
		}
		return $out;
	}

	public function meetingPrivate($state){
		if($state == 1) {
			$out = __('Reunión pública');
		} else if ($state == 2){
			$out = __('Reunión privada');
		} else {
			$out = __("No disponible");
		}  
		return $out;
	}

	public function showButtonDetailTransaction($state, $id, $planId){
		if($state == Configure::read('TRANSACTION_APROBADA')) { 
			$out = $this->link("<i class='fa fa-eye'></i>", '#', array('class'=>'btn-view-detail-transaction', 'escape' => false, 'data-plan'=>EncryptDecrypt::encrypt($planId), 'data-id'=>EncryptDecrypt::encrypt($id),'title'=>__('Ver detalle'), "data-toggle"=>"tooltip")); 
		} else if ($state == Configure::read('TRANSACTION_RECHAZADA')){ 
		 	$out = $this->link("<i class='fa fa-eye'></i>", '#', array('class'=>'btn-view-detail-transaction', 'escape' => false, 'data-plan'=>EncryptDecrypt::encrypt($planId), 'data-id'=>EncryptDecrypt::encrypt($id),'title'=>__('Ver detalle'), "data-toggle"=>"tooltip"));
		}  else {
			$out = "";
		}
		return $out;
	}

	public function changeStateButton($id, $state) {
		$out = $this->link("<i class='fa fa-times'></i>", array('action'=>'changeState'), array('class'=>'btn-xs btn_estado', 'escape' => false, 'data-id'=>EncryptDecrypt::encrypt($id),'title'=>__('Deshabilitar'), "data-toggle"=>"tooltip"));
		if($state == Configure::read('DISABLED')) {
			$out = $this->link("<i class='fa fa-check-circle-o'></i>", array('action'=>'changeState'), array('class'=>'btn-xs btn_estado', 'escape' => false, 'data-id'=>EncryptDecrypt::encrypt($id),'title'=>__('Habilitar'), "data-toggle"=>"tooltip"));
		} else if($state == Configure::read('DISABLED_MANUAL')){
			$out = $this->link("<i class='fa fa-check-circle-o'></i>", array('action'=>'changeState'), array('class'=>'btn-xs btn_estado', 'escape' => false, 'data-id'=>EncryptDecrypt::encrypt($id),'title'=>__('Habilitar'), "data-toggle"=>"tooltip"));
		}
		return $out;
	}

	public function buttonDisableToReason($id, $state) {
		$out = $this->link("<i class='fa fa-times'></i>", '#', array('class'=>'btn  btn-xs btn-disabled-coupon', 'escape' => false, 'data-id'=>EncryptDecrypt::encrypt($id),'title'=>__('Deshabilitar'), "data-toggle"=>"tooltip"));
		if($state == Configure::read('DISABLED')) {
			$out = $this->link("<i class='fa fa-check-circle-o'></i>", array('action'=>'changeState'), array('class'=>'btn-xs btn_estado', 'escape' => false, 'data-id'=>EncryptDecrypt::encrypt($id),'title'=>__('Habilitar'), "data-toggle"=>"tooltip"));
		}
		return $out;
	}

	public function nameRol($role){
		$out = __('Funcionario');
		if($role == Configure::read('ADMIN_ROLE_ID')) {
			$out = __('Administrador');
		}else if ($role == Configure::read('BUSINESS_ROLE_ID')){
			$out = __('Empresa');
		}
		return $out;
	}

	public function stateContac($state){
		$out = __('Borrador');
		if ($state == Configure::read('DISABLED')) {
			$out = __('Finalizado');
		} else if ($state == Configure::read('APPROVAL')){
			$out = __('Por calificar');
		}
		return $out;
	}

	public function planRecommended($id, $recommended, $state, $hideMessageChoose = null){
		$out = __('No disponible');
		if ($state != Configure::read('DISABLED')) {
			if (!isset($hideMessageChoose)) {
				$out ='<div class="card-btn-bottom"><a data-id='.EncryptDecrypt::encrypt($id).' class="btn_recomendado btn btn-reecapmeeting-user">'.' '.__('Destacar').'</a></div>';
				if ($recommended == Configure::read('ENABLED')) {
					$out ='<div class="card-btn-bottom"><a title='.__("Recomendado").' class="btn-reecapmeeting-user" "data-toggle"="tooltip">'.' '.__('Recomendado').'</a></div>';
				} 
			} else {
				if ($recommended == Configure::read('ENABLED')) {
					$out ='<div class="card-btn-bottom"><a class="disabled" title='.__("Recomendado").'></a></div>';
				} else {
					$out ='<div class="card-btn-bottom"><a class="btn_recomendado"></a></div>';
				}
			}
		}
		return $out;
	}

	public function validatePlanActualAndChangePlan($plan = array(), $planInfoActual = null, $planVencido = null){ 
		if ($planVencido != null) {
			if ($plan["Plan"]["id"] == $planVencido["Plan"]["id"]) { 
				if($plan["Plan"]["trial"] == Configure::read("ENABLED") && $planVencido["PlanUser"]["expired"] == Configure::read("ENABLED")){
					echo $this->link(__('No disponible'), 'javascript:void(0)', array('class' => 'btn btn-blue','disabled' => 'disabled'));
				} else {
					echo $this->link(__('Renovar'),  'javascript:void(0)', array('class' => 'btn-change-plan btn btn-primary btn-blue','data-id' => EncryptDecrypt::encrypt($plan["Plan"]["id"])));
				}
			} else {
				if($plan["Plan"]["trial"] == Configure::read("ENABLED") && $planVencido["PlanUser"]["expired"] == Configure::read("ENABLED")){
					echo $this->link(__('No disponible'), 'javascript:void(0)', array('class' => 'btn btn-blue','disabled' => 'disabled'));
				} else {
					echo $this->link(__('Adquirir'), 'javascript:void(0)', array('class' => 'btn-change-plan btn btn-primary btn-blue','data-id' => EncryptDecrypt::encrypt($plan["Plan"]["id"])));
				}
			}
		} else {
			if (isset($planInfoActual["Plan"]["id"])) {
				if($plan["Plan"]["id"] == $planInfoActual["Plan"]["id"]){
					echo "<div class='card-btn-bottom'><p class='btn-plan-active'>".__("Activo")."</p></div>";
				} else if ($planInfoActual["Plan"]["id"] != null) {
					$priceToAcquired = $plan["Plan"]["price"] * configure::read("CONF_PLAN_TIME");
					$pricePlanActual = $planInfoActual["PlanUser"]["price"];
					$userToAcquired  = $plan["Plan"]["users"];
					$userActual      = $planInfoActual["PlanUser"]["number_users"]; 
					if ($priceToAcquired > $pricePlanActual && $userToAcquired > $userActual) {
						echo $this->link(__('Adquirir'), 'javascript:void(0)', array('class' => 'btn-change-plan btn btn-blue', 'data-id' => EncryptDecrypt::encrypt($plan["Plan"]["id"])));
					} else {
						echo $this->link(__('No disponible'), 'javascript:void(0)', array('class' => 'btn btn-blue','disabled' => 'disabled'));
					} 
				} 
			} else { 
				if($plan["Plan"]["trial"] == 1){
					echo $this->link(__('Adquirir'), array('controller' => 'plans', 'action' => 'buy_plan_trial', EncryptDecrypt::encrypt($plan["Plan"]["id"])), array('class' => 'btn-blue')); 
				} else {
					echo $this->link(__('Adquirir'), 'javascript:void(0)', array('class' => 'btn-buy-plan btn btn-blue','data-id' => EncryptDecrypt::encrypt($plan["Plan"]["id"])));
				} 
			}	
		}
 	}
  
 	public function conversorSegundosHoras($time) {
	    $horas      = floor($time / 3600);
	    $minutos    = floor(($time - ($horas * 3600)) / 60);
	    $segundos   = $time - ($horas * 3600) - ($minutos * 60);
	    $hora_texto = "";
	    if ($horas > 0 ) {
	        $hora_texto .= $horas . "h ";
	    }
	    if ($minutos > 0 ) {
	        $hora_texto .= $minutos . "m ";
	    }
	    if ($segundos > 0 ) {
	        $hora_texto .= $segundos . "s";
	    }
	    return $hora_texto;
	}

	public function showDateMeeting($startDate){
		$out = "";
		$createDate = new DateTime($startDate);
		$cretaeHour = new DateTime($startDate);
		$dateNew    = $createDate->format('Y-m-d');
		$hourNew    = $createDate->format('H:i A'); 
		$tomorrow   = date("Y-m-d", strtotime("+1 day"));
		if($dateNew == date('Y-m-d')){
			$out = __("Hoy ") . $hourNew;
		} else if ($dateNew == $tomorrow) {
			$out = __("Mañana ") . $hourNew;
		} else {
			$out = $dateNew . ' ' . $hourNew;
		} 
		return $out; 
	}

	public function validateActionInPlan($infoAction, $permissions){
		$disabledAction = "disabled-actions";  
		if(!empty($permissions)){
			foreach ($permissions as $permission) { 
			 	if($permission["Permission"]["controller"] == $infoAction["controller"] && $permission["Permission"]["action"] == $infoAction["action"]){
	 				$disabledAction = "";
	 				break; 
			 	} else {
			 		$disabledAction = "disabled-actions"; 
			 	}
			} 
		} else {
			$disabledAction = "disabled-actions";
		} 
		return $disabledAction; 
	}

	public function nameModel($model){
		$texto = "";
		if ($model == 'Project') {
			$texto = __('Proyectos');
		} elseif($model == 'Client'){
			$texto = __('Clientes');
		} elseif($model == 'UserTeam'){
			$texto = __('Usuarios');
		} elseif($model == 'Contac'){
			$texto = __('Actas');
		} elseif($model == 'Commitment'){
			$texto = __('Compromisos');
		} elseif($model == 'Tag'){
			$texto = __('Tags');
		} elseif($model == 'Meeting'){
			$texto = __('Reuniones');
		} elseif($model == 'Topic'){
			$texto = __('Temas específicos');
		} elseif($model == 'Plan'){
			$texto = __('Planes');
		} elseif($model == 'Coupon'){
			$texto = __('Cupones');
		} elseif($model == 'Template'){
			$texto = __('Plantillas');
		} elseif($model == 'ApprovalContac'){
			$texto = __('Aprobaciones de actas');
		}
		return $texto;
	}
 
	public function showExpiredDate($plan = array(), $planActual = array()){ 
		$dateStart = date("Y-m-d");
		if(!empty($planActual)){
			if($plan["Plan"]["id"] == $planActual["Plan"]["id"]){ 
				echo $dateValidity = date('d-m-Y', strtotime($planActual["PlanUser"]["validity"]));
			} else {
				if($plan["Plan"]["trial"] == Configure::read("ENABLED")){
					echo $dateValidity = date('d-m-Y', strtotime("+30 days $dateStart"));
				} else {
					echo $dateValidity = date('d-m-Y', strtotime("+12 months $dateStart"));
				}
			}
		} else {
			if($plan["Plan"]["trial"] == Configure::read("ENABLED")){
				echo $dateValidity = date('d-m-Y', strtotime("+30 days $dateStart"));
			} else {
				echo $dateValidity = date('d-m-Y', strtotime("+12 months $dateStart"));
			}
		} 
	}

	public function show_recommended($recommended){
		if($recommended == 1){
			echo __("Sí");
		} else {
			echo __("No");
		}
	} 

	public function check_team_permission_action($actions = array(), $controller = array(), $teamId = null){
		$userId 	   = AuthComponent::user("id");
		$permissionApp = Cache::read("permissions_{$userId}", 'PERMISSIONS');
		$actionsAllow  = array(); 
		if(!is_null($teamId)){
			if(!empty($permissionApp["UserTeam"])){
				foreach ($permissionApp["UserTeam"] as $userTeam) {
					if($userTeam["UserTeam"]["team_id"] == $teamId && $userTeam["UserTeam"]["user_id"] == AuthComponent::user("id")){
						if(isset($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]])){
							foreach ($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]]["Restriction"] as $restriction) {
							 	if (in_array($restriction["action"], $actions) && in_array($restriction["controller"], $controller)) { 
							 		$actionsAllow[$restriction["action"]] = true; 
							 	} 
							} 
						} 
					} 
				}  	
			}  
		} else {
			$actionsAllow = $this->__check_actions_permission($permissionApp, $actions, $controller);
		}
		return $actionsAllow; 
	}

	private function __check_actions_permission($permissionApp = array(), $actions = array(), $controller = array()){
		if(!empty($permissionApp["PlanUser"])){
			if(!empty($permissionApp["UserTeam"])){
		 		return $response = $this->__checkActionAllowed($actions, $controller, $permissionApp);
		 	} 
		} else {  
			if(!empty($permissionApp["UserTeam"])){
				return $response = $this->__checkActionAllowed($actions, $controller, $permissionApp);
			}
		}  
	}

	private function __checkActionAllowed($actions = array(), $controller = array(), $permissionApp){ 
		$actionsAllow = array();
		if(!empty($permissionApp["UserTeam"])){
			foreach ($permissionApp["UserTeam"] as $userTeam) { 
	 		 	if(isset($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]])){
	 		 		foreach ($permissionApp["Positions"][$userTeam["UserTeam"]["position_id"]]["Restriction"] as $restriction) {
						if (in_array($restriction["action"], $actions) && in_array($restriction["controller"], $controller)) { 
					 		$actionsAllow[$restriction["action"]] = true; 
					 	} 
					}  
	 		 	} 
		 	} 
	 	} 
	 	return $actionsAllow;
	} 

	public function showStateStep($models = array(), $controller){
		if($this->request->controller == $controller){
			return "active";
		} else {
			if($models["models"][$controller] >= 1){
				return "completed";
			} 
		}
	}

	public function showButtonTask(){
		$actions 	 = array("add");
		$controllers = array("teams","projects","clients","positions","user_teams");
		if(in_array($this->request->action, $actions) && in_array($this->request->controller, $controllers)){
			return false;
		} else {
			return true;
		} 
	}

	public function check_button_calification_contact($contac_id){
		$conditions = array(
			'ApprovalContac.contac_id' => $contac_id,
			'ApprovalContac.user_id'   => AuthComponent::user('id'),
			'ApprovalContac.state'     => Configure::read('DISABLED'),
			'NOT' => array('ApprovalContac.state' => Configure::read('ENABLED'))
		);
		$conditions[] = array( 
			'OR' => array(
				array('Contac.in_approval' => Configure::read('ENABLED')), 
				array('Contac.in_approval' => Configure::read('NEW_APPROVAL'))
			),
		); 
		$contact   = $this->__ApprovalContac->find('first', compact('conditions'));
		return $contact; 
	}
}