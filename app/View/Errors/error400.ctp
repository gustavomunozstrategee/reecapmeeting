<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$this->layout = 'default_home';?>

<!-- <h2><?php echo $message; ?></h2>
<p class="error">
	<strong><?php echo __d('cake', 'Error'); ?>: </strong>
	<?php printf(
		__d('cake', 'The requested address %s was not found on this server.'),
		"<strong>'{$url}'</strong>"
	); ?>
</p> -->

<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
?>

<?php $this->Html->css("frontend/website/pages/error.css", array("block" => "styles_section")); ?>
            <div class="bg-error">
            <?php echo $this->Html->image("frontend/website/home/figure-left-top.svg",array('class' => 'img-responsive figurelefttop hidden-xs', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
            <?php echo $this->Html->image("frontend/website/home/figure-right-middle.svg",array('class' => 'img-responsive figurerightmiddle', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
            <?php echo $this->Html->image("frontend/website/home/figure-left-bottom.svg",array('class' => 'img-responsive figureleftbottom', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
            <?php echo $this->Html->image("frontend/website/home/error.png",array('class' => 'img-responsive figurecenter', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
                <div class="info-error">
                    <div>
                        <h1><?php echo __("Algo salió mal..."); ?></h1>
                        <p><?php echo __("¡Lo sentimos! La página que buscas"); ?><br><?php echo __("no se ha encontrado en este servidor."); ?></p>
                        <p><?php echo __("Revisa la dirección");?><br><?php echo __("URL e inténtalo de nuevo."); ?></p>
                        <div class="text-center">
                            <a href="<?php echo  $this->Html->url(array("controller" => "pages","action" => "index_page"))?>" class="btn btn-reecapmeeting"><span><?php echo __("Regresa al inicio"); ?></span></a>
                        </div>
                    </div>
                </div>
            </div>
