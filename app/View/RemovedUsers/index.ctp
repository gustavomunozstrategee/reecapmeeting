<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Usuarios removidos'); ?>
                <?php echo $this->element("team_name"); ?>
                <!-- <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Listar miembros")?>" href="<?php echo $this->Html->url(array('controller'=>'user_teams','action' => 'index')); ?>">
                    <i class="flaticon-list"> </i>
                </a>   -->
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"user_teams",'action'=>'index'));?>"><?php echo __("Usuarios") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Usuarios removidos") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row">
                        <div class="col-md-6 mt-1">
                            <div class="input-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre, Correo electrónico, Empresa'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
                        <div class="col-md-6 mt-1">
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <?php if(empty($removedUsers) && !empty($this->request->query['q'])) : ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?></p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'),array('class'=>'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="widget-body">
                <ul class="nav nav-tabs nav-fill" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $this->Html->url(array("controller"=>"user_teams","action"=>"index")) ?>">
                            <?php echo __("Miembros y empresas"); ?>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link active " href="javascript:void(0)">
                            <?php echo __("Miembros removidos"); ?>
                        </a>
                    </li>
                </ul>
                <div class="table-responsive mt-3">
                    <table cellpadding="0" cellspacing="0" class="table f-table-recapmeeting f-table-grid-layout-fixed">
                        <thead class="text-primary">
                            <tr>
                                <th><?php echo __('Empresa'); ?></th> 
                                <th><?php echo __('Usuario'); ?></th> 
                                <th><?php echo __('Correo electrónico'); ?></th> 
                                <th><?php echo __('Acciones') ?></th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($removedUsers)):?>
                                <?php foreach ($removedUsers as $removedUser): ?>
                                    <tr>
                                        <td><?php echo $this->Text->truncate($removedUser['Team']['name'],100); ?></td>
                                        <td><?php echo !empty($removedUser['User']['firstname']) ? $removedUser['User']['firstname'] . ' ' . $removedUser['User']['lastname'] : __("Nombre no asignado por parte del usuario.")?></td>
                                        <td><?php echo $removedUser['User']['email']?></td> 
                                        <td class="td-actions"> 
                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'user_teams','action' => 'activate_license', EncryptDecrypt::encrypt($removedUser['RemovedUser']['id']))); ?>" title="<?php echo __('Asignar nuevamente'); ?>" class="btn-xs btn_team_action" data-toggle="tooltip">
                                                <i class="fa fa-check"></i>
                                            </a> 
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td class="text-center" colspan="4"><?php echo __('No existen usuarios añadidos.')?></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
