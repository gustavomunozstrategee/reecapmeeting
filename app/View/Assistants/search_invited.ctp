<?php if (!empty($users)): ?>
	<?php foreach ($users as $key => $user): ?>
		<div class="suggest-element list-group-item">
			
				
			<a data-name="<?php echo EncryptDecrypt::encrypt($user["UserTeam"]["type_user"]) ?>" class="inviteds" id=" <?php echo EncryptDecrypt::encrypt($user["User"]['id']) ?> "> 	<div class="media">
					<div class="media-left align-self-center mr-3">
						<img class="avatar rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$user['User']['img'],true); ?>" alt="..." class="img-fluid rounded-circle" style="width: 50px;height: 50px">
					</div>
					<div class="media-body align-self-center">
						<div class="other-message-sender"><?php echo h($user["User"]['firstname'] . ' ' . $user["User"]['lastname']); ?> </div>
						<div class="other-message-time"><?php echo h($user["User"]['email']); ?></div>
					</div>
				</div>
			</a>
		</div>
	<?php endforeach ?>
<?php endif ?>
