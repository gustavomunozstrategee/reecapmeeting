<div>
    <div>
    <?php if(!empty($userInvitedsAdded)):?>
        <ul class="list-group w-100 mt-5">
        <?php foreach ($userInvitedsAdded as $dateId => $userInvited): ?>
        <li class="list-group-item">
            <div class="other-message">
                <div class="media">
                    <div class="media-left align-self-center mr-3">
                        <img class="avatar rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$userInvited['image']); ?>" alt="..." class="img-fluid rounded-circle" style="width: 50px;height: 50px">
                    </div>
                    <div class="media-body align-self-center">
                        <div class="other-message-sender"><?php echo h($userInvited['name']); ?> </div>
                        <div class="other-message-time"><?php echo h($userInvited['email']); ?></div>
                    </div>
                    <div class="media-right align-self-center">
                        <div class="actions td-actions">
                            
                            <a  data-id="<?php echo $dateId ?>" href="#" class="delete-user-meeting-following remove">
                                <i class="la la-close delete"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <?php endforeach ?>
        </ul>
        
    <?php else: ?>
        <div>
            <p class="text-center"><?php echo __("No hay usuarios invitados añadidos.");?></p>
        </div>
    <?php endif; ?>
  </div>
</div>