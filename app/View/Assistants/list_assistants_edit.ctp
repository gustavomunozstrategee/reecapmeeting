<div>
    <div>
        <?php if(!empty($assistants)): ?>
            <ul class="list-group w-100 mt-5 mb-3">
                <?php foreach ($assistants as $assistant): ?>
                    <?php if($assistant["state"] != configure::read("DELETE_ASSISTANT")): ?>
                        <li class="list-group-item">
                            <div class="other-message">
                                <div class="media">
                                    <div class="media-left align-self-center mr-3">
                                        <img class="avatar rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$assistant['image']); ?>" alt="..." class="img-fluid rounded-circle" style="width: 50px;height: 50px">
                                    </div>
                                    <div class="media-body align-self-center">
                                        <div class="other-message-sender"><?php echo h($assistant['name']); ?> </div>
                                        <div class="other-message-time"><?php echo h($assistant['email']); ?></div>
                                    </div>
                                    <div class="media-right align-self-center">
                                        <div class="actions td-actions">

                                             <?php if($assistant["user_id"] == NULL): ?>
                                                <a href="#" data-id="<?php echo EncryptDecrypt::encrypt($assistant['employee_id']) ?>" class="btn-icon-xs btn-xs delete-assistant-edit-meeting">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                                <?php else: ?>
                                                <a href="#" data-id="<?php echo EncryptDecrypt::encrypt($assistant['user_id']) ?>" class="btn-icon-xs btn-xs delete-assistant-edit-meeting">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>
