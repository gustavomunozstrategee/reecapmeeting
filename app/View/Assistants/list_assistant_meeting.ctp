 <div id="update-content-meeting-assistants">
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped table-hover">
			<thead>
				<tr>						
					<th><?php echo __('Asistente'); ?></th>
					<th><?php echo __('Correo electrónico'); ?></th>
				</tr>
			</thead>
			<tbody>			 
				<?php foreach ($assistants as $assistant): ?>						 
					<tr> 
						<?php if($assistant["Employee"]["id"] == NULL):?>
							<td><?php echo h($assistant['User']['firstname'] . ' ' . $assistant['User']['lastname']); ?> &nbsp;</td>
							<td><?php echo h($assistant['User']["email"]); ?>&nbsp;</td>		
							<?php else: ?>
							<td><?php echo h($assistant['Employee']['name']); ?> &nbsp;</td>
							<td><?php echo h($assistant['Employee']["email"]); ?>&nbsp;</td>
						<?php endif; ?> 
				 	</tr> 
				<?php endforeach; ?>
			</tbody>
		</table> 
		 <p>
		 	<?php 
				$this->Paginator->options(array('update' => '#update-content-meeting-assistants','url'=>array('controller'=>'assistants','action'=>'list_assistant_meeting', $meetingId)));
			?>
	        <small>
	            <?php
	                echo $this->Paginator->counter(array(
	                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
	                ));
	            ?>
	        </small>
	    </p>

	    <ul class="pagination">
	        <?php
	            echo $this->Paginator->prev('< ' . __('Ant'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
	            echo $this->Paginator->next(__('Sig') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	        ?>
	    </ul>
	    <?php echo $this->Js->writeBuffer(); ?> 
	</div>
</div> 