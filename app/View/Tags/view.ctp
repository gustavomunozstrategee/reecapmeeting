 <?php $permission = $this->Utilities->check_team_permission_action(array("index"), array("tags"), $tag['Team']['id']); ?>
<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Tags") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Tags") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Visualización del tag") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2>
                    <?php echo __('Visualización del tag'); ?> <?php echo $this->element("team_name"); ?>
                </h2>
                <div class="widget-options">
                    <div class="" role="">
                        <?php if(!empty($permission["index"])): ?>  
                                
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                                <button type="button" class="btn btn-secondary ">
                                    <i class="la la-list"></i>
                                    <?php echo __('Listar'); ?>
                                </button>
                            </a>
                                
                        <?php endif;?>


                    </div>
                </div>
            </div>
            <div class="widget-body">
                <div class="form-group row d-flex align-items-center border-bottom">
                    <label class="col-lg-4 form-control-label">
                        <?php echo __('Nombre'); ?>
                    </label>
                    <div class="col-lg-8 div-word-wrap">
                        <?php echo h($tag['Tag']['name']); ?>
                    </div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-primary text-center"><?php echo __('Reuniones donde han utilizado este tag'); ?> </h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th class="table-grid-10"><?php echo __('Tipo reunión'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Asunto'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Ubicación - dirección'); ?></th>
                                    <th class="table-grid-20"><?php echo __('Descripción'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Modalidad') ?></th>
                                    <th class="table-grid-10"><?php echo __('Fecha inicio'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Fecha fin'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Privada - Pública'); ?></th>
                                    <th class="table-grid-10 text-center"><?php echo __('Acciones'); ?></th>
                                </tr>
                            </thead>
                            <tbody> 
                                <?php if(!empty($meetings)):?>
                                    <?php foreach ($meetings as $meeting): ?>
                                    <tr>
                                        <td class="table-grid-10 td-word-wrap"><?php echo h(!empty($meeting['Meeting']['meeting_type']) ? __($meeting['Meeting']['meeting_type']) : __("No disponible")); ?>&nbsp;</td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($meeting['Meeting']['subject']),100); ?></td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo h(!empty($meeting['Meeting']['location']) ? $meeting['Meeting']['location'] : __("No disponible")); ?>&nbsp;</td>
                                        <td class="table-grid-20 td-word-wrap"><?php echo $this->Text->truncate(h($meeting['Meeting']['description']),100); ?></td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo $this->Utilities->showStateModalityMeeting($meeting['Meeting']['meeting_room']); ?></td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo h($meeting['Meeting']['start_date']); ?>&nbsp;</td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo h($meeting['Meeting']['end_date']); ?>&nbsp;</td>
                                        <td class="table-grid-10 td-word-wrap"><?php echo $this->Utilities->meetingPrivate($meeting['Meeting']['private']); ?></td>
                                        <td class="td-actions text-center"> 
                                        <?php  $permission = $this->Utilities->check_team_permission_action(array("view"), array("meetings"), $meeting["Meeting"]["team_id"]); ?>
                                            <?php if($meeting["Meeting"]["email_type"] == null || $meeting["Meeting"]["email_type"] == ""):?>
                                                <?php if(isset($permission["view"])): ?>
                                                  <a rel="tooltip" href="<?php echo $this->Html->url(array('action'=>'view', "controller" => "meetings",EncryptDecrypt::encrypt($meeting['Meeting']['id'])));?>"  title="<?php echo __('Ver detalle de la reunión'); ?>" class="flex-auto text-center f-cursor-pointer" data-toggle="tooltip">
                                                        <i class="fa fa-eye"></i>
                                                    </a> 
                                                <?php endif; ?> 
                                            <?php endif; ?>  
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                    <tr>
                                        <td class="text-center" colspan="9"><?php echo __('No existen reuniones asociadas a este tag.')?></td>
                                    </tr>
                                    <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <!--Pagination-->
                    <div class="table-pagination mt-4">
                        <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                        <div>
                            <ul class="pagination f-paginationrecapmeeting">
                                <?php
                                echo $this->Paginator->prev('< ' /*. __('')*/, array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(/*__('') . */' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                ?>
                            </ul>
                        </div>
                    </div>
                    <!--End pagination-->
                </div>
               
            </div>
        </div>
    </div>
</div>


<!-- ******************************* -->
