<?php $permissionAction = $this->Utilities->check_team_permission_action(array("add"), array("tags"));  ?>
<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Gestión de tags'); ?> 
                <?php echo $this->element("team_name"); ?> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Tags") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row">                        
                        <div class="col-md-6">
                             <div class="form-group"> 
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'hidden', 'id' => 'teamSelect', 'value' => EncryptDecrypt::decrypt($this->Session->read("TEAM")) )); ?>
                            
                                <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                    <?php echo __('Buscar');?> <i class="la la-search"></i>
                                </button>
                                
                            </div>
                        </div>
                        <div class="col-md-12">
                           <?php if(empty($clients) && !empty($this->request->query['q'])) : ?>
                                    <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
                                    <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
                                <?php endif ?> 
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="<?php echo $this->request->is('mobile') ? '' : 'widget-body' ?>">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th class="table-grid-40"><?php echo __('Nombre del tag'); ?></th>
                                <th class="table-grid-20 text-center"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                         <tbody>
                             <?php if(!empty($tags)):?>
                                <?php foreach ($tags as $tag): ?>
                                <tr>
                                    <td class="table-grid-40 td-word-wrap"><?php echo $this->Text->truncate(h($tag['Tag']['name']),100); ?></td>
                                    <!-- <td class="table-grid-40 td-word-wrap"><?php echo $this->Text->truncate(h($tag['Team']['name']),100); ?></td> -->
                                    <td class="table-grid-20 td-actions text-center">  
                                        <?php $permission = $this->Utilities->check_team_permission_action(array("view"), array("tags"), $tag['Team']['id']); ?>

                                        <?php if(isset($permission["view"])):?>
                                            <a rel="tooltip" data-toggle="tooltip" data-placement="right" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($tag['Tag']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                    <tr>
                                        <td class="text-center" colspan="3"><?php echo __('No existen tags.')?></td>
                                    </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer">
                <!--Inicio Pagination-->
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination mt-3">
                            <?php
                            echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                            echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
                <!--End Pagination-->
            </div>
        </div>
    </div>
</div>

