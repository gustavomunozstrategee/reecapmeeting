 
<?php //$this->Html->css("frontend/admin/pages/employees_view.css", array("block" => "styles_section")); ?>
<?php  $permission = $this->Utilities->check_team_permission_action(array("index","edit","add"), array("projects"), $project["Team"]["id"]);  ?>


<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Proyectos") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Proyectos") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Visualización del proyecto") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->



<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2>
                    <?php echo __('Visualización del proyecto'); ?> <?php echo $this->element("team_name"); ?>
                    <?php echo $this->element("team_name"); ?> 
                </h2>
                <div class="widget-options">
                    <div class="" role="">
                        <?php if(!empty($permission["index"])): ?>  
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                                <button type="button" class="btn btn-secondary ">
                                    <i class="la la-list"></i>
                                    <?php echo __('Listar'); ?>
                                </button>
                            </a>
                        <?php endif;?>

                        <?php if(isset($permission["edit"])): ?>
                             <a href="<?php echo $this->Html->url(array('action'=>'edit', EncryptDecrypt::encrypt($project['Project']['id'])));?>">
                                <button type="button" class="btn btn-secondary "><i class="la la-pencil"></i><?php echo __('Editar'); ?></button></a>
                            </a> 
                        <?php endif; ?>
                        <?php if(isset($permission["add"])): ?>
                            <a href="<?php echo $this->Html->url(array('action'=>'add'));?>">
                                 <button type="button" class="btn btn-secondary "><i class="la la-plus"></i><?php echo __('Adicionar'); ?></button></a>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="widget-body">
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-4">
                        <input type="hidden" value="<?php echo EncryptDecrypt::encrypt($project['Project']['id']) ?>" id="client-id">
                         <a href="<?php echo $this->Html->url('/files/Project/'.$project['Project']['img']); ?>" class="showImagesPopup"> 
                            <div class="img-thumbnail">   
                                <img src="<?php echo $this->Html->url('/files/Project/'.$project['Project']['img']); ?>" class="img-fluid" alt="Logo" />
                            </div>
                        </a>
                         <a href="#" class="btn btn-primary btn-block" style="margin-top: 10px;" data-toggle="modal" data-target="#UsersPrivacity"><?php echo __("Ver usuarios asignados al proyecto"); ?></a>
                    </div>
                    <div class="col-md-9 col-lg-9 col-sm-8">
                        <div class="form-horizontal" id="content-table">
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Nombre'); ?>
                                </label>
                                <div class="col-lg-8 div-word-wrap">
                                     <p><?php echo $project['Project']['name']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Cliente'); ?>
                                </label>
                                <div class="col-lg-8 div-word-wrap">
                                     <p><?php echo $this->Html->link($project['Client']['name'], array('controller' => 'clients', 'action' => 'view', EncryptDecrypt::encrypt($project['Client']['id'])), array('class' => 'f-link-blue')); ?></p>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Descripción'); ?>
                                </label>
                                <div class="col-lg-8">
                                     <div class="div-word-wrap">
                                        <?php echo h($project['Project']['description']); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Fecha de registro:'); ?>
                                </label>
                                <div class="col-lg-8">
                                     <?php echo h($this->Time->format('d-m-Y h:i A', $project['Project']['created'])); ?>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Estado'); ?>
                                </label>
                                <div class="col-lg-8">
                                     <?php echo $this->Utilities->showState($project['Project']['state']); ?>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Copias al correo electrónico'); ?>
                                </label>
                                <div class="col-lg-8">
                                    <p class="word-wrap"><?php echo $project['Project']['email_copies']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Número en que comenzó el acta'); ?>
                                </label>
                                <div class="col-lg-8">
                                    <?php echo h($project['Project']['number']); ?>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Número de actas finalizadas'); ?>
                                </label>
                                <div class="col-lg-8">
                                    <?php echo h(!empty($project["Contac"]["0"]["Contac"]["0"]["total_contac"]) ? $project["Contac"]["0"]["Contac"]["0"]["total_contac"] : 0); ?>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Número de actas sin finalizar'); ?>
                                </label>
                                <div class="col-lg-8">
                                    <?php echo h(!empty($project["NotContac"]["0"]["NotContac"]["0"]["total_contac_no_realizas"]) ? $project["NotContac"]["0"]["NotContac"]["0"]["total_contac_no_realizas"] : 0); ?>
                                </div>
                            </div>
                            <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                                <div class="form-group row d-flex align-items-center border-bottom">
                                    <label class="col-lg-4 form-control-label">
                                        <?php echo __('Empresa'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <?php echo $this->Html->link($project['User']['firstname'].' '.$project['User']['lastname'], array('controller' => 'users', 'action' => 'view', EncryptDecrypt::encrypt($project['User']['id'])), array('class' => '')); ?>&nbsp;
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-primary text-center"><?php echo __('Actas del proyecto'); ?> </h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th class="table-grid-10"><?php echo __('Fecha inicio') ?></th>
                                    <th class="table-grid-10"><?php echo __('Fecha fin') ?></th>
                                    <th class="table-grid-10"><?php echo __('Empresa') ?></th>
                                    <th class="table-grid-20"><?php echo __('Nombre cliente') ?></th>
                                    <th class="table-grid-10"><?php echo __('Proyecto') ?></th>
                                    <th class="table-grid-20 text-center"><?php echo __('Número del acta') ?></th>
                                    <th class="table-grid-20"><?php echo __('Estado') ?></th>
                                    <th class="table-grid-10"><?php echo __('Imagen del cliente') ?></th>
                                    <th class="table-grid-10 text-center"><?php echo __('Acción') ?></th>
                                </tr>
                            </thead>
                            <tbody> 
                                <?php if(!empty($contacs)):?>
                                <?php foreach ($contacs as $contac): ?>
                                <?php $response = $this->Utilities->permissionViewContac($contac);?> 
                                <tr>
                                    <td class="table-grid-10 td-word-wrap"><?php echo h($contac['Contac']['start_date']); ?></td>
                                    <td class="table-grid-10 td-word-wrap"><?php echo h($contac['Contac']['end_date']); ?></td>
                                    <td class="table-grid-10 td-word-wrap"><?php echo h($contac['Team']['name']); ?></td>
                                    <td class="table-grid-20 td-word-wrap"><?php echo h($contac['Client']['name']); ?></td>
                                    <td class="table-grid-10 td-word-wrap"><?php echo h($contac['Project']['name']); ?></td>
                                    <?php if($contac['Contac']['state'] == Configure::read('ENABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')): ?>
                                        <td class="table-grid-10 td-word-wrap text-center"><span class="table-number"><?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?></span></td>
                                    <?php else: ?>
                                        <td class="table-grid-10 td-word-wrap text-center"><span class="table-number"><?php echo h($contac['Contac']['number']); ?></span></td>
                                    <?php endif; ?>
                                    <td class="table-grid-10 td-word-wrap"><span class="table-number"><?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?></span></td>
                                    <td class="table-grid-10 td-word-wrap"><a href="<?php echo $this->Html->url('/files/Client/'.$contac['Client']['img']); ?>" class="showImagesPopup"><img src="<?php echo $this->Html->url('/files/Client/'.$contac['Client']['img']); ?>" width="50px" class="img-responsive" alt="" /></a></td>
                                    <td class="table-grid-10 td-actions text-center">

                                        <?php 
                                            $permission = $this->Utilities->check_team_permission_action(array("index"), array("contacs"), $contac['Team']['id']); 
                                        ?>
                                        <?php if($response == true):?>
                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view','controller'=>'contacs', EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" title="<?php echo __('Ver acta'); ?>" class="btn-xs" data-toggle="tooltip">
                                                <i class="fa fa-eye f-blue"></i>
                                            </a>
                                        <?php endif; ?> 
                                        <?php if(isset($permission["index"])) : ?>  
                                            <?php if($contac['Contac']['state'] == Configure::read('DISABLED')): ?>
                                                <a  rel="tooltip" href="<?php echo Router::url('/', true) ?>contacs/download_contac/<?php echo EncryptDecrypt::encrypt($contac["Contac"]["id"]);?>.pdf" title="<?php echo __('Descargar acta'); ?>" data-toggle="tooltip" class="btn-xs"><i class="flaticon-file"></i></a> 
                                            <?php endif; ?> 
                                        <?php endif; ?>

                                    </td>
                                </tr>
                                <?php endforeach ?>
                                <?php else: ?>
                                <tr>
                                    <td class="text-center" colspan="8"><?php echo __("No hay actas relacionadas con este proyecto.");?></td>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <!--Pagination-->
                    <div class="table-pagination mt-4">
                        <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                        <div>
                            <ul class="pagination f-paginationrecapmeeting">
                                <?php
                                echo $this->Paginator->prev('< ' /*. __('')*/, array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(/*__('') . */' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                ?>
                            </ul>
                        </div>
                    </div>
                    <!--End pagination-->
                </div>
               
            </div>
        </div>
    </div>
</div>

<!-- ******************************************************** -->
<div id="UsersPrivacity" class="modal fade">
        <div class="modal-dialog modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo __("Usuarios asignados al proyecto")?></h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
                </div>
                <div class="modal-body">

                <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                            <thead class="text-primary">
                                <tr>
                                    <th class="table-grid-20"><?php echo __('Nombre'); ?></th> 
                                    <th class="table-grid-20"><?php echo __('Correo electrónico'); ?></th> 
                                </tr>
                            </thead>
                            <tbody>                            
                                <?php if(!empty($users)):?>
                                    <?php foreach ($users as $user): ?>
                                        <tr>
                                            <td><?php echo !empty($user["User"]["firstname"]) ? $user["User"]["firstname"] . ' ' . $user["User"]["lastname"] : __("No asignado.")?></td>
                                            <td><?php echo $user["User"]["email"] ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                    <tr>
                                        <td class="text-center" colspan="9"><?php echo __('No existen usuarios asignados al proyecto.')?></td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Cerrar");?></button>
                </div>
            </div>
        </div>
    </div>