<?php $permission = $this->Utilities->check_team_permission_action(array("index","view","edit"), array("projects"), $beforeEdit["data"]["Team"]["id"]);  ?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Proyectos") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Proyectos") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Editar proyecto") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                
                <h2>
                    <?php echo __('Editar proyecto'); ?>
                    <?php echo $this->element("team_name"); ?> 
                </h2>
                <div class="widget-options">
                    <div class="" role="">
                        <?php if(!empty($permission["index"])): ?>  
                                
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                                <button type="button" class="btn btn-secondary ">
                                    <i class="la la-list"></i>
                                    <?php echo __('Listar'); ?>
                                </button>
                            </a>
                                
                        <?php endif;?>

                        <?php if(isset($permission["view"])): ?>
                             <a href="<?php echo $this->Html->url(array('action'=>'view', EncryptDecrypt::encrypt($this->request->data['Project']['id'])));?>">
                                <button type="button" class="btn btn-secondary "><i class="la la-eye"></i><?php echo __('Ver'); ?></button></a>
                            </a> 
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="widget-body">
                

            </div>
            <div class="widget-footer">
                <?php echo $this->Form->create('Project', array('class'=>'frm-share-app', 'role' => 'form','data-parsley-validates','novalidate' => true, 'type' => 'file')); ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-6">
                            <p class="text-warinig"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label required f-blue font-bold'));?></p>
                            <div class='form-group'>
                                <?php echo $this->Form->input('id'); ?>
                                <?php echo $this->Form->label('Project.name',__('Nombre'), array('class'=>'control-label required f-blue font-bold'));?>
                                <?php echo $this->Form->input('name', array('class' => 'form-control border-input', 'label'=>false,'div'=>false)); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo $this->Form->label('Project.number',__('Número en que comenzará el acta'), array('class'=>'control-label f-blue font-bold'));?>
                                <?php echo $this->Form->input('number', array('class' => 'form-control border-input', 'label'=>false,'div'=>false,'disabled' => $this->Utilities->getNumberContactByProyect($project["Project"]["id"]) == 0 ? false : true , 'onKeyPress'=>"return onlyNumberField(event);")); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo $this->Form->label('Project.client_id',__('Cliente'), array('class'=>'control-label required f-blue font-bold'));?>
                                <?php echo $this->Form->input('client_id', array('class' => 'form-control border-input', 'label'=>false,'div'=>false)); ?>
                            </div>
                            <?php echo $this->Form->label('Project.img',__('Imagen del proyecto'), array('class'=>'control-label f-blue required'));?>
                            <div class="form-group">
                                <label for="ProjectImg" class="btn btn-info"><?php echo __("Seleccionar imagen")?></label>
                                <?php echo $this->Form->input('img', array('type'=>'file','class' => 'form-control border-input imageFile', 'label'=>false,'div'=>false,'required' => true, 'style' => 'visibility:hidden;')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                            <div class='form-group'>
                                <?php echo $this->Form->label('Project.description',__('Descripción'), array('class'=>'control-label required f-blue font-bold'));?>
                                <?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none','label'=>false,'div'=>false, 'maxLength' => 300,'required' => true)); ?>
                            </div>
                            <div id="showCharacter"></div> 


                            <?php if(isset($this->request->data["Project"]["users"])):?>
                                <div id="content_users">
                                    <div class='form-group'>
                                        <?php echo $this->Form->label('Project.users',__('Seleccionar usuarios que tendran privacidad'), array('class'=>'control-label f-blue font-bold'));?>
                                        <?php echo $this->Form->input('Project.users', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select', 'multiple' => true, 'options' => $users, 'value' => !empty($this->request->data["Project"]["users"]) ? $this->request->data["Project"]["users"]: array())); ?>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div id="content_users">
                                    <div class='form-group'>
                                        <?php echo $this->Form->label('Project.users',__('Seleccionar usuarios con privilegio de ver las actas.'), array('class'=>'control-label f-blue font-bold'));?>
                                        <?php echo $this->Form->input('Project.users', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select', 'multiple' => true, 'options' => $users, 'value' => !empty($data["Project"]["users"]) ? $data["Project"]["users"]: array())); ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="form-group">
                                <div class="checkbox check_policy">
                                    <label> 
                                        <?php echo $this->Form->input("select_all", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false)); ?>
                                        <?php echo __("Seleccionar todos los usuarios");?>  
                                    </label>
                                </div> 
                            </div>
                        
                            <div class='form-group tags-input-content'>
                                <?php echo $this->Form->label('Project.email_copies',__('Copias al correo electrónico'), array('class'=>'control-label f-blue font-bold'));?>
                                <?php echo $this->Form->input('email_copies', array('type'=>'text', 'class' => 'form-control border-input txt-share-emails', 'label'=>false,'div'=>false)); ?>
                            </div>
                            
                            <?php if(isset($permission["edit"])): ?>
                                <div>
                                    <button type='submit' class='btn btn-primary pull-right'><?php echo __('Guardar') ?></button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<!-- ******************************************** -->



     <?php
        $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
        if ($this->Session->read('Config.language') == 'esp') {
            $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
        }
        $this->Html->script('controller/projects/index.js', ['block' => 'AppScript']); 
    ?>

