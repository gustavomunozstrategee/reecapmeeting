<div id="update-content-project-client"> 
    <table class="table mb-0">
        <thead>
            <tr>
                <th class="table-grid-10 text-center font-bold"><?php echo __('Imagen'); ?></th>
                <th class="table-grid-10"><?php echo __('Nombre') ?></th>
                <th class="table-grid-30"><?php echo __('Descripción') ?></th>
                <th class="table-grid-10 text-center"><?php echo __('Actas totales') ?></th> 
                <th class="table-grid-10"><?php echo __('Fecha de creación') ?></th> 
                <th class="table-grid-10"><?php echo __('Empresa') ?></th> 
                <th class="table-grid-10"><?php echo __('Estado') ?></th> 
                <th class="table-grid-10 text-center"><?php echo __('Acciones') ?></th> 
            </tr>
        </thead>
        <tbody>
        <?php if(!empty($projects)):?>
            <?php foreach ($projects as $project): ?>
                <tr>
                    <td class="table-grid-10">
                        <div class="content-img-table">
                            <a href="<?php echo $this->Html->url('/files/Project/'.$project['Project']['img']); ?>" class="showImagesPopup"> 
                                <img src="<?php echo $this->Html->url('/files/Project/'.$project['Project']['img']); ?>" class="img-responsive avatar rounded-circle img-fluid" alt="Logo" />
                            </a>
                        </div>
                    </td>
                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($project['Project']['name']),100); ?></td>
                    <td class="table-grid-30 td-word-wrap"><?php echo $this->Text->truncate(h($project['Project']['description']),100); ?></td>
                    <td class="table-grid-10 text-center td-word-wrap"><?php echo h(count($project['Contac'])); ?></td>
                    <td class="table-grid-10 td-word-wrap"><?php echo h(($this->Time->format('d-m-Y h:i A', $project['Project']['created']))); ?></td>
                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($project['Team']['name']),100); ?></td>
                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Utilities->showState($project['Project']['state']); ?></td>
                    <td class="table-grid-10 td-actions text-center">
                        <?php $permission = $this->Utilities->check_team_permission_action(array("view"), array("projects"), $project['Team']['id']); ?> 
                        <?php if (isset($permission["view"])): ?>
                            <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'projects','action' => 'view', EncryptDecrypt::encrypt($project['Project']['id']))); ?>" title="<?php echo __('Ver proyecto'); ?>" class="btn btn-xs isTooltip" data-toggle="tooltip" data-placement="top">
                                <i class="fa fa-eye"></i>
                            </a>
                        <?php endif; ?>
                        <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'contacs','action' => 'projects_contacs', EncryptDecrypt::encrypt($project['Project']['id']))); ?>" title="<?php echo __('Ver actas'); ?>" class="btn btn-xs isTooltip" data-toggle="tooltip" data-placement="top">
                          <i class="flaticon-file"></i>
                        </a> 
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else: ?>
            <tr>
                <td class="text-center" colspan="7"><?php echo __("Este cliente no tiene proyectos creados.");?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <div class="table-pagination">
    <div>
        <?php 
            $this->Paginator->options(array('update' => '#update-content-project-client','url'=>array('controller'=>'projects','action'=>'projects_clients', EncryptDecrypt::encrypt($clientId))));
        ?>
           <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
           
    </div>
    <ul class="pagination f-paginationrecapmeeting">
        <?php
            echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled',['model'=>'Project']));
            echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
        ?>
    </ul> 
    </div>
    <?php echo $this->Js->writeBuffer(); ?>
</div>

<script>
 	$(".isTooltip").hover(function(){ 
        $(this).tooltip('show');
	})
</script>