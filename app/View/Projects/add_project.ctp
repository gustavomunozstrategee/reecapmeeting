<?php echo $this->Form->create('Project', array('role' => 'form','data-parsley-validate','novalidate' => true, 'type' => 'file')); ?>
<div class="height-scroll-modal-400">
	<div class="row">
		<div class='col-md-12'>
			<p style="color:red"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label required'));?></p>
		</div>
		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Project.name',__('Nombre'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'maxLength' => 80)); ?>
			</div>
		</div>
		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Project.description',__('Descripción'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none textarea_project','label'=>false,'div'=>false)); ?>
			</div>
			<div id="showCharacterProject">0/300</div>
		</div>
		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Project.number',__('Número en que comenzará el acta'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('number', array('class' => 'form-control border-input','label'=>false,'div'=>false, "value" => 1, 'onkeypress' => "return onlyNumberField(event)")); ?>
			</div>
		</div>
		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Project.client_id',__('Cliente'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('client_id', array('class' => 'form-control border-input','readonly' => true,'label'=>false,'div'=>false)); ?>
			</div>
		</div>

		<div class='col-md-12'>
			<?php echo $this->Form->label('Project.img',__('Imagen del proyecto'), array('class'=>'control-label f-blue required'));?>
			<div class='form-group'>
				<label for="ProjectImg" class="btn btn-info"><?php echo __("Seleccionar imagen")?></label>
				<?php echo $this->Form->input('img', array('type'=>'file','class' => 'form-control border-input imageFile','label'=>false,'style' => 'visibility:hidden;','div'=>false,'placeholder' => __('imagen'))); ?>
			</div>
		</div>

		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Project.users',__('Seleccionar usuarios con privilegio de ver las actas.'), array('class'=>'control-label f-blue font-bold'));?>
				<?php echo $this->Form->input('Project.users', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select', 'multiple' => true, 'options' => $users)); ?>
			</div>
		</div>
		<div class="col-md-12">
			<div class='form-group tags-input-content'>
				<?php echo $this->Form->label('Project.email_copies',__('Copias al correo electrónico'), array('class'=>'control-label f-blue font-bold'));?>
				<?php echo $this->Form->input('email_copies', array('type'=>'text', 'class' => 'form-control border-input txt-share-emails','label'=>false,'div'=>false)); ?>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				<div class="checkbox check_policy">
					<label> 
						<?php echo $this->Form->input("select_all", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false)); ?>
						<?php echo __("Seleccionar todos los usuarios");?>  
					</label>
				</div> 
			</div>
		</div>

		<div class='col-md-12'>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Cancelar') ?></button>
	        <button type="button" class="btn btn-primary"  id="btn_saveAddProyecto" ><?php echo __('Guardar') ?></button>
		</div>
	</div>

</div>
<?php echo $this->Form->end(); ?>