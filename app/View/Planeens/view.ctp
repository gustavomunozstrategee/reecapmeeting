   <?php //echo $this->Html->script('muuri.js'); ?>
<div class="todoView">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.8.2/angular.min.js" integrity="sha512-7oYXeK0OxTFxndh0erL8FsjGvrl2VMDor6fVqzlLGfwOQQqTbYsGPv4ZZ15QHfSk80doyaM0ZJdvkyDcVO7KFA==" crossorigin="anonymous"></script>
 
 <link rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css
" crossorigin="anonymous" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<div class="<?php echo $this->request->is('mobile') ? '' : 'container-fluid' ?>">
<div class="<?php echo $this->request->is('mobile') ? '' : 'container-fluid' ?>">
 
  <div class="col-xl-12">

<div class="widget widget-18 has-shadow">
  <?php echo $this->element('./Planeen/left_nav'); ?>
    <div class="widget-header bordered d-flex align-items-center">
    <h2><i class="la la-list-alt la-2x align-middle pr-2"></i> <?php echo $planeen[0]["Planeen"]["name"] ?></h2>
    <div class="media-right align-self-center">
    <?php if($user_role == 0): ?>
      <div class="actions">
          <a href="<?php echo $this->webroot."planeens/edit_planeen/". EncryptDecrypt::encrypt($id) ?>"><i class="la la-wrench reply"></i></a>
      </div>
    <?php endif; ?>
    </div>
  </div>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.3.2/web-animations.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/haltu/muuri@0.9.3/dist/muuri.min.js"></script>
   <?php //echo $this->Html->script('muuri.js'); ?>
<link rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css
" crossorigin="anonymous" />

 


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="col-md-12" style="margin-left: -5px;margin-bottom: 10px;">

  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link show active" id="home-tab" data-toggle="tab" href="<?php echo $this->webroot."planeens/view/". EncryptDecrypt::encrypt($id) ?>" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-list"></i> Pizarra de Actividades</a>
    </li>
    <?php if($user_role == 0 || $user_role == 1): ?>
    <li class="nav-item">
      <a class="nav-link show" id="profile-tab"   href="<?php echo $this->webroot."planeens/admin/". EncryptDecrypt::encrypt($id) ?>"   ><i class=" mdi mdi-chemical-weapon"></i> Administrar Planeación</a>
    </li>
    <?php endif; ?>
    <li class="nav-item">

      <a class="nav-link" href="<?php echo $this->webroot."planeens/export_excel/". EncryptDecrypt::encrypt($id) ?>"   ><i class="la la-file-excel-o"></i> Exportar Informe en Excel</a>
    </li>
  </ul>
</div>
<style type="text/css">
  .sizeNew{
   font-size: 9px;cursor: inherit;
  }
  .sizeNew2{
   font-size: 9px;cursor: inherit;
  }
   .sizeNew2{
   font-size: 11px;cursor: inherit;
   text-transform: uppercase;
   font-family: "Helvetica";
   margin-left: 5px
  }
   
</style>
<?php echo $this->Form->create('Report', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40')); ?>
  <div class="col-md-12" style="margin-bottom: 12px">
    <div class="row">
      <div class="col-md-5"> 
          <label class="form-control-label"><?php echo __("Filtrar Tarea") ?><span class="text-danger ml-2"></span></label>
          <?php echo $this->Form->input('filtro', array('class' => 'form-control ', 'label' => false, 'div' => false)); ?>  
      </div>
      <div class="col-md-6"> 
          <label class="form-control-label"><?php echo __("Filtrar Usuario") ?><span class="text-danger ml-2"></span></label>
          <?php echo $this->Form->input('participantes', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple', 'options' => $users)); ?>  
      </div>
    </div>
    <div class="row">
       <div class="col-md-5">
            <label class="form-control-label">Módulos / Bloques de trabajos<span class="text-danger ml-2"></span></label>
              <label class="form-control-label">Módulos / Bloques de trabajos<span class="text-danger ml-2"></span></label>
              <select multiple="" class="form-control select2N " name="data[Report][tags][]">
                <?php foreach ($tags as $key => $value): ?>
                  <?php 
                    $selected = 0;
                    if(!empty($tagsFilter)) {
                      foreach ($tagsFilter as $key2 => $value2) {
                        if($value2 == $value){
                          $selected = 1;
                          break;
                        }
                      }
                    }
                   ?>
                  <option <?php if($selected) echo "selected" ?> value="<?php echo $value ?>"><?php echo $value ?></option>
                <?php endforeach ?>
              </select>
        </div>
      <div class="col-md-3">
        <div class="form-group">
                 <?php echo $this->Form->label('Meeting.start_date',__('Fecha Inicio'), array('class'=>'control-label f-blue'));?>
                <div class='input-group date ' id=''>
                    <?php echo $this->Form->input('inicio', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                    </span>
                </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
                 <?php echo $this->Form->label('Meeting.start_date',__('Fecha Fin'), array('class'=>'control-label f-blue'));?>
                <div class='input-group date ' id=''>
                  
                    <?php echo $this->Form->input('fin', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                    </span>
                </div>
        </div>
    </div>
      <div class="col-md-1">
        <button class="btn btn-success" type="submit" style="margin-top: 25px">Filtrar</button>
      </div>
       
    </div>
  </div>
<?php echo $this->Form->end(); ?>
<?php 
$tamano = "col-md-4";
if($planeen[0]["Planeen"]["qa"]){
$tamano = "col-md-3";
}
?>   
<?php echo $this->Form->end(); ?>
<div class="drag-container"></div>
<div class="board" style="margin-bottom: 20px;">
  <div class="board-column todo <?php echo $tamano ?>">
    <div class="board-column-container">
      <div class="board-column-header">Pendiente</div>
      <div class="board-column-content-wrapper">
        <div class="pendiente board-column-content" style="<?php echo $this->request->is('mobile') ? 'width: 105px;' : '' ?>">
            <?php foreach($tasks as $key => $value): ?>
              <?php if($value["PlaneensTask"]["state"] == "1"): ?>
                <div class="<?php echo $value["PlaneensTask"]["id"] ?> board-item"  data-id="1">
                  <div class="board-item-content">
                    <div class="row">
                      <div class="col-md-8 ">
                           <a class="categoria" >
                            
                             <t  class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-user"></i> <?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></t>
                               <t  class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-accusoft"></i>[<?php echo $value["PlaneensTask"]["tags"] ?>] <?php echo $value["Project"]["name"] ?></t>
                            </a>
                      </div>
                      <div class="col-md-4 float-right">
                            <a class="<?php echo $this->request->is('mobile') ? '' : 'prioridad' ?>" href="javascript:void(0);">
                               <span  class="badge badge-pill badge-danger sizeNew"> <i class="fa fa-calendar"></i> <?php echo $value["PlaneensTask"]["deadline_date"] ?> </span>
                            </a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-1">
                         <img class="user-img rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" width="32px" height="32px" title="<?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?>">
                      </div>
                      <div class="col-md-10">
                        <span class="sizeNew2">  <?php echo $value["PlaneensTask"]["description"] ?></span>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <!-- <br> -->
      </div>
    </div>
  </div>
  <div class="board-column working <?php echo $tamano ?>">
    <div class="board-column-container">
      <div class="board-column-header">En Proceso</div>
      <div class="board-column-content-wrapper">
        <div class="proceso board-column-content" style="<?php echo $this->request->is('mobile') ? 'width: 105px;' : '' ?>">
           <?php foreach($tasks as $key => $value): ?>
              <?php if($value["PlaneensTask"]["state"] == "2"): ?>
                <div class="<?php echo $value["PlaneensTask"]["id"] ?> board-item"  data-id="1">
                  <div class="board-item-content">
                    <div class="row">
                      <div class="col-md-8 ">
                           <a class="categoria"  >
                             <t  class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-user"></i> <?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></t>
                               <t class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-accusoft"></i> [<?php echo $value["PlaneensTask"]["tags"] ?>] <?php echo $value["Project"]["name"] ?></t>
                            </a>
                      </div>
                      <div class="col-md-4 float-right">
                            <a class="<?php echo $this->request->is('mobile') ? '' : 'prioridad' ?>" href="javascript:void(0)">
                               <span class="badge badge-pill badge-danger sizeNew"> <i class="fa fa-calendar"></i> <?php echo $value["PlaneensTask"]["deadline_date"] ?> </span>
                            </a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-1">
                         <img class="user-img rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" width="32px" height="32px" title="<?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?>">
                      </div>
                      <div class="col-md-10">
                        <spa class="sizeNew2"><?php echo $value["PlaneensTask"]["description"] ?></span>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            <?php endforeach; ?> 
        </div>
        <!-- <br>   -->
      </div>
    </div>
  </div>
  <?php if($planeen[0]["Planeen"]["qa"]): ?>
  <div class="board-column  <?php echo $tamano ?>">
    <div class="board-column-container">
      <div class="board-column-header">QA</div>
      <div class="board-column-content-wrapper">
        <div class="qa board-column-content" style="<?php echo $this->request->is('mobile') ? 'width: 105px;' : '' ?>">
           <?php foreach($tasks as $key => $value): ?>
              <?php if($value["PlaneensTask"]["state"] == "3"): ?>
                <div class="<?php echo $value["PlaneensTask"]["id"] ?> board-item"  data-id="1">
                  <div class="board-item-content">
                    <div class="row">
                      <div class="col-md-8 ">
                           <a class="categoria"  >
                             <t  class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-user"></i> <?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></t>
                               <t class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-accusoft"></i> [<?php echo $value["PlaneensTask"]["tags"] ?>] <?php echo $value["Project"]["name"] ?></t>
                            </a>
                      </div>
                      <div class="col-md-4 float-right">
                            <a class="<?php echo $this->request->is('mobile') ? '' : 'prioridad' ?>" href="javascript:void(0)">
                               <span class="badge badge-pill badge-danger sizeNew"> <i class="fa fa-calendar"></i> <?php echo $value["PlaneensTask"]["deadline_date"] ?> </span>
                            </a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-1">
                         <img class="user-img rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" width="32px" height="32px" title="<?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?>">
                      </div>
                      <div class="col-md-10">
                        <span class="sizeNew2"><?php echo $value["PlaneensTask"]["description"] ?></span>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
  <div class="board-column done <?php echo $tamano ?>">
    <div class="board-column-container"> 
      <?php if($this->request->is('mobile')){ ?>
        <br>
      <?php } ?>
      <div class="board-column-header">Terminado</div>
      <div class="board-column-content-wrapper">
        <div class="terminada board-column-content" style="<?php echo $this->request->is('mobile') ? 'width: 105px;' : '' ?>">
           <?php foreach($tasks as $key => $value): ?>
              <?php if($value["PlaneensTask"]["state"] == "4"): ?>
                <div class="<?php echo $value["PlaneensTask"]["id"] ?> board-item"  data-id="1">
                  <div class="board-item-content">
                    <div class="row">
                      <div class="col-md-8 ">
                           <a class="categoria"  >
                            <t  class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-user"></i> <?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></t>
                               <t class="text-primary sizeNew"  style="text-transform: uppercase;"> <i class="fa fa-accusoft"></i> [<?php echo $value["PlaneensTask"]["tags"] ?>] <?php echo $value["Project"]["name"] ?></t>
                            </a>
                      </div>
                      <div class="col-md-4 float-right">
                            <a class="<?php echo $this->request->is('mobile') ? '' : 'prioridad' ?>" href="javascript:void(0);">
                               <span class="badge badge-pill badge-danger sizeNew"> <i class="fa fa-calendar"></i> <?php echo $value["PlaneensTask"]["deadline_date"] ?> </span>
                            </a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-1">
                         <img class="user-img rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" width="32px" height="32px" title="<?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?>">
                      </div>
                      <div class="col-md-10">
                        <span class="sizeNew2"><?php echo $value["PlaneensTask"]["description"] ?></span>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <!-- <br> -->
      </div>
    </div>
  </div>
</div>
<div id="modalEdit" >
  <div class="modal fade" id="ModalLoad"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  </div>
</div>

<?php if($this->request->is('mobile')){ ?>
<style type="text/css">
  .board-item {
    position: absolute; 
  }
</style>
<?php } else { ?>
<style type="text/css">
  .board-item {
    position: absolute;
    width: calc(100% - 16px);
    margin: 8px;
  }
</style>
<?php } ?>

<style type="text/css">
  * {
  box-sizing: border-box;
}

 
.drag-container {
  position: fixed;
  left: 0;
  top: 0;
  z-index: 1000;
}
.prioridad{
  float: right;
}
.categoria{
  float: left;
  font-size: 12px;
}
.board {
  position: relative;
}
.board-column {
  position: absolute;
  left: 0;
  top: 0;
  padding: 0 10px;
  width: calc(100% / 3);
  z-index: 1;
}
.board-column.muuri-item-releasing {
  z-index: 2;
}
.board-column.muuri-item-dragging {
  z-index: 3;
  cursor: move;
}
.board-column-container {
  position: relative;
  width: 100%;
  height: 100%;
}
.board-column-header {
  position: relative;
  height: 50px;
  line-height: 50px;
  overflow: hidden;
  padding: 0 20px;
  text-align: center;
  background: #333;
  color: #fff;
  border-radius: 5px 5px 0 0;
  font-weight: bold;
  letter-spacing: 0.5px;
  text-transform: uppercase;
}
@media (max-width: 600px) {
  .board-column-header {
    text-indent: -1000px;
  }
}
.board-column.todo .board-column-header {
  background: #4A9FF9;
}
.board-column.working .board-column-header {
  background: #f9944a;
}
.board-column.done .board-column-header {
  background: #2ac06d;
}
.board-column-content-wrapper {
  position: relative;
  padding: 8px;
  background: #f0f0f0;
  height: calc(100vh - 90px);
  overflow-y: auto;
  border-radius: 0 0 5px 5px;
}
.board-column-content {
  position: relative;
  min-height: 100%;
}

.board-item.muuri-item-releasing {
  z-index: 9998;
}
.board-item.muuri-item-dragging {
  z-index: 9999;
  cursor: move;
}
.board-item.muuri-item-hidden {
  z-index: 0;
}
.board-item-content {
  position: relative;
  padding: 6px;
  background: #fff;
  border-radius: 4px;
  font-size: 17px;
  cursor: pointer;
  -webkit-box-shadow: 0px 1px 3px 0 rgba(0,0,0,0.2);
  box-shadow: 0px 1px 3px 0 rgba(0,0,0,0.2);
}
@media (max-width: 600px) {
  .board-item-content {
    text-align: center;
  }
  .board-item-content span {
    display: none;
  }
}
</style>

<script type="text/javascript">
function getActualizacionPendiente(){
     $.ajax({
          type: "POST",
          url: "../getActualizacionPendiente/<?php echo $id; ?>",
          data: {},
          beforeSend: function(){
          },
          complete:function(data){
          },
          success: function(data){
              if(data == 1){
                location.reload();
              }else{

              }
          },
          error: function(data){
            console.log('Error de carga');
            // alert("Error de carga");
          }
      });
  }

   
 $( document ).ready(function() {  
 
  setInterval('getActualizacionPendiente()',5000);

  $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });
  $('.select2_all').select2({
    });
  $(".select2N").select2({
      tags: true,
      tokenSeparators: ['{}', ' ']
  })
});
var ifDoubleClick = 0;
setInterval(function(){ 
  ifDoubleClick = 0;
}, 500);
var dragContainer = document.querySelector('.drag-container');
var itemContainers = [].slice.call(document.querySelectorAll('.board-column-content'));
var columnGrids = [];
var boardGrid;
var ItemIdAfter = "";
var ItemStateAfter = "";
var ItemIdBefore = ""
var ItemStateBefore = "";
// Init the column grids so we can drag those items around.
itemContainers.forEach(function (container) {
  var grid = new Muuri(container, {
    items: '.board-item',
    dragEnabled: true, 
    dragSort: function () {

      return columnGrids;
    },
    dragContainer: dragContainer,
    dragAutoScroll: {
      targets: (item) => {
        return [
          { element: window, priority: 0 },
          { element: item.getGrid().getElement().parentNode, priority: 1 },
        ];
      }
    },
  })
  .on('dragInit', function (item) {
    if(ifDoubleClick == 1){
      openModalEdit(item._element.classList["0"])
      //alert("HA DADO DOBLE CLICK: " + item._element.classList["0"])
    }else{
      ifDoubleClick = 1
       // console.log("HA DADO UN CLICK")
    }
    //console.log(item.getGrid())
    ItemIdAfter = item._element.classList["0"];
    ItemStateAfter = item.getGrid()._element.classList["0"]
    item.getElement().style.width = item.getWidth() + 'px';
    item.getElement().style.height = item.getHeight() + 'px'; 
  })
  .on('dragReleaseEnd', function (item) {
    ItemIdBefore = item._element.classList["0"];
    ItemStateBefore = item.getGrid()._element.classList["0"]

    if(ItemStateAfter != ItemStateBefore){
    updateTaskProccess(ItemIdBefore,ItemStateBefore)
    }
   // console.log(ItemIdAfter)
    //console.log(ItemStateAfter)
   // console.log(ItemIdBefore)
   // console.log(ItemStateBefore)
    //console.log( $("." + item._element.classList["0"]) )
    //console.log(item)
    item.getElement().style.width = '';
    item.getElement().style.height = '';
    item.getGrid().refreshItems([item]);
   // console.log(item.getGrid())
    columnGrids.forEach(function (muuri) {
      muuri.refreshItems();
    });
    grid.refreshItems().layout(); 
  })
  .on('layoutStart', function () {
   // console.log(boardGrid)
    boardGrid.refreshItems().layout();
  });
  //console.log(boardGrid)
  columnGrids.push(grid);

  setTimeout(function() {
    grid.refreshItems().layout();    
  }, 1000);

});
  // Init board grid so we can drag those columns around.
  boardGrid = new Muuri('.board', {
    dragEnabled: true,
    dragHandle: '.board-column-header', 
  });

  setTimeout(function() {
    boardGrid.refreshItems().layout();    
  }, 200);
  function updateTaskProccess(ItemID,ItemState){
     $.ajax({
          type: "POST",
          url: "../updateTaskProccess/<?php echo $id; ?>",
          data: {id:ItemID,state:ItemState},
          beforeSend: function(){
          },
          complete:function(data){
          },
          success: function(data){
              if(data == "3"){
                Swal.fire(
                '¡Bien!',
                'No tienes permisos para mover esta actividad, al actualizar volvera a su estado.',
                'error'
              )
              }
              
          },
          error: function(data){
             console.log("Error de carga");
              // alert("Error de carga");
          }
      });
  }
function openModalEdit(id){ 
  $.ajax({
        type: "GET",
        url: "../edit_modal/"+id+"/<?php echo $id ?>",
        
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        success: function (response) {
            $('#ModalLoad').html(response); //add the partial view into the modal content.
            $('#ModalLoad').modal('show'); //display the modal popup.
        },
        failure: function (response) {
            console.log(response.responseText);
            // alert(response.responseText);
        },
        error: function (response) {
            console.log(response.responseText);
            // alert(response.responseText);
        }
    });

  
}
</script>
    </div>
  </div>
</div>
<div id="modalEdit" >
  <div class="modal fade" id="ModalLoad"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  </div>
</div>
<?php
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->script("differenceHours", ['block' => 'AppScript']);
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
?>
<?php
 echo $this->Html->script('materialDateTimePicker.js', ['block' => 'AppScript']);
?>

<?php if($this->request->is('mobile')) { ?>
<style type="text/css">
  .board-item {
    margin-bottom: 20px;
  } 
  @media (max-width: 600px) { 
    .board-item-content span {
      display: block !important;  
       margin-bottom: 40px;    
    }
  } 
</style>
<?php } ?>

<style type="text/css">
  .loadContent{
    margin-top: 40px;
  }
  .daterangepicker{
    z-index: 1000000 !important;
  }
</style>
<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
.widget-body{
    overflow: initial !important;
}
.board-column.working .board-column-header {
    background: darkred !important;
}
.board-column.done .board-column-header {
    background: darkgreen !important;
}
.board-column.todo .board-column-header {
    background: #2C314D;
}
.badge-danger {
    
    background-color:  darkred !important;
}

.board-item-disabled {
    position: absolute;
    width: calc(100% - 16px);
    margin: 8px;
}

</style>
<div>