<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
 <div class="container-fluid">
  <?php //echo "<pre>"; print_r($this->request->data); ?>
    <!-- Begin Page Header-->
    <!-- End Page Header -->
    
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="widget widget-18 has-shadow">
                                <?php echo $this->element('./Planeen/left_nav'); ?>
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header bordered d-flex align-items-center">
                                        <h2><?php echo __("Editar Planeen") ?></h2>
                                        <div class="media-right align-self-center">
                                             
                                        </div>
                                    </div>
                                    <!-- End Widget Header -->
                                    <div class="widget-body">

                                      <div class="tab-pane active show" id="tab1">
                                          <?php echo $this->Form->create('Planeen', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40')); ?>                   
                                                            <div class="form-group row mb-3">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label"><?php echo __("Nombre") ?><span class="text-danger ml-2">*</span></label>
                                                               
                                                                    <?php echo $this->Form->input('name', array('class' => 'form-control',"label" => false))?>  
                                                                </div>
                                                                <div class="col-xl-12">
                                                                    <label class="form-control-label"><?php echo __("Uso de columna QA") ?><span class="text-danger ml-2"></span></label>
                                                                    <?php echo $this->Form->input('qa', array('class' => 'form-control', 'label' => false, 'div' => false,'type' => 'select', 'options' => Configure::read("QA"), "selected" => $this->request->data["Planeen"]["qa"])); ?>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3">
                                                              <div class="col-xl-12">
                                                                <label class="form-control-label">Tags<span class="text-danger ml-2"></span></label>
                                                                <select multiple="" class="form-control select2N " name="data[Planeen][tags][]">
                                                                  <?php foreach ($tagsEncontrados as $key => $value): ?>
                                                                      <option selected="true"><?php echo $value ?></option>
                                                                  <?php endforeach ?>
                                                                </select>

                                                                    
                                                            </div>
                                                              
                                                            </div>
                                                            
                                                            
                                                             
                                                            
                                                             
                                                            <ul class="pager wizard text-right">
                                                                 
                                                                <li class="next d-inline-block">
                                                                  <button class="btn btn-gradient-01"><?php echo __("Guardar Cambio") ?></button>
                                                                   
                                                                </li>
                                                            </ul>
                                            <?php echo $this->Form->end(); ?>

                                            <hr>
                                              <?php echo $this->Form->create('PlaneensUser', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40',"url"=>"addUsers")); ?>
                                              <?php echo $this->Form->input('id', array('hidden' => true, 'value' => $id)); ?> 

                                            <div class="form-group row mb-3">
                                                                <div class="col-xl-9 mb-3">
                                                                    <label class="form-control-label"><?php echo __("Usuarios") ?><span class="text-danger ml-2"></span></label>

                                                                    <?php echo $this->Form->input('participantes', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple','value' => @$this->request->data["Contac"]["funcionarios"], 'options' => $collaborators)); ?>


                                                                 
                                                                </div>
                                                                <div class="col-xl-3">
                                                                    <label class="form-control-label">Permisos<span class="text-danger ml-2"></span></label>
                                                                    <?php echo $this->Form->input('PlaneensUser.permisos', array('class' => 'form-control', 'label' => false, 'div' => false,'type' => 'select','value' => @$this->request->data["Contac"]["funcionarios"], 'options' => Configure::read("SlatesRoles"))); ?>
                                                                </div>

                                                            </div>
                                                             <ul class="pager wizard text-right">
                                                                 
                                                                <li class="next d-inline-block">
                                                                  <button class="btn btn-gradient-01"><?php echo __("Añadir Usuarios") ?></button>
                                                                   
                                                                </li>
                                                            </ul>
                                                            <br>
                                                             <?php echo $this->Form->end(); ?>
                                            <table class="table mb-0">
                                              <thead>
                                                    <tr>
                                                        <th><?php echo __("Usuario") ?></th>
                                                        <th><?php echo __("Permisos") ?></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                              <?php foreach ($lista as $key => $value): ?>
                                                <?php 
                                                  


                                                 ?>
                                                <?php if($value["PlaneensUser"]["role_id"] != 0): ?>
                                                  <tr class="remove_<?php echo $value["PlaneensUser"]["id"] ?>">
                                                      <td><?php echo $value["User"]["firstname"]." ".$value["User"]["lastname"] ?></td>
                                                      <td>
                                                        <select data-id="<?php echo $value["PlaneensUser"]["id"] ?>" class="form-control editListTask" >
                                                            <option  <?php if($value["PlaneensUser"]["role_id"] == 2) echo "selected" ?> value="2">Colaborador</option>
                                                            <option <?php if($value["PlaneensUser"]["role_id"] == 1) echo "selected" ?>  value="1">Editor</option>
                                                        </select>

                                                        
                                                        
                                                      </td>
                                                      <td class="td-actions" style="justify-content: center;">
                                                        <a href="javascript:void(0);" data-id="<?php echo $value["PlaneensUser"]["id"] ?>" class="removeUser"><i class="la la-close delete"></i></a>
                                                      </td>
                                                  </tr>
                                                
                                                <?php endif?>
                                              <?php endforeach; ?>
                                            </table>
                                        </div>
                                        
                                         
                                        
                                    </div>
                                </div>
                            </div>
</div>

<?php
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
   $this->Html->script('lib/select2/select2.min.js',               ['block' => 'AppScript']);
  $this->Html->script('lib/bootstrap-tagsinput.min.js',           ['block' => 'AppScript']);
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
?>
<script type="text/javascript">
    $(document).ready(function(){
  $('#buscador').keyup(function(){
     var nombres = $('.nombres');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.item').show(); 
              }else{
                   $(nombres[i]).parents('.item').hide();
              }
          }
     }
  });
  $('.select2_all').select2({
    });
  $(".select2N").select2({
      tags: true,

      tokenSeparators: ['{}', ' '],

      placeholder: "Ingrese los Módulos / Bloques de trabajos necesarios..."
  })

  $( ".editListTask" ).change(function() {
      id = $(this).data("id")
      role_id = $(this).val()
      $.post( "../editPermission", {id:id, role_id:role_id })
          .done(function( data ) {
            if(data == 1){
              Swal.fire(
                '¡Bien!',
                'Permisos editados correctamente.',
                'success'
              )
            }else{
              Swal.fire(
                'Error!',
                'Ha ocurrido un error, intente nuevamente.',
                'error'
              )
            }
          });

      
    });

  $( ".removeUser" ).click(function() {
      id = $(this).data("id")
      $.post( "../removeUser", {id:id})
          .done(function( data ) {
            if(data == 1){
              $(".remove_"+id).remove()
              Swal.fire(
                '¡Bien!',
                'Usuario removido correctamente.',
                'success'
              )
            }else{
              Swal.fire(
                '¡Bien!',
                'Ha ocurrido un error, intente nuevamente.',
                'error'
              )
            }
          });

      
    });


});

</script>

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
</style>
 