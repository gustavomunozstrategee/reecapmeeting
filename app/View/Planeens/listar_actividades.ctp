 
<link rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css
" crossorigin="anonymous" />
<ul class="ticket list-group ">
    <!-- 01 -->
    <?php if(!empty($actividades)): ?>
    <?php foreach($actividades as $ke=>$value): ?>
    <li class="list-group-item  item task_remove_<?php echo $value["PlaneensTask"]["id"] ?>">
        <div class="media ">
           
            <div class="media-left align-self-center pr-4">
                <img src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" class="user-img rounded-circle" alt="user img" style="width: 60px;height: 60px">
            </div>

            <div class="media-body align-self-center">

            <span style="width:160px;" class="pull-right">
                                                                    <span class="badge-text badge-text-small danger" style="width: 100%;">
                                                                   <i class="mdi mdi-tag"></i> <?php echo $value["PlaneensTask"]["tags"] ?>                                                                    </span>
                                                                </span>

                <div class="username">
                    <h4><?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></h4>
                </div>
                <div class="msg">
                    <p class="nombres" style="margin-bottom: 0px;">
                       <?php echo $value["PlaneensTask"]["description"] ?>
                    </p>
                </div>
                <div class="event-desc mr-3">
                    <i class="fa fa-calendar"></i>
                    <t>
                    <?php echo $value["PlaneensTask"]["deadline_date"] ?>
                    </t> 
                    <br>  
                    <t><b>Estado:  </b>
                      
                    <?php
                     if($value["PlaneensTask"]["state"] == 1){
                      echo "<t style='color:#4A9FF9'>Pendiente</t>";
                     }elseif($value["PlaneensTask"]["state"] == 2){
                      echo "<t style='color:#f9944a'>En Proceso</t>";
                     }elseif($value["PlaneensTask"]["state"] == 3){
                      echo "<t style='color:#f9944a'>En QA</t>";
                     }else{
                      echo "<t style='color:#2ac06d'>Terminada</t>";
                     }

                     ?>

                    </t> 
                </div>
            </div>
                <div class="media-right pr-3 align-self-center">
                    <div class="like text-center">
                      <a class="editTask" data-id="<?php echo EncryptDecrypt::encrypt($value["PlaneensTask"]["id"]) ?>" href="javascript:void(0)">
                        <i class="la la-edit la-2x"></i>
                      </a>
                         
                    </div>
                </div>
             
             
                 
             
              <div class="media-right pr-3 align-self-center">
                    <div class="like text-center">
                      <a class="eliminarActividad" data-id="<?php echo EncryptDecrypt::encrypt($value["PlaneensTask"]["id"]) ?>" href="javascript:void(0)" >
                        <i class="la la-remove la-2x"></i>
                      </a>
                         
                    </div>
                </div>
            
        </div>
         <hr>
    </li>


  <?php endforeach; ?>
  <li class="item">
  </li>
  <?php else: ?>
    <div class="alert alert-outline-success" role="alert">
      <strong></strong> <?php echo __("Aun no tienes  actividades en este Planeen.") ?>
  </div>
  <?php endif; ?>
</ul>