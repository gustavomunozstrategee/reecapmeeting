<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

<?php if($this->request->is('mobile')){ ?>
  <div>
  <div>
<?php } else { ?>
  <div class="container-fluid">
  <div class="container-fluid">
<?php } ?>
     
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
    <div class="widget widget-18 has-shadow">
      <?php echo $this->element('./Planeen/left_nav'); ?>


        <div class="widget-header bordered d-flex align-items-center">
            <h2><i class="la la-list-alt la-2x align-middle pr-2"></i> <?php echo $planeen[0]["Planeen"]["name"] ?></h2>
            <div class="media-right align-self-center">
              <?php if($user_role == 0): ?>
                <div class="actions">
                    <a href="<?php echo $this->webroot."planeens/edit_planeen/". EncryptDecrypt::encrypt($id) ?>"><i class="la la-wrench reply"></i></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.3.2/web-animations.min.js"></script>
        <script src="https://cdn.jsdelivr.net/gh/haltu/muuri@0.9.3/dist/muuri.min.js"></script>
        <script src="https://use.fontawesome.com/69b310a0ca.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <div class="col-md-12" style="margin-left: -5px;margin-bottom: 10px;">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link show " id="home-tab"  href="<?php echo $this->webroot."planeens/view/". EncryptDecrypt::encrypt($id) ?>" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-list"></i> Pizarra de Actividades</a>
            </li>
            <?php if($user_role == 0 || $user_role == 1): ?>
            <li class="nav-item">
              <a class="nav-link show active" id="profile-tab"   href="<?php echo $this->webroot."planeens/admin/". EncryptDecrypt::encrypt($id) ?>"   ><i class=" mdi mdi-chemical-weapon"></i> Administrar Planeación</a>
            </li>
            <?php endif; ?>
            <li class="nav-item">

              <a class="nav-link" href="<?php echo $this->webroot."planeens/export_excel/". EncryptDecrypt::encrypt($id) ?>"   ><i class="la la-file-excel-o"></i> Exportar Informe en Excel</a>
            </li>
          </ul>
        </div>


      <div class="widget-body">
        <div class="input-group">
          <input id="buscador" type="text" class="form-control no-ppading-right no-padding-left" placeholder=" Buscar Actividad...">
        </div>
     
        <div class="row">
          <div class="col-md-12">
              <div class="widget-body">
                <a class="btn btn-primary pull-right" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                  <?php echo __("Nueva Actividad") ?>
                </a>
              </div>
          </div>
        </div>
 
        <div class="collapse col-md-12" id="collapseExample">
          <hr>
          <?php echo $this->Form->create('PlaneensTask', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40','url'=>'/planeens/add_actividad/'.EncryptDecrypt::encrypt($id))); ?>
          <div class="row">
              <div class="col-md-12">
                  <div class='form-group'>
                    <?php echo $this->Form->input('slateId', array('class'=>'control-label','value'=>$idList,"style"=>"display:none","label"=>false));?>
                    <?php echo $this->Form->label('description',__('Descripción de la actividad'), array('class'=>'control-label'));?>
                    <?php echo $this->Form->input('description', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2")); ?>
                </div>
              </div> 
          </div>
                             
          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                    <?php echo $this->Form->label('Meeting.start_date',__('Fecha de Terminación'), array('class'=>'control-label f-blue'));?>
                    <div class='input-group date ' id=''>
                      
                        <?php echo $this->Form->input('deadline_date', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                        </span>
                    </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class='form-group'>
                      <?php echo $this->Form->label('SlatesTask.user_id',__('Usuario responsable'), array('class'=>'control-label select2'));?>
                      <?php echo $this->Form->input('user_id', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class='form-group'>
                      <?php echo $this->Form->label('SlatesTask.tags',__('Módulo / Bloque de Trabajo'), array('class'=>'control-label select2'));?>
                      <?php echo $this->Form->input('tags', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                  </div>
              </div>
              <div class="col-md-12">
                  <div class='form-group'>
                      <?php echo $this->Form->label('SlatesTask.project_id',__('Proyecto'), array('class'=>'control-label'));?>
                      <?php echo $this->Form->input('project_id', array('class' => 'form-control border-input select','label'=>false,'div'=>false,"empty"=>__("Sin proyecto"),"style"=>"")); ?>
                  </div>
              </div>
          </div>
     
          <div class="row">
              <div class="col-md-12 pull-right">
                  <button id="btnEnviar" name="btnEnviar" type='submit' class='btn btn-success pull-right'><?php echo __("Guardar Actividad") ?></button>
              </div>
          </div>
          <?php echo $this->Form->end(); ?>
          <hr>
          <p class="respuesta" style="margin-top: 20px;">
          </div>
                                        
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="widget-body no-padding" id="ListarActividades" style="margin-top: 20px"></div>
          </div>                             
      </div> 
    </div>
  </div>
</div>

<div id="modalEdit" >
    <div class="modal fade" id="ModalLoad"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
</div>

<?php
    
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
    echo $this->Html->css('frontend/admin/components/table.css'); 
     echo $this->Html->script('slates.js', ['block' => 'AppScript']);
     
?>
<?php
 echo $this->Html->script('materialDateTimePicker.js', ['block' => 'AppScript']);
?>
 <script type="text/javascript">
    $( document ).ready(function() {
      
      $("#ListarActividades").on("click", ".eliminarActividad", function(){
      var idDELETE = $(this).data("id")
      Swal.fire({
      type: 'question',
      icon: 'warning',
      title:"¿Desea eliminar la actividad?",
      text: "* No podrá recuperarla y se enviará un email de notificación al responsable.",
      customClass: 'swal2-overflow',
      showCancelButton: true,
      confirmButtonColor: '#60c400',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
    }).then(function(result) {
      if (result.isConfirmed) {
        $.post( "../eliminar_actividad", {id:idDELETE})
          .done(function( data ) {
            if(data == 1){
              Swal.fire(
                '¡Bien!',
                'Actividad eliminada correctamente.',
                'success'
              )
                refresh1();
            }else{
              Swal.fire(
                '¡Bien!',
                'Ha ocurrido un error, intente nuevamente.',
                'error'
              )
            }
          });
      }

    })

    });

      $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });

        $("#PlaneensTaskAdminForm").bind("submit",function(){
        var btnEnviar = $("#btnEnviar");
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:$(this).serialize(),
            beforeSend: function(){
                btnEnviar.val("Enviando"); // Para input de tipo button
                btnEnviar.attr("disabled","disabled");
            },
            complete:function(data){
                btnEnviar.val("Enviar formulario");
                btnEnviar.removeAttr("disabled");
            },
            success: function(data){
                
                if(data == '1'){
                    refresh1();
                    $("#PlaneensTaskAdminForm")[0].reset();
                   $(".respuesta").html('<div class="alert alert-success" role="alert">Tarea añadida correctamente.</div>');
                 }else{
                  $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
                 }
            },
            error: function(data){
               $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
            }
        });
        return false;
    });
           

        $.ajax({
            type: "POST",
            url: "../ListarActividades",
            data: {id: "<?php echo $id ?>"},
            beforeSend: function(){
            },
            complete:function(data){
            },
            success: function(data){
                $("#ListarActividades").html(data);
            },
            error: function(data){
                alert("Problemas al tratar de enviar el formulario");
            }
        });


       

    
         
    });

    function refresh1(){
      $.ajax({
            type: "POST",
            url: "../ListarActividades",
            data: {id: "<?php echo $id ?>"},
            beforeSend: function(){
            },
            complete:function(data){
            },
            success: function(data){
                $("#ListarActividades").html(data);
            },
            error: function(data){
                alert("Problemas al tratar de enviar el formulario");
            }
        });
    }
    
</script>
<script type="text/javascript">
    $(document).ready(function(){
  $('#buscador').keyup(function(){
     var nombres = $('.nombres');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.item').show(); 
              }else{
                   $(nombres[i]).parents('.item').hide();
              }
          }
     }
  });
  $('.select').select2({
       
       
      
    });

});
 $("#ListarActividades").on("click", ".editTask", function(){
  var id = $(this).data("id");
  $.ajax({
        type: "GET",
        url: "../edit/"+id+"/<?php echo $id ?>",
        
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        success: function (response) {
            $('#ModalLoad').html(response); //add the partial view into the modal content.
            $('#ModalLoad').modal('show'); //display the modal popup.
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });

});

</script>

<style type="text/css">
  .loadContent{
    margin-top: 40px;
  }
  .daterangepicker{
    z-index: 1000000 !important;
  }
</style>

<style>
 #select2-SlatesTaskProjectId-container{
            border: solid 0.5px darkgrey;border-radius: 4px;
            }
</style>
<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
.widget-body{
    overflow: initial !important;
}
.select2-container{
  border: 1px solid #eee !important;
  border-radius: 6px  !important;
}
</style> 