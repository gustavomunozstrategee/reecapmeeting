 <div class="modal-dialog modal-recapmeeting" role="document">
 <div class="modal-content">
    <?php echo $this->Form->create('PlaneensTask', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40','url'=>'/planeens/edit_actividad_modal')); ?>


    <?php echo $this->Form->input('id', array('class'=>'control-label','value'=>$id));?>


                                <div class="modal-header">
                                    <h4 class="modal-title"><?php echo __("Editar Actividad") ?> </h4>
                                </div>
                                <div class="modal-body"> 
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class='form-group'>
                                                  <?php echo $this->Form->input('idPlaneen', array('class'=>'control-label','value'=>$idPlaneen,"style"=>"display:none","label"=>false));?>
                                                   
                                            <?php echo $this->Form->label('description',__('Descripción de la actividad'), array('class'=>'control-label'));?>
                                            <?php echo $this->Form->input('description', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2")); ?>
                                            
                                             
                                            </div>
                                            </div>
                                             
                                            
                                        </div>
                         
                                        <div class="row">
                                          <div class="col-md-12">
                                                <div class="form-group">
                                                         <?php echo $this->Form->label('Meeting.start_date',__('Fecha de Terminación'), array('class'=>'control-label f-blue'));?>
                                                        <div class='input-group date ' id=''>
                                                          
                                                            <?php echo $this->Form->input('deadline_date', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class='form-group'>
                                                    <?php echo $this->Form->label('SlatesTask.user_id',__('Usuario responsable'), array('class'=>'control-label select2'));?>
                                                    <?php echo $this->Form->input('user_id', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class='form-group'>
                                                    <?php echo $this->Form->label('SlatesTask.tags',__('Módulo / Bloque de Trabajo'), array('class'=>'control-label select2'));?>
                                                    <?php echo $this->Form->input('tags', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class='form-group'>
                                                    <?php echo $this->Form->label('SlatesTask.project_id',__('Proyecto'), array('class'=>'control-label'));?>
                                                    <?php echo $this->Form->input('project_id', array('class' => 'form-control border-input select','label'=>false,'div'=>false,"empty"=>__("Sin proyecto"),"style"=>"")); ?>
                                                </div>
                                            </div>
                                        </div>
                                    
                                </div>
                                <div class="modal-footer">
                                     <a class="btn btn-danger " id="closed" data-dismiss="modal" aria-hidden="true">Cancelar</a>
                                    <button type="submit" class="btn btn-success btnArchivos" id="btnAutoGuardado">Continuar</button>
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>



</div>
<script type="text/javascript">
    $( document ).ready(function() {

      $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
        });

         $("#PlaneensTaskEditForm").bind("submit",function(){
        var btnEnviar = $("#btnAutoGuardado");
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:$(this).serialize(),
            beforeSend: function(){
                btnEnviar.val("Enviando"); // Para input de tipo button
                btnEnviar.attr("disabled","disabled");
            },
            complete:function(data){
                btnEnviar.val("Enviar formulario");
                btnEnviar.removeAttr("disabled");
            },
            success: function(data){
                
                if(data == '1'){
                    $("#closed").click()
                        Swal.fire(
                        '¡Bien!',
                        'Actividad Actualizada correctamente.',
                        'success'
                      )
                        $.ajax({
            type: "POST",
            url: "../ListarActividades",
            data: {id: "<?php echo $idPlaneen ?>"},
            beforeSend: function(){
            },
            complete:function(data){
            },
            success: function(data){
                $("#ListarActividades").html(data);
            },
            error: function(data){
                alert("Problemas al tratar de enviar el formulario");
            }
        });
                 }else{
                  $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
                 }
            },
            error: function(data){
               $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
            }
        });
        return false;
    });


    });

    $(function () {
    var frm = $('#PlaneensTaskEditModalForm');

    frm.submit(function (ev) {
         $("#btnAutoGuardado").prop('disabled', true);
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                 $(".<?php echo $id ?>").html(data)
                 $('#ModalLoad').modal('hide')
                Swal.fire({
                  type: 'ok',
                  icon: 'success',
                  title:"Actualización Realizada",
                  
                  customClass: 'swal2-overflow',
                
                   
                  confirmButtonText: 'Aceptar',
                })
            }
        });
        ev.preventDefault();
    });
});


</script>

<style>
 #select2-SlatesTaskProjectId-container{
            border: solid 0.5px darkgrey;border-radius: 4px;
            }
</style>
<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
.widget-body{
    overflow: initial !important;
}
.select2-container{
  border: 1px solid #eee !important;
  border-radius: 6px  !important;
}
</style>
<style type="text/css">
  .loadContent{
    margin-top: 40px;
  }
  .daterangepicker{
    z-index: 1000000 !important;
  }
  .btn-danger {
    color: #fff !important;
  }
</style>




