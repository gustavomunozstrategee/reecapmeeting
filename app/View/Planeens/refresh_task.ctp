<div class="board-item-content" style="opacity: 1; transform: scale(1);">
  <div class="row">
    <div class="col-md-8 ">
         <a class="categoria">
             <t class="text-primary sizeNew" style="text-transform: uppercase;"> <i class="fa fa-accusoft"></i> [<?php echo $Task["PlaneensTask"]["tags"] ?>] <?php echo $Task["Project"]["name"] ?></t>
          </a>
    </div>
    <div class="col-md-4 float-right">
          <a class="prioridad" href="">
             <span class="badge badge-pill badge-danger sizeNew"> <i class="fa fa-calendar"></i> <?php echo $Task["PlaneensTask"]["deadline_date"] ?> </span>
          </a>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1">
       <img class="user-img rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$Task["User"]["img"]); ?>" width="32px" height="32px" title="<?php echo $Task["User"]["firstname"] ?>">
    </div>
    <div class="col-md-10">
      <spa class="sizeNew2"><?php echo $Task["PlaneensTask"]["description"] ?>
    </spa></div>
  </div>
</div>