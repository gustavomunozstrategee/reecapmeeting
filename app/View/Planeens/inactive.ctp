<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css
" crossorigin="anonymous" />


 <div class="container-fluid">
    <!-- Begin Page Header-->
    
    <!-- End Page Header -->
     

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="widget widget-18 has-shadow">
                                <?php echo $this->element('Planeen/left_nav'); ?>
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header bordered d-flex align-items-center">
                                        <h2><?php echo __("Mis Planeens No Prioritarios") ?></h2>
                                        <div class="media-right align-self-center">
                                                    <div class="actions">
                                                        <a href="<?php echo $this->webroot ?>planeens/add"><i class="la la-plus reply"></i></a>
                                                    </div>
                                                </div>
                                    </div>
                                    <!-- End Widget Header -->
                                    <div class="widget-body">
                                        <div class="input-group">
                                            <span class="input-group-addon pr-0 pl-0">
                                                <a class="btn" href="#">
                                                    <i class="la la-search la-2x"></i>
                                                </a>
                                            </span>
                                            <input id="buscador" type="text" class="form-control no-ppading-right no-padding-left" placeholder="Buscar Planeación...">
                                        </div>
                                         
                                        <ul class="list-group w-100">
                                          <?php if(!empty($planeens)): ?>
                                            <?php foreach ($planeens as $key => $value): ?>
                                                <li class="list-group-item item" id="plaenHTML_<?php echo EncryptDecrypt::encrypt($value["Planeen"]["id"]) ?>">
                                                  <div class="other-message">
                                                      <div class="media ">
                                                          <div class="media-left align-self-center mr-3">
                                                             <i class="la la-list-alt la-2x align-middle pr-2"></i>
                                                          </div>
                                                          <div class="media-body align-self-center">
                                                              <div class="other-message-sender nombres"><?php echo $value["Planeen"]["name"] ?></div>
                                                              <div class="other-message-time ">
                                                                <i class="la la-users "></i>
                                                                <?php foreach ($value["Planeen"]["PlaneensUser"] as $key2 => $value2): ?>

                                                                  <?php 
                                                                    if($key2 > 0){
                                                                      echo " - ";
                                                                    }
                                                                    echo $value2["User"]["firstname"]. " ".$value2["User"]["lastname"];
                                                                    if($value2["role_id"] == 0){
                                                                      echo " (Admin)";
                                                                    }elseif($value2["role_id"] == 1){
                                                                      echo " (Colaborador)";
                                                                    }elseif($value2["role_id"] == 2){
                                                                      echo " (Editor)";
                                                                    }
                                                                  ?> 
                                                                <?php endforeach ?>
                                                             
                                                              </div>
                                                          </div>
                                                          <div class="media-right align-self-center">
                                                              <div class="actions">
                                                                <a  class="changeState" style="cursor: pointer;" data-id="<?php echo EncryptDecrypt::encrypt($value["Planeen"]["id"]) ?>"><i class="mdi mdi-pin"></i></a>
                                                                <a href="<?php echo $this->webroot ?>planeens/view/<?php echo EncryptDecrypt::encrypt($value["Planeen"]["id"]) ?>"><i class="la la-eye reply"></i></a>

                                                              </div>

                                                              

                                                          </div>

                                                      </div>
                                                      <?php
                                                      $porcentajeAvance = 0;
                                                            if($value["Actividades"]["Totales"] > 0){
                                                              $porcentajeAvance = ($value["Actividades"]["Terminadas"] / $value["Actividades"]["Totales"])*100;

                                                            }else{
                                                              $porcentajeAvance =  0;
                                                            }


                                                            ?>
                                                      
                                                      <div class="progress" style="background-color: darkred">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $porcentajeAvance ?>%">
                                                             Mis Actividades:  
                                                             <?php echo number_format($porcentajeAvance,2) ?>% Completado ( <?php echo $value["Actividades"]["Terminadas"] ?> / <?php  echo $value["Actividades"]["Totales"] ?> Actividades )
                                                        </div>
                                                      </div>
                                                      <br>
                                                      <?php
                                                        $porcentajeAvance2 = 0;
                                                        if($value["Actividades"]["TotalesPlaneen"] > 0){
                                                          $porcentajeAvance2 = ($value["Actividades"]["totalesTerminadas"] / $value["Actividades"]["TotalesPlaneen"])*100;

                                                        }else{
                                                          $porcentajeAvance2 =  0;
                                                        }


                                                      ?>
                                                      <div class="progress" style="background-color: darkred">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $porcentajeAvance2 ?>%">
                                                              Todas las Actividades:  
                                                             <?php echo number_format($porcentajeAvance2,2) ?>% Completado ( <?php echo $value["Actividades"]["totalesTerminadas"] ?> / <?php  echo $value["Actividades"]["TotalesPlaneen"] ?> Actividades )
                                                        </div>
                                                      </div>
                                                  </div>
                                                </li>
                                            <?php endforeach;?>

                                          <?php else: ?>
                                            <div class="alert alert-outline-success" role="alert">
                                              <strong></strong> <?php echo __("Aun no tienes  listas de tareas") ?>
                                          </div>
                                          <?php endif; ?>
                                            
                                             
                                        </ul>
                                    </div>
                                </div>
                            </div>
</div>

<?php
  
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);

  
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
  
   
?>

<script type="text/javascript">
    $(document).ready(function(){
  $('#buscador').keyup(function(){
     var nombres = $('.nombres');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.item').show(); 
              }else{
                   $(nombres[i]).parents('.item').hide();
              }
          }
     }
  });
});
    
      $( ".changeState" ).click(function() {
      var idDELETE = $(this).data("id")
      Swal.fire({
      type: 'question',
      icon: 'warning',
      title:"¿Desea enviar el Planeen a Prioritario?",
      text: "* Podrá cambiar la prioridad en cualquier momento.",
      customClass: 'swal2-overflow',
      showCancelButton: true,
      confirmButtonColor: '#60c400',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
    }).then(function(result) {
      if (result.isConfirmed) {
        $.post( "./activar_planeen", {id:idDELETE})
          .done(function( data ) {
            if(data == 1){
              Swal.fire(
                '¡Bien!',
                'Planeen marcado como Prioritario.',
                'success'
              )
                 $("#plaenHTML_"+idDELETE).remove()
            }else{
              Swal.fire(
                '¡Bien!',
                'Ha ocurrido un error, intente nuevamente.',
                'error'
              )
            }
          });
      }
    })

    });



</script>
 
 

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
 
.progress-bar-success{
  background-color: darkgreen !important;
}

</style>
 