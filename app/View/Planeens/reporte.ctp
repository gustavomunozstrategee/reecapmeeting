<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
 <div class="container-fluid">
    <!-- Begin Page Header-->
    <!-- End Page Header -->
    
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                                <div class="widget widget-18 has-shadow">
                                <?php echo $this->element('./Planeen/left_nav'); ?>
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header bordered d-flex align-items-center">
                                        <h2><?php echo __("Generar Reporte") ?></h2>
                                        <div class="media-right align-self-center">
                                        </div>
                                    </div>
                                    <!-- End Widget Header -->
                                    <div class="widget-body">

                                      <div class="tab-pane active show" id="tab1">
                                          <?php echo $this->Form->create('Report', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40')); ?>                   
                                                            
                                                            <div class="form-group row mb-3">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label"><?php echo __("Usuarios") ?><span class="text-danger ml-2"></span></label>

                                                                    <?php echo $this->Form->input('participantes', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple','value' => @$this->request->data["Contac"]["funcionarios"], 'options' => $collaborators)); ?>  
                                                                </div>
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label"><?php echo __("Planeens") ?><span class="text-danger ml-2"></span></label>
                                                                    <?php echo $this->Form->input('planeens', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple', 'options' => $planeens)); ?>  
                                                                </div>

                                                                <div class="col-md-6">
                                                                  <div class="form-group">
                                                                           <?php echo $this->Form->label('Meeting.start_date',__('Rango de fecha Inicial'), array('class'=>'control-label f-blue'));?>
                                                                          <div class='input-group date ' id=''>
                                                                            
                                                                              <?php echo $this->Form->input('inicio', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                                                              <span class="input-group-addon">
                                                                                  <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                                                              </span>
                                                                          </div>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-6">
                                                                  <div class="form-group">
                                                                           <?php echo $this->Form->label('Meeting.start_date',__('Rango de fecha Final'), array('class'=>'control-label f-blue'));?>
                                                                          <div class='input-group date ' id=''>
                                                                            
                                                                              <?php echo $this->Form->input('fin', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                                                              <span class="input-group-addon">
                                                                                  <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                                                              </span>
                                                                          </div>
                                                                  </div>
                                                              </div>
                                                                                   
                                                                 
                                                                
                                                            </div>
                                                            <ul class="pager wizard text-right">
                                                                 
                                                                <li class="next d-inline-block">
                                                                  <button class="btn btn-success"><?php echo __("Generar Reporte") ?></button>
                                                                   
                                                                </li>
                                                            </ul>
                                            <?php echo $this->Form->end(); ?>
                                        </div>
                                        
                                         
                                        
                                    </div>
                                </div>
                            </div>
</div>

<?php
    
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
    echo $this->Html->css('frontend/admin/components/table.css'); 
     echo $this->Html->script('slates.js', ['block' => 'AppScript']);
     
?>
<?php
 echo $this->Html->script('materialDateTimePicker.js', ['block' => 'AppScript']);
?>
<script type="text/javascript">
    $(document).ready(function(){
  $('#buscador').keyup(function(){
     var nombres = $('.nombres');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.item').show(); 
              }else{
                   $(nombres[i]).parents('.item').hide();
              }
          }
     }
  });
  $('.select2_all').select2({
    });
  $(".select2N").select2({
      tags: true,
      tokenSeparators: ['{}', ' ']
  })
  $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });
});

</script>

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
</style>
 