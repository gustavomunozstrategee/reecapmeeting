<!-- Begin Page Header-->
<?php  $permission = $this->Utilities->check_team_permission_action(array("index","add"), array("teams")); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Empresas") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Editar empresa") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                
                <h2>
                    <?php echo __('Editar empresa'); ?>
                </h2>
                    <div class="widget-options">
                        <div class="" role="">
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><button type="button" class="btn btn-secondary "><i class="la la-list"></i><?php echo __("Listar");?></button></a>
                        </div>
                    </div>
            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('Team', array('role' => 'form','data-parsley-validate','novalidate' => true,'type'=>'file')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-warinig"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label required f-blue font-bold'));?></p>
                            <div class='form-group'>
                                <?php echo $this->Form->input('id'); ?>
                                <?php echo $this->Form->label('Team.name',__('Nombre'), array('class'=>'control-label f-blue required'));?>
                                <?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'div'=>false, "maxlength" => 50)); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo $this->Form->label('Team.description',__('Descripción'), array('class'=>'control-label f-blue'));?>
                                <?php echo $this->Form->input('description', array('class' => 'form-control border-input textarea resize-none','label'=>false,'div'=>false)); ?>
                            </div>
                            <div id="showCharacter" class="form-group"></div> 
                             <div class='form-group'>
                                <?php echo $this->Form->label('Team.contact_send_url',__('API URL para envío de la información del Acta (Opcional)'), array('class'=>'control-label f-blue'));?>
                                <a href="javascript:void(0)" class="modal-documentacion">[<?php echo __("Documentación") ?>]</a>
                                <?php echo $this->Form->input('contact_send_url', array('class' => 'form-control border-input resize-none','label'=>false,'div'=>false)); ?>
                            </div>
                            

                            <div class="form-group">            
                                <?php if(!empty($plan)):?>
                                    <?php if($plan["Plan"]["white_brand"] == configure::read("ENABLED")):?>
                                        <?php echo $this->Form->label('Team.img',__('Marca blanca de la empresa'), array('class'=>'control-label f-blue'));?>
                                        <div class="img-user-upload img-user-upload--mod">
                                            <label for="TeamImg" class="btn btn-info"><?php echo __("Seleccionar imagen")?></label>
                                            <?php echo $this->Form->input('img', array('type'=>'file','class' => 'form-control border-input imageFile', 'label'=>false,'div'=>false,'required' => false, 'style' => 'visibility:hidden;')); ?>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>

                            <div class="flex-justify-end">            
                                <button type='submit' class='btn btn-primary pull-right'><?php echo __('Guardar') ?></button>
                            </div>
                        </div>
                    </div>
                    
                <?php echo $this->Form->end(); ?>

            </div>
            <div class="widget-footer">
               
            </div>
        </div>
    </div>
</div>


<!-- ************************************* -->


<div id="myModal" class="modal fade modalDocumentacion" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title f-blue"><?php echo __("Documentación") ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body f-blue">
        <p class="f-blue"><?php echo __("Debes indicar una URL expuesta hacia donde quieres enviar la información generada cada vez que se crea un Acta.El sistema ReecapMeeting automáticamente enviará la información a la URL definida la cual contendrá la siguiente estructura:") ?></p>
        <a target="_blank" href="<?php echo $this->webroot  ?>img/api_example.png"><img src="<?php echo $this->webroot  ?>img/api_example.png" width="100%"></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Cerrar") ?></button>
      </div>
    </div>

  </div>
</div>