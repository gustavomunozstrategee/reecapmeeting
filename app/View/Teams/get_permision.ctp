<?php $permissionTags     = $this->Utilities->check_team_permission_action(array("index"), array("tags"),$teamId); ?>
<?php $permissionLogs     = $this->Utilities->check_team_permission_action(array("index"), array("logs"),$teamId); ?>
<?php $permissionPages    = $this->Utilities->check_team_permission_action(array("filter"), array("pages"),$teamId); ?>
<?php $permissionClients  = $this->Utilities->check_team_permission_action(array("index"), array("clients"),$teamId); ?>
<?php $permissionProjects = $this->Utilities->check_team_permission_action(array("index"), array("projects"),$teamId); ?>
<?php 
$permissionUsers    = $this->Utilities->check_team_permission_action(array("index"), array("user_teams"),$teamId); 
    // var_dump($permissionUsers);
?>

<div class="row">
	<?php if ($teamData == AuthComponent::user("id")): ?>
		<div class="col-md-4 widget-18">
			<div class="new-message" data-url="<?php echo $this->Html->url(array('controller'=>'positions','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>">
				<div class="media">
					<div class="media-left align-self-center mr-3">
						<i class="la la-user-secret fa-4x text-grey-light"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="new-message-sender"><a class="text-white" href="<?php echo $this->Html->url(array('controller'=>'positions','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><?php echo __('Roles') ?></a></div>
					</div>
					<div class="media-right align-self-center">
						<div class="actions">
							<a href="<?php echo $this->Html->url(array('controller'=>'positions','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><i class="la la-gear reply text-grey-light"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
	<?php if (isset($permissionUsers["index"])): ?>
		<div class="col-md-4 widget-18">
			<div class="new-message" data-url="<?php echo $this->Html->url(array('controller'=>'user_teams','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>">
				<div class="media">
					<div class="media-left align-self-center mr-3">
						<i class="la la-users fa-4x text-grey-light"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="new-message-sender"><a class="text-white" href="<?php echo $this->Html->url(array('controller'=>'user_teams','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><?php echo __('Usuarios') ?></a></div>
					</div>
					<div class="media-right align-self-center">
						<div class="actions">
							<a href="<?php echo $this->Html->url(array('controller'=>'user_teams','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><i class="la la-gear reply text-grey-light"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif;?>
	<?php if (isset($permissionClients["index"])): ?>
		<div class="col-md-4 widget-18">
			<div class="new-message" data-url="<?php echo $this->Html->url(array('controller'=>'clients','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>">
				<div class="media">
					<div class="media-left align-self-center mr-3">
						<i class="la la-weixin fa-4x text-grey-light"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="new-message-sender"><a class="text-white" href="<?php echo $this->Html->url(array('controller'=>'clients','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><?php echo __('Clientes') ?></a></div>
					</div>
					<div class="media-right align-self-center">
						<div class="actions">
							<a href="<?php echo $this->Html->url(array('controller'=>'clients','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><i class="la la-gear reply text-grey-light"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?> 
	<?php if (isset($permissionProjects["index"])): ?>
		<div class="col-md-4 widget-18">
			<div class="new-message" data-url="<?php echo $this->Html->url(array('controller'=>'projects','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>">
				<div class="media">
					<div class="media-left align-self-center mr-3">
						<i class="la la-folder-open fa-4x text-grey-light"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="new-message-sender"><a class="text-white" href="<?php echo $this->Html->url(array('controller'=>'projects','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><?php echo __('Proyectos') ?></a></div>
					</div>
					<div class="media-right align-self-center">
						<div class="actions">
							<a href="<?php echo $this->Html->url(array('controller'=>'projects','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><i class="la la-gear reply text-grey-light"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?> 
	<?php if (isset($permissionTags["index"])): ?>
		<div class="col-md-4 widget-18">
			<div class="new-message" data-url="<?php echo $this->Html->url(array('controller'=>'tags','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>">
				<div class="media">
					<div class="media-left align-self-center mr-3">
						<i class="la la-tags fa-4x text-grey-light"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="new-message-sender"><a class="text-white" href="<?php echo $this->Html->url(array('controller'=>'tags','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><?php echo __('Tags') ?></a></div>
					</div>
					<div class="media-right align-self-center">
						<div class="actions">
							<a href="<?php echo $this->Html->url(array('controller'=>'tags','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><i class="la la-gear reply text-grey-light"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?> 
<!-- 	<?php if (isset($permissionLogs["index"])): ?>
		<div class="col-md-4 widget-18">
			<div class="new-message">
				<div class="media">
					<div class="media-left align-self-center mr-3">
						<i class="la la-align-justify fa-4x text-grey-light"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="new-message-sender"><?php echo __('Logs') ?></div>
					</div>
					<div class="media-right align-self-center">
						<div class="actions">
							<a href="<?php echo $this->Html->url(array('controller'=>'logs','action'=>'index',"?" => array("team_selection" => EncryptDecrypt::encrypt($teamId)))) ?>"><i class="la la-gear reply text-grey-light"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?> -->
<!-- 	<?php if (isset($permissionPages["filter"])): ?>
		<div class="col-md-4 widget-18">
			<div class="new-message">
				<div class="media">
					<div class="media-left align-self-center mr-3">
						<i class="la la-search-plus fa-4x text-grey-light"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="new-message-sender"><?php echo __('Búsqueda avanzada') ?></div>
					</div>
					<div class="media-right align-self-center">
						<div class="actions">
							<a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'filter')) ?>"><i class="la la-gear reply text-grey-light"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>   
	<?php endif; ?> -->
</div>

<style>
	.new-message{
		cursor: pointer;
	}
</style>