 

<?php if (!empty($planActual)): ?>
    <?php if (!empty($img)): ?>
        <?php if($action == "view"):?>       
            <div class="form-group row d-flex align-items-center">
                <label class="col-lg-4 form-control-label">
                    <?php echo __('Logo'); ?>
                </label>
                <div class="col-lg-8">
                    <a href="<?php echo $this->Html->url('/document/WhiteBrand/'.$img); ?>" class="showImages">                    
                        <img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$img); ?>" width="100px" class="img-responsive logo-add hidden-xs" alt=""/>
                    </a>
                </div>
                <div class="em-separator separator-dashed w-100"></div>
            </div>
<!--             <td><label class="f-blue"><?php echo __('Logo'); ?></label></td>
            <td colspan="2">
                <a href="<?php echo $this->Html->url('/document/WhiteBrand/'.$img); ?>" class="showImages">
                    <img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$img); ?>" width="100px" class="img-responsive logo-add" alt=""/>
                </a>
            </td> -->
        <?php else: ?>
            <div class="form-group row d-flex align-items-center mb-5">
                <label class="col-lg-3 form-control-label"><?php echo __('Logo'); ?></label>
                <div class="col-lg-9">
                    <a href="<?php echo $this->Html->url('/document/WhiteBrand/'.$img); ?>" class="showImages">                        
                        <img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$img); ?>" width="100px" class="img-responsive logo-add" alt=""/>
                    </a>
                    <?php echo $this->Form->input('Contac.file_white_brand', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $img, 'hidden' => true)); ?>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <?php echo $this->Form->input('Contac.not_file_white_brand', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => 2, 'hidden' => true)); ?>
    <?php endif; ?>
<?php else: ?> 
    <?php echo $this->Form->input('Contac.not_file_white_brand', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => 2, 'hidden' => true)); ?>
<?php endif; ?>

