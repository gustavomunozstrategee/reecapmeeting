
<?php $this->Html->css('frontend/admin/pages/team.css', ['block' => 'styles_section']); ?>
<div class="backg-white">
    <div class="flex-space-between-start">
        <div>
            <h1 class="f-title">
                <?php echo __('Empresas'); ?>
            </h1>
            <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar empresa");?>" href="<?php echo $this->Html->url(array('controller'=>'teams','action' => 'add')); ?>">
                <i class="flaticon-add"> </i>
            </a>  
        </div>
        <div>
            <div class="text-right f-content-search">   
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inline', 'type'=>'GET', 'role'=>'form'));?>
                <div class="input-group">
                    <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre, descripción'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                    <div class="input-group-btn">
                      <button type="submit" class="btn"><span class="glyphicon glyphicon-search f-blue"></span></button>
                  </div>
              </div>
                      <?php if(empty($teams) && !empty($this->request->query['q'])) : ?>
                      <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
                      <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'),array('class'=>'f-link-search')); ?>
                  <?php endif ?>
                  <?php echo $this->Form->end(); ?>
              </div>
        </div>
    </div>

    <section class="section-table">
        <div class="flex-start">
            <?php if(!empty($teams)):?>
                <?php foreach ($teams as $team): ?>
                    <div class="card-body-team">
                        <div class="card-team"> 
                            <?php if($team["Team"]["img"] == NULL):?>
                                <div class="card-team-bgactions flex-center">
                                    <i class="flaticon-users"></i>
                                    <p><?php echo $this->Text->truncate(h($team['Team']['name']),40); ?></p>
                                </div>
                            <?php else: ?>
                                <div class="card-team-bgactions">
                                    <div class="card-img-team">
                                       <div class="thumbnail">
                                         <img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$team['Team']['img']); ?>" class="" alt="Logo Empresa"/>
                                       </div>
                                    </div>
                                    <p><?php echo $this->Text->truncate(h($team['Team']['name']),40); ?></p>
                                </div>
                            <?php endif; ?>
                            <div class="card-team-info">
                               <div class="card-height-info">
                                 <p><?php echo $this->Text->truncate(h(!empty($team['Team']['description']) ?  $team['Team']['description'] : __("Sin descripción disponible.")),250);?></p>
                               </div>
                               <p class="f-blue"><?php echo h($team['User']['firstname'] . ' ' . $team['User']['lastname']); ?></p>
                           </div>
                       </div>
                       
                       <div class="card-team-actions">
                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($team['Team']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                            <i class="fa fa-eye"></i>
                        </a> 
                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'edit', EncryptDecrypt::encrypt($team['Team']['id']))); ?>" title="<?php echo __('Editar'); ?>" class="btn-xs" data-toggle="tooltip">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div>
                <p class="text-center"><?php echo __('No existen empresas.')?></p>
            </div>
        <?php endif; ?>
    </div>

    <!--pagination-->
    <div class="table-pagination">
       <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
        <div>
            <ul class="pagination f-paginationrecapmeeting">
                <?php
                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
    <!--End pagination-->  
    </section>
</div>




<?php
$this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']); 
$this->Html->scriptBlock("$('#SearchClientId').select2();", ['block' => 'AppScript']);
?>

