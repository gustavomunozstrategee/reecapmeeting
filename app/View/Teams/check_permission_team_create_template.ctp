<?php if(empty($planActual)):?>
    <div>
        <p class="text-notificacion"><?php echo __("Selecciona una empresa con permisos para crear plantillas.") ?></p>
    </div> 
<?php else: ?>
    <?php if (!empty($planActual)): ?>
        <?php if ($planActual["Plan"]["template"] == Configure::read('ENABLED')): ?>
            <div>
                <a class="btn btn-success" href="#" data-toggle='modal' id="btn_modalAddPlantilla"><?php echo __('Crear plantilla') ?>
                    <i class="la la-plus-circle" aria-hidden="true"></i>
                </a>
            </div>
            <div>
                 
            </div>
        <?php else: ?>
            <div>
                <p class="text-notificacion"><?php echo __("La empresa seleccionada no cuenta con permisos para crear plantillas.") ?></p>
            </div>
        <?php endif; ?>
        <?php else :?>
            <div>
                 
            </div>
    <?php endif; ?>
<?php endif; ?>