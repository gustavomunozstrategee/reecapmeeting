
<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Empresas") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Visualización de la empresa") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2>
                    <?php echo __('Visualización de la empresa'); ?>
                </h2>
                <div class="widget-options">
                    <div class="" role="">
 
                                
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                                <button type="button" class="btn btn-secondary ">
                                    <i class="la la-list"></i>
                                    <?php echo __('Listar'); ?>
                                </button>
                            </a>

                             <a href="<?php echo $this->Html->url(array('action'=>'edit', EncryptDecrypt::encrypt($team['Team']['id'])));?>">
                                <button type="button" class="btn btn-secondary "><i class="la la-pencil"></i><?php echo __('Editar'); ?></button></a>
                            </a> 

                            <a href="<?php echo $this->Html->url(array('action'=>'add'));?>">
                                 <button type="button" class="btn btn-secondary "><i class="la la-plus"></i><?php echo __('Adicionar'); ?></button></a>
                            </a>
                    </div>
                </div>
            </div>
            <div class="widget-body">
                <div class="row">
                  <?php if (!empty($team["Team"]["img"])): ?>
                     <div class="col-md-3 col-lg-3 col-sm-4">
                        <input type="hidden" value="<?php echo EncryptDecrypt::encrypt($team['Team']['id']) ?>" id="client-id">
                         <a href="<?php echo $this->Html->url('/document/WhiteBrand/'.$team['Team']['img']); ?>" class="showImagesPopup"> 
                            <div class="img-thumbnail">   
                                <img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$team['Team']['img']); ?>" class="img-fluid" alt="Logo" />
                            </div>
                        </a>
                    </div>
                    <div class="col-md-9 col-lg-9 col-sm-8">
                  <?php else: ?>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                  <?php endif ?>
                      <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-4 form-control-label">
                                <?php echo __('Nombre'); ?>
                            </label>
                            <div class="col-lg-8">
                                 <div class="div-word-wrap">
                                    <?php echo h($team['Team']['name']); ?>
                                </div>
                            </div>
                      </div>
                       <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-4 form-control-label">
                                <?php echo __('Descripción'); ?>
                            </label>
                            <div class="col-lg-8">
                                 <div class="div-word-wrap">
                                    <?php echo h(!empty($team['Team']['description']) ?  $team['Team']['description'] : __("Sin descripción disponible.")); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-4 form-control-label">
                                <?php echo __('API URL para envío de la información del Acta (Opcional)'); ?> <a href="javascript:void(0)" class="modal-documentacion">[<?php echo __("Documentación") ?>]</a>
                            </label>
                            <div class="col-lg-8">
                                 <div class="div-word-wrap">
                                    <?php echo h($team['Team']['contact_send_url']); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-4 form-control-label">
                                <?php echo __('Usuario creador'); ?>
                            </label>
                            <div class="col-lg-8">
                                 <div class="div-word-wrap">
                                    <?php echo h($team['User']['firstname'] . ' ' . $team['User']['lastname']); ?>
                                </div>
                            </div>
                        </div>
                      

                    </div>

                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-primary text-center"><?php echo __("Miembros"); ?> </h3>
                    </div>
                    <div class="col-md-12">
                      <div class="table-responsive">
                          <table class="table mb-0">
                              <tbody> 
                                <?php if(!empty($members)):?>
                                  <?php foreach ($members as $member): ?>
                                    <tr>
                                      <td><?php echo h($member['User']['firstname'] . ' ' . $member['User']['lastname'] .  ' - ' . $member['User']['email']); ?> - <?php echo __(Configure::read("TYPE_USER.{$member["UserTeam"]["type_user"]}")) ?> </td>
                                    </tr>
                                  <?php endforeach;?>
                                <?php else: ?>
                                  <tr>
                                      <td class="text-center" colspan="1">
                                        <?php echo __("No existen miembros en esta empresa."); ?>
                                      </td>
                                  </tr>
                                <?php endif; ?>
                              </tbody>
                          </table>
                      </div>
                      <!--Pagination-->
                      <div class="table-pagination mt-4">
                          <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                          <div>
                              <ul class="pagination f-paginationrecapmeeting">
                                  <?php
                                  echo $this->Paginator->prev('< ' /*. __('')*/, array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                  echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                  echo $this->Paginator->next(/*__('') . */' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                  ?>
                              </ul>
                          </div>
                      </div>
                      <!--End pagination-->
                  </div>
                </div>
          </div>
      </div>
    </div>
</div>

 <div id="myModal" class="modal fade modalDocumentacion" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title f-blue"><?php echo __("Documentación") ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body f-blue">
        <p class="f-blue"><?php echo __("Debes indicar una URL expuesta hacia donde quieres enviar la información generada cada vez que se crea un Acta.El sistema ReecapMeeting automáticamente enviará la información a la URL definida la cual contendrá la siguiente estructura:") ?></p>
        <a target="_blank" href="<?php echo $this->webroot  ?>img/api_example.png"><img src="<?php echo $this->webroot  ?>img/api_example.png" width="100%"></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Cerrar") ?></button>
      </div>
    </div>

  </div>
</div>