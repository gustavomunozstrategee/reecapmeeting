<nav class="nav-default">
    <ul>
        <li>
            <a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'index_page')) ?>">
                <?php echo $this->Html->image("frontend/admin/logo-blue.svg",array('class' => 'logo', 'alt' => 'Logo Recapmeeting', 'title' => 'Recapmeeting'));?>
            </a>
        </li>
    </ul>
    <?php if(AuthComponent::user('id')): ?>
    <ul>
        
        <li class="dropdown hidden-xs">
             <a href="#" class="dropdown-toggle f-link-blue" data-toggle="dropdown">
                <?php if($this->Session->read("Config.language") == "esp"):?>
                    <?php echo  __('Español')?><span class="caret f-blue"></span> 
                <?php else: ?>
                     <?php echo __('Inglés')?><span class="caret f-blue"></span> 
                <?php endif; ?>
            </a>

            <ul class="dropdown-menu">
                <li> 
                    <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'esp')); ?>"><?php echo __('Español')?></a>
                    <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'eng')); ?>"><?php echo __('Inglés')?></a>
                </li>
            </ul> 
        </li>


        <li class="dropdown notificaciones bg-bluedark">
            <a href="#" class="dropdown-toggle" id="ReadAllNotification" data-toggle="dropdown" title="<?php echo __('Notificaciones') ?>">
                <span id="contNuevo" class="text-white"></span>
                <i class="flaticon-bell icon-notification f-white"></i>
            </a>
            <ul class="dropdown-menu alert-dropdown resultadoNotificaciones collapseddesktop"></ul>
        </li>
       

        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo $this->Html->url('/files/User/'.AuthComponent::user('img')); ?>" class="img-user-nav hidden-xs">
            </a>
        
            <a href="#" class="f-link-blue dropdown-toggle" data-toggle="dropdown"> 
              <?php if(empty(AuthComponent::user('firstname'))):?>
                <?php echo __("Nombre no asignado");?>
                <?php else: ?>
                <?php echo $this->Text->truncate(h(AuthComponent::user('firstname')).' '.AuthComponent::user('lastname'),20); ?> 
              <?php endif; ?>
            </a>

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-ellipsis-v f-blue icon-ellipsis"></i>
            </a>

            <ul class="dropdown-menu menu-dropdownuser">
                <li> 
                  <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'edit')) ?>"><?php echo __('Perfil') ?></a>
                  <a href="<?php echo configure::read("DATA_SUITE") ?>" target="_blank"><?php echo __('Registro de actividad') ?></a> 
                  <a class="hidden-lg hidden-md hidden-sm" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'esp')); ?>"><?php echo __('Español')?></a>
                  <a class="hidden-lg hidden-md hidden-sm" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'eng')); ?>"><?php echo __('Inglés')?></a>
                  <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'logout')) ?>"><?php echo __('Cerrar sesión'); ?></a>   
                </li>
            </ul>
        </li>
    </ul>
    <?php endif; ?>
</nav>

 

