<div style="display:none" class="modal fade bs-example-modal-lg" data-backdrop="static" data-keyboard="false" id="FormSolicitud" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="gridSystemModalLabel"><?php echo __('Nuevo requerimiento'); ?></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="row"> 
                <div class="col-xl-12 col-lg-12"> 
                    <div class="card"> 
                        <div class="card-body">
                            <div class="basic-form">
                                <?php echo $this->Form->create('Ticket', array('data-parsley-validate','url' => array('controller' => 'tickets','action' => 'add') )); ?>

                                    <div class="form-group">
                                        <?php echo $this->Form->input('codigo', array('class' => 'form-control select-single','label' => false, 'type' => 'hidden','value' => Configure::read('CODE_PROJECT'))); ?>
                                    </div>  
                                    
                                    <div class="form-group">
                                        <label><strong><?php echo __('¿Dónde Se Necesita?'); ?></strong></label>
                                        <?php echo $this->Form->input('donde_se_necesita', array('class' => 'form-control','label' => false, 'type' => 'textarea', 'rows' => 4, 'required' => true)); ?>
                                    </div>

                                    <div class="form-group">
                                        <label><strong><?php echo __('¿Para Qué Se Necesita?'); ?></strong></label>
                                        <?php echo $this->Form->input('para_que_se_necesita', array('class' => 'form-control','label' => false, 'type' => 'textarea', 'rows' => 4, 'required' => true)); ?>
                                    </div>

                                    <div class="form-group">
                                        <label><strong><?php echo __('¿Cómo Se Espera Qué Funcione?'); ?></strong></label>
                                        <?php echo $this->Form->input('como_se_espera_que_funcione', array('class' => 'form-control','label' => false, 'type' => 'textarea', 'rows' => 4, 'required' => true)); ?>
                                    </div>

                                    <div class="form-group">
                                        <label><strong><?php echo __('Información Adicional'); ?></strong> <?php echo __('(Opcional)'); ?></label>
                                        <?php echo $this->Form->input('algo_adicional', array('class' => 'form-control','label' => false, 'type' => 'textarea', 'rows' => 4)); ?>
                                    </div>

                                    <div class="form-group">
                                        <input type="button" id="EnviarRequerimiento" value="<?php echo __('Enviar'); ?>" class="submit btn btn-primary pull-right">
                                    </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</div>


