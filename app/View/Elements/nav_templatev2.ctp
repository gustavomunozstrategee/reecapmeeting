<header class="header">
                <nav class="navbar fixed-top">         
                    <!-- Begin Search Box-->
                    <div class="search-box">
                        <button class="dismiss"><i class="ion-close-round"></i></button>
                        <form id="searchForm" action="#" role="search">
                            <input type="search" placeholder="Search something ..." class="form-control">
                        </form>
                    </div>
                    <!-- End Search Box-->
                    <!-- Begin Topbar -->
                    <div class="navbar-holder">
                        <div class="row align-items-center">
                            <div class="col-xl-4 col-2">
                                <!-- Toggle Button -->
                                <a id="toggle-btn" href="#" class="menu-btn active ml-2">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </a>
                                <!-- End Toggle -->
                            </div>
                            <div class="col-xl-4 col-3 d-flex justify-content-center">
                                <div class="navbar-header">
                                    <a href="<?php echo $this->webroot ?>" class="navbar-brand">
                                        <div class="brand-image brand-big">
                                            <img src="<?php echo $this->webroot ?>img/logo.png" alt="logo" class="logo-big">
                                        </div>
                                        <div class="brand-image brand-small">
                                            <img src="<?php echo $this->webroot ?>img/logo.png" alt="logo" class="logo-small">
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xl-4 col-7">
                                <!-- Begin Navbar Menu -->
                                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">
                                    <!-- Search -->
                                     
                                    <!-- End Search -->
                                    <!-- Begin Notifications -->
                                    <li class="nav-item dropdown notificaciones">
                                        <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                            <i class="la la-bell pulse-icon-container"></i>
                                            <span class=" pulse-badge-container"></span>
                                        </a>
                                        <ul aria-labelledby="notifications" class="dropdown-menu notification <?php echo $this->request->is('mobile') ? 'notificacion-content-mobile' : 'notificacion-content' ?> visible-scroll">
                                            <li class="d-none">
                                                <div class="notifications-header">
                                                    <div class="title"><?php echo __('Notificaciones'); ?> <span id="contNuevo"></span></div>
                                                    <div class="elisyam-overlay overlay-07"></div>
                                                    <img src="<?php echo $this->Html->url('/img/bg_notifications.jpg') ?>" alt="..." class="img-fluid">
                                                </div>
                                            </li>
                                            <div class="resultadoNotificaciones">

                                            </div>
                                        </ul>
                                    </li>
                                    <!-- End Notifications -->
                                    <!-- User -->
                                    <li class="nav-item dropdown"><a id="user" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><img src="<?php echo $this->Html->url('/files/User/'.AuthComponent::user('img')); ?>" alt="..." class="avatar rounded-circle"> </a>
                                        <ul aria-labelledby="user" class="user-size dropdown-menu">
                                            <li class="welcome">
                                                <img src="<?php echo $this->Html->url('/files/User/'.AuthComponent::user('img')); ?>" alt="" class="rounded-circle">
                                                <?php if(empty(AuthComponent::user('firstname'))):?>
                                                    <?php echo __("Nombre no asignado");?>
                                                <?php else: ?>
                                                    <?php echo $this->Text->truncate(h(AuthComponent::user('firstname')).' '.AuthComponent::user('lastname'),20); ?> 
                                                <?php endif; ?>
                                            </li>


                                             

                                            <li>
                                                <a class="dropdown-item" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'edit')) ?>"><?php echo __('Perfil') ?></a>
                                            </li>
                                             
                                            
                                            <li>
                                                 <a class="dropdown-item no-padding-top" href="<?php echo configure::read("DATA_SUITE") ?>" target="_blank"><?php echo __('Registro de actividad') ?></a> 
                                            </li>
                                            <li><a rel="nofollow" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'logout')) ?>" class="dropdown-item logout text-center"><i class="ti-power-off"></i></a></li>
                                        </ul>
                                    </li>
                                    <!-- End User -->
                                    <!-- Begin Quick Actions -->
                                    <li class="nav-item"><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'logout')) ?>" ><i class="ti-power-off"></i></a></li>
                                    <!-- End Quick Actions -->
                                </ul>
                                <!-- End Navbar Menu -->
                            </div>
                        </div>
                    </div>
                    <!-- End Topbar -->
                </nav>
            </header>