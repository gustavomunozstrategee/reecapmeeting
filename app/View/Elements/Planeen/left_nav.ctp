<link rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css
" crossorigin="anonymous" />
<div class="row flex-row">
	<div class="col-xl-12">
	    <!-- Begin Widget -->
	    <div class="widget ">
	        <div class="col-md-12" style="margin-left: -5px;margin-bottom: 10px;">
			  <ul class="nav nav-tabs" id="myTab" role="tablist">
			  	<li>
			  		<img   onclick="location.href='<?php echo $this->webroot ?>planeens'" src="<?php echo $this->webroot ?>img/planeen.png" alt="..." style="width: 120px;cursor:pointer" class="avatar rounded-circle d-block mx-auto">
			  	</li>
			    <li class="nav-item">
                    <a class="nav-link " href="<?php echo $this->webroot ?>planeens"><i class="la la-calendar la-2x align-middle pr-2"></i><?php
                    echo __("Planeens Prioritarios")?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $this->webroot ?>planeens/inactive"><i class="la la-calendar la-2x align-middle pr-2"></i><?php
                    echo __("Planeens No Prioritarios")?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $this->webroot ?>planeens/reporte"><i class="la  la-file-excel-o la-2x align-middle pr-2"></i><?php
                    echo __("Generar Reportes")?></a>
                </li>
			  </ul>
			</div>
	    </div>
	    <!-- End Widget -->
	</div>
</div>
<style type="text/css">
	#myTab li a{
		margin-top: 41px;
	}
	.nav-tabs>li>a::after {
		bottom: 30px !important;
	}

</style>