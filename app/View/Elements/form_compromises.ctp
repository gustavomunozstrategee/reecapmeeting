<?php if($this->request->action != 'view'): ?> 
<table class="table">
    <tr  id="divclone" style="display:none"> 
        <td colspan="4">
            <div class="row">
                <div class="col-md-7">
                    <?php echo $this->Form->textarea('Commitments.GROUPID.description', array('class' => 'form-control resize-none COMMITMENT_DESC_REQUIRE', 'placeholder' => __('Compromiso...'), 'label' => false, 'rows' => 7)) ?>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="load_content_users_commitment" style="margin-bottom: 20px;">
                                <label><?php echo __("Responsable") ?></label>
                                <?php echo $this->Form->input('Commitments.GROUPID.asistente', array('empty'=>__('Seleccionar...'), 'class' => 'form-control  selectpicker show-menu-arrow selectpicker_comimment ', 'label' => false, 'type' => 'select','options' => $allUsers,"data-live-search='true'")); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-7">
                                  <label><?php echo __("Fecha límite") ?></label>
                                   <div class="input-group  date_all_contac date date-input-content">
                                        <?php echo $this->Form->input('Commitments.GROUPID.fecha', array('readonly', 'placeholder' => __('Fecha límite'), 'class' => 'form-control COMMITMENT_DATE_REQUIRE date_all', 'label' => false, 'div' => false)) ?>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="content-description-compromises center-block">
            							<?php if ($this->request->action != 'view'): ?>
            								<div class="remove-compromises">
            									<label>&nbsp</label>
            									<button style="width: 100%;"  class="f-btn-delete btn btn-primary btn-remove-commitments" data-id="GROUPID" type="button">x</button>
            								</div>
            							<?php endif ?>
            							<?php if ($this->request->action == 'edit'): ?>
            								<?php echo $this->Form->hidden('Commitments.GROUPID.id'); ?>
            							<?php endif ?>	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </td>
    </tr>
</table> 
<?php endif; ?>

