<div class="row flex-row">
	<div class="col-xl-3">
	    <!-- Begin Widget -->
	    <div class="widget has-shadow">
	        <div class="widget-body">
	            <div class="mt-5">
	            
	                <img   onclick="location.href='<?php echo $this->webroot ?>timeens'" src="<?php echo $this->webroot ?>img/timeen.png" alt="..." style="width: 120px;cursor:pointer" class="avatar rounded-circle d-block mx-auto">
	               
	            </div>
	            <div class="em-separator separator-dashed"></div>
	            <ul class="nav flex-column">
	                <li class="nav-item">
	                    <a class="nav-link" href="<?php echo $this->webroot ?>timeens"><i class="la la-clipboard la-2x align-middle pr-2"></i><?php
	                    echo __("Mis Actividades")?></a>
	                </li>
	                <li class="nav-item">
	                    <a class="nav-link" href="<?php echo $this->webroot ?>timeens/report"><i class="la la-align-right la-2x align-middle pr-2"></i><?php
	                    echo __("Reporte")?></a>
	                </li>
	                
	                
	                <li class="nav-item">
	                    <a class="nav-link" href="javascript:void(0)"><i class="la la-question-circle la-2x align-middle pr-2"></i>FAQ</a>
	                </li>
	            </ul>
	        </div>
	    </div>
	    <!-- End Widget -->
</div>