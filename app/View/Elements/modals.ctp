<div id="modalAssistantsMeeting" class="modal fade">
  <div class="modal-dialog modal-recapmeeting">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
              <h4 class="modal-title"><?php echo __("Detalle de la reunión")?></h4>
          </div> 
          <div id="show-content-meetings-detail" class="modal-content"></div>
      </div>
  </div>
</div>

<!-- MODAL DE SELECCIONAR USUARIOS PARA RENOVAR -->
<div id="modal_choose_users" class="modal fade">
  <div class="modal-dialog modal-recapmeeting">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
              <h4 class="modal-title"><?php echo __("Seleccionar miembros")?></h4>                        
          </div>
          <div id="show_content_users_teams" class="modal-body">
             
          </div>
          <div class="modal-footer"> 
              <button type="button" class="btn btn-modal-recapmeeting" id="save_cache_users_teams"><?php echo __("Aceptar")?></button> 
          </div>
      </div>
  </div>
</div>

<!-- MODAL PARA MOSTRAR QUE SE ACTIVARAN TODOS LOS USUARIOS CUANDO RENUEVE EL PLAN -->
<div id="modal_renovated_users" class="modal fade">
  <div class="modal-dialog modal-recapmeeting">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
              <h4 class="modal-title"><?php echo __("!Atención!")?></h4>                        
          </div>
          <div class="modal-body">
              <p class="text-center"><?php echo __("Cuando renueves tu plan se activaran nuevamente todos tus usuarios.");?></p>
          </div>
          <div class="modal-footer"> 
              <button type="button" class="btn btn-modal-recapmeeting" id="renovated_users"><?php echo __("Aceptar")?></button> 
          </div>
      </div>
  </div>
</div>


<!-- MODAL PARA MOSTRAR QUE SE ACTIVARAN TODOS LOS USUARIOS CUANDO SE HAGA EL UPGRADE EL PLAN -->
<div id="count_users_to_renovated" class="modal fade">
  <div class="modal-dialog modal-recapmeeting">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
              <h4 class="modal-title"><?php echo __("!Atención!")?></h4>                        
          </div>
          <div class="modal-body" id="count_users_disabled">
               
          </div>
          <div class="modal-footer"> 
              <button type="button" class="btn btn-modal-recapmeeting" id="reactivate_users"><?php echo __("Aceptar")?></button> 
              <button type="button" class="btn btn-modal-recapmeeting" id="cancel_reactivate_users"><?php echo __("Solo adquirir el plan")?></button> 
          </div>
      </div>
  </div>
</div>