<div class="modal fade" id="modalResendActa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><b><?php echo __('Reenvío de') ?></b> <b id="nombreActa"></b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="widget has-shadow">
          <div class="widget-body sliding-tabs">
             
            <div class="tab-content pt-3">
              <div class="tab-pane fade show active" id="tab-compartir-email" role="tabpanel" aria-labelledby="base-tab-compartir-email">
                <?php echo $this->Form->create('Contact', array('class'=>'frm-share-app mt-5','novalidate', 'url'=>array('controller'=>'contacs', 'action'=>'re_send'))); ?>
                   <?php echo $this->Form->input('ContactId',array("hidden"=>"true",'label'=>false)); ?>  
                  <div class="form-group">
                      <label for="ShareApplicationEmail"><?php echo __('Ingresa aquí los correos electrónicos a los cuales deseas reenviar el Acta.') ?></label>
                  </div>
                  <div class="form-group form-group-share">
                      <?php echo $this->Form->input('email', array('required', 'placeholder'=>__('Escribe aquí los correos'), 'class'=>'form-control txt-share-emails','div'=>false,'label'=>false,"required"=>false)); ?>
                  </div>
                  <div class="form-group h-25">
                    <button type="submit" class="btn btn-primary btn-share-app2 pull-right"><?php echo __("Enviar") ?></button>
                  </div>
                  <br>
                  <br>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary float-left" data-dismiss="modal"><?php echo __("Cerrar") ?></button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  

  $( document ).ready(function() {
    $( ".getContactId" ).click(function() {
       
      $("#ContactContactId").val($(this).attr("data-id"))
      $("#nombreActa").html($(this).attr("data-nombre"))
    });
});
</script>
