<div class="default-sidebar">
    <nav class="side-navbar box-scroll sidebar-scroll">
        <ul class="list-unstyled">
            <?php if (AuthComponent::user('role_id') == NULL) { ?> 
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'index')) ?>">
                        <i class="ti ti-home"></i>
                        <span><?php echo __('Inicio') ?></span>
                    </a>
                </li>
                
                <li class="">
                    <a href="#dropdown-db" aria-expanded="false" data-toggle="collapse" class="collapsed">
                        <i class="ti ti-clipboard">
                        </i><span><?php echo __('Actas') ?></span>
                    </a>
                    <ul id="dropdown-db" class="collapse list-unstyled  pt-0">
                        <li>
                            <a class="" href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'add')) ?>">
                                <?php echo __('Crear acta') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'index')) ?>">
                                <?php echo __('Listar') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#dropdown-db1" aria-expanded="false" data-toggle="collapse" class="collapsed">
                        <i class="ti ti-calendar">
                        </i><span><?php echo __('Reuniones') ?></span>
                    </a>
                    <ul id="dropdown-db1" class="collapse list-unstyled  pt-0">
                        <li>
                            <a class="" href="<?php echo $this->Html->url(array('controller'=>'meetings','action'=>'add')) ?>">
                                <?php echo __('Agendar reunión') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'meetings','action'=>'index')) ?>">
                                <?php echo __('Listar') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'teams','action'=>'index')) ?>">
                        <i class="ti ti-map-alt"></i>
                        <span><?php echo __('Configuración') ?></span>
                    </a>
                </li>
               
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'tasks','action'=>'index')) ?>">
                        <i class="ti ti-list"></i>
                        <span><?php echo __('Taskee') ?> </span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'timeens','action'=>'index')) ?>">
                        <i class="ti ti-time"></i>
                        <span><?php echo __('Timeen ') ?><b style="color:#FBBA38">(Beta)</b></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'planeens')) ?>" ">
                        <i class="ti ti-agenda"></i>
                        <span><?php echo __('Planeen') ?></span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'login_ennovart')) ?>" target="_blank">
                        <i class="ti ti-light-bulb"></i>
                        <span><?php echo __('EenovART') ?></span>
                    </a>
                </li>
               
               <?php if(AuthComponent::user('email') == 'yeisonmejia@strategee.us' || AuthComponent::user('email') == 'johandiaz@strategee.us') { ?>
                <li>
                    <a data-toggle="modal" data-target="#FormSolicitud" href="javascript:void(0);" >
                        <i class="ti ti-ticket"></i>
                        <span><?php echo __('Nueva solicitud') ?></span>
                    </a>
                </li>
     			<?php } ?>           
                 
            <?php } elseif(AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')) { ?>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'index_page')) ?>">
                        <i class="ti-home"></i>
                        <span><?php echo __('Inicio') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'plans','action'=>'index')) ?>">
                        <i class="ti-folder"></i>
                        <span><?php echo __('Planes') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'index')) ?>">
                        <i class="ti-user"></i>
                        <span><?php echo __('Usuarios') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'coupons','action'=>'index')) ?>">
                        <i class="ti-receipt"></i>
                        <span><?php echo __('Cupones') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller'=>'tasks','action'=>'mycommitmentsadmin')) ?>">
                        <i class="ti ti-list"></i>
                        <span><?php echo __('Taskee') ?></span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </nav>
</div>
<?php echo $this->element('modal_share'); ?>