
<?php 
	$userId  = AuthComponent::user("id");
    $step    = Cache::read("stepUser_{$userId}", 'STEPUSER'); 
?>

<?php if($step == configure::read("ENABLED")):?> 

<div class="container-navstep-web">
<nav class="nav-step-circle">
    <ul class="nav-step-ul-circle">
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "teams") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "teams","action" => "add"));?>"><i class="flaticon-interview-2"></i></a><span class="nav-step-name"><span class="nav-step-number">1</span><?php echo __("Adicionar empresa");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "positions") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "positions","action" => "add"));?>"><i class="flaticon-communication-2"></i></a><span class="nav-step-name"><span class="nav-step-number">2</span><?php echo __("Adicionar rol");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "clients") ?>"><a  class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "clients","action" => "add"));?>"><i class="flaticon-partnership"></i></a><span class="nav-step-name"><span class="nav-step-number">3</span><?php echo __("Adicionar cliente");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "projects") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "projects","action" => "add"));?>"><i class="flaticon-tool"></i></a><span class="nav-step-name"><span class="nav-step-number">4</span><?php echo __("Adicionar proyecto");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "user_teams") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "user_teams","action" => "add"));?>"><i class="flaticon-users"></i></a><span class="nav-step-name"><span class="nav-step-number">5</span><?php echo __("Adicionar usuario");?></span></li>
    </ul>
</nav>
</div>

<div class="nav-step-circle-mobile">
<button type="button" class="btn-nav-step-circle" id="btn-nav-step-circle"><?php echo __("");?><i class="flaticon-list"></i></button>
<div class="container-navstep-mobile">
<nav class="nav-step-circle">
    <ul class="nav-step-ul-circle">
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "teams") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "teams","action" => "add"));?>"><i class="flaticon-interview-2"></i></a><span class="nav-step-name"><span class="nav-step-number">1</span><?php echo __("Adicionar empresa");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "positions") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "positions","action" => "add"));?>"><i class="flaticon-communication-2"></i></a><span class="nav-step-name"><span class="nav-step-number">2</span><?php echo __("Adicionar rol");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "clients") ?>"><a  class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "clients","action" => "add"));?>"><i class="flaticon-partnership"></i></a><span class="nav-step-name"><span class="nav-step-number">3</span><?php echo __("Adicionar cliente");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "projects") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "projects","action" => "add"));?>"><i class="flaticon-tool"></i></a><span class="nav-step-name"><span class="nav-step-number">4</span><?php echo __("Adicionar proyecto");?></span></li>
        <li class="<?php  echo $this->Utilities->showStateStep($tasks, "user_teams") ?>"><a class="nav-step-a" href="<?php echo $this->Html->url(array("controller" => "user_teams","action" => "add"));?>"><i class="flaticon-users"></i></a><span class="nav-step-name"><span class="nav-step-number">5</span><?php echo __("Adicionar usuario");?></span></li>
    </ul>
</div>
</nav>  
</div>


<?php endif ?>

