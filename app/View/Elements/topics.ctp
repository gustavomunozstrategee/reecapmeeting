<?php $key = isset($key) ? $key : '<%= key %>'; ?>
<tr>
    <td style="width: 100%"> 
        <?php echo $this->Form->textarea("Topic.{$key}.description", array("class" => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Descripción'),"maxlength"=>255,"rows"=>"5")); ?>
    </td>   
    <td> 
        <?php echo $this->Form->text("Topic.{$key}.time", array("class" => 'form-control border-input time-duration','label'=>false,'div'=>false,'placeholder' => 'Tiempo',"readonly"=>"readonly", 'id' => $key)); ?>
    </td>   
    <td class="actions pull-right td-actions" style="vertical-align:middle">
        <a style="margin-top: 36px "" href="#" class="remove delete"><i class="la la-close delete"></i> </a>
    </td> 
</tr>  
