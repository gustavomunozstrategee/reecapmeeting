<?php $this->Html->css("frontend/website/elements/footer_website.css", array("block" => "styles_section")); ?>
<footer>
    <div class="footer-recapmeeting">
        <div class="row-footer">
            <div class="col-footer logo-reecapmeeting">
                <h3 class="f-blue font-bold">
                    <?php echo __('RecapMeeting')?>
                </h3>
                <p class="f-blue font-bold">
                    <?php echo __('ACTAS EN LÍNEA')?>
                </p>
                <p><?php echo __('A solution of StrategeeSuite.com')?></p>
                <p><?php echo __('© 1993-') ?><?php echo date('Y'); ?></p>
                <p>
                    <?php echo __('Strategee. LLC. or its affiliates.') ?>
                </p>
                <div class="socials">
                    <ul>
                        <li><a href="<?php echo configure::read("INSTAGRAM_URL_SUITE")?>" target="_blank"><i class="flaticon-instagram-logo" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo configure::read("FACEBOOK_URL_SUITE")?>" target="_blank"><i class="flaticon-facebook-logo" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo configure::read("YOUTUBE_URL_SUITE")?>" target="_blank"><i class="flaticon-youtube" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo configure::read("TWITER_URL_SUITE")?>" target="_blank"><i class="flaticon-twitter-logo-silhouette" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-footer">
                <h4>
                    <?php echo __('RecapMeeting')?>
                </h4>
                <div class="lineborder-bottom-footer"></div>
                <ul>
                    <li>
                        <a href="#comofunciona" class="ancla-home" data-href="<?php echo $this->Html->url(array("action" => "home"));?>#comofunciona">
                           <?php echo __('Olvida el papel')?>
                        </a>
                    </li>
                    <li>
                        <a href="#funciona" class="ancla-home" data-href="<?php echo $this->Html->url(array("action" => "home"));?>#porquedejarelpapel">
                          <?php echo __('¿Cómo funciona?')?>
                        </a>
                    </li>
                    <li>
                        <a href="#controltotal" class="ancla-home" data-href="<?php echo $this->Html->url(array("action" => "home"));?>#paraquienesutil">
                           <?php echo __('Control total')?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $this->Html->url(array("action" => "contacto"));?>">                        
                           <?php echo __('Quiero más información')?>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-footer">
                <h4>
                    <?php echo __('Políticas')?>
                </h4>
                <div class="lineborder-bottom-footer"></div>
                <ul>
                    <li>
                        <a href="http://strategeesuite.com/legal" target="_blank">
                            <?php echo __('Strategee Suite Legal Information')?>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-footer">
                <h4>
                    <?php echo __('Contacto')?>
                </h4>
                <div class="lineborder-bottom-footer"></div>
                <p>
                    <a href="tel:+57(4)604-1884" class="link-contact"><?php echo __('+ 57 (4) 604-1884')?></a>
                </p>
                <p>
                    <a href="mailto:info@strategeesuite.com" class="link-contact"><?php echo __('info@strategeesuite.com')?></a>
                </p>
            </div>
        </div>
    </div>
</footer> 

