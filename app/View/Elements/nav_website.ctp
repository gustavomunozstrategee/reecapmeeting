<?php $this->Html->css("frontend/website/elements/nav_website.css", array("block" => "styles_section")); ?>

<button type="button" class="burguer-button" id="burguer-button-website">
<i class="flaticon-list"></i>
</button>

<nav class="nav-website">
    <ul class="ul-left">
        <li>
            <a href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image("frontend/website/home/logo.svg", array('class' => 'img-responsive nav-website-logo', 'alt' => 'Logo Recapmeeting', 'title' => 'Recapmeeting')); ?></a>
        </li>
    </ul>
    <ul class="ul-right nav-website-mobile" id="nav-website-mobile">

    <li class="dropdown menu-li-language">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <?php if($this->Session->read("Config.language") == "esp"):?>
                <?php echo  __('Español')?><span class="caret"></span> 
            <?php else: ?>
                <?php echo __('Inglés')?><span class="caret"></span> 
            <?php endif; ?>
        </a>
        <ul class="dropdown-menu menu-dropdown-language">
            <li class="menu-dropdown-language-li"> 
                <a class="menu-dropdown-language-a" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'esp')); ?>"><?php echo __('Español')?></a>
                <a class="menu-dropdown-language-a" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'eng')); ?>"><?php echo __('Inglés')?></a>
            </li>
        </ul> 
    </li>
     <?php if(Authcomponent::user("id")):?>
        <li><a class="btn-nav" href="<?php echo $this->Html->url(array("controller" => "pages", "action" => "index_page"));?>"><?php echo __('Ir a la página principal')?></a></li> 
     <?php else:?>
        <li>
            <a class="" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "login"));?>">
                <?php echo __('Inicia sesión')?>
            </a>
        </li>
        <li>
            <a class="btn btn-navregister" href="<?php echo $this->Html->url(array("controller" => "users" , "action" => "add"));?>">
                <?php echo __('Regístrate')?>
            </a>
        </li>
    <?php endif; ?>


    </ul>
</nav>



