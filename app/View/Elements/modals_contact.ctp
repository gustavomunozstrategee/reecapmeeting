<div class="modal fade" id="modalAddClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel1"><?php echo __('Agregar un nuevo cliente') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="resultadoViewAddClient"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalAddProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel2"><?php echo __('Agregar un nuevo proyecto') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="resultadoViewAddProject"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalAddEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel3"><?php echo __('Agregar un nuevo asistente del cliente') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="resultadoViewAddEmployee"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalAddFuncionario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel4"><?php echo __('Agregar un nuevo funcionario') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="resultadoViewAddFuncionario"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalAddTemplate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel5"><?php echo __('Agregar una nueva plantilla') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="resultadoViewAddTemplate"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalLastContac" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel6" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel6"><?php echo __('Información de la última acta') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="show-detail-last-contac"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>

<?php $this->Html->script('controller/index.js', ['block' => 'AppScript']); ?>
 