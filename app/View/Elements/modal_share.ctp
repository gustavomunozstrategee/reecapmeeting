<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?php echo __('Compartir') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="widget has-shadow">
          <div class="widget-body sliding-tabs">
            <ul class="nav nav-tabs" id="example-one" role="tablist">
              <li class="nav-item col-md-6">
                <a class="nav-link active" id="base-tab-compartir-email" data-toggle="tab" href="#tab-compartir-email" role="tab" aria-controls="tab-compartir-email" aria-selected="true">
                  <?php echo __('Compartir por correo electrónico'); ?>
                </a>
              </li>
              <li class="nav-item col-md-6">
                <?php $msg = __('¡Hola! Descubrí RecapMeeting, una potente herramienta que te ayuda a gestionar y crear actas de tus reuniones al instante, descúbrelo aquí https://recapmeeting.com/'); ?>
                <a class="nav-link" id="base-compartir-whatsapp" target="__blank" href="https://web.whatsapp.com/send?text=<?php echo $msg; ?>" data-action="share/whatsapp/share">
                  <?php echo __('Compartir en WhatsApp'); ?>
                </a>
              </li>
            </ul>
            <div class="tab-content pt-3">
              <div class="tab-pane fade show active" id="tab-compartir-email" role="tabpanel" aria-labelledby="base-tab-compartir-email">
                <?php echo $this->Form->create('ShareApplication', array('class'=>'frm-share-app mt-5','novalidate', 'url'=>array('controller'=>'share_applications', 'action'=>'share'))); ?>
                  <div class="form-group">
                      <label for="ShareApplicationEmail"><?php echo __('Ingresa aquí los correos electrónicos de tus amigos y ayúdanos a compartir la aplicación.') ?></label>
                  </div>
                  <div class="form-group form-group-share">
                      <?php echo $this->Form->input('email', array('required', 'placeholder'=>__('Escribe aquí los correos'), 'class'=>'form-control txt-share-emails','div'=>false,'label'=>false,"required"=>false)); ?>
                  </div>
                  <div class="form-group h-25">
                    <button type="submit" class="btn btn-primary btn-share-app2 pull-right"><?php echo __("Compartir") ?></button>
                  </div>
                  <br>
                  <br>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary float-left" data-dismiss="modal"><?php echo __("Cerrar") ?></button>
      </div>
    </div>
  </div>
</div>