<div class="row flex-row">
	<div class="col-xl-3">
	    <!-- Begin Widget -->
	    <div class="widget has-shadow">
	        <div class="widget-body">
	            <div class="mt-5">
	            
	                <img   onclick="location.href='<?php echo $this->webroot ?>tasks/mycommitments'" src="<?php echo $this->webroot ?>img/taskee.png" alt="..." style="width: 120px;cursor:pointer" class="avatar rounded-circle d-block mx-auto">
	               
	            </div>
	            <div class="em-separator separator-dashed"></div>
	            <ul class="nav flex-column">
	                <li class="nav-item">
	                    <a class="nav-link" href="<?php echo $this->webroot ?>tasks/mycommitments"><i class="la la-clipboard la-2x align-middle pr-2"></i><?php
	                    echo __("Mis Compromisos")?></a>
	                </li>
	                <?php if($userPosition["UserTeam"]["smf_state"] == 1): ?>
	                	<li class="nav-item">
	                   	 	<a class="nav-link" href="<?php echo $this->webroot ?>tasks/"><i class="la la-bar-chart la-2x align-middle pr-2"></i><?php echo __("Reporte SMF") ?></a>
	                	</li>
	                <?php endif; ?>
	                <li class="nav-item">
                        <a class="nav-link" href="<?php echo $this->webroot ?>slates/"><i class="la la-folder-open la-2x align-middle pr-2"></i>
                        	<?php echo __("Mis listas de Tareas") ?>
                        </a>
                    </li>
	                <?php if($userPosition["Position"]["permission_taskee"] == "1" || $userPosition["Position"]["permission_taskee"] == "2"): ?>
	                	<?php if($userPosition["Position"]["permission_taskee"] == "1" ): ?>
	                		<li class="nav-item">
		                   	 	<a class="nav-link" href="<?php echo $this->webroot ?>tasks/admin"><i class="la la-bar-chart la-2x align-middle pr-2"></i>
		                   	 		<?php echo __("Solicitud de compromisos") ?>
		                   	 	</a>
		                	</li>	
	                	<?php endif; ?>

	                	<?php if($userPosition["Position"]["permission_taskee"] == "1" || $userPosition["Position"]["permission_taskee"] == "2"): ?>
	                		<li class="nav-item">
		                   	 	<a class="nav-link" href="<?php echo $this->webroot ?>tasks/report_admin_defeated"><i class="la la-bar-chart la-2x align-middle pr-2"></i>
		                   	 		<?php echo __("Compromisos vencidos") ?>
		                   	 	</a>
		                	</li>

		                	<li class="nav-item">
		                   	 	<a class="nav-link" href="<?php echo $this->webroot ?>tasks/report_admin_pending"><i class="la la-bar-chart la-2x align-middle pr-2"></i>
		                   	 		<?php echo __("Compromisos pendientes") ?>
		                   	 	</a>
		                	</li>	
		                	<li class="nav-item">
		                   	 	<a class="nav-link" href="<?php echo $this->webroot ?>tasks/report_admin"><i class="la la-bar-chart la-2x align-middle pr-2"></i>
		                   	 		<?php echo __("Compromisos terminados") ?>
		                   	 	</a>
		                	</li>	
	                	<?php endif; ?>	


	                <?php endif; ?>
	                
	                <li class="nav-item">
	                    <a class="nav-link" href="javascript:void(0)"><i class="la la-question-circle la-2x align-middle pr-2"></i>FAQ</a>
	                </li>
	            </ul>
	        </div>
	    </div>
	    <!-- End Widget -->
</div>