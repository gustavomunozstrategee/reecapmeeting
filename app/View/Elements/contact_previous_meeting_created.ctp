  	<?php if($this->request->action == "add"):?>
		<?php if(!empty($infoMeeting)): ?> 
	  		<a target="_blank" href="<?php echo $this->Html->url(array("controller" => "meetings","action" => "view", EncryptDecrypt::encrypt($infoMeeting)));?>" class="pull-right f-link-blue"><?php echo __("Agenda de la reunión >")?></a>
		<?php else: ?> 
			<h2 class="f-blue"><?php echo __("El acta no se inició desde la reunión")?></h2>				 
		<?php endif;?> 
	<?php endif;?> 
	
	<?php if($this->request->action == "edit"):?>
		<?php if(!empty($meetingId)): ?> 
	  		<a target="_blank" href="<?php echo $this->Html->url(array("controller" => "meetings","action" => "view", EncryptDecrypt::encrypt($meetingId)));?>" class="pull-right f-link-blue"><?php echo __("Agenda de la reunión >")?></a>
		<?php else: ?> 
			<h2 class="f-blue"><?php echo __("El acta no se inició desde la reunión")?></h2>				 
		<?php endif;?> 
	<?php endif;?>	