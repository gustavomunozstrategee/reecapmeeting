<aside>
    <button type="button" class="burguer-button-navleft" id="burguer-button-navleft">
      <i class="flaticon-list"></i>
   </button>
<?php $permissionTags     = $this->Utilities->check_team_permission_action(array("index"), array("tags")); ?>
<?php $permissionLogs     = $this->Utilities->check_team_permission_action(array("index"), array("logs")); ?>
<?php $permissionPages    = $this->Utilities->check_team_permission_action(array("filter"), array("pages")); ?>
<?php $permissionClients  = $this->Utilities->check_team_permission_action(array("index"), array("clients")); ?>
<?php $permissionProjects = $this->Utilities->check_team_permission_action(array("index"), array("projects")); ?>
<?php 
    $permissionUsers    = $this->Utilities->check_team_permission_action(array("index"), array("users")); 
?>

<nav class="nav-aside" id="nav-aside">
    <ul>
      <?php if (AuthComponent::user('role_id') == NULL): ?> 
            <a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'index_page')) ?>"><li><i class="flaticon-construction"></i><?php echo __('Inicio') ?></li></a>
            <a href="<?php echo $this->Html->url(array('controller'=>'plans','action'=>'planning_management')) ?>"><li><i class="flaticon-choices"></i><?php echo __('Planes') ?></li></a>
            <a href="<?php echo $this->Html->url(array('controller'=>'teams','action'=>'index')) ?>"><li><i class="flaticon-interview-2"></i><?php echo __('Empresas') ?></li></a>
            
            <a href="<?php echo $this->Html->url(array('controller'=>'positions','action'=>'index')) ?>">
                <li><i class="flaticon-communication-2"></i><?php echo __('Roles') ?></li>
            </a>

            <?php if (isset($permissionUsers["index"])): ?>
                <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'index')) ?>"><li><i class="flaticon-users"></i><?php echo __('Usuarios') ?></li></a>
            <?php endif; ?>
            
        
            <?php if (isset($permissionClients["index"])): ?>
                <a href="<?php echo $this->Html->url(array('controller'=>'clients','action'=>'index')) ?>"><li><i class="flaticon-partnership"></i><?php echo __('Clientes') ?></li></a>
            <?php endif; ?> 
            <?php if (isset($permissionProjects["index"])): ?>
                <a href="<?php echo $this->Html->url(array('controller'=>'projects','action'=>'index')) ?>"><li><i class="flaticon-tool"></i><?php echo __('Proyectos') ?></li></a>
            <?php endif; ?> 

            <a href="<?php echo $this->Html->url(array('controller'=>'meetings','action'=>'index')) ?>"><li><i class="flaticon-notebook"></i><?php echo __('Reuniones') ?></li></a>
            <a href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'index')) ?>"><li><i class="flaticon-file-1"></i><?php echo __('Actas') ?></li></a>
            <a href="<?php echo $this->Html->url(array('controller'=>'commitments','action'=>'index')) ?>"><li><i class="flaticon-search-2"></i><?php echo __('Compromisos') ?></li></a>
        
            <?php if (isset($permissionTags["index"])): ?>
                <a href="<?php echo $this->Html->url(array('controller'=>'tags','action'=>'index')) ?>"><li><i class="flaticon-hashtag"></i><?php echo __('Tags') ?></li></a>
            <?php endif; ?> 
            <?php if (isset($permissionLogs["index"])): ?>
                <a href="<?php echo $this->Html->url(array('controller'=>'logs','action'=>'index')) ?>"><li><i class="flaticon-time-1"></i><?php echo __('Logs') ?></li></a>
            <?php endif; ?>
            <?php if (isset($permissionPages["filter"])): ?>
                <a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'filter'));?>"><li><i class="flaticon-search-1"></i><?php echo __('Búsqueda avanzada') ?></li></a>   
            <?php endif; ?>
      <?php endif; ?> 

      <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
        <a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'index_page')) ?>"><li><i class="flaticon-construction"></i><?php echo __('Inicio') ?></li></a>
        <a href="<?php echo $this->Html->url(array('controller'=>'plans','action'=>'index')) ?>"><li><i class="flaticon-choices"></i><?php echo __('Planes') ?></li></a>
        <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'index')) ?>"><li><i class="flaticon-users"></i><?php echo __('Usuarios') ?></li></a>
        <a href="<?php echo $this->Html->url(array('controller'=>'coupons','action'=>'index')) ?>"><li><i class="flaticon-discount-voucher"></i><?php echo __('Cupones') ?></li></a>
      <?php endif; ?> 
    </ul>
</nav>
</aside>

