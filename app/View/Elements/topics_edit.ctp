<?php $key  = isset($key)  ? $key   : '<%= key %>'; ?>


<tr>
    <td class="td-word-wrap" style="width: 100%"> 
    	<div class="form-group">    		
        	<?php echo $this->Form->input("Topic.{$key}.description", array("class" => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Descripción'),"type"=>"textarea",'value' => !empty($data["Topic"]["description"]) ? $data["Topic"]["description"] : NULL,"maxlength"=>255,"rows"=>"5", "required" => false)); ?>
    	</div>
    	<div id="showCharacter"></div>
    </td>   
    <td> 
        <?php echo $this->Form->text("Topic.{$key}.time", array("class" => 'form-control border-input time-duration','label'=>false,'div'=>false,'placeholder' => __('Tiempo'),"readonly"=>"readonly", 'id' => $key, 'value' => !empty($data["Topic"]["time"]) ? $data["Topic"]["time"] : NULL, "required" => false)); ?>
    </td>   
    <td class="actions pull-right td-actions" style="vertical-align:middle">
        <a style="margin-top: 36px " href="javascript:void(0)" class="remove delete"><i class="la la-close delete"></i> </a>
    </td>  
</tr>  
