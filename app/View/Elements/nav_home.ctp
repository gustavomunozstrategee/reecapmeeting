<?php $this->Html->css("frontend/website/nav_home.css", array("block" => "styles_section")); ?>
<header>

<button type="button" class="burguer-button" id="burguer-button">
  <i class="flaticon-list"></i>
</button>

<!-- <nav class="select-language-mobile">
</nav> -->

<nav id="menu" class="menu">
    <ul class="ul-left">
    <li>
      <a><?php echo $this->Html->image("frontend/website/home/logo.svg",array('class' => 'img-responsive logotipo', 'alt' => 'Logo Recapmeeting', 'title' => 'Recapmeeting'));?></a>
    </li>
    <li><a href="#comofunciona" class="ancla-home"><?php echo __('Olvida el papel')?></a></li>
    <li><a href="#funciona" class="ancla-home"><?php echo __('¿Cómo funciona?')?></a></li>
    <li><a href="#controltotal" class="ancla-home"><?php echo __('Control total')?></a></li>
    <li><a href="<?php echo $this->Html->url(array("action" => "contacto"));?>">                        
            <?php echo __('Quiero más información')?>
        </a>  
    </li>
    <li class="dropdown menu-li-language">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <?php if($this->Session->read("Config.language") == "esp"):?>
                <?php echo  __('Español')?><span class="caret"></span> 
            <?php else: ?>
                <?php echo __('Inglés')?><span class="caret"></span> 
            <?php endif; ?>
        </a>
        <ul class="dropdown-menu menu-dropdown-language">
            <li class="menu-dropdown-language-li"> 
                <a class="menu-dropdown-language-a" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'esp')); ?>"><?php echo __('Español')?></a>
                <a class="menu-dropdown-language-a" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'change_language', 'eng')); ?>"><?php echo __('Inglés')?></a>
            </li>
        </ul> 
    </li>
  </ul>

    <ul class="ul-right">
    <?php if(Authcomponent::user("id")):?>
        <li><a class="btn-nav" href="<?php echo $this->Html->url(array("controller" => "pages", "action" => "index_page"));?>"><?php echo __('Ir a la página principal')?></a></li> 
    <?php else: ?>
        <li><a class="btn-nav" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "login"));?>"><?php echo __('Inicia sesión')?></a></li>
        <li><a class="btn btn-navregister" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "add"));?>"><?php echo __('Regístrate')?></a></li>

    <?php endif; ?> 
    <!-- <li class="dropdown select-languague">
             <a href="#" class="dropdown-toggle f-link-blue" data-toggle="dropdown">
                <?php echo __('Idioma')?>
                <span class="caret f-blue"></span>
            </a>

            <ul class="dropdown-menu">
                <li> 
                    <a href=""><?php echo __('Español')?></a>
                    <a href=""><?php echo __('Inglés')?></a>
                </li>
            </ul> 
    </li> -->

  </ul> 
</nav>
</header>









