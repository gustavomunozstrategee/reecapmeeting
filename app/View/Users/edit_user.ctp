<div class="backg-white" >   
    <div class="flex-space-between">
        <div>
            <h1 class="f-title">
                <?php echo __('Editar usuario'); ?>
            </h1>
        </div>
    </div>

    <div class="grid-form bg-form">
        <div class="col-50">
            <?php echo $this->Form->create('User',array('data-parsley-validate','type'=>'file','novalidate' => true)); ?>

            <div class="div-img-user">
                <img src="<?php echo $this->Html->url('/files/User/'.$this->request->data['User']['img']); ?>" width="200" height="200" alt="" />
            </div>
            <div class="img-user-upload">
                <div class='form-group'>
                    <?php if ($this->request->data['User']['id'] == AuthComponent::user('id')): ?>
                    <?php echo $this->Form->label('User.img',__('Cambiar imagen'), array('class'=>'control-label'));?>
                    <?php echo $this->Form->input('img', array('type'=>'file','class' => 'form-control border-input', 'label'=>false,'div'=>false,'required' => false)); ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="col-50">
            <p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>
            <div class='form-group'>
                <?php echo $this->Form->label('User.firstname',__('Nombre'), array('class'=>'control-label f-blue required'));?>
                <?php echo $this->Form->input('firstname',array('class' => 'form-control border-input','label' => false, 'placeholder' => __('Nombre'),'div'=>false)); ?>
            </div>
            <div class='form-group'>
                <?php echo $this->Form->label('User.lastname',__('Apellidos'), array('class'=>'control-label f-blue required'));?>
                <?php echo $this->Form->input('lastname',array('class' => 'form-control border-input','label' => false, 'placeholder' => __('Apellidos'),'div'=>false)); ?>
            </div>
            <div class='form-group'>
                <?php echo $this->Form->label('User.telephone',__('Teléfono'), array('class'=>'control-label f-blue required'));?>
                <?php echo $this->Form->input('telephone',array('class' => 'form-control border-input','label' => false, 'placeholder' => __('Teléfono'),'div'=>false, 'onkeypress' => "return onlyNumberField(event)")); ?>
            </div>
            <div class='form-group'>
                <?php echo $this->Form->label('User.email',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
                <?php echo $this->Form->input('email',array('disabled'=>true,'class' => 'form-control border-input','label' => false, 'placeholder' => __('Correo electrónico'),'div'=>false)); ?>
            </div>

            <div>
                <button type='submit' class='btn btn-reecapmeeting-user pull-right'><?php echo __('Guardar') ?></button>
            </div>

            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>