<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Adicionar usuario administrador'); ?>
				<a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __('Listar'); ?>"  href="<?php echo $this->Html->url(array('action'=>'index'));?>">
				   <i class="flaticon-list"></i>
			    </a>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"plans",'action'=>'index'));?>"><?php echo __("Usuarios") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">

            </div>
            <div class="widget-body">
			    <?php echo $this->Form->create('User',array('data-parsley-validate','type'=>'file','novalidate' => true)); ?>
					<p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>
			        <div class='form-group'>
						<?php echo $this->Form->label('User.firstname',__('Nombre'), array('class'=>'control-label f-blue required'));?>
						<?php echo $this->Form->input('firstname',array('class' => 'form-control border-input','label' => false, 'placeholder' => __('Nombre'), 'div'=>false, 'maxlength' => 80)); ?>
					</div>
			        <div class='form-group'>
						<?php echo $this->Form->label('User.lastname',__('Apellidos'), array('class'=>'control-label f-blue required'));?>
						<?php echo $this->Form->input('lastname',array('class' => 'form-control border-input','label' => false, 'placeholder' => __('Apellidos'), 'div'=>false, 'maxlength' => 80)); ?>
					</div>
			        <div class='form-group'>
					  <?php echo $this->Form->label('User.email',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
					  <?php echo $this->Form->input('email',array('class' => 'form-control border-input','label' => false, 'placeholder' => __('Correo electrónico'), 'div'=>false, 'maxlength' => 80)); ?>
				    </div>
			        <div>
					   <button type='submit' class='btn btn-primary'><?php echo __('Guardar') ?></button>
			        </div>
		        <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
 