<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')){ ?>
                    <?php echo __('Crear cuenta administradora'); ?> 
                <?php } ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar administrador")?>" href="<?php echo $this->Html->url(array('action'=>'add_admin_user')); ?>">
                    <i class="flaticon-add"> </i>
                </a> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"plans",'action'=>'index'));?>"><?php echo __("Usuarios") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row"> 
                        <div class="col-md-6">
                            <div class="input-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Buscar...'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <?php if(empty($users) && !empty($this->request->query['q'])) : ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?></p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'),array('class' => 'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="<?php echo $this->request->is('mobile') ? '' : 'widget-body' ?>">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                        <thead>
                            <tr>
                                <th class="table-grid-20"><?php echo __('Nombre'); ?></th>
                                <th class="table-grid-20"><?php echo __('Apellido'); ?></th>
                                <th class="table-grid-20"><?php echo __('Correo electrónico'); ?></th>
                                <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                                <th class="table-grid-10"><?php echo __('Rol'); ?></th>
                                <?php endif; ?> 
                                <!-- <th class="table-grid-10"><?php echo __('Imagen'); ?></th> -->
                                <th class="table-grid-10"><?php echo __('Estado'); ?></th>
                                <th class="table-grid-10"><?php echo __('Fecha de registro'); ?></th>
                                <th class="table-grid-10 text-center"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($users)){ ?>
                            <?php foreach ($users as $user): ?>
                                <tr>
                                    <td class="table-grid-20 td-word-wrap"><?php echo $this->Text->truncate(h($user['User']['firstname']),100); ?></td>
                                    <td class="table-grid-20 td-word-wrap"><?php echo $this->Text->truncate(h($user['User']['lastname']),100); ?></td>
                                    <td class="table-grid-20 td-word-wrap"><?php echo $this->Text->truncate(h($user['User']['email']),100); ?></td>
                                    <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Utilities->nameRol($user['User']['role_id']); ?></td>
                                    <?php endif; ?> 
                                    <!-- <td class="table-grid-10 td-word-wrap">
                                        <img src="<?php echo $this->Html->url('/files/User/'.$user['User']['img']); ?>" width="50px" class="img-responsive" alt="" />
                                    </td> -->
                                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Utilities->showState($user['User']['state']); ?></td>
                                    <td class="table-grid-10 td-word-wrap"><?php echo h($this->Time->format('d-m-Y h:i A', $user['User']['created'])); ?></td>
                                    <td class="td-actions text-center table-grid-10">
                                        <?php if (AuthComponent::user('role_id') == Configure::read('EMPLOYEE_ROLE_ID') || AuthComponent::user('role_id') == Configure::read('BUSINESS_ROLE_ID')): ?>
                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($user['User']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <?php endif; ?>

                                        <?php if (AuthComponent::user('role_id') == Configure::read('BUSINESS_ROLE_ID')): ?>
                                        <?php if ($user['User']['role_id'] != Configure::read('BUSINESS_ROLE_ID')): ?>
                                        <?php echo $this->Utilities->changeStateButton($user['User']['id'], $user['User']['state']); ?>
                                        <?php endif ?>
                                        <?php endif; ?>

                                        <?php if (AuthComponent::user('role_id') == Configure::read('BUSINESS_ROLE_ID')): ?>
                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'edit_user', EncryptDecrypt::encrypt($user['User']['id']))); ?>" title="<?php echo __('Editar'); ?>" class="btn-xs" data-toggle="tooltip">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <?php endif ?>

                                        <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                                            <?php if($user["PlanUser"]["id"] != null):?>

                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view',EncryptDecrypt::encrypt($user['User']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <?php echo $this->Utilities->changeStateButton($user['User']['id'], $user['User']['state']); ?>

                                            <?php else: ?>
                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view',EncryptDecrypt::encrypt($user['User']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <?php echo $this->Utilities->changeStateButton($user['User']['id'], $user['User']['state']); ?>

                                                <?php if ($user['User']['role_id'] != Configure::read('ADMIN_ROLE_ID')): ?>
                                                    <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'add','controller' => 'coupons')); ?>" title="<?php echo __('Obsequiar cupón'); ?>" class="btn-xs" data-toggle="tooltip" id="gift-coupon-business" data-id="<?php echo EncryptDecrypt::encrypt($user['User']['id'])?>">
                                                        <i class="fa fa-gift"></i>
                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="8"><?php echo __('No existen usuarios.')?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 