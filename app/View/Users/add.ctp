<?php $this->Html->css("frontend/admin/pages/users.css", array("block" => "styles_section")); ?>

  <section class="bg__register">
     <div class="row__containerregister">
           <div class="col__registerleft">
            <a href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image("frontend/website/home/logo.svg", array('class' => 'img-responsive logo-register', 'alt' => 'Recapmeeting')); ?></a>
            <!-- uno -->
        <h1 class="f-title-register"><?php echo __('REGISTRO') ?></h1>
       <div class="row__register">
         <div class="icon-list"><i class="flaticon-communication-2 ml-0"></i></div>
         <div>
         <p class="text-greenlight font-bold"><?php echo __('Registra') ?></p>
         <p class="text-list"><?php echo __('a tus clientes, proyectos y demás usuarios que utilizarán la cuenta.') ?></p>
         </div>
        </div>
      <!-- dos -->
      <div class="row__register">
      <div class="icon-list"><i class="flaticon-interface ml-0"></i></div>
      <div>
       <p class="text-greenlight font-bold"><?php echo __('Crea') ?></p>
       <p class="text-list"><?php echo __('proyectos y actas con la información necesaria.') ?></p>
      </div>
      </div>
      <!-- tres -->
                <div class="row__register">
                   <div class="icon-list"><i class="flaticon-communication-1 ml-0"></i></div>
                    <div>
                    <p class="text-greenlight font-bold"><?php echo __('Controla') ?></p>
                    <p class="text-list"><?php echo __('todos los pendientes generados en las diferentes reuniones.') ?></p>
                    </div>
                </div>    
           </div>

           <div class="col__registerright">
           <a href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image("frontend/website/home/logo.svg", array('class' => 'img-responsive logo-register-xs', 'alt' => 'Recapmeeting')); ?></a>
           <div class="row" >
                <div class="col-md-6" style="margin-top: 5px">
                    <a href="<?= 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online' ?>">
                        <img width="100%" src="<?php echo $this->webroot ?>img/googleLogin.png">
                    </a>
                </div>

                <div class="col-md-6" style="margin-top: 5px">
                    <a href="<?php echo 'Https://login.live.com/oauth20_authorize.srf?client_id='.Configure::read("CLIENT_ID_OUT2").'&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri='.Configure::read("CLIENT_URL_OUT2") ?>">
                         <img width="100%" src="<?php echo $this->webroot ?>img/outlookLogin.png">
                    </a>
                     
                </div>

            </div>
             <hr>
           <?php echo $this->Form->create('User',array('data-parsley-validate','novalidate' => true,'enctype' => 'multipart/form-data')); ?>
                        <!-- <h1 class="f-title-login"><?php echo __('REGISTRO') ?></h1> -->
                        <div class="flex-start">
                            <div class="col-50 padding-left-15">
                                <div class="form-group">
                                    <?php echo $this->Form->label('User.firstname',__('Nombres'), array('class'=>'control-label text-blue-dark required'));?>
                                    <?php echo $this->Form->input('firstname',array('class' => 'form-control border-input input-transparent','label' => false,'div'=>false, "data-parsley-pattern" => "/^\D+$/")); ?>
                                </div>
                            </div>
                            <div class="col-50 padding-right-15">
                                <div class="form-group">
                                    <?php echo $this->Form->label('User.lastname',__('Apellidos'), array('class'=>'control-label text-blue-dark required'));?>
                                    <?php echo $this->Form->input('lastname',array('class' => 'form-control border-input input-transparent','label' => false,'div'=>false, "data-parsley-pattern" => "/^\D+$/")); ?>
                                </div>
                            </div>
                        </div>
                        <div class="flex-start">
                            <div class="col-100">
                                <div class="form-group">
                                    <?php echo $this->Form->label('User.email',__('Correo electrónico'), array('class'=>'control-label text-blue-dark required'));?>
                                    <?php echo $this->Form->input('email',array('class' => 'form-control border-input input-transparent','label' => false, 'div'=>false)); ?>
                                </div>
                            </div> 
                        </div>
                        <div class="flex-start">
                            <div class="col-50 padding-left-15">
                                <div class="form-group">
                                    <?php echo $this->Form->label('User.password',__('Contraseña'), array('class'=>'control-label text-blue-dark required'));?>
                                    <?php echo $this->Form->input('password',array('class' => 'form-control border-input input-transparent','label' => false,'div'=>false)); ?>
                                </div>
                            </div>
                            <div class="col-50 padding-right-15">
                                <div class="form-group">
                                    <?php echo $this->Form->label('User.confirm_password',__('Confirmar contraseña'), array('class'=>'control-label text-blue-dark required'));?>
                                    <?php echo $this->Form->input('confirm_password',array('class' => 'form-control border-input input-transparent','label' => false,'div'=>false, 'type' => 'password')); ?>
                                </div>
                            </div> 
                        </div>
                        <div class="flex-start">
                          <div class="col-100">
                          <div class="form-group">
                                <div class="checkbox check_policy">
                                    <label>
                                    <?php if(isset($this->request->data["User"]["accepting_policies"])):?>
                                        <?php echo $this->Form->input("accepting_policies", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false,'checked' => true, 'required' => true)); ?>
                                    <?php else: ?>
                                        <?php echo $this->Form->input("accepting_policies", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false, 'required' => true)); ?>
                                    <?php endif; ?>
                                        <?php echo __("Declaro que he leído y acepto lo siguiente:");?> 
                                        <a href="<?php echo configure::read("LEGAL_COOKIES")?>" target="_blank">Cookies Policy</a>, 
                                        <a href="<?php echo configure::read("LEGAL_DISCLAIMER")?>" target="_blank">Disclaimer</a>, 
                                        <a href="<?php echo configure::read("LEGAL_EULA")?>" target="_blank">EULA (End-User Licence Agreement)</a>, 
                                        <a href="<?php echo configure::read("LEGAL_PRIVACITY")?>" target="_blank">Privacy Policy</a>, 
                                        <a href="<?php echo configure::read("LEGAL_RETURNS")?>" target="_blank">Returns & Refunds Policy</a> 
                                        <a href="<?php echo configure::read("LEGAL_T_Y_C")?>" target="_blank">and Terms & Conditions.</a> 
                                    <a>*</a>
                                    </label>
                                </div>
                            </div>
                          </div>
                          <div class="col-100">
                                <div class="checkbox check_policy">
                                    <label>
                                    <?php if(isset($this->request->data["User"]["accepting_data_collection"])):?>
                                        <?php echo $this->Form->input("accepting_data_collection", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false,'checked'  => true, 'required' => true)); ?>
                                    <?php else: ?>
                                        <?php echo $this->Form->input("accepting_data_collection", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false,'required' => true)); ?>
                                    <?php endif; ?>
                                        <?php $urlPrivacity = "<a href=".configure::read("LEGAL_PRIVACITY")." target='_blank'>" . __("Privacy Policy") . '</a>'; ?>
                                        <?php echo sprintf(__("Doy mi consentimiento para la recopilación, uso, retención, transferencia, divulgación y procesamiento de mis datos personales. Los datos personales son recopilados, usados, retenidos, transferidos, divulgados y procesados ​​de acuerdo con nuestra %s que cumple con las leyes nacionales e internacionales, incluidas las normas GDPR."), $urlPrivacity)?>
                                     <a>*</a>
                                    </label>
                                </div>
                          </div>
                          
                          <div class="form-group">
                              <br>
                                  <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('RE_CAPTCHA_KEY_WEB_SITE')?>"></div>
                              <br>
                          </div>

                        </div>
                        <div class="flex-justify-end"> 
                            <?php echo $this->Form->end(array('class' => 'btn btn-reecapmeeting myb-10','label'=>__('Regístrate'))); ?>
                        </div> 
                    </div>
                
           </div>
           
     
  </section>

 



