<?php $this->Html->css("frontend/admin/pages/users.css", array("block" => "styles_section")); ?>

<section class="bg__register-password">
    <?php echo $this->Html->image("frontend/website/home/bg-form-right.svg",array('class' => 'img-responsive bg-form-right', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <div class="row__containerpassword">

    <div class="col__registerleft">
       <a href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image("frontend/website/home/logo.svg", array('class' => 'img-responsive logo-register', 'alt' => 'Recapmeeting')); ?></a>
    </div>    

    <div class="col__registerrightpassword">
        <div>
        <a href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image("frontend/website/home/logo.svg", array('class' => 'img-responsive logo-register-xs', 'alt' => 'Recapmeeting')); ?></a>
        <?php echo $this->Form->create('User',array('data-parsley-validate','novalidate' => true,'url' => array('controller' => 'users','action' => 'change_password', $hash, $restorePassword)));?>
        <?php if(isset($restorePassword)):?>
        <h1 class="text-greenlight f-title-green text-center"><?php echo __('RESTABLECER CONTRASEÑA') ?></h1>
        <?php else: ?>
        <h1 class="text-greenlight f-title-green"><?php echo __('COMPLETAR REGISTRO') ?></h1>
        <?php endif; ?>
    

        <div>
            <?php if (!empty($hash)): ?>
            <?php echo $this->Form->input('hash',array('value' => $hash,'type' => 'hidden')); ?>
            <?php endif ?>
            <?php if (isset($restorePassword)): ?>
            <?php echo $this->Form->input('restorePassword',array('value' => $restorePassword,'type' => 'hidden')); ?>
            <?php endif ?>

            <?php if(!isset($restorePassword)):?> 
                <div class="form-group">
                    <?php echo $this->Form->label('User.firstname',__('Nombres'), array('class'=>'control-label f-blue required'));?>
                    <?php echo $this->Form->input('firstname',array('class' => 'form-control border-input input-transparent','label' => false,'div'=>false, "data-parsley-pattern" => "/^\D+$/")); ?>
                </div> 
          
                <div class="form-group">
                    <?php echo $this->Form->label('User.lastname',__('Apellidos'), array('class'=>'control-label f-blue required'));?>
                    <?php echo $this->Form->input('lastname',array('class' => 'form-control border-input input-transparent','label' => false,'div'=>false, "data-parsley-pattern" => "/^\D+$/")); ?>
                </div>

                <div class="form-group">
                    <div class="checkbox check_policy">
                        <label>
                        <?php if(isset($this->request->data["User"]["accepting_policies"])):?>
                            <?php echo $this->Form->input("accepting_policies", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false,'checked' => true, 'required' => true)); ?>
                        <?php else: ?>
                            <?php echo $this->Form->input("accepting_policies", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false, 'required' => true)); ?>
                        <?php endif; ?>
                            <?php echo __("Declaro que he leído y acepto lo siguiente:");?> 
                            <a href="<?php echo configure::read("LEGAL_COOKIES")?>" target="_blank">Cookies Policy</a>, 
                            <a href="<?php echo configure::read("LEGAL_DISCLAIMER")?>" target="_blank">Disclaimer</a>, 
                            <a href="<?php echo configure::read("LEGAL_EULA")?>" target="_blank">EULA (End-User Licence Agreement)</a>, 
                            <a href="<?php echo configure::read("LEGAL_PRIVACITY")?>" target="_blank">Privacy Policy</a>, 
                            <a href="<?php echo configure::read("LEGAL_RETURNS")?>" target="_blank">Returns & Refunds Policy</a> 
                            <a href="<?php echo configure::read("LEGAL_T_Y_C")?>" target="_blank">and Terms & Conditions.</a> 
                        <a>*</a>
                        </label>
                    </div>                               
                </div>        

                <div>
                    <div class="checkbox check_policy">
                        <label>
                        <?php if(isset($this->request->data["User"]["accepting_data_collection"])):?>
                            <?php echo $this->Form->input("accepting_data_collection", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false,'checked' => true, 'required' => true)); ?>
                        <?php else: ?>
                            <?php echo $this->Form->input("accepting_data_collection", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false, 'required' => true)); ?>
                        <?php endif; ?>
                            <?php $urlPrivacity = "<a href=".configure::read("LEGAL_PRIVACITY")." target='_blank'>" . __("Privacy Policy") . '</a>'; ?>
                            <?php echo sprintf(__("Doy mi consentimiento para la recopilación, uso, retención, transferencia, divulgación y procesamiento de mis datos personales. Los datos personales son recopilados, usados, retenidos, transferidos, divulgados y procesados ​​de acuerdo con nuestra %s que cumple con las leyes nacionales e internacionales, incluidas las normas GDPR."), $urlPrivacity)?>
                         <a>*</a>
                        </label>
                    </div>
                </div>            
            <?php endif ?>
            <div>
                <div class='form-group'>
                    <?php echo $this->Form->label('User.password',__('Nueva contraseña'), array('class'=>'control-label f-blue required'));?>
                    <?php echo $this->Form->input('password',array('placeholder' => __('Nueva contraseña'),'label'=> false,'class' => 'form-control border-input input-transparent','required'=>true,'div' => false));?>
                </div>
            </div>
            <div>
                <div class='form-group'>
                    <?php echo $this->Form->label('User.confirm_password',__('Confirmar la nueva contraseña'), array('class'=>'control-label f-blue required'));?>
                    <?php echo $this->Form->input('confirm_password',array('placeholder' => __('Confirmar la nueva contraseña'),'label'=> false,'class' => 'form-control border-input input-transparent','required'=>true,'type'=>'password','div' => false));?>
                </div>
            </div>
        </div>

        <div class="flex-justify-end">
            <?php if(isset($restorePassword)):?>
            <?php echo $this->Form->end(array('class'=>'btn btn-reecapmeeting-user','label'=>__('Restablecer contraseña'))); ?>
            <?php else: ?>
            <?php echo $this->Form->end(array('class'=>'btn btn-reecapmeeting-user','label'=>__('Confirmar'))); ?>
            <?php endif; ?>
        </div>
    </div>
    </div>
    </div>

</section>

