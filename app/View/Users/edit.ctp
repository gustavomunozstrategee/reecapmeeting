<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Editar perfil'); ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Usuarios") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Form->create('User',array('','type'=>'file', "data-parsley-validate" => "data-parsley-validate")); ?>
<div class="row flex-row">
    <div class="col-xl-3">
        <div class="widget has-shadow">
            <div class="widget-body">
                <div class="mt-5">
                    <?php $img = AuthComponent::user('img'); ?>
                    <?php if (!file_exists(WWW_ROOT."files/User/{$img}")){ ?>
                        <img src="<?php echo $this->Html->url('/files/User/default.jpg'); ?>" style="width: 120px;" class="avatar rounded-circle d-block mx-auto preview-profile-image"/>
                    <?php } else { ?>
                        <img src="<?php echo $this->Html->url('/files/User/'.AuthComponent::user("img")); ?>" style="width: 120px;" class="avatar rounded-circle d-block mx-auto preview-profile-image"/>
                    <?php } ?>
                </div>
                <h3 class="text-center mt-3 mb-1"><?php echo AuthComponent::user('firstname').' '.AuthComponent::user('lastname'); ?></h3>
                <p class="text-center"><?php echo AuthComponent::user('email'); ?></p>
                <div class="em-separator separator-dashed"></div>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <?php if ($this->request->data['User']['id'] == AuthComponent::user('id')) {?>
                            <div class="text-center">
                                <label for="UserImg" class="btn btn-primary"><?php echo __("Seleccionar imagen")?></label>
                                <?php echo $this->Form->input('img', array('type'=>'file','class' => 'form-control border-input', 'label'=>false,'div'=>false,'required' => false, 'style' => 'visibility:hidden;')); ?>
                            </div>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xl-9">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
            </div>
            <div class="widget-body">
                <div class="form-horizontal">
                    <?php if(AuthComponent::user("role_id") != configure::read("ADMIN_ROLE_ID")) {?>
<!--                         <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end required"><?php //echo __('Razón social') ?></label>
                            <div class="col-lg-6">
                                <?php //echo $this->Form->input('company_name',array('class' => 'form-control','label' => false, 'placeholder' => __('Razón social'),'div'=>false,'maxlength' => 100)); ?>
                            </div>
                        </div> -->

                        <!-- <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end required"><?php //echo __('Nit'); ?></label>
                            <div class="col-lg-6">
                                <?php //echo $this->Form->input('nit',array('class' => 'form-control','label' => false, 'placeholder' => __('Nit'),'div'=>false, 'maxlength' => 15)); ?>
                            </div>
                        </div> -->
                    <?php }?> 
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-2 form-control-label d-flex justify-content-lg-end required"><?php echo __('Nombre'); ?></label>
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('firstname',array('class' => 'form-control','label' => false, 'placeholder' => __('Nombre'),'div'=>false)); ?>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-2 form-control-label d-flex justify-content-lg-end required"><?php echo __('Apellidos'); ?></label>
                        <div class="col-lg-6">
                            <?php echo $this->Form->input('lastname', array('class' => 'form-control','label' => false, 'placeholder' => __('Apellidos'),'div'=>false)); ?>
                        </div>
                    </div>

                    <?php if(AuthComponent::user("role_id") != configure::read("ADMIN_ROLE_ID")){ ?>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end"><?php echo __('Teléfono'); ?></label>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('telephone',array('class' => 'form-control','label' => false, 'placeholder' => __('Telefóno'),'div'=>false, 'onKeyPress'=>"return onlyNumberField(event);",'data-parsley-type'=>"number",'maxlength' => 10,"required" => false)); ?>
                            </div>
                        </div>

                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end"><?php echo __('Dirección'); ?></label>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('address',array('class' => 'form-control','label' => false, 'placeholder' => __('Dirección'),'div'=>false)); ?>
                            </div>
                        </div>

                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end"></label>
                            <?php if($this->Session->read("Config.language") == "esp") {?>
                                <div class="col-lg-6">
                                    <p><?php echo __('Idioma de aplicación'); ?></p>
                                    <?php echo $this->Form->input('lang', array('class' => 'form-control', 'label'=>false,'div'=>false,'required' => true, 'empty' => __("Seleccione una opción"), 'type' => 'select', 'options' => configure::read("LANG"))); ?>
                                </div>
                            <?php } else{ ?>
                                <div class="col-lg-6">
                                    <p><?php echo __('Idioma de aplicación'); ?></p>
                                    <?php echo $this->Form->input('lang', array('class' => 'form-control', 'label'=>false,'div'=>false,'required' => true, 'empty' => __("Seleccione una opción"), 'type' => 'select', 'options' => configure::read("LANG_ENG"))); ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?> 

                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-2 form-control-label d-flex justify-content-lg-end"></label>
                        <div class="col-lg-6">
                            <p><?php echo __('Correo electrónico'); ?></p>
                            <?php echo $this->Form->input('email',array('disabled'=>true,'class' => 'form-control','label' => false, 'placeholder' => __('Correo eléctronico'),'div'=>false, 'value' => AuthComponent::user('email'))); ?>
                            <?php if ($this->request->data['User']['id'] == AuthComponent::user('id')): ?>
                                <p class="f-blue"><?php echo __('¿Quieres cambiar tu contraseña? Haz clic')?> <a class="f-link-blue f-bold" href='#modalPassword' data-toggle='modal'><?php echo __('aquí') ?></a> <?php echo __('para iniciar con el proceso.')?></p>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="em-separator separator-dashed"></div>
                <div class="text-right">
                    <button class="btn btn-primary" type="submit"><?php echo __('Guardar') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?php echo __('Cambiar contraseña') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class='col-md-12 text-center'>
            <p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>
        </div>

        <div class="body">
            <?php if (AuthComponent::user("password") != ""): ?>                
                <div class="form-group">
                    <?php echo $this->Form->label('User.password',__('Contraseña actual'), array('class'=>'control-label f-blue required'));?>
                    <?php echo $this->Form->input('password',array('placeholder' => __('Contraseña actual'),'label'=> false,'class' => 'form-control'));?>
                </div>
            <?php else: ?>
                <?php echo $this->Form->input('password',array('placeholder' => __('Contraseña actual'),'label'=> false,'class' => 'form-control',"type" =>"hidden","value" => uniqid()));?>
            <?php endif ?>

            <div class="form-group">
                <?php echo $this->Form->label('User.passwordn',__('Nueva contraseña'), array('class'=>'control-label f-blue required'));?>
               <?php echo $this->Form->input('passwordn',array('placeholder' => __('Nueva contraseña'),'label'=> false,'class' => 'form-control','type'=>'password'));?>
            </div>

            <div class="form-group">
                <?php echo $this->Form->label('User.re_passwordn',__('Confirmar la nueva contraseña'), array('class'=>'control-label f-blue required'));?>
                <?php echo $this->Form->input('re_passwordn',array('placeholder' => __('Confirmar la nueva contraseña'),'label'=> false,'class' => 'form-control','type'=>'password'));?>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn_password"><?php echo __('Guardar nueva contraseña') ?></button>
      </div>
    </div>
  </div>
</div>

<?php 
    $this->Html->script('controller/users/actions.js', ['block' => 'AppScript']);
    $this->Html->scriptBlock("EDIT.initElement(); ",   ['block' => 'AppScript']); 
?>


