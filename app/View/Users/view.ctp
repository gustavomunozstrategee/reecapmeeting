<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Detalle del usuario'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar administrador")?>" href="<?php echo $this->Html->url(array('action'=>'add_admin_user')); ?>">
                    <i class="flaticon-add"> </i>
                </a> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"plans",'action'=>'index'));?>"><?php echo __("Usuarios") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">

            </div>
            <div class="widget-body">
                <div class="row">
                    <div class="col-md-4">
                        <div>
                            <input type="hidden" value="<?php echo EncryptDecrypt::encrypt($user['User']['id']) ?>" id="user-id">
                            <div class="img-user-admin">
                                <div class="thumbnail">
                                    <img src="<?php echo $this->Html->url('/files/User/'.$user['User']['img']); ?>" class="" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <table class="table">
                            <tr>
                                <td><?php echo __('Nombre'); ?></td>
                                <td><?php echo h($user['User']['firstname']); ?> <?php echo h($user['User']['lastname']); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
                            </tr>
                            <tr>
                                <td><?php echo __('Correo electrónico'); ?></td>
                                <td><?php echo h($user['User']['email']); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
                            </tr>
                            <tr>
                                <td><?php echo __('Estado'); ?></td>
                                <td><?php echo $this->Utilities->showState($user["User"]["state"]); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
                            </tr>
                            <tr>
                                <td><?php echo __('Creación'); ?></td>
                                <td><?php echo h($this->Time->format('d-m-Y h:i A', $user['User']['created'])); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (AuthComponent::user('role_id') == Configure::read('BUSINESS_ROLE_ID') || AuthComponent::user('role_id') == Configure::read('EMPLOYEE_ROLE_ID')): ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h2><?php echo __('Compromisos'); ?></h2>
                </div>
                <div class="widget-body">
                    <div id="show-content-commitments-users"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h2><?php echo __('Cifras'); ?></h2>
                </div>
                <div class="widget-body">
                    <div id="show-content-conctacs-numbers"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (AuthComponent::user('role_id') == NULL): ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h2><?php echo __('Actas relacionadas'); ?></h2>
                </div>
                <div class="widget-body">
                    <div id="show-content-conctacs-users"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php
    $this->Html->script('controller/users/actions.js',  ['block' => 'AppScript']);
    if (AuthComponent::user('role_id') == Configure::read('BUSINESS_ROLE_ID') || AuthComponent::user('role_id') == Configure::read('EMPLOYEE_ROLE_ID')){
        $this->Html->scriptBlock("EDIT.listar(); ",            ['block' => 'AppScript']);
        $this->Html->scriptBlock("EDIT.listarMyContac(); ",    ['block' => 'AppScript']);
    } else {
        $this->Html->scriptBlock("EDIT.listarStatisticalNumber(); ",    ['block' => 'AppScript']);
    }
?>