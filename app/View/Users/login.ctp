<?php $this->Html->css("frontend/admin/pages/users.css", array("block" => "styles_section")); ?> 

<div class="content-login">
<?php echo $this->Html->image("frontend/website/home/bg-form-left.svg",array('class' => 'bg-form-left', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
<div class="container-fluid">
<div class="row">

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="content-out-middle">
    <div class="content-in-middle" >
    <?php echo $this->Form->create('User',array('data-parsley-validate')); ?>
            <div class="form-color-text padding-form">
            <a href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image("frontend/website/home/logo.svg", array('class' => 'img-responsive logo-login-mobile hidden-lg hidden-md hidden-sm visible-xs', 'alt' => 'Recapmeeting')); ?></a>

           <!-- <h1  style="text-align: center;" class="f-title-login"><?php echo __('INICIA SESIÓN'); ?></h1>-->
            <div class="row" >
                <div class="col-md-6" style="margin-top: 5px">
                    <a href="<?= 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online' ?>">
                        <img width="100%" src="<?php echo $this->webroot ?>img/googleLogin.png">
                    </a>
                </div>

                <div class="col-md-6" style="margin-top: 5px">
                    <a href="<?php echo 'Https://login.live.com/oauth20_authorize.srf?client_id='.Configure::read("CLIENT_ID_OUT2").'&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri='.Configure::read("CLIENT_URL_OUT2") ?>">
                         <img width="100%" src="<?php echo $this->webroot ?>img/outlookLogin.png">
                    </a>
                     
                </div>

            </div>
 <hr>

  
             <div class="visible-xs hidden-sm hidden-lg hidden-md">

            <div class="login-info">
                <p class="linea1">
                    <?php echo __('¡Toma el control!') ?>
                </p>
                <p class="linea2">
                    <?php echo __('Invita, gestiona y crea actas de tus reuniones al instante.') ?>
                </p>
            </div>
            </div>
            <div class="form-group">
                <?php echo $this->Form->label('User.email',__('Correo electrónico'), array('class'=>'control-label required'));?>
                <?php echo $this->Form->input('email',array('class' => 'form-control border-input input-transparent','label'=>false,'placeholder'=>__('Correo electrónico'), 'div' => false)); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->label('User.password',__('Contraseña'), array('class'=>'control-label  required'));?>
                <?php echo $this->Form->input('password',array('class' => 'form-control border-input input-transparent','placeholder'=>__('Contraseña'),'label' => false , 'div' => false)); ?>
            </div>

            <a style="width: 170px;" class="f-d-block mxy-10" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'remember_password')) ?>"><?php echo __('Restablecer contraseña') ?></a>

            <?php echo $this->Form->end(array('class'=>'btn btn-reecapmeeting margin-btn','label'=>__('Inicia sesión'))); ?>
            <div class="flex-start">
                <p class="pr-5 mt-20"><?php echo __('¿No tienes una cuenta?') ?></p>
                <a class="pr-5 mt-20" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'add')) ?>"><?php echo __('Regístrate') ?></a>
            </div>
            </div>
</div>
</div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hidden-xs">
<div class="content-out-middle">
    <div class="content-in-middle">
    <div class="padding-form-right">
    <div class="">
<a href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image("frontend/website/home/logo.svg", array('class' => 'img-responsive logo-login-mobile', 'alt' => 'Recapmeeting')); ?></a>
   
<p class="linea1">
        <?php echo __('¡Toma el control!') ?>
    </p>
    <p class="linea2">
        <?php echo __('Invita, gestiona y crea actas de tus reuniones al instante.') ?>
    </p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

 