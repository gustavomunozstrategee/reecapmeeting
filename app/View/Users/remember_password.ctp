<?php $this->Html->css("frontend/admin/pages/remember_password.css", array("block" => "styles_section")); ?>

<div class="row-password" id="login-box">
<?php echo $this->Html->image("frontend/website/home/bg-form.svg",array('class' => 'img-responsive bg-form-figure hidden-xs', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <div class="position-relative">
        <?php echo $this->Form->create('User', array('data-parsley-validate','novalidate' => true)); ?>
        <h1 class="text-greenlight f-title-green text-center"><?php echo __('RESTABLECER CONTRASEÑA'); ?></h1>
        <p><?php echo __('Ingresa el correo electrónico que registraste al momento de crear tu cuenta.') ?></p>
        <!-- <p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p> -->
        <div class="form-group">
            <?php echo $this->Form->label('User.email',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
            <?php echo $this->Form->input('email',array('class' => 'form-control border-input input-transparent','label'=>false,'div'=>false)); ?>
        </div>
        <div class="flex-space-around-btns position-relative">
            <?php echo $this->Form->end(array('class'=>'btn btn-reecapmeeting-user myb-10','label'=>__('Restablecer contraseña'))); ?>
            <a class="btn btn-reecapmeeting-user myb-10" href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'login')) ?>"><?php echo  __('Atrás'); ?></a>
        </div>
    </div>
</div>
 