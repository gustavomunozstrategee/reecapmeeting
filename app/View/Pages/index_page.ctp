<?php $permissionAction = $this->Utilities->check_team_permission_action(array("add"), array("meetings")); ?>
<div class="page-header">
    <div class="d-flex align-items-center">
        <h2 class="page-header-title"><?php echo __("Inicio") ?></h2>
        <div>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href=""><i class="ti ti-home"></i> <?php echo __("Inicio") ?></a></li>
            </ul>
        </div>
    </div>
</div>

<?php if(AuthComponent::user('role_id') == NULL) { ?>
<div class="row flex-row" >
    <div class="col-md-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center"></div>
            <div class="widget-body sliding-tabs">
                <ul class="nav nav-tabs" id="example-one" role="tablist">
                    <li class="nav-item col-md-6">
                        <a class="nav-link " id="base-compromisos-semana" data-toggle="tab" href="#tab-compromisos-semana" role="tab" aria-controls="tab-compromisos-semana" aria-selected="true">
                            <?php echo __('Compromisos para esta semana'); ?>
                        </a>
                    </li>
                    <li class="nav-item col-md-6">
                        <a class="nav-link active" id="base-menu-opciones" data-toggle="tab" href="#tab-menu-opciones" role="tab" aria-controls="tab-menu-opciones" aria-selected="false">
                            <?php echo __('Menú de opciones') ?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content pt-3">
                    <div class="tab-pane fade" id="tab-compromisos-semana" role="tabpanel" aria-labelledby="base-compromisos-semana">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th><?php echo __("Compromiso") ?></th>
                                        <th><?php echo __("Empresa") ?></th>
                                        <th><?php echo __("Cliente") ?></th>
                                        <th><?php echo __("Proyecto") ?></th>
                                        <th><?php echo __("Fecha") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($commitments)): ?>
                                        <?php foreach ($commitments as $key => $value): ?>
                                            <tr class="text-uppercase">
                                                <td class="td-actions">
                                                    <a href="javascript:void(0)" class="terminateCommitment" data-id="<?php echo EncryptDecrypt::encrypt($value["Commitment"]["id"]) ?>"><i class="la la-check-circle-o edit"></i></a>
                                                </td>
                                                <td>
                                                    <span class="text-primary">
                                                        <?php echo $value["Commitment"]["description"] ?>
                                                    </span>
                                                </td>
                                                <td><?php echo $value["Team"]["name"] ?></td>
                                                <td><?php echo $value["Client"]["name"] ?></td>
                                                <td><?php echo $value["Project"]["name"] ?></td>
                                                <td><?php echo $value["Commitment"]["delivery_date"] ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    <?php else: ?>
                                        <tr>
                                            <td class="text-primary text-center" colspan="6">
                                                <?php echo __("No hay compromisos por realizar para esta semana.");?>
                                            </td>
                                        </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade show active" id="tab-menu-opciones" role="tabpanel" aria-labelledby="tab-menu-opciones">
                        <div class="row">
                            <div class="list-group w-100">
                                <a class="list-group-item" href="<?php echo $this->webroot ?>commitments">
                                    <div class="media">
                                        <div class="event-date align-self-center mr-3">
                                            <i class="ion-filing text-linkedin" style="font-size: 4rem"></i>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <div class="event-title text-secondary"><?php echo __("Compromisos") ?></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="list-group w-100">
                                <a class="list-group-item" href="<?php echo $this->webroot ?>contacs">
                                    <div class="media">
                                        <div class="event-date align-self-center mr-3">
                                            <i class="ion-clipboard text-linkedin" style="font-size: 4rem"></i>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <div class="event-title text-secondary"><?php echo __("Actas") ?></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="list-group w-100">
                                <a class="list-group-item" href="<?php echo $this->webroot ?>meetings">
                                    <div class="media">
                                        <div class="event-date align-self-center mr-3">
                                            <i class="ion-calendar text-linkedin" style="font-size: 4rem"></i>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <div class="event-title text-secondary"><?php echo __('Reuniones') ?></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="list-group w-100">
                                <a class="list-group-item" href="<?php echo $this->webroot ?>teams">
                                    <div class="media">
                                        <div class="event-date align-self-center mr-3">
                                            <i class="ion-map text-linkedin" style="font-size: 4rem"></i>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <div class="event-title text-secondary"><?php echo __('Empresas') ?></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //ESTAS SON LAS OPCIONES DEL ADMINISTRADOR ?>
<?php } elseif(AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')) { ?>
<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?php echo __("Cifras en general"); ?></h2>
            </div>
            <div class="widget-body">
                <div class="row">
                    <div class="col-xl-3">
                        <div class="widget widget-16 has-shadow">
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-xl-12 d-flex justify-content-center align-items-center">
                                        <i class="la la-file-text-o la-3x"></i>
                                    </div>
                                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                                        <div class="counter"><?php echo h($proyectos); ?></div>
                                        <div class="total-views text-center mh-50"><?php echo __('Total de proyectos')?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-3">
                        <div class="widget widget-16 has-shadow">
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-xl-12 d-flex justify-content-center align-items-center">
                                        <i class="la la-users la-3x"></i>
                                    </div>
                                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                                        <div class="counter"><?php echo h($clientes); ?></div>
                                        <div class="total-views text-center mh-50"><?php echo __('Total de clientes')?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-3">
                        <div class="widget widget-16 has-shadow">
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-xl-12 d-flex justify-content-center align-items-center">
                                        <i class="la la-file-o la-3x"></i>
                                    </div>
                                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                                        <div class="counter"><?php echo h($actas); ?></div>
                                        <div class="total-views text-center mh-50"><?php echo __('Total de actas realizadas')?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="widget widget-16 has-shadow">
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-xl-12 d-flex justify-content-center align-items-center">
                                        <i class="la la-star-o la-3x"></i>
                                    </div>
                                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                                        <div class="counter"><?php echo h($tags); ?></div>
                                        <div class="total-views  text-center mh-50"><?php echo __('Total de tags')?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php $this->Html->script('controller/pages/index.js', ['block' => 'AppScript']); ?>









