<div class="backg-white">	
	<div class="row">
	<div class="col-md-12">
		  <h1 class="f-title"><?php echo __('Búsqueda avanzada'); ?> <?php echo $this->element("team_name"); ?></h1>
	</div>
	</div>

	<?php echo $this->Form->create('Search',array('role' => 'form'));?>

	<div class="row">
	<div class="col-xs-12">
		<div class="bg-filter">
		<div class="f-content-search-filter">
			<div class="form-group">
				<?php echo $this->Form->input('team_id', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false,'type' => "hidden", 'value' => EncryptDecrypt::decrypt($this->Session->read("TEAM")))); ?>
			</div>
			<div class="input-group">
			<?php if(!empty($this->Session->read("SearchAdvance"))):?>
				<?php echo $this->Form->input('frase', array('placeholder'=>__('Buscar'),'label'=>false,'div'=>false,'class'=>'form-control border-input','value' => $this->Session->read("SearchAdvance")));?>
			<?php else: ?>
				<?php echo $this->Form->input('frase', array('placeholder'=>__('Buscar'),'label'=>false,'div'=>false,'class'=>'form-control border-input','value' => !empty($this->request->data["Search"]["frase"]) ? $this->request->data["Search"]["frase"] : NULL));?>
			 <?php endif; ?>

				<div class="input-group-btn">
					<button type="submit" class="btn btn-default" id="searchDataInSession" value="searching"><span class="glyphicon glyphicon-search f-blue"></span></button>
				</div>
			</div>
			</div>
		</div>
	</div>
	</div>


	<div class="row">
		<div class="col-md-4">
			<div class="bg-filter-checkbox">
				<label><?php echo __('Buscar en').':'; ?></label>
				<?php 
					echo $this->Form->input('parametros', array('type' => 'select','options' => $listaParametros, 'multiple' => 'checkbox','class' => 'form-control','label'=>false,'required'=>true));
				?>
			</div>

			<div class='bg-filter-clear'>
				<label><?php echo __('EXCLUIR').':' ?></label>
				<?php echo $this->Form->input('excluir', array('label'=>false,'placeholder'=>__('Palabras, personas, clientes, proyectos'),'div'=>false, 'class' => 'form-control')); ?>
				<!-- proyecto cliente 2 -->
			</div>
			<div class="">
				<button class="btn btn_limpiarFiltros" type="button" id="btn_limpiarFiltros"><?php echo __('LIMPIAR FILTROS') ?></button>
			</div>
		</div>


		
		<div class="col-md-8">
			<div class="bg-filter-checkbox heightscroll__filter">
			<?php if(!empty($totalResults) || !empty($dataSearched)):?>
			<?php  if($totalResults == 1): ?>
				<div class="text-center form-group"><?php echo  $totalResults . __(" Resultado de búsqueda ") . "''" .  "{$dataSearched}"  . "''";?></div>
			<?php else: ?>
				<div class="text-center form-group"><?php echo  $totalResults . __(" Resultados de búsqueda ") . "''" .  "{$dataSearched}"  . "''";?></div>
			<?php endif; ?>
		   <?php endif; ?>
				<?php if (isset($this->request->data['Search']) && $this->request->data['Search']['parametros'] != '' && $this->request->data['Search']['frase'] != ''){ $nombreModelo = ""; ?>
					<?php $totalData = 0;?>
					<?php foreach ($this->request->data['Search']['parametros'] as $model):?>
						<?php if ($nombreModelo != $model): ?>
							<tr>
								<?php if($model == "UserTeam"): ?>
									<td colspan="2">
										<p class='f-blue font-bold'> <?php echo $this->Utilities->nameModel($model) ?> </p>
									</td>
								<?php else: ?>
									<td>
										<p class='f-blue font-bold'> <?php echo $this->Utilities->nameModel($model) ?> </p>
									</td>
								<?php endif; ?>
							</tr> 
							<?php if($model == "Contac"): ?>
			                	<?php if(!empty($results["Contac"])):?>				                 
								<div class="box">
					                <div class="box-body box-special no-padding">
					                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed table-special">
					                        <thead class="text-primary">
					                            <tr>
					                                <th class="table-grid-20"><?php echo __('Usuario'); ?></th>
					                                <th class="table-grid-10"><?php echo __('Cliente'); ?></th>
					                                <th class="table-grid-10"><?php echo __('Proyecto'); ?></th>
					                                <th class="table-grid-10"><?php echo __('Empresa'); ?></th>
					                                <th class="table-grid-10 text-center"><?php echo __('#Acta'); ?></th>
					                                <th class="table-grid-10"><?php echo __('Fecha y hora inicio'); ?></th>
					                                <th class="table-grid-10"><?php echo __('Fecha y hora fin'); ?></th>
					                                <th class="table-grid-10"><?php echo __('Estado'); ?></th> 
					                                <th class="table-grid-10"><?php echo __('Acciones'); ?></th> 
					                            </tr>
					                        </thead>
					                        <tbody> 
					                        	<?php foreach ($results["{$model}"] as $result):?>
					                        	<?php $response = $this->Utilities->permissionViewContac($result);?>
					                        	<tr>
					                                <td class="table-grid-20 td-word-wrap">
					                                <?php if(empty($result['User']['firstname'])):?>
					                                    <?php echo $result['User']['email']; ?>
					                                <?php else: ?>
					                                    <?php echo $result['User']['firstname'].' '.$result['User']['lastname']; ?>
					                                <?php endif; ?>
					                                </td>
					                                <td class="table-grid-10 td-word-wrap">
					                                    <?php echo $result['Client']['name']; ?>
					                                </td>
					                                <td class="table-grid-10 td-word-wrap">
					                                    <?php echo $result['Project']['name']; ?>
					                                </td>
					                                <td class="table-grid-10 td-word-wrap">
					                                    <?php echo $result['Team']['name']; ?>
					                                </td>

					                                <?php if($result['Contac']['state'] == Configure::read('ENABLED') || $result['Contac']['state'] == Configure::read('APPROVAL')): ?>
					                                    <td class="table-grid-10 td-word-wrap text-center"><span class="table-number"><?php echo $this->Utilities->stateContac($result['Contac']['state']); ?></span></td>
					                                <?php else: ?>
					                                    <td class="table-grid-10 td-word-wrap text-center"><span class="table-number"><?php echo h($result['Contac']['number']); ?></span></td>
					                                <?php endif; ?>

					                                <td class="table-grid-10 td-word-wrap"><?php echo h($this->Time->format('d-m-Y h:i A', $result['Contac']['start_date'])); ?>&nbsp;</td>
					                                <td class="table-grid-10 td-word-wrap"><?php echo h($this->Time->format('d-m-Y h:i A', $result['Contac']['end_date'])); ?>&nbsp;</td>
					                                <td class="table-grid-10 td-word-wrap"><span class="table-number"><?php echo $this->Utilities->stateContac($result['Contac']['state']); ?></span></td>
					                                <?php echo $this->Utilities->stateEnabledContac($result['Contac']['id'],$result['Contac']['state'],$result['Contac']['user_edit']); ?>
					                            	<td>
					                            		<?php if($response == true):?>
					                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'contacs' ,'action' => 'view', EncryptDecrypt::encrypt($result['Contac']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
					                                            <i class="fa fa-eye"></i>
					                                        </a> 
					                                    <?php endif; ?>
					                            	</td>
					                            </tr>
					                            <?php endforeach;?>  
					                        </tbody>
				                       	</table>
			                      	</div>
			                  	</div>
		                       	<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?> 
						<?php if ($model != 'Todo') { ?>
							<div>
							<p><?php if (!empty($results["{$model}"])) { ?></p>
								<?php  $totalData += count($results["{$model}"]); ?>
								<table class="table">		 
									<?php foreach ($results["{$model}"] as $result):?>									
										
										
									<?php if ($model == 'Project') { ?>
										<tr>
											<td>
												<a href="<?php echo $this->Html->url(array('controller'=>'projects','action'=>'view',EncryptDecrypt::encrypt($result["{$model}"]["id"]))); ?>">
													<?php echo ucwords($result["{$model}"]["name"]); ?>
												</a>
											</td>
										</tr>
									<?php } elseif($model == 'Client'){  ?>
										<tr>
											<td>
												<a href="<?php echo $this->Html->url(array('controller'=>'clients','action'=>'view',EncryptDecrypt::encrypt($result["{$model}"]["id"]))); ?>">
													<?php echo ucwords($result["{$model}"]["name"]); ?>
												</a>
											</td>
										</tr>
									<?php } elseif($model == 'UserTeam'){  ?> 								     
										<?php if($result["{$model}"]["position_id"] == NULL){?>
											<tr>
												<a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'view',EncryptDecrypt::encrypt($result["User"]["id"]))); ?>">
													<?php if(empty($result["User"]["firstname"])): ?>
														<td> 
															<?php echo __("Nombre no asignado por el usuario."); ?>
														</td>
													<?php else: ?>
														<td>
															<?php echo ucwords($result["User"]["name"]); ?>
														</td> 
													<?php endif; ?>
													<td>
														<?php echo ucwords($result["User"]["email"]); ?>
													</td>
												</a>
											</tr>
										<?php } else { ?>
										<tr>
											<a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'view',EncryptDecrypt::encrypt($result["User"]["id"]))); ?>">
												<?php if(empty($result["User"]["firstname"])): ?>
													<td> 
														<?php echo __("Nombre no asignado por el usuario."); ?>
													</td>
												<?php else: ?>
													<td>
														<?php echo ucwords($result["User"]["name"]); ?>
													</td> 
												<?php endif; ?>
												<td>
													<?php echo ucwords($result["User"]["email"]); ?>
												</td>
											</a>
										</tr>
										<?php } ?> 
				                            
									  
									<?php } elseif($model == 'Commitment'){  ?>
									    <tr>
										  <td>
										    <a href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'view',EncryptDecrypt::encrypt($result["{$model}"]["contac_id"]))); ?>">
											  <?php echo ucwords($result["{$model}"]["name"]); ?>
	                                        </a>
										  </td>
										</tr>
									<?php } elseif($model == 'Meeting'){  ?>
	                                      <tr>
										    <td>
											   <a class="btn-view-assistants-meeting" href="<?php echo $this->Html->url(array('controller'=>'meetings','action'=>'view',EncryptDecrypt::encrypt($result["{$model}"]["id"]))); ?>">
											      <?php echo ucwords($result["{$model}"]["subject"]); ?>
	                                           </a>
											</td>
										  </tr>
								    <?php }?>

									<?php //echo $this->Utilities->searchData($result,$model); $nombreModelo = $model;?>
									<?php endforeach;?>
								</table> 
							<?php } else { ?>
								<?php //echo $this->Utilities->nameModel($model); ?>
								  <p><?php echo __('No se encontraron resultados.'); ?></p>
								</div>
							<?php } ?>
						<?php } ?>
					<?php endforeach; ?>
				<?php } else { ?>
					<p class="text-center"><?php echo __('Aún no has buscado nada.'); ?></p>
				<?php } ?> 
		</div>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>

<?php
$this->Html->script('controller/pages/actions.js',		['block' => 'AppScript']);
$this->Html->scriptBlock("EDIT.viewSearch(); ",        	    	['block' => 'AppScript']);
?>
