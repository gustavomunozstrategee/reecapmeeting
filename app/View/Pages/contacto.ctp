<?php $this->Html->css("frontend/website/pages/contacto.css", array("block" => "styles_section")); ?>

<script src="<?php echo Configure::read('URL_SCRIPT_CAPTCHA')?>"></script>

 
<?php echo $this->element("nav_website"); ?>

<header class="bg-header_contacto">
    <div class="container position-relative">
        <h1 class="title-contacto">
            <?php echo __('CONTACTO') ?>
        </h1>
        <p class="f-white"><?php echo __('¿Tienes alguna duda?') ?></p>
        <p class="f-white"><?php echo __('Escríbenos ahora') ?></p>
        
        <!-- <div class="section-testimonios">

        <div class="slider-testimonios-contacto">
          <div class="card-testimonio">
          <?php echo $this->Html->image("frontend/website/home/logo-strategee.svg",array('class' => 'img-responsive img-empresa-testimonio', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
          <p class="info-testimonio">"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto."</p>  
          <p class="date-testimonio">Junio 11 2017</p>
          </div>
         
          <span class="btn-testimonios">
             <a href="https://www.strategee.us/" target="_blank"><i class="flaticon-add"></i></a> 
          </span>
        </div> 

        </div>  -->
    </div>
</header>

<div class="backg-white">
<?php echo $this->Form->create('control', array('url' => array('controller' => 'pages', 'action'=>'contacto'))); ?>
<script>
    localStorage.setItem("legales", "Yes");
</script>

<section class="container"> 
    <div class="content-form-contact"> 
        <div class="form-group"> 
            <?php echo $this->Form->label('Page.name_conctact',__('Nombre'), array('class'=>'control-label f-blue'));?>
            <?php echo $this->Form->input('control.name_conctact', array('class' => 'form-control border-input only-letters-field','label'=>false,'div'=>false,'maxLength' => 80)); ?>
        </div>

        <div class="form-group"> 
            <?php echo $this->Form->label('Page.country',__('País'), array('class'=>'control-label f-blue'));?>
            <?php echo $this->Form->input('control.country', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => "",'maxLength' => 80)); ?>
        </div>

        <div class="form-group"> 
            <?php echo $this->Form->label('Page.email',__('Correo electrónico'), array('class'=>'control-label f-blue'));?>
            <?php echo $this->Form->input('control.email', array('class' => 'form-control border-input','label'=>false,'div'=>false,'maxLength' => 80)); ?>
        </div>

        <div class="form-group"> 
            <?php echo $this->Form->label('Page.subject',__('Asunto'), array('class'=>'control-label f-blue'));?>
            <?php echo $this->Form->textarea('control.subject', array('class' => 'form-control border-input resize-none','label'=>false,'div'=>false,'maxLength' => 300)); ?>
        </div>

        <div class="form-group">
            <br>
                <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('RE_CAPTCHA_KEY_WEB_SITE')?>"></div>
            <br>
        </div>

        <div id="subjectCharacters"></div> 
        <div class="flex-justify-end">
        <button type="submit" class="btn btn-reecapmeeting"><?php echo __('Guardar') ?></button>
        </div>
    </div> 
</section>
</div>
<?php echo $this->element("footer_website"); ?> 


<style>
    .dropdown-backdrop{
        z-index: -1 !important;
    }
 </style>