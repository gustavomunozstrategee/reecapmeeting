<?php $this->Html->css("frontend/website/home.css", array("block" => "styles_section")); ?> 
 
<section class="bg-header">
    <?php echo $this->element('nav_home'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="contentout_verticalmiddle">
                    <div class="contentin_verticalmiddle">
                        <div class="container-infoheader">
                            <p>
                                <?php echo __('¡Toma el control!') ?>
                            </p>
                            <h1>
                                <?php echo __('Invita, gestiona y crea actas de tus reuniones al instante.') ?>
                            </h1>
                           
                            <a class="btn btn-reecapmeeting" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "login"));?>">
                                <?php echo __('¡Empieza ahora!') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Html->image("frontend/website/home/circle-header.png",array('class' => 'img-responsive mockup hidden-xs', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
        </div>
    </div>
</section>

<section id="comofunciona" class="container-section">
    <?php echo $this->Html->image("frontend/website/home/circle-bg2.png",array('class' => 'img-responsive circle-bg2  hidden-xs', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <?php echo $this->Html->image("frontend/website/home/circle2-bg2.png",array('class' => 'img-responsive circle2-bg2  hidden-xs hidden-sm', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                <div class="contentout_verticalmiddle">
                    <div class="contentin_verticalmiddle">
                        <div class="container-info">
                            <h3 class="subtitle">
                                <?php echo __('Olvídate de las')?><br>
                                <?php echo __('notas de papel')?>
                            </h3>
                            <div class="lineborder-bottom"></div>
                            <p>
                                <?php echo __('RecapMeeting te ofrece un potente sistema de seguimiento de los compromisos de tus reuniones, ya no tendrás que cargar con tus hojas de apuntes para tener presente los temas que se trataron en la última reunión de tu empresa o tus clientes, deja que RecapMeeting lo haga por ti.')?>
                            </p>
                            <p class="text-two">
                                <?php echo __('Recibirás las actas de tus reuniones y los recordatorios de tus compromisos en tu correo electrónico.')?>
                            </p>
                            <a class="btn btn-reecapmeeting-line" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "add"));?>">
                                <?php echo __('¡Crea tu cuenta ahora!') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Nueva sección -->
<section class="container-section hidden-xs" id="funciona">
    <?php echo $this->Html->image("frontend/website/home/curve-middle.png",array('class' => 'img-responsive curve-middletop', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <div class="bg3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="text-center text-white margin-top-4em font-size-36">
                        <?php echo __('Simplicidad y potencia en una sola') ?><br class="hidden-xs">
                        <?php echo __('herramienta, así lo hacemos:') ?>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <nav class="StepByStep">
                        <ul>
                            <a href="javascript:void(0)" class="link-steps activado" data-class="Step1">
                                <li>
                                    <?php echo __('REGISTRA TU')?><br class="hidden-xs">
                                    <?php echo __('EMPRESA')?><i class="flaticon-next hidden-xs"></i>
                                </li>
                            </a>
                            <a href="javascript:void(0)" class="link-steps " data-class="Step2">
                                <li>
                                    <?php echo __('AGREGA TUS')?><br class="hidden-xs">
                                    <?php echo __('PARTICIPANTES')?><i class="flaticon-next hidden-xs"></i>
                                </li>
                            </a>
                            <a href="javascript:void(0)" class="link-steps " data-class="Step3">
                                <li>
                                    <?php echo __('AGENDA UNA')?><br class="hidden-xs">
                                    <?php echo __('REUNIÓN')?><i class="flaticon-next hidden-xs"></i>
                                </li>
                            </a>
                            <a href="javascript:void(0)" class="link-steps " data-class="Step4">
                                <li>
                                    <?php echo __('DILIGENCIA')?><br class="hidden-xs">
                                    <?php echo __('UN ACTA')?><i class="flaticon-next hidden-xs"></i>
                                </li>
                            </a>
                            <a href="javascript:void(0)" class="link-steps " data-class="Step5">
                                <li>
                                    <?php echo __('GESTIONA TUS')?><br class="hidden-xs">
                                    <?php echo __('COMPROMISOS')?><i class="flaticon-next hidden-xs"></i>
                                </li>
                            </a>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php if($this->Session->read("Config.language") == "esp"): ?>
                        <div class="element-step Step1"> 
                            <?php echo $this->Html->image("frontend/website/home/step/step1.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 1', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step2">
                            <?php echo $this->Html->image("frontend/website/home/step/step2.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 2', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step3">
                            <?php echo $this->Html->image("frontend/website/home/step/step3.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 3', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step4">
                            <?php echo $this->Html->image("frontend/website/home/step/step4.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 4', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step5">
                            <?php echo $this->Html->image("frontend/website/home/step/step5.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 5', 'title' => 'Recapmeeting'));?>
                        </div>
                    <?php else: ?>
                        <div class="element-step Step1"> 
                            <?php echo $this->Html->image("frontend/website/home/step/step1-ingles.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 1', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step2">
                            <?php echo $this->Html->image("frontend/website/home/step/step2-ingles.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 2', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step3">
                            <?php echo $this->Html->image("frontend/website/home/step/step3-ingles.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 3', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step4">
                            <?php echo $this->Html->image("frontend/website/home/step/step4-ingles.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 4', 'title' => 'Recapmeeting'));?>
                        </div>
                        <div class="element-step Step5">
                            <?php echo $this->Html->image("frontend/website/home/step/step5-ingles.svg",array('class' => 'img-responsive img-step hidden-xs','alt' => 'Paso 5', 'title' => 'Recapmeeting'));?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Html->image("frontend/website/home/curve-middle-bottom.png",array('class' => 'img-responsive curve-middlebottom','alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
</section>



<section class="container-section">
    <?php echo $this->Html->image("frontend/website/home/circle-bg2.png",array('class' => 'img-responsive circle-bg2  hidden-xs','alt' => 'Recapeeting', 'title' => 'Recapmeeting'));?>
    <?php if($this->Session->read("Config.language") == "esp"): ?>
        <?php echo $this->Html->image("frontend/website/home/mockup-pc.svg",array('class' => 'img-responsive mockup-pc hidden-xs hidden-sm','alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <?php else: ?>
        <?php echo $this->Html->image("frontend/website/home/mockup-pc-ingles.svg",array('class' => 'img-responsive mockup-pc hidden-xs hidden-sm','alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <?php endif; ?>
    <div class="container" id="controltotal">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                <div class="contentout_verticalmiddle">
                    <div class="contentin_verticalmiddle">
                        <div  class="container-info">
                            <h3 class="subtitle title">
                                <?php echo __('Protege tu información')?><br>
                                <?php echo __('y los compromisos de la')?><br>
                                <?php echo __('reunión')?>
                            </h3>
                            <div class="lineborder-bottom"></div>
                            <p>
                                <?php echo __("Queremos que tengas una visión 360° de tu empresa, cada reunión, cada acta o informe y cada compromiso será gestionado desde RecapMeeting de manera ágil, segura y confiable.")?>
                            </p>
                            <p class="text-two">
                                <?php echo __('La información de tu empresa estará disponible 24/7 para que puedas acceder a ella desde y cuando desees.')?>
                            </p>
                            <a class="btn btn-reecapmeeting-line" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "login"));?>">
                                <?php echo __('¡Empieza ahora!') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="container-section">
    <?php echo $this->Html->image("frontend/website/home/curve-top.png",array('class' => 'img-responsive curve-topbg3','alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
    <div id="porquedejarelpapel" class="bg-icons">
        <div class="contentout_verticalmiddle">
            <div class="contentin_verticalmiddle">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="text-center text-gray margin-top-4em font-size-36 font-bold">
                                <?php echo __('Tenemos todo lo que necesitas,')?><br class="hidden-xs">
                                <?php echo __('todo en un solo lugar')?>
                            </h3>
                            <div class="borderbottom-title"></div>
                            <p class="text-center">
                                <?php echo __('Funcionalidades complementarias para hacer de RecapMeeting el mejor aliado de tu compañía.')?>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="NavIcons">
                                <ul>   
                                    <a href="javascript:void(0)" data-class="list-agenda" class="evento-hover">
                                        <li><i class="flaticon-clock" aria-hidden="true"></i></li>
                                    </a>
                                    <a href="javascript:void(0)" data-class="list-folder" class="evento-hover">
                                        <li><i class="flaticon-folder" aria-hidden="true"></i></li>
                                    </a>   
                                    <a href="javascript:void(0)" data-class="list-chat" class="evento-hover">
                                        <li><i class="flaticon-bell" aria-hidden="true"></i></li>
                                    </a>
                                    <a href="javascript:void(0)" data-class="list-search" class="evento-hover">
                                        <li><i class="flaticon-search-2" aria-hidden="true"></i></li>
                                    </a>         
                                    <a href="javascript:void(0)" data-class="list-sharing_archives" class="evento-hover">
                                        <li><i class="flaticon-monitor" aria-hidden="true"></i></li>
                                    </a>
                                    <a href="javascript:void(0)" data-class="list-chatting" class="evento-hover">
                                        <li><i class="flaticon-world" aria-hidden="true"></i></li>
                                    </a>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                                    <div class="content-ittems-hover">
                                        

                                        <div class="contenedores list-agenda">
                                            <div class="row-functions">
                                                <div class="item-functions hidden-xs">
                                                    <?php echo $this->Html->image("frontend/website/home/agenda.svg",array('class' => 'img-responsive','alt' => 'Agenda', 'title' => 'Sincroniza tus reuniones con tus calendarios en otras plataformas'));?>
                                                </div>
                                                <div class="item-functions">
                                                    <h3>
                                                        <?php echo __('Agenda')?>
                                                    </h3>
                                                    <p>
                                                        <?php echo __('Sincroniza tus reuniones con tus calendarios en otras plataformas.')?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="contenedores activate list-search">                       
                                            <div class="row-functions">
                                                <div class="item-functions hidden-xs">
                                                    <?php echo $this->Html->image("frontend/website/home/search.svg",array('class' => 'img-responsive','alt' => 'Búsqueda avanzada', 'title' => 'Encuentra de manera ágil tus compromisos, reuniones y participantes'));?>
                                                </div>
                                                <div class="item-functions">
                                                    <h3>
                                                        <?php echo __('Búsqueda avanzada')?>
                                                    </h3>
                                                    <p>
                                                        <?php echo __('Encuentra de manera ágil tus compromisos, reuniones y participantes.')?>
                                                    </p>
                                                </div>
                                            </div>   
                                        </div>

                                        <div class="contenedores list-chatting">
                                            <div class="row-functions">
                                                <div class="item-functions hidden-xs">
                                                    <?php echo $this->Html->image("frontend/website/home/online.svg",array('class' => 'img-responsive', 'alt' => 'Siempre en línea', 'title' => 'Accede a RecapMeeting desde cualquier lugar y con cualquier dispositivo'));?>
                                                </div>
                                                <div class="item-functions">
                                                    <h3>
                                                        <?php echo __('Siempre en línea')?>
                                                    </h3>
                                                    <p>
                                                        <?php echo __('Accede a RecapMeeting desde cualquier lugar y con cualquier dispositivo.')?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="contenedores list-chat">
                                            <div class="row-functions">
                                                <div class="item-functions hidden-xs">
                                                    <?php echo $this->Html->image("frontend/website/home/notifications.svg",array('class' => 'img-responsive', 'alt' => 'Alertas y notificaciones', 'title' => 'Recibe al instante alertas de actividad de los usuarios de tu organización'));?>
                                                </div>
                                                <div class="item-functions">
                                                    <h3>
                                                        <?php echo __('Alertas y notificaciones')?>
                                                    </h3>
                                                    <p>
                                                        <?php echo __('Recibe al instante alertas de actividad de los usuarios de tu organización.')?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="contenedores list-folder">
                                            <div class="row-functions">
                                                <div class="item-functions hidden-xs">
                                                    <?php echo $this->Html->image("frontend/website/home/seguridad.svg",array('class' => 'img-responsive', 'alt' => 'Seguridad', 'title' => 'Agenda reuniones y actas privadas con tu empresa'));?>
                                                </div>
                                                <div class="item-functions">
                                                    <h3>
                                                        <?php echo __('Seguridad')?>
                                                    </h3>
                                                    <p>
                                                        <?php echo __('Agenda reuniones y actas privadas con tu empresa.')?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="contenedores list-sharing_archives">
                                            <div class="row-functions">
                                                <div class="item-functions hidden-xs">
                                                    <?php echo $this->Html->image("frontend/website/home/actas.svg",array('class' => 'img-responsive', 'alt' => 'Actas al instante', 'title' => 'Recibe de inmediato el acta de tus reuniones vía email'));?>
                                                </div>
                                                <div class="item-functions">
                                                    <h3>
                                                        <?php echo __('Actas al instante')?>
                                                    </h3>
                                                    <p>
                                                        <?php echo __('Recibe de inmediato el acta de tus reuniones vía email.')?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Html->image("frontend/website/home/curve-bottom.png",array('class' => 'img-responsive curve-bottombg3', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
</section>

<section id="paraquienesutil">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="text-center subtitle">
                    <?php echo __('Empieza a usar RecapMeeting ahora')?>
                </h3>
                <div class="borderbottom-title"></div>
                <p class="text-center">
                    <?php echo __('Registrate hoy, configura tu cuenta en unos sencillos pasos, no necesitas ninguna instalación.')?>
                </p>
                <p class="text-center">
                    <a class="btn btn-reecapmeeting" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "login"));?>">
                        <?php echo __('Regístrate') ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>


<div class="container-section">
    <?php echo $this->Html->image("frontend/website/home/curve-top.png",array('class' => 'img-responsive curve-topbg3','alt' => 'Recapmeeting','title' => 'Recapmeeting'));?>
    <section id="quieromasinformacion" class="bg-footer">

    <div class="container-section flex-center">
       <p><?php echo __('¡Trabaja en equipo!')?></p>
       <h3 class="font-size-36 font-bold text-center">
            <?php echo __('LA MEJOR MANERA DE HACER')?><br>
            <?php echo __('SEGUIMIENTO EFECTIVO')?>
        </h3>
        <h2>
            <?php echo __('A LOS COMPROMISOS DE TUS REUNIONES')?>
        </h2>
        <a class="btn btn-reecapmeeting" href="<?php echo $this->Html->url(array("controller" => "users", "action" => "login"));?>">
            <?php echo __('¡Empieza ahora!') ?>
        </a>
    </div>

    <div class="section-help">
    <div class="flex-space-around">
            <div>
                <p class="subtitle"><?php echo __('¿Aún tienes alguna duda?') ?></p>
                <p class="f-blue hidden-xs"><?php echo __('Nuestro equipo está disponible para ayudarte.') ?></p>
            </div>

            <div>
              <a class="btn btn-white btn-help" href="<?php echo $this->Html->url(array("action" => "contacto"));?>"><?php echo __('Formular una inquietud')?></a>
            </div>
    </div>
  </div>
  
</section>
    <?php echo $this->Html->image("frontend/website/home/curve-bottom.png",array('class' => 'img-responsive curve-bottombg3 hidden-sm hidden-xs', 'alt' => 'Recapmeeting', 'title' => 'Recapmeeting'));?>
</div>

<?php echo $this->element("footer_website"); ?> 


<!-- Modal -->
<div class="modal fade" id="buyPlans" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-recapmeeting" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Planes') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12">
                      <p><?php echo __('Diligencia los campos de este formulario para obtener más información acerca de los beneficios de nuestros planes.') ?></p>
                  </div>
                </div>
                <form id="form_plan">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('Nombre') ?>*</label>
                                <input type="text" class="form-control  border-input only-letters-field" id="" name="data[control][name_conctact]" maxlength="80">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('Apellido') ?>*</label>
                                <input type="text" class="form-control border-input only-letters-field" id="" name="data[control][lastname]" maxlength="80">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('Correo electrónico') ?>*</label>
                                <input type="email" class="form-control  border-input" id="" name="data[control][email]" maxlength="80">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('Teléfono') ?>*</label>
                                <input type="text" class="form-control border-input only-numbers-field" id="" maxlength="10" name="data[control][phone]">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('País') ?>*</label>
                                <input type="text" class="form-control border-input" id="" name="data[control][country]" maxlength="80">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('Ciudad') ?>*</label>
                                <input type="text" class="form-control border-input" id="" name="data[control][city]" maxlength="80">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('Empresa') ?>*</label>
                                <input type="text" class="form-control border-input" id="" name="data[control][business]" maxlength="80">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="" class="f-blue"><?php echo __('Cantidad de usuarios') ?>*</label>
                                <input type="text" class="form-control border-input only-numbers-field" id="" name="data[control][number_user]" maxlength="6">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-modal-recapmeeting" id="send_information_plan_solicited"><?php echo __('Solicitar información') ?></button>
            </div>
        </div>
    </div>
</div>
<div class="active-modal-float">
    <?php if($this->Session->read("Config.language") == "esp"):?>
        
            <a href="javascript:void(0)" data-url="<?php echo Configure::read("URL_YOUTUBE_ESP")?>"  class="active-modal-fake"><i class="fas fa-play"></i></a>
    
    <?php else: ?>
        
            <a href="javascript:void(0)" data-url="<?php echo Configure::read("URL_YOUTUBE_ENG")?>"  class="active-modal-fake"><i class="fas fa-play"></i></a>
        
    <?php endif; ?>
</div>
    <div class="modal-fake">
        <div class="modal-fake-content">        
            <div id="player">

            </div>
        </div>  
    </div>
    <script>
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '360',
            width: '100%',
            videoId: '',
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPlayerReady(event) {
        event.target.playVideo();
    }

    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 60000);
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }

</script>
    <?php $this->Html->script('frontend/website/video.js', array('block'=>'videoScript'));?> 
<script>
    localStorage.setItem("legales", "no");
</script>




<style>
    .dropdown-backdrop{
        z-index: -1 !important;
    }
 </style>