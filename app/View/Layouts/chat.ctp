<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Chat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/') ?>favicon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/') ?>favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/') ?>favicon.png">
    <?php 
      echo $this->Html->css('bootstrap.3.3.7/css/bootstrap.min.css');
      echo $this->Html->css('font-awesome-4.7.0/css/font-awesome.min.css');
      echo $this->Html->script('/assets/vendors/js/base/jquery.min.js');
      echo $this->Html->script('bootstrap.3.3.7/js/bootstrap.min.js');
      echo $this->Html->css('chat.css'); 
    ?>
</head>
<body>
  <?php echo $this->Flash->render(); ?>
  <?php echo $this->fetch('content'); ?>
  <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(function(){
      $(".heading-compose").click(function() {
        $(".side-two").css({
          "left": "0"
        });
      });

      $(".newMessage-back").click(function() {
        $(".side-two").css({
          "left": "-100%"
        });
      });
    })
  </script>
</body>
</html>