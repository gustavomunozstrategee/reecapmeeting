<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?></title>
  <meta name="title" content="<?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?>">
  <meta name="description" content="<?php echo Configure::read("Application.name")?> <?php echo __("potente sistema de seguimiento de los compromisos de tus reuniones y actas.")?>">
  <meta name="keywords" content="<?php echo __("Reuniones, compromisos, actas, seguimiento, recordatorios, empresas, notificaciones, correo electrónico")?>">
  <meta name="author" content="Strategee">   
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,700" rel="stylesheet">
  <link rel="icon" type="image/png" href="<?php echo $this->Html->url('/') ?>favicon.png" />
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->Html->url('/') ?>favicon.ico" />
  <?php 
    echo $this->Html->css('bootstrap.3.3.7/css/bootstrap.min.css');
    echo $this->Html->css('lib/font-awesome.min.css');
    echo $this->Html->css('lib/flaticon.css');
    echo $this->Html->css('lib/parsley.css');
    echo $this->Html->css('lib/sweetalert.css');
    echo $this->Html->css('bootstrap.3.3.7/datetimepicker/css/bootstrap-datetimepicker.min.css');
    echo $this->Html->css('lib/dropzone.min.css');
    echo $this->Html->css('lib/bootstrap-tagsinput.css');
    echo $this->Html->css('lib/select2.min.css');
    echo $this->Html->css('lib/datepicker.css');
    echo $this->Html->css("Magnific-popup/magnific-popup.css"); 
    echo $this->fetch('AppCss');
    echo $this->fetch('styles_section');
    echo $this->Html->css('sass/main.css'); 
    echo $this->Html->css('frontend/admin/layout.css');
    echo $this->Html->css('frontend/admin/components/step.css');
    echo $this->Html->css('frontend/admin/components/table.css');
    echo $this->Html->css('frontend/admin/components/btnsearch.css'); 
    echo $this->Html->css('frontend/admin/components/btnsearch.css');
    echo $this->Html->css('frontend/admin/components/buttons.css');
    echo $this->Html->css('frontend/admin/components/paginator.css'); 
    echo $this->Html->css('frontend/admin/components/colors.css');         
    echo $this->Html->css('frontend/admin/components/modals.css');  
    echo $this->Html->css('frontend/admin/components/checkbox.css');  
    echo $this->Html->css('frontend/admin/components/grid.css'); 
    echo $this->Html->css('frontend/admin/components/scroll.css');
    echo $this->Html->css('frontend/admin/components/form.css');  
    echo $this->Html->css('lib/slick.css');
    echo $this->Html->css('lib/slick-theme.css');
    echo $this->Html->css('frontend/admin/components/slider.css');   
    echo $this->Html->css('frontend/admin/components/cards.css');
    echo $this->Html->css('frontend/admin/components/padding.css');  
    echo $this->Html->css('frontend/admin/components/margin.css');
    echo $this->Html->css('frontend/admin/nav-left.css'); 
    echo $this->Html->css('frontend/admin/components/alert.css');    
  ?>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">

</head>

<body>
 <?php
 if(AuthComponent::user('id')){
  echo $this->element('nav');
}      
?> 
<?php if(AuthComponent::user('id')): ?>
  <div class="wrapper row-offcanvas row-offcanvas-left">
    <?php echo $this->element('nav_left'); ?>

    <div class="content-body">                
      <section class="content-info">
        <?php 
        echo $this->Flash->render();
        echo $this->fetch('content'); 
              echo $this->element('sql_dump'); // SQL
              ?>
            </section>
          </div>

        </div>
      <?php else: ?>
        <?php
          echo $this->Flash->render();
          echo $this->fetch('content');
        ?>
      <?php endif; ?>
      <div class="alertConfirmDato"></div>
     
      <?php echo $this->element('modals');?>


      <?php if(!empty($tasks)):?>
      <?php $response = $this->Utilities->showButtonTask(); ?>
        <?php if($response == true): ?>
        <div class="container-btn-help">
            <div class="position-relative">
                <div class="container-btn-help-pointer"></div>
                <a href="javascript:void(0)" id="btn-help" class="btn-help-step"> <i class="flaticon-info"></i></a>
            </div>
            <div class="nav-info-step">
                <a href="<?php echo $tasks["url"]; ?>"><?php echo $tasks["message"]?></a>
            </div> 
        </div>
        <?php endif; ?>
      <?php endif; ?>

    
      <?php if(!isset($_COOKIE["cookieAccept".AuthComponent::user("id")]) && AuthComponent::user()):?>

        <div class="content-modal-accept_privacity">
          <div class="modal-accept_privacity" id="modal_privacity">
            <div class="flex-space-between">
              <div class="modal-body">
                <p>
                  <?php echo __('Para continuar con la navegación de nuestra página web aceptas y autorizas expresamente el uso de') ?>&nbsp;<a href="<?php echo configure::read("LEGAL_COOKIES")?>" class="f-link-privacity" target="_blank" ><?php echo __('cookies') ?></a> <?php echo __('de acuerdo a nuestra') ?> <a class="f-link-privacity" href="<?php echo configure::read("LEGAL_PRIVACITY")?>" target="_blank"><?php echo __('política de privacidad.') ?></a>
                </p>
              </div>
              <button class="btn-modal-accept_privacity" type="button" id="accept_privacity"><?php echo __('Aceptar') ?><i class="flaticon-checked"></i></button>
            </div>
          </div>
        </div>
        <!-- End Modal -->
      <?php endif; ?>

      <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
      <?php
      echo $this->element('copys_js');
      echo $this->element('global_variables');
      echo $this->fetch('GlobalScript');
      echo $this->Html->script('lib/sweetalert.min.js');
      echo $this->Html->script('lib/jquery.1.12.4.min.js');
      echo $this->Html->script('lib/jquery-ui.min.js');
      echo $this->Html->script('lib/Magnific-Popup/jquery.magnific-popup.js'); 
      echo $this->Html->script('lib/app.js');
      echo $this->Html->script('bootstrap.3.3.7/js/bootstrap.min.js');
      echo $this->Html->script('frontend/admin/scripts.js'); 
      echo $this->Html->script('frontend/admin/slick.js'); 
      echo $this->Html->script('frontend/admin/nav-left.js');
      echo $this->Html->script('lib/parsley/parsley.min.js'); 
      if ($this->request->action == 'add' || $this->request->action == 'edit' || $this->request->controller == 'users') {
        if ($this->Session->read('Config.language') == 'esp') {
            echo $this->Html->script('lib/parsley/i18n/es.js');
        } else {
            echo $this->Html->script('lib/parsley/i18n/en.js');
        }
      } 
      echo $this->fetch('AppScript');
      if ($this->request->action == 'index' || $this->request->action == 'index_page' && $this->request->controller != 'contacs') {
        echo $this->Html->script('controller/index.js');
      }
      ?>
      <?php 
      echo $this->Html->script('lib/config-farebase.js'); //escuchador 
      $user_id          = AuthComponent::user("id");
      $cachePermission  = Cache::read("permissions_{$user_id}", 'PERMISSIONS');
      $userHaveLicence  = (!empty($cachePermission['UserTeam'])) ? true : 0;

      // $this->log(Cache::read("permissions_{$user_id}", 'PERMISSIONS'),'debug');
      echo $this->Html->scriptBlock("USER_HAVE_LICENCE = {$userHaveLicence}"); 
      echo $this->Html->script('controller/transactions/index.js'); 
      ?>
      <?php echo $this->Html->script('lib/multiselect.js'); ?>
      <?php echo $this->Html->script('frontend/ad_banner'); ?> 
      <?php echo $this->Html->script('team'); ?> 
      <script type="text/javascript">
        if (document.getElementById("ad_banner") != undefined) {
          setTimeout(function(){$("div#publicidadADB").hide(); }, 50);
        } else {
          setTimeout(function(){$("div#publicidadADB").show();}, 3000);
        }
      </script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126422241-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126422241-1');
      </script>
    </body>
    </html>

