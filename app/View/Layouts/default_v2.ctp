<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?></title>
        <meta name="title" content="<?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?>">
        <meta name="description" content="<?php echo Configure::read("Application.name")?> <?php echo __("potente sistema de seguimiento de los compromisos de tus reuniones y actas.")?>">
        <meta name="keywords" content="<?php echo __("Reuniones, compromisos, actas, seguimiento, recordatorios, empresas, notificaciones, correo electrónico")?>">
        <meta name="author" content="Strategee">   
        
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/') ?>favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/') ?>favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/') ?>favicon.png">
        <!-- Stylesheet -->
        <?php
            echo $this->Html->css('/assets/vendors/css/base/bootstrap.min.css');
            echo $this->Html->css('/assets/vendors/css/base/elisyam-1.5.min.css');
            echo $this->Html->css('/assets/css/animate/animate.min.css');
            echo $this->Html->css('/assets/css/owl-carousel/owl.carousel.min.css');
            echo $this->Html->css('/assets/css/owl-carousel/owl.theme.min.css');
            echo $this->Html->css('/assets/css/leaflet/leaflet.min.css');
            echo $this->Html->script('/assets/vendors/js/base/jquery.min.js');
             echo $this->Html->script('assets/css/bootstrap-select/bootstrap-select.min.css');
             
           
            
         ?>

         <?php 
  echo $this->Html->css('bootstrap.3.3.7/css/bootstrap.min.css');
  echo $this->Html->css('lib/font-awesome.min.css');
  echo $this->Html->css('lib/flaticon.css');
  echo $this->Html->css('lib/parsley.css');
  echo $this->Html->css('lib/sweetalert.css');
  echo $this->Html->css('bootstrap.3.3.7/datetimepicker/css/bootstrap-datetimepicker.min.css');
  echo $this->Html->css('lib/dropzone.min.css');
  echo $this->Html->css('lib/bootstrap-tagsinput.css');
  echo $this->Html->css('lib/select2.min.css');
  echo $this->Html->css('lib/datepicker.css');
  echo $this->Html->css("Magnific-popup/magnific-popup.css"); 
  echo $this->fetch('AppCss');
  echo $this->fetch('styles_section');
  echo $this->Html->css('sass/main.css'); 
  echo $this->Html->css('frontend/admin/layout.css');
  echo $this->Html->css('frontend/admin/components/step.css');
  //echo $this->Html->css('frontend/admin/components/table.css');
  echo $this->Html->css('frontend/admin/components/btnsearch.css'); 
  echo $this->Html->css('frontend/admin/components/btnsearch.css');
  echo $this->Html->css('frontend/admin/components/buttons.css');
  echo $this->Html->css('frontend/admin/components/paginator.css'); 
  echo $this->Html->css('frontend/admin/components/colors.css');         
  echo $this->Html->css('frontend/admin/components/modals.css');  
  echo $this->Html->css('frontend/admin/components/checkbox.css');  
  echo $this->Html->css('frontend/admin/components/grid.css'); 
  echo $this->Html->css('frontend/admin/components/scroll.css');
  echo $this->Html->css('frontend/admin/components/form.css');  
  echo $this->Html->css('lib/slick.css');
  echo $this->Html->css('lib/slick-theme.css');
  echo $this->Html->css('frontend/admin/components/slider.css');   
  echo $this->Html->css('frontend/admin/components/cards.css');
  echo $this->Html->css('frontend/admin/components/padding.css');  
  echo $this->Html->css('frontend/admin/components/margin.css');
  echo $this->Html->css('frontend/admin/nav-left.css'); 
  echo $this->Html->css('frontend/admin/components/alert.css');    
  ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">


        
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body id="page-top">
        <!-- Begin Preloader 
        <div id="preloader">
            <div class="canvas">
                <img src="<?php echo $this->webroot ?>/img/logo.png" alt="logo" class="loader-logo">
                <div class="spinner"></div>   
            </div>
        </div>
        <!-- End Preloader -->
        <div class="page bg-2 rounded-widget">
            <!-- Begin Header -->
            <?php echo $this->element('nav_templatev2'); ?>
            <!-- End Header -->
            <!-- Begin Page Content -->
            <div class="page-content d-flex align-items-stretch">
                 <?php echo $this->element('nav_left_templatev2'); ?>
                 
                <!-- End Left Sidebar -->
                <div class="content-inner">
                    <div class="container-fluid">
                        <?php echo $this->fetch('content'); ?>
                    </div>
                    <div class="alertConfirmDato"></div>
                    <?php echo $this->element('modals');?>
                    <!-- End Container -->
                    <!-- Begin Page Footer-->
                     
                    <!-- End Page Footer -->
                    
                     
                </div>
                <!-- End Content -->
            </div>
            <!-- End Page Content -->
        </div>
         
        <!-- Begin Vendor Js -->
        <?php
          
            echo $this->Html->script('/assets/vendors/js/base/core.min.js');
        ?>
        <!-- End Vendor Js -->
        <!-- Begin Page Vendor Js -->
        <?php
            echo $this->Html->script('/assets/vendors/js/nicescroll/nicescroll.min.js');
            echo $this->Html->script('/assets/vendors/js/noty/noty.min.js');
            echo $this->Html->script('/assets/vendors/js/chart/chart.min.js');
            echo $this->Html->script('/assets/vendors/js/owl-carousel/owl.carousel.min.js');
            echo $this->Html->script('/assets/vendors/js/calendar/moment.min.js');
            echo $this->Html->script('/assets/vendors/js/calendar/fullcalendar.min.js');
            echo $this->Html->script('/assets/vendors/js/leaflet/leaflet.min.js');

            
             

            echo $this->Html->script('/assets/vendors/js/app/app.min.js');

        
            
        ?>


        <!-- End Vendor Js -->
        <!-- Begin Page Vendor Js -->
        
        
        
        <!-- End Page Vendor Js -->
        <!-- Begin Page Snippets -->
        <?php 
          //echo $this->Html->script('/assets/js/dashboard/db-clean.min.js');
        ?>
        <!-- End Page Snippets -->


        
       <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
      <?php
      echo $this->element('copys_js');
      echo $this->element('global_variables');
      echo $this->fetch('GlobalScript');
      echo $this->Html->script('lib/sweetalert.min.js');
      //echo $this->Html->script('lib/jquery.1.12.4.min.js');
      echo $this->Html->script('lib/jquery-ui.min.js');
      echo $this->Html->script('lib/Magnific-Popup/jquery.magnific-popup.js'); 
      echo $this->Html->script('lib/app.js');
     
      echo $this->Html->script('frontend/admin/scripts.js'); 
      echo $this->Html->script('frontend/admin/slick.js'); 
      echo $this->Html->script('frontend/admin/nav-left.js');
      echo $this->Html->script('lib/parsley/parsley.min.js'); 
      if ($this->request->action == 'add' || $this->request->action == 'edit' || $this->request->controller == 'users') {
        if ($this->Session->read('Config.language') == 'esp') {
            echo $this->Html->script('lib/parsley/i18n/es.js');
        } else {
            echo $this->Html->script('lib/parsley/i18n/en.js');
        }
      } 
      echo $this->fetch('AppScript');
      if ($this->request->action == 'index' || $this->request->action == 'index_page' && $this->request->controller != 'contacs') {
        echo $this->Html->script('controller/index.js');
      }
      ?>
      <?php 
      echo $this->Html->script('lib/config-farebase.js'); //escuchador 
      $user_id          = AuthComponent::user("id");
      $cachePermission  = Cache::read("permissions_{$user_id}", 'PERMISSIONS');
      $userHaveLicence  = (!empty($cachePermission['UserTeam'])) ? true : 0;

      // $this->log(Cache::read("permissions_{$user_id}", 'PERMISSIONS'),'debug');
      echo $this->Html->scriptBlock("USER_HAVE_LICENCE = {$userHaveLicence}"); 
      echo $this->Html->script('controller/transactions/index.js'); 
      ?>
      <?php echo $this->Html->script('lib/multiselect.js'); ?>
       
      <?php echo $this->Html->script('team'); ?> 
      <script type="text/javascript">
        if (document.getElementById("ad_banner") != undefined) {
          setTimeout(function(){$("div#publicidadADB").hide(); }, 50);
        } else {
          setTimeout(function(){$("div#publicidadADB").show();}, 3000);
        }
      </script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126422241-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126422241-1');
      </script>
       
    </body>
</html>