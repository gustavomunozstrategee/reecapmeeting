<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?></title>
  	<meta name="title" content="<?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?>">
  	<meta name="description" content="<?php echo Configure::read("Application.name")?> <?php echo __("potente sistema de seguimiento de los compromisos de tus reuniones y actas.")?>">
  	<meta name="keywords" content="<?php echo __("Reuniones, compromisos, actas, seguimiento, recordatorios, empresas, notificaciones, correo electrónico")?>">
    <meta name="author" content="Strategee">   
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" type="image/png" href="<?php echo $this->Html->url('/') ?>favicon.png" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->Html->url('/') ?>favicon.ico" />
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<?php echo $this->Flash->render(); ?>
	<?php 
		echo $this->element('global_variables');	
		echo $this->fetch('GlobalScript');
		echo $this->Html->css('bootstrap.3.3.7/css/bootstrap.min.css');
		echo $this->Html->css('lib/sweetalert.css');
		echo $this->Html->css('lib/font-awesome.min.css');
		echo $this->Html->css('lib/flaticon.css');
		echo $this->Html->css('lib/slick.css');
		echo $this->Html->css('lib/noty.css');
		echo $this->Html->css('lib/slick-theme.css');
		echo $this->Html->css('frontend/website/base.css');
		echo $this->Html->css('frontend/website/elements/grid.css');
		echo $this->Html->css('frontend/admin/components/alert.css');         
		echo $this->Html->css('frontend/admin/components/modals.css');  
		echo $this->fetch('styles_section');
	?>
</head>
<body>

	<?php echo $this->fetch('content'); ?>
	<?php 
		echo $this->Html->script('lib/sweetalert.min.js');
		echo $this->Html->script('lib/jquery.1.12.4.min.js');
		echo $this->Html->script('bootstrap.3.3.7/js/bootstrap.min.js');
	    echo $this->Html->script('lib/jquery.geocomplete.js', array("block" => "scripts_section")); 
        echo $this->Html->script('https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyBbvlqcV_fMowHkFITyk9wuukAHSjhOZ0g',  array("block" => "scripts_section"));  
		echo $this->Html->script('frontend/admin/slick.js'); 
		echo $this->Html->script('frontend/website/home.js'); 		
		echo $this->Html->script('frontend/website/nav-home-mobile.js');
		echo $this->Html->script('frontend/website/nav-website-mobile.js');
		echo $this->fetch('scripts_section');
        echo $this->element('copys_js');
        echo $this->Html->script('controller/transactions/index.js'); 
        echo $this->fetch('GlobalScript');
        echo $this->fetch('videoScript');
        echo $this->Html->script('/assets/vendors/js/noty/noty.min.js');
     	echo $this->Html->script('lib/Magnific-Popup/jquery.magnific-popup.js'); 
        echo $this->Html->script('lib/app.js');
        echo $this->Html->script('lib/multiselect.js');
        echo $this->fetch('AppScript');

	?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126422241-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126422241-1');
</script>
</body>
</html>
