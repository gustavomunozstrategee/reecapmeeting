<?php $permission = $this->Utilities->check_team_permission_action(array("index","view","edit"), array("clients"), $beforeEdit["data"]["Team"]["id"]);  ?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Clientes") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Clientes") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Editar cliente") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                
                <h2>
                    <?php echo __('Editar cliente'); ?>
                    <?php echo $this->element("team_name"); ?> 
                </h2>
                <div class="widget-options">
                    <div class="" role="">
                        <?php if(!empty($permission["index"])): ?>  
                                
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                                <button type="button" class="btn btn-secondary ">
                                    <i class="la la-list"></i>
                                    <?php echo __('Listar'); ?>
                                </button>
                            </a>
                                
                        <?php endif;?>

                        <?php if(isset($permission["view"])): ?>
                             <a href="<?php echo $this->Html->url(array('action'=>'view', EncryptDecrypt::encrypt($this->request->data['Client']['id'])));?>">
                                <button type="button" class="btn btn-secondary "><i class="la la-eye"></i><?php echo __('Ver'); ?></button></a>
                            </a> 
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('Client', array('role' => 'form','data-parsley-validate','type'=>'file','novalidate' => true)); ?>
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-6">
                            <div class="div-img-user">
                                 <div class="thumbnail text-center">
                                   <img alt="imagen" class="preview-customer-image img-fluid img-thumbnail" src="<?php echo $this->Html->url('/files/Client/'.$beforeEdit["data"]['Client']['img']); ?>"/>
                                </div>
                            </div>
                            <div class="img-user-upload">
                                <label for="ClientImg" class="btn btn-info btn-block mt-3"><?php echo __("Seleccionar imagen")?></label>
                                <?php echo $this->Form->input('img', array('type'=>'file','class' => 'form-control border-input imageFile', 'label'=>false,'div'=>false,'required' => false, 'style' => 'visibility:hidden;')); ?>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-6">
                            <p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>
                            <div class='form-group'>
                                <?php echo $this->Form->input('id'); ?>
                                <?php echo $this->Form->label('Client.name',__('Nombre'), array('class'=>'control-label f-blue required'));?>
                                <?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Nombre'), 'maxLength' => 80)); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input resize-none', 'label'=>false,'div'=>false, "type" => "hidden","value" => EncryptDecrypt::decrypt($this->Session->read("TEAM")))); ?>
                            </div>
                            <div class='form-group'>
                                <?php echo $this->Form->label('Client.description',__('Descripción'), array('class'=>'control-label f-blue required'));?>
                                <?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none', 'label'=>false,'div'=>false,'placeholder' => __('Descripción'),'maxLength' => 300)); ?>
                            </div>
                            <div id="showCharacter" class="form-group"></div> 
                            <?php if(isset($permission["edit"])):?>          
                                <div class="flex-justify-end">
                                    <button type='submit' class="btn btn-primary float-right"><?php echo __('Guardar') ?></button>
                                </div> 
                            <?php endif; ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>

            </div>
            <div class="widget-footer">
               
            </div>
        </div>
    </div>
</div>


<?php
    $this->Html->script('controller/clients/actions.js', ['block' => 'AppScript']);
    $this->Html->scriptBlock("EDIT.initElement(); ", ['block' => 'AppScript']);  
?>
