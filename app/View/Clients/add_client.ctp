<?php echo $this->Form->create('Client', array('role' => 'form','data-parsley-validate','type'=>'file')); ?>
	<div class="row">
		<div class='col-md-12'>
    		<p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label required'));?></p>
		</div>
		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Client.name',__('Nombre'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'maxLength' => 80,'div'=>false,'placeholder' => __('Nombre'))); ?>
			</div>
		</div>
		<div class='col-md-12' >
			<div class='form-group'>
				<?php echo $this->Form->label('Client.description',__('Descripción'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none textarea_client', 'label'=>false,'div'=>false,'placeholder' => __('Descripción'))); ?>
			</div>
			<div id="showCharacterClient">0/300</div>
		</div>
		<div class='col-md-12'>
			<?php echo $this->Form->label('Client.img',__('Imagen'), array('class'=>'control-label f-blue required'));?>
			<div class='form-group'>
				<label for="ClientImg" class="btn btn-info"><?php echo __("Seleccionar imagen")?></label>
				<?php echo $this->Form->input('img', array('type'=>'file','class' => 'form-control border-input imageFile','label'=>false,'div'=>false, 'style' => 'visibility:hidden;', 'placeholder' => __('imagen'))); ?>
			</div>
		</div>
		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Client.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('team_id', array('type'=>'select','class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Empresa'), 'options' => $team)); ?>
			</div>
		</div>
		<div class='col-md-12'>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Cancelar') ?></button>
	        <button type="button" id="btn_saveAddCliente" class="btn btn-primary"><?php echo __('Guardar') ?></button>
		</div>
	</div>
<?php echo $this->Form->end(); ?>

