<!-- <?php //$this->Html->css("frontend/admin/pages/employees_view.css", array("block" => "styles_section")); ?> -->
<?php $permission = $this->Utilities->check_team_permission_action(array("index","add","edit"), array("clients"), $client["Team"]["id"]); 
?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Clientes") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Clientes") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Visualización del cliente") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->



<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2>
                    <?php echo __('Visualización del cliente'); ?>
                    <?php echo $this->element("team_name"); ?> 
                </h2>
                <div class="widget-options">
                    <div class="" role="">
                        <?php if(!empty($permission["index"])): ?>  
                                
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                                <button type="button" class="btn btn-secondary ">
                                    <i class="la la-list"></i>
                                    <?php echo __('Listar'); ?>
                                </button>
                            </a>
                                
                        <?php endif;?>

                        <?php if(isset($permission["edit"])): ?>
                             <a href="<?php echo $this->Html->url(array('action'=>'edit', EncryptDecrypt::encrypt($client['Client']['id'])));?>">
                                <button type="button" class="btn btn-secondary "><i class="la la-pencil"></i><?php echo __('Editar'); ?></button></a>
                            </a> 
                        <?php endif; ?>
                        <?php if(isset($permission["add"])): ?>
                            <a href="<?php echo $this->Html->url(array('action'=>'add'));?>">
                                 <button type="button" class="btn btn-secondary "><i class="la la-plus"></i><?php echo __('Adicionar'); ?></button></a>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="widget-body">
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-4">
                        <input type="hidden" value="<?php echo EncryptDecrypt::encrypt($client['Client']['id']) ?>" id="client-id">
                        <a href="<?php echo $this->Html->url('/files/Client/'.$client['Client']['img']); ?>" class="showImagesPopup"> 
                            <div class="img-thumbnail">   
                                <img src="<?php echo $this->Html->url('/files/Client/'.$client['Client']['img']); ?>" class="img-fluid">
                            </div> 
                        </a>
                    </div>
                    <div class="col-md-9 col-lg-9 col-sm-8">
                        <div class="form-horizontal" id="content-table">
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-3 form-control-label">
                                    <?php echo __('Nombre'); ?>
                                </label>
                                <div class="col-lg-9">
                                     <?php echo h($client['Client']['name']); ?>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-3 form-control-label">
                                    <?php echo __('Descripción'); ?>
                                </label>
                                <div class="col-lg-9">
                                     <div class="div-word-wrap">
                                        <?php echo h($client['Client']['description']); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-3 form-control-label">
                                    <?php echo __('Fecha de registro:'); ?>
                                </label>
                                <div class="col-lg-9">
                                     <?php echo h(($this->Time->format('d-m-Y h:i A', $client['Client']['created']))); ?>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-3 form-control-label">
                                    <?php echo __('Estado'); ?>
                                </label>
                                <div class="col-lg-9">
                                     <?php echo $this->Utilities->showState($client['Client']['state']); ?>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <label class="col-lg-3 form-control-label">
                                    <?php echo __('Número de proyectos'); ?>
                                </label>
                                <div class="col-lg-9">
                                    <?php echo h($projectsCount); ?>
                                </div>
                            </div>
                            <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                                <div class="form-group row d-flex align-items-center border-bottom">
                                    <label class="col-lg-3 form-control-label">
                                        <?php echo __('Empresa'); ?>
                                    </label>
                                    <div class="col-lg-9">
                                        <?php echo $this->Html->link($client['User']['firstname'].' '.$client['User']['lastname'], array('controller' => 'users', 'action' => 'view', EncryptDecrypt::encrypt($client['User']['id'])), array('class' => '')); ?>&nbsp;
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-primary text-center"><?php echo __('Proyectos del cliente'); ?></h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div id="show-content-projects-users"></div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>

<?php
    $this->Html->script('controller/clients/actions.js',  ['block' => 'AppScript']);
    $this->Html->scriptBlock("EDIT.listarProjectsClients(); ", ['block' => 'AppScript']);
?>

