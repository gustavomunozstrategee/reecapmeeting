<?php $permissionAction = $this->Utilities->check_team_permission_action(array("add","index"), array("clients"));  ?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Gestión de clientes'); ?> 
                <?php echo $this->element("team_name"); ?> 
                <?php if(!empty($permissionAction["add"])): ?>  
                    <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar cliente");?>" href="<?php echo $this->Html->url(array('controller'=>'clients','action' => 'add')); ?>">
                        <i class="flaticon-add"></i>
                    </a> 
                <?php endif;?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Clientes") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row">                        
                        <div class="col-md-6">
                             <div class="form-group"> 
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre, Descripción'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo $this->Form->input('teams', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'hidden','id' => 'teamSelect','value' => EncryptDecrypt::decrypt($this->Session->read("TEAM")) )); ?>
                            
                                <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                    <?php echo __('Buscar');?> <i class="la la-search"></i>
                                </button>
                                
                            </div>
                        </div>
                        <div class="col-md-12">
                           <?php if(empty($clients) && !empty($this->request->query['q'])) : ?>
                                    <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
                                    <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
                                <?php endif ?> 
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="<?php echo $this->request->is('mobile') ? '' : 'widget-body' ?>">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th><?php echo __('Nombre'); ?></th>
                                <th class="text-center"><?php echo __('Proyectos'); ?></th>
                                <th class="text-center"><?php echo __('Actas'); ?></th>
                                <th><?php echo __('Descripción'); ?></th>
                                <!-- <th class=""><?php echo __('Empresa'); ?></th> -->
                                <th><?php echo __('Creación'); ?></th>
                                <th><?php echo __('Estado'); ?></th>
                                <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                                    <th><?php echo __('Empresa'); ?></th>
                                <?php endif; ?>
                                <th class="text-center table-grid-10">
                                    <?php echo __('Acciones'); ?>
                                </th>
                            </tr>
                        </thead>
                         <tbody>
                            <?php if(!empty($clients)):?>
                                <?php foreach ($clients as $client): ?>
                                    <tr>
                                        <td class="table-grid-10 td-word-wrap">
                                            <?php echo $this->Text->truncate(h($client['Client']['name']),100); ?>
                                        </td>
                                        <td class="table-grid-10 text-center">
                                            <span class="table-number"><?php echo count($client['Project']);?></span>
                                        </td>
                                        <td class="table-grid-10 text-center">
                                            <span class="table-number"><?php echo count($client['Contac']);?></span>
                                        </td>
                                        <td class="table-grid-10 td-word-wrap">
                                          <?php echo $this->Text->truncate(h($client['Client']['description']),100); ?>
                                        </td>
                                        <!-- <td class="table-grid-10 td-word-wrap"><?php //echo $this->Text->truncate(h($client['Team']['name']),100); ?></td> -->
                                        <td class="table-grid-10 td-word-wrap">
                                            <?php echo h($this->Time->format('d-m-Y h:i A', $client['Client']['created'])); ?>
                                        </td>
                                        <td class="table-grid-10 td-word-wrap">
                                            <?php echo $this->Utilities->showState($client['Client']['state']); ?>
                                        </td>
                                        <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                                            <td class="table-grid-10 td-word-wrap">
                                                <?php echo $this->Html->link($client['User']['firstname'].' '.$client['User']['lastname'], array('controller' => 'users', 'action' => 'view', $client['User']['id'])); ?>
                                            </td>
                                        <?php endif; ?> 
                                        <td class="table-grid-10 td-actions text-center">
                                            <?php $permission = $this->Utilities->check_team_permission_action(array("view","edit","changeState"), array("clients","app"), $client['Team']['id']); ?>

                                            <?php if(isset($permission["view"])):?>
                                                <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($client['Client']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                                    <i class="fa fa-eye"></i>
                                                </a> 
                                            <?php endif; ?>

                                            <?php if(isset($permission["edit"])):?>
                                                <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'edit', EncryptDecrypt::encrypt($client['Client']['id']))); ?>" title="<?php echo __('Editar'); ?>" class="btn-xs" data-toggle="tooltip">
                                                    <i class="fa fa-pencil"></i>
                                                </a> 
                                            <?php endif; ?>

                                            <?php if(isset($permission["changeState"])):?> 
                                                <?php echo $this->Utilities->changeStateButton($client['Client']['id'], $client['Client']['state']); ?> 
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td class="text-center" colspan="8"><?php echo __('No existen clientes.')?></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer">
                <!--Inicio Pagination-->
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination mt-3">
                            <?php
                            echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                            echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
                <!--End Pagination-->
            </div>
        </div>
    </div>
</div>

  <!--  <div class="tab f-blue font-bold">
    <a href="<?php echo $this->Html->url(array('controller'=>'clients','action'=>'index'));?>">
        <span class="<?php echo $this->params->params['controller'] == 'clients' ? 'active' : '' ?>"><?php echo __("CLIENTES")?></span>
    </a>
    <a href="<?php echo $this->Html->url(array('controller'=>'user_teams','action'=>'index'));?>">
        <span class="<?php echo $this->params->params['controller'] == 'user_teams' ? 'active' : '' ?>"><?php echo __("USUARIOS")?></span>
    </a>
</div> -->


           
<?php
$this->Html->script('lib/sweetalert.min.js', ['block' => 'AppScript']);
?>
