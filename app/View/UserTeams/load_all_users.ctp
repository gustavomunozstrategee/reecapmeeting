<div class='form-group'>
    <?php echo $this->Form->label('Project.users',__('Seleccionar usuarios con privilegio de ver las actas.'), array('class'=>'control-label f-blue font-bold'));?>
    <?php echo $this->Form->input('Project.users', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select', 'multiple' => true, 'options' => $users)); ?>
</div>