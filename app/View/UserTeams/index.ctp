<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Miembros de empresas'); ?>
                <?php echo $this->element("team_name"); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar usuario a empresa")?>" href="<?php echo $this->Html->url(array('controller'=>'user_teams','action' => 'add')); ?>">
                    <i class="flaticon-add"> </i>
                </a> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Usuarios") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
               <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row"> 
                        <div class="col-md-6"> 
                            <div class="form-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre, Cliente, Rol'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                                <?php echo $this->Form->input('teams', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'hidden','id' => 'teamSelect','value' => EncryptDecrypt::decrypt($this->Session->read("TEAM")) )); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                    <?php echo __('Buscar');?>
                                    <i class="la la-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?php if(empty($userTeams) && !empty($this->request->query['q'])) : ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="<?php echo $this->request->is('mobile') ? '' : 'widget-body' ?>">
                <ul class="nav nav-tabs nav-fill" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">
                            <?php echo __("Miembros y empresas"); ?>
                        </a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="<?php echo $this->Html->url(array('controller'=>'removed_users','action'=>'index'));?>">
                            <?php echo __("Miembros removidos"); ?>
                        </a>
                    </li>-->
                </ul>
                <div class="tab-content pt-3">
                    <div class="tab-pane fade show active" id="j-tab-1" role="tabpanel" aria-labelledby="just-tab-1">
                        <div class="table-responsive">
                            <table cellpadding="0" cellspacing="0" class="table f-table-recapmeeting f-table-grid-layout-fixed">
                                <thead class="text-primary">
                                    <tr>
                                        <th><?php echo __('Usuario añadido'); ?></th>
                                        <th><?php echo __('Correo electrónico'); ?></th>
                                        <th><?php echo __('Rol'); ?></th>
                                        <th><?php echo __('Cliente(s)'); ?></th>
                                        <th><?php echo __('Tipo de usuario'); ?></th>
                                        <th><?php echo __('Estado licencia') ?></th> 
                                        <th><?php echo __('Estado usuario') ?></th> 
                                        <th class="text-center"><?php echo __('Acciones') ?></th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($userTeams)) {?>
                                        <?php foreach ($userTeams as $userTeam): ?>
                                            <?php if($userTeam['User']['id'] != AuthComponent::user('id')){ ?>
                                                <tr>
                                                    <?php if(!empty($userTeam['User']['firstname']) && !empty($userTeam['User']['firstname'])): ?>
                                                        <td><?php echo $userTeam['User']['firstname'] . ' ' . $userTeam['User']['lastname']?></td>
                                                        <?php else: ?>
                                                        <td><?php echo __("Nombre no asignado por parte del usuario");?></td> 
                                                    <?php endif; ?>
                                                    <td><?php echo !empty($userTeam['User']['email']) ? $userTeam['User']['email'] : __("Ninguno") ?></td>
                                                    <td><?php echo !empty($userTeam['Position']['name']) ? $userTeam['Position']['name'] : __("Ninguno")?></td>
                                                    <td>
                                                        <?php if ($userTeam["UserTeam"]["type_user"] == 1): ?>
                                                            <?php echo __("Ninguno") ?>
                                                        <?php else: ?>
                                                            <ul>                                                                
                                                                <?php 
                                                                    echo $this->Utilities->getClientsByExtern($userTeam["UserTeam"]["user_id"]);
                                                                 ?>
                                                            </ul>
                                                        <?php endif ?>
                                                     </td>
                                                    <?php if($userTeam["UserTeam"]["type_user"] == 1):?>
                                                        <td><?php echo __("Colaborador"); ?></td>
                                                    <?php else: ?>
                                                        <td><?php echo __("Asistente externo"); ?></td>
                                                    <?php endif; ?>
                                                    <td>
                                                         <?php if($userTeam['UserTeam']['state'] == configure::read("ENABLED")): ?> 
                                                            <?php echo __("Activa");?>
                                                            <?php else: ?>
                                                            <?php echo __("Vencida");?>     
                                                        <?php endif; ?> 
                                                    </td>
                                                    <td>
                                                        <?php if($userTeam['UserTeam']['user_state'] == configure::read("ENABLED")): ?> 
                                                            <?php echo __("Desactivado");?>
                                                        <?php endif; ?>  
                                                        <?php if($userTeam['UserTeam']['user_state'] == configure::read("DISABLED")): ?> 
                                                            <?php echo __("Activo");?>
                                                        <?php endif; ?>  
                                                    </td>  
                                                    <td class="td-actions text-center"> 
                                                        <?php 
                                                                //Validar por empresa
                                                                $permissionUsers    = $this->Utilities->check_team_permission_action(array("index"), array("user_teams")); 
                                                        ?>
                                                        <?php if ($permissionUsers["index"]): ?>
                                                            
                                                        
                                                            <?php if($userTeam['UserTeam']['user_id'] != authcomponent::user("id")): ?>
                                                                <?php if($userTeam['UserTeam']['user_state'] == configure::read("DISABLED")):?>
                                                                    <?php if($userTeam['UserTeam']['type_user'] == configure::read("COLLABORATOR")):?>
                                                                        <?php if ($userTeam['Position']['id'] != 1): ?>
                                                                            
                                                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'disabled_license', EncryptDecrypt::encrypt($userTeam['UserTeam']['id']))); ?>" title="<?php echo __('Deshabilitar licencia'); ?>" class="btn-xs btn_team_action" data-toggle="tooltip">
                                                                                <i class="fa fa-ban"></i>
                                                                            </a> 
                                                                        <?php endif ?>
                                                                        <?php else: ?>
                                                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'disabled_license', EncryptDecrypt::encrypt($userTeam['UserTeam']['id']))); ?>" title="<?php echo __('Deshabilitar usuario'); ?>" class="btn-xs btn_team_action" data-toggle="tooltip">
                                                                            <i class="fa fa-ban"></i>
                                                                        </a> 
                                                                    <?php endif; ?>
                                                                <?php else: ?>
                                                                    <?php if($userTeam['UserTeam']['type_user'] == configure::read("COLLABORATOR")):?>
                                                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'enabled_license', EncryptDecrypt::encrypt($userTeam['UserTeam']['id']))); ?>" title="<?php echo __('Habilitar licencia'); ?>" class="btn-xs btn_team_action" data-toggle="tooltip">
                                                                            <i class="fa fa-check"></i>
                                                                        </a> 
                                                                    <?php else: ?>
                                                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'enabled_license', EncryptDecrypt::encrypt($userTeam['UserTeam']['id']))); ?>" title="<?php echo __('Habilitar usuario'); ?>" class="btn-xs btn_team_action" data-toggle="tooltip">
                                                                            <i class="fa fa-check"></i>
                                                                        </a> 
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                <?php if($userTeam['UserTeam']['state'] == configure::read("ENABLED")):?>
                                                                    <?php if($userTeam['UserTeam']['type_user'] == configure::read("COLLABORATOR")):?>
                                                                        <?php if($userTeam['User']['id'] != $userTeam['Team']['user_id']){ ?>
                                                                            <!--<a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'remove_user', EncryptDecrypt::encrypt($userTeam['UserTeam']['id']))); ?>" title="<?php echo __('Remover licencia'); ?>" class="btn-xs btn_team_action" data-toggle="tooltip">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>-->
                                                                        <?php } ?>
                                                                     <?php endif; ?>
                                                                <?php endif; ?>
                                                                <?php if($userTeam['UserTeam']['position_id'] != NULL || $userTeam['UserTeam']['position_id'] != ""):?>
                                                                    <?php if($userTeam['User']['id'] != $userTeam['Team']['user_id']){ ?>
                                                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'user_teams','action' => 'edit_role', EncryptDecrypt::encrypt($userTeam['UserTeam']['id']))); ?>" title="<?php echo __('Editar rol asignado'); ?>" class="btn-xs" data-toggle="tooltip">
                                                                            <i class="fa fa-pencil"></i>
                                                                        </a>
                                                                    <?php } ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    <?php } else { ?>
                                        <tr>
                                            <td class="text-center" colspan="9"><?php echo __('No existen usuarios añadidos.')?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
