<!-- Begin Page Header-->
<?php  $permission = $this->Utilities->check_team_permission_action(array("index","add"), array("user_teams")); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Usuarios") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Usuarios") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Editar rol asignado") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->
<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                
                <h2>
                    <?php echo __('Editar rol asignado'); ?> 
                
                    <?php if(!empty($userTeam['User']['firstname']) && !empty($userTeam['User']['firstname'])): ?>
                        (<?php echo $userTeam['User']['firstname'] . ' ' . $userTeam['User']['lastname']?>)
                        <?php else: ?>
                        (<?php echo $userTeam['User']['email'];?> )
                    <?php endif; ?>
                    <?php echo $this->element("team_name"); ?>
                </h2>
                <?php if(!empty($permission["index"])): ?>  
                    <div class="widget-options">
                        <div class="" role="">
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><button type="button" class="btn btn-secondary "><i class="la la-list"></i><?php echo __("Listar usuarios");?></button></a>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('UserTeam', array('role' => 'form','datas-parsley-validate','novalidate' => true)); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-warinig"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label required f-blue font-bold'));?></p>
                            <div class='form-group'>
                                <?php echo $this->Form->label('UserTeam.position_id',__('Tipo de rol'), array('class'=>'control-label f-blue required'));?>
                                <?php echo $this->Form->input('position_id', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','empty' => __("Seleccione una opción"), 'options' => $positions, 'selected' => $role_selected)); ?>
                            </div> 
                            <div class='form-group'>
                                <?php echo $this->Form->label('UserTeam.department_id',__('Area dentro de la empresa'), array('class'=>'control-label f-blue required'));?>
                                <?php echo $this->Form->input('UserTeam.department_id', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','empty' => __("Seleccione una opción"), 'options' => $departments, 'selected' => $department_selected)); ?>


                            </div>   
                            <div class='form-group'>
                                <?php 
                                $opciones = array(
                                        "0" => "Inactivo",
                                        "1" => "Activo"
                                );
                                 ?>
                                 <?php echo $this->Form->label('UserTeam.department_id',__('¿Usuario SMF?'), array('class'=>'control-label f-blue required'));?>
                                 <?php echo $this->Form->input('UserTeam.smf_state', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','empty' => __("Seleccione una opción"), 'options' => $opciones, 'selected' => $userTeam["UserTeam"]["smf_state"])); ?>
                            </div>
                            <div>
                                <button type='submit' class='btn btn-primary pull-right'><?php echo __('Guardar') ?></button>
                            </div> 
                        </div>

                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="widget-footer">
            </div>
        </div>
    </div>
</div>
