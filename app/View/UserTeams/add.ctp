<!-- Begin Page Header-->
<?php  $permission = $this->Utilities->check_team_permission_action(array("index","add"), array("user_teams")); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
               <?php echo __("Usuarios") ?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <?php if (!empty($permission["index"])): ?>
                        
                    <li class="breadcrumb-item">
                      <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Usuarios") ?></a>
                    </li>
                    <?php endif ?>
                    <li class="breadcrumb-item active"><?php echo __("Adicionar usuario") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2>
                    <?php echo __('Adicionar usuario'); ?> <?php echo $this->element("team_name"); ?>
                </h2>
                <?php if(!empty($permission["index"])): ?>  
                    <div class="widget-options">
                        <div class="" role="">
                            <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><button type="button" class="btn btn-secondary "><i class="la la-list"></i><?php echo __("Listar usuarios");?></button></a>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('UserTeam', array('class'=>'frm-prevent-enter', 'role' => 'form','datas-parsley-validate','novalidate' => true)); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>                    
                            <div class='form-group'>
                                <?php echo $this->Form->label('UserTeam.type_user',__('Tipo de usuario'), array('class'=>'control-label f-blue required'));?>
                                <?php if($this->Session->read("Config.language") == "esp"):?>
                                    <?php echo $this->Form->input('type_user', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','empty' => __("Seleccione una opción"), 'options' => configure::read("TYPE_USER"))); ?>
                                    <?php else: ?>
                                    <?php echo $this->Form->input('type_user', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','empty' => __("Seleccione una opción"), 'options' => configure::read("TYPE_USER_ENG"))); ?>
                                <?php endif; ?>
                            </div>   
                           
                            <div id="show_process_assigned" class="form-group"></div> 
                            <div class="pull-left form-group">   
                                <?php $totalCompleted = 0 ?>
                                <?php if(!empty($tasks["models"])):?>
                                    <?php foreach ($tasks["models"] as $model): ?>
                                        <?php if($model >= 1 ):?>
                                            <?php $totalCompleted += 1; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>     
                                    <?php if($totalCompleted == 5 && $stepApp == 1): ?>                 
                                        <a id="FinishStep" href="<?php echo $this->Html->url(array("controller" => "contacs","action" => "add"));?>"><button type='button' id="stepCompleted" class="btn btn-reecapmeeting-user pull-right"><?php echo __('Finalizar') ?></button></a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>


<div class="backg-white">
    <div class="grid-form">
        <div class="col-form-img col-form-img-addemployees">
        </div>
        <?php if(!empty($planActualInfo)):?>
            <!-- Formulario -->
            <div class="col-form-input none-shadow">
               
            </div>
            <!--End Formulario -->
            <?php else: ?>
                <div class="col-50-textbuyplan">
                  <div class="flex-center">
                    <p class="text-buyplan"><?php echo __("Para adicionar usuarios a una empresa necesitas tener un plan activo.");?></p>
                    <a class="btn btn-reecapmeeting-user" href="<?php echo $this->Html->url(array('controller'=>'plans','action'=>'planning_management')) ?>"><?php echo __('Comprar planes') ?></a>
                  </div>
                </div> 
        <?php endif; ?>
    </div>
    </div>
    <!-- End -->

    </div>
</div>

<?php
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
	$this->Html->script('lib/parsley/parsley.min.js',			    ['block' => 'AppScript']);
	$this->Html->script('controller/user_teams/assign_user.js?v=240520220730',		['block' => 'AppScript']);
  	$this->Html->script('lib/sweetalert.min.js',	                ['block' => 'AppScript']);
	if ($this->Session->read('Config.language') == 'esp') {
		$this->Html->script('lib/parsley/i18n/es.js',				['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
	}
?>

