<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Miembros y empresas'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar usuario a empresa")?>" href="<?php echo $this->Html->url(array('controller'=>'user_teams','action' => 'add')); ?>">
                    <i class="flaticon-add"> </i>
                </a>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Listar mis empresas y miembros de empresas")?>" href="<?php echo $this->Html->url(array('controller'=>'user_teams','action' => 'index')); ?>">
                    <i class="flaticon-list"> </i>
                </a>   
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"user_teams",'action'=>'index'));?>"><?php echo __("Usuarios") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Miembros y empresas") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row">
                        <div class="col-md-6 mt-1">
                            <div class="form-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre, Empresa, Cliente, Rol'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
                        <!--
                        <div class="col-md-4 mt-1">
                            <div class="form-group f-input-select-white">
                                <?php echo $this->Form->label('UserTeam.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
                                <?php if(!empty($this->request->query["teams"])):?>
                                    <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams','multiple' => true, 'value' => $this->request->query["teams"])); ?>
                                <?php else: ?>
                                    <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams','multiple' => true)); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        -->
                        <div class="col-md-6 mt-1">
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>
                        <div class="col-md-12 mt-1">
                            <?php if(empty($userTeams) && !empty($this->request->query['q'])) : ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'all_my_teams'), array('class' => 'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="widget-body">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                        <thead class="text-primary">
                            <tr>
                                <th><?php echo __('Empresa'); ?></th>
                                <th><?php echo __('Usuario añadido'); ?></th>
                                <th><?php echo __('Correo electrónico'); ?></th>
                                <th><?php echo __('Rol'); ?></th>
                                <th><?php echo __('Cliente'); ?></th>
                                <th><?php echo __('Tipo de usuario'); ?></th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($userTeams)):?>
                                <?php foreach ($userTeams as $userTeam): ?>
                                    <tr>
                                        <td><?php echo $this->Text->truncate($userTeam['Team']['name'],100); ?></td> 
                                        <?php if(!empty($userTeam['User']['firstname']) && !empty($userTeam['User']['firstname'])): ?>
                                            <td><?php echo $userTeam['User']['firstname'] . ' ' . $userTeam['User']['lastname']?></td>
                                            <?php else: ?>
                                            <td><?php echo __("Nombre no asignado por parte del usuario.");?></td> 
                                        <?php endif; ?>
                                        <td><?php echo !empty($userTeam['User']['email']) ? $userTeam['User']['email'] : __("Ninguno") ?></td>
                                        <td><?php echo !empty($userTeam['Position']['name']) ? $userTeam['Position']['name'] : __("Ninguno")?></td>
                                        <td><?php echo !empty($userTeam['Client']['name']) ? $userTeam['Client']['name'] : __("Ninguno")?></td>
                                        <?php if($userTeam["UserTeam"]["type_user"] == 1):?>
                                            <td><?php echo __("Colaborador"); ?></td>
                                        <?php else: ?>
                                            <td class="table-grid-10"><?php echo __("Asistente externo"); ?></td>
                                        <?php endif; ?> 
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="6"><?php echo __('No existen usuarios añadidos.')?></td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                            echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                            echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>