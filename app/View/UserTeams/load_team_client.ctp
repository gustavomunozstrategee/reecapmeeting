<div class='form-group' style="display: none !important;">
    <?php echo $this->Form->label('UserTeam.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
    <?php echo $this->Form->input('UserTeam.team_id', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select', 'options' => $team,"id"=>"TeamSelection")); ?>
</div> 

<?php if($this->Session->check('ActivateUser')):?>
	    <div class="form-group">
	    	<p class="f-blue font-bold"><?php echo __("Reasignar usuario a empresa");?></p>
	    </div> 
	    <div class='form-group'>
	        <?php echo $this->Form->label('UserTeam.email',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
	        <?php echo $this->Form->input('UserTeam.email', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'value' => $this->Session->read('ActivateUser.email'))); ?>
	    </div> 
	<?php else: ?>
	    <div class="form-group">
	        <p class="f-blue font-bold"><?php echo __("Buscar usuario para asignar");?></p>
	    </div> 
	    <div class='form-group'>
	        <?php echo $this->Form->label('UserTeam.email',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
	        <?php echo $this->Form->input('UserTeam.email', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
	    </div> 
<?php endif; ?>

<div>
    <button type='button' class='btn btn-info pull-right assign_user_team'><?php echo __('Buscar') ?></button>
</div>
<div id="save_user_assigned" style="display:none;"> 
    <button type="submit" class='btn btn-primary save_user'><?php echo __("Invitar usuario");?></button>
</div>