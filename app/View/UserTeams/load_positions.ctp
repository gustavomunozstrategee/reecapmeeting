 
<?php if($type >= 1):?>
	<?php if($type == configure::read("COLLABORATOR")): ?>

		<div class='form-group'>
			<?php echo $this->Form->label('UserTeam.department_id',__('Área dentro de la empresa'), array('class'=>'control-label f-blue required'));?>
			<?php echo $this->Form->input('UserTeam.department_id', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','empty' => __("Seleccione una opción"), 'options' => $departments)); ?>
		</div>

		<div class='form-group'>
			<?php echo $this->Form->label('UserTeam.position_id',__('Rol dentro de la empresa'), array('class'=>'control-label f-blue required'));?>
			<?php echo $this->Form->input('UserTeam.position_id', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','empty' => __("Seleccione una opción"), 'options' => $positions)); ?>
		</div>
		
		<div class='form-group'>
	        <?php echo $this->Form->input('UserTeam.team_id', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'hidden', "value" => EncryptDecrypt::decrypt($this->Session->read("TEAM")),"id" => "TeamSelection")); ?>
	    </div> 

<!-- 	    <div class="col-50 padding-left-15">
	        <div class="form-group">
	            <div class="checkbox check_policy">
	                <label> 
	                    <?php //echo $this->Form->input("UserTeam.select_all", array('type'=>'checkbox', 'value' => 1, 'label'=>false, 'div'=>false, 'hiddenField'=>false)); ?>
	                    <?php //echo __("Seleccionar todos las empresas");?>  
	                </label>
	            </div> 
	        </div>
	    </div> -->

		<?php if($this->Session->check('ActivateUser')):?>
		    <div class="form-group">
		    	<p class="f-blue font-bold"><?php echo __("Reasignar usuario a empresa");?></p>
		    </div> 
		    <div class='form-group'>
		        <?php echo $this->Form->label('UserTeam.email',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
		        <?php echo $this->Form->input('UserTeam.email', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'value' => $this->Session->read('ActivateUser.email'))); ?>
		    </div> 
		<?php else: ?>
		    <div class="form-group">
		        <p class="f-blue font-bold"><?php echo __("Buscar usuario para asignar");?></p>
		    </div> 
		    <div class='form-group'>
		        <?php echo $this->Form->label('UserTeam.email',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
		        <?php echo $this->Form->input('UserTeam.email', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
		    </div> 
		<?php endif; ?>

		<div>
		    <button type='button' class='btn btn-info pull-right assign_user_team'><?php echo __('Buscar') ?></button>
		</div>
		<div id="save_user_assigned" style="display:none;"> 
		    <button type="submit" class='btn btn-primary save_user'><?php echo __("Invitar usuario");?></button>
		</div>

	<?php else: ?>
		
		<div class='form-group'>
		    <?php echo $this->Form->label('UserTeam.client_id',__('Cliente'), array('class'=>'control-label f-blue required'));?>
		    <?php echo $this->Form->input('UserTeam.client_id', array('multiple' => true,'class' => 'form-control border-input','label'=>false,'div'=>false, 'type' => 'select','options' => $clients)); ?>
		</div>  
		
		<div id="team_field"></div> 
	<?php endif; ?>
<?php endif; ?>
