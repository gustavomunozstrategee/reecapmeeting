 <section class="section-table">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting">
                        <thead class="text-primary">
                            <tr>
                                <th><?php echo __('Usuario'); ?></th> 
                                <th><?php echo __('Correo electrónico'); ?></th> 
                                <th>
                                    <?php echo __('Seleccionar todos') ?>
                                    <div class="text-center"><input type="checkbox" name="all_user" class="choose_all" value="1"></div>
                                </th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($users)):?>
                                <?php foreach ($users as $user): ?>
                                    <tr>
                                    <?php if(!empty($user['User']['firstname'])):?>
                                        <?php if($user['User']['id'] == $user['OwnUser']['id']):?>
                                            <td>
                                                <?php echo $user['User']['name'] . __(" (Tu) "); ?>
                                            </td>
                                        <?php else: ?>
                                            <td>
                                                <?php echo $user['User']['name']; ?>
                                            </td>
                                            <?php endif;?>
                                        <?php else: ?>
                                        <td>
                                            <?php echo __("Nombre no asignado por parte del usuario"); ?>
                                        </td>
                                    <?php endif; ?> 
                                    	<td><?php echo $user["User"]["email"]?></td>
                                    	<td class="text-center">
                                    		<input type="checkbox" name="users_teams[]" class="choose_users_to_renovated" value="<?php echo EncryptDecrypt::encrypt($user["User"]["id"]); ?>">
                                    	</td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="14"><?php echo __('No existen usuarios')?></td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
    </div>
</section>
 