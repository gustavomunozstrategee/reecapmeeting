<div class="backg-white">    
    <div class="flex-space-between">
        <div>
            <h1 class="f-title"><?php echo __('Historial de logs'); ?> <?php echo $this->element("team_name"); ?></h1>
        </div>
    </div>
    <!-- Buscadores -->
    <div class="flex-start my-10 f-datapicker">
        <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inline', 'type'=>'GET', 'role'=>'form'));?>
        <div class="form-group">
            <!-- <?php echo $this->Form->label('Log.date',__('Buscar por fecha'), array('class'=>'control-label f-blue'));?> -->
            <div class='input-group date' id='datetimepickerStartLog'>
                <?php echo $this->Form->input('date', array('class' => 'form-control', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true, 'value'=> !empty($this->request->query['date']) ? $this->request->query['date'] : '')); ?>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <div class="form-group f-input-select-white">
            <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'hidden', 'id' => 'teamSelect','value' => EncryptDecrypt::decrypt($this->Session->read("TEAM")) )); ?>
        </div>
        <div class="form-group f-input-select-white">
            <!-- <?php echo $this->Form->label('Log.date',__('Buscar por funcionario'), array('class'=>'control-label f-blue'));?> -->
            <?php echo $this->Form->input('user_id', array('class' => 'form-control','label'=>false,'div'=>false,'placeholder' => 'Nombre', 'type' => 'select', 'options' => $users, 'empty' => __("Selecciona un funcionario"), 'selected' => !empty($this->request->query['user_id']) ? $this->request->query['user_id'] : '')); ?>
        </div>
        <div class="form-group f-input-select-white">
            <!-- <?php echo $this->Form->label('Log.model',__('Buscar por módulo'), array('class'=>'control-label f-blue'));?> -->
            <?php if($this->Session->read("Config.language") == "esp"):?>
                <?php echo $this->Form->input('model', array('class' => 'form-control','label'=>false,'div'=>false,'placeholder' => 'Nombre', 'type' => 'select', 'options' => configure::read("MODELS"), 'empty' => __("Selecciona un módulo"), 'selected' => !empty($this->request->query['model']) ? $this->request->query['model'] : '')); ?>
            <?php else: ?>
                <?php echo $this->Form->input('model', array('class' => 'form-control','label'=>false,'div'=>false,'placeholder' => 'Nombre', 'type' => 'select', 'options' => configure::read("MODELS_ENG"), 'empty' => __("Selecciona un módulo"), 'selected' => !empty($this->request->query['model']) ? $this->request->query['model'] : '')); ?>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-blue" id="search_team"><?php echo __('Buscar');?></button>
        <?php if(empty($logs) && (!empty($this->request->query["date"]) || !empty($this->request->query["model"]) || !empty($this->request->query["user_id"]) || !empty($this->request->query["teams"]))) : ?>
            <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
            <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
       
        <?php endif ?>
        <?php echo $this->Form->end(); ?>
    </div>
    <section class="section-table">
        <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
            <thead class="text-primary">
                <tr>
                    <th class="table-grid-10"><?php echo __('Módulo'); ?></th>
                    <th class="table-grid-20"><?php echo __('Descripción'); ?></th>
                    <th class="table-grid-10"><?php echo __('Antes de editar'); ?></th>
                    <th class="table-grid-10"><?php echo __('Después de editar'); ?></th>
                    <!-- <th class="table-grid-10"><?php echo __('Empresa'); ?></th> -->
                    <th class="table-grid-10"><?php echo __('Usuario'); ?></th>
                    <th class="table-grid-10"><?php echo __('Fecha de creación'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($logs)):?>
                <?php foreach ($logs as $log): ?>
                <tr>
                    <td class="table-grid-10 td-word-wrap"><?php echo h($this->Utilities->nameModel($log['Log']['model'])); ?>&nbsp;</td>
                    <?php if($this->Session->read("Config.language") == "esp"):?>
                        <td class="table-grid-20 td-word-wrap"><?php echo $this->Text->truncate(h($log['Log']['description']),100); ?>&nbsp;</td>
                    <?php else: ?>
                        <td class="table-grid-20 td-word-wrap"><?php echo $this->Text->truncate(h($log['Log']['description_eng']),100); ?>&nbsp;</td>
                    <?php endif; ?>
                    <td class="table-grid-10">
                        <?php if(!empty(__($log['Log']['before_edit']))) { ?>
                            <?php $beforeEdit = stripslashes($log['Log']['before_edit']); ?>
                            <?php $listInfo   = json_decode($beforeEdit); ?> 
                            <?php if(empty($listInfo)) { ?>
                                <?php echo __($log['Log']['before_edit']) ?>
                            <?php } else { ?>
                                    <button type="button" class="btn btn-blue btn-xs btn-show-details">
                                        <?php echo __('Ver detalles'); ?>
                                    </button>
                                    <table class="table table-bordered table-condensed table-striped tbl-details hidden">
                                        <?php  foreach ($listInfo as $field => $value) { ?>
                                            <tr>
                                                <td><?php echo __($field); ?></td>
                                                <td><?php echo $value; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                            <?php } ?>
                        <?php } else { 
                                echo  __("Sin modificaciones"); 
                            }
                        ?>
                    </td>
                    <td class="table-grid-10">
                        <?php if(!empty($log['Log']['after_edit'])) { ?>
                            <?php $afterEdit = stripslashes($log['Log']['after_edit']); ?>
                            <?php $listInfo  = json_decode($afterEdit); ?>
                            <?php if(empty($listInfo)) { ?>
                                <?php echo __($log['Log']['after_edit']) ?>
                            <?php } else { ?>
                                    <button type="button" class="btn btn-blue btn-xs btn-show-details">
                                        <?php echo __('Ver detalles'); ?>
                                    </button>
                                    <table class="table table-bordered table-condensed table-striped tbl-details hidden">
                                        <?php  foreach ($listInfo as $field => $value) { ?>
                                            <tr>
                                                <td><?php echo __($field); ?></td>
                                                <td><?php echo $value; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                            <?php } ?>
                        <?php } else { 
                                echo  __("Sin modificaciones"); 
                            }
                        ?>
                    </td>
                    <!-- <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($log['Team']['name']),100); ?></td> -->
                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($log['User']['firstname'] . ' ' . $log['User']['lastname']),100); ?></td>
                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($log['Log']['created']),100); ?></td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td class="text-center" colspan="7"><?php echo __('No existen logs.')?></td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>

        <div class="table-pagination">
            <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
            <div>
                <ul class="pagination f-paginationrecapmeeting">
                    <?php
                        echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                        echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
        </div>
    </section>
    <?php
    	$this->Html->script('lib/datepicker/bootstrap-datepicker.js',['block' => 'AppScript']);
    	if ($this->Session->read('Config.language') == 'esp') {
    		$this->Html->script('lib/datepicker/locales/bootstrap-datepicker.es.js', ['block' => 'AppScript']);
    	}
        $this->Html->script('controller/logs/index.js',   ['block' => 'AppScript']);
        $this->Html->scriptBlock("Logs.init(); ",    ['block' => 'AppScript']);
    ?>
    <div class="modal fade" id="modalDetailLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg modal-recapmeeting" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo __('Detalles del log'); ?></h4>
          </div>
          <div class="modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __('Cerrar'); ?></button>
          </div>
        </div>
      </div>
    </div>
</div>


