<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
 <div class="container-fluid">
    <!-- Begin Page Header-->
    <!-- End Page Header -->
     <?php echo $this->element('nav_taskee'); ?>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                                <div class="widget widget-18 has-shadow">
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header bordered d-flex align-items-center">
                                        <h2><?php echo __("Nueva Lista") ?></h2>
                                        <div class="media-right align-self-center">
                                        </div>
                                    </div>
                                    <!-- End Widget Header -->
                                    <div class="widget-body">

                                      <div class="tab-pane active show" id="tab1">
                                          <?php echo $this->Form->create('Slate', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40')); ?>                   
                                                            <div class="form-group row mb-3">
                                                                <div class="col-xl-12 mb-3">
                                                                    <label class="form-control-label"><?php echo __("Nombre") ?><span class="text-danger ml-2">*</span></label>
                                                               
                                                                    <?php echo $this->Form->input('name', array('class' => 'form-control',"label" => false))?>  
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <div class="col-xl-9 mb-3">
                                                                    <label class="form-control-label"><?php echo __("Participantes") ?><span class="text-danger ml-2"></span></label>

                                                                    <?php echo $this->Form->input('participantes', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple','value' => @$this->request->data["Contac"]["funcionarios"], 'options' => $collaborators)); ?>  
                                                                </div>
                                                                <div class="col-xl-3">
                                                                    <label class="form-control-label">Permisos<span class="text-danger ml-2"></span></label>
                                                                    <select name="Slate[permisos]" class="custom-select form-control">
                                                                        <option value="1">Colaborador</option>
                                                                        <option value="2">Editor</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <ul class="pager wizard text-right">
                                                                 
                                                                <li class="next d-inline-block">
                                                                  <button class="btn btn-gradient-01"><?php echo __("Crear Listado") ?></button>
                                                                   
                                                                </li>
                                                            </ul>
                                            <?php echo $this->Form->end(); ?>
                                        </div>
                                        
                                         
                                        
                                    </div>
                                </div>
                            </div>
</div>

<?php
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
   $this->Html->script('lib/select2/select2.min.js',               ['block' => 'AppScript']);
  $this->Html->script('lib/bootstrap-tagsinput.min.js',           ['block' => 'AppScript']);
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
?>
<script type="text/javascript">
    $(document).ready(function(){
  $('#buscador').keyup(function(){
     var nombres = $('.nombres');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.item').show(); 
              }else{
                   $(nombres[i]).parents('.item').hide();
              }
          }
     }
  });
  $('.select2_all').select2({
    });
});

</script>

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
</style>
 