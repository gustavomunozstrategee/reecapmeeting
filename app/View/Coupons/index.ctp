<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Gestión de cupones'); ?>
                <?php if(AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?>
                    <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __('Obsequiar cupón'); ?>" href="<?php echo $this->Html->url(array('action'=>'add')); ?>">
                        <i class="flaticon-add"> </i>
                    </a>
                <?php endif;?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"coupons",'action'=>'index'));?>"><?php echo __("Cupones") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row"> 
                        <div class="col-md-6">
                            <div class="input-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre, descripción, cliente'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <?php if(empty($coupons) && !empty($this->request->query['q'])) : ?>
                            <p class="mxy-10"><?php echo __('No se encontraron datos.') ?></p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'),array('class' => 'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div> 
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="<?php echo $this->request->is('mobile') ? '' : 'widget-body' ?>">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                        <thead class="text-primary">
                            <tr>
                                <th class="table-grid-9"><?php echo __('Nombre'); ?></th>
                                <th class="table-grid-9"><?php echo __('Código'); ?></th>
                                <th class="table-grid-9"><?php echo __('Fecha inicio'); ?></th>
                                <th class="table-grid-9"><?php echo __('Fecha fin'); ?></th>
                                <th class="table-grid-9"><?php echo __('Estado'); ?></th>
                                <th class="table-grid-9"><?php echo __('Fecha de redención'); ?></th>
                                <th class="table-grid-9"><?php echo __('Plan asociado'); ?></th>
                                <th class="table-grid-9 text-center"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($coupons)):?>
                            <?php foreach ($coupons as $coupon): ?>
                            <tr>
                                <td class="table-grid-9 td-word-wrap"><?php echo $this->Text->truncate(h($coupon['Coupon']['name']),100); ?></td>
                                <td class="table-grid-9 td-word-wrap"><?php echo h($coupon['Coupon']['code']); ?></td>
                                <td class="table-grid-9 td-word-wrap"><?php echo h($coupon['Coupon']['start_date']); ?></td>
                                <td class="table-grid-9 td-word-wrap"><?php echo h($coupon['Coupon']['end_date']); ?></td>
                                <td class="table-grid-9 td-word-wrap"><?php echo $this->Utilities->showStateCoupon($coupon['Coupon']['usage_coupon'], $coupon['Coupon']['state'], $coupon['Coupon']['end_date']); ?></td>
                                <td class="table-grid-9 td-word-wrap"><?php echo h(!empty($coupon['Coupon']['date_use']) ? $coupon['Coupon']['date_use'] : __("Sin usar")); ?></td>
                                <td class="table-grid-9 td-word-wrap"><?php echo h($coupon['Plan']['name']); ?></td>
                                <td class="td-actions text-center table-grid-9">
                                    <?php if($coupon['Coupon']['usage_coupon'] == Configure::read("CUPON_NO_USADO") && $coupon['Coupon']['end_date'] > date('Y-m-d')):?>
                                    <?php echo $this->Utilities->buttonDisableToReason($coupon['Coupon']['id'], $coupon['Coupon']['state']); ?>
                                    <?php endif; ?>
                                    <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($coupon['Coupon']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="9"><?php echo __('No existen cupones.')?></td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->Html->script('controller/coupons/actions.js',	['block' => 'AppScript']);?>
<div class="modal fade" id="reasonDisabledCoupon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabeldfsd23"><?php echo __('Deshabilitar cupón') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label><?php echo __('Enviar motivo al usuario por el cual se va deshabilitar el cupón.')?></label>
            <textarea name="data[Coupon][reason]" id="ReasonCouponDisabled" class="form-control resize-none"></textarea>
        </div>
        <p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...')?></p>
      </div>
      <div class="modal-footer">
        <a class="btn btn-secundary send_reject "><?php echo __('Enviar motivo') ?></a>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo __('Cancelar') ?></button>
      </div>
    </div>
  </div>
</div>

