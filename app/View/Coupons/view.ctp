<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Visualización del cupón'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __('Listar'); ?>"  href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                    <i class="flaticon-menu"></i>
                </a>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"plans",'action'=>'index'));?>"><?php echo __("Cupones") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-xl-12">
    <div class="widget has-shadow">
      <div class="widget-header bordered no-actions d-flex align-items-center">
        <div class="row w-100">
          <div class="col-md-12">
            <?php if (AuthComponent::user('role_id') == Configure::read('ADMIN_ROLE_ID')): ?> 
              <a class="btn-circle-icon float-sm-right" data-toggle="tooltip" data-placement="right"  title="<?php echo __('Adicionar'); ?>"  href="<?php echo $this->Html->url(array('action'=>'add'));?>">
                <i class="flaticon-add"> </i>
              </a>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="widget-body">
        <table class="table">
          <tr>
            <td><?php echo __('Vigencia del cupón'); ?></td>
            <td><?php echo h($coupon['Coupon']['start_date']); ?> <?php echo h($coupon['Coupon']['end_date']); ?></td>
          </tr>
          <tr>
            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
          </tr>
          <tr>
            <td><?php echo __('Nombre del plan'); ?></td>
            <td><?php echo h($coupon['Plan']['name']); ?></td>
          </tr>
          <tr>
            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
          </tr>
          <tr>
            <td><?php echo __('Descripción del plan'); ?></td>
            <td><?php echo h($coupon['Plan']['description']); ?></td>
          </tr>
          <tr>
            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
          </tr>
          <tr>
            <td><?php echo __('Nombre del destinatario') ?></td>
            <td><?php echo h($coupon['Coupon']['addressee_name']); ?></td>
          </tr>
          <tr>
            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
          </tr>
          <tr>
            <td><?php echo __('Correo del destinatario'); ?></td>
            <td><?php echo h($coupon['Coupon']['email_user']); ?></td>
          </tr>
          <tr>
            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
          </tr>
          <tr>
            <td><?php echo __('Teléfono'); ?></td>
            <td><?php echo h($coupon['Coupon']['phone']); ?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

