<?php echo $this->Html->css('lib/daterangepicker.css',			['block' => 'AppCss']); ?>
<?php $dataCoupon = $this->Session->read("CouponForm"); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Obsequiar cupón'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right"  title="<?php echo __('Listar'); ?>" href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                    <i class="flaticon-list"></i>
                </a>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"coupons",'action'=>'index'));?>"><?php echo __("Cupones") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">

            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('Coupon', array('role' => 'form','data-parsley-validate')); ?>
                    <p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>
                    <div class='form-group'>
                        <?php echo $this->Form->label('Coupon.name',__('Nombre'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'div'=>false, 'maxLength' => 80)); ?>
                    </div>
                    <div class='form-group'>
                        <?php echo $this->Form->label('Coupon.code',__('Código'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('code', array('class' => 'form-control border-input','label'=>false,'div'=>false,'readOnly' => true, 'maxLength' => 80)); ?>
                    </div>
                    <div class='form-group'>
                        <button class="btn btn-primary btn-reecapmeeting-user generar-codigo" id="generar-codigo" type="button">
                            <?php echo __('Generar código');?>
                        </button>
                    </div>
                    
                    <div class='form-group'>
                        <?php echo $this->Form->label('Coupon.addressee_name',__('Nombre del destinatario'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('addressee_name', array('type'=>'text','class' => 'form-control border-input select-type-coupon only-letters-field','label'=>false,'div'=>false, 'value' => $dataCoupon["name"], 'maxLength' => 80)); ?>
                    </div>
                    
                    <div class='form-group'>
                        <?php echo $this->Form->label('Coupon.email_user',__('Correo electrónico'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('email_user', array('type'=>'email','class' => 'form-control border-input select-type-coupon','label'=>false,'div'=>false, 'value' => $dataCoupon["email"], 'maxLength' => 80)); ?>
                    </div>
                
                    <div class="form-group">
                        <label for="DateRange" class="control-label f-blue required"><?php echo __('Fecha inicio - Fecha fin');?></label>
                        <input type="text" name="daterange" readonly="readonly" class="form-control border-input" value="<?php echo @$this->request->data["daterange"]?>" />
                    </div>

                    <div class='form-group'>
                        <?php echo $this->Form->label('Coupon.plan_id',__('Plan'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('plan_id', array('type'=>'text','class' => 'form-control border-input select-type-coupon','label'=>false,'div'=>false,'type' => 'select')); ?>
                    </div>

                    <div class='form-group'>
                        <?php echo $this->Form->label('Coupon.phone',__('Teléfono'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('phone', array('type'=>'text','class' => 'form-control border-input select-type-coupon only-numbers-field','label'=>false,'div'=>false, 'value' => $dataCoupon["telephone"], 'maxLength' => 10,'required' => true)); ?>
                    </div>

                    <div class='form-group'>
                        <button type='submit' class='btn btn-primary btn-reecapmeeting-user'><?php echo __('Guardar')?></button>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
    $this->Html->script('lib/fullcalendar-3.8.2/moment.min.js',	['block' => 'AppScript']);
    $this->Html->script('lib/daterangepicker.js',				['block' => 'AppScript']);
	$this->Html->script('controller/coupons/actions.js',		['block' => 'AppScript']);
	$this->Html->script('lib/parsley/parsley.min.js',			['block' => 'AppScript']);
	if ($this->Session->read('Config.language') == 'esp') {
		$this->Html->script('lib/parsley/i18n/es.js',			['block' => 'AppScript']);
	}
?>