<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>



 <div class="container-fluid">
    <!-- Begin Page Header-->
    
    <!-- End Page Header -->
     <?php echo $this->element('nav_taskee'); ?>

    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                      <div class="widget widget-08 has-shadow">
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header  d-flex align-items-center">
                                        <h2><?php echo __($lista[0]["Slate"]["name"]) ?></h2>
                                        <div class="widget-options">
                                            <div class="dropdown">
                                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                                                    <i class="la la-ellipsis-h"></i>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a href="<?php echo $this->webroot."slatestasks/add/".EncryptDecrypt::encrypt($lista[0]["Slate"]["id"]) ?>" class="dropdown-item"> 
                                                        <i class="la la-plus"></i><?php echo __("Adicionar Tarea") ?>
                                                    </a>
                                                    <?php if($lista[0]["Slate"]["user_id"] == AuthComponent::user("id")): ?>
                                                    <a href="<?php echo $this->webroot."slates/edit/".EncryptDecrypt::encrypt($lista[0]["Slate"]["id"]) ?>" class="dropdown-item"> 
                                                        <i class="la la-edit"></i><?php echo __("Editar Listado") ?>
                                                    </a>
                                                    <a data-id="<?php echo $lista[0]["Slate"]["id"] ?>" href="#" class="dropdown-item faq deleteList"> 
                                                        <i class="la la-remove"></i><?php echo __("Eliminar Listado") ?>
                                                    </a>
                                                    <?php else: ?>
                                                      <a data-id="<?php echo $lista[0]["Slate"]["id"] ?>" href="#" class="dropdown-item faq outList"> 
                                                        <i class="la la-remove"></i><?php echo __("Salirme de la lista") ?>
                                                    </a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                          
                                          <div class="media-body align-self-center ">
                                        <div class="other-message-time ">
                                        <i class="la la-users "></i>
                                          <?php foreach ($lista[0]["Slate"]["SlatesUser"] as $key2 => $value2): ?>

                                            <?php 
                                              if($key2 > 0){
                                                echo " - ";
                                              }
                                              echo $value2["User"]["firstname"]. " ".$value2["User"]["lastname"];
                                              if($value2["role_id"] == 0){
                                                echo " (Admin)";
                                              }elseif($value2["role_id"] == 1){
                                                echo " (Colaborador)";
                                              }elseif($value2["role_id"] == 2){
                                                echo " (Editor)";
                                              }
                                            ?> 
                                          <?php endforeach ?>
                                        </div>
                                      </div>
                                        </div>
                                        <hr>
                                    <!-- End Widget Header -->
                                    <!-- Begin Widget Body -->
                                    <div class="widget-body">
                                       <div class="input-group">
                                            <input id="buscador" type="text" class="form-control no-ppading-right no-padding-left" placeholder=" Buscar Tarea...">
                                        </div>
                                        <div class="widget-body no-padding" style="margin-top: 20px">
                                          <ul class="ticket list-group w-100">
                                            <!-- 01 -->
                                            <?php if(!empty($tareas)): ?>
                                            <?php foreach($tareas as $ke=>$value): ?>
                                            <li class="list-group-item  item task_remove_<?php echo $value["SlatesTask"]["id"] ?>">

                                                <div class="media ">
                                                  <div class="media-left align-self-center pr-4">
                                                        <div class="styled-checkbox mt-2">
                                                                <input data-id="<?php echo $value["SlatesTask"]["id"] ?>" type="checkbox" name="cb10" id="cb10" class="checkTask">
                                                                <label for="cb10"></label>
                                                            </div>
                                                    </div>

                                                    <div class="media-left align-self-center pr-4">
                                                        <img src="<?php echo $this->Html->url('/files/User/'.$value["User"]["img"]); ?>" class="user-img rounded-circle" alt="user img" style="width: 60px;height: 60px">
                                                    </div>
                                                    
                                                    <div class="media-body align-self-center">
                                                        <div class="username">
                                                            <h4><?php echo $value["User"]["firstname"] ?> <?php echo $value["User"]["lastname"] ?></h4>
                                                        </div>
                                                        <div class="msg">
                                                            <p class="nombres">
                                                               <?php echo $value["SlatesTask"]["description"] ?>
                                                            </p>
                                                        </div>
                                                        <div class="status">
                                                          <i class="la la-calendar-check-o"></i> <?php echo $value["SlatesTask"]["deadline_date"] ?>
                                                          <i class="la la-bell"></i> <?php echo $value["SlatesTask"]["date_notidication"] ?>
                                                        </div>
                                                    </div>
                                                    <?php if($lista[0]["Slate"]["user_id"] == AuthComponent::user("id")): ?>
                                                        <div class="media-right pr-3 align-self-center">
                                                            <div class="like text-center">
                                                              <a href="<?php echo $this->webroot ?>slatestasks/edit/<?php echo EncryptDecrypt::encrypt($value["SlatesTask"]["id"]) ?>">
                                                                <i class="la la-edit la-2x"></i>
                                                              </a>
                                                                 
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php if($value["SlatesTask"]["user_id"] == AuthComponent::user("id")): ?>
                                                        <div class="media-right pr-3 align-self-center">
                                                            <div class="like text-center">
                                                              <a href="<?php echo $this->webroot ?>slatestasks/edit_notification/<?php echo EncryptDecrypt::encrypt($value["SlatesTask"]["id"]) ?>">
                                                                <i class="la la-bell la-2x"></i>
                                                              </a>
                                                                 
                                                            </div>
                                                        </div>
                                                    <?php endif?>
                                                </div>
                                            </li>
                                          <?php endforeach; ?>
                                          <?php else: ?>
                                            <div class="alert alert-outline-success" role="alert">
                                              <strong></strong> <?php echo __("Aun no tienes  tareas en esta  lista.") ?>
                                          </div>
                                          <?php endif; ?>
                                            
                                        </ul>

                                          
                                        </div>
                                          








                                        
                                        <!-- Begin List -->




 
                                        <!-- End List -->
                                    </div>
                                    <!-- End Widget Body -->
                                </div>


                                
                            </div>
</div>

<?php
  
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
   $this->Html->script('lib/select2/select2.min.js',               ['block' => 'AppScript']);
  $this->Html->script('lib/bootstrap-tagsinput.min.js',           ['block' => 'AppScript']);
   $this->Html->script('slates.js',           ['block' => 'AppScript']);

  
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
  
   
?>

 

<script type="text/javascript">
    $(document).ready(function(){
  $('#buscador').keyup(function(){
     var nombres = $('.nombres');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.item').show(); 
              }else{
                   $(nombres[i]).parents('.item').hide();
              }
          }
     }
  });
  $('.select2_all').select2({
       
       
      
    });
});

</script>
 
 

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
</style>
 