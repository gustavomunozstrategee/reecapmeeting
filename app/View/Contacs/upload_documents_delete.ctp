<?php if (!empty($documentos)) { ?>
	<?php $i = 1; foreach ($documentos as $value): ?>
	    <div class="div__documentupload">
		<?php 
			$filename  = $value['ContacsDocument']['document'];
			$extension = pathinfo($filename, PATHINFO_EXTENSION);
		?> 
		<?php if($extension == "docx" || $extension == "doc"): ?>
			<img class="img-responsive" title="<?php echo __('Documento').' '.$i ?>" style="height:100px; width:100px;" src="<?php echo $this->Html->url('/img/frontend/admin/icon-word.svg'); ?>"/>	 

		<?php elseif ($extension == "pdf" || $extension == "PDF"): ?>
			<img class="img-responsive" title="<?php echo __('Documento').' '.$i ?>" style="height:100px; width:100px;" src="<?php echo $this->Html->url('/img/frontend/admin/icon-pdf.svg'); ?>"/>
		
		<?php else: ?>
			<img class="img-responsive" title="<?php echo __('Documento').' '.$i ?>" style="height:100px; width:100px;" src="<?php echo $this->Html->url('/document/Contac/default.png'); ?>"/>
		<?php endif; ?>
		<a class="btns btn_downloadDocumento" target="_blank" href="<?php echo $this->Html->url('/document/Contac/'.$value['ContacsDocument']['document']); ?>" ><i class="flaticon-download" aria-hidden="true"></i></a>
		<a class="btns btn_eliminarDocumento"  data-uid="<?php echo $value['ContacsDocument']['id'] ?>" data-name="<?php echo $value['ContacsDocument']['document'] ?>"><i class="flaticon-cancel" aria-hidden="true"></i></a>  
		</div>
		<?php $i++; endforeach; ?>
<?php }else{ ?>
	<p class="text-center"> <?php echo __('No hay documentos.') ?> </p>
<?php } ?>


