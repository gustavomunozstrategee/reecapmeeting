<?php if (!empty($documentos)) { ?>
	<?php $i = 1; foreach ($documentos as $value): ?>
	<div class="row">
		<!-- <div class="col-md-3">	 -->
			<?php 
				$filename  = $value['ContacsDocument']['document'];
				$extension = pathinfo($filename, PATHINFO_EXTENSION);
			?> 
			<?php if($extension == "docx" || $extension == "doc"): ?>
				<a class="link__uploaddocument" target="_blank" href="<?php echo $this->Html->url('/document/Contac/'.$value['ContacsDocument']['document']); ?>" >
					<img class="img-responsive" title="<?php echo __('Documento').' '.$i ?>" style="height:100px; width:100px;"src="<?php echo $this->Html->url('/img/frontend/admin/icon-word.svg'); ?>"/>
				</a>
			<?php elseif ($extension == "pdf" || $extension == "PDF"): ?>
				<a class="link__uploaddocument" target="_blank" href="<?php echo $this->Html->url('/document/Contac/'.$value['ContacsDocument']['document']); ?>" >
					<img class="img-responsive" title="<?php echo __('Documento').' '.$i ?>" style="height:100px; width:100px;"src="<?php echo $this->Html->url('/img/frontend/admin/icon-pdf.svg'); ?>"/>
				</a> 
			<?php else: ?>
				<a class="link__uploaddocument" target="_blank" href="<?php echo $this->Html->url('/document/Contac/'.$value['ContacsDocument']['document']); ?>" >
					<img class="img-responsive" title="<?php echo __('Documento').' '.$i ?>" style="height:100px; width:100px;"src="<?php echo $this->Html->url('/document/Contac/default.png'); ?>"/>
				</a>
			<?php endif; ?> 
		</div>
	<?php $i++; endforeach; ?>
<?php }else{ ?>
	<div class="col-lg-12">
		<p class="text-empty"> <?php echo __('No hay documentos.') ?> </p>
	</div>
<?php } ?>