<div id="update-content-user-contact">
    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
        <thead>
            <tr>
                <th class="table-grid-10"><?php echo __('Fecha inicio') ?></th>
                <th class="table-grid-10"><?php echo __('Fecha fin') ?></th>
                <th class="table-grid-10 text-center"><?php echo __('Descargar acta') ?></th>
                <th class="table-grid-30"><?php echo __('Nombre cliente') ?></th>
                <th class="table-grid-20"><?php echo __('Imagen del cliente') ?></th>
                <th class="table-grid-10"><?php echo __('Proyecto') ?></th>
                <th class="table-grid-10 text-center"><?php echo __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php if(!empty($contacs)):?>
            <?php foreach ($contacs as $contac): ?>
                 <?php $response = $this->Utilities->permissionViewContac($contac);?> 
                <tr>
                    <td class="table-grid-10 td-word-wrap"><?php echo h($contac['Contac']['start_date']); ?></td>
                    <td class="table-grid-10 td-word-wrap"><?php echo h($contac['Contac']['end_date']); ?></td>
                    <td class="table-grid-10 text-center td-word-wrap"><a rel="tooltip" href="<?php echo Router::url('/', true) ?>contacs/download_contac/<?php echo EncryptDecrypt::encrypt($contac["Contac"]["id"]);?>.pdf" title="<?php echo __('Descargar acta'); ?>" data-toggle="tooltip"><i class="flaticon-file table-icon f-blue"></i></a></td>
                    <td class="table-grid-30 td-word-wrap"><?php echo h($contac['Client']['name']); ?></td>
                    <td class="table-grid-20 td-word-wrap"><img src="<?php echo $this->Html->url('/files/Client/'.$contac['Client']['img']); ?>" width="50px" class="img-responsive" alt=""/></td>
                    <td class="table-grid-10 td-word-wrap"><?php echo h($contac['Project']['name']); ?></td>
                    <td class="table-grid-10 td-actions text-center">
                     
                    <?php if($response == true):?> 
                        <a rel="tooltip"  href="<?php echo $this->Html->url(array('action' => 'view','controller'=>'contacs', EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" title="<?php echo __('Ver acta'); ?>" class="btn-xs" data-toggle="tooltip">
                            <i class="fa fa-eye"></i>
                        </a>
                    <?php endif; ?>
                    </td>
                </tr> 
            <?php endforeach ?>
        <?php else: ?>
            <tr>
                <td class="text-center" colspan="7"><?php echo __("No hay actas relacionadas.");?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table> 
    <div class="table-pagination">
        <div>
        <?php 
            $this->Paginator->options(array('update' => '#update-content-user-contact','url'=>array('controller'=>'contacs','action'=>'my_contacs', EncryptDecrypt::encrypt($userId))));
        ?>
        <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
        </div>
    
    <div>
    <ul class="pagination f-paginationrecapmeeting">
        <?php 
            echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
            echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
        ?>
    </ul>
   </div>

    </div>
    <?php echo $this->Js->writeBuffer(); ?>  
</div>