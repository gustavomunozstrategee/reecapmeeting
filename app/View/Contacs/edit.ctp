<?php $permissionAction = $this->Utilities->check_team_permission_action(array("index"), array("contacs")); ?>
 <div class="row">
                            <div class="page-header">
                                <div class="d-flex align-items-center">
                                    <h2 class="page-header-title"><?php echo __('Actas'); ?></h2>
                                    <div>
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>"><i class="ti ti-home"></i></a> </li>
                                            <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>contacs"><?php echo __('Actas'); ?></a></li>
                                            <li class="breadcrumb-item active"><?php echo __('Crear acta'); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
<div class="row flex-row">
  <div class="col-xl-12">
    <div class="widget widget-07 has-shadow">
      <div class="widget-header bordered d-flex align-items-center">
                 
                  <?php 
                    $infoMeeting = array();
                    $meetingContacSelected = $this->Session->read("MeetingContacSelected"); 
                    if(empty($meetingContacSelected)){
                      $meetingId = NULL;
                    } else {
                      $meetingId = $meetingContacSelected["Meeting"]["id"];
                    }
                  ?> 
                  <?php echo $this->element('contact_previous_meeting_created', array('infoMeeting' => $infoMeeting, 'meetingId' => $meetingId)) ?>
                 
                <div class="widget-options">
                    <div class="" role="">
                        <a href="<?php echo $this->webroot ?>contacs"><button type="button" class="btn btn-secondary "><i class="ti ti-clipboard"></i><?php echo __("Listar actas") ?></button></a>
                    </div>
                </div>
            </div>
      
      <div class="widget-body">
        <?php echo $this->Form->create('Contac', array('role' => 'form','type' => 'file', 'class'=>'form-horizontal frm-send-contact-information')); ?>
          <?php echo $this->Form->input('duration', array('type'=>'hidden', 'value'=>'00:00:00','label'=>false, 'div'=>false)); ?>
          <?php echo $this->Form->input('firebase_code', array('type'=>'hidden', 'label'=>false, 'div'=>false, 'class'=>'txtFirebaseCode')); ?>
          <?php echo $this->Form->input('id',array('value'=> EncryptDecrypt::encrypt($data['Contac']['id']))); ?>
          <div class="form-group row d-flex align-items-center mb-5">
              <div class="col-lg-3">
                <h4><?php echo __('Empresa'); ?></h4>
              </div>
              <div class="col-lg-9">
                <b><?php echo $data["Team"]["name"]?>  </b>
              </div>
          </div>

           

          <div>
            <?php echo $this->Form->hidden('team_id', array('class' => 'form-control border-input', 'required' => true, 'label' => false, 'div' => false, 'type' => 'text')); ?>
          </div>

          <?php $permissionClient  = $this->Utilities->check_team_permission_action(array("add"), array("clients"), $data["Contac"]["team_id"]); ?>
          <?php $permissionProject = $this->Utilities->check_team_permission_action(array("add"), array("projects"), $data["Contac"]["team_id"]); ?> 
          
          <div class="form-group row d-flex align-items-center mb-5">
                        <div class="col-lg-3">
                            <h4>
                                <?php echo __('Cliente'); ?>
                                <?php if(isset($permissionClient["add"])) : ?> 
                                    <a href=""  class="btn-select2" data-toggle='modal' id="btn_modalAddClient" title="<?php echo __('Registrar un nuevo cliente'); ?>" >
                                       <i class="la la-plus-circle" ></i>
                                    </a>
                                <?php endif; ?>                          
                            </h4>
                        </div>
                        <div class="col-lg-9">
                            <div class=" form-project form-project-max">
                                <div id="load_clients_team" >
                                    <?php echo $this->Form->input('Contac.client_id', array('class' => 'selectpicker show-menu-arrow form-control', 'required' => false, 'label' => false, 'div' => false,'value' =>  @$this->request->data["Contac"]["client_id"], 'options' => $clients, 'empty' => __("Seleccione una opción"),"data-live-search='true'")); ?>
                                </div>
                            </div>
                            <div class="form-group form-project form-project-max">
                            </div>
                        </div>
                    </div>
                    <div class="em-separator separator-dashed"></div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <div class="col-lg-3">
                            <h4>
                                <?php echo __('Proyecto'); ?>
                                <?php if(isset($permissionProject["add"])) : ?> 
                                    <a class="btn-select2" data-toggle='modal' id="btn_modalAddProject" title="<?php echo __('Registrar un nuevo cliente'); ?>">
                                        <i class="la la-plus-circle" ></i>
                                    </a>
                                <?php endif; ?>                          
                            </h4>
                        </div>
                        
                        <div class="col-lg-9">
                                                    
                            <div class="form-project form-project-max">
                                
                                
                                <div id="load_projects_client">
                                    <?php echo $this->Form->input('Contac.project_id', array('class' => 'selectpicker show-menu-arrow form-control', 'required' => false, 'label' => false, 'div' => false, 'value' => @$this->request->data["Contac"]["project_id"], 'options' => $projects, 'empty' => __("Seleccione una opción"),"data-live-search='true'")); ?>
                                </div>
                            </div>
                        </div>
                    </div>
          
          <div class="em-separator separator-dashed"></div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <div class="col-lg-3">
                            <h4><?php echo __('Duración de la reunión'); ?></h4>
                        </div>
                        
                        <div class="col-lg-9">
                            <div class="form-group">
                                <div class="input-form">
                                    <?php echo $this->Form->label('Contac.start_date', __('Fecha y hora del inicio de la reunión'), array('class' => 'control-label f-blue')); ?>
                                    <div class='input-group date' id='datetimepickerOn'>
                                        <?php echo $this->Form->input('start_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Calendario'), 'type' => 'text', 'readonly' => true)); ?>
                                         
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-form">
                                    <?php echo $this->Form->label('Contac.end_date', __('Fecha y hora del fin de la reunión'), array('class' => 'control-label f-blue lbl_endDate')); ?>
                                    <div class='input-group date' id='datetimepickerOff'>
                                        <?php echo $this->Form->input('end_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Calendario'), 'type' => 'text', 'readonly' => true)); ?>
                                         
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style type="text/css">
                        span .selection{
                            width: 100% !important;
                        }
                        .bootstrap-tagsinput{
                            width: 100% !important;
                        }
                        .select2-container{
                            width: 100% !important;
                        }
                        .bootstrap-tagsinput .tag {
    
                        color: #999 !important;
                        background: #e4e4e4 !important;
                        border-radius: 5px !important;
                        padding: 0px 10px 0px 10px !important;
                    }
                    .dropdown-menu.show{
                        z-index: 9999;
                    }
                    .bootstrap-select .dropdown-toggle .filter-option{
                        z-index: 10;
                    }
                    </style>
                    <div class="em-separator separator-dashed"></div>
          
          <div class="form-group row d-flex align-items-center mb-5">
            <label class="col-lg-3 form-control-label"><?php echo __('Asistentes a la reunión'); ?></label>
            <div class="col-lg-9">
              <!-- COLABORADORES -->
              <div>
                <?php echo $this->Form->label('Contac.funcionarios', __('Colaboradores'), array('class' => 'control-label f-blue')); ?>
                <!-- <a class="btn-select2" data-toggle='modal' id="btn_modalAddFuncionario" title="<?php echo __('Registrar un nuevo funcionario de la empresa'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a> -->
              </div>
              <div id="load_collaborators_team">
                <?php echo $this->Form->input('Contac.funcionarios', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple','value' => @$this->request->data["Contac"]["funcionarios"], 'options' => $collaborators)); ?>            
              </div> 
              <!-- EXTERNOS -->
              <div>
                <?php echo $this->Form->label('Contac.externos', __('Asistentes externos'), array('class' => 'control-label f-blue')); ?>
                <!-- <a class="btn-select2" data-toggle='modal' id="btn_modalAddEmployee" title="<?php echo __('Registrar un nuevo asistente del cliente'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a> -->
              </div>
              <div id="load_assitants_team"> 
                <?php echo $this->Form->input('Contac.externos', array('class' => 'form-control select2_all', 'label' => false, 'type' => 'select', 'multiple' => 'multiple','value' =>    @$this->request->data["Contac"]["externos"], 'options' => $assistants)); ?> 
              </div>
            </div>
          </div>
          
          <div class="form-group row d-flex align-items-center mb-5">
            <div class="col-lg-3"></div>
            <div class="col-lg-9">
              <div class="checkbox checkkbox-label">
                <div id="content-approval-solicitud" style="display: block;">
                  <label for="cbox_perrmission_report_tickets_area">
                    <?php if($data["Contac"]["approval"] == configure::read("ENABLED")):?>
                      <input type="checkbox" name="data[Contac][approval]" value="1" id="requestApproval" checked="checked">
                      <?php echo __('¿Solicitar aprobación?') ?>
                    <?php else: ?>
                      <input type="checkbox" name="data[Contac][approval]" value="1" id="requestApproval">
                      <?php echo __('¿Solicitar aprobación?') ?>
                    <?php endif; ?>
                  </label>
                </div>
              </div>
              <div id="content-approval">
                <div>
                  <?php echo $this->Form->label('Contac.approvalusers', __('Solicitar aprobación de usuarios'), array('class' => 'control-label f-blue')); ?>
                </div>
                <div id="load_content_user_approval">
                  <?php echo $this->Form->input('approvalusers', array('class' => 'form-control select2_all select_approval_user', 'label' => false, 'div' => false, 'multiple' => 'multiple', 'type' => 'select')); ?> 
                </div>
                <div class="form-group">
                  <?php echo $this->Form->label('Contac.date_limit', __('Fecha límite para calificar'), array('class' => 'control-label f-blue')); ?>
                  <div class='input-group date' id='datetimepickerdatelimit'>
                    <?php echo $this->Form->input('limit_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Fecha límite de calificación'), 'type' => 'text', 'readonly' => true, 'value' => $dateLimitCalified)); ?>
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-5">
            <label class="col-lg-3 form-control-label"></label>
            <div class="col-lg-9">
              <div class="checkbox checkkbox-label">
                <label class="f-blue font-bold">
                  <?php if($data["Contac"]["password"] != ""):?>
                  <?php echo $this->Form->checkbox('protegida', array('hiddenField' => false, 'div' => false, 'label' => false, 'type' => 'checkbox', 'id' => 'ContacProteger','checked' => 'checked')); ?>
                  <?php echo __("Proteger con contraseña") ?>
                  <?php else: ?>
                  <?php echo $this->Form->checkbox('protegida', array('hiddenField' => false, 'div' => false, 'label' => false, 'type' => 'checkbox', 'id' => 'ContacProteger')); ?>
                  <?php echo __("Proteger con contraseña") ?>
                  <?php endif; ?>
                </label>
              </div>
              <div class="input-group">
                <?php echo $this->Form->input('password', array('class' => 'form-control border-input', 'type' => 'password', 'placeholder' => __('Contraseña'), 'label' => false, 'div' => false, 'maxlength' => 4, 'autocomplete' => 'new-password')); ?>
                <span class="input-group-btn">
                  <button class="btn btn-default" id="btn_generatePassword" type="button"><span class="glyphicon glyphicon-refresh f-blue"></span></button>
                </span>
              </div>
              <div>
                <button type="button" id="btn_ocultarPassword" class="link-show-password btn btn-info"><?php echo __("Ocultar contraseña") ?></button>
                <button type="button" id="btn_verPassword"     class="link-show-password btn btn-info"><?php echo __("Mostrar contraseña") ?></button>
              </div>
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-5">
            <label class="col-lg-3 form-control-label"><?php echo __('Copias de correos electrónicos'); ?></label>
            <div class="col-lg-9">
              <?php echo $this->Form->input('copiesI',array('class' => 'form-control border-input','label'=>false,'div'=>false,'data-role'=>'tagsinput','value'=>$data['Contac']['copies']));?>
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-5">
            <div class="col-lg-12">
              <div id="permission_create_template">                        
                <!-- MÉTODO AJAX QUE DEVOLVERÁ SI EL USUARIO DEPENDIENDO DEL EQUIPO DE TRABAJO SELECCIONADO PUEDE CREAR PLANTILLAS -->
              </div> 
            </div>
            <div class="col-lg-12">
              <div class="listTemplate"></div> 
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-5">
            <label class="col-lg-12"><?php echo __("Descripción");?></label>
            <div class="col-lg-12">
              <?php echo $this->Form->input('description', array('class' => 'form-control border-input', 'label'=>false,'placeholder' => __('Descripción de la reunión'),'div'=>false)); ?>
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-5">
            <div class="col-lg-12">
              <div id="show-content-meeting-day-choose"> </div>
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-10">
            <div class="col-lg-12">
              <div class="">
                <table class="table">
                  <thead>
                    <tr>
                      <th class="text-center" colspan="4"><label class="f-blue font-bold"><?php echo __("Adicionar compromiso");?></label></th>
                    </tr>
                    
                  </thead>
                  <tbody class="hijos">
                    <tr  id="nuevoCompromiso" > 
                                            <td colspan="4">
                                                <div class="row">
                                                    <div class="col-md-7 form-group">
                                                        <?php echo $this->Form->textarea('Commitments.NUEVO.description', array('class' => 'form-control resize-none COMMITMENT_DESC_REQUIRE', 'placeholder' => __('Compromiso...'), 'label' => false, 'rows' => 7)) ?>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-project form-project-max">
                                                                 <div   id="load_content_users_commitment" style="margin-bottom: 20px;">
                                                                  <label><?php echo __("Responsable") ?></label>
                                                                    <?php echo $this->Form->input('Commitments.NUEVO.asistente', array('empty'=>__('Seleccionar...'), 'class' => 'form-control  selectpicker show-menu-arrow selectpicker_comimment ', 'label' => false, 'type' => 'select','options' => $allUsers,"data-live-search='true'")); ?>
                                                                  </div>
                                                                </div> 
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-7">
                                                                      <label><?php echo __("Fecha límite") ?></label>
                                                                       <div class="input-group  date_all_contac date date-input-content">
                                                                    <?php echo $this->Form->input('Commitments.NUEVO.fecha', array('readonly', 'placeholder' => __('Fecha límite'), 'class' => 'form-control COMMITMENT_DATE_REQUIRE date_all', 'label' => false, 'div' => false)) ?>
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                    </span>
                                                                </div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                      <label>&nbsp</label>
                                                                        <button style="width: 100%;" class="addcompromiso btn btn-primary" type="button"><?php echo __('Añadir compromiso') ?></button>
                                                                        <button style="width: 100%;" class="btn btn-primary f-btn-delete btn-remove-nuevo-compromiso d-none" data-id="NUEVO" type="button"><?php echo __("x"); ?></button>
                                                                    
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>



                                                   <hr>
                                            </td>
                                            <?php echo $this->Form->hidden('Commitments.NUEVO.id'); ?>

                                           
                     </tr>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
          <div class="form-group row d-flex align-items-center mb-10">  
            <div class="col-lg-12 mt-10">
              <div class="">
                <table id="commitments" class="table ">
                  <thead>
                    <tr>
                      <th class="text-center" colspan="4"><label class="f-blue"><?php echo __('Compromisos adicionados') ?></label></th>
                    </tr>
                  </thead>
                  <tbody class="hijosF"></tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-5">
            <div class="col-lg-12">
              <?php if(isset($permissionAction["index"])) : ?> 
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                  <div class="bg-filter flex-justify-end">
                    <button type='button' class='btn btn-primary btn-blue btnArchivos mxy-10' id="btnGuardar" style="margin:10px">
                      <?php echo __('Guardar y enviar') ?>
                    </button>
                    <button type='button' class='btn btn-primary btn-blue btnArchivos' id="btnSaveBorrador" style="margin:10px">
                      <?php echo __('Guardar borrador') ?>
                    </button> 
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </div>

          <div class="form-group row d-flex align-items-center mb-5">
            <label class="col-lg-3 form-control-label"></label>
            <div class="col-lg-9">
              
            </div>
          </div>
        <?php echo $this->Form->end(); ?>
        <?php if(isset($permissionAction["index"])) : ?> 
          <div class="row">
            <div class="col-lg-12">
              <div class="bg-filter">
                <div class="flex-space-between">
                  <p><span class="f-blue font-bold"><?php echo __('Imágenes') ?></span>&nbsp;<?php echo __('Selecciona las imágenes que deseas adjuntar en el acta.') ?></p>
                  <div>
                    <?php echo $this->Html->image('frontend/admin/icon-jpg.svg', array('height' => 'auto;','width' => '60px;')); ?>
                    <?php echo $this->Html->image('frontend/admin/icon-png.svg', array('height' => 'auto;','width' => '60px;')); ?>
                  </div>
                </div>
                <div class="scroll-uploadimg">
                  <form method="post" action="saveImage" class="dropzone" id="dropzoneImage" enctype='multipart/form-data'></form>
                  <div id="imageDropzone" class="imageDropzone-div"></div>
                </div>
                <div class="flex-space-between">
                  <p><span class="f-blue font-bold"><?php echo __('Documentos') ?></span>&nbsp;<?php echo __('Selecciona los documentos que deseas adjuntar en el acta.') ?></p>
                  <div>
                    <?php echo $this->Html->image('frontend/admin/icon-pdf.svg', array('height' => 'auto;','width' => '60px;')); ?>
                    <?php echo $this->Html->image('frontend/admin/icon-word.svg', array('height' => 'auto;','width' => '60px;')); ?>
                  </div>
                </div>
                <div class="scroll-uploadimg">
                  <form method="post" action="saveImage" class="dropzone" id="dropzoneDocument" enctype='multipart/form-data'></form>
                  <div id="documentDropzone" class="imageDropzone-div"></div>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modalPreloadFirebase" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-recapmeeting" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?php echo __('Cargando los datos') ?></h4>
        </div>
      </div>
    </div>
  </div>
  <?php
    echo $this->element('modals_contact');
    echo $this->element('form_compromises');
  ?>
</div>

<?php
  echo $this->Html->scriptBlock("data_firebase = ".$data_firebase, ['block' => 'GlobalScript']);
  $this->Html->script('controller/contacs/initChronometer.js',    ['block' => 'AppScript']);
  $this->Html->script('lib/select2/select2.min.js',               ['block' => 'AppScript']);
  $this->Html->script('lib/bootstrap-tagsinput.min.js',           ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);

  $this->Html->script('lib/dropzone.min.js',                      ['block' => 'AppScript']);
  $this->Html->script('lib/ckeditor/ckeditor.js',                 ['block' => 'AppScript']);
  $this->Html->script('controller/contacs/actions.js?v=656565656',            ['block' => 'AppScript']);
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
  $this->Html->scriptBlock("EDIT.initElementEdit();",              ['block' => 'AppScript']); 
  $this->Html->script('controller/contacs/actionsV2.js?v=7675756765',           ['block' => 'AppScript']);
?>
<script type="text/javascript">
  $(function(){
    $(document).on('submit', '.frm-send-contact-information', function(){
      $('#preloader').show();
    });  
  })
</script>
<style>
.bootstrap-select .dropdown-menu { max-width: 100% !important; }
  .scroll-uploadimg{
    height:230px;
    overflow-y:auto;
    border:1px solid #cccccc;
}

.imageDropzone-div{
    display:flex;
    flex-direction: row;
    flex-wrap:wrap;
    justify-content:flex-start;
    align-items: flex-start;
    /*border:1px solid #cccccc;*/
}

.imageDropzone-div .showImages .img-size-upload {
    width:130px;
}

.imageDropzone-div .img-size-upload{
    width:130px;
}

.scroll-height-250{
  height:250px;
  overflow-y:auto;
}

.scroll-height-150{
  height:150px;
  overflow-y:auto;
}

.imageDropzone-div .link__uploadimg,
.imageDropzone-div .link__uploaddocument{
   padding:0.5em;
}

.div__imageupload .btn_eliminarImagen {
    right: 0px;
    background-color: #878787;
}

.div__documentupload .btns, .div__imageupload .btns {
    position: absolute;
    top: 0;
    width: 20px;
    height: 20px;
    display: block;
    border-radius: 50%;
    line-height: 15px;
    cursor: pointer;
    text-align: center;
}
.div__documentupload .btns i:before, .div__imageupload .btns i:before {
    color: #ffffff;
    margin-left: 0;
    font-size: 10px;
}
.flaticon-cancel:before {
    content: "\f12b";
}
[class^="flaticon-"]:before, [class*=" flaticon-"]:before, [class^="flaticon-"]:after, [class*=" flaticon-"]:after {
    font-family: Flaticon;
    font-size: 20px;
    font-style: normal;
    margin-left: 20px;
}
.div__documentupload .btn_downloadDocumento {
    right: 0;
    background-color: #1e212a;
}
.div__documentupload .btns, .div__imageupload .btns {
    position: absolute;
    top: 0;
    width: 20px;
    height: 20px;
    display: block;
    border-radius: 50%;
    line-height: 15px;
    cursor: pointer;
    text-align: center;
}
.flaticon-download:before {
    content: "\f138";
}
.div__documentupload .btn_eliminarDocumento {
    right: 20px;
    background-color: #878787;
}
.div__documentupload, .div__imageupload {
    position: relative;
    padding: 0.5em;
}
div#imageDropzone>p.text-center,div#documentDropzone>p.text-center  {
    padding: 5px;
}
</style>