<?php if (isset($calificaciones[0])): ?>
    <div class="height-scroll">
	<div class="table-responsive" id="update-content-user-contact">
		<table cellpadding="0" cellspacing="0" class="table table-striped table-hover f-table-grid-layout-fixed">
			<thead>
				<tr>						
					<th class="table-grid-20 f-blue font-bold"><?php echo __('Usuario'); ?></th>
					<th class="table-grid-20 f-blue font-bold"><?php echo __('Calificación'); ?></th>
					<th class="table-grid-40 f-blue font-bold"><?php echo __('Comentario'); ?></th>
					<th class="table-grid-20 f-blue font-bold"><?php echo __('Fecha de calificación'); ?></th>
				</tr>
			</thead>
			<tbody>			 
				<?php foreach ($calificaciones as $assistant): ?>						 
					<tr> 
						<td class="table-grid-20 td-word-wrap">
						<?php if(empty($assistant['User']['firstname'])): ?>
							<?php echo h($assistant['User']['email']); ?> &nbsp;
						<?php else: ?>
							<?php echo h($assistant['User']['firstname'] . ' ' . $assistant['User']['lastname']); ?> &nbsp;
						<?php endif; ?>
						</td>		
						<td class="table-grid-20 td-word-wrap">
							<?php echo $this->Utilities->viewQualification($assistant['QualificationContact']['qualification']); ?>
						</td>
						<td class="table-grid-40 td-word-wrap"><?php echo !empty($assistant['QualificationContact']['commentary']) ? $assistant['QualificationContact']['commentary'] : __("No existen comentarios."); ?></td>
						<td class="table-grid-20 td-word-wrap"><?php echo $assistant['QualificationContact']['created']; ?></td>
				 	</tr> 
				<?php endforeach; ?>
			</tbody>
		</table>
        </div>
		
		<div class="table-pagination">
        <div>
		<?php 
            $this->Paginator->options(array('update' => '#update-content-user-contact','url'=>array('controller'=>'contacs','action'=>'view_hystory_approved', EncryptDecrypt::encrypt($contac_id))));
        ?>
			<small>
				<?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?>
			</small>
	   </div>

	   <div>
	      <ul class="pagination f-paginationrecapmeeting">
	        <?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
		</div>
		</div>
		<?php echo $this->Js->writeBuffer(); ?> 
	</div>
<?php else: ?>
	<p class="text-center"><?php echo __('Aún no se encuentran calificaciones para el acta.') ?></p>
<?php endif; ?>