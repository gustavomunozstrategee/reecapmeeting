<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Ingresa la contraseña del acta'); ?>
            </h2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
            	<h2><?php echo __('Debes ingresar la contraseña del acta correspondiente.'); ?></h2>
            </div>
            <div class="widget-body">
				<?php echo $this->Form->create('Contac', array('role' => 'form','data-parsley-validate')); ?>	
					<div class="input-group">
						<?php echo $this->Form->input('password',array('class' => 'form-control border-input','type'=>'password','placeholder' => __('Contraseña'),'label'=>false,'div'=>false,'maxLength' => 4));?>
						<div class="input-group-btn">
							<button type='button' class='btn btn-primary btnArchivos' id="btnPassword"><?php echo __('Enviar contraseña') ?></button>
						</div>
					</div>
				<?php echo $this->Form->end(); ?>								
            </div>
        </div>
    </div>
</div>
<?php
	$this->Html->script('controller/contacs/actions.js',['block' => 'AppScript']);
	$this->Html->scriptBlock("EDIT.enterPassword(); ",['block' => 'AppScript']);
?>