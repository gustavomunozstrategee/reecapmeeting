<?php $permissionAction = $this->Utilities->check_team_permission_action(array("index"), array("contacs")); ?>
<?php 
    $infoMeeting = $this->Session->read("MeetingContac");
    if(empty($infoMeeting)){
        if(!empty($data)){
            $meetingId = $data["Contac"]["meeting_id"];
        } else {
            $meetingId = NULL;
        }
    } else {
        $meetingId = $infoMeeting["Meeting"]["id"];
    }
?>
<?php $permissionClient  = $this->Utilities->check_team_permission_action(array("add"), array("clients")); ?>
<?php $permissionProject = $this->Utilities->check_team_permission_action(array("add"), array("projects")); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"></h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>"><i class="ti ti-home"></i> <?php echo __("Inicio") ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>contacs"><?php echo __("Actas") ?></a></li>
                    <li class="breadcrumb-item active"><?php echo __("Registrar acta") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->create('Contac', array('role' => 'form', 'datas-parsley-validate', 'type' => 'file','novalidate' => true)); ?>
<?php echo $this->Form->input('duration', array('type'=>'hidden', 'value'=>'00:00:00','label'=>false, 'div'=>false)); ?>
<div class="modal fade" id="modalChooseTeamWork" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo __('Seleccionar empresa') ?></h4>
            </div>
            <div class="modal-body"> 
                <div class="section-title">
                    <h5 style="margin-bottom: 8px;"><?php echo __('Empresa') ?></h5>

                    <?php if(!empty($infoMeeting)):?>
                    <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input ', 'required' => true, 'label' => false, 'div' => false, 'empty' => __('Selecciona una opción'), 'value' => $infoMeeting["Meeting"]["team_id"])); ?> 
                <?php else: ?>
                    <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input ', 'required' => true, 'label' => false, 'div' => false, 'empty' => __('Selecciona una opción'))); ?> 
                <?php endif; ?>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger"  onclick="location.href='<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'index')); ?>'">
                <i class="la la-long-arrow-left"></i>
                <?php echo __('Regresar') ?>
            </button>
            <button type="button" class="btn btn-success" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'teams','action'=>'add')) ?>'">
                <?php echo __('Crear empresa') ?>
                <i class="la la-plus-circle"></i> 
            </button>
        </div>
        </div>
    </div>
</div>

<div class="row flex-row">
                            <div class="col-12">
                                <!-- Form -->
                                <div class="widget has-shadow">
                                    <div class="widget-header bordered no-actions  align-items-center">

                  
                                        <h4><?php echo __('Registrar acta'); ?></h4>

                   
                
                                    </div>

                                    <div class="widget-body">
                                        <form class="form-horizontal">
                                            <div class="widget-header d-flex align-items-center">
                                                <h2></h2>
                                                <div class="widget-options">
                                                     
                                                    <?php if(isset($permissionAction["index"])) : ?> 
                                                        <button type='button' class='btn btn-success' id="btnAutoGuardado"><i class="ti ti-save"></i><?php echo __('Habilitar autoguardado') ?>
                                                        </button>
                                                     <?php endif ?>
                                                </div>
                                            </div>
                                            <div class="form-group row d-flex align-items-center mb-5">
                                                <label class="col-lg-2 form-control-label"><?php echo __('Cliente') ?></label>
                                                <div class="col-lg-10">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                 
                                                                <?php if(isset($permissionClient["add"])) : ?> 
                                        <a class="btn-select2" data-toggle='modal' href="#" id="btn_modalAddClient" title="<?php echo __('Registrar un nuevo cliente'); ?>"><i class="la la-plus-circle" aria-hidden="true"></i></a>
                                    <?php endif; ?>
                                                            </span>
                                                            <?php echo $this->Form->input('Contac.client_id', array('class' => 'form-control border-input ', 'required' => false, 'label' => false, 'div' => false,'value' =>  @$this->request->data["Contac"]["client_id"], 'options' => $clients, 'empty' => __("Seleccione una opción"))); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                 
                                                 
                                            </div>
                                            <hr>


                                            <div class="form-group row d-flex align-items-center mb-5">
                                                <label class="col-lg-2 form-control-label"><?php echo __('Cliente') ?></label>
                                                <div class="col-lg-10">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <?php if(isset($permissionProject["add"])) : ?> 
                                                                      <a class="" href="" data-toggle='modal' id="btn_modalAddProject" title="<?php echo __('Registrar un nuevo cliente'); ?>"><i class="la la-plus-circle" aria-hidden="true"></i></a>
                                                                <?php endif; ?> 
                                                            </span>
                                                            <?php echo $this->Form->input('Contac.project_id', array('class' => 'form-control border-input ', 'required' => false, 'label' => false, 'div' => false, 'value' => @$this->request->data["Contac"]["project_id"], 'options' => $projects, 'empty' => __("Seleccione una opción"))); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>








                                            <div class="form-group row d-flex align-items-center mb-5">
                                                <label class="col-lg-2 form-control-label"><?php echo __('Cliente') ?></label>
                                                <div class="col-lg-10">
                                                   

                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group row d-flex align-items-center mb-5">
                                                <label class="col-lg-2 form-control-label"><?php echo __('Cliente') ?></label>
                                                <div class="col-lg-10">
                                                   

                                                </div>
                                            </div>
                                            <hr>
                                        </form>
                                    </div>
                                </div>
                                <!-- End Form -->
                            </div>
</div>













<div class="backg-white">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="flex-space-between">
                <div>
                    <h1 class="f-title"><?php echo __('Registrar acta'); ?></h1> 
                    <?php if(isset($permissionAction["index"])) : ?> 
                        <a class="btn-circle-icon" href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                            <i class="flaticon-list"></i>
                        </a>
                    <?php endif ?> 
                </div>
                <div class="mt-20">
                    
                </div> 
            </div>
        </div>
    </div>
    <div class="todo-contenido">
        <?php 
            $infoMeeting = $this->Session->read("MeetingContac");
            if(empty($infoMeeting)){
                if(!empty($data)){
                    $meetingId = $data["Contac"]["meeting_id"];
                } else {
                    $meetingId = NULL;
                }
            } else {
                $meetingId = $infoMeeting["Meeting"]["id"];
            }
        ?>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="flex-space-between">
                    <div id="show_last_contac"><!--MOSTRAR LA OPCIÓN DE VER EL ACTA ANTERIOR--></div>
                    <?php echo $this->element('contact_previous_meeting_created', array('infoMeeting' => $meetingId)) ?>
                </div>  
            </div>     
        </div>
        
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="bg-filter">
                        <!-- Empresa -->
                        <div class="row-flex">
                            <div class="col-30-flex">
                                <label class="f-blue"><?php echo __('Empresa'); ?></label>
                            </div>
                            <div class="col-70-flex">
                                <a data-toggle="modal" href="#modalChooseTeamWork"><?php echo __("Cambiar empresa")?></a>                         
                            </div>
                        </div>
                        <?php echo $this->Form->input('meeting_id', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'readOnly' => true, 'type' => 'hidden')); ?>
                        <div id="permission_white_label">
                        <!-- AQUI SE CARGARÁ SI EL EQUIPO DE TRABAJO TIENE PERMISO DE SUBIR LA MARCA BLANCA-->
                        </div>    
                        <?php $permissionClient  = $this->Utilities->check_team_permission_action(array("add"), array("clients")); ?>
                        <?php $permissionProject = $this->Utilities->check_team_permission_action(array("add"), array("projects")); ?>
                        <div class="row-flex">   
                            <div class="col-30-flex">
                                <?php echo $this->Form->label('Contac.client_id', __('Cliente'), array('class' => 'control-label f-blue hidden-xs')); ?>
                                <div id="show_img_client"></div>
                            </div>

                            <div class="col-70-flex">
                                <div class="form-group">
                                    <?php echo $this->Form->label('Contac.client_id', __('Cliente'), array('class' => 'control-label f-blue')); ?>
                                    <?php if(isset($permissionClient["add"])) : ?> 
                                        <a class="btn-select2" data-toggle='modal' id="btn_modalAddClient" title="<?php echo __('Registrar un nuevo cliente'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a>
                                    <?php endif; ?>

                                    <div id="load_clients_team">
                                        <?php echo $this->Form->input('Contac.client_id', array('class' => 'form-control border-input select2_all', 'required' => false, 'label' => false, 'div' => false,'value' =>  @$this->request->data["Contac"]["client_id"], 'options' => $clients, 'empty' => __("Seleccione una opción"))); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-flex">   
                            <div class="col-30-flex">
                                <?php echo $this->Form->label('Contac.project_id', __('Proyecto'), array('class' => 'control-label f-blue hidden-xs')); ?>
                            </div>

                            <div class="col-70-flex project-table-section d-flex">
                                <div id="show_img_project" class="image-project"></div>                        
                                <div class="form-group form-project form-project-max">
                                    <?php echo $this->Form->label('Contac.project_id', __('Proyecto'), array('class' => 'control-label f-blue')); ?>
                                    <?php if(isset($permissionProject["add"])) : ?> 
                                        <a class="btn-select2" data-toggle='modal' id="btn_modalAddProject" title="<?php echo __('Registrar un nuevo cliente'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a>
                                    <?php endif; ?>
                                    <div id="load_projects_client">
                                        <?php echo $this->Form->input('Contac.project_id', array('class' => 'form-control border-input select2_all', 'required' => false, 'label' => false, 'div' => false, 'value' => @$this->request->data["Contac"]["project_id"], 'options' => $projects, 'empty' => __("Seleccione una opción"))); ?>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row-flex">   
                            <div class="col-30-flex">
                                <label class="f-blue font-bold"><?php echo __('Duración de la reunión'); ?></label>
                            </div>
                            <div class="col-70-flex">
                                <div class="form-group">
                                    <div class="input-form">
                                        <?php echo $this->Form->label('Contac.start_date', __('Fecha y hora del inicio de la reunión'), array('class' => 'control-label f-blue')); ?>
                                        <div class='input-group date' id='datetimepickerOn'>
                                            <?php echo $this->Form->input('start_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Calendario'), 'type' => 'text', 'readonly' => true)); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-form">
                                        <?php echo $this->Form->label('Contac.end_date', __('Fecha y hora del fin de la reunión'), array('class' => 'control-label f-blue lbl_endDate')); ?>
                                        <div class='input-group date' id='datetimepickerOff'>
                                            <?php echo $this->Form->input('end_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Calendario'), 'type' => 'text', 'readonly' => true)); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-flex">
                            <div class="col-30-flex">
                                <label class="f-blue font-bold"><?php echo __('Asistentes a la reunión'); ?></label>
                            </div>
                            <div class="col-70-flex"> 
                                <!-- COLABORADORES -->
                                <div class="form-group">
                                    <div>
                                        <?php echo $this->Form->label('Contac.funcionarios', __('Colaboradores'), array('class' => 'control-label f-blue')); ?>
                                        <!-- <a class="btn-select2" data-toggle='modal' id="btn_modalAddFuncionario" title="<?php echo __('Registrar un nuevo funcionario de la empresa'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a> -->
                                    </div>
                                    <div id="load_collaborators_team">
                                        <?php echo $this->Form->input('Contac.funcionarios', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple','value' => @$this->request->data["Contac"]["funcionarios"], 'options' => $collaborators)); ?>            
                                    </div>
                                </div> 
                                <!-- EXTERNOS -->
                                <div class="form-group">
                                    <div>
                                        <?php echo $this->Form->label('Contac.externos', __('Asistentes externos'), array('class' => 'control-label f-blue')); ?>
                                        <!-- <a class="btn-select2" data-toggle='modal' id="btn_modalAddEmployee" title="<?php echo __('Registrar un nuevo asistente del cliente'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a> -->
                                    </div>
                                    <div id="load_assitants_team"> 
                                        <?php echo $this->Form->input('Contac.externos', array('class' => 'form-control select2_all', 'label' => false, 'type' => 'select', 'multiple' => 'multiple','value' =>    @$this->request->data["Contac"]["externos"], 'options' => $assistants)); ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Copias de correos electrónicos -->
                        <div class="row-flex">
                            <div class="col-30-flex">
                                <label class="f-blue"><?php echo __('Copias de correos electrónicos'); ?></label>
                            </div>
                            <div class="col-70-flex">
                                <?php echo $this->Form->input('copiesI', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'data-role' => 'tagsinput')); ?>
                            </div>
                        </div>
                        <div class="row-flex">
                            <div class="col-30-flex">
                                <!--
                                    <div class="checkbox">
                                        <label class="f-blue font-bold">
                                            <?php echo $this->Form->checkbox('reunion', array('hiddenField' => false, 'div' => false, 'label' => false, 'type' => 'checkbox')); ?>
                                            <?php echo __('Sin finalizar acta') ?>
                                        </label>
                                    </div>
                                -->
                            </div>
                            <div class="col-70-flex">
                                <div class="checkbox">
                                    <div id="content-approval-solicitud_">
                                        <label for="cbox_perrmission_report_tickets_area">
                                            <input type="checkbox" name="data[Contac][approval]" value="1" id="requestApproval">
                                            <b class=" f-blue font-bold"><?php echo __('¿Solicitar aprobación?') ?></b>
                                        </label>
                                    </div>
                                </div>
                                <div id="content-approval">
                                    <div>
                                        <?php echo $this->Form->label('Contac.approvalusers', __('Solicitar aprobación de usuarios'), array('class' => 'control-label f-blue')); ?>
                                    </div>
                                    <div class="form-group">
                                        <div id="load_content_user_approval">
                                            <?php echo $this->Form->input('approvalusers', array('class' => 'form-control select2_all select_approval_user', 'label' => false, 'div' => false, 'multiple' => 'multiple', 'type' => 'select')); ?> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->label('Contac.date_limit', __('Fecha límite para calificar'), array('class' => 'control-label f-blue')); ?>
                                        <div class='input-group date' id='datetimepickerdatelimit'>
                                            <?php echo $this->Form->input('limit_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Fecha límite de calificación'), 'type' => 'text', 'readonly' => true)); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-flex">
                            <div class="col-30-flex"></div>
                            <div class="col-70-flex">
                                <div class="checkbox">
                                    <label class="label-checkbox-recapmeeting">
                                        <?php echo $this->Form->checkbox('protegida', array('hiddenField' => false, 'div' => false, 'label' => false, 'type' => 'checkbox', 'id' => 'ContacProteger')); ?>
                                        <b class=" f-blue font-bold"><?php echo __("Proteger con contraseña") ?></b>
                                    </label>
                                </div>
                                <div class="input-group">
                                    <?php echo $this->Form->input('password', array('class' => 'form-control border-input', 'type' => 'password', 'placeholder' => __('Contraseña'), 'label' => false, 'div' => false, 'maxlength' => 4, 'autocomplete' => 'new-password')); ?>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="btn_generatePassword" type="button"><span class="glyphicon glyphicon-refresh f-blue"></span></button>
                                    </span>
                                </div>
                                <div>
                                    <button type="button" id="btn_ocultarPassword" class="link-show-password"><?php echo __("Ocultar contraseña") ?></button>
                                    <button type="button" id="btn_verPassword"     class="link-show-password"><?php echo __("Mostrar contraseña") ?></button>
                                </div> 
                            </div>
                        </div>
                        <label class="f-blue font-bold"><?php echo __('Descripción') ?></label>
                        <div id="permission_create_template">                        
                            <!-- MÉTODO AJAX QUE DEVOLVERÁ SI EL USUARIO DEPENDIENDO DEL EQUIPO DE TRABAJO SELECCIONADO PUEDE CREAR PLANTILLAS -->
                        </div> 
                        <div class="listTemplate"></div>
                        <div>
                            <?php echo $this->Form->input('description', array('class' => 'form-control border-input', 'label' => false, 'required' => true, 'placeholder' => __('Descripción de la reunión'), 'div' => false)); ?>
                        </div>






                        <div id="show-content-meeting-day-choose"> </div>
                        <div class="table-responsive">
                            <table class="table-compromise f-table-grid-layout-fixed">
                                <thead>
                                    <tr>
                                        <th class="text-center" colspan="3"><label class="f-blue"><?php echo __('Compromisos') ?></label></th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><?php echo __("Fecha límite") ?></th>
                                        <th class="text-center"><?php echo __("Usuario"); ?></th>
                                        <th class="text-center"><?php echo __("Descripción") ?></th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr id="nuevoCompromiso" class="">
                                        <td class="">
                                            <div class="content-date-compromises center-block">
                                                <div class="input-group  date_all_contac date">
                                                    <?php echo $this->Form->input('Commitments.NUEVO.fecha', array('readonly', 'placeholder' => __('Fecha límite'), 'class' => 'form-control COMMITMENT_DATE_REQUIRE date_all', 'label' => false, 'div' => false)) ?>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="table-grid-30-compromises">
                                            <div class="content-users-compromises center-block">
                                                <div id="load_content_users_commitment">
                                                    <?php echo $this->Form->input('Commitments.NUEVO.asistente', array('empty'=>__('Seleccionar...'), 'class' => 'form-control select2_all selectclient', 'label' => false, 'type' => 'select','options' => $allUsers)); ?>
                                                </div>
                                            </div>
                                        </td> 
                                        <td class="table-grid-30-compromises">
                                            <div class="content-description-compromises center-block">
                                                <?php echo $this->Form->textarea('Commitments.NUEVO.description', array('class' => 'form-control resize-none COMMITMENT_DESC_REQUIRE', 'placeholder' => __('Descripción'), 'label' => false, 'rows' => 3)) ?>
                                                <button class="f-btn-delete btn-remove-nuevo-compromiso hidden" data-id="NUEVO" type="button"><?php echo __("x"); ?></button>
                                            </div> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="">
                            <div class="table-responsive">
                                <table id="commitments" class="f-table-grid-layout-fixed">
                                    <tbody class="hijosF">
                                        <?php if(!empty($this->request->data["Commitments"])):?>
                                            <?php unset($this->request->data["Commitments"]["NUEVO"]) ?> 
                                            <?php foreach ($this->request->data["Commitments"] as $keyId => $commitment):?>
                                                <tr id="nuevoCompromiso<?php echo $keyId ?>" class="tr-height100">
                                                    <td  class="table-grid-30">
                                                        <div class="content-date-compromises center-block">
                                                            <div class="input-group date_all_contac date">
                                                                <?php echo $this->Form->input("Commitments.{$keyId}.fecha", array('readonly', 'placeholder' => __('Fecha límite'), 'class' => 'form-control COMMITMENT_DATE_REQUIRE date_all', 'label' => false, 'value' => $commitment["fecha"], 'div' => false)) ?>
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td  class="table-grid-30">
                                                        <div class="content-users-compromises center-block">
                                                            <div id="load_content_users_commitment">
                                                                <?php echo $this->Form->input("Commitments.{$keyId}.asistente", array('class' => 'form-control select2_all selectclient', 'label' => false, 'type' => 'select','options' => $allUsers, 'value' => $commitment["asistente"])); ?>
                                                            </div>
                                                        </div>
                                                    </td> 
                                                    <td  class="table-grid-30">
                                                        <div class="content-description-compromises center-block">
                                                            <?php echo $this->Form->textarea("Commitments.{$keyId}.description", array('class' => 'form-control resize-none COMMITMENT_DESC_REQUIRE', 'placeholder' => __('Descripción'), 'label' => false, 'rows' => 3, 'value' => $commitment["description"])) ?>
                                                            <button class="f-btn-delete btn-remove-nuevo-compromiso" data-id="<?php echo $keyId ?>" type="button"><?php echo __("x"); ?></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>
                                        <?php endif; ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="flex-justify-end ptb-15">
                            <button class="addcompromiso btn btn-blue" type="button"><?php echo __('Guardar y crear nuevo compromiso') ?></button>
                        </div>
                        <!--                 
                            <label class="f-blue font-bold"><?php echo __('Descripción') ?></label>
                            <div id="permission_create_template"></div> 
                            <div>
                                <?php echo $this->Form->input('description', array('class' => 'form-control border-input', 'label' => false, 'required' => true, 'placeholder' => __('Descripción de la reunión'), 'div' => false)); ?>
                            </div>
                            <div id="show-content-meeting-day-choose"> </div>  
                        -->
                    </div> 
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <!--
                        <div class="bg-filter">
                            <label class="f-blue"><?php echo __('Temporizador del acta'); ?></label> 
                            <div class="input-group col-md-12">
                                <?php echo $this->Form->input('duration', array('class' => 'form-control border-input duration-contac', 'label' => false, 'div' => false, 'id' => 'timers', 'readOnly' => true, 'type' => 'hidden')); ?>
                                <div id="timeContainer" class="form-control border-input input-height-40">
                                    <time id="timerValue" class="line-height-20"></time>
                                </div>
                                <div class="input-group-btn"> 
                                    <button type="button" class="btn btn-meeting" id="start"><?php echo $this->Html->image("frontend/admin/play-meeting.svg");?></button>
                                    <button type="button" class="btn btn-meeting" id="stop"><?php echo $this->Html->image("frontend/admin/pause-meeting.svg");?></button>
                                    <button type="button" class="btn btn-meeting" id="reset"><?php echo $this->Html->image("frontend/admin/update-meeting.svg");?></button> 
                                </div>
                            </div>                       
                        </div>
                        <hr>
                        <div class="bg-filter">
                            <label class="f-blue"><?php echo __("Privacidad")?></label>
                            <div class="checkbox">
                                <label class="label-checkbox-recapmeeting">
                                    <?php echo $this->Form->checkbox('protegida', array('hiddenField' => false, 'div' => false, 'label' => false, 'type' => 'checkbox', 'id' => 'ContacProteger')); ?>
                                    <?php echo __("Proteger con contraseña") ?>
                                </label>
                            </div>
                            <div class="input-group">
                                <?php echo $this->Form->input('password', array('class' => 'form-control border-input', 'type' => 'password', 'placeholder' => __('Contraseña'), 'label' => false, 'div' => false, 'maxlength' => 4, 'autocomplete' => 'new-password')); ?>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn_generatePassword" type="button">
                                        <span class="glyphicon glyphicon-refresh f-blue"></span>
                                    </button>
                                </span>
                            </div>
                            <div>
                                <button type="button" id="btn_ocultarPassword" class="link-show-password"><?php echo __("Ocultar contraseña") ?></button>
                                <button type="button" id="btn_verPassword"     class="link-show-password"><?php echo __("Mostrar contraseña") ?></button>
                            </div> 
                        </div>
                    -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="bg-filter">
                        <div class="flex-justify-end">
                            <?php if(isset($permissionAction["index"])) : ?> 
                                <button type='button' class='btn btn-blue btnArchivos mxy-10' id="btnGuardar" style="margin:10px">
                                    <?php echo __('Guardar y enviar') ?>
                                </button>
                                <button type='button' class='btn btn-blue btnArchivosBorrador mxy-10' id="btnSaveBorrador" style="margin:10px">
                                    <?php echo __('Guardar borrador') ?>
                                </button>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php echo $this->Form->end(); ?> 
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"> 
                <div class="bg-filter">
                    <!-- Imágenes -->
                    <div>
                        <div class="flex-space-between">
                            <p><span class="f-blue font-bold"><?php echo __('Imágenes')?></span>&nbsp;<?php echo __('Selecciona las imágenes que deseas adjuntar en el acta.') ?></p>
                            <div>
                            <?php echo $this->Html->image('frontend/admin/icon-png.svg', array('height' => 'auto;','width' => '60px;')); ?>
                            <?php echo $this->Html->image('frontend/admin/icon-jpg.svg', array('height' => 'auto;','width' => '60px;')); ?>
                            </div>
                        </div>
                        <div class="scroll-uploadimg">
                            <form method="post" action="saveImage" class="dropzone" id="dropzoneImage" enctype='multipart/form-data'> </form>
                        </div>
                    </div>
                    <!-- Documentos -->
                    <div>
                        <div class="flex-space-between">
                            <p><span class="f-blue font-bold"><?php echo __('Documentos') ?></span>&nbsp;<?php echo __('Selecciona los documentos que deseas adjuntar en el acta.') ?></p>
                            <div>
                                <?php echo $this->Html->image('frontend/admin/icon-pdf.svg', array('height' => 'auto;','width' => '60px;')); ?>
                                <?php echo $this->Html->image('frontend/admin/icon-word.svg', array('height' => 'auto;','width' => '60px;')); ?>
                            </div>
                        </div>
                        <div class="scroll-uploadimg">
                            <form method="post" action="saveImage" class="dropzone" id="dropzoneDocument" enctype='multipart/form-data'> </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-loading" style="display: block;">
            <div class="container-loading">
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
            </div>
        </div>
        <?php
            echo $this->element('modals_contact');
            $this->Html->script('lib/datepicker/bootstrap-datepicker.js', ['block' => 'AppScript']);
            $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
            $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
            $this->Html->script('bootstrap.3.3.7/datetimepicker/js/bootstrap-datetimepicker.min.js', ['block' => 'AppScript']);
            $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
            $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
            echo $this->Html->script('/assets/vendors/js/bootstrap-select/bootstrap-select.min.js');
            if($this->Session->read('Config.language') == 'esp') {
                $this->Html->script('lib/datepicker/locales/bootstrap-datepicker.es.js', ['block' => 'AppScript']);
                $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
                $this->Html->script('bootstrap.3.3.7/datetimepicker/js/locales/bootstrap-datetimepicker.es.js', ['block' => 'AppScript']);
            }
            $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
            $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
            $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
            $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
        ?>
        
       
    </div>
</div>


