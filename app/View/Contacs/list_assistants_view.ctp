 <div class="input-form">
    <h3 class="form-control-label">
        <?php echo __('Colaboradores'); ?>     
    </h3>
</div>

  <?php if(!empty($users["collaborators"])): ?>
    <ul class="nav flex-column mb-3 mt-3">
      <?php foreach ($users["collaborators"] as $collaborator): ?>                                             
        <?php echo "<li class='nav-item'><i class='la la-user la-2x align-middle'></i>".__($collaborator). "</li><br />\n"; ?>                      
      <?php endforeach; ?>
    <ul>
  <?php else: ?>
        <p><?php echo __("No se agregaron colaboradores al acta."); ?></p>
  <?php endif; ?>

  <div class="input-form">
      <h3 class="form-control-label">
          <?php echo __('Asistentes externos'); ?>
      </h3>
  </div>
  <?php if(!empty($users["assistants"])): ?>
      <ul class="nav flex-column mb-3 mt-3">
        <?php foreach ($users["assistants"] as $assistant): ?>                                             
          <?php echo "<li class='nav-item'><i class='la la-user la-2x align-middle'></i>".__($assistant). "</li><br />\n"; ?>                      
        <?php endforeach; ?>
      </ul>
  <?php else: ?>
        <p><?php echo __("No se agregaron asistentes externos al acta."); ?></p>
  <?php endif; ?> 