<?php $permissionAction = $this->Utilities->check_team_permission_action(array("index"), array("contacs")); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Listado de actas'); ?>
                <?php if(!empty($permissionAction["index"])): ?>  
                    <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Crear acta");?>" href="<?php echo $this->Html->url(array('action'=>'add')); ?>">
                        <i class="flaticon-add"></i>
                    </a> 
                <?php endif;?>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Actas") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row">
                        <div class="col-md-3"> 
                            <div class="form-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Número acta, proyecto'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
              
                        <div class="col-md-3"> 
                            <div class="form-group f-input-select-white_">
                                <?php echo $this->Form->input('project_id', array('class' => 'form-control select-picker-list','label'=>false,'div'=>false,'placeholder' => __('Nombre'), 'type' => 'select', 'options' => $projects, 'data-live-search' => 'true', 'empty' => __("Selecciona un proyecto"), 'selected' => !empty($this->request->query['project_id']) ? $this->request->query['project_id'] : '')); ?>
                            </div>
                        </div>
                
                        <div class="col-md-3"> 
                            <div class="form-group f-input-select-white_ form-select-enterprise_">
                                <?php if(!empty($this->request->query["teams"])):?>
                                    <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams select-picker-list','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'value' => $this->request->query["teams"], 'data-live-search' => 'true', 'empty' => __("Selecciona una empresa"))); ?>
                                <?php else: ?>
                                    <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams select-picker-list','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'data-live-search' => 'true', 'empty' => __("Selecciona una empresa"))); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    
                        <div class="col-md-3"> 
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>

                        <div class="col-md-12"> 
                            <?php if(empty($contacs) && !empty($this->request->query['q'])) : ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="<?php echo $this->request->is('mobile') ? '' : 'widget-body' ?>">
                <ul class="nav nav-tabs nav-fill" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?php echo $this->params->params['controller'] == 'contacs' && $this->params->params['action'] == 'index'? 'active' : '' ?>" 
                            href="javascript:void(0)">
                            <?php echo __("Todas las actas")?>
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link <?php //echo $this->params->params['controller'] == 'contacs' && $this->params->params['action'] == 'approval_contacs'? 'active' : '' ?>" 
                            href="<?php //echo $this->Html->url(array('controller'=>'contacs','action'=>'approval_contacs'));?>">
                            <?php //echo __("Actas por aprobar")?>
                        </a>
                    </li> -->
                </ul>
                <div class="tab-content pt-3">
                    <div class="table-responsive" style="min-height: 200px">
                        <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                            <thead class="text-primary">
                                <tr>
                                    <th class="table-grid-10 text-center"><?php echo __('#Acta'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Cliente'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Proyecto'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Fecha y hora inicio'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Estado'); ?></th>
                                    <th class="table-grid-10 text-center"><?php echo __('Acciones'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($contacs)):?>
                                <?php foreach ($contacs as $contac): ?>
                                <?php $response = $this->Utilities->permissionViewContac($contac);?> 
                                <tr>
                                    <?php if($contac['Contac']['state'] == Configure::read('ENABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')){ ?>
                                        <td class="table-grid-10 td-word-wrap text-center">
                                            <span class="table-number">
                                                <?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?></span> 
                                                
                                            </td>
                                    <?php  } else { ?>
                                        <td class="table-grid-10 td-word-wrap text-center">
                                            <span class="table-number">
                                                <?php echo h($contac['Contac']['number']); ?></span> 
                                            </td>
                                    <?php } ?>
                                    <td class="table-grid-10 td-word-wrap">
                                        <?php echo h($contac['Client']['name']) ?>
                                    </td>
                                    <td class="table-grid-10 td-word-wrap">
                                        <?php echo !empty($contac['Project']['name']) ? h($contac['Project']['name']) : __("Indefinido"); ?>
                                    
                                    </td>
                                    

                                    

                                    <td class="table-grid-10 td-word-wrap"><?php echo h($this->Time->format('d-m-Y h:i A', $contac['Contac']['start_date'])); ?>&nbsp;</td>
                                    <!-- <td class="table-grid-10 td-word-wrap"><?php echo h($this->Time->format('d-m-Y h:i A', $contac['Contac']['end_date'])); ?>&nbsp;</td> -->
                                    <td class="table-grid-10 td-word-wrap"><span class="table-number"><?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?></span></td>
                                    <?php echo $this->Utilities->stateEnabledContac($contac['Contac']['id'],$contac['Contac']['state'],$contac['Contac']['user_edit']); ?>

                                    <td class="table-grid-10 td-word-wrap td-actions text-center">
                                        <?php 
                                            $permission     = $this->Utilities->check_team_permission_action(array("index"), array("contacs"), $contac['Team']['id']); 
                                            $permissionView = $this->Utilities->check_team_permission_action(array("view"), array("meetings"), $contac['Team']['id']); 
                                            $meeting        = $this->Utilities->getMeetingContac($contac['Contac']['id']); 
                                        ?>
                                        <?php if($response == true):?>
                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                                <i class="fa fa-eye"></i>
                                            </a> 
                                        <?php endif; ?>
                                        <?php if(AuthComponent::user("id") == $contac['Contac']['user_id']):?>
                                            <?php if(!empty($contac["CommentContac"])):?>
                                                <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'comment_contacs','action' => 'comments_contac', EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" title="<?php echo __('Ver comentarios del acta'); ?>" class="btn-xs" data-toggle="tooltip">
                                                    <i class="fa fa-comments"></i>
                                                </a> 
                                            <?php endif; ?>
                                        <?php endif; ?> 
                                        <?php if(isset($permission["index"])) : ?>  

                                            <?php $responseContactApproval = $this->Utilities->check_button_calification_contact($contac['Contac']['id']); ?>

                                            <?php if(!empty($responseContactApproval)) { ?>

                                                <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'detail_contac_to_approved', EncryptDecrypt::encrypt($responseContactApproval['ApprovalContac']['id']))); ?>" title="<?php echo __('Ir a calificar acta'); ?>" class="btn-xs" data-toggle="tooltip">
                                                    <i class="la la-check"></i>
                                                </a> 

                                            <?php } ?>

                                            <?php if(isset($permissionView["view"])):?>
                                                <?php if(!empty($meeting)):?>
                                                    <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'meetings','action' => 'view', EncryptDecrypt::encrypt($meeting))); ?>" title="<?php echo __('Ver reunión del acta'); ?>" class="btn-xs" data-toggle="tooltip">
                                                        <i class="fa fa-book"></i>
                                                    </a> 
                                                <?php endif ?>
                                            <?php endif ?>

                                            <?php if($contac['Contac']['state'] == Configure::read('DISABLED')): ?>
                                                <a  rel="tooltip" href="<?php echo Router::url('/', true) ?>contacs/download_contac/<?php echo EncryptDecrypt::encrypt($contac["Contac"]["id"]);?>.pdf" title="<?php echo __('Descargar acta'); ?>" data-toggle="tooltip" class="btn-xs">
                                                    <i class="fa fa-file-text-o"></i>
                                                </a> 
                                    			<?php
                                                $actaName = __("Acta ").$contac['Contac']['number']." - ".$contac['Client']['name']. " - ".$contac['Project']['name'];

                                                 ?>
                                                <a data-nombre="<?php echo $actaName  ?>"  rel="tooltip" href="" data-id="<?php echo $contac['Contac']['id'] ?>" data-target="#modalResendActa" title="<?php echo __('Reenviar Acta'); ?>" data-toggle="modal" class="btn-xs getContactId">
                                                    <i class="fa fa-envelope"></i>
                                                </a> 
                                    
                                    
                                    
                                            <?php endif; ?> 

                                            <?php //if ($contac['Contac']['state'] == Configure::read('ENABLED') && AuthComponent::user('role_id') != Configure::read('ADMIN_ROLE_ID') || $contac['Contac']['user_edit'] == AuthComponent::user('id') && $contac['Contac']['state'] != 3): ?>
                                            <?php if ($contac['Contac']['user_id'] == AuthComponent::user('id') && $contac['Contac']['state'] != Configure::read('DISABLED')): ?>
                                                <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'edit',  EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" title="<?php echo __('Editar borrador'); ?>" class="btn-xs" data-toggle="tooltip">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            <?php endif ?>

                                            <?php echo $this->Utilities->valideStateFinalizedoApproved($contac['Contac']['state'],$contac['Contac']['id']); ?>
                                            
                                            <?php if ($contac['Contac']['state'] == 3): ?>
                                        <!--         <a rel="tooltip" href="javascript:void(0)" data-id="<?php echo EncryptDecrypt::encrypt($contac['Contac']['id'])?>" class="btn-xs btn_approved2" title="<?php echo __('Ver calificaciones'); ?>" data-toggle="tooltip">
                                                    <i class="fa fa-check-square"></i>
                                                </a> -->
                                            <?php endif ?>
      
                                            <?php if ($contac['Contac']['user_edit'] == Configure::read('DISABLED') && $contac['Contac']['state'] == Configure::read('ENABLED') && $contac['Contac']['user_id'] == AuthComponent::user("id")): ?>
                                                <a rel="tooltip" href="#" data-uid="<?php echo EncryptDecrypt::encrypt($contac['Contac']['id'])?>" data-firebase="<?php echo EncryptDecrypt::encrypt($contac['Contac']['firebase'])?>" title="<?php echo __('Descartar borrador'); ?>" class="btn-xs btn_descartarBorrador" data-toggle="tooltip">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </a>
                                            <?php endif ?>

                                            <?php if($contac['Contac']['state'] == Configure::read('ENABLED') && $contac['Contac']['in_approval'] == NULL):?>
                                                <a href="#" rel="tooltip" title="<?php echo __('Finalizar acta'); ?>" class="btn-xs btn-get-contac-to-finish" data-id="<?php echo EncryptDecrypt::encrypt($contac['Contac']['id'])?>" data-toggle="tooltip">
                                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </a>
                                            <?php endif; ?>

                                            <?php if($contac['Contac']['in_approval'] == Configure::read('NEW_APPROVAL') && $contac['Contac']['state'] == Configure::read('ENABLED')):?>
                                                <a rel="tooltip" title="<?php echo __('Solicitar nuevamente calificación'); ?>" class="btn-xs btn-get-contac-to-finish-again f-cursor-pointer" data-id="<?php echo EncryptDecrypt::encrypt($contac['Contac']['id'])?>" data-toggle="tooltip">
                                                    <i class="fa fa-repeat" aria-hidden="true"></i>
                                                </a>
                                            <?php endif; ?>

                                            <!-- BOTÓN PARA SOLICITAR LA CONTRASEÑA DEL ACTA -->
                                            <?php if($contac['Contac']['password'] != "" || $contac['Contac']['password'] != NULL): ?>
                                                <a rel="tooltip" title="<?php echo __('Solicitar la contraseña del acta'); ?>" class="btn-xs btn_team_action" data-toggle="tooltip" href="<?php echo $this->Html->url(array('action' => 'request_password_contac', EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" >
                                                    <i class="fa fa-key" aria-hidden="true"></i>
                                                </a> 
                                            <?php endif; ?>

                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <?php else: ?>
                                <tr>
                                    <td class="text-center" colspan="6"><?php echo __('No existen actas.')?></td>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="backg-white"> 
    <!--SOLICITAR APROBACIÓN DEL ACTA-->
    <div class="modal fade" id="finishContac" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel1"><?php echo __("¿Desea finalizar el acta?")?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="text-danger errorP" style="display:none"><?php echo __('Espera...'); ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-finish-contac"><?php echo __("Finalizar")?></button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="passwordContacModalFinish" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel2"><?php echo __("Finalizar Acta - Ingresar contraseña del acta")?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="text-danger errorP" style="display:none"><?php echo __('Espera...'); ?></p>
            <div class="row">
                <div class='col-md-12'>
                    <div class='form-group'>
                        <?php echo $this->Form->input('password',array('class' => 'form-control border-input txt-end-minute-pass-index','type'=>'password','placeholder' => __('Contraseña'),'label'=>false,'div'=>false));?>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-finish-contac btn-end-minute-pass-index"><?php echo __("Finalizar")?></button>
          </div>
        </div>
      </div>
    </div>
    <!-- FIN SOLICITAR APROBACIÓN DEL ACTA-->

    <!--SOLICITAR NUEVAMENTE APROBACIÓN DEL ACTA NUEVAMENTE-->
    <div class="modal fade" id="NewApprovalContac" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel3"><?php echo __("¿Desea solicitar nuevamente una calificación del acta?")?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-new-approval-contac"><?php echo __("Enviar solicitud")?></button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="NewApprovalContacPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel4"><?php echo __("Solicitar nuevamente la calificación del acta - Ingresar contraseña del acta")?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p>
            <div class="row">
                <div class='col-md-12'>
                    <div class='form-group'>
                        <?php echo $this->Form->input('password',array('class' => 'form-control border-input','type'=>'password','placeholder' => __('Contraseña'),'label'=>false,'div'=>false,'id' => 'passNewApproval'));?>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-new-approval-contac"><?php echo __("Enviar solicitud")?></button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="viewApprovedContacModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel5"><?php echo __("Historial de las calificaciones del acta")?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class='resultHystoryApproved'></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo __('Cerrar') ?></button>
          </div>
        </div>
      </div>
    </div>
</div>
<?php
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);

    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actions.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 

    $this->Html->scriptBlock("$.fn.selectpicker.Constructor.BootstrapVersion = '4';", array('block' => 'AppScript')); 
    $this->Html->scriptBlock("$('.select-picker-list').selectpicker();", array('block' => 'AppScript')); 
    if ($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    } 
    echo $this->element('modal_resend_acta');
?>

