<div class="col-sm-12">
	<?php echo $this->Form->hidden('contac_id', array('value' => $contacId,'id' => 'contac-id-refuse'));?> 
</div>

<div class="col-sm-12">
	<?php echo $this->Form->hidden('user_id', array('value' => $userId,'id' => 'user-id-refuse'));?> 
</div>

<div id="refuseContacWithPassword" class="modal fade">
	<div class="modal-dialog modal-recapmeeting">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
	            <h4 class="modal-title"><?php echo __("Calificar Acta - Ingresar contraseña del acta")?></h4>
        		<p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p>	            
	        </div>
	        <div id="modalBody" class="modal-body"> 	
	        	<div class='col-md-12'>
			        <div class='form-group'>
						<?php echo $this->Form->input('password',array('class' => 'form-control border-input','type'=>'password','placeholder' => __('Contraseña'),'label'=>false,'div'=>false));?>
					</div>
				</div>
				<div class='col-md-12'>
			        <div class='form-group'>
						<?php echo $this->Form->textarea('comment',array('class' => 'form-control border-input resize-none','placeholder' => __('Motivo de rechazo'),'label'=>false,'div'=>false));?>
					</div>
				</div>
	        </div>
	        <div class="modal-footer"> 
	            <button type="button" class="btn btn-modal-recapmeeting btn-refuse-contac-password" data-id="<?php echo $contacId ?>" data-user="<?php echo $userId?>"><?php echo __("Rechazar")?></button>
	        </div>
	    </div>
	</div>
</div>

<div id="refuseContacWithoutPassword" class="modal fade">
	<div class="modal-dialog modal-recapmeeting">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
	            <h4 class="modal-title"><?php echo __("Calificar acta")?></h4>
        		<p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p> 
	        </div>
	        <div id="modalBody" class="modal-body">
	        	<div class='col-md-12'>
			        <div class='form-group'>
						<?php echo $this->Form->textarea('comment',array('class' => 'form-control border-input resize-none','placeholder' => __('Motivo de rechazo'),'label'=>false,'div'=>false,'id' => 'commentContac'));?>
					</div>
				</div>
	        </div>
	        <div class="modal-footer"> 
	            <button type="button" class="btn btn-modal-recapmeeting btn-refuse-contac" data-id="<?php echo $contacId ?>" data-user="<?php echo $userId?>"><?php echo __("Rechazar")?></button>
	        </div>
	    </div>
	</div>
</div>


<?php
	$this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']); 
	$this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']);
	$this->Html->script('lib/sweetalert.min.js',		   ['block' => 'AppScript']);
?>