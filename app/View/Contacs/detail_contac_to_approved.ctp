<!-- Begin Page Header-->
<div class="row">
	<div class="page-header">
		<div class="d-flex align-items-center">
			<h2 class="page-header-title"><?php echo __('Acta para calificar'); ?></h2>
			<div>
				<ul class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo Router::url("/",true) ?>">
							<i class="ti ti-home"></i>
						</a>
					</li>
					<li class="breadcrumb-item">
						<a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Actas") ?></a>
					</li>
					<li class="breadcrumb-item">
						<a href="<?php echo $this->Html->url(array('action'=>'approval_contacs'));?>"><?php echo __("Actas por aprobar") ?></a>
					</li>  
					<li class="breadcrumb-item active"><?php echo __("Calificar acta") ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- End Page Header -->
<div class="row flex-row">
	<div class="col-12">
		<div class="widget has-shadow">
			<div class="widget-header bordered no-actions d-flex align-items-center">
				<h2>
					<?php echo __("Calificar acta") ?>
				</h2>
				<div class="widget-options">
					<div class="" role="">
						<button data-id="<?php echo EncryptDecrypt::encrypt($contac['Contac']['id'])?>" data-approval="<?php echo EncryptDecrypt::encrypt($id)?>" type="button" id="btn-approval-contac" class="btn btn-acta-aprobada btn-approval-contac"><?php echo __("Aprobar")?><i class="flaticon-checked"></i></button>
						<button data-id="<?php echo EncryptDecrypt::encrypt($contac['Contac']['id'])?>" data-approval="<?php echo EncryptDecrypt::encrypt($id)?>" type="button" id="btn-denied-contac" class="btn btn-acta-rechazada btn-denied-contac"><?php echo __("Rechazar")?><i class="flaticon-cancel"></i></button>		
					</div>
				</div>
			</div>
			<div class="widget-body px-5">
				<div class="form-horizontal" id="content-table">
					<div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Estado'); ?>
                        </label>
                        <div class="col-lg-8">
                            <?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Fecha y hora de la última modificación'); ?>
                        </label>
                        <div class="col-lg-8">
                            <?php echo h($this->Time->format('Y-m-d H:i:s', $contac['Contac']['modified'])); ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
					<div class="form-group row d-flex align-items-center">
						<label class="col-lg-4 form-control-label"><?php echo __('Empresa'); ?></label>
						<div class="col-lg-8">
							<label id="lbl_team"><?php echo $contac['Team']['name']; ?> </label>
						</div>
                        <div class="em-separator separator-dashed w-100"></div>
					</div>
					<?php if(!empty($contac['Team']['img'])):?>
						<div class="form-group row d-flex align-items-center">
							<label class="col-lg-4 form-control-label">
								<?php echo __('Logo'); ?>
							</label>
							<div class="col-lg-8">
								<a href="<?php echo $this->Html->url('/document/WhiteBrand/'.$contac['Team']['img']); ?>" class="showImages">                    
									<img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$contac['Team']['img']); ?>" width="100px" class="img-responsive logo-add hidden-xs" alt=""/>
								</a>
							</div>
                            <div class="em-separator separator-dashed w-100"></div>
						</div>
					<?php endif; ?>
					<div class="form-group row d-flex align-items-center">
						<label class="col-lg-4 form-control-label">
							<?php echo __('Cliente'); ?>
							<div id="show_img_client"></div>
						</label>
						<div class="col-lg-8">
							<label id="lbl_cliente"><?php echo $contac['Client']['name']; ?></label>
						</div>
                        <div class="em-separator separator-dashed w-100"></div>
					</div>
					<div class="form-group row d-flex align-items-center">
						<label class="col-lg-4 form-control-label">
							<?php echo __('Proyecto'); ?>
							<div id="show_img_project"></div> 
						</label>
						<div class="col-lg-8">
							<label id="lbl_proyecto"><?php echo $contac['Project']['name']; ?></label>
						</div>
                        <div class="em-separator separator-dashed w-100"></div>
					</div>
					<div class="form-group row d-flex align-items-center">
						<label class="col-lg-4 form-control-label">
							<?php echo __('Número del acta'); ?>
						</label>
						<div class="col-lg-8">
							<?php if ($contac['Contac']['state'] == Configure::read('ENABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')): ?>
								<?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?>
							<?php else: ?>
								<?php echo h($contac['Contac']['number']); ?>
							<?php endif; ?>
						</div>
                        <div class="em-separator separator-dashed w-100"></div>
					</div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Duración de la reunión'); ?></h4>
                            </div>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center ">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Fecha y hora del inicio de la reunión'); ?> 
                        </label>
                        <div class="col-lg-8">
                            <?php echo h($this->Time->format('d-m-Y h:i A', $contac['Contac']['start_date'])); ?>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Fecha y hora fin de la reunión'); ?> 
                        </label>
                        <div class="col-lg-8">
                            <?php echo h($this->Time->format('d-m-Y h:i A', $contac['Contac']['end_date'])); ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Asistentes a la reunión'); ?> 
                        </label>
                        <div class="col-lg-8">
                            <?php if($contac['Contac']['state'] == Configure::read('DISABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL') ): ?> 
                                <div class="input-form">
                                    <h3 class="form-control-label">
                                        <?php echo __('Colaboradores'); ?>     
                                    </h3>
                                </div>
                                
                                <?php if(!empty($users)): ?>
                                    <ul class="nav flex-column mb-3 mt-3">
                                        <?php foreach ($users as $collaborator): ?>                                             
                                            <?php echo "<li class='nav-item'><i class='la la-user la-2x align-middle'></i>". $collaborator . "</li>"; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <p>
                                        <?php echo __("No se agregaron colaboradores al acta."); ?>
                                    </p>
                                <?php endif; ?>

                                <div class="input-form">
                                    <h3 class="form-control-label">
                                        <?php echo __('Asistentes externos'); ?>
                                    </h3>
                                </div>

                                <?php if(!empty($externos)): ?>
                                    <ul class="nav flex-column mb-3 mt-3">
                                        <?php foreach ($externos as $assistant): ?>
                                            <?php echo "<li class='nav-item'><i class='la la-user la-2x align-middle'></i>". $assistant . "</li>\n"; ?>                      
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                      <p><?php echo __("No se agregaron asistentes externos al acta."); ?></p>
                                    <?php endif; ?> 
                            <?php else :?>
                                <div id="list_assistants"></div>
                            <?php endif; ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Copias al correo electrónico') ?>
                        </label>
                        <div class="col-lg-8">
                            <?php if ($contac['Contac']['state'] == Configure::read('DISABLED')) { ?>
                                <?php $copies = explode(',', $contac['Contac']['copies']); ?>
                                <div class="col-70-flex">
                                    <?php if(!empty($copies["0"])): ?>
                                        <div class="form-group">
                                            <p><?php echo implode("<br />", $copies); ?></p>
                                        </div>
                                    <?php else: ?>
                                        <div class="form-group">
                                            <p><?php echo __("No hay copias de correo electrónico agregadas al acta.") ?></p>
                                        </div> 
                                    <?php endif;?>
                               </div>
                            <?php } else { ?>
                                <div class="col-70-flex">
                                    <div class="form-group">
                                        <p id="lbl_copias">
                                            <?php echo __("No hay copias de correo electrónico agregadas al acta.") ?>
                                        </p>
                                    </div>
                              </div>
                            <?php } ?> 
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Creador del acta'); ?>
                        </label>
                        <div class="col-lg-8">
                            <?php if($contac['Contac']['user_id'] == Authcomponent::user("id")):?>
                                <?php echo $this->Html->link($contac['User']['firstname'].' '.$contac['User']['lastname'], array('controller' => 'users', 'action' => 'view', EncryptDecrypt::encrypt($contac['User']['id'])), array('class' => '')); ?>
                            <?php else: ?>
                                 <?php echo $contac['User']['firstname'].' '.$contac['User']['lastname']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12 form-control-label form-control-label">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Descripción') ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12 div-word-wrap p-5">  
                            <?php echo $contac['Contac']['description'] ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12 form-control-label form-control-label">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Compromisos') ?></h4>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group row d-flex align-items-center px-5">
                        <div class="col-lg-12">
    						<div class="table-responsive">
    							<table class="table">
    								<thead>
    									<th class="col-25-commiments" style="width: 20%;">
    										<label class="f-blue"><?php echo __("Fecha límite") ?></label>
    									</th> 						
    									<th class="col-30-commiments">
    										<label class="f-blue"><?php echo __("Usuario"); ?></label>
    									</th>
    									<th class="col-30-commiments">
    										<label class="f-blue"><?php echo __("Descripción") ?></label>
    									</th>
    								</thead>
    								<tbody>
    									<?php if(!empty($commitments)):?>
    										<?php foreach ($commitments as $commitment): ?> 
    											<tr>
    												<td class="col-25-commiments div-word-wrap" style="width: 20%;"><?php echo $commitment["Commitment"]["delivery_date"]; ?></td>
    												<td class="col-30-commiments div-word-wrap"><?php echo !empty($commitment["User"]["firstname"]) ? $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"] . ' - ' . $commitment["User"]["email"] : __("Nombre no asignado por parte del usuario") . ' - ' . $commitment["User"]["email"]?></td>
    												<td class="col-30-commiments div-word-wrap"><?php echo $commitment["Commitment"]["description"]; ?></td>
    											</tr>
    										<?php endforeach; ?>
    									<?php else: ?>
    										<tr>
    											<td colspan="3" align="center">
    												<p>
    													<?php echo __("No hay compromisos agregados al acta.")?>
    												</p>
    											</td>
    										</tr>
    									<?php endif;?> 		
    								</tbody>
    							</table>
                            </div>
						</div>
                        <div class="em-separator separator-dashed w-100"></div>
					</div>
				</div>
			</div>
			<div class="widget-body">
                <div class="form-horizontal px-5">
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Imágenes agregadas al acta.') ?> &nbsp;</h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <?php echo $this->Html->image('frontend/admin/icon-png.svg', array('height' => 'auto;','width' => '60px;')); ?>
                        <?php echo $this->Html->image('frontend/admin/icon-jpg.svg', array('height' => 'auto;','width' => '60px;')); ?>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div id="imageDropzone" class="imageDropzone-div row scroll-uploadimg w-100">
                        	<?php if(!empty($contac["ContacsFile"])):?>
								<?php foreach ($contac["ContacsFile"] as $file): ?>	
									<div class="col-md-3">
										<a href="<?php echo $this->Html->url('/files/Contac/'.$file['img']); ?>" class="showImages">
											<div class="img-size-upload">  
												<div class="thumbnail">
													<?php echo $this->Html->image("/files/Contac/{$file['img']}",array('class' => 'img img-responsive img-thumbnail'))?> 					 	 
												</div>
											</div>
										</a>
									</div>
								<?php endforeach; ?>										 
							<?php else: ?>
                                <div class="col-lg-12">
    								<p><?php echo __("No hay imágenes en el acta.")?></p>
								</div>
							<?php endif; ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Documentos agregados al acta.') ?> &nbsp;</h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <?php echo $this->Html->image('frontend/admin/icon-pdf.svg', array('height' => 'auto;','width' => '60px;')); ?>
                        <?php echo $this->Html->image('frontend/admin/icon-word.svg', array('height' => 'auto;','width' => '60px;')); ?>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div id="documentDropzone" class="imageDropzone-div row scroll-uploadimg w-100">
                        	<?php if(!empty($contac["ContacsDocument"])):?>
								<?php $numberDocument = 1;?>
								<?php foreach ($contac["ContacsDocument"] as $document): ?>
									<div class="col-md-3">
										<?php 
											$filename  = $document['document'];
											$extension = pathinfo($filename, PATHINFO_EXTENSION);
										?> 
									<?php if($extension == "docx" || $extension == "doc"): ?>							 
										<a class="document-upload"  target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
											<?php echo $this->Html->image('frontend/admin/icon-word.svg',array('width' => '100px','class'=>'img-responsive')); ?>
										</a> 
									<?php elseif ($extension == "pdf" || $extension == "PDF"): ?>
										<a class="document-upload"   target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
											<?php echo $this->Html->image('frontend/admin/icon-pdf.svg',array('width' => '100px','class'=>'img-responsive')); ?>
										</a>
									<?php else :?>
										<a class="document-upload"   target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
											<?php echo $this->Html->image('/document/Contac/default.png',array('width' => '100px','class'=>'img-responsive')); ?>
										</a>
									<?php endif; ?>
										<?php $numberDocument++;?>
									</div>
								<?php endforeach; ?> 								 
							<?php else: ?>
                                <div class="col-lg-12">
                                    <p><?php echo __("No hay documentos en el acta.")?></p>
                                </div>
							<?php endif; ?>  
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>

<div id="passwordContacModal" class="modal fade">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php echo __("Calificar Acta - Ingresar contraseña del acta.")?></h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
				<p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p>	            
			</div>
			<div id="modalBody" class="modal-body"> 	
				<div class='col-md-12'>
					<div class='form-group'>
						<?php echo $this->Form->input('password',array('class' => 'form-control border-input','type'=>'password','placeholder' => __('Contraseña'),'label'=>false,'div'=>false));?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-approved"><?php echo __("Aprobar")?></button> 
			</div>
		</div>
	</div>
</div>

<div id="passwordContacModalWithOutPassword" class="modal fade">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php echo __("Calificar acta")?></h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
				<p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p> 
			</div>
			<div id="modalBody" class="modal-body">
				<b><p class="text-center"><?php echo __("¿Deseas aprobar el acta?")?> </p></b>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-approved"><?php echo __("Aprobar")?></button> 
			</div>
		</div>
	</div>
</div>

<!--MODALES PARA RECHAZAR EL ACTA-->
<div id="refuseContacWithPassword" class="modal fade">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php echo __("Calificar Acta - Ingresar contraseña del acta.")?></h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
				<p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p>	            
			</div>
			<div id="modalBody" class="modal-body"> 	
				<div class='col-md-12'>
					<div class='form-group'>
						<?php echo $this->Form->input('password',array('class' => 'form-control border-input','type'=>'password','placeholder' => __('Contraseña'),'label'=>false,'div'=>false,'id' => 'passwordRefuse'));?>
					</div>
				</div>
				<div class='col-md-12'>
					<div class='form-group'>
						<?php echo $this->Form->input('comment',array('class' => 'form-control border-input resize-none','placeholder' => __('Motivo de rechazo'),'label'=>false,'div'=>false,'maxLength' => 100, 'type' => 'textarea'));?>
					</div>
					<div id="showCharacterPassword">0/100</div> 
				</div>
			</div>
			<div class="modal-footer"> 
				<button type="button" class="btn btn-danger btn-refuse-contac"><?php echo __("Rechazar")?></button>
			</div>
		</div>
	</div>
</div>

<div id="refuseContacWithoutPassword" class="modal fade">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php echo __("Calificar acta")?></h4>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span> 
					<span class="sr-only"><?php echo __("Cerrar")?></span>
				</button>
				<p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p> 
			</div>
			<div id="modalBody" class="modal-body">
				<div class='col-md-12'>
					<div class='form-group'>
						<?php echo $this->Form->input('comment',array('class' => 'form-control border-input resize-none','placeholder' => __('Motivo de rechazo'),'label'=>false,'div'=>false,'id' => 'commentContac','maxLength' => 100, 'type' => 'textarea'));?>
					</div>
					<div id="showCharacterWithoutPassword">0/100</div> 
				</div>
			</div>
			<div class="modal-footer"> 
				<button type="button" class="btn btn-danger btn-refuse-contac"><?php echo __("Rechazar")?></button>
			</div>
		</div>
	</div>
</div>
<!-- FIN MODALES PARA RECHAZAR EL ACTA-->


<?php 
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js?v=3', ['block' => 'AppScript']);
    $this->Html->script('lib/sweetalert.min.js', ['block' => 'AppScript']);
?>





