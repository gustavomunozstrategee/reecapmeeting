<div class="form-group row d-flex align-items-center">
	<div class="col-lg-12">
		<div class="table-responsive">
			<table class="table">
				<thead>
					<th class="col-25-commiments" style="width: 20%;">
						<label class="f-blue"><?php echo __("Fecha límite") ?></label>
					</th> 						
					<th class="col-30-commiments">
						<label class="f-blue"><?php echo __("Usuario"); ?></label>
					</th>
					<th class="col-30-commiments">
						<label class="f-blue"><?php echo __("Descripción") ?></label>
					</th>
				</thead>
				<tbody>
					<?php if(!empty($commitments)):?>
						<?php foreach ($commitments as $commitment): ?> 
							<tr>
								<td class="col-25-commiments div-word-wrap" style="width: 20%;"><?php echo $commitment["Commitment"]["delivery_date"]; ?></td>
								<td class="col-30-commiments div-word-wrap"><?php echo !empty($commitment["User"]["firstname"]) ? $commitment["User"]["firstname"] . ' ' . $commitment["User"]["lastname"] . ' - ' . $commitment["User"]["email"] : __("Nombre no asignado por parte del usuario") . ' - ' . $commitment["User"]["email"]?></td>
								<td class="col-30-commiments div-word-wrap"><?php echo $commitment["Commitment"]["description"]; ?></td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="3" align="center">
								<p>
									<?php echo __("No hay compromisos agregados al acta.")?>
								</p>
							</td>
						</tr>
					<?php endif;?> 		
				</tbody>
			</table>
		</div>
		<div class="em-separator separator-dashed w-100"></div>
	</div>
</div> 

													  
