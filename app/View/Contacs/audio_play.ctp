<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?></title>
        <meta name="title" content="<?php echo Configure::read("Application.name")?> <?php echo __("Invita, gestiona y crea actas de tus reuniones al instante.");?>">
        <meta name="description" content="<?php echo Configure::read("Application.name")?> <?php echo __("potente sistema de seguimiento de los compromisos de tus reuniones y actas.")?>">
        <meta name="keywords" content="<?php echo __("Reuniones, compromisos, actas, seguimiento, recordatorios, empresas, notificaciones, correo electrónico")?>">
        <meta name="author" content="Strategee">   
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/') ?>favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/') ?>favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/') ?>favicon.png">
        <!-- Stylesheet -->
        <?php
            echo $this->Html->css('/assets/vendors/css/base/bootstrap.min.css');
            echo $this->Html->css('/assets/vendors/css/base/elisyam-1.5.min.css');
            echo $this->Html->css('/assets/css/animate/animate.min.css');
            echo $this->Html->css('/assets/css/owl-carousel/owl.carousel.min.css');
            echo $this->Html->css('/assets/css/owl-carousel/owl.theme.min.css');
            echo $this->Html->css('/assets/css/leaflet/leaflet.min.css');
            echo $this->Html->script('/assets/vendors/js/base/jquery.min.js');
            echo $this->Html->css('/assets/css/bootstrap-select/bootstrap-select.min.css');
            echo $this->Html->css('lib/font-awesome.min.css');
            echo $this->Html->css('lib/flaticon.css');
            echo $this->Html->css('lib/parsley.css');
            echo $this->Html->css('lib/sweetalert.css');
            echo $this->Html->css('bootstrap.3.3.7/datetimepicker/css/bootstrap-datetimepicker.min.css');
            echo $this->Html->css('lib/dropzone.min.css');
            echo $this->Html->css('lib/bootstrap-tagsinput.css');
            echo $this->Html->css('lib/select2.min.css');
            echo $this->Html->css('lib/datepicker.css');
            echo $this->Html->css("Magnific-popup/magnific-popup.css"); 
            echo $this->fetch('AppCss');
            echo $this->fetch('styles_section');
            echo $this->Html->css("modify.css"); 
            

        ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>

<div class="centrado-porcentual">
   						 <?php echo $this->Html->image("frontend/admin/logo-blue.svg",array('class' => 'logo', 'alt' => 'Logo Recapmeeting', 'title' => 'Recapmeeting'));?>
                        <br>
                         <br>
                        <?php if($contac['Contac']['state'] == 0): ?>
                            <?php if($audioURL != ""): ?>
                                <?php $number = 0; ?>
                                <?php foreach($audioURL as $key => $value): ?>
                                    <?php $number++;  ?>
                                    <div>
                                    <label> <?php echo __("Audio #").$number ?> </label><br>
                                        <audio controls controlsList="nodownload">
                                          <source src="<?php echo $this->webroot.$value ?>" type="audio/ogg">
                                          <source src="<?php echo $this->webroot.$value ?>" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                    </div>
                                <?php endforeach; ?>
                            
                            <?php else: ?>
                                <label class="col-lg-8 form-control-label">
                                    <?php echo  __("Audio no Disponible") ?>
                                </label>
                            <?php endif;?>
                        <?php else: ?>
                            <label class="col-lg-8 form-control-label">
                                  
                                <?php echo __("Aun no se ha terminado el Acta") ?>
                            </label>
                        <?php endif; ?>
</div>
</html>
<style type="text/css">
	.centrado-porcentual {
    position: absolute;
    left: 50%;
    top: 30%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    text-align: center;
     width: 80%;
}
body {
    
    background-color: white !important;
}
.logo{
max-width: 400px !important;
}
</style>


 