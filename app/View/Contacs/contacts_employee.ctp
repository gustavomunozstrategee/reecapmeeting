<div id="update-content-employee-contacs">
    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting">
        <thead>
            <tr>
                <th><?php echo __('Fecha inicio') ?></th>
                <th><?php echo __('Fecha fin') ?></th>
                <th class="text-center"><?php echo __('Descargar acta') ?></th>
                <th><?php echo __('Nombre cliente') ?></th>
                <th><?php echo __('Imagen del cliente') ?></th>
                <th><?php echo __('Proyecto') ?></th>
                <th><?php echo __('Acción') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($contacs)):?>
            <?php foreach ($contacs as $contac): ?>
            <tr>
                <td><?php echo h($contac['Contac']['start_date']); ?></td>
                <td><?php echo h($contac['Contac']['end_date']); ?></td>
                <td class="text-center"><a href="<?php echo Router::url('/', true) ?>contacs/download_contac/<?php echo EncryptDecrypt::encrypt($contac["Contac"]["id"]);?>.pdf"><i class="flaticon-file table-icon f-blue"></i></a></td>
                <td><?php echo h($contac['Client']['name']); ?></td>
                <td><img src="<?php echo $this->Html->url('/files/Client/'.$contac['Client']['img']); ?>" width="50px" class="img-responsive" alt="" /></td>
                <td><?php echo h($contac['Project']['name']); ?></td>
                <td>
                    <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view','controller'=>'contacs', EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" title="<?php echo __('Ver acta'); ?>" class="btn btn-info  btn-xs">
                        <i class="fa fa-eye"></i>
                </td>
            </tr>
            <?php endforeach ?>
            <?php else: ?>
            <tr>
                <td class="text-center" colspan="14"><?php echo __("No hay actas relacionadas");?></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    <div class="table-pagination">
        <div>
            <?php
            $this->Paginator->options(array('update' => '#update-content-employee-contacs','url'=>array('controller'=>'contacs','action'=>'contacts_employee', EncryptDecrypt::encrypt($employeeId))));
            ?>

            <?php
            echo $this->Paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
            ?>
        </div>
        <div>
            <ul class="pagination f-paginationrecapmeeting">
                <?php
                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>

    <?php echo $this->Js->writeBuffer(); ?>
</div>
