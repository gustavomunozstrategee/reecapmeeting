<section class="section-table">
    <h1 class="f-title"><?php echo __('Actas creadas'); ?></h1>
    <div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting">
        <thead>
            <tr>
                <th><?php echo __('Fecha inicio') ?></th>
                <th><?php echo __('Fecha fin') ?></th>
                <th><?php echo __('Empresa') ?></th>
                <th><?php echo __('Proyecto') ?></th>
                <th><?php echo __('Nombre cliente') ?></th>
                <th><?php echo __('Imagen del cliente') ?></th>
                <th><?php echo __('Número del acta') ?></th>
                <th><?php echo __('Estado') ?></th>
                <th><?php echo __('Acción') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($contacs)):?>
            <?php foreach ($contacs as $contac): ?>
                <?php 
                    $permission     = $this->Utilities->check_team_permission_action(array("index"), array("contacs"), $contac['Team']['id']); 
                    $permissionView = $this->Utilities->check_team_permission_action(array("view"), array("projects"), $contac['Contac']['project_id']);
                ?>

            <tr>
                <td><?php echo h($contac['Contac']['start_date']); ?></td>
                <td><?php echo h($contac['Contac']['end_date']); ?></td>
                <td><?php echo h($contac['Team']['name']); ?></td> 
                <td><?php echo h($contac['Project']['name']); ?></td>
                <td><?php echo h($contac['Client']['name']); ?></td>
                <td>
                    <a href="<?php echo $this->Html->url('/files/Client/'.$contac['Client']['img']); ?>" class="showImagesPopup"> 
                        <img src="<?php echo $this->Html->url('/files/Client/'.$contac['Client']['img']); ?>" width="50px" class="img-responsive" alt="" />
                    </a>
                </td>
                <?php if($contac['Contac']['state'] == Configure::read('ENABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')): ?>
                    <td class="table-grid-10 td-word-wrap text-center"><span class="table-number"><?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?></span></td>
                <?php else: ?>
                    <td class="table-grid-10 td-word-wrap text-center"><span class="table-number"><?php echo h($contac['Contac']['number']); ?></span></td>
                <?php endif; ?>
                <td class="table-grid-10 td-word-wrap"><span class="table-number"><?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?></span></td>
                <td>
                    <?php $response = $this->Utilities->permissionViewContac($contac);?>
                    <?php if($response == true):?>
                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view','controller'=>'contacs', EncryptDecrypt::encrypt($contac['Contac']['id']))); ?>" title="<?php echo __('Ver acta'); ?>" class="btn-xs" data-toggle="tooltip">
                            <i class="fa fa-eye f-blue"></i>
                        </a>
                    <?php endif; ?>
                    <?php if(isset($permission["index"])) : ?>  
                        <?php if($contac['Contac']['state'] == Configure::read('DISABLED')): ?>
                            <a  rel="tooltip" href="<?php echo Router::url('/', true) ?>contacs/download_contac/<?php echo EncryptDecrypt::encrypt($contac["Contac"]["id"]);?>.pdf" title="<?php echo __('Descargar acta'); ?>" data-toggle="tooltip" class="btn-xs"><i class="flaticon-file"></i></a> 
                        <?php endif; ?> 
                    <?php endif; ?>
                    <?php if (isset($permissionView["view"])): ?>
                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view','controller'=>'projects', EncryptDecrypt::encrypt($contac['Project']['id']))); ?>" title="<?php echo __('Ver proyecto'); ?>" class="btn btn-xs" data-toggle="tooltip">
                            <i class="fa fa-eye"></i>
                        </a>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach ?>
            <?php else: ?>
            <tr>
                <td class="text-center" colspan="14"><?php echo __("No hay informes creados.");?></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
    <div class="table-pagination">
        <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>

        <div>
            <ul class="pagination f-paginationrecapmeeting">
                <?php
                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
</section>
