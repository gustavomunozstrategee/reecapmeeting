<?php if (!empty($imagenes)) { ?>
	<?php $i = 1; foreach ($imagenes as $value): ?>
	    <div class="div__imageupload col-md-2">
		  <a class="btns btn_eliminarImagen" data-uid="<?php echo $value['ContacsFile']['id'] ?>" data-name="<?php echo $value['ContacsFile']['img'] ?>"><i class="flaticon-cancel" aria-hidden="true"></i></a>
		   <div class="img-size-upload1">	
		      <div class="thumbnail">
		       <img class="img-fluid img-thumbnail" title="<?php echo __('Imagen').' '.$i ?>" src="<?php echo $this->Html->url('/files/Contac/'.$value['ContacsFile']['img']); ?>"/>
		      </div>
		   </div>
		</div>
	<?php $i++; endforeach; ?>
<?php }else{ ?>
	<p class="text-center"> <?php echo __('No hay imágenes.') ?> </p>
<?php } ?>