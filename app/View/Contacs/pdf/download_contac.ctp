<div>
<?php echo $this->Html->charset(); ?>
</div>
<div style="width: 100%;display:block;margin:0 auto; padding: 5px;">
	<table align="center" bgcolor="#ffffff" border="1" width="100%" cellspacing="5" cellpadding="5" style="margin: 0 0 -10px 0">
		<!-- <tr colspan="3" align="center"> -->
		    <!-- <td colspan="3"> -->
	    	 	<?php if(!empty($contac["contac"]["Team"]["img"])): ?> 
	    	 		 <?php //echo $this->Html->image("/document/WhiteBrand/{$contac["contac"]["Team"]["img"]}", array('width' => '200px', 'style' => 'height:auto;')); ?> 
		    	<?php else: ?>
			    	<?php // echo $this->Html->image("/img/frontend/admin/logo.png", array('width' => '250px;')); ?>
		    	<?php endif; ?>	
		    <!-- </td> -->
		<!-- </tr> -->
		<tr colspan="3" align="center">
		  <td colspan="1" style="width:300px;"><strong><?php echo __('Acta') ?>:</strong ></td>
			<td colspan="2" style="width:300px;color:#aaaaaa;">#<?php echo $contac["contac"]["Contac"]["number"] ?></td>
		</tr>
		<?php if(!empty($contac["contac"]["Team"]["img"])): ?> 
			<tr colspan="3" align="center">
				<td colspan="1" style="width:300px;" align="center" valign="middle">
			  		<div style="text-align:center;">
			    		<?php echo $this->Html->image("/document/WhiteBrand/{$contac["contac"]["Team"]["img"]}", array('width' => '200px', 'style' => 'height:auto;')); ?> 
			  		</div>
			 	</td>
			 	<?php if (!file_exists(WWW_ROOT."files/Project/{$contac["contac"]["Project"]["img"]}")): ?>
				 	<td colspan="2" align="center" valign="middle">  
				     	<?php echo $this->Html->image("/img/frontend/website/email/logo.jpg", array('width' => '200px','style' => 'height:auto;')); ?>	
					</td>
				 <?php else: ?>
				 	<td colspan="2" style="width:300px;" align="center" valign="middle">
				     	<?php echo $this->Html->image("/files/Project/{$contac["contac"]["Project"]["img"]}", array('width' => '200px','style' => 'height:auto;')); ?>
					</td>
				<?php endif; ?>
			</tr>
		<?php else: ?>
			<tr colspan="3" align="center">
				<?php if (!file_exists(WWW_ROOT."files/Project/{$contac["contac"]["Project"]["img"]}")): ?>
				 	<td colspan="3" align="center" valign="middle">  
				     	<?php echo $this->Html->image("/img/frontend/website/email/logo.jpg", array('width' => '200px','style' => 'height:auto;')); ?>	
					</td>
				 <?php else: ?>
				 	<td colspan="3" style="width:300px;" align="center" valign="middle">
				     	<?php echo $this->Html->image("/files/Project/{$contac["contac"]["Project"]["img"]}", array('width' => '200px','style' => 'height:auto;')); ?>
					</td>
				<?php endif; ?>
			</tr>
		<?php endif; ?> 
		<tr colspan="3" valign="top" align="left">
		  <td colspan="1" style="width:300px;"><strong style="color:#1e212a;"><?php echo __('Empresa'); ?>:</strong></td>
		  <td colspan="2" style="width:300px;color:#606060;word-break: break-all;"><?php echo $contac["contac"]["Team"]["name"] ?></td>
		</tr>
		<tr colspan="3" valign="top" align="left">
		  <td colspan="1" style="width:300px;"><strong style="color:#1e212a;"><?php echo __('Cliente'); ?>:</strong></td>
		  <td colspan="2" style="width:300px;color:#606060;word-break: break-all;"><?php echo $contac["contac"]["Client"]["name"] ?></td>
		</tr>
		<tr colspan="3" valign="top" align="left">
		  <td colspan="1" style="width:300px;"><strong style="color:#1e212a;"><?php echo __('Proyecto'); ?>:</strong></td>
		  <td colspan="2" style="width:300px;color:#606060;word-break: break-all;"><?php echo $contac["contac"]["Project"]["name"] ?></td>
		</tr>
		<tr colspan="3" valign="top" align="left">
		  <td colspan="1" style="width:300px;"><strong style="color:#1e212a;"><?php echo __('Fecha / hora inicio del acta') ?>:</strong></td>
		  <td colspan="2" style="width:300px;color:#606060;"><?php echo h($this->Time->format('d-m-Y h:i A', $contac['contac']['Contac']['start_date'])); ?></td>
		</tr>
		<tr colspan="3" valign="top" align="left">
		  <td colspan="1" style="width:300px;"><strong style="color:#1e212a;"><?php echo __('Fecha / hora fin del acta') ?>:</strong></td>
		  <td colspan="2" style="width:300px;color:#606060;"><?php echo h($this->Time->format('d-m-Y h:i A', $contac['contac']['Contac']['end_date'])); ?></td>
		</tr>
		<tr colspan="3" valign="top" align="left">
		  <td colspan="1" style="width:300px;"><strong style="color:#1e212a;"><?php echo __('Acta elaborada por'); ?>:</strong></td>
		  <td colspan="2" style="width:300px;color:#606060;"><?php echo $contac["contac"]["User"]["firstname"] . ' ' . $contac["contac"]["User"]["lastname"] ?></td>
		</tr>
		<tr colspan="3" valign="top" align="left" bgcolor="#f8f8f8">
		  <td colspan="3"><strong style="color:#1e212a;font-size:18px;"><?php echo __('Asistentes del cliente') ?>: </strong></strong></td>
		</tr>
		<tr colspan="3" valign="top" align="left" style="width:600px;">
		<?php if(!empty($contac['users']["assistants"])):?>
							<td colspan="3" style="color:#606060;width:600px;">
							<?php foreach ($contac['users']["assistants"] as $externo): ?>									 
							 	<?php echo $externo . "<br />\n"; ?>
							<?php endforeach; ?>
							</td>									 
						<?php else: ?>
							<td colspan="3" >
								<span style="color:#606060;">
								 	<?php echo __("No hay asistentes externos agregados al acta.")?>
			                    </span>
							</td>
						<?php endif; ?>
		</tr>

		<tr colspan="3" valign="top" align="left" bgcolor="#f8f8f8">
		  <td colspan="3"><strong style="color:#1e212a;font-size:18px;"><?php echo __('Colaboradores') ?>:</strong></td>
		</tr>
		<tr colspan="3" valign="top" align="left">
		<?php if(!empty($contac['users']["collaborators"])):?>
                            <td colspan="3" style="color:#606060;">
                            <?php foreach ($contac['users']["collaborators"] as $user): ?>									 
                                <?php echo $user . "<br />\n"; ?>
                            <?php endforeach; ?>
                            </td>									 
                        <?php else: ?>
                            <td colspan="3">
                                <span style="color:#606060;">
                                    <?php echo __("No hay funcionarios en el acta.")?>
                                </span>
                            </td>
                        <?php endif; ?>
		</tr>


		<tr colspan="3" valign="top" align="left" bgcolor="#f8f8f8">
		  <td colspan="3"><strong style="color:#1e212a;font-size:18px;"><?php echo __('Copias al correo electrónico'); ?></strong></td>
		</tr>


		<tr colspan="3" valign="top" align="left">
		   <?php $copies = explode(',', $contac['contac']['Contac']['copies']); ?> 
			 <?php if(!empty($copies["0"])): ?> 
				<td colspan="3">
					<span style="color:#606060;"><?php echo implode("<br />", $copies); ?></span>  
				</td>
				<?php else: ?>
				<td colspan="3">
					<span style="color:#606060;"><?php echo __("No hay copias de correo electrónico agregadas al acta.") ?></span>  
				</td>
			<?php endif;?>
		</tr>

		<tr colspan="3" valign="top" align="left">
		  <td colspan="1"><strong style="color:#1e212a;"><?php echo __('Estado'); ?>:</strong></td>
		  <td colspan="2"><?php echo $this->Utilities->stateContac($contac['contac']['Contac']['state']); ?></td>
		</tr>


				<tr style="width:600px; ">
			 		<td align=center bgcolor="#f8f8f8" colspan="3" class="text-center" style="width:600px;"><strong style="color:#1e212a;"><?php echo __('Compromisos') ?></strong></td>
						<tr colspan="3" style="width:600px;">						
							<td colspan="1" bgcolor="#f8f8f8" style="width:200px;"><strong style="color:#1e212a;"><?php echo __('Descripción'); ?></strong></td>
							<td colspan="1" bgcolor="#f8f8f8" style="width:200px;"><strong style="color:#1e212a;"><?php echo __('Usuario'); ?></strong></td>
							<td colspan="1" bgcolor="#f8f8f8" style="width:200px;"><strong style="color:#1e212a;"><?php echo __('Fecha límite'); ?></strong></td> 
						</tr> 
					<?php if(!empty($contac['commitments'])):?>
						<?php foreach ($contac['commitments'] as $commitment): ?>
							<tr colspan="3" style="width:600px;">							 
				 				<td colspan="1" style="width:200px;"><span style="color:#aaaaaa;"><?php echo $commitment["Commitment"]["description"]; ?></span></td>
				 				<td colspan="1" style="width:200px;"><span style="color:#aaaaaa;"><?php echo $commitment["User"]["firstname"] . '' . $commitment["User"]["lastname"] . ' - ' .$commitment["User"]["email"] ;?></span></td>	
				 				<td colspan="1" style="width:200px;"><span style="color:#aaaaaa;"><?php echo h($this->Time->format('d-m-Y', $commitment["Commitment"]["delivery_date"])); ?></span></td>
							</tr>
						<?php endforeach; ?>
						<?php else: ?>
							<tr colspan="3" style="width:600px;">
								<td colspan="3" style="width:600px;">
								<span style="color:#aaaaaa;">
								 	<?php echo __("No hay compromisos agregados al acta.")?>
								</span>
								</td>							
							</tr>
					<?php endif; ?>
				</tr>
		
				
	           <tr colspan="3" style="width:600px;" valign="top" align="center" bgcolor="#f2f2f2;">
	              <td colspan="3" style="width:600px;"><strong style="color:#1e212a;"><?php echo __('Imágenes del acta'); ?>:</strong></td>
			       </tr>

			   <tr colspan="3" style="width:600px;" valign="top">
					<?php if(!empty($contac['contac']["ContacsFile"])):?>
						<td colspan="3" style="width:600px;">
						<?php $contador = 0?>
						<?php foreach ($contac['contac']["ContacsFile"] as $file): ?> 
							<?php $contador++?>
							<?php echo $this->Html->image("/files/Contac/{$file['img']}", array('style' => 'height:auto; width:180px; float:left; padding:10px;')); ?> <span style="width:10px; float:left"> </span>							
							<?php if($contador == 4):?>
							<br>
							<br>
							<br>
							<?php $contador = 0?>
							<?php endif;?>
						<?php endforeach; ?>
						</td>									 
					<?php else: ?>
						<td colspan="3" style="width:600px;">
							<span id="selectExternos">
							 	<?php echo __("No hay imágenes en el acta.")?>
								</span>
						</td>
					<?php endif; ?>
				</tr>

				<tr colspan="3" style="width:600px;" valign="top" align="center" bgcolor="#f2f2f2;">
	            <td colspan="3" style="width:600px;"><strong style="color:#1e212a;"><?php echo __('Documentos del acta'); ?>:</strong></td>
			   </tr>


				<tr colspan="3" style="width:600px;">
					<?php if(!empty($contac['contac']["ContacsDocument"])):?>
						<?php $numberDocument = 1;?>
						<td colspan="3" style="width:600px;">
						<?php foreach ($contac['contac']["ContacsDocument"] as $document): ?>
								<?php 
									$filename  = $document['document'];
									$extension = pathinfo($filename, PATHINFO_EXTENSION);
								?> 
								<?php if($extension == "docx" || $extension == "doc"): ?>							 
	                        		<a style="color: #0f1c2c; font-size: 14px;"  target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
															    <!-- <?php echo __("Documento "). $numberDocument?> -->
	                    			 	    <?php echo $this->Html->image('/img/word.png', array('width' => '60px;','height' => 'auto')); ?>
															</a>
															
								<?php elseif ($extension == "pdf" || $extension == "PDF"): ?>
	                        		<a style="color: #0f1c2c; font-size: 14px;"  target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
																 <!-- <?php echo __("Documento "). $numberDocument?> -->
																 <?php echo $this->Html->image('/img/pdf.png', array('width' => '60px;','height' => 'auto')); ?>
															</a>
														
								<?php else :?>
									<a style="color: #0f1c2c; font-size: 14px; display:inline-block;margin-right:15px;"  target="_blank" href="<?php echo  Router::url("/",true)."document/Contac/{$document['document']}"?>">
										  <!-- <?php echo __("Documento "). $numberDocument?> -->
										  <?php echo $this->Html->image('/document/Contac/default.png', array('width' => '60px;','height' => 'auto')); ?>
									</a>
									
								<?php endif; ?>
	                       <?php $numberDocument++;?>
						<?php endforeach; ?> 								 
						</td>						  
					<?php else: ?>
					    <td colspan="3" style="width:600px;">
							<span id="selectExternos">
							 	<?php echo __("No hay documentos en el acta.")?>
							</span>
						</td>
					<?php endif; ?>
				</tr>   	
	</table>

 	<div> 
		<div style="margin:0 0 0 0; color:#606060; width: 100%;word-wrap: break-word; font-family: Arial, Helvetica, sans-serif;">
			 <div style="border:1px solid #1e212a; padding: 5px"> 
			 <p style="background-color:#f2f2f2;color:#1e212a;text-align:center;padding:5px;margin:0;font-weight:700;"><?php echo __('Descripción'); ?></p>

				<?php echo $contac['contac']['Contac']['description']; ?> 
			</div>
		</div>

	</div>  
</div> 
