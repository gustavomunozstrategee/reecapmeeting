<?php if (!empty($imagenes)) { ?>
	<div class="row">
		<?php $i = 1; foreach ($imagenes as $value): ?>
			<div class="col-md-3 form-group">				
				<a href="<?php echo $this->Html->url('/files/Contac/'.$value['ContacsFile']['img']); ?>" class="showImages">
					<div class="img-size-upload">
					   <div class="thumbnail">	
					      <img class="img img-responsive img-thumbnail" title="<?php echo __('Imagen').' '.$i ?>"  src="<?php echo $this->Html->url('/files/Contac/'.$value['ContacsFile']['img']); ?>"/>
			           </div>
			        </div> 
				</a>
			</div>
		<?php $i++; endforeach; ?>
	</div>
<?php }else{ ?>
	<div class="col-lg-12">
		<p class="text-empty"> <?php echo __('No hay imágenes.') ?> </p>
	<div>
<?php } ?>
