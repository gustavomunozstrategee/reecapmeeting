<?php 
	if(!empty($meeting)){
		$modalId = "meetingcontac";
	} else {
		$modalId = "";
	}
?>
<div id="<?php echo $modalId ?>" class="modal fade">
	<div class="modal-dialog modal-lg modal-recapmeeting">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
	        	<h4 class="modal-title"><?php echo __("Agenda de la reunión")?></h4>
	        </div>
	        <div id="modalBody" class="modal-body"> 
			    <div class="table-responsive">	
	    		<table cellpadding="0" cellspacing="0" class="table f-table-grid-layout-fixed">
					<thead>
						<tr>						
							<th class="table-grid-20 f-blue font-bold"><?php echo __('Creada por'); ?></th>
							<th class="table-grid-10 f-blue font-bold"><?php echo __('Hora'); ?></th>  
							<th class="table-grid-30 f-blue font-bold"><?php echo __('Lugar'); ?></th> 
							<th class="table-grid-40 f-blue font-bold"><?php echo __('Asunto'); ?></th> 
						</tr>
					</thead>
					<tbody>	
						<tr>
							<td class="table-grid-20 td-word-wrap"><?php echo h($meeting['User']['firstname'] . ' ' . $meeting['User']['lastname']); ?></td>
							<td class="table-grid-10 td-word-wrap"><?php echo h($this->Time->format('d-m-Y h:i A', $meeting['Meeting']['start_date'])); ?></td>
							<td class="table-grid-30 td-word-wrap"><?php echo h(!empty($meeting['Meeting']['location']) ? $meeting['Meeting']['location'] : __("No disponible")); ?>&nbsp;</td>
							<td class="table-grid-40 td-word-wrap"><?php echo h($meeting["Meeting"]['subject']); ?></td>
						</tr> 
					</tbody>
				</table>
				</div>
				<div class="table-responsive">	
				<table cellpadding="0" cellspacing="0" class="table table-hover">
					<thead class="text-primary">
						<h3 class="text-center"><?php echo __("Tags")?></h3>
					</thead>
					<tbody>	
						<?php if(!empty($meeting["Tag"])):?>
						<tr class="text-primary">						
							<th><?php echo __('Nombre'); ?></th> 
						</tr>
						<?php foreach ($meeting["Tag"] as $tag): ?>
							<tr> 
								<td><?php echo h($tag['name']); ?>&nbsp;</td>  
							</tr>
						<?php endforeach; ?>
						<?php else: ?>
		                    <tr>
		                        <td class="text-center" colspan="14"><?php echo __('No se definieron tags para la reunión.')?></td>
		                    </tr>
		            <?php endif; ?>
					</tbody>
				</table>
				</div>

				<div class="table-responsive">	
				<table cellpadding="0" cellspacing="0" class="table">
					<thead class="text-primary">
						<h3 class="text-center"><?php echo __("Temas específicos")?></h3>
					</thead>
					<tbody>	
						<tr class="text-primary">						
							<th><?php echo __('Descripción'); ?></th>
							<th><?php echo __('Tiempo'); ?></th> 
						</tr>
						<?php if(!empty($meeting["Topic"])):?>
						<?php foreach ($meeting["Topic"] as $topic): ?>
							<tr> 
								<td><?php echo h($topic['description']); ?>&nbsp;</td> 
								<td><?php echo $this->Utilities->conversorSegundosHoras($topic['time']); ?>&nbsp;</td> 
							</tr>
						<?php endforeach; ?>
						<?php else: ?>
                            <tr>
                                <td class="text-center" colspan="14"><?php echo __('No se  definieron temas específicos para la reunión.')?></td>
                            </tr>
                    <?php endif; ?>
					</tbody>
				</table>
				</div>

				<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover">
					<thead class="text-primary">
						<h3 class="text-center"><?php echo __("Asistentes")?></h3>
					</thead>
					<tbody>	
						<tr class="text-primary">						
							<th><?php echo __('Nombre'); ?></th>
							<th><?php echo __('Correo electrónico'); ?></th> 
							<th><?php echo __('Cliente'); ?></th> 
						</tr>
						<?php if(!empty($meeting["Assistant"])):?>
						<?php foreach ($meeting["Assistant"] as $assistant): ?>
							<tr> 
								<td><?php echo h($assistant['name']); ?>&nbsp;</td>  
								<td><?php echo h($assistant['email']); ?>&nbsp;</td>  
								<td><?php echo h($assistant['client']); ?>&nbsp;</td>  
							</tr>
						<?php endforeach; ?>
						<?php else: ?>
                            <tr>
                                <td class="text-center" colspan="14"><?php echo __('No se definió una agenda para la reunión.')?></td>
                            </tr>
                    <?php endif; ?>
					</tbody>
				</table> 
				</div>
	        </div>
	        <div class="modal-footer">
	            <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __("Cerrar")?></button>
	        </div>
	    </div>
	</div>
</div>
 