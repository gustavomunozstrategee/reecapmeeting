<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Actas por aprobar'); ?>
		        <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __('Listar'); ?>" href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'index'));?>">
			  		<i class="flaticon-list"></i>
			  	</a> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array('controller'=>'contacs', 'action'=>'index')); ?>">
                            <?php echo __("Actas") ?>
                        </a>
                    </li>                     
                    <li class="breadcrumb-item active"><?php echo __("Actas por aprobar")?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inlines w-100', 'type'=>'GET', 'role'=>'form'));?>
                    <div class="row">
                        <div class="col-md-3"> 
                            <div class="form-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Número acta, proyecto'), 'id'=>'q','label'=>false,'div'=>false,'class'=>'form-control'));?>
                            </div>
                        </div>
              
                        <div class="col-md-3"> 
                            <div class="form-group f-input-select-white_">
                                <?php echo $this->Form->input('project_id', array('class' => 'form-control select-picker-list','label'=>false,'div'=>false,'placeholder' => __('Nombre'), 'type' => 'select', 'options' => $projects, 'data-live-search' => 'true', 'empty' => __("Selecciona un proyecto"), 'selected' => !empty($this->request->query['project_id']) ? $this->request->query['project_id'] : '')); ?>
                            </div>
                        </div>
                
                        <div class="col-md-3"> 
                            <div class="form-group f-input-select-white_ form-select-enterprise_">
                                <?php if(!empty($this->request->query["teams"])):?>
                                    <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams select-picker-list','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'value' => $this->request->query["teams"], 'data-live-search' => 'true', 'empty' => __("Selecciona una empresa"))); ?>
                                <?php else: ?>
                                    <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams select-picker-list','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'data-live-search' => 'true', 'empty' => __("Selecciona una empresa"))); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    
                        <div class="col-md-3"> 
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>

                        <div class="col-md-12"> 
                            <?php if(empty($contacs) && !empty($this->request->query['q'])) : ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="widget-body">
            	<ul class="nav nav-tabs nav-fill" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?php echo $this->params->params['controller'] == 'contacs' && $this->params->params['action'] == 'index'? 'active' : '' ?>" 
                            href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'index'));?>">
                            <?php echo __("Todas las actas")?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $this->params->params['controller'] == 'contacs' && $this->params->params['action'] == 'approval_contacs'? 'active' : '' ?>" 
                            href="javascript:void(0)">
                            <?php echo __("Actas por aprobar")?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content pt-3">
		            <div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
							<thead class="text-primary">
								<tr>
                                    <th class="table-grid-10 text-center"><?php echo __('#Acta'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Cliente'); ?></th>						
									<th class="table-grid-10"><?php echo __('Proyecto'); ?></th>
                                    <th class="table-grid-10"><?php echo __('Fecha y hora inicio'); ?></th>
									<!-- <th class="table-grid-10 text-center"><?php echo __('Número del acta'); ?></th> -->
									<th class="table-grid-10"><?php echo __('Estado'); ?></th>
									<th class="table-grid-10 text-center"><?php echo __('Acciones'); ?></th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($contacs)):?>
								<?php foreach ($contacs as $contac): ?>
									<tr>
                                         <?php if($contac['Contac']['state'] == Configure::read('ENABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')){ ?>
                                            <td class="table-grid-10 td-word-wrap text-center">
                                                <span class="table-number">
                                                    <?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?>
                                                </span> 
                                            </td>
                                        <?php  } else { ?>
                                            <td class="table-grid-10 td-word-wrap text-center">
                                                <span class="table-number">
                                                    <?php echo h($contac['Contac']['number']); ?></span> 
                                                </td>
                                        <?php } ?>
                                        <td class="table-grid-10 td-word-wrap">
                                            <?php echo h($contac['Client']['name']) ?>
                                        </td>
										<td class="table-grid-10 td-word-wrap">
											<?php echo $this->Text->truncate($contac['Project']['name'],100); ?>
										</td>
										
                                        <td class="table-grid-10 td-word-wrap"><?php echo h($this->Time->format('d-m-Y h:i A', $contac['Contac']['start_date'])); ?>&nbsp;</td>
										<td class="table-grid-10 td-word-wrap"><?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?>&nbsp;</td>
		 
										<td class="table-grid-10 td-actions text-center"> 
										    <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'detail_contac_to_approved', EncryptDecrypt::encrypt($contac['ApprovalContac']['id']))); ?>" title="<?php echo __('Ir a calificar acta'); ?>" class="btn-xs" data-toggle="tooltip">
										        <i class="la la-check"></i>
										    </a>  
										</td>
									</tr>
								<?php endforeach; ?>
								<?php else: ?>
		                            <tr>
		                                <td class="text-center" colspan="9"><?php echo __('No existen actas por aprobar.')?></td>
		                            </tr>
		                    <?php endif; ?>
							</tbody>
						</table>
		            </div>
		        </div>
            </div>
            <div class="widget-footer border-top p-4">
        		<div class="table-pagination">
        			<div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
					<div>
						<ul class="pagination f-paginationrecapmeeting">
							<?php
								echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
								echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
								echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
							?>
						</ul>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</div>
<?php
    $this->Html->scriptBlock("$.fn.selectpicker.Constructor.BootstrapVersion = '4';", array('block' => 'AppScript')); 
    $this->Html->scriptBlock("$('.select-picker-list').selectpicker();", array('block' => 'AppScript'));
    
    if ($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
?>
