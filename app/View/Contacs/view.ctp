<?php $permissionAction = $this->Utilities->check_team_permission_action(array("index"), array("contacs")); ?>

<!-- Begin Page Header-->
<div class="row">
  <div class="page-header">
    <div class="d-flex align-items-center">
      <h2 class="page-header-title"><?php echo __('Visualización del acta'); ?></h2>
      <div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?php echo Router::url("/",true) ?>">
              <i class="ti ti-home"></i>
            </a>
          </li>
          <?php if(isset($permissionAction["index"])) : ?>
            <li class="breadcrumb-item">
              <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Actas") ?></a>
            </li>
          <?php endif ?> 
          <li class="breadcrumb-item active"><?php echo __("Ver acta") ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- End Page Header -->

<div class="row flex-row">
    <div class="col-12">
        <!-- Form -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h4 class="font-weight-bolder form-control-label">
                    <?php echo __('Número del acta'); ?>: 
                    <?php if ($contac['Contac']['state'] == Configure::read('ENABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')): ?>
                    <?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?>
                    <?php else: ?>
                     <?php echo h($contac['Contac']['number']); ?>
                   <?php endif; ?>
                </h4>
                    <?php if ($contac['Contac']['user_edit'] != Configure::read('DISABLED') && $contac['Contac']['state'] != Configure::read('DISABLED')): ?>
                    <?php if ($contac['Contac']['state'] == Configure::read('CONTAC_EDITAR')): ?>
                      <p class="f-blue font-bold"><?php echo __('El acta está siendo editada por el usuario').': '.$usuario_edit['User']['firstname'].' '.$usuario_edit['User']['lastname']."." ?></p>
                    <?php endif ?>
                  <?php endif ?>

            </div>
            <div class="widget-body px-5">
                <div class="form-horizontal">
                
                	<div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __("Audios")?>
                        </label>
                        <?php if($contac['Contac']['state'] == 0): ?>
                            <?php if($audioURL != ""): ?>
                                <?php $number = 0; ?>
                                <?php foreach($audioURL as $key => $value): ?>
                                    <?php $number++;  ?>
                                    <div>
                                    <label> <?php echo __("Audio #").$number ?> </label><br>
                                        <audio controls>
                                          <source src="<?php echo $this->webroot.$value ?>" type="audio/ogg">
                                          <source src="<?php echo $this->webroot.$value ?>" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                    </div>
                                <?php endforeach; ?>
                            
                            <?php else: ?>
                                <label class="col-lg-8 form-control-label">
                                    <?php echo  __("Audio no Disponible") ?>
                                </label>
                            <?php endif;?>
                        <?php else: ?>
                            <label class="col-lg-8 form-control-label">
                                  
                                <?php echo __("Aun no se ha terminado el Acta") ?>
                            </label>
                        <?php endif; ?>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                	
                    
                    
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __("Estado")?>
                        </label>
                        <label class="col-lg-8 form-control-label">
                            <?php echo $this->Utilities->stateContac($contac['Contac']['state']); ?>
                        </label>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Fecha y hora de la última modificación'); ?>
                        </label>
                        <div class="col-lg-8">
                            <?php echo h($this->Time->format('Y-m-d H:i:s', $contac['Contac']['modified'])); ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label"><?php echo __('Empresa'); ?></label>
                        <div class="col-lg-8">
                            <label id="lbl_team"><?php echo $contac['Team']['name']; ?> </label>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <?php if($contac['Contac']['state'] == Configure::read('DISABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')):?>
                        <?php if(!empty($contac['Team']['img'])):?>
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-lg-4 form-control-label">
                                    <?php echo __('Logo'); ?>
                                </label>
                                <div class="col-lg-8">
                                    <a href="<?php echo $this->Html->url('/document/WhiteBrand/'.$contac['Team']['img']); ?>" class="showImages">                    
                                        <img src="<?php echo $this->Html->url('/document/WhiteBrand/'.$contac['Team']['img']); ?>" width="100px" class="img-responsive logo-add hidden-xs" alt=""/>
                                    </a>
                                </div>
                                <div class="em-separator separator-dashed w-100"></div>
                            </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <div id="permission_white_label">                

                        </div>
                    <?php endif; ?>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Cliente'); ?>
                            <div id="show_img_client"></div>
                        </label>
                        <div class="col-lg-8">
                            <label id="lbl_cliente"><?php echo $contac['Client']['name']; ?></label>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Proyecto'); ?>
                            <div id="show_img_project"></div> 
                        </label>
                        <div class="col-lg-8">
                            <label id="lbl_proyecto"><?php echo $contac['Project']['name']; ?></label>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Duración de la reunión'); ?></h4>
                            </div>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Fecha y hora del inicio de la reunión'); ?> 
                        </label>
                        <div class="col-lg-8">
                            <?php echo h($this->Time->format('d-m-Y h:i A', $contac['Contac']['start_date'])); ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Fecha y hora fin de la reunión'); ?> 
                        </label>
                        <div class="col-lg-8">
                            <?php echo h($this->Time->format('d-m-Y h:i A', $contac['Contac']['end_date'])); ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>  
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Asistentes a la reunión'); ?> 
                        </label>
                        <div class="col-lg-8">
                            <?php if($contac['Contac']['state'] == Configure::read('DISABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL') ): ?> 

                                <div class="input-form">
                                    <h3 class="form-control-label">
                                        <?php echo __('Colaboradores'); ?>     
                                    </h3>
                                </div>
                                
                                <?php if(!empty($users["collaborators"])): ?>
                                    <ul class="nav flex-column mb-3 mt-3">
                                        <?php foreach ($users["collaborators"] as $collaborator): ?>                                             
                                            <?php echo "<li class='nav-item'><i class='la la-user la-2x align-middle'></i>".__($collaborator). "</li>"; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <p>
                                        <?php echo __("No se agregaron colaboradores al acta."); ?>
                                    </p>
                                <?php endif; ?>

                                <div class="input-form">
                                    <h3 class="form-control-label">
                                        <?php echo __('Asistentes externos'); ?>
                                    </h3>
                                </div>

                                <?php if(!empty($users["assistants"])): ?>
                                    <ul class="nav flex-column mb-3 mt-3">
                                        <?php foreach ($users["assistants"] as $assistant): ?>
                                            <?php echo "<li class='nav-item'><i class='la la-user la-2x align-middle'></i>".__($assistant) . "</li><br />\n"; ?>                      
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                      <p><?php echo __("No se agregaron asistentes externos al acta."); ?></p>
                                    <?php endif; ?> 
                            <?php else :?>
                                <div id="list_assistants"></div>
                            <?php endif; ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Copias al correo electrónico') ?>
                        </label>
                        <div class="col-lg-8">
                            <?php if ($contac['Contac']['state'] == Configure::read('DISABLED')) { ?>
                                <?php $copies = explode(',', $contac['Contac']['copies']); ?>
                                <div class="col-70-flex">
                                    <?php if(!empty($copies["0"])): ?>
                                        <div class="form-group">
                                            <p><?php echo implode("<br />", $copies); ?></p>
                                        </div>
                                    <?php else: ?>
                                        <div class="form-group">
                                            <p><?php echo __("No hay copias de correo electrónico agregadas al acta.") ?></p>
                                        </div> 
                                    <?php endif;?>
                               </div>
                            <?php } else { ?>
                                <div class="col-70-flex">
                                    <div class="form-group">
                                        <p id="lbl_copias">
                                            <?php echo __("No hay copias de correo electrónico agregadas al acta.") ?>
                                        </p>
                                    </div>
                              </div>
                            <?php } ?> 
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-lg-4 form-control-label">
                            <?php echo __('Creador del acta'); ?>
                        </label>
                        <div class="col-lg-8">
                            <?php if($contac['Contac']['user_id'] == Authcomponent::user("id")):?>
                                <?php echo $this->Html->link($contac['User']['firstname'].' '.$contac['User']['lastname'], array('controller' => 'users', 'action' => 'view', EncryptDecrypt::encrypt($contac['User']['id'])), array('class' => '')); ?>
                            <?php else: ?>
                                 <?php echo $contac['User']['firstname'].' '.$contac['User']['lastname']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12 form-control-label form-control-label">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Descripción') ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12 div-word-wrap p-5">  
                            <?php if ($contac['Contac']['state'] == Configure::read('DISABLED') || $contac['Contac']['state'] == Configure::read('APPROVAL')) { ?>
                                <?php echo $contac['Contac']['description'] ?>
                            <?php } else { ?>
                                <p  id="lbl_description"></p>
                            <?php } ?>
                        </div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12 form-control-label form-control-label">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Compromisos') ?></h4>
                            </div>
                        </div>
                    </div>       
                </div>
            </div>

            <div id="content-table"></div>
            
            <div class="widget-body">
                <div class="form-horizontal px-5">
                    <div class="form-group row d-flex align-items-center">
                        <div class="col-lg-12 form-control-label form-control-label">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Imágenes agregadas al acta.') ?> &nbsp;</h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <?php echo $this->Html->image('frontend/admin/icon-png.svg', array('height' => 'auto;','width' => '60px;')); ?>
                        <?php echo $this->Html->image('frontend/admin/icon-jpg.svg', array('height' => 'auto;','width' => '60px;')); ?>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <div id="imageDropzone" class="imageDropzone-div scroll-uploadimg w-100"></div>
                        <div class="em-separator separator-dashed w-100"></div>
                    </div>
                    <div class="form-group row d-flex align-items-center py-4">
                        <div class="col-lg-12 form-control-label form-control-label">
                            <div class="section-title mt-3 mb-3">
                                <h4><?php echo __('Documentos agregados al acta.') ?> &nbsp;</h4>
                            </div>
                            <div class="form-group row d-flex align-items-center">
                                <?php echo $this->Html->image('frontend/admin/icon-pdf.svg', array('height' => 'auto;','width' => '60px;')); ?>
                                <?php echo $this->Html->image('frontend/admin/icon-word.svg', array('height' => 'auto;','width' => '60px;')); ?>
                            </div>
                            <div class="form-group row d-flex align-items-center">
                                <div id="documentDropzone" class="imageDropzone-div row scroll-uploadimg"></div>
                                <div class="em-separator separator-dashed w-100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <?php if ($contac['Contac']['user_edit'] != Configure::read('DISABLED') && $contac['Contac']['state'] != Configure::read('DISABLED')): ?>
        <?php if ($contac['Contac']['state'] == Configure::read('CONTAC_EDITAR')): ?>
            <p class="f-blue font-bold"><?php echo __('El acta está siendo editada por el usuario').': '.$usuario_edit['User']['firstname'].' '.$usuario_edit['User']['lastname']."." ?></p>
        <?php endif ?>
    <?php endif ?>
</section>

<?php if ($contac['Contac']['state'] != Configure::read('DISABLED')): ?>
    <div class="modal fade" id="modalPreloadFirebase" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-recapmeeting" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo __('Cargando los datos') ?></h4>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?> 
<?php
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actions.js', ['block' => 'AppScript']);
    $this->Html->scriptBlock("EDIT.initElementView(); ", ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    }
?>
