<?php $permissionAction = $this->Utilities->check_team_permission_action(array("index"), array("contacs")); ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Registrar acta'); ?>
                <?php if(isset($permissionAction["index"])) : ?> 
                    <a class="badge badge-primary mr-1" href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                        <i class="la la-list"></i>
                    </a> 
                <?php endif ?> 
            </h2>
            <div>
                <?php if(isset($permissionAction["index"])) : ?> 
                    <button type='button' class='btn btn-primary ripple btnArchivos' id="btnAutoGuardado"><?php echo __('Habilitar autoguardado') ?></button>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<div class="row flex-row">
    <div class="col-xl-12">
        <div class="widget widget-07 has-shadow">
            <div class="widget-header bordered d-flex align-items-center">
                <h2>
                    <?php 
                        $infoMeeting = $this->Session->read("MeetingContac");
                        if(empty($infoMeeting)){
                            if(!empty($data)){
                                $meetingId = $data["Contac"]["meeting_id"];
                            } else {
                                $meetingId = NULL;
                            }
                        } else {
                            $meetingId = $infoMeeting["Meeting"]["id"];
                        }
                    ?>
                    <div id="show_last_contac"><!--MOSTRAR LA OPCIÓN DE VER EL ACTA ANTERIOR--></div>
                </h2>
                <div class="widget-options">
                    <?php echo $this->element('contact_previous_meeting_created', array('infoMeeting' => $meetingId)) ?>
                </div>
            </div>

            <div class="widget-body">
                <?php echo $this->Form->create('Contac', array('role' => 'form', 'datas-parsley-validate', 'type' => 'file','novalidate' => true)); ?>
                    <?php echo $this->Form->input('duration', array('type'=>'hidden', 'value'=>'00:00:00','label'=>false, 'div'=>false)); ?>
                    <div class="modal fade" id="modalChooseTeamWork" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-recapmeeting" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"><?php echo __('Seleccionar empresa') ?></h4>
                                </div>
                                <div class="modal-body"> 
                                    <div class="flex-center">
                                        <a class="m-10px f-link-blue-border" href="<?php echo $this->Html->url(array('controller'=>'teams','action'=>'add')) ?>"><?php echo __('Crear empresa') ?></a>
                                        <a class="m-10px f-link-blue-border" href="<?php echo $this->Html->url(array('controller'=>'contacs','action'=>'index')); ?>"><?php echo __('Regresar') ?></a>
                                    </div>
                                    <?php echo $this->Form->label('Contac.team_id', __('Empresa'), array('class' => 'control-label f-blue')); ?>
                                    <?php if(!empty($infoMeeting)):?>
                                        <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input select2_all', 'required' => true, 'label' => false, 'div' => false, 'empty' => __('Selecciona una opción'), 'value' => $infoMeeting["Meeting"]["team_id"])); ?> 
                                    <?php else: ?>
                                        <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input select2_all', 'required' => true, 'label' => false, 'div' => false, 'empty' => __('Selecciona una opción'))); ?> 
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"><?php echo __('Empresa'); ?></label>
                        <div class="col-lg-9">
                            <a data-toggle="modal" href="#modalChooseTeamWork"><?php echo __("Cambiar empresa")?></a> 
                        </div>
                    </div>
                    <div id="permission_white_label">
                        <!-- AQUI SE CARGARÁ SI EL EQUIPO DE TRABAJO TIENE PERMISO DE SUBIR LA MARCA BLANCA-->
                    </div>   
                    <?php $permissionClient  = $this->Utilities->check_team_permission_action(array("add"), array("clients")); ?>
                    <?php $permissionProject = $this->Utilities->check_team_permission_action(array("add"), array("projects")); ?> 

                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"><?php echo __('Cliente'); ?></label>
                        <div class="col-lg-9">
                            <div class="form-group form-project form-project-max">
                                <?php echo $this->Form->label('Contac.client_id', __('Cliente'), array('class' => 'control-label f-blue')); ?>
                                <?php if(isset($permissionClient["add"])) : ?> 
                                    <a class="btn-select2" data-toggle='modal' id="btn_modalAddClient" title="<?php echo __('Registrar un nuevo cliente'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a>
                                <?php endif; ?>
                                <div id="load_clients_team">
                                    <?php echo $this->Form->input('Contac.client_id', array('class' => 'form-control border-input select2_all', 'required' => false, 'label' => false, 'div' => false,'value' =>  @$this->request->data["Contac"]["client_id"], 'options' => $clients, 'empty' => __("Seleccione una opción"))); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"><?php echo __('Proyecto'); ?></label>
                        <div class="col-lg-9">
                            <div id="show_img_project" class="image-project"></div>                        
                            <div class="form-group form-project form-project-max">
                                <?php echo $this->Form->label('Contac.project_id', __('Proyecto'), array('class' => 'control-label f-blue')); ?>
                                <?php if(isset($permissionProject["add"])) : ?> 
                                    <a class="btn-select2" data-toggle='modal' id="btn_modalAddProject" title="<?php echo __('Registrar un nuevo cliente'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a>
                                <?php endif; ?>
                                <div id="load_projects_client">
                                    <?php echo $this->Form->input('Contac.project_id', array('class' => 'form-control border-input select2_all', 'required' => false, 'label' => false, 'div' => false, 'value' => @$this->request->data["Contac"]["project_id"], 'options' => $projects, 'empty' => __("Seleccione una opción"))); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"><?php echo __('Duración de la reunión'); ?></label>
                        <div class="col-lg-9">
                            <div class="form-group">
                                <div class="input-form">
                                    <?php echo $this->Form->label('Contac.start_date', __('Fecha y hora del inicio de la reunión'), array('class' => 'control-label f-blue')); ?>
                                    <div class='input-group date' id='datetimepickerOn'>
                                        <?php echo $this->Form->input('start_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Calendario'), 'type' => 'text', 'readonly' => true)); ?>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-form">
                                    <?php echo $this->Form->label('Contac.end_date', __('Fecha y hora del fin de la reunión'), array('class' => 'control-label f-blue lbl_endDate')); ?>
                                    <div class='input-group date' id='datetimepickerOff'>
                                        <?php echo $this->Form->input('end_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Calendario'), 'type' => 'text', 'readonly' => true)); ?>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"><?php echo __('Asistentes a la reunión'); ?></label>
                        <div class="col-lg-9">
                            <!-- COLABORADORES -->
                            <div class="form-group">
                                <div>
                                    <?php echo $this->Form->label('Contac.funcionarios', __('Colaboradores'), array('class' => 'control-label f-blue')); ?>
                                    <!-- <a class="btn-select2" data-toggle='modal' id="btn_modalAddFuncionario" title="<?php echo __('Registrar un nuevo funcionario de la empresa'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a> -->
                                </div>
                                <div id="load_collaborators_team">
                                    <?php echo $this->Form->input('Contac.funcionarios', array('class' => 'form-control select2_all', 'label' => false, 'div' => false,'type' => 'select', 'multiple' => 'multiple','value' => @$this->request->data["Contac"]["funcionarios"], 'options' => $collaborators)); ?>            
                                </div>
                            </div> 
                            <!-- EXTERNOS -->
                            <div class="form-group">
                                <div>
                                    <?php echo $this->Form->label('Contac.externos', __('Asistentes externos'), array('class' => 'control-label f-blue')); ?>
                                    <!-- <a class="btn-select2" data-toggle='modal' id="btn_modalAddEmployee" title="<?php echo __('Registrar un nuevo asistente del cliente'); ?>"><i class="flaticon-add" aria-hidden="true"></i></a> -->
                                </div>
                                <div id="load_assitants_team"> 
                                    <?php echo $this->Form->input('Contac.externos', array('class' => 'form-control select2_all', 'label' => false, 'type' => 'select', 'multiple' => 'multiple','value' =>    @$this->request->data["Contac"]["externos"], 'options' => $assistants)); ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"><?php echo __('Copias de correos electrónicos'); ?></label>
                        <div class="col-lg-9">
                            <?php echo $this->Form->input('copiesI', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'data-role' => 'tagsinput')); ?>
                        </div>
                    </div>

                    
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <div id="content-approval-solicitud_">
                                    <label for="cbox_perrmission_report_tickets_area">
                                        <input type="checkbox" name="data[Contac][approval]" value="1" id="requestApproval">
                                        <b class=" f-blue font-bold"><?php echo __('¿Solicitar aprobación?') ?></b>
                                    </label>
                                </div>
                            </div>
                            <div id="content-approval">
                                <div>
                                    <?php echo $this->Form->label('Contac.approvalusers', __('Solicitar aprobación de usuarios'), array('class' => 'control-label f-blue')); ?>
                                </div>
                                <div class="form-group">
                                    <div id="load_content_user_approval">
                                        <?php echo $this->Form->input('approvalusers', array('class' => 'form-control select2_all select_approval_user', 'label' => false, 'div' => false, 'multiple' => 'multiple', 'type' => 'select')); ?> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php echo $this->Form->label('Contac.date_limit', __('Fecha límite para calificar'), array('class' => 'control-label f-blue')); ?>
                                    <div class='input-group date' id='datetimepickerdatelimit'>
                                        <?php echo $this->Form->input('limit_date', array('class' => 'form-control border-input', 'label' => false, 'div' => false, 'placeholder' => __('Fecha límite de calificación'), 'type' => 'text', 'readonly' => true)); ?>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <label class="label-checkbox-recapmeeting">
                                    <?php echo $this->Form->checkbox('protegida', array('hiddenField' => false, 'div' => false, 'label' => false, 'type' => 'checkbox', 'id' => 'ContacProteger')); ?>
                                    <b class=" f-blue font-bold"><?php echo __("Proteger con contraseña") ?></b>
                                </label>
                            </div>
                            <div class="input-group">
                                <?php echo $this->Form->input('password', array('class' => 'form-control border-input', 'type' => 'password', 'placeholder' => __('Contraseña'), 'label' => false, 'div' => false, 'maxlength' => 4, 'autocomplete' => 'new-password')); ?>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn_generatePassword" type="button"><span class="glyphicon glyphicon-refresh f-blue"></span></button>
                                </span>
                            </div>
                            <div>
                                <button type="button" id="btn_ocultarPassword" class="link-show-password"><?php echo __("Ocultar contraseña") ?></button>
                                <button type="button" id="btn_verPassword"     class="link-show-password"><?php echo __("Mostrar contraseña") ?></button>
                            </div> 
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-3 form-control-label"></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <label class="label-checkbox-recapmeeting">
                                    <?php echo $this->Form->checkbox('protegida', array('hiddenField' => false, 'div' => false, 'label' => false, 'type' => 'checkbox', 'id' => 'ContacProteger')); ?>
                                    <b class=" f-blue font-bold"><?php echo __("Proteger con contraseña") ?></b>
                                </label>
                            </div>
                            <div class="input-group">
                                <?php echo $this->Form->input('password', array('class' => 'form-control border-input', 'type' => 'password', 'placeholder' => __('Contraseña'), 'label' => false, 'div' => false, 'maxlength' => 4, 'autocomplete' => 'new-password')); ?>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn_generatePassword" type="button"><span class="glyphicon glyphicon-refresh f-blue"></span></button>
                                </span>
                            </div>
                            <div>
                                <button type="button" id="btn_ocultarPassword" class="link-show-password"><?php echo __("Ocultar contraseña") ?></button>
                                <button type="button" id="btn_verPassword"     class="link-show-password"><?php echo __("Mostrar contraseña") ?></button>
                            </div> 
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <div class="col-lg-12">
                            <div id="permission_create_template">                        
                                <!-- MÉTODO AJAX QUE DEVOLVERÁ SI EL USUARIO DEPENDIENDO DEL EQUIPO DE TRABAJO SELECCIONADO PUEDE CREAR PLANTILLAS -->
                            </div>
                            <div class="listTemplate"></div> 
                        </div>
                    </div>


                    <div class="form-group row d-flex align-items-center mb-5">
                        <div class="col-lg-12">
                            <div class="form-group form-project form-project-max">
                                <label class="f-blue font-bold"><?php echo __('Descripción') ?></label>
                                <?php echo $this->Form->input('description', array('class' => 'form-control border-input', 'label' => false, 'required' => true, 'placeholder' => __('Descripción de la reunión'), 'div' => false)); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center mb-5">
                        <div class="col-lg-12">
                            <div id="show-content-meeting-day-choose"> </div>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center mb-5">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table-compromise f-table-grid-layout-fixed table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" colspan="3"><label class="f-blue"><?php echo __('Compromisos') ?></label></th>
                                        </tr>
                                        <tr>
                                            <th class="text-center"><?php echo __("Fecha límite") ?></th>
                                            <th class="text-center"><?php echo __("Usuario"); ?></th>
                                            <th class="text-center"><?php echo __("Descripción") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody class="hijos">
                                        <tr id="nuevoCompromiso" class="tr-height100">
                                            <td class="table-grid-30-compromises">
                                                <div class="content-date-compromises center-block">
                                                    <div class="input-group  date_all_contac date">
                                                        <?php echo $this->Form->input('Commitments.NUEVO.fecha', array('readonly', 'placeholder' => __('Fecha límite'), 'class' => 'form-control COMMITMENT_DATE_REQUIRE date_all', 'label' => false, 'div' => false)) ?>
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="table-grid-30-compromises">
                                                <div class="content-users-compromises center-block">
                                                    <div id="load_content_users_commitment">
                                                        <?php echo $this->Form->input('Commitments.NUEVO.asistente', array('empty'=>__('Seleccionar...'), 'class' => 'form-control select2_all selectclient', 'label' => false, 'type' => 'select','options' => $allUsers)); ?>
                                                    </div>
                                                </div>
                                            </td> 
                                            <td class="table-grid-30-compromises">
                                                <div class="content-description-compromises center-block">
                                                    <?php echo $this->Form->textarea('Commitments.NUEVO.description', array('class' => 'form-control resize-none COMMITMENT_DESC_REQUIRE', 'placeholder' => __('Descripción'), 'label' => false, 'rows' => 3)) ?>
                                                    <button class="f-btn-delete btn-remove-nuevo-compromiso hidden" data-id="NUEVO" type="button"><?php echo __("x"); ?></button>
                                                </div> 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="commitments" class="f-table-grid-layout-fixed table-bordered">
                                    <tbody class="hijosF">
                                        <?php if(!empty($this->request->data["Commitments"])):?>
                                            <?php unset($this->request->data["Commitments"]["NUEVO"]) ?> 
                                            <?php foreach ($this->request->data["Commitments"] as $keyId => $commitment):?>
                                                <tr id="nuevoCompromiso<?php echo $keyId ?>" class="tr-height100">
                                                    <td  class="table-grid-30">
                                                        <div class="content-date-compromises center-block">
                                                            <div class="input-group date_all_contac date">
                                                                <?php echo $this->Form->input("Commitments.{$keyId}.fecha", array('readonly', 'placeholder' => __('Fecha límite'), 'class' => 'form-control COMMITMENT_DATE_REQUIRE date_all', 'label' => false, 'value' => $commitment["fecha"], 'div' => false)) ?>
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td  class="table-grid-30">
                                                        <div class="content-users-compromises center-block">
                                                            <div id="load_content_users_commitment">
                                                                <?php echo $this->Form->input("Commitments.{$keyId}.asistente", array('class' => 'form-control select2_all selectclient', 'label' => false, 'type' => 'select','options' => $allUsers, 'value' => $commitment["asistente"])); ?>
                                                            </div>
                                                        </div>
                                                    </td> 
                                                    <td  class="table-grid-30">
                                                        <div class="content-description-compromises center-block">
                                                            <?php echo $this->Form->textarea("Commitments.{$keyId}.description", array('class' => 'form-control resize-none COMMITMENT_DESC_REQUIRE', 'placeholder' => __('Descripción'), 'label' => false, 'rows' => 3, 'value' => $commitment["description"])) ?>
                                                            <button class="f-btn-delete btn-remove-nuevo-compromiso" data-id="<?php echo $keyId ?>" type="button"><?php echo __("x"); ?></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>
                                        <?php endif; ?>  
                                    </tbody>
                                </table>
                            </div> 
                        </div>

                        <div class="col-lg-12">
                            <button class="addcompromiso btn btn-blue" type="button"><?php echo __('Guardar y crear nuevo compromiso') ?></button>
                        </div>

                        <div class="col-lg-12">
                            <div class="flex-justify-end">
                                <?php if(isset($permissionAction["index"])) : ?> 
                                    <button type='button' class='btn btn-blue btnArchivos mxy-10' id="btnGuardar" style="margin:10px">
                                        <?php echo __('Guardar y enviar') ?>
                                    </button>
                                    <button type='button' class='btn btn-blue btnArchivosBorrador mxy-10' id="btnSaveBorrador" style="margin:10px">
                                        <?php echo __('Guardar borrador') ?>
                                    </button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="widget-body">
                <div class="col-lg-12">
                    <!-- Imágenes -->
                    <div>
                        <div class="flex-space-between">
                            <p><span class="f-blue font-bold"><?php echo __('Imágenes')?></span>&nbsp;<?php echo __('Selecciona las imágenes que deseas adjuntar en el acta.') ?></p>
                            <div>
                            <?php echo $this->Html->image('frontend/admin/icon-png.svg', array('height' => 'auto;','width' => '60px;')); ?>
                            <?php echo $this->Html->image('frontend/admin/icon-jpg.svg', array('height' => 'auto;','width' => '60px;')); ?>
                            </div>
                        </div>
                        <div class="scroll-uploadimg">
                            <form method="post" action="saveImage" class="dropzone" id="dropzoneImage" enctype='multipart/form-data'> </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <!-- Documentos -->
                    <div>
                        <div class="flex-space-between">
                            <p><span class="f-blue font-bold"><?php echo __('Documentos') ?></span>&nbsp;<?php echo __('Selecciona los documentos que deseas adjuntar en el acta.') ?></p>
                            <div>
                                <?php echo $this->Html->image('frontend/admin/icon-pdf.svg', array('height' => 'auto;','width' => '60px;')); ?>
                                <?php echo $this->Html->image('frontend/admin/icon-word.svg', array('height' => 'auto;','width' => '60px;')); ?>
                            </div>
                        </div>
                        <div class="scroll-uploadimg">
                            <form method="post" action="saveImage" class="dropzone" id="dropzoneDocument" enctype='multipart/form-data'> </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->element('modals_contact');
    $this->Html->script('lib/datepicker/bootstrap-datepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('bootstrap.3.3.7/datetimepicker/js/bootstrap-datetimepicker.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker/locales/bootstrap-datepicker.es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
        $this->Html->script('bootstrap.3.3.7/datetimepicker/js/locales/bootstrap-datetimepicker.es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
?>
<style type="text/css">
    .table-compromise {
        margin-top: 2em;
    }
    .f-table-grid-layout-fixed {
        table-layout: fixed;
    }
    .table-compromise th {
        border: 1px solid #1e212a;
        color: #1e212a;
        padding: 1em 0;
        background-color: #eaeaea;
    }
    #nuevoCompromiso td {
        border: 1px solid #1e212a;
    }

    .table-grid-30-compromises {
        width: 30%;
        vertical-align: middle;
    }
</style>