<div class="row">
    <div class="col-md-3">
        <div class="widget widget-17 has-shadow">
            <div class="widget-body">
                <div class="row">
                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="counter"><?php echo h($totalEmployees); ?></div>
                        <div class="total-visitors"><?php echo __('Asistentes externos') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="widget widget-17 has-shadow">
            <div class="widget-body">
                <div class="row">
                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="counter"><?php echo h($totalContacs); ?></div>
                        <div class="total-visitors"><?php echo __('Actas') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="widget widget-17 has-shadow">
            <div class="widget-body">
                <div class="row">
                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="counter"><?php echo h($totalProjects); ?></div>
                        <div class="total-visitors"><?php echo __('Proyectos') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="widget widget-17 has-shadow">
            <div class="widget-body">
                <div class="row">
                    <div class="col-xl-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="counter"><?php echo h($totalClients); ?></div>
                        <div class="total-visitors"><?php echo __('Clientes') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>