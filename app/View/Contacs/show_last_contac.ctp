<?php if(!empty($lastContac)): ?> 
	<a class="view-detail-last-contact f-link-blue" href="<?php echo $this->Html->url(array("controller" => "contacs", "action" => "view", EncryptDecrypt::encrypt($lastContac['Contac']['id']))); ?>"><?php echo __("< Acta de la reunión anterior")?></a>
	<?php else: ?>           
	<h4 class="f-blue"><?php echo __("No hay ninguna acta realizada")?></h4>                 
<?php endif;?>