<?php $permissionAction = $this->Utilities->check_team_permission_action(array("add"), array("meetings")); ?>
<div class="page-header">
    <div class="d-flex align-items-center">
        <h2 class="page-header-title"><?php echo __("Inicio") ?></h2>
        <div>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href=""><i class="ti ti-home"></i> <?php echo __("Inicio") ?></a></li>
            </ul>
        </div>
    </div>
</div>

 
<div class="row flex-row" >
    <div class="col-md-12">
        <div class="widget has-shadow bg-black">
            
            <div class="widget-body sliding-tabs">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="widget-header bordered d-flex align-items-center">
                                        <h2>Mis Compromisos</h2>
                                        <div class="widget-options">
                                            <a href="<?php echo $this->webroot?>tasks/add_commitment">
                                                <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-success ripple"><i class="fa fa-plus-circle"></i><?php echo __("Crear Compromiso") ?>
                                                </button>

                                            </div>
                                            </a>
                                        </div>
                                    </div>
                            <div class="col-md-6" style="margin-top: 20px">
                                <div class="list-group w-100">
                                    <a class="list-group-item" href="<?php echo $this->webroot ?>tasks/mycommitments">
                                        <div class="media">
                                            <div class="event-date align-self-center mr-3">
                                                <i class="ion-clipboard text-linkedin" style="font-size: 4rem"></i>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <div class="event-title text-secondary"><?php echo __("Mis Compromisos") ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            </div>
                            
                            <?php if($userPosition["UserTeam"]["smf_state"] == 1): ?>
                             <div class="col-md-6" style="margin-top: 20px">
                                <div class="list-group w-100">
                                    <a class="list-group-item" href="<?php echo $this->webroot ?>tasks/reports">
                                        <div class="media">
                                            <div class="event-date align-self-center mr-3">
                                                <i class="ion-stats-bars text-linkedin" style="font-size: 4rem"></i>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <div class="event-title text-secondary"><?php echo __("Reporte SMF") ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>


                        <?php if($userPosition["Position"]["permission_taskee"] == "1" || $userPosition["Position"]["permission_taskee"] == "2"): ?>
                            <div class="row">
                                <div class="widget-header bordered no-actions d-flex align-items-center">
                                </div>
                                <div class="widget-header bordered no-actions d-flex align-items-center">
                                    <h4><?php echo __("Administración") ?></h4>
                                </div>
                                    
                        </div>



                        <?php endif; ?>



                        
                        <div class="row" style="margin-top: 20px">
                            <?php if($userPosition["Position"]["permission_taskee"] == "1" ): ?>
                            <div class="col-md-6" style="margin-top: 20px">
                                <div class="list-group w-100">
                                    <a class="list-group-item" href="<?php echo $this->webroot ?>tasks/admin">
                                        <div class="media">
                                            <div class="event-date align-self-center mr-3">
                                                <i class="ion-filing text-linkedin" style="font-size: 4rem"></i>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <div class="event-title text-secondary"><?php echo __("Administrar Compromisos") ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>
                            <?php endif; ?>
                            <?php if($userPosition["Position"]["permission_taskee"] == "1" || $userPosition["Position"]["permission_taskee"] == "2"): ?>
                                <div class="col-md-6" style="margin-top: 20px">
                                <div class="list-group w-100">
                                    <a class="list-group-item" href="<?php echo $this->webroot ?>tasks/report_admin_defeated">
                                        <div class="media">
                                            <div class="event-date align-self-center mr-3">
                                                <i class="ion-stats-bars text-linkedin" style="font-size: 4rem"></i>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <div class="event-title text-secondary"><?php echo __("Reporte de compromisos vencidos") ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>

                            <div class="col-md-6" style="margin-top: 20px">
                                <div class="list-group w-100">
                                    <a class="list-group-item" href="<?php echo $this->webroot ?>tasks/report_admin_pending">
                                        <div class="media">
                                            <div class="event-date align-self-center mr-3">
                                                <i class="ion-stats-bars text-linkedin" style="font-size: 4rem"></i>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <div class="event-title text-secondary"><?php echo __("Reporte de compromisos pendientes") ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>
                            <div class="col-md-6" style="margin-top: 20px">
                                <div class="list-group w-100">
                                    <a class="list-group-item" href="<?php echo $this->webroot ?>tasks/report_admin">
                                        <div class="media">
                                            <div class="event-date align-self-center mr-3">
                                                <i class="ion-stats-bars text-linkedin" style="font-size: 4rem"></i>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <div class="event-title text-secondary"><?php echo __("Reporte de compromisos terminados") ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>
                             
                            <?php endif; ?>
                            
                        </div>
                        
                    </div>
                             
                </div>
            </div>
        </div>
    </div>
</div>
 
 
<?php $this->Html->script('controller/pages/index.js', ['block' => 'AppScript']); ?>









