<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>



  <div class="container-fluid">
                        <!-- Begin Page Header-->
                        
                        <!-- End Page Header -->
                         <?php echo $this->element('nav_taskee'); ?>


<div class="col-xl-9">
                                <!-- Begin Widget 07 -->
                                <div class="widget widget-07 has-shadow">
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header bordered d-flex align-items-center">
                                        <h2>Reporte SMF</h2>
                                        <div class="widget-options">
                                            <a href="<?php echo $this->webroot?>tasks/add_commitment">
                                                <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-success ripple"><i class="fa fa-plus-circle"></i><?php echo __("Crear Compromiso") ?>
                                                </button>

                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- End Widget Header -->
                                    <!-- Begin Widget Body -->
                                    <div class="widget-body">
                                        <?php $this->request->data['Search'] = $this->request->query; ?>
                                        <?php echo $this->Form->create('Search', array('type'=>'GET','url'=>array('controller'=>$this->request->controller, 'action'=> $this->request->action))); ?>            
                                        <!-- <form method="POST" action="" name=""> -->
                                            <div class="form-group row mr-0 mb-3 ml-0">
                                                <div class="col-xl-6 mb-3">
                                                    <label>Rango de fechas:</label> 
                                                    <?php echo $this->Form->input('fechas', array('class' => 'form-control','id'=>"reporte","style"=>"margin-bottom: 10px","label"=>false)); ?>
                                                </div>
                                                <div class="col-xl-2">
                                                    <div class="btn-group btn-group-sm" style="float: none; margin-top: 28px">
                                                        <button type="submit" class="tabledit-edit-button btn btn-lg btn-success td-actions">
                                                            <i class="la la-filter p-1 mr-0 text-white"></i> Filtrar 
                                                        </button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        <!-- </form> -->
                                        <?php echo $this->Form->end(); ?>   
                                        
                                        <?php echo $this->Form->create('Search', array('type'=>'GET','url'=>array('controller'=>$this->request->controller, 'action'=> 'generate_report','target' => '_blank'))); ?>            
                                        <!-- <form method="POST" action="generate_report" name=""> -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input  style="display: none;" value="<?php if(isset($rangoFechas)) echo $rangoFechas[0] ?>" id="reporte" name="fechaInicio" readonly class="form-control">
                                                    <input style="display: none;" value="<?php if(isset($rangoFechas)) echo $rangoFechas[1] ?>" id="reporte" name="fechaFin" readonly class="form-control">
                                                    <div class="btn-group btn-group-sm" style="float: right; margin-top: 28px">
                                                        <button type="submit" class="tabledit-edit-button btn btn-lg btn-success td-actions">
                                                            <i class="la la-file-excel-o p-1 mr-0 text-white"></i> Exportar a Excel 
                                                        </button>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        <!-- </form> -->
                                        <?php echo $this->Form->end(); ?>   

                                        <ul class="list-group w-100 mt-5">
                                            <?php  $totalDia = 0; ?>
                                            <?php foreach ($commitments as $key => $value): ?>
                                            <?php 
                                            $totalCompromiso = UtilitiesHelper::calculateBonificationSMF($userPosition,$value["Commitment"]["type_categorie"], $value["Commitment"]["delivery_date"], $value["Commitment"]["priority"]);
                                            $totalDia = $totalDia + $totalCompromiso;
                                            ?>
                                            <li class="list-group-item">
                                                <div class="other-message">
                                                    <div class="media">
                                                        <div class="media-left align-self-center mr-3">
                                                            <div class="mb-3">
                                                                <div class="" >
                                                                    
                                                                    <label for="check-<?php echo $value["Commitment"]["id"] ?>"><span style="width:100px;" ><span class="badge-text badge-text-small success"> $ <?php echo number_format($value["Commitment"]["smf_generate"],0)  ?></span></span></label>
                                                                </div>
                                                            </div>
                                                             
                                                        </div>
                                                        <div class="media-left align-self-center mr-3">
                                                            <img src="<?php echo $this->Html->url('/files/User/'.AuthComponent::user('img')); ?>" class="img-fluid rounded-circle" style="width: 50px;">
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <div class="other-message-sender">
                                                                <?php if(empty(AuthComponent::user('firstname'))):?>
                                                                <?php echo __("Nombre no asignado");?>
                                                                <?php else: ?>
                                                                <?php echo $this->Text->truncate(h(AuthComponent::user('firstname')).' '.AuthComponent::user('lastname'),20); ?> 
                                                                <?php endif; ?>
                                                                <span style="width:160px;" class="pull-right">
                                                                    <span class="badge-text badge-text-small danger" style="width: 100%;">
                                                                    <?php
                                                                    echo   Configure::read("TipoCompromiso.".$value["Commitment"]["type_categorie"]) ."-". Configure::read("PrioridadTareas.".$value["Commitment"]["priority"])

                                                                     ?>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <div class="other-message-time">
                                                                <?php echo $value["Commitment"]["description"]?>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="media-body align-self-center">
                                                                            <div class="event-title text-secondary"><?php echo $value["Client"]["name"] ?> - <?php echo $value["Project"]["name"] ?></div>
                                                                            <div class="event-desc mr-3">

                                                                                <i class="fa fa-calendar"></i>
                                                                                <span><?php echo $value["Commitment"]["delivery_date"] ?></span> 

                                                                                <?php if($this->request->is('mobile')) { ?> 
                                                                                    <br> 
                                                                                <?php } ?>
                                                                               

                                                                                <i class="fa fa-flag-checkered"></i>
                                                                                <span><?php echo $value["Commitment"]["finished_date"] ?></span> 

                                                                                <?php if($this->request->is('mobile')) { ?> 
                                                                                    <br> 
                                                                                <?php } ?>
                                                                                
                                                                                <i class="fa fa-bell"></i>

                                                                                <span>
                                                                                    <?php
                                                                                    if($value["Commitment"]["notification_date"] == "0000-00-00"){

                                                                                    echo "Sin definir";
                                                                                     
                                                                                    }else{
                                                                                    echo $value["Commitment"]["notification_date"];

                                                                                    }
                                                                                    ?>
                                                                                        
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         
                                                    </div>
                                                </div>
                                            </li>
                                            <?php endforeach?>
                                             <?php if(empty($commitments)): ?>
                                                 
                                                 <div class="alert alert-outline-info dotted" role="alert">
                                                    <strong></strong> No se encontraron resultados.
                                                </div>
                                             
                                                 
                                                 <?php endif ?>
                                             
                                        </ul>
                                    </div>
                                    <!-- End Widget Body -->
                                    <!-- Begin Widget Footer -->
                                     
                                    <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination justify-content-end">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>

                                    <!-- End Widget Footer -->
                                </div>
                                <!-- End Widget 07 -->
                            </div>
                            </div>

<?php
  
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);

  
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
  
   
?>
 

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
</style>
 