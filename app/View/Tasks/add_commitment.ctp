 <div class="container-fluid">
                        <!-- Begin Page Header-->
                        
                        <!-- End Page Header -->
                         <?php echo $this->element('nav_taskee'); ?>



<div class="col-xl-9">
    
    <div class="widget widget-07 has-shadow">
        <div class="widget-header bordered no-actions d-flex align-items-center">
            <h4><?php echo __("Crear Compromiso") ?></h4>
        </div>
        <div class="card-body">
                <div class="ml-auto pull-right">
                    <div class="row">

                        
                    </div>
                    
                </div>
                <?php echo $this->Form->create('Commitment', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40')); ?>
                        <?php echo $this->Form->input('user_creator',array('class'=>'control-label',"type"=>"hidden","value"=>AuthComponent::user("id")));?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class='form-group'>
                            <?php echo $this->Form->label('Commitment.description',__('Descripción del compromiso'), array('class'=>'control-label'));?>
                            <?php echo $this->Form->input('description', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2")); ?>
                            <?php echo $this->Form->input('font', array('class' => 'form-control border-input','label'=>false,'div'=>false,"type" => "hidden","value" => "Web App")); ?>
                            <?php echo $this->Form->input('state', array('class' => 'form-control border-input','label'=>false,'div'=>false,"type" => "hidden","value" => "1")); ?>
                            </div>
                            </div>
                            <div class="col-md-4" id="notificacionDate">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Commitment.type',__('Tipo de Compromiso'), array('class'=>'control-label'));?>
                                    <?php echo $this->Form->input('type_categorie', array('class' => 'form-control border-input','label'=>false,'div'=>false,"options" => Configure::read("TipoCompromiso"))); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Commitment.priority',__('Prioridad'), array('class'=>'control-label'));?>
                                    <?php echo $this->Form->input('priority', array('class' => 'form-control border-input','label'=>false,'div'=>false,"options" => Configure::read("PrioridadTareas"))); ?>
                                </div> 
                            </div>
                            <div class="col-md-4">
                                

                                <div class="form-group">
                                        <?php echo $this->Form->label('Meeting.start_date',__('Fecha de Terminación'), array('class'=>'control-label f-blue'));?>
                                        <div class='input-group date ' id=''>
                                            <?php echo $this->Form->input('delivery_date', array('class' => 'form-control date_commiment', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                            </span>
                                        </div>
                                </div>
                            </div>
                        </div>
                         
                        <div class="row">
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Commitment.user_id',__('Usuario responsable'), array('class'=>'control-label select2'));?>
                                    <?php echo $this->Form->input('user_id', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Commitment.project_id',__('Proyecto'), array('class'=>'control-label'));?>
                                    <?php echo $this->Form->input('project_id', array('class' => 'form-control border-input select','label'=>false,'div'=>false,"empty"=>__("Sin proyecto"),"style"=>"")); ?>
                                </div>
                            </div>

                            


                        </div>
                        
                        
                        
                        
                        
                        <div class="row">
                            <div class="col-md-12 pull-right">
                                <button type='submit' class='btn btn-primary pull-right'><?php echo __("Crear compromiso") ?></button>
                            </div>
                        </div>
                <?php echo $this->Form->end(); ?>
            </div>

        
    </div>
</div>
</div>

<?php
    echo $this->element('modals_contact');
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
    echo $this->Html->css('frontend/admin/components/table.css'); 
?>

<script type="text/javascript">
    $( document ).ready(function() {
         
            $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,

                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });

             
        
    });
    
</script>
<style>
 #select2-CommitmentProjectId-container{
            border: solid 0.5px darkgrey;border-radius: 4px;
            }
</style>



