<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
 
<div class="container-fluid">
    <!-- Begin Page Header-->
    <!-- End Page Header -->
    <?php echo $this->element('nav_taskee'); ?>
    <div class="col-xl-9">
        <div class="col-xl-12">
            <!-- Begin Widget 07 -->
            <div class="widget widget-07 has-shadow">
                <!-- Begin Widget Header -->
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>Mis Compromisos</h2>
                    <div class="widget-options">
                        <a href="<?php echo $this->webroot?>tasks/add_commitment">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-success ripple"><i class="fa fa-plus-circle"></i><?php echo __("Crear Compromiso") ?></button> 
                            </div>
                        </a>
                    </div>
                </div>
                <!-- End Widget Header -->
                <!-- Begin Widget Body -->
                <div class="widget-body">
                    <div class="dropdown bootstrap-select show-menu-arrow">
                        <select class="selectpicker show-menu-arrow filtro">
                            <option <?php if(isset($_GET["order"]) && $_GET["order"] == "first" ) echo "selected" ?> value="first" data-icon="la la-sort-amount-desc" >Primeros a vencer</option>
                            <option <?php if(isset($_GET["order"]) && $_GET["order"] == "last" ) echo "selected" ?> value="last" data-icon="la la-sort-amount-asc">Ultimos a vencer</option>
                            <option <?php if(isset($_GET["order"]) && $_GET["order"] == "priority" ) echo "selected" ?> value="priority" data-icon="la la-star">Prioridad</option>
                        </select>
                    </div>
                    <ul class="list-group w-100 mt-5">
                        <?php  $totalDia = 0; ?>
                        <?php foreach ($commitments as $key => $value): ?>
                        <?php 
                            $totalCompromiso = UtilitiesHelper::calculateBonificationSMF($userPosition,$value["Commitment"]["type_categorie"], $value["Commitment"]["delivery_date"], $value["Commitment"]["priority"]);
                            $totalDia = $totalDia + $totalCompromiso;
                        ?>
                        <li class="list-group-item">
                            <div class="other-message">
                                <?php if($this->request->is('mobile')) { ?> 
                                    <div>
                                <?php } else { ?>
                                    <div class="media">
                                <?php } ?>
                                    <div class="media-left align-self-center mr-3">
                                        <div class="mb-3">
                                            <div class="styled-checkbox" >
                                                <input type="checkbox" name="" data-id="<?php echo $value["Commitment"]["id"] ?>" id="check-<?php echo $value["Commitment"]["id"] ?>" class="finish_commiment">
                                                <?php if($userPosition["UserTeam"]["smf_state"] == 1): ?>
                                                    <label for="check-<?php echo $value["Commitment"]["id"] ?>"><span style="width:100px;" ><span class="badge-text badge-text-small success"> $ <?php echo number_format($totalCompromiso,0)  ?></span></span></label>
                                                <?php else: ?>
                                                    <label for="check-<?php echo $value["Commitment"]["id"] ?>"><span style="width:100px;" ></label>
                                                <?php endif ?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="media-left align-self-center mr-3">
                                        <img src="<?php echo $this->Html->url('/files/User/'.AuthComponent::user('img')); ?>" class="img-fluid rounded-circle" style="width: 50px;">
                                    </div>
                                    <div class="media-body align-self-center">
                                        <div class="other-message-sender">
                                            <?php if(empty(AuthComponent::user('firstname'))):?>
                                                <?php echo __("Nombre no asignado");?>
                                            <?php else: ?>
                                                <?php echo $this->Text->truncate(h(AuthComponent::user('firstname')).' '.AuthComponent::user('lastname'),20); ?> 
                                            <?php endif; ?>
                                            <?php if($this->request->is('mobile')) { ?> 
                                                <span style="width:100%;">
                                            <?php } else { ?>
                                                <span style="width:160px;" class="pull-right">
                                                <?php } ?>
                                                <span class="badge-text badge-text-small danger" style="width: 100%;">
                                                    <?php echo Configure::read("TipoCompromiso.".$value["Commitment"]["type_categorie"]) ."-". Configure::read("PrioridadTareas.".$value["Commitment"]["priority"]) ?>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="other-message-time">
                                            <?php echo $value["Commitment"]["description"]?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="media-body align-self-center mt-2">
                                                        <div class="event-title text-secondary"><?php echo $value["Client"]["name"] ?> - <?php echo $value["Project"]["name"] ?></div>
                                                        <div class="event-desc">
                                                            <i class="fa fa-calendar"></i>
                                                                <span><?php echo $value["Commitment"]["delivery_date"] ?></span> 
                                                            <i class="fa fa-bell"></i>
                                                            <span>
                                                                <?php
                                                                    if($value["Commitment"]["notification_date"] == "0000-00-00"){
                                                                        echo "Sin definir";
                                                                    } else {
                                                                        echo $value["Commitment"]["notification_date"];
                                                                    }
                                                                ?>                                                                
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="media-right align-self-center">
                                        <div class="widget-options">
                                            <div class="dropdown">
                                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                                                    <i class="la la-ellipsis-h"></i>
                                                </button>
                                                <div class="dropdown-menu" x-placement="bottom-start" style="display: none; position: absolute; transform: translate3d(-113px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                    <a data-date="<?php echo $value["Commitment"]["notification_date"] ?>" data-id="<?php echo $value["Commitment"]["id"]; ?>" href="javascript:void(0)" class="dropdown-item cambioNotificacion"> 
                                                        <i class="la la-bell"></i> <?php echo __("Configurar Notificación") ?>
                                                    </a>
                                                    <a data-date="<?php echo $value["Commitment"]["delivery_date"] ?>" data-id="<?php echo $value["Commitment"]["id"]; ?>" href="javascript:void(0)" class="dropdown-item solicitarCambioFecha"> 
                                                        <i  class="la la-edit "></i><?php echo __("Solicitar Edición Fecha") ?>
                                                    </a>
                                                    <a data-id="<?php echo $value["Commitment"]["id"]; ?>" class="dropdown-item solicitarCambioDescripcion" href="javascript:void(0)"> 
                                                        <i class="la la-edit "></i><?php echo __("Solicitar Edición") ?>
                                                    </a>
                                                    <a data-id="<?php echo $value["Commitment"]["id"]; ?>" href="javascript:void(0)" class="dropdown-item faq rechazar_compromiso"> 
                                                        <i class="la la-remove"></i><?php echo __("Rechazar Compromiso") ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php endforeach?>
                        <?php if(empty($commitments)): ?>                         
                            <div class="alert alert-outline-success dotted" role="alert">
                                <strong>Bien!</strong> No tienes compromisos pendientes.
                            </div> 
                        <?php endif ?>

                        <?php if($userPosition["UserTeam"]["smf_state"] == 1): ?>
                            <li class="list-group-item">
                                <div class="other-message">
                                    <div class="media">
                                        <div class="media-left align-self-center mr-6">
                                            <hr>
                                             <span style="width: 200px !important" ><span  style="width: 200px !important" class="badge-text badge-text-small success">TOTAL HOY : $<?php echo number_format($totalDia,0); ?></span></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
                <!-- End Widget Body -->
                <!-- Begin Widget Footer -->
                 
                <div class="widget-footer border-top p-4">
                    <div class="table-pagination">
                        <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                        <div>
                            <ul class="pagination justify-content-end">
                                <?php
                                    echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                    echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <!-- End Widget Footer -->
            </div>
        <!-- End Widget 07 -->
        </div> 
    </div>
    <!-- End Row -->
</div>
 
<?php
  
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);

  
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
  
   
?>

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
</style>
 