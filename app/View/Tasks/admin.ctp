<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>


<div class="container-fluid">
<!-- Begin Page Header-->
                        
<!-- End Page Header -->
 <?php echo $this->element('nav_taskee'); ?>
 

    <div class="col-xl-9">
            <!-- Begin Widget 07 -->
        <div class="widget widget-07 has-shadow">
            <!-- Begin Widget Header -->
            <div class="widget-header bordered d-flex align-items-center">
                <h2><?php echo __("Solicitudes de Compromisos") ?></h2>
                 
            </div>
            <!-- End Widget Header -->
            <!-- Begin Widget Body -->
            <div class="widget-body">
                 
                <ul class="list-group w-100 mt-5">
                    <?php  $totalDia = 0; ?>
                    <?php foreach ($commitments as $key => $value): ?>
                    <li class="list-group-item divRemove_<?php echo $value["Log"]["CommitmentsLog"]["id"] ?>">
                        <div class="other-message">
                            <div class="media">
                                <div class="media-left align-self-center mr-3"></div>
                                <div class="media-left align-self-center mr-3">
                                    <img src="<?php echo $this->Html->url('/files/User/'.$value['User']['img']); ?>" class="img-fluid rounded-circle" style="width: 50px;">
                                </div>
                                <div class="media-body align-self-center">
                                    <div class="other-message-sender">
                                        <?php if(empty($value["User"]['firstname'])):?>
                                            <?php echo __("Nombre no asignado");?>
                                        <?php else: ?>
                                            <?php echo $this->Text->truncate(h($value["User"]['firstname']).' '.$value["User"]['lastname'],20); ?> 
                                        <?php endif; ?>
                                        <?php if($this->request->is('mobile')) { ?> 
                                            <span style="width:100%;">
                                        <?php } else { ?>
                                            <span style="width:160px;" class="pull-right">
                                            <?php } ?>
                                            <span class="badge-text badge-text-small danger" style="width: 100%;">
                                                <?php echo Configure::read("TipoCompromiso.".$value["Commitment"]["type_categorie"]) ."-". Configure::read("PrioridadTareas.".$value["Commitment"]["priority"]) ?>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="other-message-time">
                                        <?php echo $value["Commitment"]["description"]?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="media-body align-self-center mt-2">
                                                    <div class="event-title text-secondary"><?php echo $value["Client"]["name"] ?> - <?php echo $value["Project"]["name"] ?></div>
                                                    <div class="event-desc mr-3">
                                                        <i class="fa fa-calendar"></i>
                                                        <span><?php echo $value["Commitment"]["delivery_date"] ?></span> 
                                                        <?php if($this->request->is('mobile')) { ?> 
                                                            <br>
                                                        <?php } ?>
                                                        <i class="fa fa-bell"></i>
                                                        <span>
                                                            <?php
                                                            if($value["Commitment"]["notification_date"] == "0000-00-00"){
                                                                echo "Sin definir";                                                                 
                                                            } else {
                                                                echo $value["Commitment"]["notification_date"];
                                                            }
                                                            ?>                                                                    
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="media-right align-self-center">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row" style="justify-content: center;">
                            <label>
                                <?php
                                    if(isset($value["Log"]["CommitmentsLog"]["type"]) && $value["Log"]["CommitmentsLog"]["type"] == "date"){
                                        echo __("CAMBIO DE FECHA");
                                    }elseif ($value["Log"]["CommitmentsLog"]["type"] == "rechazo") {
                                       echo __("SOLICITUD DE RECHAZO");
                                    }elseif ($value["Log"]["CommitmentsLog"]["type"] == "description") {
                                       echo __("CAMBIO DE DESCRIPCIÓN");
                                    }
                                ?>
                            </label>  
                        </div>
                        <div class="container"  style="justify-content: center;">
                            <hr>
                            <?php
                                echo __('Fecha de cuándo se solicitó:').' <i class="fa fa-calendar"></i> '.$value["Log"]["CommitmentsLog"]["created"].'<br>';
                                if(isset($value["Log"]["CommitmentsLog"]["type"]) && $value["Log"]["CommitmentsLog"]["type"] == "date"){
                                    echo 'Nueva Fecha: <i class="fa fa-calendar"></i> '.$value["Log"]["CommitmentsLog"]["date"].'<br>';
                                }

                            ?> 
                            <br>
                            <p><?php echo $value["Log"]["CommitmentsLog"]["description"] ?></p>
                        </div>
                        
                        <div class="btn-group pull-right" role="group">
                            <a href="javascript:void(0);" data-id="<?php echo $value["Log"]["CommitmentsLog"]["id"] ?>"  class="btn btn-danger rechazarSolicitud"><?php echo "Rechazar" ?></a>
                            <a href="javascript:void(0);" data-id="<?php echo $value["Log"]["CommitmentsLog"]["id"] ?>" class="btn btn-success ripple aceptarSolicitud"><?php echo "Aceptar" ?></a>
                        </div>
                    </li>
                    <?php endforeach?>
                     <?php if(empty($commitments)): ?> 
                        <div class="alert alert-outline-success dotted" role="alert">
                            <strong>Bien!</strong> No tienes compromisos pendientes.
                        </div> 
                     <?php endif ?> 
                </ul>
            </div>
            <!-- End Widget Body -->
            <!-- Begin Widget Footer --> 
            <!-- End Widget Footer -->
        </div>
        <!-- End Widget 07 -->
    </div>
</div>

<?php
  
  $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
  $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);

  
  if($this->Session->read('Config.language') == 'esp') {
    $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
    $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
  }
  
   
?>

<style type="text/css">
    .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
.daterangepicker{
    z-index: 10000000 !important;
}
</style>
 