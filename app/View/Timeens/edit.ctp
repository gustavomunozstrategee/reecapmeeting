<?php
  echo $this->Html->css('materialDateTimePicker.css?v=202204180854');
  echo $this->Html->css('xdsoftdatetimepicker.min.css');
?>

<?php echo $this->Form->create('Timeen', array('role' => 'form','data-parsley-validate=""','class'=>'form-material m-t-40',"url" => array("action" => "edit") )); ?>
<div class="modal-dialog centered">
    <div class="modal-content">
      <div class="modal-header"><h4 class="modal-title" id="myModalLabel"><?php echo __("Editar Tiempo") ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body contenidoModal">
        <?php echo $this->Form->label('Proyecto',__('Proyecto'), array('class'=>'control-label'));?>
        <?php echo $this->Form->input('project_id', array('class' => 'form-control border-input select','label'=>false,'div'=>false,"empty"=>__("Seleccione Proyecto"),"required" =>"false")); ?>
        <br>

        <?php echo $this->Form->label('descripcion',__('Descripción'), array('class'=>'control-label'));?>
        <?php echo $this->Form->input('descripcion', array('class' => 'form-control border-input','label'=>false,'div'=>false,"rows"=>"2")); ?>
        <br>
                            <div class="row">
                              <div class="col-md-12">
                                    <div class="form-group">
                                             <?php echo $this->Form->label('Timeen.dia',__('Inicio de Actividad'), array('class'=>'control-label f-blue'));?>
                                            <div class='input-group date ' id=''>
                                              
                                                <?php echo $this->Form->input('fecha', array('class' => 'form-control timepicker_day', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                                 
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                    <div class="form-group">
                                             <?php echo $this->Form->label('Timeen.dia',__('Trabajo Desde:'), array('class'=>'control-label f-blue'));?>
                                            <div class='input-group date ' id=''>
                                              
                                                <?php echo $this->Form->input('hora_inicio', array('class' => 'form-control timepicker', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => false,'autocomplete' => 'off','maxlength' => 5)); ?>
                                                 
                                            </div>
                                    </div>
                                </div>
                                 
                                <div class="col-md-6">
                                    <div class="form-group ">
                                             <?php echo $this->Form->label('Timeen.dia',__('Trabajo Hasta:'), array('class'=>'control-label f-blue'));?>
                                            <div class='input-group date ' id=''>
                                              
                                                <?php echo $this->Form->input('hora_fin', array('class' => 'form-control timepicker', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => false,'autocomplete' => 'off','maxlength' => 5)); ?>
                                                 
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="validationsRequest"></div>
                                </div>
                            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
         
        <button type='submit' class='btn btn-success pull-right btnEnviar'><?php echo __("Guardar Cambios") ?></button>
      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>



<script type="text/javascript">
    $( document ).ready(function() {

        jQuery('.timepicker').datetimepicker({
            datepicker:false,
            format:'H:i',
            allowTimes:[
              '1:00' , '2:00',  '3:00', '4:00', '5:00',  '6:00',
              '7:00' , '8:00',  '9:00', '10:00','11:00', '12:00',
              '13:00', '14:00', '15:00','16:00', '17:00', '18:00', 
              '19:00', '20:00', '21:00', '22:00', '23:00', '00:00'
            ]
        });

        function loadContent(){
          $.ajax({
                type: "POST",
                url: "timeens/loadContent",
                data: {},
                beforeSend: function(){
                },
                complete:function(data){
                },
                success: function(data){
                    $(".loadContent").html(data);
                },
                error: function(data){
                    alert("Error de carga");
                }
            });
        }
         
            $('.date_commiment').daterangepicker({
                "singleDatePicker": true,
                "timePicker": false,
                "timePicker24Hour": true,

                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('.date_commiment_hour').daterangepicker({
                "singleDatePicker": false,
                "timePicker": true,
                "timePicker24Hour": false,

                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });
            $('#CommitmentProjectId').select2({
                language: 'es',
            });

            setTimeout(function() {
                $('.xdsoft_noselect').remove();
            }, 10);


        // $('#TimeenHoraInicio').bootstrapMaterialDatePicker({
        //     date: false,
        //     format: 'HH:mm',
        //     lang : 'es',
        //     pick12HourFormat: false
        // });
        // $('#TimeenHoraFin').bootstrapMaterialDatePicker({
        //     date: false,
        //     format: 'HH:mm',
        //     lang : 'es',
        //     pick12HourFormat: false
        // });

         


        $('.timepicker_day').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            lang : 'es'
        });
         
        $('.select').select2({
        });

        $("#TimeenAddTimeForm").bind("submit",function(){
        var btnEnviar = $(".btnEnviar");
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:$(this).serialize(),
            beforeSend: function(){
                btnEnviar.val("Enviando"); // Para input de tipo button
                btnEnviar.attr("disabled","disabled");
            },
            complete:function(data){
                btnEnviar.val("Enviar formulario");
                btnEnviar.removeAttr("disabled");
            },
            success: function(data){
                $(".respuesta").html(data);
                if(data =='1'){
                    loadContent();
                    $('#ModalLoad').modal('hide');
                    $("#TimeenAddTimeForm")[0].reset();
                   $(".respuesta").html('<div class="alert alert-success" role="alert">Tarea añadida correctamente.</div>');
                 }else if(data == "0"){
                  
                  $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
                 }else{
                    
                    $(".validationsRequest").html('<div class="alert alert-danger" role="alert">'+data+'</div>');
                 }
            },
            error: function(data){
               $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
            }
        });
        return false;
    });

        $("#TimeenEditForm").bind("submit",function(){
        var btnEnviar = $(".btnEnviar");
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:$(this).serialize(),
            beforeSend: function(){
                btnEnviar.val("Enviando"); // Para input de tipo button
                btnEnviar.attr("disabled","disabled");
            },
            complete:function(data){
                btnEnviar.val("Enviar formulario");
                btnEnviar.removeAttr("disabled");
            },
            success: function(data){
                $(".respuesta").html(data);
                if(data =='1'){
                    loadContent();
                    $('#ModalLoad').modal('hide');
                    $("#TimeenEditForm")[0].reset();
                   $(".respuesta").html('<div class="alert alert-success" role="alert">Tarea editada correctamente.</div>');
                 }else if(data == "0"){
                  
                  $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
                 }else{
                    
                    $(".validationsRequest").html('<div class="alert alert-danger" role="alert">'+data+'</div>');
                 }
            },
            error: function(data){
               $(".respuesta").html('<div class="alert alert-danger" role="alert">Revise su formulario e intente nuevamente.</div>');
            }
        });
        return false;
    });
    });
    
</script>
<style>
 .select2-container {
            border: solid 0.5px darkgrey;border-radius: 4px !important;
            z-index: 10000000 !important;
            }
</style>
 
