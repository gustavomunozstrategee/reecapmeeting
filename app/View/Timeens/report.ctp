<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.8.2/angular.min.js" integrity="sha512-7oYXeK0OxTFxndh0erL8FsjGvrl2VMDor6fVqzlLGfwOQQqTbYsGPv4ZZ15QHfSk80doyaM0ZJdvkyDcVO7KFA==" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<div class="container-fluid">
<div class="container-fluid">
     <?php echo $this->element('./Timeen/left_nav'); ?>
	<div class="col-xl-9">
		<div class="widget widget-07 has-shadow">
			<div class="col-md-12 addTimeDiv">
				<h4 class="d-flex justify-content-between align-items-center py-2 mb-4">
	              <div class="font-weight-bold"><?php echo __("Reportes") ?></div>
	            </h4>
			</div>

			<div class="row">
				
	            <div class="col-md-12">
	             	<div class="col-md-12">
	             		<form method="POST" action="" name="">
	             		<label><?php echo __("Rango de fechas") ?></label>
                    	<?php echo $this->Form->input('fechas', array('class' => 'form-control','id'=>"reporte","style"=>"margin-bottom: 10px","label"=>false)); ?>
                    	
                    	<?php echo $this->Form->label('Commitment.user_id',__('Usuarios'), array('class'=>''));?>
                        <?php echo $this->Form->input('user_id', array('class' => 'form-control selectpicker show-menu-arrow show-tick','label'=>false,'div'=>false,"multiple"=>"true","data-actions-box"=>"true","tabindex" =>"98")); ?>
                        <br><br>
                        <?php echo $this->Form->label('Commitment.project_id',__('Proyectos'), array('class'=>''));?>
                        <?php echo $this->Form->input('project_id', array('class' => 'form-control select2 show-menu-arrow show-tick','label'=>false,'div'=>false,"multiple"=>"true","data-actions-box"=>"true","tabindex" =>"98")); ?>


                        <div class="btn-group btn-group-sm" style="margin-top: 10px;float: right;">
	                        <button type="submit" class="tabledit-edit-button btn btn-lg btn-success td-actions">
	                        	<i class="la la-filter p-1 mr-0 text-white"></i> 
	                        	Filtrar 
		                    </button>
						</div>
						</form>
	             	</div>
	            </div>
	        	
			</div>


			<div class="col-md-12">
				</form>

                                        <form method="POST" action="generate_report" name="">
                                            <div class="row">
                                                <div class="col-md-12" >
                                                         
                                                        <div style="display: none;">
                                                         <?php echo $this->Form->input('user_id', array('class' => 'form-control selectpicker show-menu-arrow show-tick','label'=>false,'div'=>false,"multiple"=>"true","data-actions-box"=>"true","tabindex" =>"98","style"=>"display:none")); ?>
                                                        </div>


                                                        <div class="btn-group btn-group-sm" style="float: right; margin-top: 28px">
                                                            <button type="submit" class="tabledit-edit-button btn btn-lg btn-success td-actions">
                                                                
                                                                    <i class="la la-file-excel-o p-1 mr-0 text-white"></i> Exportar a Excel 
                                                                
                                                            </button>

                                                        </div>
                                                         

                                                    </div>
                                                
                                            </div>
                                        </form>
				<div class="respuesta"></div>
			</div>
			
			<div class="col-md-12 loadContent" >
			</div>
		</div>
	</div>
</div>
<div id="modalEdit" >
	<div class="modal fade" id="ModalLoad"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	</div>
</div>
<?php
    
    $this->Html->script('lib/select2/select2.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/bootstrap-tagsinput.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/dropzone.min.js', ['block' => 'AppScript']);
    $this->Html->script('lib/ckeditor/ckeditor.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('controller/contacs/actions.js',   ['block' => 'AppScript']);
    $this->Html->script('controller/contacs/actionsV2.js', ['block' => 'AppScript']); 
    $this->Html->script('controller/contacs/initChronometer.js',['block' => 'AppScript']); 
    $this->Html->script("differenceHours", ['block' => 'AppScript']);
    $this->Html->scriptBlock("EDIT.initElementAdd(); ", ['block' => 'AppScript']);
    echo $this->Html->css('frontend/admin/components/table.css'); 


     

?>
<?php
 echo $this->Html->script('materialDateTimePicker.js', ['block' => 'AppScript']);
?>
<script type="text/javascript">
    $( document ).ready(function() {
    	$('.select2').select2();
	    loadContent();
	    function loadContent(){
	      $.ajax({
	            type: "POST",
	            url: "../timeens/reportLoad",
	            data: {},
	            beforeSend: function(){
	            },
	            complete:function(data){
	            },
	            success: function(data){
	                $(".loadContent").html(data);
	            },
	            error: function(data){
	                alert("Error de carga");
	            }
	        });
	    }

	    $(".addTimeDiv").on("click", ".addTime", function(){
	    	var id = $(this).data("id");
	    	$.ajax({
	            type: "POST",
	            url: "timeens/add",
	            contentType: "application/json; charset=utf-8",
	            dataType: "html",
	            success: function (response) {
	                $('#ModalLoad').html(response); //add the partial view into the modal content.
	                $('#ModalLoad').modal('show'); //display the modal popup.
	            },
	            failure: function (response) {
	                alert(response.responseText);
	            },
	            error: function (response) {
	                alert(response.responseText);
	            }
	        });
	    });
	    $(".loadContent").on("click", ".editTime", function(){
	    	var id = $(this).data("id");
	    	$.ajax({
	            type: "GET",
	            url: "timeens/edit/" + id,
	            contentType: "application/json; charset=utf-8",
	            dataType: "html",
	            success: function (response) {
	                $('#ModalLoad').html(response); //add the partial view into the modal content.
	                $('#ModalLoad').modal('show'); //display the modal popup.
	            },
	            failure: function (response) {
	                alert(response.responseText);
	            },
	            error: function (response) {
	                alert(response.responseText);
	            }
	        });
	    });
	    

    });


    
</script>

<style type="text/css">
	.loadContent{
		margin-top: 40px;
	}
	.daterangepicker{
		z-index: 1000000 !important;
	}

</style>