 <?php
 setlocale(LC_TIME, 'es_ES.UTF-8');
 setlocale(LC_TIME, "spanish");
    function sumarHoras($horas = array()) {
        $total = 0;
        foreach($horas as $h) {
            $parts = explode(":", $h);
             
            $total += $parts[2] + $parts[1]*60 + $parts[0]*3600;       
        }
        return  str_replace(":", "h ", gmdate("H:i", $total))."m";
    }
?>
<?php if(!empty($timeens)): ?>
<div class="table-responsive">
<table class="table " style="">
    <thead>
        <th>Fecha</th>
        <th>Tiempo</th>
        <th>Descripcion</th>
        <th>Usuario</th>
        <th>Proyecto</th>
        <th>Duracion</th>
    </thead>
    <tbody >
    <?php foreach ($timeens as $key => $value):?>
        <?php
        

         ?>
        <?php $banderaFecha = 1; ?>
        <?php foreach ($value["informacion"] as $key2 => $value2): ?>
            <tr style="<?php if($banderaFecha == 1 &&  $key > 0) echo 'border-top: 1px solid' ?>" >
                <td class="text-report">
                  
                    <?php if($banderaFecha == 1): ?>
                         <b class="text-primary">
                             <?php
                                $banderaFecha = 0;
                                $fechaRegistro =  new DateTime($value["dia"]["fecha"]);
                                echo  $fechaRegistro->format("M");
                              ?>
                              <br>
                               <?php echo  $fechaRegistro->format("d"); ?>
                         </b>
                         
                    <?php endif ?>  
                    
                        
                    </td>
                <td class="text-report">
                    <?php 
                     $horaInicio = new DateTime($value2["Timeen"]["hora_inicio"]);
                     $horaTermino = new DateTime($value2["Timeen"]["hora_fin"]);

                    echo $horaInicio->format('H:i')." - ".$horaTermino->format('H:i') ?>
                        
                    </td>
                <td class="text-report"><?php echo $value2["Timeen"]["descripcion"] ?></td>
                <td class="text-report"><?php echo $value2["User"]["firstname"]." ".$value2["User"]["lastname"] ?></td>
                <td class="text-report"><?php echo $value2["Project"]["name"] ?> - <?php echo $value2["Project"]["Client"]["name"] ?></td>
                <td class="text-report">   
                    <?php
                        $horaInicio = new DateTime($value2["Timeen"]["hora_inicio"]);
                        $horaTermino = new DateTime($value2["Timeen"]["hora_fin"]);
                        $interval = $horaInicio->diff($horaTermino);
                        $horasACalcular[] = $interval->format('%H:%I:s');
                        $interval = $interval->format('%Hh %Im');
                        echo $interval;
                     ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php endforeach ?>
    </tbody>
</table>
</div>

<style type="text/css">
    .text-report{
        font-size: 12px;
        text-align: center;
        border-top: 1px solid;
    }
    .text-primary{
        color: #2C314D !important;
    }
</style>
<?php else: ?>
    <div class="alert alert-info" role="alert">No se ha encontrado información.</div>
<?php endif; ?>


