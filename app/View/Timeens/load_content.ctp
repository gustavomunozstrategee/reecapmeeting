 <?php
setlocale(LC_ALL,"es_ES");
 setlocale(LC_TIME, "spanish");
    function sumarHoras($horas = array()) {
        $total = 0;
        foreach($horas as $h) {
            $parts = explode(":", $h);
             
            $total += $parts[2] + $parts[1]*60 + $parts[0]*3600;       
        }
        return  str_replace(":", "h ", gmdate("H:i", $total))."m";
    }
?>
<?php if(!empty($timeens)): ?>
<?php foreach ($timeens as $key => $value):?>
    <div class="card pb-4 mb-2">
        <div class="row no-gutters align-items-center">
            <div class="col-12 col-md-12 px-4 pt-4">
                <a href="javascript:void(0)" class="text-body font-weight-semibold">
                    <?php  

                        $fecha = new DateTime($value["dia"]["fecha"]);
                        echo $this->Utilities->getMonthAndDay($fecha);
                        



                          //$fecha->format('l j \of F Y');

                    ?> - <b id="totalTiempo_<?php echo $key ?>">{totalTiempo}</b></a><br>
            </div>
                <?php $horasACalcular = array(); ?>
                <?php foreach ($value["informacion"] as $key2 => $value2): ?>
                <div class="col-12 col-md-12 px-4  ">
                    <hr>
                </div>
                <!-- <div> -->
                    <div class="col-4 col-md-4 text-muted small px-4  "> 
                      <strong><?php echo substr($value2["Timeen"]["hora_inicio"],0,-3) ?> — <?php echo substr($value2["Timeen"]["hora_fin"],0,-3) ?></strong> <br> 
                        <p id="calculoHora_<?php echo $value2['Timeen']['id'] ?>">
                            <?php
                            $horaInicio = new DateTime($value2["Timeen"]["hora_inicio"]);
                            $horaTermino = new DateTime($value2["Timeen"]["hora_fin"]);
                            $interval = $horaInicio->diff($horaTermino);
                            $horasACalcular[] = $interval->format('%H:%I:s');
                            $interval = $interval->format('%Hh %Im');
                            echo $interval;
                         ?>
                        <p>
                    </div>
                    <div class="col-4 col-md-6  small px-4 scroll">
                        <div class="col-md-12">
                            <strong><?php echo h($value2["Timeen"]["descripcion"]) ?></strong>
                        </div>
                        <div class="col-md-12">
                             <?php echo $value2["Project"]["name"] ?> - Strategee
                        </div>
                    </div>
                <!-- </div> -->
                
                <div class="col-4 col-md-2  small px-4  ">
                    <div class="col-md-12">
                        <div class="text-danger float-center edited_label_timeen_task_mobile">
                            <?php if($value2["Timeen"]["edited"] == 1): ?>
                                Editado
                            <?php endif ?>
                        </div>
                        <div class="media-right align-self-center">
                            <div class="widget-options">
                                <div class="dropdown">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="dropdown-toggle">
                                        <i class="la la-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start" >
                                        <a data-id='<?php echo $value2["Timeen"]["id"] ?>'   href="javascript:void(0)" class="dropdown-item editTime"> 
                                            <i class="la la-bell"></i> Editar
                                        </a>
                                        <a data-id='<?php echo $value2["Timeen"]["id"] ?>'   href="javascript:void(0)"   href="#" class="dropdown-item removeTime"> 
                                            <i class="la la-remove"></i> Eliminar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
                <script type="text/javascript">
                    $( document ).ready(function() {
                        $("#totalTiempo_<?php echo $key ?>").html('Registrado <?php echo sumarHoras($horasACalcular); ?>')
                     });
                </script>
              </div>
            </div>
        <?php endforeach ?>
    <div style="margin-top: 60px"></div>
<?php else: ?>
    <div class="alert alert-info" role="alert">No se ha encontrado tiempo.</div>
<?php endif; ?>


<?php if($this->request->is('mobile')) { ?>
<style type="text/css">
    div.scroll {
        margin: 4px, 4px;
        padding: 4px;
        width: 300px;
        overflow-x: auto;
        overflow-y: hidden;
        white-space: nowrap;
    }

    .edited_label_timeen_task_mobile{
        width: 100px;
    }
</style>
<?php } ?>