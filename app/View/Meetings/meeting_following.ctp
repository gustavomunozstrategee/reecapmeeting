<?php $data = $this->Session->read('Dates'); ?> 
<?php if(!empty($data)):?>
    <div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table ">
        <thead>
            <tr>
                <th><?php echo __('Fecha inicio') ?></th>  
                <th><?php echo __('Fecha fin') ?></th>  
                <th><?php echo __('Modalidad') ?></th>  
                <th class="text-center"><?php echo __('') ?></th>  
            </tr>
        </thead>
        <tbody>
        <?php if(!empty($data)):?>
            <?php foreach ($data as $dateId => $dates): ?>
                <tr>
                    <td><?php echo h($this->Time->format('d-m-Y h:i A', $dates['start_date'])); ?></td> 
                    <td><?php echo h($this->Time->format('d-m-Y h:i A', $dates['end_date'])); ?></td> 
                    <td><?php echo h($dates['meeting_name']); ?></td>
    		     	<td class="td-actions text-center ">

                         <a href="#" data-toggle="tooltip" data-placement="right"  class="btn-add-topic-meeting-following" rel="tooltip" data-id="<?php echo $dateId ?>" title="<?php echo __('Nuevo ítem'); ?>"><i class="la la-plus-circle edit add "></i></a>

                        <a href="#" data-toggle="tooltip" rel="tooltip" class="btn-view-topic-meeting-following" data-id="<?php echo $dateId ?>" title="<?php echo __('Ver temas específicos'); ?>"><i class="la la-eye edit "></i></a>

                        <a href="<?php echo $this->Html->url(array("controller" => "meetings","action"=>"delete_following_item"),true); ?>" class="btn-delete-topic-meeting-following" data-toggle="tooltip"  rel="tooltip" data-id="<?php echo $dateId ?>" title="<?php echo __('Eliminar'); ?>">
                            <i class="la la-remove delete "></i>
                        </a>

    				   											     		
    		     	</td>  
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td class="text-center" colspan="14"><?php echo __("No hay fechas de reuniones de seguimiento añadidas recientemente");?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
<?php endif; ?>