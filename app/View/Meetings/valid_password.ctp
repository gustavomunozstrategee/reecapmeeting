    <div class="section-table">
	    <h1 class="f-title"><?php echo __('Ingresa la contraseña de la reunión'); ?></h1>
        <p><?php echo __('Debes ingresar la contraseña correspondiente a la reunión.'); ?> </p>
        <?php echo $this->Form->create('Meeting', array('role' => 'form')); ?> 
                    <div class="input-group">
                        <?php echo $this->Form->input('password',array('class' => 'form-control border-input','type'=>'password','placeholder' => __('Contraseña'),'label'=>false,'div'=>false,'maxLength' => 4));?>
                        <div class="input-group-btn">
                            <button class='btn btn-cuadrado'><?php echo __('Enviar contraseña') ?></button>
                        </div>
                    </div>
        <?php echo $this->Form->end(); ?>
    </div>
