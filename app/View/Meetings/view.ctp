 
<?php $permissionContac  =  $this->Utilities->check_team_permission_action(array("index2"), array("contacs")); ?>
<?php $permissionMeeting =  $this->Utilities->check_team_permission_action(array("add"), array("meetings"), $meeting['Meeting']['team_id']); ?>


<!-- Begin Page Header-->
<div class="row">
  <div class="page-header">
    <div class="d-flex align-items-center">
      <h2 class="page-header-title"><?php echo __("Reuniones") ?></h2>
      <div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?php echo Router::url("/",true) ?>">
              <i class="ti ti-home"></i>
            </a>
          </li>
            <li class="breadcrumb-item">
              <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><?php echo __("Reuniones") ?></a>
            </li>

          <li class="breadcrumb-item active"><?php echo __('Detalle de la reunión'); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- End Page Header -->
<div class="row flex-row">
    <div class="col-md-8 col-sm-6 col-lg-8">
        <!-- Form -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?php echo __('Detalle de la reunión'); ?></h2>
                <div class="widget-options">
                    <div class="" role="">
                        <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right"  title="<?php echo __('Ver reuniones'); ?>" href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                            <i class="la la-list la-2x"></i>
                        </a> 
                        <?php if(isset($permissionMeeting["add"])):?> 
                            <?php if($meeting['Meeting']['contac_id'] == NULL): ?>
                                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right"  title="<?php echo __('Editar reunión'); ?>" href="<?php echo $this->Html->url(array('action' => 'edit',EncryptDecrypt::encrypt($meeting['Meeting']['id']))); ?>">
                                    <i class="la la-pencil la-2x"></i>
                                </a> 
                            <?php endif; ?>
                            <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right"  title="<?php echo __('Agendar reunión'); ?>" href="<?php echo $this->Html->url(array('action' => 'add','controller' => 'meetings'));?>">
                                <i class="la la-plus la-2x"></i>
                            </a> 
                        <?php endif; ?>
                        <?php if(date("Y-m-d H:i:s") >= $meeting["Meeting"]["start_date"] && $meeting["Meeting"]["email_type"] == null && $meeting["Meeting"]["contac_id"] == null): ?>
                            <?php if(isset($permissionContac["index"])):?> 
                                <a class="btn-circle-icon btn-save-meetings-to-contac" data-id="<?php echo EncryptDecrypt::encrypt($meeting['Meeting']['id'])?>" data-toggle="tooltip" data-placement="right"  title="<?php echo __('Iniciar reunión'); ?>" href="<?php echo $this->Html->url(array("controller" => "contacs",'action'=>'add'));?>">
                                    <i class="ti ti-clipboard la-2x"></i>
                                </a> 
                            <?php endif; ?>
                        <?php endif; ?>     
                    </div>
                </div>
            </div>
            <div class="widget-body px-5 row">
                <div class="form-horizontal w-100">
                    <?php if(!empty($meeting["Meeting"]) && !empty($meeting["User"])): ?>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Creada por'); ?>
                            </label>
                            <div class="col-lg-9">
                                <?php echo h($meeting['User']['firstname'] . ' ' . $meeting['User']['lastname']); ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Fecha y hora de inicio'); ?>
                            </label>
                            <div class="col-lg-9">
                                <?php echo h($this->Time->format('d-m-Y h:i A', $meeting['Meeting']['start_date'])); ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Fecha y hora de fin'); ?>
                            </label>
                            <div class="col-lg-9">
                                <?php echo h($this->Time->format('d-m-Y h:i A', $meeting['Meeting']['end_date'])); ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Lugar'); ?>
                            </label>
                            <div class="col-lg-9">
                                <?php echo h(!empty($meeting['Meeting']['location']) ? $meeting['Meeting']['location'] : __("No disponible")); ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Asunto'); ?>
                            </label>
                            <div class="col-lg-9">
                                <?php echo $this->Text->truncate(h($meeting["Meeting"]['subject']),255); ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Empresa'); ?>
                            </label>
                            <div class="col-lg-9">
                                <?php echo h($meeting["Team"]['name']); ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Cliente'); ?>
                            </label>
                            <div class="col-lg-9">
                                <?php echo h($meeting["Client"]['name']); ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Descripción'); ?>
                            </label>
                            <div class="col-lg-9 div-word-wrap">
                                <?php echo h($meeting["Meeting"]['description']); ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <div class="col-lg-12 div-word-wrap">
                                <p class="text-center"><?php echo __('No exite información.')?></td>
                            </div>
                        </div> 
                    <?php endif; ?>
                </div>
            </div>
        </div>        
    </div>
    <div class="col-md-4 col-sm-6 col-lg-4">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h4 class="font-weight-bolder form-control-label">
                    <?php echo __("Asistentes")?>
                </h4>
            </div>
            <div class="widget-body px-3">
                <?php if (!empty($meeting["Assistant"])): ?>                
                    <ul class="list-group w-100">
                        <?php $Assistant = Set::combine($meeting["Assistant"], '{n}.email', '{n}');?>
                        <?php foreach ($Assistant as $dateId => $userInvited): ?>
                            <li class="list-group-item">
                                <div class="other-message">
                                    <div class="media">
                                        <div class="media-left align-self-center mr-3">
                                            <img class="avatar rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$userInvited['image']); ?>" alt="..." class="img-fluid rounded-circle" style="width: 50px;height: 50px">
                                        </div>
                                        <div class="media-body align-self-center">
                                            <div class="other-message-sender"><?php echo h($userInvited['name']); ?> </div>
                                            <div class="other-message-time"><?php echo h($userInvited['email']); ?></div>
                                            <!-- 
                                                <div class="other-message-time">
                                                    <small class="text-info">
                                                        <?php echo h($userInvited['client']); ?>         
                                                    </small>
                                                </div> 
                                            -->
                                        </div>
                                        <div class="media-right align-self-center">
                                            
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                <?php else: ?>
                    <div class="form-horizontal w-100">
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <div class="col-lg-12 div-word-wrap">
                                <p class="text-center">
                                    <?php echo __('No se definieron asistentes para la reunión.')?>
                                </p>
                            </div>
                        </div> 
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-6 col-lg-8">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h4 class="font-weight-bolder form-control-label">
                    <?php echo __("Temas específicos")?>
                </h4>
            </div>
            <div class="widget-body px-3">
                <?php if (!empty($meeting["Topic"])): ?>

                    <div class="form-horizontal w-100 px-3">
                        <div class="form-group row d-flex align-items-center border-bottom">
                            
                            <label class="col-lg-9 form-control-label">
                                <?php echo __('Descripción'); ?>
                            </label>
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Tiempo'); ?>
                            </label>
                        </div>
                        <?php foreach ($meeting["Topic"] as $topic): ?>
                            <div class="form-group row d-flex align-items-center border-bottom">
                                <div class="col-lg-9 div-word-wrap">
                                    <?php echo h($topic['description']); ?>
                                </div>
                                <div class="col-lg-3 div-word-wrap">
                                    <?php echo $this->Utilities->conversorSegundosHoras($topic['time']); ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                <?php else: ?>
                    <div class="form-horizontal w-100">
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <div class="col-lg-12 div-word-wrap">
                                <p class="text-center">
                                    <?php echo __('No se definieron temas específicos para la reunión.')?>
                                </p>
                            </div>
                        </div> 
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-lg-4">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h4 class="font-weight-bolder form-control-label">
                    <?php echo __("Tags")?>
                </h4>
            </div>
            <div class="widget-body px-3">
                <?php if (!empty($meeting["Tag"])): ?>

                    <div class="form-horizontal w-100 px-3">
                        <div class="form-group row d-flex align-items-center border-bottom">
                            
                            <label class="col-lg-9 form-control-label">
                                <?php echo __('Descripción'); ?>
                            </label>
                            <label class="col-lg-3 form-control-label">
                                <?php echo __('Tiempo'); ?>
                            </label>
                        </div>
                        <?php foreach ($meeting["Tag"] as $tag): ?>
                            <span class="btn btn-outline-primary text-uppercase m-1"  style="cursor: none;">
                                <?php echo h($tag['name']); ?>
                            </span>
                        <?php endforeach ?>
                    </div>
                <?php else: ?>
                    <div class="form-horizontal w-100">
                        <div class="form-group row d-flex align-items-center border-bottom">
                            <div class="col-lg-12 div-word-wrap">
                                <p class="text-center">
                                    <?php echo __('No se definieron tags para la reunión.')?>
                                </p>
                            </div>
                        </div> 
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<?php
    $this->Html->script('controller/meetings/index.js', ['block' => 'AppScript']);
?>
 
