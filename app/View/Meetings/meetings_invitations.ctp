<?php echo $this->Html->css('lib/fullcalendar.css', ['block' => 'AppCss']); ?>
<?php $this->Html->css("frontend/admin/components/calendar-editado.css", array("block" => "styles_section")); ?> 
<?php $permission = $this->Utilities->check_team_permission_action(array("add"), array("meetings")); ?>
<div class="backg-white">
    <div class="flex-space-between-start">
        <div>
            <h1 class="f-title"><?php echo __('Reuniones creadas'); ?></h1>
            <?php if(!empty($permission["add"])): ?>  
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right"  title="<?php echo __("Agendar reunión") ?>" href="<?php echo $this->Html->url(array('action'=>'add'));?>">
                    <i class="flaticon-add"></i>
                </a> 
            <?php endif;?>
            <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Ver reuniones") ?>" href="<?php echo $this->Html->url(array('controller'=>'meetings','action'=>'index'));?>" class="tabs-animation <?php echo $this->params->params['controller'] == 'meetings' ? 'active' : '' ?>">
                <i class="flaticon-list"></i>
            </a> 
        </div>

        <!-- Buscadores -->
    <div class="f-datapicker mt-20"> 
        <?php echo $this->Form->create('Search',array('url'=>array('controller'=>$this->request->controller),'class'=>'form-inline', 'type'=>'GET', 'role'=>'form'));?>
        <div class="form-group">
            <div class='input-group date' id='datetimepickerStart'>
                <?php echo $this->Form->input('q', array('class' => 'form-control border-input', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true, 'value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '')); ?>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>

        <div class="form-group f-input-select-white">
            <?php echo $this->Form->label('Client.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
            <?php if(!empty($this->request->query["teams"])):?>
                <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams','multiple' => true, 'value' => $this->request->query["teams"])); ?>
            <?php else: ?>
                <?php echo $this->Form->input('teams', array('class' => 'form-control border-input select-teams','label'=>false,'div'=>false, 'type' => 'select','options' => $teams, 'id' => 'teams','multiple' => true)); ?>
            <?php endif; ?>
        </div>
         
        <button type="submit" class="btn btn-blue" id="search_team"><?php echo __('Buscar');?></button>
        <?php if(empty($meetings) && !empty($this->request->query['q'])) : ?>
            <p class="mxy-10"><?php echo __('No se encontraron datos.') ?> </p>
            <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'), array('class' => 'f-link-search')); ?>
        <?php endif ?>
        <?php echo $this->Form->end(); ?>
    </div>

    </div>


     
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="bg-filter-main mt-2em">
                <div class="header-cardmeeting">
                    <h4 class="bg-filter-subtitle f-blue"><?php echo __("Listado de reuniones")?></h4>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="tab tabs-main addActive">
                            <a href="<?php echo $this->Html->url(array('controller'=>'meetings','action'=>'index'));?>" class="tabs-animation">
                                <span class="<?php echo $this->params->params['controller'] == 'meetings' && $this->params->params['action'] == 'index' ? 'active' : '' ?>"><?php echo __("Creadas por mí")?></span>
                            </a>
                            <a href="<?php echo $this->Html->url(array('controller'=>'meetings','action'=>'meetings_invitations'));?>" class="tabs-animation <?php echo $this->params->params['controller'] == 'meetings' && $this->params->params['action'] == 'meetings_invitations' ? 'active' : '' ?>">
                                <span><?php echo __("Invitaciones recibidas")?></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body table-responsive no-padding">
                                <div class="content-list-meetings">
                                    <!-- <div> -->
                                        <?php if(!empty($meetings)):?>
                                        <?php foreach ($meetings as $meeting): ?>
                                        <div class="card-meeting">
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Tipo reunión'); ?></label></div>
                                                <div class="col-50"><?php echo h(!empty($meeting['Meeting']['meeting_type']) ? __($meeting['Meeting']['meeting_type']) : __("No disponible")); ?></div>
                                            </div>
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Asunto'); ?></label></div>
                                                <div class="col-50 div-word-wrap"><?php echo $this->Text->truncate(h($meeting['Meeting']['subject']),150); ?></div>
                                            </div>
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Descripción'); ?></label></div>
                                                <div class="col-50 div-word-wrap"><?php echo $this->Text->truncate(h($meeting['Meeting']['description']),150); ?></div>
                                            </div>
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Ubicación - dirección'); ?></label></div>
                                                <div class="col-50 div-word-wrap"><?php echo $this->Text->truncate(h(!empty($meeting['Meeting']['location']) ? $meeting['Meeting']['location'] : __("No disponible")),150); ?></div>
                                            </div>  
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Modalidad') ?></label></div>
                                                <div class="col-50"><?php echo $this->Utilities->showStateModalityMeeting($meeting['Meeting']['meeting_room']); ?></div>
                                            </div>
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Empresa'); ?></label></div>
                                                <div class="col-50 div-word-wrap"><?php echo $this->Text->truncate(h($meeting['Team']['name']),150); ?></div>
                                            </div>
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Fecha y hora de inicio'); ?></label></div>
                                                <div class="col-50"><?php echo h($this->Time->format('d-m-Y h:i A', $meeting['Meeting']['start_date'])); ?></div>
                                            </div>
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Fecha y hora de fin'); ?></label></div>
                                                <div class="col-50"><?php echo h($this->Time->format('d-m-Y h:i A', $meeting['Meeting']['end_date'])); ?></div>
                                            </div>
                                            <div class="row-list-meeting">
                                                <div class="col-50"><label class="f-blue"><?php echo __('Privada - Pública'); ?></label></div>
                                                <div class="col-50"><?php echo $this->Utilities->meetingPrivate($meeting['Meeting']['private']); ?></div>
                                            </div>

                                            <div class="card-meeting-actions">
                                                <?php $permissionTeam = $this->Utilities->check_team_permission_action(array("add","view"), array("meetings"), $meeting['Team']['id']); ?>

                                                <?php if($meeting["Meeting"]["email_type"] == null || $meeting["Meeting"]["email_type"] == ""):?>

                                                    <?php if(isset($permissionTeam["add"])):?>
                                                        <?php if($meeting['Meeting']['contac_id'] == NULL): ?>
                                                            <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'edit',EncryptDecrypt::encrypt($meeting['Meeting']['id']))); ?>" title="<?php echo __('Editar reunión'); ?>" class="flex-auto text-center" data-toggle="tooltip">
                                                                <i class="fa fa-pencil f-white"></i>
                                                            </a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>

                                                    <?php if(isset($permissionTeam["view"])): ?>
                                                        <a rel="tooltip" data-id="<?php echo EncryptDecrypt::encrypt($meeting['Meeting']['id'])?>" title="<?php echo __('Ver detalle de la reunión'); ?>" class="btn-view-assistants-meeting flex-auto text-center f-cursor-pointer" data-toggle="tooltip">
                                                            <i class="fa fa-eye f-white"></i>
                                                        </a>
                                                    <?php endif; ?>

                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                        <div>
                                            <div class="text-center mxy-10" colspan="14"><?php echo __('No existen reuniones.')?></div>
                                        </div>
                                        <?php endif; ?>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="div-pagination"> 
    					  <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
    					   <div>
    					   <ul class="pagination f-paginationrecapmeeting">
    					   <?php echo $this->Paginator->prev('< ' /*. __('Anterior')*/, array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                 echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                 echo $this->Paginator->next(/*__('Siguiente') .*/ ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?> 
    						</ul>    
    					   </div>
    					</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Calendario -->
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="bg-filter-main  mt-2em">
            <div class="header-cardmeeting">
                    <div class="flex-space-between line-height-40">
                    <h4 class="bg-filter-subtitle-calendario f-blue"><?php echo __('Calendario de reuniones'); ?></h4>
                        <?php if(!empty($permission["add"])): ?> 
                            <div class="bg-white">
                                <a  class="btn-sync-calendar display-inline-block f-cursor-pointer btn-email" data-toggle="tooltip" data-placement="top" title="<?php echo __("Sincronizar con Gmail")?>">
                                  <?php echo $this->Html->image("frontend/website/home/gmail.svg",array('class' => '', 'alt' => 'Logo de Gmail'));?>
                                </a>
                                <a class="btn-email display-inline-block" data-toggle="tooltip" data-placement="top" title="<?php echo __("Sincronizar con Outlook")?>"  href="<?php echo oAuthService::getLoginUrl(configure::read("AUTH_OUTLOOK_URL_EVENTS"))?>">
                                    <?php echo $this->Html->image("frontend/website/home/outlook.svg",array('class' => '', 'alt' => 'Logo de Outlook'));?>
                                </a>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
                <div id="my-calendar"></div>
            </div>
        </div>
    </div>

    <div id="calendarModal" class="modal fade">
        <div class="modal-dialog modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
                    <h4 class="modal-title"><?php echo __("Reuniones en esta fecha")?></h4>
                </div>
                <div id="modalBody" class="modal-body">
                    <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover" id="content-meetings">
                        <thead class="text-primary">
                            <tr>
                                <th><?php echo __('Asunto'); ?></th>
                                <th><?php echo __('Lugar'); ?></th>
                                <th><?php echo __('Detalle'); ?></th>
                            </tr>
                        </thead>
                        <tbody id="show-content-meetings-calendar"></tbody>
                    </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __("Cerrar")?></button>
                </div>
            </div>
        </div>
    </div>
    <div id="calendarModalDetailMeeting" class="modal fade">
        <div class="modal-dialog modal-lg modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
                    <h4 class="modal-title"><?php echo __("Detalle de la reunión")?></h4>
                </div>
                <div id="modalBody" class="modal-body">
                    <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover" id="content-meetings">
                        <thead class="text-primary">
                            <tr>
                                <th><?php echo __('Tipo de reunión'); ?></th>
                                <th><?php echo __('Asunto'); ?></th>
                                <th><?php echo __('Fecha y hora de inicio'); ?></th>
                                <th><?php echo __('Fecha y hora de fin'); ?></th>
                                <th><?php echo __('Lugar'); ?></th>
                                <th><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody id="show-content-detail-meeting"></tbody>
                    </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __("Cerrar")?></button>
                </div>
            </div>
        </div>
    </div>
    <div id="calendarModalDetailMeetingDay" class="modal fade">
        <div class="modal-dialog modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
                    <h4 class="modal-title"><?php echo __("Detalle de la reunión")?></h4>
                </div>
                <div id="modalBody" class="modal-body">
                    <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover" id="content-meetings">
                        <thead class="text-primary">
                            <tr>
                                <th><?php echo __('Tipo de reunión'); ?></th>
                                <th><?php echo __('Asunto'); ?></th>
                                <th><?php echo __('Fecha y hora de inicio'); ?></th>
                                <th><?php echo __('Fecha y hora de fin'); ?></th>
                                <th><?php echo __('Lugar'); ?></th>
                            </tr>
                        </thead>
                        <tbody id="show-content-detail-meeting-days"></tbody>
                    </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __("Cerrar")?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $this->Html->script('lib/fullcalendar-3.8.2/moment.min.js', 										['block' => 'AppScript']);
	$this->Html->script('lib/fullcalendar-3.8.2/fullcalendar.js', 										['block' => 'AppScript']);
	$this->Html->script('lib/fullcalendar-3.8.2/gcal.js', 												['block' => 'AppScript']);
	$this->Html->script('lib/datepicker/bootstrap-datepicker.js',										['block' => 'AppScript']);
	$this->Html->script('controller/meetings/index.js', 												['block' => 'AppScript']);
	if ($this->Session->read('Config.language') == 'esp') {
		$this->Html->script('lib/datepicker/locales/bootstrap-datepicker.es.js',						['block' => 'AppScript']);
		$this->Html->script('lib/fullcalendar-3.8.2/locale/es.js', 										['block' => 'AppScript']);
	}
?>