<?php echo $this->Html->css('lib/timingfield.css',				['block' => 'AppCss']); ?>
<?php echo $this->Html->css('lib/jquery.tag-editor.css',		['block' => 'AppCss']); ?>
<?php $permission = $this->Utilities->check_team_permission_action(array("add"), array("meetings")); ?>

 <div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><?php echo __('Reuniones'); ?></h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>"><i class="ti ti-home"></i></a> </li>
                    <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>meetings"><?php echo __('Reuniones'); ?></a></li>
                    <li class="breadcrumb-item active"><?php echo __('Editar reunión'); ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row flex-row">
    <div class="col-xl-12">
        <!-- Begin Widget 07 -->
        <div class="widget widget-07 has-shadow">
            <!-- Begin Widget Header -->
            <div class="widget-header bordered d-flex align-items-center">
                <h2><?php echo __('Editar reunión'); ?></h2>
                <div class="widget-options">
                    <div class="" role="">
                        <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><button type="button" class="btn btn-secondary "><i class="ti ti-calendar"></i><?php echo __('Ver reuniones'); ?></button></a>
                    </div>
                </div>
            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('Meeting', array('role' => 'form','data-parsley-validate','novalidate' => true)); ?>
                    <div class="row">
                        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                            <div class="row">

                                <div class="col-md-12">
                                    <p class="text-warning"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>
                                </div>
                                
                                <div class="col-md-6 col-lg-6 col-sm-12">
                                    <div class='form-group'>
                                        <?php echo $this->Form->input('id', array("value" => EncryptDecrypt::encrypt($this->request->data["Meeting"]["id"])))?>
                                        <?php echo $this->Form->label('Meeting.meeting_type',__('Tipo de reunión'), array('class'=>'control-label f-blue required'));?>
                                        <?php if($this->Session->read("Config.language") == "esp"):?>
                                            <?php echo $this->Form->input('meeting_type', array('class' => 'form-control border-input meeting-type','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("TYPE_MEETING"))); ?>
                                        <?php else: ?>
                                            <?php echo $this->Form->input('meeting_type', array('class' => 'form-control border-input meeting-type','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("TYPE_MEETING_ENG"))); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12">
                                    <div class='form-group'>
                                        <?php echo $this->Form->label('Meeting.meeting_room',__('Modalidad de la reunión'), array('class'=>'control-label f-blue required'));?>
                                        <?php if($this->Session->read("Config.language") == "esp"):?>
                                            <?php echo $this->Form->input('meeting_room', array('class' => 'form-control border-input select-location','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("MODALITY_MEETING"), 'empty' => __("Seleccione una opción"))); ?>
                                        <?php else: ?>
                                            <?php echo $this->Form->input('meeting_room', array('class' => 'form-control border-input select-location','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("MODALITY_MEETING_ENG"), 'empty' => __("Seleccione una opción"))); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class='col-md-12 col-lg-12' style="display: none;" id="location-meeting">
                                    <div class='form-group'>
                                        <?php echo $this->Form->label('Meeting.location',__('Ubicación - dirección'), array('class'=>'control-label f-blue required'));?>
                                        <?php echo $this->Form->input('location', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => "","required"=>"false")); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    <div class='form-group'>
                                        <?php echo $this->Form->label('Meeting.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
                                        <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Empresa'),'type' => 'select','options' => $teams)); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    <div class='form-group'>
                                        <div id="show_content_client">
                                            <?php echo $this->Form->label('Meeting.client_id',__('Cliente'), array('class'=>'control-label f-blue required'));?>
                                            <?php echo $this->Form->input('client_id', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Cliente'), 'options' => $clients,'type' => 'select')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-12 col-lg-12'>
                                    <div class='form-group'>
                                        <?php echo $this->Form->label('Meeting.subject',__('Asunto'), array('class'=>'control-label f-blue required'));?>
                                        <?php echo $this->Form->input('subject', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Asunto'))); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <?php echo $this->Form->label('Meeting.start_date',__('Fecha y hora del inicio de la reunión'), array('class'=>'control-label f-blue'));?>
                                        <div class='input-group date' id='datetimepickerStart'>
                                            <?php echo $this->Form->input('start_date', array('class' => 'form-control ', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <?php echo $this->Form->label('Meeting.end_date',__('Fecha y hora fin de la reunión'), array('class'=>'control-label f-blue'));?>
                                        <div class='input-group date' id='datetimepickerEnd'>
                                            <?php echo $this->Form->input('end_date', array('class' => 'form-control ', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <?php echo $this->Form->label('Meeting.description',__('Descripción'), array('class'=>'control-label f-blue required'));?>
                                        <?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none','label'=>false,'div'=>false,'placeholder' => __('Descripción'),'maxLength' => 300)); ?>
                                    </div>
                                    <div id="showCharacter"></div>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h2><?php echo __('Agenda de la reunión'); ?></h2>
                                        </div>
                                        <div class="widget-body">
                                            <div class="table-responsive">
                                                <table class="table mb-0 border" id="grade-table">
                                                    <thead>
                                                        <tr style="text-align: center; ">
                                                            <th><?php echo __('Descripción') ?></th>
                                                            <th><?php echo __('Tiempo') ?></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if (!empty($this->request->data["Topic"])): ?>
                                                            <?php foreach ($this->request->data["Topic"] as $idField => $topic): ?>
                                                                <?php echo $this->element('topics_edit', array('key' => $idField, 'data' => $topic));?>
                                                            <?php endforeach ?>
                                                        <?php endif ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td class="actions">
                                                                <a rel="tooltip" title="<?php echo __('Nuevo ítem'); ?>" class="btn btn-success btn-xs add">
                                                                    <i class="la la-plus text-white"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <script id="grade-template" type="text/x-underscore-template">
                                                    <?php echo $this->element('topics_edit');?>
                                                </script>

                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 p-md-0">
                            <div class="widget mb-0">
                                <div class="widget-header">
                                    <h2><?php echo __('Añadir invitados');?></h2>
                                </div>
                                <div class="widget-body">
                                    <div class='form-group'>
                                        <?php echo $this->Form->input('invited', array('class' => 'form-control border-input invited-users','label'=>false,'placeholder' => __('Añadir invitados con el nombre o correo'),'id' => 'invited')); ?>
                                        <div id="suggestions"></div>
                                    </div>
                                    <p class="text-danger text-center loadEditAssistants" style="display:none"><?php echo __('Espera...'); ?></p>
                                    <div id="show-content-inviteds" class="scroll-inviteds">
                                        <p class="text-warninf text-center errorP"><?php echo __('Cargando asistentes...'); ?></p>
                                            <?php if(!empty($assistantsSession)):?>
                                            <div>
                                                <?php foreach ($assistantsSession as $assistant): ?>
                                                <div class="flex-inviteds">
                                                <?php if($assistant['state'] != configure::read("DELETE_ASSISTANT")):?>
                                                    <div class="col-90">
                                                        <p><?php echo h($assistant['name']); ?></p>
                                                        <p><?php echo h($assistant["email"]); ?></p>
                                                    </div>
                                                    <div class="col-10"> 
                                                        <a href="#" data-id="<?php echo EncryptDecrypt::encrypt($assistant['user_id']) ?>" class="btn-xs btn-icon-xs delete-assistant-edit-meeting">
                                                            <i class="fa fa-times"></i>
                                                        </a> 
                                                    </div>
                                                <?php endif; ?>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <?php endif; ?>
                                        
                                    </div>
                                    <p class="f-blue"><?php echo __("Para añadir asistentes externos primero debes escoger una empresa y un cliente.")?></p>

                                </div>
                            </div>
                            <div class="widget mb-0">
                                <div class="widget-header">
                                    <h2><?php echo __('Tags');?></h2>
                                </div>
                                <div class="widget-body">
                                    <div class='form-group'>
                                        <?php echo $this->Form->input('Meeting.tag', array('class' => 'form-control border-input tagEdit','label'=>false,'div'=>false,'placeholder' => 'Tag','id' => 'tag')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="widget">
                                <div class="widget-header">
                                    <h2><?php echo __('Privacidad');?></h2>
                                </div>
                                <div class="widget-body">
                                    <div class="radio">
                                        <label>
                                          <input type="radio" name="data[Meeting][private]" id="public-meeting" class="public-meeting" value="1" <?php echo (@$this->request->data['Meeting']['private'] == 1 ? 'checked' : 'checked');?>><?php echo __("Reunión pública")?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                       <label>
                                         <input type="radio" name="data[Meeting][private]" id="password-meeting" class="password-meeting" value="2" <?php echo (@$this->request->data['Meeting']['private'] == 2 ? 'checked' : '');?>><?php echo __("Proteger con contraseña")?>
                                        </label>
                                    </div>

                                    <div id="content-password" style="display: none;">

                                        <div>
                                            <div class="input-group">
                                                <?php echo $this->Form->input('password', array('class' => 'form-control border-input','label'=>false,'div'=>false,"required"=>false,'placeholder' => __('Contraseña'), 'id' => 'pass-meeting','value' => @$this->request->data['Meeting']['password'], "maxlength" => 4, 'autocomplete' => 'new-password')); ?>
                                                
                                                    
                                                    <span class="input-group-btn generate-password">
                                                        <button type="button" class="btn btn-info">
                                                            <i class="la la-refresh"></i>
                                                        </button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <button type="button" class="btn btn-info show-password link-show-password"><i class="ion ion-eye"></i><?php echo __("Mostrar contraseña")?></button>
                                        <button type="button" class="btn btn-danger hide-password link-show-password" style="display: none;"><i class="ion ion-eye-disabled"></i><?php echo __("Ocultar contraseña")?></button>
                                    </div>
                                    
                                    <?php if(isset($permission["add"])):?>
                                        <?php if($meeting["Meeting"]["created_from"] == "" || $meeting["Meeting"]["created_from"] == null): ?>
                                            <div id="meeting-normal" class="mt-5 text-center">
                                                <button type='submit' class='btn btn-primary my-10'><?php echo __("Guardar")?></button>
                                            </div>
                                        <?php else: ?>
                                            <div id="meeting-normal-auth" class=" mt-5 text-center">
                                                <button type='button' class='btn btn-primary my-10 btn-auth-meeting-edit'><?php echo __("Guardar")?></button>
                                            </div>
                                        <?php endif; ?>
                                     <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>                
            </div>
        </div>
    </div>
</div>

<div class="backg-white">

    <div id="AutenticationEmailMeetingEdit" class="modal fade">
        <div class="modal-dialog modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
                    <h4 class="modal-title"><?php echo __("Seleccionar modo de autenticación.")?></h4>
                </div>
                <div class="modal-body">
                    <p><?php echo __("Para terminar de editar la reunión deberas autenticarte con tu cuenta, ten en cuenta que debe ser con la cuenta con la que registraste la reunión al principio.")?></p>
                </div>
                <div class="modal-footer">
                    <?php if($meeting["Meeting"]["created_from"] == "gmail"): ?>
                    <a class="btn-auth-gmail-edit-meeting f-cursor-pointer" data-toggle="tooltip" data-placement="right" title="<?php echo __("Sincronizar con Gmail") ?>">
                        <?php echo $this->Html->image("frontend/website/home/gmail.svg",array('class' => 'icon-email', 'alt' => 'Logo de Gmail'));?>
                    </a> 
                    <?php endif; ?>
                    <?php if($meeting["Meeting"]["created_from"] == "outlook"): ?>
                        <a data-toggle="tooltip" data-placement="right" title="<?php echo __("Sincronizar con Outlook") ?>" class="btn-outlook f-cursor-pointer" href="<?php echo oAuthService::getLoginUrl(configure::read("AUTH_OUTLOOK_URL_EDIT_EVENTS"))?>"><?php echo $this->Html->image("frontend/website/home/outlook.svg",array('class' => 'icon-email', 'alt' => 'Logo de Outlook'));?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	$this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('lib/underscore-min.js',                                                                                        ['block' => 'AppScript']);
    $this->Html->script('lib/timingfield.js',                                                                                           ['block' => 'AppScript']);
    $this->Html->script('lib/sweetalert.min.js',                                                                                        ['block' => 'AppScript']);
    $this->Html->script('lib/jquery.geocomplete.js',                                                                                    ['block' => 'AppScript']);
    $this->Html->script('lib/jquery.caret.min.js',                                                                                      ['block' => 'AppScript']);
    $this->Html->script('lib/jquery.tag-editor.min.js',                                                                                 ['block' => 'AppScript']);
    $this->Html->script('https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyBbvlqcV_fMowHkFITyk9wuukAHSjhOZ0g', ['block' => 'AppScript']);
    $this->Html->script('controller/topics/index_edit.js',                                                                              ['block' => 'AppScript']);
?> 
 