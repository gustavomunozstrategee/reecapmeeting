
<?php echo $this->Html->css('lib/fullcalendar.css', ['block' => 'AppCss']); ?>
<?php $this->Html->css("frontend/admin/components/calendar-editado.css", array("block" => "styles_section")); ?> 
<?php $permissionAdd = $this->Utilities->check_team_permission_action(array("add"), array("meetings")); ?>
<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Calendario de reuniones'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Agendar reunión") ?>" href="<?php echo $this->Html->url(array('controller'=>'meetings','action' => 'add')); ?>">
                    <i class="flaticon-add"></i>
                </a> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"teams",'action'=>'index'));?>"><?php echo __("Empresas") ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Reuniones") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                
                <h2>
                    <?php echo __('Listar reuniones'); ?>
                </h2>
  
                <div class="widget-options">
                    <div class="" role="">
                        <a  class="btn-sync-calendar display-inline-block f-cursor-pointer btn-email" data-toggle="tooltip"  style="width: 30px; cursor:pointer;" data-placement="top" title="<?php echo __("Sincronizar con Gmail")?>">
                          <?php echo $this->Html->image("frontend/website/home/gmail.svg",array('class' => '', 'alt' => 'Logo de Gmail'));?>
                      </a>
                      <a class="btn-email display-inline-block" data-toggle="tooltip" style="width: 30px; cursor: pointer;"  data-placement="top" title="<?php echo __("Sincronizar con outlook") ?>"  href="<?php echo oAuthService::getLoginUrl(configure::read("AUTH_OUTLOOK_URL_EVENTS"))?>">
                        <?php echo $this->Html->image("frontend/website/home/outlook.svg",array('class' => '', 'alt' => 'Logo de Outlook'));?>
                    </a>
                    </div>
                </div>

            </div>
            <div class="widget-body">
                <div id="my-calendar"></div>
            </div>
        </div>
    </div>
</div>


<div id="calendarModal" class="modal fade">
    <div class="modal-dialog modal-recapmeeting">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo __("Reuniones en esta fecha")?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
            </div>
            <div id="modalBody" class="modal-body">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table f-table-grid-layout-fixed" id="content-meetings">
                        <thead>
                            <tr>
                                <th class="table-grid-50 f-blue font-bold"><?php echo __('Asunto'); ?></th>
                                <th class="table-grid-20 f-blue font-bold"><?php echo __('Lugar'); ?></th>
                                <th class="table-grid-30 f-blue font-bold text-center"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody id="show-content-meetings-calendar"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __("Cerrar")?></button>
            </div>
        </div>
    </div>
</div>
<div id="calendarModalDetailMeeting" class="modal fade">
    <div class="modal-dialog modal-lg modal-recapmeeting">
        <div class="modal-content">
            <div class="modal-header">
                <h4  class="modal-title"><?php echo __("Detalle de la reunión")?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
            </div>
            <div id="modalBody" class="modal-body">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table f-table-grid-layout-fixed" id="content-meetings">
                        <thead>
                            <tr>
                                <th class="table-grid-10 f-blue font-bold"><?php echo __('Tipo de reunión'); ?></th>
                                <th class="table-grid-30 f-blue font-bold"><?php echo __('Asunto'); ?></th>
                                <th class="table-grid-15 f-blue font-bold"><?php echo __('Fecha y hora de inicio'); ?></th>
                                <th class="table-grid-15 f-blue font-bold"><?php echo __('Fecha y hora de fin'); ?></th>
                                <th class="table-grid-10 f-blue font-bold"><?php echo __('Lugar'); ?></th>
                                <th class="table-grid-20 f-blue font-bold text-center"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody id="show-content-detail-meeting"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __("Cerrar")?></button>
            </div>
        </div>
    </div>
</div>
<div id="calendarModalDetailMeetingDay" class="modal fade">
    <div class="modal-dialog modal-recapmeeting">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo __("Detalle de la reunión")?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span></button>
            </div>
            <div id="modalBody" class="modal-body">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover" id="content-meetings">
                        <thead class="text-primary">
                            <tr>
                                <th><?php echo __('Tipo de reunión'); ?></th>
                                <th><?php echo __('Asunto'); ?></th>
                                <th><?php echo __('Fecha y hora de inicio'); ?></th>
                                <th><?php echo __('Fecha y hora de fin'); ?></th>
                                <th><?php echo __('Lugar'); ?></th>
                            </tr>
                        </thead>
                        <tbody id="show-content-detail-meeting-days"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-modal-recapmeeting" data-dismiss="modal"><?php echo __("Cerrar")?></button>
            </div>
        </div>
    </div>
</div>
<div id="modalAssistantsMeeting" class="modal fade">
    <div class="modal-dialog modal-recapmeeting">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo __("Detalle de la reunión")?></h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span> <span class="sr-only"><?php echo __("Cerrar")?></span>
                </button>
            </div>
            <div id="show-content-meetings-detail" class="modal-content"></div>
        </div>
    </div>
</div>
</div>
<?php
$this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
    $this->Html->script('lib/fullcalendar-3.8.2/moment.min.js',                                         ['block' => 'AppScript']);
$this->Html->script('lib/fullcalendar-3.8.2/fullcalendar.js',                                       ['block' => 'AppScript']);
    if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/fullcalendar-3.8.2/locale/es.js',['block' => 'AppScript']);

    }

$this->Html->script('lib/fullcalendar-3.8.2/gcal.js', 												['block' => 'AppScript']);
$this->Html->script('lib/datepicker/bootstrap-datepicker.js',										['block' => 'AppScript']);
$this->Html->script('controller/meetings/index.js', 												['block' => 'AppScript']);

?>