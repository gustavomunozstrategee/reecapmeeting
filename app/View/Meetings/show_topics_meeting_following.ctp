 
    <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo __('Descripción') ?></th>
                                                        <th><?php echo __('Tiempo') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>  
    <?php if(!empty($topics)):?>
        <?php foreach ($topics as $topic): ?>
        <tr>
                                                        <td>
                                                            <?php echo $this->Text->truncate(h($topic['description']),300); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $this->Utilities->conversorSegundosHoras($topic['time']); ?>
                                                        </td>
                                                    </tr>
        <?php endforeach; ?>
         </tbody>
                                            </table>
    <?php else: ?>
        <div class="flex-center">
            <p class="text-center mxy-10"><?php echo __("No hay temas específicos en esta reunión.");?></p>
        </div>
    <?php endif; ?>
     


   


   <div class="table-responsive">
                                            
                                                    
                                               
                                        