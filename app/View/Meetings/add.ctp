<?php echo $this->Html->css('lib/timingfield.css',			['block' => 'AppCss']); ?>
<?php echo $this->Html->css('lib/jquery.tag-editor.css',	['block' => 'AppCss']); ?>
<?php $permission = $this->Utilities->check_team_permission_action(array("index","add"), array("meetings")); ?>


 <div class="row">
                            <div class="page-header">
                                <div class="d-flex align-items-center">
                                    <h2 class="page-header-title"><?php echo __('Reuniones'); ?></h2>
                                    <div>
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>"><i class="ti ti-home"></i></a> </li>
                                            <li class="breadcrumb-item"><a href="<?php echo $this->webroot ?>meetings"><?php echo __('Reuniones'); ?></a></li>
                                            <li class="breadcrumb-item active"><?php echo __('Crear citación a reunión'); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

<div class="row flex-row">
    <div class="col-xl-12">
        <!-- Begin Widget 07 -->
        <div class="widget widget-07 has-shadow">
            <!-- Begin Widget Header -->
            <div class="widget-header bordered d-flex align-items-center">
                <h2><?php echo __('Crear citación a reunión'); ?></h2>
                <div class="widget-options">
                    <div class="" role="">
                        <a href="<?php echo $this->Html->url(array('action'=>'index'));?>"><button type="button" class="btn btn-secondary "><i class="ti ti-calendar"></i><?php echo __('Ver reuniones'); ?></button></a>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->create('Meeting', array('role' => 'form','data-parsley-validate','novalidate' => true)); ?>
            <div class="widget-body">
                 <div class="row">
                    <div class="col-md-7 col-lg-7 sol-sm-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Meeting.meeting_type',__('Tipo de reunión'), array('class'=>'control-label f-blue required'));?>
                                    <?php if($this->Session->read("Config.language") == "esp"):?>
                                            <?php echo $this->Form->input('meeting_type', array('class' => 'form-control border-input meeting-type','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("TYPE_MEETING"))); ?>
                                        <?php else: ?>
                                            <?php echo $this->Form->input('meeting_type', array('class' => 'form-control border-input meeting-type','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("TYPE_MEETING_ENG"))); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Meeting.meeting_room',__('Modalidad de la reunión'), array('class'=>'control-label f-blue required'));?>
                                     <?php if($this->Session->read("Config.language") == "esp"):?>
                                        <?php echo $this->Form->input('meeting_room', array('class' => 'form-control border-input select-location','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("MODALITY_MEETING"), 'empty' => __("Seleccione una opción"))); ?>
                                        <?php else: ?>
                                        <?php echo $this->Form->input('meeting_room', array('class' => 'form-control border-input select-location','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("MODALITY_MEETING_ENG"), 'empty' => __("Seleccione una opción"))); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-12" style="display: none;" id="location-meeting">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Meeting.location',__('Ubicación - dirección'), array('class'=>'control-label f-blue required'));?>
                                    <?php echo $this->Form->input('location', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => "")); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <?php echo $this->Form->label('Meeting.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
                                    <?php echo $this->Form->input('team_id', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Empresa'),'type' => 'select','empty' => __("Seleccione una opción"), 'options' => $teams)); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='form-group'>
                                    <div id="show_content_client">
                                        <?php echo $this->Form->label('Meeting.client_id',__('Cliente'), array('class'=>'control-label f-blue required'));?>
                                        <?php echo $this->Form->input('client_id', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Cliente'),'type' => 'select','empty' => __("Seleccione una opción"))); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                 <div class='form-group'>
                                    <?php echo $this->Form->label('Meeting.subject',__('Asunto'), array('class'=>'control-label f-blue required'));?>
                                    <?php echo $this->Form->input('subject', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Asunto'))); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo $this->Form->label('Meeting.start_date',__('Fecha y hora del inicio de la reunión'), array('class'=>'control-label f-blue'));?>
                                    <div class='input-group date' id='datetimepickerStart'>
                                        <?php echo $this->Form->input('start_date', array('class' => 'form-control ', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo $this->Form->label('Meeting.end_date',__('Fecha y hora fin de la reunión'), array('class'=>'control-label f-blue'));?>
                                    <div class='input-group date' id='datetimepickerEnd'>
                                        <?php echo $this->Form->input('end_date', array('class' => 'form-control ', 'label'=>false,'div'=>false,'placeholder' => __('Calendario'),'type' => 'text','readonly' => true)); ?>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar f-blue"><i class="ti ti-calendar"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php $data = $this->Session->read('Dates'); ?>
                                <div class='text-center' id="show-content-date-meetings" style="display:none; ">
                                    <div class="flex-justify-end">
                                        <button type='button' class='btn btn-success mxy-10 btn-add-new-date'><i class="ti ti-calendar"></i> <?php echo __("Añadir fechas para próxima reunión")?></button>
                                    </div>
                                    <br>
                                </div>
                                
                            </div>
                            <div class="col-md-12">
                                <p class="text-danger text-center loadAddMeetingFollowing" style="display:none"><?php echo __('Espera...'); ?></p>
                                <div id="list-following-meetings" class="table-responsive">
                                    <?php if(!empty($data)):?>
                                    <?php echo $this->Form->label('Meeting.topics',__('Reuniones de seguimiento'), array('class'=>'control-label f-blue'));?>
                                    <table cellpadding="0" cellspacing="0" class="table">
                                        <thead>
                                            <tr>
                                                <th class=""><?php echo __('Fecha inicio') ?></th>
                                                <th class="f-blue"><?php echo __('Fecha fin') ?></th>
                                                <th class="f-blue"><?php echo __('Modalidad') ?></th>
                                                <th class="f-blue"><?php echo __('') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($data)):?>
                                            <?php foreach ($data as $dateId => $dates): ?>
                                            <tr>
                                                <td><?php echo h($this->Time->format('d-m-Y h:i A',$dates['start_date'])); ?></td>
                                                <td><?php echo h($this->Time->format('d-m-Y h:i A',$dates['end_date'])); ?></td>
                                                <td><?php echo h($dates['meeting_name']); ?></td>
                                                <td class="text-center">
                                                    <a rel="tooltip" data-id="<?php echo $dateId ?>" title="<?php echo __('Nuevo ítem'); ?>" class="btn-xs f-blue btn-add-topic-meeting-following">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <a rel="tooltip" data-id="<?php echo $dateId ?>" title="<?php echo __('Ver temas específicos'); ?>" class="btn-xs f-blue btn-view-topic-meeting-following">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                            <?php else: ?>
                                            <tr>
                                                <td class="text-center" colspan="14"><?php echo __("No hay fechas de reuniones de seguimiento añadidas recientemente");?></td>
                                            </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class='form-group'>
                                        <?php echo $this->Form->label('Meeting.description',__('Descripción'), array('class'=>'control-label f-blue required'));?>
                                        <?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none','label'=>false,'div'=>false,'placeholder' => __('Descripción'),'maxLength' => 300)); ?>
                                    </div>
                                    <div id="showCharacter"></div>
                            </div>

                            <div class="col-md-12" id="content-agend">
                                <hr>

                                <div class="widget-body">
                                        <div class="table-responsive">
                                            <table class="table mb-0" id="grade-table">
                                                <thead>
                                                    <tr style="text-align: center; ">
                                                        <th><?php echo __('Descripción') ?></th>
                                                        <th><?php echo __('Tiempo') ?></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php if (!empty($this->request->data['Topic'])) :?>
                                                <?php for ($key = 0; $key < count($this->request->data['Topic']); $key++) :?>
                                                <?php echo $this->element('topics', array('key' => $key));?>
                                                <?php endfor;?>
                                                <?php else: ?>
                                                <?php $key = 0;?>
                                                <?php echo $this->element('topics', array('key' => $key));?>
                                                <?php endif;?>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td class="actions">
                                                        <a rel="tooltip" title="<?php echo __('Nuevo ítem'); ?>" class="add btn btn-success">
                                                            <i class="fa fa-plus text-white"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                            </table>
                                            <script id="grade-template" type="text/x-underscore-template">
                                        <?php echo $this->element('topics');?>
                                    </script>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 p-md-0">
                        <div class="bg-filter mt-2em">
                            <label class="f-blue"><?php echo __('Invitados');?></label>
                            <div class='form-group'>
                                <?php echo $this->Form->input('invited', array('class' => 'form-control border-input invited-users','label'=>false,'placeholder' => __('Añadir invitados con el nombre o correo'),'id' => 'invited')); ?>
                                <div id="suggestions"></div>
                            </div>
                            <p class="text-danger text-center loadAddAssistants" style="display:none"><?php echo __('Espera...'); ?></p>
                            <div id="show-content-inviteds" class="scroll-inviteds">
                                <div>
                                    <?php $userInvitedsAdded = $this->Session->read('Inviteds');?>
                                    <?php if(!is_null($userInvitedsAdded) || count($userInvitedsAdded) > 0):?>
                                    <ul class="list-group w-100 mt-5">
                                    <?php foreach ($userInvitedsAdded as $key => $userInvited): ?>
                                        
                                            <li class="list-group-item">
                                                <div class="other-message">
                                                    <div class="media">
                                                        <div class="media-left align-self-center mr-3">
                                                            <img class="avatar rounded-circle" src="<?php echo $this->Html->url('/files/User/'.$userInvited['image']); ?>" alt="..." class="img-fluid rounded-circle" style="width: 50px;height: 50px">
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <div class="other-message-sender"><?php echo h($userInvited['name']); ?> </div>
                                                            <div class="other-message-time"><?php echo h($userInvited['email']); ?></div>
                                                        </div>
                                                        <div class="media-right align-self-center">
                                                            <div class="actions td-actions">
                                                                
                                                                <a  data-id="<?php echo $key ?>" href="#" class="delete-user-meeting-following remove">
                                                <i class="la la-close delete"></i>
                                            </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                       
                                     
                                    <?php endforeach ?>
                                     </ul>
                                     <br>
                                    <?php else: ?>
                                    <div>
                                        <p class="text-center"><?php echo __("No hay usuarios invitados añadidos.");?></p>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <p class="f-blue"><?php echo __("Para añadir asistentes externos primero debes escoger una empresa y un cliente.")?></p>
                        </div>
                        <hr>
                        <!--Tags-->
                        <div class="bg-filter">
                            <label class="f-blue"><?php echo __('Tags');?></label>
                            <div class='form-group'>
                                <?php echo $this->Form->input('tag', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => 'Tag','id' => 'tag')); ?>
                            </div>
                        </div>
                        <!--Tags-->
                        <hr>
                        <div class="bg-filter">
                            <label class="f-blue"><?php echo __('Privacidad');?></label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="data[Meeting][private]" class="public-meeting" value="1" <?php echo (@$this->request->data['Meeting']['private'] == 1 ? 'checked' : 'checked');?>> <?php echo __("Reunión pública")?>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="data[Meeting][private]" class="password-meeting" value="2" <?php echo (@$this->request->data['Meeting']['private'] == 2 ? 'checked' : '');?>> <?php echo __("Proteger con contraseña")?>
                                </label>
                            </div>
                            <div id="content-password" style="display: none;">

                                <div>
                                    <div class="input-group">
                                        <?php echo $this->Form->input('password', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Contraseña'), 'id' => 'pass-meeting','value' => @$this->request->data['Meeting']['password'], "maxlength" => 4, 'autocomplete' => 'new-password')); ?>
                                        
                                            
                                            <span class="input-group-btn generate-password">
                                                                <button type="button" class="btn btn-info">
                                                                    <i class="la la-refresh"></i>
                                                                </button>
                                                            </span>
                                    </div>
                                </div>

                                <div>
                                    <button type="button" class="btn btn-info show-password "><i class="ion ion-eye"></i><?php echo __("Mostrar contraseña")?></button>
                                    <button type="button" class="btn btn-danger hide-password " style="display: none;"><i class="ion ion-eye-disabled"></i><?php echo __("Ocultar contraseña")?></button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <?php if(isset($permission["add"])):
                        ?> 
                            <div  id="meeting-normal">
                                <br>
                                <button type='button' class='btn btn-success my-10 pull-right btn-save-meeting'><?php echo __("Enviar invitaciones")?></button>
                            </div>
                            <div  id="meeting-following" style="display: none">
                                <button type='button' class='btn btn-success my-10 pull-right btn-save-meeting-following'><?php echo __("Enviar invitaciones")?></button>
                            </div>
                        <?php endif ?> 
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                    </div>
                 </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>








<div class="backg-white">
    <div class="flex-space-between">
    </div>
    <div class="">
        <div class="">
            <div class="">
               

                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="general-form">
                            <!-- <div class='text-center'>
                            <p><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label required f-blue'));?></p>
                            </div> -->
                            <div class="bg-filter mt-2em">
                                <div class='row'>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12" style="display: none;" id="location-meeting">
                                        <div class='form-group'>
                                            <?php echo $this->Form->label('Meeting.location',__('Ubicación - dirección'), array('class'=>'control-label f-blue required'));?>
                                            <?php echo $this->Form->input('location', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => "")); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        
                                    </div>
                                </div>
                                
                                

                                <div>
                                    
                                </div>
                                
                        </div>
                    </div>
                    </div>
                    
                </div>

                
            </div>
        </div>
    </div>
    <div id="modalTopics" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                        <h4 class="modal-title"><?php echo __("Temas específicos")?></h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only"><?php echo __("Cerrar")?></span>
                        </button>
                    </div>
                <div class="modal-body">
                <form id="topics-following">
                    <div class="row">
                        <div class="col-md-8 form-group">
                        <?php echo $this->Form->label('Topic.description',__('Descripción'), array('class'=>'control-label f-blue'));?>
                        <?php echo $this->Form->textarea("Topic.description", array("class" => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => 'Descripción',"id"=>"description-topics","maxlength"=>255,"rows"=>"5")); ?>
                         </div>
                         <div class="col-md-4 form-group">
                        <?php echo $this->Form->label('Topic.time',__('Tiempo'), array('class'=>'control-label f-blue'));?>
                        <?php echo $this->Form->text("Topic.time", array("class" => 'form-control border-input time-duration topic-time','label'=>false,'div'=>false,'placeholder' => 'Tiempo',"readonly"=>"readonly","id" => "add-topic-following")); ?>
                        </div>
                    </div>
                    
                    
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo __("Cerrar")?></button>
                        <button type="button" class="btn btn-success btn-add-topics-meetings"><?php echo __("Añadir")?></button>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <div id="modalTopicsDetail" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    
                        <h4 class="modal-title"><?php echo __("Temas específicos de esta reunión")?></h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only"><?php echo __("Cerrar")?></span>
                        </button>
                     
                </div>
                <div class="modal-body">
                <div id="show-content-topics-meetings-followings"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="AutenticationEmail" class="modal fade">
        <div class="modal-dialog modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    
                        <h4 class="modal-title"><?php echo __("Seleccionar modo de autenticación")?></h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only"><?php echo __("Cerrar")?></span>
                        </button>
                         <p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p>
                     
                </div>
                 
                <div class="modal-body">
                    <p><?php echo __("Para terminar de agendar la reunión deberás autenticarte con tu cuenta de Gmail o de Outlook, ten en cuenta que la cuenta que selecciones será con la que quedará organizada la reunión en los calendarios o puedes omitir la sincronización y la reunión solamente se guardará en el sistema y no en los calendarios de los usuarios.")?></p>
                </div>
                <div class="modal-footer">
                    <a style="width: 50px" class="btn-auth-gmail f-cursor-pointer" data-toggle="tooltip" data-placement="top" title="<?php echo __('Sincronizar con Gmail'); ?>">
                     <?php echo $this->Html->image("frontend/website/home/gmail.svg",array('class' => 'icon-email', 'alt' => 'Logo de Gmail'));?>
                    </a>
                    <a style="width: 50px" class="btn-outlook f-cursor-pointer" data-toggle="tooltip" data-placement="top" title="<?php echo __('Sincronizar con Outlook'); ?>" href="<?php echo oAuthService::getLoginUrl(configure::read("AUTH_OUTLOOK_URL"))?>"><?php echo $this->Html->image("frontend/website/home/outlook.svg",array('class' => 'icon-email', 'alt' => 'Logo de Outlook'));?></a>
                    <div class="text-center" style="color: white">
                      <a class="btn btn-danger btn-add-meetings-data-without-sync"><?php echo __("Omitir sincronización")?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="AutenticationEmailMeetingFollowing" class="modal fade">
        <div class="modal-dialog modal-recapmeeting">
            <div class="modal-content">
                <div class="modal-header">
                    
                        <h4 class="modal-title"><?php echo __("Seleccionar modo de autenticación")?></h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only"><?php echo __("Cerrar")?></span>
                        </button>
                         <p class="text-danger text-center errorP" style="display:none"><?php echo __('Espera...'); ?></p>
                     
                </div>
                <div class="modal-body">
                    <p><?php echo __("Para terminar de agendar la reunión deberás autenticarte con tu cuenta de Gmail o de Outlook, ten en cuenta que la cuenta que selecciones será con la que quedará organizada la reunión en los calendarios o puedes omitir la sincronización y la reunión solamente se guardará en el sistema y no en los calendarios de los usuarios.")?></p>
                </div>
                <div class="modal-footer">  
                    <a style="width: 50px" class="btn-auth-gmail-followings-meetings f-cursor-pointer" data-toggle="tooltip" data-placement="top" title="<?php echo __('Sincronizar con Gmail'); ?>">
                      <?php echo $this->Html->image("frontend/website/home/gmail.svg",array('class' => 'icon-email', 'alt' => 'Logo de Gmail'));?>
                    </a>
                    <a style="width: 50px" class="btn-outlook-following f-cursor-pointer" href="<?php echo oAuthService::getLoginUrl(configure::read("AUTH_OUTLOOK_URL_MEETING_FOLLOWING"))?>" data-toggle="tooltip" data-placement="top" title="<?php echo __('Sincronizar con Outlook'); ?>"><?php echo $this->Html->image("frontend/website/home/outlook.svg",array('class' => 'icon-email', 'alt' => 'Logo de Outlook'));?></a>
                    <div class="text-center" style="color: white">
                    <a class="btn btn-danger btn-add-meetings-followings-data-without-sync"><?php echo __("Omitir sincronización")?></a>       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	//$this->Html->script('bootstrap.3.3.7/datetimepicker/js/bootstrap-datetimepicker.min.js',													['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/moment.min.js', ['block' => 'AppScript']);
    $this->Html->script('/assets/vendors/js/datepicker/daterangepicker.js', ['block' => 'AppScript']);
	if($this->Session->read('Config.language') == 'esp') {
        $this->Html->script('lib/datepicker-es.js', ['block' => 'AppScript']);
        $this->Html->script('lib/select2/i18n/es.js', ['block' => 'AppScript']);
    }
    $this->Html->script('lib/underscore-min.js', 																								['block' => 'AppScript']);
	$this->Html->script('lib/timingfield.js', 																									['block' => 'AppScript']);
    $this->Html->script('lib/sweetalert.min.js',																								['block' => 'AppScript']);
	$this->Html->script('lib/jquery.geocomplete.js', 																							['block' => 'AppScript']);
	$this->Html->script('controller/topics/index.js', 																							['block' => 'AppScript']);
	$this->Html->script('lib/jquery.caret.min.js', 																								['block' => 'AppScript']);
	$this->Html->script('lib/jquery.tag-editor.min.js', 																						['block' => 'AppScript']);
	$this->Html->script('controller/tags/index.js', 																							['block' => 'AppScript']);
	$this->Html->script('https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyBbvlqcV_fMowHkFITyk9wuukAHSjhOZ0g', ['block' => 'AppScript']);
?> 

