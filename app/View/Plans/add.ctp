<?php echo $this->Html->css('lib/select2.min.css',		['block' => 'AppCss']);?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Registrar plan'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __('Listar'); ?>" href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                    <i class="flaticon-list"></i>
                </a>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"plans",'action'=>'index'));?>"><?php echo __("Planes") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center"></div>
            <div class="widget-body">
                <?php echo $this->Form->create('Plan', array('role' => 'form','data-parsley-validate','novalidate' => true)); ?>
                    <div class='form-group'>
                        <?php echo $this->Form->label('Plan.name',__('Nombre'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'div'=>false,'maxlength' =>'40')); ?>
                    </div>
                    
                    <div class='form-group'>
                        <?php echo $this->Form->label('Plan.users',__('Usuarios incluidos en el plan'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('users', array('class' => 'form-control border-input','type'=>'number','label'=>false,'div'=>false)); ?>
                    </div>
              
                    <div class='form-group'>
                        <?php echo $this->Form->label('Plan.price',__('Precio'), array('class'=>'control-label f-blue required'));?>
                        <?php echo $this->Form->input('price', array('class' => 'form-control border-input','type'=>'text','label'=>false,'div'=>false)); ?>
                    </div>
                    
                    <div class='form-group'>
                        <?php echo $this->Form->label('Plan.white_brand',__('¿Este plan tendrá marca blanca? La marca blanca permitirá a las empresas que adquieran este plan subir el logo de su empresa al crear el acta'), array('class'=>'control-label f-blue required'));?>
                        <?php if($this->Session->read("Config.language") == "esp"):?>
                            <?php echo $this->Form->input('white_brand', array('class' => 'form-control border-input','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("WHITE_BRAND"),'empty' => __("Seleccione una opción"))); ?>
                        <?php else: ?>
                            <?php echo $this->Form->input('white_brand', array('class' => 'form-control border-input','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("WHITE_BRAND_ENG"),'empty' => __("Seleccione una opción"))); ?>
                        <?php endif; ?>
                    </div>

					<div class='form-group'>
                       <?php echo $this->Form->label('Plan.template',__('Permiso para crear plantillas en el acta'), array('class'=>'control-label f-blue required'));?>
                        <?php if($this->Session->read("Config.language") == "esp"):?>
                            <?php echo $this->Form->input('template', array('class' => 'form-control border-input','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("TEMPLATE"),'empty' => __("Seleccione una opción"))); ?>
                        <?php else: ?>
                            <?php echo $this->Form->input('template', array('class' => 'form-control border-input','type'=>'select','label'=>false,'div'=>false,'options' => configure::read("TEMPLATE_ENG"),'empty' => __("Seleccione una opción"))); ?>
                        <?php endif; ?>
                    </div>
                   
                    <div class="col-50 padding-right-15">
                        <div class='form-group'>
                            <?php echo $this->Form->label('Plan.description',__('Descripción'), array('class'=>'control-label f-blue required'));?>
                            <?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none','label'=>false,'div'=>false, 'maxLength' => 300)); ?>
                        </div>
                        <div id="showCharacter"></div>
            			<input type="checkbox" id="cbox-plan-recommended" name="data[Plan][recommended]" value="1"> <label for="cbox-plan-recommended" class="f-blue">
                            <?php echo __('¿Deseas establecer este plan como plan recomendado?')?>
                        </label>
                    </div>
                    
                    <div class="flex-justify-end">
                        <button type='submit' class="btn btn-primary"><?php echo __('Guardar') ?> </button>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
	$this->Html->script('lib/select2/select2.min.js', 				['block' => 'AppScript']);
	$this->Html->script('Plans/plan_app.js',  				        ['block' => 'AppScript']);
	$this->Html->script('lib/parsley/parsley.min.js',			    ['block' => 'AppScript']);
	if ($this->Session->read('Config.language') == 'esp') {
		$this->Html->script('lib/parsley/i18n/es.js',				['block' => 'AppScript']);
	}
?>