<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Visualización del plan'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Listar");?>"  href="<?php echo $this->Html->url(array('action'=>'index'));?>">
                    <i class="flaticon-menu"></i>
                </a> 
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo $this->Html->url(array("controller"=>"plans",'action'=>'index'));?>"><?php echo __("Planes") ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <div class="row w-100"> 
                    <div class="col-md-12"> 
                        <a class="btn-circle-icon float-sm-right" data-toggle="tooltip" data-placement="right" title="<?php echo __("Editar") ?>" href="<?php echo $this->Html->url(array('action'=>'edit', EncryptDecrypt::encrypt($plan['Plan']['id'])));?>">
                            <i class="flaticon-draw"></i>
                        </a>
                        <a class="btn-circle-icon float-sm-right" data-toggle="tooltip" data-placement="right" title="<?php echo __("Adicionar") ?>" href="<?php echo $this->Html->url(array('action'=>'add'));?>">
                            <i class="flaticon-add"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="widget-body">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <tr>
                            <td><?php echo __('Nombre'); ?></td>
                            <td><?php echo h($plan['Plan']['name']); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Descripción'); ?></td>
                            <td><?php echo $this->Text->truncate(h($plan['Plan']['description']),100); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Usuarios'); ?></td>
                            <td><?php echo h($plan['Plan']['users']); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Precio'); ?></td>
                            <td><?php echo h("$ " . $plan['Plan']['price']); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><div class="em-separator separator-dashed m-1"></div></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Fecha de registro'); ?></td>
                            <td><?php echo h($this->Time->format('d-m-Y h:i A', $plan['Plan']['created'])); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2 class="f-blue font-bold"><?php echo __('Empresas con este plan adquirido'); ?></h2>
            </div>
            <div class="widget-body">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                        <thead>
                            <tr>
                                <th><?php echo __('Nombre') ?></th>
                                <th><?php echo __('Correo electrónico') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($business)) {?>
                                <?php foreach ($business as $busines): ?>
                                    <tr>
                                        <td><?php echo h($busines['User']['firstname'] . ' ' . $busines['User']['lastname']); ?></td>
                                        <td><?php echo h($busines['User']['email']); ?></td>
                                    </tr>
                                <?php endforeach ?>
                            <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="2"><?php echo __("No hay empresas con este plan adquirido.");?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div>
                        <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
                            ));
                        ?>
                    </div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                                echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                                echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->Html->script('lib/sweetalert.min.js', ['block' => 'AppScript']); ?>