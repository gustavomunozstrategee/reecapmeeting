 <div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <?php echo __('Gestión de planes'); ?>
                <a class="btn-circle-icon" data-toggle="tooltip" data-placement="right" title="<?php echo __("Registrar plan") ?>" href="<?php echo $this->Html->url(array('action'=>'add')); ?>">
                    <i class="flaticon-add"> </i>
                </a>
            </h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Router::url("/",true) ?>">
                            <i class="ti ti-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo __("Planes") ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <?php echo $this->Form->create('User', array('role' => 'form','type'=>'GET','class'=>'form-inlines w-100')); ?>
                    <div class="row"> 
                        <div class="col-md-6">
                            <div class="input-group">
                                <?php echo $this->Form->input('q', array('value'=> !empty($this->request->query['q']) ? $this->request->query['q'] : '','placeholder'=>__('Nombre, descripción, precio'), 'class'=>'form-control','label'=>false,'div'=>false)) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary float-sm-right" id="search_team">
                                <?php echo __('Buscar');?>
                                <i class="la la-search"></i>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <?php if(empty($plans) && !empty($this->request->query['q'])) : ?>
                                <p class="mxy-10"><?php echo __('No se encontraron datos.') ?></p>
                                <?php echo $this->Html->link(__('Borrar todos los filtros'), array('action' => 'index'),array('class' => 'f-link-search')); ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="<?php echo $this->request->is('mobile') ? '' : 'widget-body' ?>">
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" class="table table-hover f-table-recapmeeting f-table-grid-layout-fixed">
                        <thead class="text-primary">
                            <tr>
                                <th class="table-grid-10"><?php echo __('Nombre'); ?></th>
                                <th class="table-grid-20"><?php echo __('Descripción'); ?></th>
                                <th class="table-grid-10 text-center"><?php echo __('Usuarios'); ?></th>
                                <th class="table-grid-10"><?php echo __('Precio'); ?></th> 
                                <th class="table-grid-10"><?php echo __('Estado'); ?></th>
                                <th class="table-grid-10 text-center"><?php echo __('Plan recomendado'); ?></th>
                                <th class="table-grid-10 text-center"><?php echo __('¿Marca blanca?'); ?></th>
                                <th class="table-grid-10 text-center"><?php echo __('¿Crear plantilla?'); ?></th>
                                <th class="table-grid-10 text-center"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($plans)) {?>
                            <?php foreach ($plans as $plan): ?>
                                <tr>
                                    <td class="table-grid-10 td-word-wrap"><?php echo $this->Text->truncate(h($plan['Plan']['name']),100); ?></td>
                                    <td class="table-grid-20 td-word-wrap"><?php echo $this->Text->truncate(h($plan['Plan']['description']),100); ?></td>
                                    <td class="table-grid-10 td-word-wrap text-center"><span class="table-number"><?php echo h($plan['Plan']['users']); ?></span></td>
                                    <td class="table-grid-10 td-word-wrap"><span class="table-number"><?php echo h( "$ " . $plan['Plan']['price']); ?></span></td> 
                                    <td class="table-grid-10 td-word-wrap"><span class="table-number"><?php echo $this->Utilities->showState($plan['Plan']['state']); ?></span></td>
                                    <td class="table-grid-10 text-center td-word-wrap"><?php echo $this->Utilities->show_recommended($plan['Plan']['recommended']); ?></td>
                                    <td class="table-grid-10 text-center td-word-wrap"><?php echo $this->Utilities->showStateWhiteBrand($plan['Plan']['white_brand']); ?></td>
                                    <td class="table-grid-10 text-center td-word-wrap"><?php echo $this->Utilities->showStateTemplate($plan['Plan']['template']); ?></td> 
                                    <td class="table-grid-10 td-actions text-center"> 
                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'view', EncryptDecrypt::encrypt($plan['Plan']['id']))); ?>" title="<?php echo __('Ver'); ?>" class="btn-xs" data-toggle="tooltip">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a rel="tooltip" href="<?php echo $this->Html->url(array('action' => 'edit', EncryptDecrypt::encrypt($plan['Plan']['id']))); ?>" title="<?php echo __('Editar'); ?>" class="btn-xs" data-toggle="tooltip">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?php if ($plan['Plan']['recommended'] == Configure::read('DISABLED')): ?>
                                        <?php echo $this->Utilities->changeStateButton($plan['Plan']['id'], $plan['Plan']['state']); ?>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php } else {  ?>
                                <tr>
                                    <td class="text-center" colspan="9"><?php echo __('No existen planes.')?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget-footer border-top p-4">
                <div class="table-pagination">
                    <div><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} en total, comenzando en {:start}, terminando en {:end}')));?></div>
                    <div>
                        <ul class="pagination f-paginationrecapmeeting">
                            <?php
                            echo $this->Paginator->prev('< ' . __(''), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                            echo $this->Paginator->next(__('') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	$this->Html->script('lib/sweetalert.min.js', ['block' => 'AppScript']);
 ?>