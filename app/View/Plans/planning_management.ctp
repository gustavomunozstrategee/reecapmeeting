<?php $this->Html->css("frontend/admin/pages/planning_management.css", array("block" => "styles_section")); ?>
 
<div class="backg-white">
    <section class="bg-raiting curve-clippath">
        <div  class="paddingxy-2em">
        <h1 class="position-relative text-center"><?php echo __('Adquiere el plan que desees'); ?></h1>
        <div class="flex-center"> 
        <?php if(!empty($permissions["PlanUser"])): ?> 
            <a class="btn btn-reecapmeeting-user position-relative" href="<?php echo $this->Html->url(array('controller'=>'transactions','action'=>'index'));?>">
                <?php echo __("Transacciones")?>
            </a>
        <?php endif; ?>
        </div>
        </div>
    </section>

    <div>
        <div>
            <section>
                <div class="mxy-4">
                    <div class="slider-items">
                        <?php $count = 0;?>
                        <?php foreach ($plans as $plan): ?>
                        <div>
                            <?php if ($plan['Plan']['recommended'] == configure::read("ENABLED")){ ?>
                            <div class="card notRecomendado card-plan-diamond">
                                <?php } else { ?>
                                <div class="card recomendado card-plan">
                                    <?php } ?>
                                    <?php $hideMessageChoose = 1;?>
                                    <p><?php echo $this->Utilities->planRecommended($plan['Plan']['id'], $plan['Plan']['recommended'], $plan['Plan']['state'], $hideMessageChoose); ?></p>
                                    <div class="card-header">
                                        <?php if($plan['Plan']['recommended'] == configure::read("ENABLED")):?>
                                            <div class="tag-diamond">
                                                <?php echo __("Recomendado");?>
                                            </div>   
                                        <?php endif; ?> 
                                        <p class="card-name"><?php  echo   $plan['Plan']['name'] ?></p>
                                        <?php if($plan['Plan']['trial'] == configure::read("ENABLED")):?>
                                            <p class="card-price"><?php echo   '$ ' . $plan['Plan']['price'] . __(" /Mes") . ' <br> ' . __("30 Días")?></p>
                                            <?php else:?>
                                            <p class="card-price"><?php echo   '$ ' . $plan['Plan']['price'] . __(" /Mes") . ' <br> ' . __("1 AÑO")?></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="card-info">
                                        <p class="card-users"><i class="flaticon-users text-greenlight"></i></p>
                                        <p class="card-users"><?php echo $plan['Plan']['users'] ?></p>
                                        <p class="card-description">
                                            <?php echo $this->Text->truncate($plan['Plan']['description'],150); ?>
                                        </p>                                  
                                        <p class="f-blue font-bold"><?php echo __("Fecha de vencimiento:");?></p> 
                                        <p class="f-blue"><?php echo $this->Utilities->showExpiredDate($plan, $planActualInfo); ?></p>
                                    </div>
                                    <div class="card-btn-bottom"><?php echo $this->Utilities->validatePlanActualAndChangePlan($plan, $planActualInfo, $planVencidoInfo, $planTrial); ?></div>
                                </div>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
                <div class="bg-white paddingxy-2em">
                    <h3 class="f-blue font-bold"><?php echo __('Redimir cupón'); ?></h3>
                    <form class="flex-space-between"> 
                        <div class='form-group'>
                            <?php echo $this->Form->label('Coupon.name',__('Ingresa aquí el código del cupón a redimir, recuerda que está compuesto por números y letras.'), array('class'=>'control-label f-blue'));?>
                            <?php echo $this->Form->input('code_coupon', array('class' => 'form-control border-input','label'=>false,'div'=>false)); ?>
                        </div>
                        <button type='button' class='btn btn-reecapmeeting-user  btn-redeem-coupon'><?php echo __('Remidir cupón')?></button>
                    </form>
                </div>
            </section>
            <div id="show-loading-content" class="text-center" style="display: none;">
                <img src="<?php echo $this->Html->url('/img/loading.gif'); ?>" height="90" width="90">
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('lib/sweetalert.min.js',['block' => 'AppScript']); ?>
<?php echo $this->Html->script('Plans/plan_app.js', ['block' => 'AppScript']); ?>

