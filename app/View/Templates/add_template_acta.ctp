<?php echo $this->Form->create('Template', array('role' => 'form','data-parsley-validate','type'=>'file')); ?>
	<div class="row">
		<div class='col-md-12'>
    		<p style="color:red"><?php echo $this->Form->label('',__('Campo obligatorio '), array('class'=>'control-label f-blue required'));?></p>
		</div>
		<div class='col-md-12'>
			<div class='form-group'>
				<?php echo $this->Form->label('Template.name',__('Nombre'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('name', array('class' => 'form-control border-input','label'=>false,'div'=>false,'placeholder' => __('Nombre'), "maxlength" => 50)); ?>
			</div>
		</div>
		<div class='col-md-12' >
			<div class='form-group'>
				<?php echo $this->Form->label('Template.description',__('Descripción'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('description', array('class' => 'form-control border-input resize-none', 'label'=>false,'div'=>false,'placeholder' => 'Descripción')); ?>
			</div>
		</div>

		<div class='col-md-12' >
			<div class='form-group'>
				<?php echo $this->Form->label('Template.team_id',__('Empresa'), array('class'=>'control-label f-blue required'));?>
				<?php echo $this->Form->input('team_id', array('class' => 'form-control border-input resize-none', 'label'=>false,'div'=>false, 'type' => 'select','options' => $team)); ?>
			</div>
		</div>

		<div class='col-md-12'>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Cancelar') ?></button>
	        <button type="button" class="btn btn-primary" id="btn_saveAddTemplate"><?php echo __('Guardar') ?></button>
		</div>
	</div>
<?php echo $this->Form->end(); ?>