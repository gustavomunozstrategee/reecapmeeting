
<?php foreach ($templates as $value): ?>
<div class="col-sm-9">
	<div class="mt-3">
		<div class="styled-radio">
			<input type="radio" name="click_template" class="click_template" id="rad-<?php echo $value['Template']['id'] ?>" value="<?php echo $value['Template']['id'] ?>">
			<label for="rad-<?php echo $value['Template']['id'] ?>"><?php echo $value['Template']['name'] ?></label>
			<?php if($value['Template']['user_id'] == AuthComponent::user('id')){ ?>
	  			<button class="btn btn-default btn-sm btn_remove_template border" data-uid="<?php echo $value['Template']['id'] ?>" type="button"><i class="fa fa-times f-blue"></i></button> 
			<?php } ?>
	  		<button class="btn btn-default btn-sm btn_edit_template border" data-uid="<?php echo $value['Template']['id'] ?>" type="button"><i class="fa fa-edit f-blue"></i></button>
		</div>
	</div>
</div>
<?php endforeach ?>
